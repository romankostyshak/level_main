{
  "layout_cards": [
    {
      "id": 51,
      "x": 1.21,
      "y": 1.011,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 50,
      "x": 0.745,
      "y": 0.749,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 49,
      "x": 0.75,
      "y": -0.277,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 48,
      "x": 1.222,
      "y": 0.694,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 47,
      "x": -1.761,
      "y": -0.335,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        36,
        38
      ]
    },
    {
      "id": 46,
      "x": -1.758,
      "y": 0.819,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        44,
        36
      ]
    },
    {
      "id": 45,
      "x": 1.723,
      "y": 0.814,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        51,
        41
      ]
    },
    {
      "id": 44,
      "x": -1.21,
      "y": 1.014,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 43,
      "x": 1.725,
      "y": -0.347,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        41,
        37
      ]
    },
    {
      "id": 42,
      "x": -1.207,
      "y": 0.689,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 1.597,
      "y": 0.224,
      "layer": 2,
      "closed_by": [
        48,
        35
      ]
    },
    {
      "id": 40,
      "x": -0.753,
      "y": 0.745,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 39,
      "x": -0.758,
      "y": -0.279,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 37,
      "x": 1.207,
      "y": -0.555,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 36,
      "x": -1.605,
      "y": 0.23,
      "layer": 2,
      "closed_by": [
        42,
        34
      ]
    },
    {
      "id": 35,
      "x": 1.222,
      "y": -0.216,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -1.207,
      "y": -0.221,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 0.259,
      "y": 0.243,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 32,
      "y": 0.243,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -0.56,
      "y": 0.243,
      "layer": 1,
      "closed_by": [
        40,
        39,
        30
      ]
    },
    {
      "id": 30,
      "x": -0.263,
      "y": 0.243,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 29,
      "x": 0.546,
      "y": 0.243,
      "layer": 1,
      "closed_by": [
        50,
        49,
        33
      ]
    },
    {
      "id": 38,
      "x": -1.22,
      "y": -0.55,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        34
      ]
    }
  ],
  "cards_in_layout_amount": 23,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 62.5,
    "star2": 8.3,
    "star3": 28.6
  }
}