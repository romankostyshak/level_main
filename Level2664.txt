{
  "layout_cards": [
    {
      "id": 12,
      "x": -1.184,
      "y": 0.666,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        18
      ]
    },
    {
      "id": 13,
      "x": 1.026,
      "y": -0.293,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 14,
      "x": 0.856,
      "y": 0.666,
      "angle": 315.0,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 15,
      "x": -0.986,
      "y": -0.291,
      "angle": 315.0,
      "layer": 3,
      "closed_by": [
        20
      ]
    },
    {
      "id": 16,
      "x": 0.442,
      "y": -0.293,
      "angle": 315.0,
      "layer": 5,
      "closed_by": [
        50
      ]
    },
    {
      "id": 17,
      "x": -0.637,
      "y": 0.662,
      "angle": 315.0,
      "layer": 5,
      "closed_by": [
        51
      ]
    },
    {
      "id": 18,
      "x": -0.847,
      "y": 0.662,
      "angle": 315.0,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 19,
      "x": 0.688,
      "y": -0.296,
      "angle": 315.0,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 20,
      "x": -0.865,
      "y": -0.296,
      "angle": 315.0,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 21,
      "x": 0.712,
      "y": 0.661,
      "angle": 315.0,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 22,
      "x": 0.089,
      "y": 0.661,
      "angle": 315.0,
      "layer": 5,
      "closed_by": [
        51
      ]
    },
    {
      "id": 23,
      "x": -0.293,
      "y": -0.296,
      "angle": 315.0,
      "layer": 5,
      "closed_by": [
        50
      ]
    },
    {
      "id": 24,
      "y": -0.414,
      "layer": 2,
      "closed_by": [
        19,
        31
      ]
    },
    {
      "id": 25,
      "x": -0.289,
      "y": 0.66,
      "angle": 315.0,
      "layer": 4,
      "closed_by": [
        17,
        22
      ]
    },
    {
      "id": 26,
      "x": 0.003,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 27,
      "x": 0.086,
      "y": -0.298,
      "angle": 315.0,
      "layer": 4,
      "closed_by": [
        16,
        23
      ]
    },
    {
      "id": 28,
      "x": 0.003,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 29,
      "y": -0.577,
      "layer": 1,
      "closed_by": [
        24
      ]
    },
    {
      "id": 30,
      "x": 0.003,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        26
      ]
    },
    {
      "id": 31,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 32,
      "x": -2.042,
      "y": -0.377,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 33,
      "x": -1.424,
      "y": 0.662,
      "angle": 315.0,
      "layer": 1,
      "closed_by": [
        12,
        41
      ]
    },
    {
      "id": 34,
      "x": 1.264,
      "y": -0.293,
      "angle": 315.0,
      "layer": 1,
      "closed_by": [
        13,
        39
      ]
    },
    {
      "id": 35,
      "x": 1.092,
      "y": 0.666,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        14
      ]
    },
    {
      "id": 36,
      "x": -1.225,
      "y": -0.289,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        15
      ]
    },
    {
      "id": 37,
      "x": 1.332,
      "y": 0.665,
      "angle": 315.0,
      "layer": 1,
      "closed_by": [
        35,
        40
      ]
    },
    {
      "id": 38,
      "x": -1.468,
      "y": -0.293,
      "angle": 315.0,
      "layer": 1,
      "closed_by": [
        32,
        36
      ]
    },
    {
      "id": 39,
      "x": 1.725,
      "y": -0.365,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 40,
      "x": 1.748,
      "y": 0.583,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 41,
      "x": -2.019,
      "y": 0.582,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 42,
      "x": 1.968,
      "y": 0.583,
      "angle": 315.0,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 43,
      "x": 1.965,
      "y": -0.368,
      "angle": 315.0,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 44,
      "x": -2.253,
      "y": 0.582,
      "angle": 315.0,
      "layer": 3,
      "closed_by": [
        49
      ]
    },
    {
      "id": 45,
      "x": -2.523,
      "y": -0.377,
      "angle": 315.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 46,
      "x": -2.285,
      "y": -0.377,
      "angle": 315.0,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 47,
      "x": 2.206,
      "y": 0.583,
      "angle": 315.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 48,
      "x": 2.203,
      "y": -0.365,
      "angle": 315.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 49,
      "x": -2.492,
      "y": 0.582,
      "angle": 315.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 50,
      "x": 0.259,
      "y": -0.293,
      "angle": 315.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -0.46,
      "y": 0.662,
      "angle": 315.0,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 40,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 3.5,
    "star2": 56.8,
    "star3": 39.2
  }
}