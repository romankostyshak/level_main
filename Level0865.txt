{
  "layout_cards": [
    {
      "id": 51,
      "y": -0.414,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": 0.001,
      "y": -0.09,
      "layer": 2,
      "closed_by": [
        49,
        48
      ]
    },
    {
      "id": 49,
      "x": 0.266,
      "y": 0.14,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 48,
      "x": -0.256,
      "y": 0.141,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 46,
      "x": -0.569,
      "y": 0.14,
      "layer": 1,
      "closed_by": [
        41,
        34
      ]
    },
    {
      "id": 45,
      "x": 0.014,
      "y": 0.861,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 44,
      "x": 0.414,
      "y": 1.021,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 43,
      "x": 0.013,
      "y": 0.537,
      "layer": 4,
      "closed_by": [
        45
      ]
    },
    {
      "id": 42,
      "x": -0.412,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 41,
      "x": -0.864,
      "y": 0.861,
      "layer": 2,
      "closed_by": [
        42,
        35
      ]
    },
    {
      "id": 39,
      "x": 0.574,
      "y": 0.142,
      "layer": 1,
      "closed_by": [
        38,
        47
      ]
    },
    {
      "id": 38,
      "x": 0.86,
      "y": 0.861,
      "layer": 2,
      "closed_by": [
        44,
        37
      ]
    },
    {
      "id": 37,
      "x": 1.269,
      "y": 0.465,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 36,
      "x": 1.266,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 30,
      "x": 1.269,
      "y": -0.256,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 40,
      "x": -1.266,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        41
      ]
    },
    {
      "id": 35,
      "x": -1.266,
      "y": 0.46,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 33,
      "x": -1.266,
      "y": -0.259,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 0.865,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 34,
      "x": -0.568,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        48,
        32
      ]
    },
    {
      "id": 32,
      "x": -0.865,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 47,
      "x": 0.574,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        49,
        31
      ]
    }
  ],
  "cards_in_layout_amount": 22,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 31.7,
    "star2": 2.3,
    "star3": 65.5
  }
}