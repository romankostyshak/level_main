{
  "layout_cards": [
    {
      "id": 17,
      "x": -0.957,
      "y": 1.049,
      "layer": 1,
      "closed_by": [
        19,
        20
      ]
    },
    {
      "id": 18,
      "x": -0.962,
      "y": 0.888,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 19,
      "x": -0.578,
      "y": 1.047,
      "layer": 2,
      "closed_by": [
        21
      ]
    },
    {
      "id": 20,
      "x": -1.343,
      "y": 1.047,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 21,
      "x": -0.698,
      "y": 0.888,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 22,
      "x": -1.212,
      "y": 0.888,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 23,
      "x": -1.585,
      "y": 1.049,
      "layer": 1,
      "closed_by": [
        20
      ]
    },
    {
      "id": 24,
      "x": -0.331,
      "y": 1.049,
      "layer": 1,
      "closed_by": [
        19
      ]
    },
    {
      "id": 25,
      "x": 0.335,
      "y": -0.601,
      "layer": 1,
      "closed_by": [
        28
      ]
    },
    {
      "id": 26,
      "x": 0.962,
      "y": -0.605,
      "layer": 1,
      "closed_by": [
        28,
        29
      ]
    },
    {
      "id": 27,
      "x": 1.587,
      "y": -0.605,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 28,
      "x": 0.583,
      "y": -0.605,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 29,
      "x": 1.35,
      "y": -0.601,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 30,
      "x": 1.667,
      "y": 0.465,
      "layer": 5,
      "closed_by": [
        50
      ]
    },
    {
      "id": 31,
      "x": -1.264,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 32,
      "x": 0.702,
      "y": -0.446,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 33,
      "x": 1.215,
      "y": -0.444,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 34,
      "x": 0.958,
      "y": -0.284,
      "layer": 4,
      "closed_by": [
        49
      ]
    },
    {
      "id": 35,
      "x": -0.972,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        31,
        40
      ]
    },
    {
      "id": 36,
      "x": 0.975,
      "y": 0.698,
      "layer": 1,
      "closed_by": [
        37,
        41
      ]
    },
    {
      "id": 37,
      "x": 1.266,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 38,
      "x": -0.972,
      "y": -0.577,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 39,
      "x": 0.972,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 40,
      "x": -0.569,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        38,
        42
      ]
    },
    {
      "id": 41,
      "x": 0.574,
      "y": 0.623,
      "layer": 2,
      "closed_by": [
        39,
        43
      ]
    },
    {
      "id": 42,
      "x": -0.261,
      "y": -0.017,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 43,
      "x": 0.268,
      "y": 0.462,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": -0.028,
      "y": 0.216,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 45,
      "x": 1.268,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 46,
      "x": -1.264,
      "y": -0.495,
      "layer": 4,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "x": -1.661,
      "y": -0.017,
      "layer": 5,
      "closed_by": [
        51
      ]
    },
    {
      "id": 48,
      "x": -0.962,
      "y": 0.722,
      "layer": 4,
      "closed_by": [
        18
      ]
    },
    {
      "id": 49,
      "x": 0.958,
      "y": -0.448,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 50,
      "x": 2.013,
      "y": 0.224,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -2.012,
      "y": 0.216,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 35,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 43.7,
    "star2": 50.7,
    "star3": 4.7
  }
}