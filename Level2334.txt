{
  "layout_cards": [
    {
      "id": 21,
      "x": -1.644,
      "y": -0.451,
      "angle": 45.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 22,
      "x": 1.746,
      "y": 0.46,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 1.633,
      "y": -0.462,
      "angle": 315.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 24,
      "x": -1.549,
      "y": 0.921,
      "angle": 315.0,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 25,
      "x": 1.536,
      "y": 0.925,
      "angle": 45.0,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 26,
      "x": -1.506,
      "y": 0.061,
      "layer": 3,
      "closed_by": [
        21,
        37
      ]
    },
    {
      "id": 27,
      "x": 1.508,
      "y": 0.061,
      "layer": 3,
      "closed_by": [
        22,
        23
      ]
    },
    {
      "id": 28,
      "x": -1.266,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        24,
        26,
        41
      ]
    },
    {
      "id": 29,
      "x": 1.266,
      "y": -0.178,
      "layer": 2,
      "closed_by": [
        27,
        40
      ]
    },
    {
      "id": 30,
      "x": 1.266,
      "y": 0.698,
      "layer": 2,
      "closed_by": [
        25,
        27,
        42
      ]
    },
    {
      "id": 31,
      "x": -0.734,
      "y": -0.172,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -0.734,
      "y": 0.697,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 0.722,
      "y": -0.172,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 0.744,
      "y": 0.694,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 0.386,
      "y": 0.259,
      "layer": 3,
      "closed_by": [
        33,
        34
      ]
    },
    {
      "id": 36,
      "x": -1.266,
      "y": -0.178,
      "layer": 2,
      "closed_by": [
        26,
        39
      ]
    },
    {
      "id": 37,
      "x": -1.745,
      "y": 0.462,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -0.384,
      "y": 0.266,
      "layer": 3,
      "closed_by": [
        31,
        32
      ]
    },
    {
      "id": 39,
      "x": -0.758,
      "y": -0.409,
      "angle": 45.0,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 40,
      "x": 0.754,
      "y": -0.412,
      "angle": 315.0,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 41,
      "x": -0.758,
      "y": 0.93,
      "angle": 315.0,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 42,
      "x": 0.754,
      "y": 0.944,
      "angle": 45.0,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 43,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        35,
        38
      ]
    },
    {
      "id": 44,
      "x": -0.572,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        38,
        39
      ]
    },
    {
      "id": 45,
      "x": -0.003,
      "y": 0.702,
      "layer": 2,
      "closed_by": [
        35,
        38
      ]
    },
    {
      "id": 46,
      "x": -0.574,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        38,
        41
      ]
    },
    {
      "id": 47,
      "x": 0.573,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        35,
        42
      ]
    },
    {
      "id": 48,
      "x": 0.333,
      "y": 0.3,
      "layer": 1,
      "closed_by": [
        43,
        45,
        47,
        50
      ]
    },
    {
      "id": 49,
      "x": -0.331,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        43,
        44,
        45,
        46
      ]
    },
    {
      "id": 50,
      "x": 0.572,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        35,
        40
      ]
    }
  ],
  "cards_in_layout_amount": 30,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 38.8,
    "star2": 52.8,
    "star3": 7.7
  }
}