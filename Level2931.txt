{
  "layout_cards": [
    {
      "id": 22,
      "x": 1.679,
      "y": -0.497,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 1.679,
      "y": 0.86,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 1.679,
      "y": 0.638,
      "layer": 2,
      "closed_by": [
        23
      ],
      "card_type": 6
    },
    {
      "id": 25,
      "x": 1.679,
      "y": -0.199,
      "layer": 2,
      "closed_by": [
        22
      ],
      "card_type": 6
    },
    {
      "id": 28,
      "x": 2.394,
      "y": -0.18,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 29,
      "x": 2.394,
      "y": 0.632,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 2.394,
      "y": 0.861,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 31,
      "x": 2.786,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        30,
        33
      ]
    },
    {
      "id": 32,
      "x": 2.015,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        24,
        25,
        30,
        33
      ]
    },
    {
      "id": 33,
      "x": 2.394,
      "y": -0.493,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 34,
      "x": -2.404,
      "y": -0.18,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 1.345,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        24,
        25,
        38,
        49
      ]
    },
    {
      "id": 36,
      "x": 0.949,
      "y": 0.633,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 37,
      "x": 0.958,
      "y": -0.18,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 0.962,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 39,
      "x": -2.398,
      "y": 0.861,
      "layer": 2,
      "closed_by": [
        51
      ]
    },
    {
      "id": 40,
      "x": -0.947,
      "y": 0.634,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 41,
      "x": -0.947,
      "y": -0.172,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -0.572,
      "y": 0.224,
      "layer": 1,
      "closed_by": [
        43,
        44
      ]
    },
    {
      "id": 43,
      "x": -0.949,
      "y": 0.861,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 44,
      "x": -0.947,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 45,
      "x": -1.343,
      "y": 0.221,
      "layer": 1,
      "closed_by": [
        43,
        44
      ]
    },
    {
      "id": 46,
      "x": -2.015,
      "y": 0.223,
      "layer": 1,
      "closed_by": [
        39,
        48
      ]
    },
    {
      "id": 47,
      "x": -2.785,
      "y": 0.223,
      "layer": 1,
      "closed_by": [
        39,
        48
      ]
    },
    {
      "id": 48,
      "x": -2.404,
      "y": -0.414,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 49,
      "x": 0.948,
      "y": 0.864,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 50,
      "x": 0.573,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        38,
        49
      ]
    },
    {
      "id": 51,
      "x": -2.398,
      "y": 0.632,
      "layer": 3,
      "closed_by": []
    }
  ],
  "level_mechanic": 1,
  "free_cards_from_stickers": [
    22,
    23,
    24,
    25,
    28,
    29,
    30,
    31,
    32,
    33,
    35,
    36,
    37,
    38,
    49,
    50
  ],
  "cards_in_layout_amount": 28,
  "cards_in_deck_amount": 22,
  "stars_statistic": {
    "star1": 52.2,
    "star2": 34.7,
    "star3": 12.9
  }
}