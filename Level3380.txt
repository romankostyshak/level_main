{
  "layout_cards": [
    {
      "id": 8,
      "x": 0.56,
      "y": 0.462,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 9,
      "x": -0.555,
      "y": 0.458,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 10,
      "x": 0.002,
      "y": 0.587,
      "layer": 6,
      "closed_by": [
        26
      ]
    },
    {
      "id": 11,
      "x": 0.564,
      "y": -0.017,
      "layer": 6,
      "closed_by": [
        8
      ]
    },
    {
      "id": 12,
      "x": -0.555,
      "y": -0.026,
      "layer": 6,
      "closed_by": [
        9
      ]
    },
    {
      "id": 13,
      "x": 0.56,
      "y": 0.587,
      "layer": 4,
      "closed_by": [
        11
      ]
    },
    {
      "id": 14,
      "x": 0.002,
      "y": -0.025,
      "layer": 4,
      "closed_by": [
        10
      ]
    },
    {
      "id": 15,
      "x": -0.555,
      "y": 0.592,
      "layer": 4,
      "closed_by": [
        12
      ]
    },
    {
      "id": 16,
      "x": -0.555,
      "y": -0.158,
      "layer": 3,
      "closed_by": [
        15
      ]
    },
    {
      "id": 17,
      "x": 0.564,
      "y": -0.158,
      "layer": 3,
      "closed_by": [
        13
      ]
    },
    {
      "id": 18,
      "x": 2.148,
      "y": 0.414,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 19,
      "x": 1.588,
      "y": 0.409,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 20,
      "x": -0.555,
      "y": 0.731,
      "layer": 2,
      "closed_by": [
        15
      ]
    },
    {
      "id": 21,
      "x": 0.002,
      "y": 0.726,
      "layer": 2,
      "closed_by": [
        14
      ]
    },
    {
      "id": 22,
      "x": 0.563,
      "y": 0.726,
      "layer": 2,
      "closed_by": [
        13
      ]
    },
    {
      "id": 23,
      "x": 0.001,
      "y": -0.289,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 24,
      "x": -0.555,
      "y": -0.289,
      "layer": 2,
      "closed_by": [
        12,
        16
      ]
    },
    {
      "id": 25,
      "x": 0.001,
      "y": -0.158,
      "layer": 3,
      "closed_by": [
        14
      ]
    },
    {
      "id": 26,
      "x": 0.001,
      "y": 0.1,
      "layer": 7,
      "closed_by": [],
      "card_type": 2
    },
    {
      "id": 27,
      "x": -1.593,
      "y": 0.037,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 28,
      "x": -2.154,
      "y": 0.035,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 29,
      "x": 0.564,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        30
      ]
    },
    {
      "id": 30,
      "x": 0.564,
      "y": -0.289,
      "layer": 2,
      "closed_by": [
        11,
        17
      ]
    },
    {
      "id": 31,
      "x": -2.154,
      "y": -0.09,
      "layer": 1,
      "closed_by": [
        28
      ]
    },
    {
      "id": 32,
      "x": -1.593,
      "y": -0.09,
      "layer": 1,
      "closed_by": [
        27
      ]
    },
    {
      "id": 33,
      "x": -0.555,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        24
      ]
    },
    {
      "id": 34,
      "y": -0.416,
      "layer": 1,
      "closed_by": [
        23,
        26
      ]
    },
    {
      "id": 35,
      "x": 2.148,
      "y": 0.541,
      "layer": 1,
      "closed_by": [
        18
      ]
    },
    {
      "id": 36,
      "x": 1.588,
      "y": 0.541,
      "layer": 1,
      "closed_by": [
        19
      ]
    },
    {
      "id": 37,
      "x": 0.56,
      "y": 0.86,
      "layer": 1,
      "closed_by": [
        8,
        22
      ]
    },
    {
      "id": 38,
      "y": 0.856,
      "layer": 1,
      "closed_by": [
        10,
        21
      ]
    },
    {
      "id": 39,
      "x": -0.555,
      "y": 0.856,
      "layer": 1,
      "closed_by": [
        9,
        20
      ]
    },
    {
      "id": 40,
      "x": 1.266,
      "y": 0.054,
      "angle": 349.997,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 1.865,
      "y": 0.275,
      "layer": 3,
      "closed_by": [
        44,
        45
      ]
    },
    {
      "id": 42,
      "x": -2.154,
      "y": 0.303,
      "layer": 4,
      "closed_by": [
        48,
        50
      ]
    },
    {
      "id": 43,
      "x": -1.876,
      "y": 0.164,
      "layer": 3,
      "closed_by": [
        42,
        47
      ],
      "card_type": 3
    },
    {
      "id": 44,
      "x": 2.151,
      "y": 0.148,
      "layer": 4,
      "closed_by": [
        46,
        49
      ]
    },
    {
      "id": 45,
      "x": 1.588,
      "y": 0.143,
      "layer": 4,
      "closed_by": [
        40,
        46
      ]
    },
    {
      "id": 46,
      "x": 1.868,
      "y": 0.017,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 47,
      "x": -1.592,
      "y": 0.3,
      "layer": 4,
      "closed_by": [
        48,
        51
      ]
    },
    {
      "id": 48,
      "x": -1.873,
      "y": 0.432,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 49,
      "x": 2.476,
      "y": 0.054,
      "angle": 9.998,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 50,
      "x": -2.483,
      "y": 0.388,
      "angle": 9.998,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -1.266,
      "y": 0.386,
      "angle": 349.997,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 44,
  "cards_in_deck_amount": 32,
  "stars_statistic": {
    "star1": 16.9,
    "star2": 68.6,
    "star3": 14.3
  }
}