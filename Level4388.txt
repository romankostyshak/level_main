{
  "layout_cards": [
    {
      "id": 15,
      "x": -0.259,
      "y": -0.119,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 18,
      "x": 0.259,
      "y": -0.119,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 19,
      "x": -0.259,
      "y": 0.72,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 20,
      "x": 0.259,
      "y": 0.72,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 21,
      "x": 2.059,
      "y": -0.418,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -2.059,
      "y": 1.039,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 2.059,
      "y": 1.039,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 1.899,
      "y": 0.479,
      "angle": 330.0,
      "layer": 6,
      "closed_by": [
        23
      ]
    },
    {
      "id": 25,
      "x": -1.899,
      "y": 0.479,
      "angle": 30.0,
      "layer": 6,
      "closed_by": [
        22
      ]
    },
    {
      "id": 26,
      "x": -1.899,
      "y": 0.136,
      "angle": 330.0,
      "layer": 5,
      "closed_by": [
        25,
        51
      ]
    },
    {
      "id": 27,
      "x": 1.899,
      "y": 0.136,
      "angle": 30.0,
      "layer": 5,
      "closed_by": [
        21,
        24
      ]
    },
    {
      "id": 28,
      "x": 1.598,
      "y": 0.298,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 29,
      "x": -1.319,
      "y": 0.479,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 30,
      "x": 1.319,
      "y": 0.479,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 31,
      "x": -1.179,
      "y": 1.039,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 32,
      "x": 1.179,
      "y": 1.039,
      "layer": 1,
      "closed_by": [
        30
      ]
    },
    {
      "id": 33,
      "x": 1.319,
      "y": 0.136,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 34,
      "x": -1.319,
      "y": 0.136,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 35,
      "x": 0.259,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        18
      ]
    },
    {
      "id": 36,
      "x": -0.259,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        15
      ]
    },
    {
      "id": 37,
      "x": -1.6,
      "y": 0.298,
      "layer": 4,
      "closed_by": [
        26
      ]
    },
    {
      "id": 39,
      "x": -0.259,
      "y": 1.039,
      "layer": 5,
      "closed_by": [
        19
      ]
    },
    {
      "id": 40,
      "x": 0.259,
      "y": 1.039,
      "layer": 5,
      "closed_by": [
        20
      ]
    },
    {
      "id": 41,
      "x": -0.578,
      "y": 1.039,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 42,
      "x": 0.578,
      "y": 1.039,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 43,
      "x": 0.259,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        35,
        42
      ]
    },
    {
      "id": 44,
      "x": -0.259,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        36,
        41
      ]
    },
    {
      "id": 45,
      "x": 0.578,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 46,
      "x": 1.179,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        28,
        33
      ]
    },
    {
      "id": 47,
      "x": 0.578,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 48,
      "x": -0.578,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 49,
      "x": -1.179,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        34,
        37
      ]
    },
    {
      "id": 50,
      "x": -0.578,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 51,
      "x": -2.059,
      "y": -0.418,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 34,
  "cards_in_deck_amount": 28,
  "layers_in_level": 7,
  "open_cards": 8,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 28.9,
    "star2": 65.8,
    "star3": 3.7
  }
}