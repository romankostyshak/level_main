{
  "layout_cards": [
    {
      "id": 15,
      "x": 0.467,
      "y": 0.777,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 16,
      "y": 0.861,
      "layer": 1,
      "closed_by": [
        17,
        18
      ]
    },
    {
      "id": 17,
      "x": 0.305,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 18,
      "x": -0.307,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 19,
      "x": 0.388,
      "y": 0.938,
      "layer": 3,
      "closed_by": [
        15
      ]
    },
    {
      "id": 20,
      "x": -0.386,
      "y": 0.938,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 21,
      "x": -0.467,
      "y": 0.777,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 22,
      "x": 0.001,
      "y": -0.093,
      "layer": 1,
      "closed_by": [
        23,
        24
      ]
    },
    {
      "id": 23,
      "x": 0.305,
      "y": -0.18,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 24,
      "x": -0.305,
      "y": -0.18,
      "layer": 2,
      "closed_by": [
        26
      ]
    },
    {
      "id": 25,
      "x": 0.307,
      "y": -0.497,
      "layer": 3,
      "closed_by": [
        27,
        30
      ]
    },
    {
      "id": 26,
      "x": -0.305,
      "y": -0.493,
      "layer": 3,
      "closed_by": [
        27,
        31
      ]
    },
    {
      "id": 27,
      "y": -0.574,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 1.588,
      "y": -0.178,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 29,
      "x": 1.105,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        36
      ]
    },
    {
      "id": 30,
      "x": 0.546,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        32
      ]
    },
    {
      "id": 31,
      "x": -0.544,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 32,
      "x": 0.865,
      "y": -0.179,
      "layer": 5,
      "closed_by": [
        34,
        36
      ]
    },
    {
      "id": 33,
      "x": -0.865,
      "y": -0.18,
      "layer": 5,
      "closed_by": [
        35,
        37
      ]
    },
    {
      "id": 34,
      "x": 1.026,
      "y": -0.578,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -1.029,
      "y": -0.578,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 1.054,
      "y": 0.3,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 37,
      "x": -1.054,
      "y": 0.303,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -1.105,
      "y": 0.702,
      "layer": 5,
      "closed_by": [
        37
      ]
    },
    {
      "id": 39,
      "x": 1.105,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 40,
      "x": -1.105,
      "y": 1.021,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 41,
      "x": 1.345,
      "y": 0.782,
      "layer": 3,
      "closed_by": [
        39,
        48
      ]
    },
    {
      "id": 42,
      "x": -1.348,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        40,
        49
      ]
    },
    {
      "id": 43,
      "x": 1.427,
      "y": -0.179,
      "layer": 5,
      "closed_by": [
        34,
        36
      ]
    },
    {
      "id": 44,
      "x": -1.421,
      "y": -0.177,
      "layer": 5,
      "closed_by": [
        35,
        37
      ]
    },
    {
      "id": 45,
      "x": -1.582,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        44
      ]
    },
    {
      "id": 46,
      "x": 1.585,
      "y": -0.574,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 47,
      "x": -1.582,
      "y": -0.572,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 48,
      "x": 1.667,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        50
      ]
    },
    {
      "id": 49,
      "x": -1.664,
      "y": 0.702,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 50,
      "x": 1.667,
      "y": 1.021,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -1.664,
      "y": 1.019,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 37,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 5.0,
    "star2": 57.4,
    "star3": 37.0
  }
}