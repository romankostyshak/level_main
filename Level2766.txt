{
  "layout_cards": [
    {
      "id": 21,
      "x": 1.746,
      "y": -0.259,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -1.985,
      "y": 0.14,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 23,
      "x": 1.909,
      "y": 0.141,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 24,
      "x": -1.424,
      "y": -0.495,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 25,
      "x": 1.347,
      "y": -0.495,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 26,
      "x": -1.585,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        22,
        24
      ]
    },
    {
      "id": 27,
      "x": 1.506,
      "y": -0.261,
      "layer": 2,
      "closed_by": [
        23,
        25
      ]
    },
    {
      "id": 28,
      "x": 0.331,
      "y": -0.418,
      "layer": 5,
      "closed_by": [
        36
      ]
    },
    {
      "id": 29,
      "x": -0.335,
      "y": -0.423,
      "layer": 5,
      "closed_by": [
        36
      ]
    },
    {
      "id": 30,
      "x": 0.573,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 31,
      "x": -0.573,
      "y": -0.421,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 32,
      "x": 0.734,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 33,
      "x": -0.813,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 34,
      "x": 0.944,
      "y": -0.574,
      "layer": 2,
      "closed_by": [
        25,
        32
      ]
    },
    {
      "id": 35,
      "x": -1.026,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        24,
        33
      ]
    },
    {
      "id": 36,
      "x": -0.003,
      "y": -0.418,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 37,
      "x": -1.827,
      "y": -0.261,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 1.184,
      "y": -0.261,
      "layer": 1,
      "closed_by": [
        27,
        34
      ]
    },
    {
      "id": 39,
      "x": -1.184,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        26,
        35
      ]
    },
    {
      "id": 40,
      "x": -1.184,
      "y": 0.62,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 1.187,
      "y": 0.62,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -0.944,
      "y": 1.019,
      "layer": 5,
      "closed_by": [
        40
      ]
    },
    {
      "id": 43,
      "x": 0.944,
      "y": 1.019,
      "layer": 5,
      "closed_by": [
        41
      ]
    },
    {
      "id": 44,
      "x": 0.708,
      "y": 0.702,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 45,
      "x": 0.266,
      "y": 0.624,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 46,
      "x": -0.707,
      "y": 0.703,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 47,
      "x": -0.465,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": -0.261,
      "y": 0.624,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 49,
      "x": 0.002,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        45,
        48
      ]
    },
    {
      "id": 50,
      "x": 0.467,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        44
      ]
    }
  ],
  "cards_in_layout_amount": 30,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 44.4,
    "star2": 47.0,
    "star3": 7.7
  }
}