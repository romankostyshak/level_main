{
  "layout_cards": [
    {
      "id": 49,
      "x": 0.331,
      "y": -0.493,
      "layer": 1,
      "closed_by": [
        47,
        25
      ]
    },
    {
      "id": 48,
      "x": -0.333,
      "y": -0.5,
      "layer": 1,
      "closed_by": [
        46,
        25
      ]
    },
    {
      "id": 47,
      "x": 0.703,
      "y": -0.493,
      "layer": 2,
      "closed_by": [
        45,
        24
      ]
    },
    {
      "id": 46,
      "x": -0.703,
      "y": -0.499,
      "layer": 2,
      "closed_by": [
        50,
        23
      ]
    },
    {
      "id": 45,
      "x": 1.108,
      "y": -0.493,
      "layer": 3,
      "closed_by": [
        44,
        22
      ]
    },
    {
      "id": 44,
      "x": 1.5,
      "y": -0.5,
      "layer": 4,
      "closed_by": [
        42,
        37
      ]
    },
    {
      "id": 43,
      "x": -1.506,
      "y": -0.5,
      "layer": 4,
      "closed_by": [
        41,
        20
      ]
    },
    {
      "id": 50,
      "x": -1.103,
      "y": -0.5,
      "layer": 3,
      "closed_by": [
        43,
        21
      ]
    },
    {
      "id": 42,
      "x": 1.781,
      "y": -0.458,
      "angle": 15.0,
      "layer": 5,
      "closed_by": [
        40
      ]
    },
    {
      "id": 41,
      "x": -1.792,
      "y": -0.462,
      "angle": 345.0,
      "layer": 5,
      "closed_by": [
        39
      ]
    },
    {
      "id": 40,
      "x": 2.046,
      "y": -0.349,
      "angle": 30.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -2.042,
      "y": -0.351,
      "angle": 330.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -0.331,
      "y": 0.944,
      "layer": 1,
      "closed_by": [
        33,
        25
      ]
    },
    {
      "id": 34,
      "x": 0.708,
      "y": 0.944,
      "layer": 2,
      "closed_by": [
        32,
        24
      ]
    },
    {
      "id": 33,
      "x": -0.703,
      "y": 0.944,
      "layer": 2,
      "closed_by": [
        31,
        23
      ]
    },
    {
      "id": 32,
      "x": 1.105,
      "y": 0.944,
      "layer": 3,
      "closed_by": [
        30,
        22
      ]
    },
    {
      "id": 31,
      "x": -1.1,
      "y": 0.943,
      "layer": 3,
      "closed_by": [
        29,
        21
      ]
    },
    {
      "id": 30,
      "x": 1.503,
      "y": 0.944,
      "layer": 4,
      "closed_by": [
        28,
        37
      ]
    },
    {
      "id": 29,
      "x": -1.5,
      "y": 0.943,
      "layer": 4,
      "closed_by": [
        36,
        20
      ]
    },
    {
      "id": 28,
      "x": 1.781,
      "y": 0.907,
      "angle": 345.0,
      "layer": 5,
      "closed_by": [
        27
      ]
    },
    {
      "id": 36,
      "x": -1.781,
      "y": 0.907,
      "angle": 15.0,
      "layer": 5,
      "closed_by": [
        26
      ]
    },
    {
      "id": 27,
      "x": 2.046,
      "y": 0.795,
      "angle": 330.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -2.045,
      "y": 0.799,
      "angle": 30.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 25,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        24,
        23
      ]
    },
    {
      "id": 38,
      "x": 0.335,
      "y": 0.944,
      "layer": 1,
      "closed_by": [
        34,
        25
      ]
    },
    {
      "id": 24,
      "x": 0.388,
      "y": 0.224,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 23,
      "x": -0.384,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 22,
      "x": 0.787,
      "y": 0.228,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 21,
      "x": -0.787,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 37,
      "x": 1.184,
      "y": 0.231,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 20,
      "x": -1.182,
      "y": 0.223,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 31,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 54.7,
    "star2": 27.4,
    "star3": 17.3
  }
}