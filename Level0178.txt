{
  "layout_cards": [
    {
      "id": 51,
      "x": -2.176,
      "y": 1.041,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 50,
      "x": -2.176,
      "y": 0.441,
      "layer": 5,
      "closed_by": [
        45
      ]
    },
    {
      "id": 49,
      "x": 2.207,
      "y": -0.572,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 48,
      "x": -1.355,
      "y": -0.358,
      "layer": 2,
      "closed_by": [
        37,
        34
      ]
    },
    {
      "id": 47,
      "x": 2.207,
      "y": -0.108,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 46,
      "x": 1.809,
      "y": 0.699,
      "layer": 6,
      "closed_by": [
        41
      ]
    },
    {
      "id": 45,
      "x": -1.814,
      "y": 0.699,
      "layer": 6,
      "closed_by": [
        51
      ]
    },
    {
      "id": 44,
      "x": 1.35,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        46
      ]
    },
    {
      "id": 43,
      "x": 1.368,
      "y": -0.358,
      "layer": 2,
      "closed_by": [
        42,
        32
      ]
    },
    {
      "id": 42,
      "x": 0.907,
      "y": -0.358,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 2.207,
      "y": 1.041,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 2.207,
      "y": 0.441,
      "layer": 5,
      "closed_by": [
        46
      ]
    },
    {
      "id": 39,
      "x": -2.176,
      "y": -0.108,
      "layer": 4,
      "closed_by": [
        50
      ]
    },
    {
      "id": 38,
      "x": -2.176,
      "y": -0.573,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 37,
      "x": -1.812,
      "y": -0.358,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 36,
      "x": -0.893,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 35,
      "x": -1.355,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        45
      ]
    },
    {
      "id": 34,
      "x": -0.896,
      "y": -0.358,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 0.907,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        44
      ]
    },
    {
      "id": 32,
      "x": 1.809,
      "y": -0.358,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 31,
      "y": 0.119,
      "layer": 4,
      "closed_by": [
        30
      ],
      "card_type": 6
    },
    {
      "id": 28,
      "y": 0.72,
      "layer": 2,
      "closed_by": [
        27
      ]
    },
    {
      "id": 30,
      "y": -0.199,
      "layer": 5,
      "closed_by": [
        29
      ]
    },
    {
      "id": 29,
      "y": -0.5,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 27,
      "y": 0.418,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 26,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        28
      ]
    }
  ],
  "cards_in_layout_amount": 26,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 57.1,
    "star2": 11.0,
    "star3": 30.9
  }
}