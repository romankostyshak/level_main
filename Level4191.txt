{
  "layout_cards": [
    {
      "x": -2.279,
      "y": -0.101,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [
        4
      ]
    },
    {
      "id": 1,
      "x": -1.679,
      "y": -0.342,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        2
      ]
    },
    {
      "id": 2,
      "x": -1.878,
      "y": -0.222,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        3
      ]
    },
    {
      "id": 3,
      "x": -2.078,
      "y": -0.162,
      "layer": 3,
      "closed_by": [
        0
      ]
    },
    {
      "id": 4,
      "x": -2.479,
      "y": 0.017,
      "angle": 330.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 5,
      "x": 2.482,
      "y": 0.041,
      "angle": 30.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 6,
      "x": 2.273,
      "y": -0.075,
      "angle": 30.0,
      "layer": 4,
      "closed_by": [
        5
      ]
    },
    {
      "id": 7,
      "x": 2.094,
      "y": -0.136,
      "layer": 3,
      "closed_by": [
        6
      ]
    },
    {
      "id": 8,
      "x": 1.894,
      "y": -0.194,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        7
      ]
    },
    {
      "id": 9,
      "x": 1.674,
      "y": -0.321,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        8
      ]
    },
    {
      "id": 29,
      "x": 0.66,
      "y": 0.759,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -2.242,
      "y": 0.856,
      "angle": 330.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -1.842,
      "y": 0.675,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 32,
      "x": -1.641,
      "y": 0.615,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 33,
      "x": -1.442,
      "y": 0.495,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 34,
      "x": -0.499,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 35,
      "x": -0.578,
      "y": 0.398,
      "angle": 345.0,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 36,
      "x": -0.578,
      "y": 0.136,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        37,
        40
      ]
    },
    {
      "id": 37,
      "x": -0.499,
      "y": -0.5,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 0.497,
      "y": -0.476,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -2.042,
      "y": 0.736,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 40,
      "x": -0.66,
      "y": 0.759,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 0.578,
      "y": 0.141,
      "angle": 345.0,
      "layer": 3,
      "closed_by": [
        29,
        38
      ]
    },
    {
      "id": 42,
      "x": 0.578,
      "y": 0.4,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 45,
      "x": 2.25,
      "y": 0.878,
      "angle": 30.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 46,
      "x": 2.042,
      "y": 0.758,
      "angle": 30.0,
      "layer": 4,
      "closed_by": [
        45
      ]
    },
    {
      "id": 47,
      "x": 1.863,
      "y": 0.698,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": 1.661,
      "y": 0.638,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 49,
      "x": 1.442,
      "y": 0.513,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 50,
      "x": 0.497,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        29,
        42
      ]
    }
  ],
  "level_mechanic": 1,
  "free_cards_from_stickers": [
    5,
    6,
    7,
    8,
    9,
    29,
    38,
    41,
    42,
    45,
    46,
    47,
    48,
    49,
    50
  ],
  "cards_in_layout_amount": 30,
  "cards_in_deck_amount": 26,
  "layers_in_level": 5,
  "open_cards": 8,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 61.2,
    "star2": 37.7,
    "star3": 0.2
  }
}