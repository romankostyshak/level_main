{
  "layout_cards": [
    {
      "x": 1.985,
      "y": 0.215,
      "layer": 3,
      "closed_by": [
        4,
        14,
        46,
        47
      ]
    },
    {
      "id": 1,
      "x": -1.985,
      "y": 0.223,
      "layer": 3,
      "closed_by": [
        3,
        6,
        48,
        49
      ]
    },
    {
      "id": 2,
      "x": -2.223,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        1
      ]
    },
    {
      "id": 3,
      "x": -2.223,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        8
      ]
    },
    {
      "id": 4,
      "x": 2.223,
      "y": 0.703,
      "layer": 4,
      "closed_by": [
        9
      ]
    },
    {
      "id": 5,
      "x": -2.062,
      "y": -0.414,
      "layer": 5,
      "closed_by": [
        10
      ]
    },
    {
      "id": 6,
      "x": -2.223,
      "y": -0.259,
      "layer": 4,
      "closed_by": [
        5
      ]
    },
    {
      "id": 7,
      "x": 2.223,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        0
      ]
    },
    {
      "id": 8,
      "x": -2.065,
      "y": 0.86,
      "layer": 5,
      "closed_by": [
        11
      ]
    },
    {
      "id": 9,
      "x": 2.065,
      "y": 0.859,
      "layer": 5,
      "closed_by": [
        12
      ]
    },
    {
      "id": 10,
      "x": -1.745,
      "y": -0.493,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 11,
      "x": -1.745,
      "y": 0.938,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 12,
      "x": 1.743,
      "y": 0.939,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 13,
      "x": 1.746,
      "y": -0.5,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 14,
      "x": 2.223,
      "y": -0.256,
      "layer": 4,
      "closed_by": [
        15
      ]
    },
    {
      "id": 15,
      "x": 2.065,
      "y": -0.414,
      "layer": 5,
      "closed_by": [
        13
      ]
    },
    {
      "id": 16,
      "x": 0.624,
      "y": 0.754,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 17,
      "x": 0.703,
      "y": -0.18,
      "layer": 1,
      "closed_by": [
        18
      ]
    },
    {
      "id": 18,
      "x": 0.625,
      "y": -0.307,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 19,
      "x": 0.483,
      "y": -0.439,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 20,
      "x": -0.707,
      "y": 0.632,
      "layer": 1,
      "closed_by": [
        24
      ]
    },
    {
      "id": 21,
      "x": 0.708,
      "y": 0.634,
      "layer": 1,
      "closed_by": [
        16
      ]
    },
    {
      "id": 22,
      "x": 0.469,
      "y": 0.893,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 23,
      "x": -0.703,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        25
      ]
    },
    {
      "id": 24,
      "x": -0.625,
      "y": 0.744,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        26
      ]
    },
    {
      "id": 25,
      "x": -0.623,
      "y": -0.298,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        27
      ]
    },
    {
      "id": 26,
      "x": -0.472,
      "y": 0.892,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 27,
      "x": -0.481,
      "y": -0.439,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 28,
      "x": -0.335,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 29,
      "x": -1.373,
      "y": -0.493,
      "layer": 3,
      "closed_by": [
        49
      ]
    },
    {
      "id": 30,
      "x": -1.373,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 31,
      "x": 0.331,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 32,
      "x": -0.331,
      "y": -0.497,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 33,
      "x": 0.335,
      "y": -0.497,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 34,
      "x": 0.003,
      "y": 1.021,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 0.001,
      "y": -0.577,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 1.375,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 37,
      "x": -1.373,
      "y": 0.638,
      "layer": 1,
      "closed_by": [
        41
      ]
    },
    {
      "id": 38,
      "x": 1.375,
      "y": 0.638,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 39,
      "x": 1.375,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 40,
      "x": 1.375,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 41,
      "x": -1.373,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 42,
      "x": -1.373,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 43,
      "x": 1.375,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 44,
      "x": -1.373,
      "y": 0.938,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 45,
      "x": 1.375,
      "y": -0.5,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 46,
      "x": 1.526,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        50
      ]
    },
    {
      "id": 47,
      "x": 1.524,
      "y": 0.634,
      "layer": 4,
      "closed_by": [
        50
      ]
    },
    {
      "id": 48,
      "x": -1.534,
      "y": 0.633,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 49,
      "x": -1.531,
      "y": -0.18,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 50,
      "x": 1.373,
      "y": 0.231,
      "layer": 5,
      "closed_by": [
        12,
        13
      ]
    },
    {
      "id": 51,
      "x": -1.371,
      "y": 0.223,
      "layer": 5,
      "closed_by": [
        10,
        11
      ]
    }
  ],
  "cards_in_layout_amount": 52,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 0.0,
    "star2": 2.8,
    "star3": 96.2
  }
}