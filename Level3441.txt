{
  "layout_cards": [
    {
      "id": 18,
      "x": -1.559,
      "y": 0.81,
      "angle": 340.0,
      "layer": 2,
      "closed_by": [
        51
      ]
    },
    {
      "id": 19,
      "x": 1.559,
      "y": 0.893,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 20,
      "x": -1.559,
      "y": 0.177,
      "angle": 340.0,
      "layer": 1,
      "closed_by": [
        18,
        21
      ]
    },
    {
      "id": 21,
      "x": -1.559,
      "y": -0.367,
      "angle": 340.0,
      "layer": 2,
      "closed_by": [
        49
      ]
    },
    {
      "id": 22,
      "x": 1.557,
      "y": -0.282,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 23,
      "x": 1.559,
      "y": 0.18,
      "angle": 20.0,
      "layer": 1,
      "closed_by": [
        19,
        22
      ]
    },
    {
      "id": 25,
      "x": 0.148,
      "y": 0.859,
      "layer": 1,
      "closed_by": [
        27
      ]
    },
    {
      "id": 26,
      "x": 0.625,
      "y": 0.064,
      "layer": 4,
      "closed_by": [
        34,
        39,
        41
      ]
    },
    {
      "id": 27,
      "x": -0.004,
      "y": 0.261,
      "angle": 340.0,
      "layer": 2,
      "closed_by": [
        28,
        30
      ]
    },
    {
      "id": 28,
      "x": -0.361,
      "y": 0.391,
      "angle": 340.0,
      "layer": 3,
      "closed_by": [
        29,
        33
      ]
    },
    {
      "id": 29,
      "x": -0.625,
      "y": 0.458,
      "layer": 4,
      "closed_by": [
        35,
        40,
        42
      ]
    },
    {
      "id": 30,
      "x": 0.356,
      "y": 0.126,
      "angle": 340.0,
      "layer": 3,
      "closed_by": [
        26,
        32
      ]
    },
    {
      "id": 31,
      "x": -0.143,
      "y": -0.335,
      "layer": 1,
      "closed_by": [
        27
      ]
    },
    {
      "id": 32,
      "x": 0.067,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 33,
      "x": -0.064,
      "y": 0.856,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 34,
      "x": 0.386,
      "y": -0.172,
      "layer": 5,
      "closed_by": [
        43
      ]
    },
    {
      "id": 35,
      "x": -0.384,
      "y": 0.702,
      "layer": 5,
      "closed_by": [
        44
      ]
    },
    {
      "id": 36,
      "x": 0.703,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 37,
      "x": -0.703,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 38,
      "x": 1.269,
      "y": 0.939,
      "layer": 5,
      "closed_by": [
        45
      ]
    },
    {
      "id": 39,
      "x": 0.708,
      "y": 0.694,
      "layer": 5,
      "closed_by": [
        45
      ]
    },
    {
      "id": 40,
      "x": -0.703,
      "y": -0.178,
      "layer": 5,
      "closed_by": [
        46
      ]
    },
    {
      "id": 41,
      "x": 0.947,
      "y": -0.172,
      "layer": 5,
      "closed_by": [
        43,
        45
      ]
    },
    {
      "id": 42,
      "x": -0.944,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        44,
        46
      ]
    },
    {
      "id": 43,
      "x": 0.546,
      "y": -0.333,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 44,
      "x": -0.544,
      "y": 0.859,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 45,
      "x": 0.947,
      "y": 0.537,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 46,
      "x": -0.944,
      "y": -0.025,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 47,
      "x": -1.268,
      "y": -0.418,
      "layer": 5,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": 1.266,
      "y": 0.303,
      "layer": 4,
      "closed_by": [
        38,
        41
      ]
    },
    {
      "id": 49,
      "x": -1.266,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        42,
        47
      ]
    },
    {
      "id": 50,
      "x": 1.266,
      "y": -0.333,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 51,
      "x": -1.266,
      "y": 0.861,
      "layer": 3,
      "closed_by": [
        49
      ]
    }
  ],
  "cards_in_layout_amount": 33,
  "cards_in_deck_amount": 30,
  "layers_in_level": 6,
  "open_cards": 4,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 50.6,
    "star2": 48.0,
    "star3": 1.1
  }
}