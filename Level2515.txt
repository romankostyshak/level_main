{
  "layout_cards": [
    {
      "id": 5,
      "x": -2.308,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        9,
        17
      ]
    },
    {
      "id": 8,
      "x": 2.305,
      "y": 0.231,
      "layer": 2,
      "closed_by": [
        10,
        11
      ]
    },
    {
      "id": 9,
      "x": -2.226,
      "y": 0.638,
      "layer": 3,
      "closed_by": [
        12
      ]
    },
    {
      "id": 10,
      "x": 2.229,
      "y": -0.187,
      "layer": 3,
      "closed_by": [
        13
      ]
    },
    {
      "id": 11,
      "x": 2.226,
      "y": 0.634,
      "layer": 3,
      "closed_by": [
        14
      ]
    },
    {
      "id": 12,
      "x": -2.065,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        18
      ]
    },
    {
      "id": 13,
      "x": 2.069,
      "y": -0.259,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 14,
      "x": 2.069,
      "y": 0.703,
      "layer": 4,
      "closed_by": [
        19
      ]
    },
    {
      "id": 15,
      "x": -1.906,
      "y": -0.187,
      "layer": 5,
      "closed_by": [
        46
      ]
    },
    {
      "id": 16,
      "x": -2.065,
      "y": -0.254,
      "layer": 4,
      "closed_by": [
        15
      ]
    },
    {
      "id": 17,
      "x": -2.226,
      "y": -0.187,
      "layer": 3,
      "closed_by": [
        16
      ]
    },
    {
      "id": 18,
      "x": -1.906,
      "y": 0.634,
      "layer": 5,
      "closed_by": [
        46
      ]
    },
    {
      "id": 19,
      "x": 1.906,
      "y": 0.633,
      "layer": 5,
      "closed_by": [
        45
      ]
    },
    {
      "id": 20,
      "x": 1.906,
      "y": -0.187,
      "layer": 5,
      "closed_by": [
        45
      ]
    },
    {
      "id": 22,
      "x": -0.381,
      "y": -0.259,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 23,
      "x": 0.001,
      "y": -0.416,
      "layer": 1,
      "closed_by": [
        24,
        25
      ]
    },
    {
      "id": 24,
      "x": 0.386,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        26
      ]
    },
    {
      "id": 25,
      "x": -0.384,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        27
      ]
    },
    {
      "id": 26,
      "x": 0.791,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        28,
        40
      ]
    },
    {
      "id": 27,
      "x": -0.786,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        22,
        41
      ]
    },
    {
      "id": 28,
      "x": 0.386,
      "y": -0.256,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 29,
      "x": 0.003,
      "y": -0.259,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -0.001,
      "y": 0.861,
      "layer": 1,
      "closed_by": [
        31,
        32
      ]
    },
    {
      "id": 31,
      "x": 0.386,
      "y": 0.781,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 32,
      "x": -0.386,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 33,
      "x": 0.786,
      "y": 0.703,
      "layer": 3,
      "closed_by": [
        38,
        40
      ]
    },
    {
      "id": 34,
      "x": -0.785,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        37,
        41
      ]
    },
    {
      "id": 35,
      "x": -1.264,
      "y": 0.223,
      "layer": 1,
      "closed_by": [
        47,
        48
      ]
    },
    {
      "id": 36,
      "x": -1.343,
      "y": -0.18,
      "layer": 5,
      "closed_by": [
        46
      ]
    },
    {
      "id": 37,
      "x": -0.381,
      "y": 0.697,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 38,
      "x": 0.386,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 39,
      "x": -0.003,
      "y": 0.697,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 0.948,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        42,
        43
      ]
    },
    {
      "id": 41,
      "x": -0.944,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        36,
        44
      ]
    },
    {
      "id": 42,
      "x": 1.345,
      "y": 0.628,
      "layer": 5,
      "closed_by": [
        45
      ]
    },
    {
      "id": 43,
      "x": 1.343,
      "y": -0.18,
      "layer": 5,
      "closed_by": [
        45
      ]
    },
    {
      "id": 44,
      "x": -1.343,
      "y": 0.629,
      "layer": 5,
      "closed_by": [
        46
      ]
    },
    {
      "id": 45,
      "x": 1.506,
      "y": 0.216,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 46,
      "x": -1.503,
      "y": 0.223,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 47,
      "x": -1.105,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 48,
      "x": -1.105,
      "y": -0.256,
      "layer": 2,
      "closed_by": [
        27
      ]
    },
    {
      "id": 49,
      "x": 1.103,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 50,
      "x": 1.108,
      "y": -0.256,
      "layer": 2,
      "closed_by": [
        26
      ]
    },
    {
      "id": 51,
      "x": 1.269,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        49,
        50
      ]
    }
  ],
  "cards_in_layout_amount": 44,
  "cards_in_deck_amount": 34,
  "stars_statistic": {
    "star1": 2.3,
    "star2": 41.9,
    "star3": 55.2
  }
}