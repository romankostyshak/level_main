{
  "layout_cards": [
    {
      "id": 14,
      "x": -0.898,
      "y": 0.238,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 15,
      "x": 0.898,
      "y": 0.238,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 16,
      "y": 0.238,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 19,
      "x": -1.159,
      "y": 0.699,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        14
      ]
    },
    {
      "id": 20,
      "x": -1.159,
      "y": -0.259,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        14
      ]
    },
    {
      "id": 21,
      "x": 1.159,
      "y": 0.699,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        15
      ]
    },
    {
      "id": 22,
      "x": -1.939,
      "y": -0.259,
      "angle": 300.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 23,
      "x": -1.939,
      "y": 0.699,
      "angle": 60.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 1.939,
      "y": 0.699,
      "angle": 300.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 1.939,
      "y": -0.259,
      "angle": 60.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -1.939,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 27,
      "x": -1.939,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 28,
      "x": -1.378,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        20,
        22
      ]
    },
    {
      "id": 29,
      "x": 1.939,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 30,
      "x": 1.939,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 31,
      "x": 1.378,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        25,
        37
      ]
    },
    {
      "id": 32,
      "x": 1.378,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        21,
        24
      ]
    },
    {
      "id": 33,
      "x": -2.2,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        26,
        27
      ]
    },
    {
      "id": 34,
      "x": 2.2,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        29,
        30
      ]
    },
    {
      "id": 35,
      "x": -1.659,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        26,
        27,
        28,
        36
      ]
    },
    {
      "id": 36,
      "x": -1.378,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        19,
        23
      ]
    },
    {
      "id": 37,
      "x": 1.159,
      "y": -0.259,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        15
      ]
    },
    {
      "id": 38,
      "x": 1.659,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        29,
        30,
        31,
        32
      ]
    },
    {
      "id": 39,
      "x": -1.12,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        28,
        36,
        42,
        43
      ]
    },
    {
      "id": 40,
      "x": 1.12,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        31,
        32,
        41,
        44
      ]
    },
    {
      "id": 41,
      "x": 0.578,
      "y": 0.638,
      "angle": 300.0,
      "layer": 2,
      "closed_by": [
        16,
        21
      ]
    },
    {
      "id": 42,
      "x": -0.578,
      "y": 0.638,
      "angle": 60.0,
      "layer": 2,
      "closed_by": [
        16,
        19
      ]
    },
    {
      "id": 43,
      "x": -0.578,
      "y": -0.199,
      "angle": 300.0,
      "layer": 2,
      "closed_by": [
        16,
        20
      ]
    },
    {
      "id": 44,
      "x": 0.578,
      "y": -0.199,
      "angle": 60.0,
      "layer": 2,
      "closed_by": [
        16,
        37
      ]
    },
    {
      "id": 45,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        43,
        44
      ]
    },
    {
      "id": 46,
      "x": -0.578,
      "y": 0.638,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 47,
      "x": -0.578,
      "y": -0.199,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 48,
      "x": 0.578,
      "y": 0.638,
      "layer": 1,
      "closed_by": [
        41
      ]
    },
    {
      "id": 49,
      "x": 0.578,
      "y": -0.199,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 50,
      "y": 1.059,
      "layer": 1,
      "closed_by": [
        41,
        42
      ]
    }
  ],
  "cards_in_layout_amount": 35,
  "cards_in_deck_amount": 26,
  "layers_in_level": 4,
  "open_cards": 7,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 13.9,
    "star2": 74.7,
    "star3": 11.0
  }
}