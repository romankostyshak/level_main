{
  "layout_cards": [
    {
      "id": 21,
      "x": 1.139,
      "y": -0.017,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 22,
      "x": -2.059,
      "y": -0.418,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 2.059,
      "y": 1.019,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 24,
      "x": -1.478,
      "y": -0.439,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 1.48,
      "y": 1.039,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 26,
      "x": 0.039,
      "y": 1.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 27,
      "x": -0.039,
      "y": -0.398,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 2.118,
      "y": 0.079,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -2.059,
      "y": 0.46,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 30,
      "x": 2.059,
      "y": 0.136,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 31,
      "x": -1.978,
      "y": 0.379,
      "layer": 1,
      "closed_by": [
        29,
        33
      ]
    },
    {
      "id": 32,
      "x": 1.98,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        30,
        34
      ]
    },
    {
      "id": 33,
      "x": -1.978,
      "y": -0.358,
      "layer": 2,
      "closed_by": [
        22,
        35
      ]
    },
    {
      "id": 34,
      "x": 1.98,
      "y": 0.958,
      "layer": 2,
      "closed_by": [
        23,
        38
      ]
    },
    {
      "id": 35,
      "x": -1.539,
      "y": -0.358,
      "layer": 3,
      "closed_by": [
        24,
        39
      ]
    },
    {
      "id": 36,
      "x": -2.118,
      "y": 0.518,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 1.539,
      "y": 0.958,
      "layer": 3,
      "closed_by": [
        25,
        40
      ]
    },
    {
      "id": 39,
      "x": -1.539,
      "y": 0.379,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 1.539,
      "y": 0.216,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 41,
      "x": -1.139,
      "y": 0.46,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 42,
      "x": 1.139,
      "y": 0.136,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 43,
      "x": 0.74,
      "y": 0.059,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 44,
      "x": -0.74,
      "y": 0.537,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 45,
      "x": 0.017,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 46,
      "x": -0.18,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 47,
      "x": -0.337,
      "y": 0.62,
      "layer": 1,
      "closed_by": [
        44,
        46
      ]
    },
    {
      "id": 48,
      "x": 0.18,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 49,
      "x": 0.337,
      "y": -0.017,
      "layer": 1,
      "closed_by": [
        43,
        48
      ]
    },
    {
      "id": 50,
      "x": -0.017,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 51,
      "x": -1.139,
      "y": 0.62,
      "layer": 1,
      "closed_by": [
        44
      ]
    }
  ],
  "cards_in_layout_amount": 30,
  "cards_in_deck_amount": 26,
  "layers_in_level": 4,
  "open_cards": 10,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 19.2,
    "star2": 69.2,
    "star3": 11.1
  }
}