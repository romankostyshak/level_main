{
  "layout_cards": [
    {
      "id": 49,
      "y": 0.773,
      "layer": 1,
      "closed_by": [
        45,
        50
      ]
    },
    {
      "id": 48,
      "x": 0.331,
      "y": -0.017,
      "layer": 1,
      "closed_by": [
        50,
        46
      ]
    },
    {
      "id": 47,
      "x": -0.328,
      "y": -0.023,
      "layer": 1,
      "closed_by": [
        45,
        46
      ]
    },
    {
      "id": 45,
      "x": -0.328,
      "y": 0.54,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 50,
      "x": 0.333,
      "y": 0.537,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 46,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 44,
      "x": 0.46,
      "y": 0.985,
      "angle": 45.0,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 40,
      "x": 0.652,
      "y": -0.177,
      "layer": 4,
      "closed_by": [
        34,
        32
      ]
    },
    {
      "id": 38,
      "y": -0.416,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 35,
      "x": -0.001,
      "y": -0.495,
      "layer": 4,
      "closed_by": [
        34,
        33
      ]
    },
    {
      "id": 39,
      "x": -0.638,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        33,
        31
      ]
    },
    {
      "id": 34,
      "x": 0.307,
      "y": -0.414,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -0.305,
      "y": -0.414,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 0.86,
      "y": -0.418,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -0.865,
      "y": -0.414,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 43,
      "x": -0.449,
      "y": 0.98,
      "angle": 315.0,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 42,
      "x": 0.652,
      "y": 0.537,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 41,
      "x": -0.638,
      "y": 0.537,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 30,
      "x": 0.865,
      "y": 1.019,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -0.865,
      "y": 1.016,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 20,
  "cards_in_deck_amount": 22,
  "stars_statistic": {
    "star1": 24.3,
    "star2": 0.9,
    "star3": 74.4
  }
}