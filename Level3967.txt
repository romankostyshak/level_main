{
  "layout_cards": [
    {
      "id": 11,
      "x": -1.939,
      "y": -0.1,
      "layer": 2,
      "closed_by": [
        46,
        48
      ]
    },
    {
      "id": 12,
      "x": 0.337,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        14,
        39
      ]
    },
    {
      "id": 13,
      "x": 0.898,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 14,
      "x": 0.337,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 15,
      "x": -0.337,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 16,
      "x": -0.898,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 17,
      "x": -0.337,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        15,
        39
      ]
    },
    {
      "id": 18,
      "x": 0.337,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        12,
        14,
        39
      ]
    },
    {
      "id": 19,
      "x": -0.337,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        15,
        17,
        39
      ]
    },
    {
      "id": 20,
      "x": 1.179,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        13,
        23,
        24
      ]
    },
    {
      "id": 21,
      "x": -1.179,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        16,
        28,
        29
      ]
    },
    {
      "id": 22,
      "x": 1.2,
      "y": 0.36,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        13,
        20,
        23,
        24
      ]
    },
    {
      "id": 23,
      "x": 1.059,
      "y": 0.6,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        13,
        24
      ]
    },
    {
      "id": 24,
      "x": 0.958,
      "y": 0.819,
      "angle": 10.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 2.059,
      "y": 0.799,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 26,
      "x": -0.537,
      "y": -0.418,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 27,
      "x": -1.2,
      "y": 0.36,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        16,
        21,
        28,
        29
      ]
    },
    {
      "id": 28,
      "x": -1.059,
      "y": 0.6,
      "angle": 340.0,
      "layer": 3,
      "closed_by": [
        16,
        29
      ]
    },
    {
      "id": 29,
      "x": -0.958,
      "y": 0.819,
      "angle": 349.997,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 0.559,
      "y": -0.217,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        32,
        34,
        35
      ]
    },
    {
      "id": 31,
      "x": -0.56,
      "y": -0.217,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        26,
        33,
        36
      ]
    },
    {
      "id": 32,
      "x": 0.559,
      "y": -0.319,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        34,
        35
      ]
    },
    {
      "id": 33,
      "x": -0.56,
      "y": -0.319,
      "angle": 340.0,
      "layer": 2,
      "closed_by": [
        26,
        36
      ]
    },
    {
      "id": 34,
      "x": 0.537,
      "y": -0.418,
      "angle": 9.998,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 35,
      "x": 0.5,
      "y": -0.518,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -0.499,
      "y": -0.518,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 37,
      "x": -1.58,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        21,
        40,
        42
      ]
    },
    {
      "id": 38,
      "x": 1.577,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        20,
        25,
        41
      ]
    },
    {
      "id": 39,
      "y": 0.939,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 40,
      "x": -1.758,
      "y": 0.759,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 41,
      "x": 1.758,
      "y": 0.759,
      "angle": 10.0,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 42,
      "x": -2.059,
      "y": 0.799,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 43,
      "x": 2.4,
      "y": 0.759,
      "angle": 349.997,
      "layer": 1,
      "closed_by": [
        25,
        45
      ]
    },
    {
      "id": 44,
      "x": -2.398,
      "y": 0.759,
      "angle": 9.998,
      "layer": 1,
      "closed_by": [
        42,
        46
      ]
    },
    {
      "id": 45,
      "x": 2.279,
      "y": 0.1,
      "angle": 349.997,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 46,
      "x": -2.279,
      "y": 0.1,
      "angle": 9.998,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 47,
      "x": 2.059,
      "y": -0.1,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 48,
      "x": -2.059,
      "y": -0.1,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 49,
      "x": 1.939,
      "y": -0.1,
      "layer": 2,
      "closed_by": [
        45,
        47
      ]
    },
    {
      "id": 50,
      "x": 1.86,
      "y": -0.136,
      "angle": 349.997,
      "layer": 1,
      "closed_by": [
        45,
        47,
        49
      ]
    },
    {
      "id": 51,
      "x": -1.858,
      "y": -0.136,
      "angle": 9.998,
      "layer": 1,
      "closed_by": [
        11,
        46,
        48
      ]
    }
  ],
  "cards_in_layout_amount": 41,
  "cards_in_deck_amount": 20,
  "layers_in_level": 5,
  "open_cards": 7,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 0.0,
    "star2": 38.1,
    "star3": 61.0
  }
}