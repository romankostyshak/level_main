{
  "layout_cards": [
    {
      "id": 51,
      "x": -1.771,
      "y": 0.158,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 50,
      "x": 0.261,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 49,
      "x": 0.261,
      "y": -0.3,
      "layer": 2,
      "closed_by": [
        44,
        37
      ]
    },
    {
      "id": 48,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        49,
        45
      ]
    },
    {
      "id": 47,
      "x": -0.268,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 46,
      "x": -0.268,
      "y": 0.425,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 45,
      "x": -0.268,
      "y": -0.3,
      "layer": 2,
      "closed_by": [
        46,
        37
      ]
    },
    {
      "id": 44,
      "x": 0.261,
      "y": 0.425,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 43,
      "x": -1.49,
      "y": -0.573,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -1.212,
      "y": 0.158,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 41,
      "x": 1.498,
      "y": -0.572,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 40,
      "x": -0.001,
      "y": 0.266,
      "layer": 1,
      "closed_by": [
        50,
        49,
        47,
        45
      ]
    },
    {
      "id": 36,
      "x": -1.491,
      "y": 0.15,
      "layer": 1,
      "closed_by": [
        35,
        34,
        25,
        23
      ]
    },
    {
      "id": 35,
      "x": -1.771,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        51
      ]
    },
    {
      "id": 34,
      "x": -1.212,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 33,
      "x": 1.213,
      "y": 0.158,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 32,
      "x": 1.213,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 31,
      "x": 1.773,
      "y": 0.158,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 30,
      "x": 1.773,
      "y": 0.879,
      "layer": 2,
      "closed_by": [
        31,
        39
      ]
    },
    {
      "id": 29,
      "x": 1.773,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 28,
      "x": 1.493,
      "y": 0.136,
      "layer": 1,
      "closed_by": [
        32,
        30,
        29,
        27
      ]
    },
    {
      "id": 27,
      "x": 1.213,
      "y": 0.879,
      "layer": 2,
      "closed_by": [
        33,
        39
      ]
    },
    {
      "id": 26,
      "x": 1.493,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        30,
        27
      ]
    },
    {
      "id": 25,
      "x": -1.212,
      "y": 0.879,
      "layer": 2,
      "closed_by": [
        42,
        38
      ]
    },
    {
      "id": 24,
      "x": -1.491,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        25,
        23
      ]
    },
    {
      "id": 23,
      "x": -1.771,
      "y": 0.879,
      "layer": 2,
      "closed_by": [
        51,
        38
      ]
    },
    {
      "id": 22,
      "x": -0.004,
      "y": 1.019,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -1.495,
      "y": 1.019,
      "layer": 3,
      "closed_by": [],
      "effect_id": 5
    },
    {
      "id": 39,
      "x": 1.493,
      "y": 1.019,
      "layer": 3,
      "closed_by": [],
      "effect_id": 5
    },
    {
      "id": 37,
      "x": 0.003,
      "y": -0.578,
      "layer": 3,
      "closed_by": [],
      "effect_id": 5
    }
  ],
  "cards_in_layout_amount": 30,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 45.7,
    "star2": 43.2,
    "star3": 10.5
  }
}