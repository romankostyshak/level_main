{
  "layout_cards": [
    {
      "id": 18,
      "x": -1.664,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        47,
        49
      ]
    },
    {
      "id": 19,
      "x": 1.666,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        46,
        48
      ]
    },
    {
      "id": 21,
      "x": 1.054,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        25,
        46
      ]
    },
    {
      "id": 22,
      "x": 0.652,
      "y": -0.405,
      "angle": 15.0,
      "layer": 1,
      "closed_by": [
        23,
        40
      ]
    },
    {
      "id": 23,
      "x": 0.93,
      "y": -0.289,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        24,
        34
      ]
    },
    {
      "id": 24,
      "x": 1.177,
      "y": -0.103,
      "angle": 45.0,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 25,
      "x": 1.37,
      "y": 0.143,
      "angle": 60.0,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 26,
      "x": 0.893,
      "y": 0.619,
      "layer": 2,
      "closed_by": [
        21,
        24
      ]
    },
    {
      "id": 27,
      "x": 0.734,
      "y": 0.458,
      "layer": 1,
      "closed_by": [
        23,
        26,
        43
      ]
    },
    {
      "id": 28,
      "x": 1.508,
      "y": 0.537,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -0.648,
      "y": -0.405,
      "angle": 344.997,
      "layer": 1,
      "closed_by": [
        30,
        39
      ]
    },
    {
      "id": 30,
      "x": -0.919,
      "y": -0.293,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        31,
        51
      ]
    },
    {
      "id": 31,
      "x": -1.151,
      "y": -0.119,
      "angle": 315.0,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 32,
      "x": -1.35,
      "y": 0.128,
      "angle": 300.0,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 33,
      "x": -1.049,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        32,
        47
      ]
    },
    {
      "id": 34,
      "x": 0.625,
      "y": -0.481,
      "angle": 15.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -0.001,
      "y": 0.781,
      "layer": 3,
      "closed_by": [
        44,
        45
      ]
    },
    {
      "id": 36,
      "x": -0.888,
      "y": 0.619,
      "layer": 2,
      "closed_by": [
        31,
        33
      ]
    },
    {
      "id": 37,
      "x": -0.731,
      "y": 0.46,
      "layer": 1,
      "closed_by": [
        30,
        36,
        42
      ]
    },
    {
      "id": 38,
      "x": -1.427,
      "y": 0.54,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -0.316,
      "y": -0.474,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        50,
        51
      ]
    },
    {
      "id": 40,
      "x": 0.319,
      "y": -0.476,
      "angle": 9.998,
      "layer": 2,
      "closed_by": [
        34,
        50
      ]
    },
    {
      "id": 41,
      "y": -0.521,
      "layer": 1,
      "closed_by": [
        39,
        40
      ]
    },
    {
      "id": 42,
      "x": -0.33,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 43,
      "x": 0.331,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 44,
      "x": -0.328,
      "y": 0.782,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 45,
      "x": 0.335,
      "y": 0.777,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 46,
      "x": 1.213,
      "y": 1.021,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 47,
      "x": -1.213,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 48,
      "x": 1.746,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 49,
      "x": -1.745,
      "y": 1.021,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 50,
      "x": -0.002,
      "y": -0.363,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -0.592,
      "y": -0.481,
      "angle": 344.997,
      "layer": 3,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 33,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 48.2,
    "star2": 44.0,
    "star3": 7.2
  }
}