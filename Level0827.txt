{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.002,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        47,
        44
      ]
    },
    {
      "id": 50,
      "x": 0.333,
      "y": -0.092,
      "layer": 1,
      "closed_by": [
        48,
        44
      ]
    },
    {
      "id": 49,
      "x": -0.333,
      "y": -0.096,
      "layer": 1,
      "closed_by": [
        48,
        47
      ]
    },
    {
      "id": 48,
      "x": 0.001,
      "y": -0.178,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 47,
      "x": -0.246,
      "y": 0.625,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 46,
      "y": -0.254,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 45,
      "y": -0.412,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 44,
      "x": 0.263,
      "y": 0.628,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 43,
      "x": 0.395,
      "y": 0.629,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 42,
      "x": -0.381,
      "y": 0.625,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 41,
      "x": 0.476,
      "y": 0.629,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 40,
      "x": -0.462,
      "y": 0.625,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 39,
      "x": 0.55,
      "y": 0.629,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -0.546,
      "y": 0.628,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 37,
      "x": 1.373,
      "y": 0.143,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -1.373,
      "y": 0.142,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 1.588,
      "y": -0.017,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 34,
      "x": -1.585,
      "y": -0.017,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 33,
      "x": 1.827,
      "y": -0.256,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 32,
      "x": -1.824,
      "y": -0.261,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 31,
      "x": 2.173,
      "y": -0.017,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 30,
      "x": -2.167,
      "y": -0.017,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 29,
      "x": 2.413,
      "y": 0.143,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 28,
      "x": -2.414,
      "y": 0.143,
      "layer": 1,
      "closed_by": [
        30
      ]
    },
    {
      "id": 27,
      "y": -0.335,
      "layer": 4,
      "closed_by": [
        45
      ]
    }
  ],
  "cards_in_layout_amount": 25,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 55.2,
    "star2": 15.2,
    "star3": 29.2
  }
}