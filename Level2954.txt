{
  "layout_cards": [
    {
      "id": 19,
      "x": -0.638,
      "y": 0.699,
      "layer": 4,
      "closed_by": [],
      "card_type": 2
    },
    {
      "id": 20,
      "x": -0.642,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        21,
        22
      ]
    },
    {
      "id": 21,
      "x": -0.333,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 22,
      "x": -0.947,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 23,
      "x": -0.333,
      "y": 0.786,
      "layer": 3,
      "closed_by": [
        19
      ]
    },
    {
      "id": 24,
      "x": -0.947,
      "y": 0.785,
      "layer": 3,
      "closed_by": [
        19
      ]
    },
    {
      "id": 25,
      "x": 0.634,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        26,
        27
      ],
      "card_type": 3
    },
    {
      "id": 26,
      "x": 0.331,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 27,
      "x": 0.947,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 28,
      "x": 0.331,
      "y": 0.785,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 29,
      "x": 0.944,
      "y": 0.782,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 30,
      "x": 0.638,
      "y": 0.699,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 0.652,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        35,
        37
      ]
    },
    {
      "id": 32,
      "x": 1.906,
      "y": -0.414,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 33,
      "x": -1.347,
      "y": -0.259,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 34,
      "x": -0.651,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        36,
        38
      ]
    },
    {
      "id": 35,
      "x": 0.333,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 36,
      "x": -0.333,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 37,
      "x": 0.944,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 38,
      "x": -0.944,
      "y": -0.256,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 39,
      "x": 1.187,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 40,
      "x": -1.187,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 41,
      "x": 1.345,
      "y": -0.259,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 42,
      "x": 1.667,
      "y": -0.259,
      "layer": 5,
      "closed_by": [
        47
      ]
    },
    {
      "id": 43,
      "x": -1.664,
      "y": -0.256,
      "layer": 5,
      "closed_by": [
        48
      ]
    },
    {
      "id": 44,
      "x": 0.004,
      "y": -0.333,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 45,
      "x": 0.006,
      "y": -0.414,
      "layer": 4,
      "closed_by": [
        46
      ]
    },
    {
      "id": 46,
      "x": 0.004,
      "y": -0.499,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 47,
      "x": 1.901,
      "y": -0.259,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 48,
      "x": -1.904,
      "y": -0.256,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 49,
      "x": -1.904,
      "y": -0.414,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 50,
      "x": 1.901,
      "y": -0.573,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 51,
      "x": -1.904,
      "y": -0.573,
      "layer": 3,
      "closed_by": [
        49
      ]
    }
  ],
  "cards_in_layout_amount": 33,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 39.8,
    "star2": 47.1,
    "star3": 12.4
  }
}