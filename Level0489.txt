{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.518,
      "y": 1.031,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 50,
      "y": 1.031,
      "layer": 1,
      "closed_by": [
        49,
        48
      ]
    },
    {
      "id": 49,
      "x": 0.263,
      "y": 1.031,
      "layer": 2,
      "closed_by": [
        51
      ]
    },
    {
      "id": 48,
      "x": -0.259,
      "y": 1.031,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "x": -0.518,
      "y": 1.031,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 45,
      "x": -0.303,
      "y": -0.199,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 43,
      "y": -0.587,
      "layer": 3,
      "closed_by": [
        45,
        33
      ]
    },
    {
      "id": 42,
      "x": -1.613,
      "y": -0.375,
      "layer": 1,
      "closed_by": [
        41
      ]
    },
    {
      "id": 41,
      "x": -1.613,
      "y": 0.032,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 40,
      "x": -1.613,
      "y": 0.442,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 39,
      "x": -1.613,
      "y": 0.86,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 1.531,
      "y": 0.86,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 37,
      "x": 1.531,
      "y": 0.016,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 36,
      "x": 1.531,
      "y": 0.435,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 35,
      "x": 1.531,
      "y": -0.381,
      "layer": 1,
      "closed_by": [
        37
      ]
    },
    {
      "id": 33,
      "x": 0.307,
      "y": -0.199,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -0.263,
      "y": -0.199,
      "layer": 2,
      "closed_by": [
        43,
        34
      ]
    },
    {
      "id": 44,
      "x": 0.518,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 46,
      "x": 0.268,
      "y": -0.199,
      "layer": 2,
      "closed_by": [
        43,
        44
      ]
    },
    {
      "id": 31,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        32,
        46
      ]
    },
    {
      "id": 34,
      "x": -0.518,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        45
      ]
    }
  ],
  "cards_in_layout_amount": 21,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 45.6,
    "star2": 1.5,
    "star3": 52.4
  }
}