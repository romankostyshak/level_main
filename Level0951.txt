{
  "layout_cards": [
    {
      "id": 45,
      "x": 0.386,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 49,
      "x": 0.003,
      "y": -0.5,
      "layer": 1,
      "closed_by": [
        45,
        50
      ]
    },
    {
      "id": 50,
      "x": -0.386,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 47,
      "x": 0.386,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 48,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        47,
        46
      ]
    },
    {
      "id": 46,
      "x": -0.386,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 44,
      "x": 0.787,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 43,
      "x": -0.703,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 42,
      "x": 0.786,
      "y": -0.5,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 41,
      "x": -0.703,
      "y": -0.5,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 40,
      "x": 1.141,
      "y": 1.003,
      "angle": 355.0,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 39,
      "x": -1.072,
      "y": 1.008,
      "angle": 5.0,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 38,
      "x": 1.171,
      "y": -0.486,
      "angle": 5.0,
      "layer": 4,
      "closed_by": [
        32
      ]
    },
    {
      "id": 35,
      "x": -1.059,
      "y": -0.488,
      "angle": 355.0,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 34,
      "x": 1.427,
      "y": 0.938,
      "angle": 340.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -1.36,
      "y": 0.939,
      "angle": 25.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 1.429,
      "y": -0.421,
      "angle": 25.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -1.355,
      "y": -0.423,
      "angle": 335.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 1.911,
      "y": 0.748,
      "angle": 340.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 29,
      "x": 1.906,
      "y": -0.202,
      "angle": 25.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -1.825,
      "y": -0.202,
      "angle": 335.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 28,
      "x": -1.832,
      "y": 0.73,
      "angle": 25.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 2.065,
      "y": 0.79,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 26,
      "x": 2.069,
      "y": -0.256,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 25,
      "x": -2.012,
      "y": 0.781,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 24,
      "x": -1.993,
      "y": -0.252,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 23,
      "x": 2.148,
      "y": 0.703,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 22,
      "x": 2.148,
      "y": -0.179,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 21,
      "x": -2.065,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 37,
      "x": -2.065,
      "y": -0.18,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 20,
      "x": 2.226,
      "y": 0.266,
      "layer": 2,
      "closed_by": [
        23,
        22
      ]
    },
    {
      "id": 19,
      "x": -2.144,
      "y": 0.263,
      "layer": 2,
      "closed_by": [
        21,
        37
      ]
    }
  ],
  "cards_in_layout_amount": 32,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 17.1,
    "star2": 73.3,
    "star3": 8.9
  }
}