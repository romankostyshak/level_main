{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.331,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 50,
      "x": 0.333,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 49,
      "x": 1.61,
      "y": -0.017,
      "layer": 1,
      "closed_by": [
        37
      ]
    },
    {
      "id": 48,
      "x": -1.61,
      "y": -0.017,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 47,
      "x": 1.911,
      "y": 0.623,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 46,
      "x": -1.906,
      "y": 0.62,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 45,
      "x": 1.608,
      "y": 0.861,
      "layer": 1,
      "closed_by": [
        37,
        27
      ]
    },
    {
      "id": 44,
      "x": 0.002,
      "y": -0.416,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 43,
      "x": 0.001,
      "y": 0.702,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 42,
      "y": 0.782,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 41,
      "x": 0.003,
      "y": -0.497,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 40,
      "x": 0.002,
      "y": -0.578,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 39,
      "y": 0.86,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -1.748,
      "y": 0.303,
      "layer": 2,
      "closed_by": [
        46,
        35
      ]
    },
    {
      "id": 37,
      "x": 1.746,
      "y": 0.3,
      "layer": 2,
      "closed_by": [
        47,
        34
      ]
    },
    {
      "id": 36,
      "x": 0.335,
      "y": 0.62,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 35,
      "x": -1.371,
      "y": -0.093,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 34,
      "x": 1.371,
      "y": -0.093,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 33,
      "x": 1.133,
      "y": -0.256,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 32,
      "x": -1.133,
      "y": -0.256,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 31,
      "x": 0.892,
      "y": -0.337,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -0.893,
      "y": -0.337,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -1.615,
      "y": 0.859,
      "layer": 1,
      "closed_by": [
        38,
        28
      ]
    },
    {
      "id": 28,
      "x": -1.212,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 27,
      "x": 1.213,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        26
      ]
    },
    {
      "id": 26,
      "x": 1.133,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 25,
      "x": -0.333,
      "y": 0.62,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 24,
      "x": -1.133,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 23,
      "x": 0.892,
      "y": 1.021,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -0.888,
      "y": 1.019,
      "layer": 4,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 30,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 31.8,
    "star2": 60.2,
    "star3": 7.2
  }
}