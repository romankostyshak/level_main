{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.289,
      "y": 0.075,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 50,
      "x": -0.268,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 49,
      "x": -0.001,
      "y": 0.939,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 48,
      "x": 0.275,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 47,
      "y": -0.256,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 46,
      "x": -0.284,
      "y": 0.075,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 45,
      "x": -0.582,
      "y": 0.186,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 44,
      "x": 0.577,
      "y": 0.187,
      "layer": 2,
      "closed_by": [
        51
      ]
    },
    {
      "id": 43,
      "x": -1.827,
      "y": -0.495,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -1.192,
      "y": -0.495,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 41,
      "x": -2.775,
      "y": 1.023,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 40,
      "x": 1.799,
      "y": -0.493,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 39,
      "x": 1.174,
      "y": -0.493,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -2.154,
      "y": 1.023,
      "layer": 1,
      "closed_by": [
        31,
        30
      ]
    },
    {
      "id": 37,
      "x": 2.125,
      "y": 1.018,
      "layer": 1,
      "closed_by": [
        29,
        28
      ]
    },
    {
      "id": 36,
      "x": 2.742,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        28
      ]
    },
    {
      "id": 35,
      "x": 1.503,
      "y": -0.054,
      "layer": 3,
      "closed_by": [
        40,
        39
      ]
    },
    {
      "id": 34,
      "x": 2.125,
      "y": -0.054,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 33,
      "x": -1.529,
      "y": -0.05,
      "layer": 3,
      "closed_by": [
        43,
        42
      ]
    },
    {
      "id": 32,
      "x": -2.154,
      "y": -0.05,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 31,
      "x": -2.456,
      "y": 0.467,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 30,
      "x": -1.827,
      "y": 0.467,
      "layer": 2,
      "closed_by": [
        33,
        32
      ]
    },
    {
      "id": 29,
      "x": 1.799,
      "y": 0.465,
      "layer": 2,
      "closed_by": [
        35,
        34
      ]
    },
    {
      "id": 28,
      "x": 2.43,
      "y": 0.465,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 27,
      "x": 0.902,
      "y": 0.62,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 26,
      "x": -0.925,
      "y": 0.62,
      "layer": 1,
      "closed_by": [
        45
      ]
    }
  ],
  "cards_in_layout_amount": 26,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 54.6,
    "star2": 27.4,
    "star3": 17.4
  }
}