{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.331,
      "y": -0.18,
      "layer": 1,
      "closed_by": [
        47,
        45
      ]
    },
    {
      "id": 50,
      "x": 0.333,
      "y": -0.18,
      "layer": 1,
      "closed_by": [
        47,
        46
      ]
    },
    {
      "id": 49,
      "x": 0.972,
      "y": -0.18,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": -0.972,
      "y": -0.18,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 47,
      "x": -0.004,
      "y": 0.143,
      "layer": 2,
      "closed_by": [
        44,
        43
      ]
    },
    {
      "id": 46,
      "x": 0.652,
      "y": 0.143,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 45,
      "x": -0.648,
      "y": 0.143,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 44,
      "x": 0.333,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 43,
      "x": -0.331,
      "y": 0.3,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 42,
      "x": 0.652,
      "y": 0.46,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 41,
      "x": -0.651,
      "y": 0.46,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 40,
      "x": 0.975,
      "y": 0.623,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -0.972,
      "y": 0.623,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 1.985,
      "y": 0.683,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 37,
      "x": 1.985,
      "y": 0.943,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 1.985,
      "y": 0.442,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 35,
      "x": 1.985,
      "y": 0.143,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 34,
      "x": -1.985,
      "y": 0.14,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 33,
      "x": -1.985,
      "y": 0.939,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -1.985,
      "y": 0.442,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 31,
      "x": -1.985,
      "y": 0.68,
      "layer": 4,
      "closed_by": [
        33
      ]
    }
  ],
  "cards_in_layout_amount": 21,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 37.4,
    "star2": 1.0,
    "star3": 61.0
  }
}