{
  "layout_cards": [
    {
      "id": 11,
      "x": -1.825,
      "y": -0.335,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 12,
      "x": -2.17,
      "y": 1.019,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 15,
      "x": 0.675,
      "y": 0.754,
      "angle": 45.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 16,
      "x": 0.414,
      "y": -0.377,
      "angle": 344.997,
      "layer": 1,
      "closed_by": [
        17
      ]
    },
    {
      "id": 17,
      "x": 0.537,
      "y": -0.361,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        18
      ]
    },
    {
      "id": 18,
      "x": 0.688,
      "y": -0.381,
      "angle": 315.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 19,
      "x": 0.412,
      "y": 0.748,
      "angle": 15.0,
      "layer": 1,
      "closed_by": [
        20
      ]
    },
    {
      "id": 20,
      "x": 0.536,
      "y": 0.735,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        15
      ]
    },
    {
      "id": 21,
      "x": 1.452,
      "y": 0.782,
      "layer": 1,
      "closed_by": [
        23
      ]
    },
    {
      "id": 22,
      "x": 1.452,
      "y": -0.416,
      "layer": 1,
      "closed_by": [
        24
      ]
    },
    {
      "id": 23,
      "x": 1.539,
      "y": 0.74,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 24,
      "x": 1.542,
      "y": -0.375,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        26
      ]
    },
    {
      "id": 25,
      "x": 1.679,
      "y": 0.736,
      "angle": 30.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 26,
      "x": 1.684,
      "y": -0.37,
      "angle": 330.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 2.331,
      "y": 0.749,
      "angle": 14.998,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 28,
      "x": -2.548,
      "y": 0.303,
      "layer": 1,
      "closed_by": [
        38,
        40,
        48
      ]
    },
    {
      "id": 29,
      "x": -0.949,
      "y": 0.939,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 2.338,
      "y": -0.379,
      "angle": 344.997,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 31,
      "x": 2.433,
      "y": 0.707,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 32,
      "x": 2.436,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 33,
      "x": 2.516,
      "y": 0.642,
      "angle": 344.997,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 2.516,
      "y": -0.275,
      "angle": 15.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -1.452,
      "y": 0.3,
      "layer": 1,
      "closed_by": [
        37,
        39,
        49
      ]
    },
    {
      "id": 36,
      "x": -2.0,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        48,
        49
      ]
    },
    {
      "id": 37,
      "x": -1.292,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 38,
      "x": -2.706,
      "y": 0.86,
      "layer": 2,
      "closed_by": [
        46,
        50
      ]
    },
    {
      "id": 39,
      "x": -1.292,
      "y": -0.18,
      "layer": 2,
      "closed_by": [
        45,
        51
      ]
    },
    {
      "id": 40,
      "x": -2.703,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 41,
      "x": -0.731,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 42,
      "x": -0.731,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 43,
      "x": -3.263,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 44,
      "x": -3.266,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 45,
      "x": -0.962,
      "y": -0.256,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 46,
      "x": -3.026,
      "y": 0.939,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 47,
      "x": -3.025,
      "y": -0.256,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 48,
      "x": -2.171,
      "y": 0.861,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 49,
      "x": -1.825,
      "y": -0.18,
      "layer": 2,
      "closed_by": [
        51
      ]
    },
    {
      "id": 50,
      "x": -2.411,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        12
      ],
      "card_type": 5
    },
    {
      "id": 51,
      "x": -1.588,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        11
      ],
      "card_type": 5
    }
  ],
  "level_mechanic": 1,
  "free_cards_from_stickers": [
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24,
    25,
    26,
    27,
    30,
    31,
    32,
    33,
    34
  ],
  "cards_in_layout_amount": 39,
  "cards_in_deck_amount": 22,
  "stars_statistic": {
    "star1": 0.4,
    "star2": 21.4,
    "star3": 77.7
  }
}