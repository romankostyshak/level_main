{
  "layout_cards": [
    {
      "id": 14,
      "x": 2.571,
      "y": 0.864,
      "layer": 2,
      "closed_by": [
        18
      ]
    },
    {
      "id": 15,
      "x": 1.603,
      "y": 0.861,
      "layer": 1,
      "closed_by": [
        16,
        17
      ]
    },
    {
      "id": 16,
      "x": 2.012,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        18
      ]
    },
    {
      "id": 17,
      "x": 1.182,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 18,
      "x": 2.411,
      "y": 1.024,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 19,
      "x": 0.785,
      "y": 1.021,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 20,
      "x": 0.623,
      "y": 0.865,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 21,
      "x": 2.733,
      "y": 0.703,
      "layer": 1,
      "closed_by": [
        14
      ]
    },
    {
      "id": 22,
      "x": 0.467,
      "y": 0.708,
      "layer": 1,
      "closed_by": [
        20
      ]
    },
    {
      "id": 23,
      "x": 0.943,
      "y": -0.418,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 2.733,
      "y": -0.256,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 25,
      "x": 0.469,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        30
      ]
    },
    {
      "id": 26,
      "x": -0.782,
      "y": 0.787,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 27,
      "x": -2.413,
      "y": 0.787,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 28,
      "x": 1.598,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        32,
        33
      ],
      "card_type": 8
    },
    {
      "id": 29,
      "x": -1.598,
      "y": 0.782,
      "layer": 1,
      "closed_by": [
        36
      ],
      "card_type": 6
    },
    {
      "id": 30,
      "x": 0.703,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 31,
      "x": 2.493,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 32,
      "x": 1.906,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        34,
        35
      ]
    },
    {
      "id": 33,
      "x": 1.294,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        23,
        34
      ]
    },
    {
      "id": 34,
      "x": 1.6,
      "y": -0.097,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 2.253,
      "y": -0.418,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -1.455,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 37,
      "x": -1.748,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 38,
      "x": -2.733,
      "y": 0.939,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -1.597,
      "y": 0.623,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 40,
      "x": -0.628,
      "y": 0.856,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 41,
      "x": -0.467,
      "y": 0.939,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -2.572,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 43,
      "x": -2.732,
      "y": -0.493,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": -2.575,
      "y": -0.335,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 45,
      "x": -2.413,
      "y": -0.18,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 46,
      "x": -2.016,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 47,
      "x": -1.595,
      "y": -0.34,
      "layer": 1,
      "closed_by": [
        46,
        48
      ]
    },
    {
      "id": 48,
      "x": -1.184,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        51
      ]
    },
    {
      "id": 49,
      "x": -0.628,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        51
      ]
    },
    {
      "id": 50,
      "x": -0.465,
      "y": -0.497,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 51,
      "x": -0.787,
      "y": -0.179,
      "layer": 3,
      "closed_by": []
    }
  ],
  "level_mechanic": 2,
  "free_cards_from_stickers": [
    26,
    27,
    29,
    36,
    37,
    38,
    39,
    40,
    41,
    42,
    43,
    44,
    45,
    46,
    47,
    48,
    49,
    50,
    51
  ],
  "cards_in_layout_amount": 38,
  "cards_in_deck_amount": 22,
  "stars_statistic": {
    "star1": 7.5,
    "star2": 79.6,
    "star3": 12.4
  }
}