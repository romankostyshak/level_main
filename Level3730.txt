{
  "layout_cards": [
    {
      "id": 18,
      "x": -1.998,
      "y": 0.98,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 19,
      "x": -1.998,
      "y": -0.238,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 20,
      "x": -0.958,
      "y": 0.98,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 21,
      "x": -0.958,
      "y": -0.238,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -1.799,
      "y": 0.92,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 23,
      "x": -0.759,
      "y": -0.179,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 24,
      "x": -0.759,
      "y": 0.92,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        20
      ]
    },
    {
      "id": 25,
      "x": -1.616,
      "y": 0.36,
      "layer": 2,
      "closed_by": [
        22,
        38
      ]
    },
    {
      "id": 26,
      "x": -1.097,
      "y": 0.36,
      "layer": 2,
      "closed_by": [
        23,
        24
      ]
    },
    {
      "id": 27,
      "x": -0.577,
      "y": 0.36,
      "layer": 2,
      "closed_by": [
        23,
        24
      ]
    },
    {
      "id": 28,
      "x": -1.536,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        25,
        26
      ]
    },
    {
      "id": 29,
      "x": -1.016,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        26,
        27
      ]
    },
    {
      "id": 30,
      "x": -1.016,
      "y": -0.1,
      "layer": 1,
      "closed_by": [
        26,
        27
      ]
    },
    {
      "id": 31,
      "x": -0.499,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        27
      ]
    },
    {
      "id": 32,
      "x": -0.499,
      "y": -0.1,
      "layer": 1,
      "closed_by": [
        27
      ]
    },
    {
      "id": 33,
      "x": 0.958,
      "y": 0.958,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 0.958,
      "y": -0.238,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 2.0,
      "y": -0.238,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 2.0,
      "y": 0.958,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 37,
      "x": -1.536,
      "y": -0.1,
      "layer": 1,
      "closed_by": [
        25,
        26
      ]
    },
    {
      "id": 38,
      "x": -1.799,
      "y": -0.179,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        19
      ]
    },
    {
      "id": 39,
      "x": 1.799,
      "y": 0.898,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 40,
      "x": 1.799,
      "y": -0.179,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 41,
      "x": 0.759,
      "y": 0.898,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 42,
      "x": 0.759,
      "y": -0.179,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 43,
      "x": 1.616,
      "y": 0.36,
      "layer": 2,
      "closed_by": [
        39,
        40
      ]
    },
    {
      "id": 44,
      "x": 1.536,
      "y": -0.1,
      "layer": 1,
      "closed_by": [
        43,
        51
      ]
    },
    {
      "id": 45,
      "x": 1.536,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        43,
        51
      ]
    },
    {
      "id": 46,
      "x": 0.577,
      "y": 0.36,
      "layer": 2,
      "closed_by": [
        41,
        42
      ]
    },
    {
      "id": 47,
      "x": 0.497,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": 1.016,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        46,
        51
      ]
    },
    {
      "id": 49,
      "x": 1.016,
      "y": -0.1,
      "layer": 1,
      "closed_by": [
        46,
        51
      ]
    },
    {
      "id": 50,
      "x": 0.497,
      "y": -0.1,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 51,
      "x": 1.097,
      "y": 0.36,
      "layer": 2,
      "closed_by": [
        41,
        42
      ]
    }
  ],
  "level_mechanic": 1,
  "free_cards_from_stickers": [
    33,
    34,
    35,
    36,
    39,
    40,
    41,
    42,
    43,
    44,
    45,
    46,
    47,
    48,
    49,
    50,
    51
  ],
  "cards_in_layout_amount": 34,
  "cards_in_deck_amount": 34,
  "stars_statistic": {
    "star1": 85.6,
    "star2": 14.2,
    "star3": 0.1
  }
}