{
  "layout_cards": [
    {
      "id": 50,
      "x": -1.904,
      "y": 0.781,
      "layer": 2,
      "closed_by": [
        49
      ]
    },
    {
      "id": 46,
      "x": 1.906,
      "y": 0.781,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 45,
      "x": 1.74,
      "y": -0.177,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 44,
      "x": 1.738,
      "y": 0.781,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 43,
      "x": 1.904,
      "y": -0.177,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 42,
      "x": -1.825,
      "y": 0.298,
      "layer": 4,
      "closed_by": [
        38,
        37
      ]
    },
    {
      "id": 38,
      "x": -1.825,
      "y": -0.179,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 37,
      "x": -1.825,
      "y": 0.777,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 1.108,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 35,
      "x": -1.735,
      "y": -0.18,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 34,
      "x": -1.026,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 33,
      "x": -1.108,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        34
      ]
    },
    {
      "id": 32,
      "x": 1.031,
      "y": -0.09,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 31,
      "x": -0.948,
      "y": 0.623,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 30,
      "x": 0.948,
      "y": -0.017,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 29,
      "x": -0.865,
      "y": 0.541,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 28,
      "x": 0.87,
      "y": 0.059,
      "layer": 4,
      "closed_by": [
        26
      ]
    },
    {
      "id": 27,
      "x": -0.573,
      "y": 0.46,
      "layer": 5,
      "closed_by": [
        25
      ]
    },
    {
      "id": 26,
      "x": 0.574,
      "y": 0.14,
      "layer": 5,
      "closed_by": [
        24
      ]
    },
    {
      "id": 25,
      "x": -0.412,
      "y": 0.381,
      "layer": 6,
      "closed_by": [
        23
      ]
    },
    {
      "id": 24,
      "x": 0.409,
      "y": 0.216,
      "layer": 6,
      "closed_by": [
        23
      ]
    },
    {
      "id": 51,
      "x": -1.825,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        50,
        35
      ]
    },
    {
      "id": 49,
      "x": -1.733,
      "y": 0.781,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 48,
      "x": -1.904,
      "y": -0.18,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 47,
      "x": 1.822,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        46,
        45
      ]
    },
    {
      "id": 41,
      "x": 1.822,
      "y": 0.319,
      "layer": 4,
      "closed_by": [
        39,
        40
      ]
    },
    {
      "id": 39,
      "x": 1.822,
      "y": -0.179,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 1.822,
      "y": 0.777,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 23,
      "y": 0.3,
      "layer": 7,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 29,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 53.5,
    "star2": 32.4,
    "star3": 13.7
  }
}