{
  "layout_cards": [
    {
      "id": 49,
      "x": 0.333,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        50,
        42
      ]
    },
    {
      "id": 48,
      "x": -0.331,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        50,
        41
      ]
    },
    {
      "id": 47,
      "x": 0.333,
      "y": 0.537,
      "layer": 1,
      "closed_by": [
        45,
        44
      ]
    },
    {
      "id": 46,
      "x": -0.333,
      "y": 0.537,
      "layer": 1,
      "closed_by": [
        45,
        43
      ]
    },
    {
      "id": 45,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        23,
        22
      ]
    },
    {
      "id": 50,
      "x": 0.001,
      "y": -0.497,
      "layer": 2,
      "closed_by": [
        23,
        22
      ]
    },
    {
      "id": 44,
      "x": 0.545,
      "y": 0.619,
      "layer": 2,
      "closed_by": [
        39,
        22
      ]
    },
    {
      "id": 43,
      "x": -0.544,
      "y": 0.619,
      "layer": 2,
      "closed_by": [
        40,
        23
      ]
    },
    {
      "id": 42,
      "x": 0.546,
      "y": -0.497,
      "layer": 2,
      "closed_by": [
        38,
        22
      ]
    },
    {
      "id": 41,
      "x": -0.546,
      "y": -0.497,
      "layer": 2,
      "closed_by": [
        35,
        23
      ]
    },
    {
      "id": 40,
      "x": -0.787,
      "y": 0.698,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 39,
      "x": 0.786,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 38,
      "x": 0.787,
      "y": -0.416,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 35,
      "x": -0.786,
      "y": -0.416,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 34,
      "x": 1.026,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        29,
        24
      ]
    },
    {
      "id": 32,
      "x": -1.026,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        28,
        25
      ]
    },
    {
      "id": 33,
      "x": 1.026,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        30,
        24
      ]
    },
    {
      "id": 30,
      "x": 1.314,
      "y": -0.254,
      "layer": 5,
      "closed_by": [
        27
      ]
    },
    {
      "id": 29,
      "x": 1.314,
      "y": 0.859,
      "layer": 5,
      "closed_by": [
        27
      ]
    },
    {
      "id": 28,
      "x": -1.312,
      "y": 0.859,
      "layer": 5,
      "closed_by": [
        26
      ]
    },
    {
      "id": 27,
      "x": 1.315,
      "y": 0.224,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -1.312,
      "y": 0.224,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -1.312,
      "y": -0.254,
      "layer": 5,
      "closed_by": [
        26
      ]
    },
    {
      "id": 25,
      "x": -0.787,
      "y": 0.136,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 23,
      "x": -0.261,
      "y": 0.079,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 22,
      "x": 0.268,
      "y": 0.079,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 0.79,
      "y": 0.136,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -1.024,
      "y": -0.34,
      "layer": 4,
      "closed_by": [
        36,
        25
      ]
    }
  ],
  "cards_in_layout_amount": 28,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 62.2,
    "star2": 22.1,
    "star3": 15.1
  }
}