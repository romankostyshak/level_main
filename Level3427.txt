{
  "layout_cards": [
    {
      "id": 22,
      "x": -0.412,
      "y": -0.342,
      "layer": 3,
      "closed_by": [
        51
      ]
    },
    {
      "id": 23,
      "x": 0.412,
      "y": -0.34,
      "layer": 3,
      "closed_by": [
        51
      ]
    },
    {
      "id": 24,
      "x": -0.361,
      "y": 0.856,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 25,
      "x": 0.351,
      "y": 0.86,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 26,
      "x": 0.001,
      "y": 0.785,
      "layer": 2,
      "closed_by": [
        24,
        25
      ]
    },
    {
      "id": 27,
      "x": 0.004,
      "y": -0.252,
      "layer": 2,
      "closed_by": [
        22,
        23
      ]
    },
    {
      "id": 28,
      "x": -0.333,
      "y": 0.229,
      "layer": 1,
      "closed_by": [
        26,
        27
      ]
    },
    {
      "id": 29,
      "x": 0.333,
      "y": 0.222,
      "layer": 1,
      "closed_by": [
        26,
        27
      ]
    },
    {
      "id": 30,
      "x": -1.906,
      "y": 0.224,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 31,
      "x": -1.758,
      "y": 0.758,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        32,
        35
      ]
    },
    {
      "id": 32,
      "x": -1.519,
      "y": 0.726,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        33,
        36
      ]
    },
    {
      "id": 33,
      "x": -1.279,
      "y": 0.726,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [
        34,
        37
      ]
    },
    {
      "id": 34,
      "x": -1.036,
      "y": 0.726,
      "angle": 330.0,
      "layer": 5,
      "closed_by": [
        38
      ]
    },
    {
      "id": 35,
      "x": -1.929,
      "y": 0.008,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 36,
      "x": -1.697,
      "y": 0.008,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 37,
      "x": -1.452,
      "y": 0.008,
      "angle": 330.0,
      "layer": 5,
      "closed_by": [
        38
      ]
    },
    {
      "id": 38,
      "x": -1.184,
      "y": 0.187,
      "layer": 6,
      "closed_by": [
        39
      ]
    },
    {
      "id": 39,
      "x": -0.975,
      "y": 0.061,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 1.48,
      "y": 0.024,
      "angle": 29.999,
      "layer": 5,
      "closed_by": [
        49
      ]
    },
    {
      "id": 41,
      "x": 1.705,
      "y": 0.016,
      "angle": 29.999,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 42,
      "x": 0.975,
      "y": 0.059,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 43,
      "x": 1.947,
      "y": 0.017,
      "angle": 29.999,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 44,
      "x": 1.764,
      "y": 0.771,
      "angle": 29.999,
      "layer": 2,
      "closed_by": [
        43,
        45
      ]
    },
    {
      "id": 45,
      "x": 1.523,
      "y": 0.731,
      "angle": 29.999,
      "layer": 3,
      "closed_by": [
        41,
        46
      ]
    },
    {
      "id": 46,
      "x": 1.274,
      "y": 0.731,
      "angle": 29.999,
      "layer": 4,
      "closed_by": [
        40,
        47
      ]
    },
    {
      "id": 47,
      "x": 1.034,
      "y": 0.735,
      "angle": 29.999,
      "layer": 5,
      "closed_by": [
        49
      ]
    },
    {
      "id": 48,
      "x": 1.914,
      "y": 0.231,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 49,
      "x": 1.179,
      "y": 0.187,
      "layer": 6,
      "closed_by": [
        42
      ]
    },
    {
      "id": 50,
      "y": 1.024,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 51,
      "y": -0.493,
      "layer": 4,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 30,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 65.2,
    "star2": 5.9,
    "star3": 28.6
  }
}