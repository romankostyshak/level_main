{
  "layout_cards": [
    {
      "id": 49,
      "x": -1.049,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 48,
      "x": 1.054,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 45,
      "x": -0.652,
      "y": 0.381,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 50,
      "x": 0.652,
      "y": 0.379,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 44,
      "x": -0.261,
      "y": 0.381,
      "layer": 4,
      "closed_by": [
        42,
        40
      ]
    },
    {
      "id": 43,
      "x": 0.27,
      "y": 0.381,
      "layer": 4,
      "closed_by": [
        41,
        40
      ]
    },
    {
      "id": 42,
      "x": -0.656,
      "y": -0.331,
      "layer": 5,
      "closed_by": [
        38
      ]
    },
    {
      "id": 41,
      "x": 0.652,
      "y": -0.331,
      "layer": 5,
      "closed_by": [
        39
      ]
    },
    {
      "id": 40,
      "x": 0.002,
      "y": -0.331,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 46,
      "x": 1.057,
      "y": 0.379,
      "layer": 2,
      "closed_by": [
        50,
        34
      ]
    },
    {
      "id": 39,
      "x": 1.059,
      "y": -0.333,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -1.049,
      "y": -0.333,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 47,
      "x": -1.047,
      "y": 0.379,
      "layer": 2,
      "closed_by": [
        45,
        35
      ]
    },
    {
      "id": 35,
      "x": -1.343,
      "y": 0.541,
      "layer": 3,
      "closed_by": [
        32,
        30
      ]
    },
    {
      "id": 34,
      "x": 1.348,
      "y": 0.544,
      "layer": 3,
      "closed_by": [
        33,
        31
      ]
    },
    {
      "id": 33,
      "x": 1.616,
      "y": 1.019,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -1.613,
      "y": 1.019,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 1.616,
      "y": 0.143,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -1.615,
      "y": 0.143,
      "layer": 4,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 19,
  "cards_in_deck_amount": 20,
  "stars_statistic": {
    "star1": 17.4,
    "star2": 0.2,
    "star3": 81.4
  }
}