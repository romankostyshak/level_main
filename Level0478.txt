{
  "layout_cards": [
    {
      "id": 51,
      "x": -1.495,
      "y": 0.224,
      "layer": 1,
      "closed_by": [
        50,
        42
      ]
    },
    {
      "id": 50,
      "x": -1.411,
      "y": 0.851,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 49,
      "x": 1.399,
      "y": -0.398,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 48,
      "x": -0.771,
      "y": -0.252,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        37,
        36
      ]
    },
    {
      "id": 47,
      "x": -0.767,
      "y": 0.711,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        37,
        36
      ]
    },
    {
      "id": 46,
      "x": 0.758,
      "y": -0.256,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        39,
        35
      ]
    },
    {
      "id": 44,
      "x": -0.694,
      "y": -0.536,
      "angle": 15.0,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 43,
      "x": 0.683,
      "y": -0.531,
      "angle": 344.997,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 42,
      "x": -1.416,
      "y": -0.388,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 41,
      "x": 1.398,
      "y": 0.855,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 40,
      "x": 0.757,
      "y": 0.712,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        39,
        35
      ]
    },
    {
      "id": 39,
      "x": 1.093,
      "y": 0.216,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 1.48,
      "y": 0.231,
      "layer": 1,
      "closed_by": [
        49,
        41
      ]
    },
    {
      "id": 37,
      "x": -1.095,
      "y": 0.215,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -0.574,
      "y": 0.215,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 0.577,
      "y": 0.215,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 34,
      "y": 0.215,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -0.694,
      "y": 0.981,
      "angle": 344.997,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 32,
      "y": 1.0,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 31,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        30
      ]
    },
    {
      "id": 30,
      "y": -0.187,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 29,
      "y": 0.625,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 45,
      "x": 0.685,
      "y": 0.98,
      "angle": 15.0,
      "layer": 1,
      "closed_by": [
        40
      ]
    }
  ],
  "cards_in_layout_amount": 23,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 62.1,
    "star2": 11.5,
    "star3": 25.5
  }
}