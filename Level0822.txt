{
  "layout_cards": [
    {
      "id": 51,
      "x": 2.413,
      "y": 0.07,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 50,
      "x": 0.731,
      "y": 0.061,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 49,
      "x": -2.414,
      "y": 0.537,
      "layer": 4,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": 2.413,
      "y": 0.536,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 47,
      "x": -0.731,
      "y": 0.3,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 46,
      "x": -2.414,
      "y": 0.298,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 45,
      "x": -0.731,
      "y": 0.537,
      "layer": 1,
      "closed_by": [
        19
      ]
    },
    {
      "id": 44,
      "x": 2.413,
      "y": 0.537,
      "layer": 2,
      "closed_by": [
        51
      ]
    },
    {
      "id": 43,
      "x": 2.411,
      "y": 0.064,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 42,
      "x": 0.731,
      "y": 0.298,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 41,
      "x": -0.731,
      "y": 0.54,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 40,
      "x": 0.014,
      "y": 0.259,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        37
      ]
    },
    {
      "id": 39,
      "x": 0.007,
      "y": -0.108,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 38,
      "x": -0.731,
      "y": 0.061,
      "layer": 4,
      "closed_by": [
        47
      ]
    },
    {
      "id": 37,
      "x": 0.001,
      "y": 0.252,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        39,
        25
      ]
    },
    {
      "id": 36,
      "x": 1.565,
      "y": 0.3,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 1.825,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 34,
      "x": 1.519,
      "y": 0.291,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        35,
        33
      ]
    },
    {
      "id": 33,
      "x": 1.292,
      "y": 0.3,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 32,
      "x": -1.292,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 31,
      "x": 0.734,
      "y": 0.532,
      "layer": 1,
      "closed_by": [
        20
      ]
    },
    {
      "id": 30,
      "x": 2.414,
      "y": 0.303,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -1.511,
      "y": 0.293,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        32,
        27
      ]
    },
    {
      "id": 28,
      "x": -1.559,
      "y": 0.298,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 27,
      "x": -1.825,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 26,
      "x": -2.414,
      "y": 0.537,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 25,
      "x": 0.008,
      "y": 0.703,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 24,
      "x": 0.008,
      "y": 0.296,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 23,
      "x": -2.414,
      "y": 0.064,
      "layer": 3,
      "closed_by": [
        49
      ]
    },
    {
      "id": 22,
      "x": -2.417,
      "y": 0.063,
      "layer": 1,
      "closed_by": [
        26
      ]
    },
    {
      "id": 21,
      "x": 0.731,
      "y": 0.537,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 20,
      "x": 0.731,
      "y": 0.064,
      "layer": 2,
      "closed_by": [
        21
      ]
    },
    {
      "id": 19,
      "x": -0.735,
      "y": 0.063,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 18,
      "x": 1.524,
      "y": 0.284,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        35,
        33
      ]
    },
    {
      "id": 17,
      "x": -1.508,
      "y": 0.298,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        29
      ]
    }
  ],
  "cards_in_layout_amount": 35,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 4.1,
    "star2": 65.4,
    "star3": 29.5
  }
}