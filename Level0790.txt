{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.861,
      "y": 1.023,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 50,
      "x": -0.865,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 49,
      "x": -1.534,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 48,
      "x": 1.531,
      "y": 1.023,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 47,
      "x": 1.531,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 46,
      "x": -1.536,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        37
      ]
    },
    {
      "id": 45,
      "x": -0.865,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 44,
      "x": 0.865,
      "y": 0.944,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 43,
      "x": 1.534,
      "y": 0.944,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 42,
      "x": 1.531,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 41,
      "x": 0.869,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        16
      ]
    },
    {
      "id": 40,
      "x": -0.864,
      "y": 0.944,
      "layer": 2,
      "closed_by": [
        14
      ]
    },
    {
      "id": 39,
      "x": -1.534,
      "y": 0.944,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 38,
      "x": -0.865,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 37,
      "x": -1.534,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 36,
      "x": -1.853,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 35,
      "x": -1.855,
      "y": 0.861,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 34,
      "x": 1.85,
      "y": 0.782,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -1.853,
      "y": -0.18,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -1.855,
      "y": 0.786,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 0.49,
      "y": 0.3,
      "layer": 6,
      "closed_by": [
        28
      ]
    },
    {
      "id": 30,
      "x": -0.497,
      "y": 0.298,
      "layer": 6,
      "closed_by": [
        29
      ]
    },
    {
      "id": 29,
      "x": -0.256,
      "y": 0.298,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 0.263,
      "y": 0.298,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 0.545,
      "y": -0.1,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 26,
      "x": 0.545,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 25,
      "x": -0.546,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 24,
      "x": 1.853,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 23,
      "x": 1.853,
      "y": -0.18,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 22,
      "x": 1.85,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 21,
      "x": -0.546,
      "y": -0.1,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 20,
      "x": -0.546,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 19,
      "x": 0.541,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        15
      ]
    },
    {
      "id": 18,
      "x": 0.541,
      "y": 0.72,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 17,
      "x": 0.865,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        41
      ]
    },
    {
      "id": 16,
      "x": 0.545,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 15,
      "x": 0.541,
      "y": 0.786,
      "layer": 4,
      "closed_by": [
        18
      ]
    },
    {
      "id": 14,
      "x": -0.546,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        12
      ]
    },
    {
      "id": 13,
      "x": -0.546,
      "y": 0.72,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 12,
      "x": -0.546,
      "y": 0.787,
      "layer": 4,
      "closed_by": [
        13
      ]
    }
  ],
  "cards_in_layout_amount": 40,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 1.3,
    "star2": 48.1,
    "star3": 49.9
  }
}