{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.007,
      "y": 0.46,
      "layer": 3,
      "closed_by": [],
      "card_value": "HQ"
    },
    {
      "id": 50,
      "x": -0.324,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        51
      ],
      "card_value": "HJ"
    },
    {
      "id": 49,
      "x": 0.344,
      "y": 0.702,
      "layer": 2,
      "closed_by": [
        51
      ],
      "card_value": "D10"
    },
    {
      "id": 48,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        50,
        49
      ],
      "card_value": "S9"
    },
    {
      "id": 47,
      "x": -0.66,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        50
      ],
      "card_value": "C8"
    },
    {
      "id": 46,
      "x": 0.652,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        49
      ],
      "card_value": "S7"
    },
    {
      "id": 45,
      "x": -0.335,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        36
      ],
      "card_value": "H4"
    },
    {
      "id": 44,
      "x": 1.319,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        37
      ],
      "card_value": "C10"
    },
    {
      "id": 43,
      "x": -1.319,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        35
      ],
      "card_value": "H6"
    },
    {
      "id": 42,
      "x": 0.98,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        44,
        41
      ],
      "card_value": "SJ"
    },
    {
      "id": 41,
      "x": 0.652,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        37
      ],
      "card_value": "DJ"
    },
    {
      "id": 40,
      "x": 1.618,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        44
      ],
      "card_value": "D2"
    },
    {
      "id": 39,
      "x": 0.337,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        41
      ],
      "card_value": "HA"
    },
    {
      "id": 38,
      "x": -1.6,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        43
      ],
      "card_value": "D3"
    },
    {
      "id": 37,
      "x": 0.995,
      "y": 0.059,
      "layer": 3,
      "closed_by": [],
      "card_value": "S3"
    },
    {
      "id": 36,
      "x": -0.638,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        35
      ],
      "card_value": "D8"
    },
    {
      "id": 35,
      "x": -0.958,
      "y": 0.059,
      "layer": 3,
      "closed_by": [],
      "card_value": "S6"
    },
    {
      "id": 34,
      "x": -0.958,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        43,
        36
      ],
      "card_value": "D7"
    }
  ],
  "cards_in_layout_amount": 18,
  "cards_in_deck_amount": 20,
  "stars_statistic": {
    "star1": 42.5,
    "star2": 0.8,
    "star3": 55.7
  }
}