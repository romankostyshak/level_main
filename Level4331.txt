{
  "layout_cards": [
    {
      "id": 21,
      "x": 1.758,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 22,
      "x": 1.96,
      "y": 0.136,
      "angle": 300.0,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 23,
      "x": -1.957,
      "y": 0.136,
      "angle": 60.0,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 24,
      "x": -2.16,
      "y": 0.479,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 25,
      "x": 2.157,
      "y": 0.479,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 26,
      "x": -1.758,
      "y": 0.479,
      "angle": 300.0,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 27,
      "x": 1.758,
      "y": 0.479,
      "angle": 60.0,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 28,
      "x": 1.5,
      "y": 0.879,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 29,
      "x": -1.08,
      "y": 0.879,
      "angle": 300.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 1.077,
      "y": 0.879,
      "angle": 60.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -1.019,
      "y": 0.059,
      "layer": 5,
      "closed_by": [
        33
      ]
    },
    {
      "id": 32,
      "x": 1.019,
      "y": 0.059,
      "layer": 5,
      "closed_by": [
        34
      ]
    },
    {
      "id": 33,
      "x": -0.819,
      "y": 0.418,
      "angle": 300.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 0.819,
      "y": 0.418,
      "angle": 60.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -0.398,
      "y": 0.418,
      "layer": 5,
      "closed_by": [
        33
      ]
    },
    {
      "id": 36,
      "x": -1.498,
      "y": 0.879,
      "layer": 5,
      "closed_by": [
        29
      ]
    },
    {
      "id": 38,
      "x": 0.4,
      "y": 0.418,
      "layer": 5,
      "closed_by": [
        34
      ]
    },
    {
      "id": 39,
      "x": -0.62,
      "y": 0.059,
      "angle": 300.0,
      "layer": 4,
      "closed_by": [
        31,
        35
      ]
    },
    {
      "id": 40,
      "x": 0.62,
      "y": 0.059,
      "angle": 60.0,
      "layer": 4,
      "closed_by": [
        32,
        38
      ]
    },
    {
      "id": 41,
      "x": -0.879,
      "y": 0.059,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 42,
      "x": -0.358,
      "y": 0.059,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 43,
      "x": 0.36,
      "y": 0.059,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 44,
      "x": 0.879,
      "y": 0.059,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 45,
      "x": -0.62,
      "y": -0.337,
      "angle": 300.0,
      "layer": 2,
      "closed_by": [
        41,
        42
      ]
    },
    {
      "id": 46,
      "x": -0.898,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 47,
      "x": -0.337,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 48,
      "x": 0.898,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 49,
      "x": 0.337,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": 0.62,
      "y": -0.337,
      "angle": 60.0,
      "layer": 2,
      "closed_by": [
        43,
        44
      ]
    },
    {
      "id": 51,
      "x": -1.758,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        23
      ]
    }
  ],
  "cards_in_layout_amount": 30,
  "cards_in_deck_amount": 28,
  "layers_in_level": 6,
  "open_cards": 4,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 44.8,
    "star2": 54.4,
    "star3": 0.8
  }
}