{
  "layout_cards": [
    {
      "id": 23,
      "x": 0.625,
      "y": -0.293,
      "angle": 340.0,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 24,
      "x": 2.581,
      "y": -0.3,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 25,
      "x": 0.625,
      "y": 0.976,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 26,
      "x": 2.591,
      "y": 0.976,
      "angle": 340.0,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 27,
      "x": -2.706,
      "y": -0.179,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 28,
      "x": -0.972,
      "y": -0.179,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -0.972,
      "y": 0.787,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -2.171,
      "y": -0.172,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -1.506,
      "y": -0.172,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -1.508,
      "y": 0.777,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -2.171,
      "y": 0.777,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -2.413,
      "y": -0.256,
      "layer": 1,
      "closed_by": [
        27,
        30
      ]
    },
    {
      "id": 35,
      "x": -1.264,
      "y": -0.256,
      "layer": 1,
      "closed_by": [
        28,
        31
      ]
    },
    {
      "id": 36,
      "x": -2.706,
      "y": 0.777,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -2.413,
      "y": 0.861,
      "layer": 1,
      "closed_by": [
        33,
        36
      ]
    },
    {
      "id": 39,
      "x": -1.264,
      "y": 0.861,
      "layer": 1,
      "closed_by": [
        29,
        32
      ]
    },
    {
      "id": 40,
      "x": -1.838,
      "y": 0.3,
      "layer": 1,
      "closed_by": [
        30,
        31,
        32,
        33
      ]
    },
    {
      "id": 41,
      "x": 0.652,
      "y": -0.209,
      "angle": 340.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 42,
      "x": 0.651,
      "y": 0.902,
      "angle": 20.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 43,
      "x": 2.549,
      "y": -0.209,
      "angle": 20.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 44,
      "x": 2.555,
      "y": 0.898,
      "angle": 340.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 45,
      "x": 1.963,
      "y": -0.128,
      "angle": 20.0,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 46,
      "x": 1.235,
      "y": 0.819,
      "angle": 20.0,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 47,
      "x": 1.968,
      "y": 0.814,
      "angle": 340.0,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 48,
      "x": 1.6,
      "y": -0.172,
      "layer": 1,
      "closed_by": [
        45,
        50
      ]
    },
    {
      "id": 49,
      "x": 1.595,
      "y": 0.859,
      "layer": 1,
      "closed_by": [
        46,
        47
      ]
    },
    {
      "id": 50,
      "x": 1.246,
      "y": -0.128,
      "angle": 340.0,
      "layer": 2,
      "closed_by": []
    }
  ],
  "level_mechanic": 1,
  "free_cards_from_stickers": [
    23,
    24,
    25,
    26,
    41,
    42,
    43,
    44,
    45,
    46,
    47,
    48,
    49,
    50
  ],
  "cards_in_layout_amount": 27,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 28.3,
    "star2": 68.2,
    "star3": 2.9
  }
}