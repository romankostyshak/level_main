{
  "layout_cards": [
    {
      "x": 0.814,
      "y": 0.54,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 1,
      "x": -0.81,
      "y": 0.541,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 2,
      "x": 0.814,
      "y": 0.537,
      "layer": 1,
      "closed_by": [
        11
      ]
    },
    {
      "id": 3,
      "x": -1.929,
      "y": 0.54,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 4,
      "x": -1.373,
      "y": 0.537,
      "layer": 1,
      "closed_by": [
        23
      ]
    },
    {
      "id": 5,
      "x": -0.81,
      "y": 0.537,
      "layer": 1,
      "closed_by": [
        10
      ]
    },
    {
      "id": 6,
      "x": -0.259,
      "y": 0.537,
      "layer": 1,
      "closed_by": [
        12
      ]
    },
    {
      "id": 7,
      "x": 0.263,
      "y": 0.537,
      "layer": 1,
      "closed_by": [
        13
      ]
    },
    {
      "id": 8,
      "x": 1.373,
      "y": 0.537,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 9,
      "x": 1.932,
      "y": 0.537,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 10,
      "x": -0.81,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 11,
      "x": 0.814,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        21
      ]
    },
    {
      "id": 12,
      "x": -0.259,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        14
      ]
    },
    {
      "id": 13,
      "x": 0.263,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        17
      ]
    },
    {
      "id": 14,
      "x": -0.259,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 15,
      "x": -0.259,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 16,
      "x": 0.814,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 17,
      "x": 0.263,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        19
      ]
    },
    {
      "id": 18,
      "x": -0.259,
      "y": 1.019,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 19,
      "x": 0.263,
      "y": 1.019,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 20,
      "x": -0.81,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 21,
      "x": 0.814,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 22,
      "x": 1.373,
      "y": 0.699,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 23,
      "x": -1.373,
      "y": 0.699,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 0.263,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 25,
      "x": -0.259,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        15
      ]
    },
    {
      "id": 26,
      "x": -0.81,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 27,
      "x": 1.373,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        34
      ]
    },
    {
      "id": 28,
      "x": 1.932,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 29,
      "x": -1.373,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        37
      ]
    },
    {
      "id": 30,
      "x": -1.927,
      "y": -0.34,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 31,
      "x": 0.268,
      "y": -0.411,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 32,
      "x": 0.814,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 33,
      "x": -0.81,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 34,
      "x": 1.373,
      "y": -0.418,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 0.814,
      "y": -0.337,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -0.81,
      "y": -0.5,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 37,
      "x": -1.373,
      "y": -0.418,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -0.259,
      "y": -0.5,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 39,
      "x": -1.985,
      "y": -0.337,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 0.814,
      "y": -0.5,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 41,
      "x": -0.259,
      "y": -0.578,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 42,
      "x": 0.27,
      "y": -0.574,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 43,
      "x": 0.814,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 44,
      "x": -1.983,
      "y": 0.541,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 45,
      "x": -0.809,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        1
      ]
    },
    {
      "id": 46,
      "x": 0.27,
      "y": -0.495,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 47,
      "x": -0.81,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 48,
      "x": 0.814,
      "y": 0.694,
      "layer": 4,
      "closed_by": [
        0
      ]
    },
    {
      "id": 49,
      "x": 1.985,
      "y": -0.337,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 50,
      "x": 1.985,
      "y": 0.537,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -0.81,
      "y": -0.337,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 52,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 0.0,
    "star2": 0.0,
    "star3": 99.3
  }
}