{
  "layout_cards": [
    {
      "id": 18,
      "x": 0.335,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        21,
        25
      ]
    },
    {
      "id": 19,
      "x": 0.335,
      "y": -0.5,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 20,
      "x": -0.333,
      "y": -0.5,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 21,
      "x": 0.465,
      "y": -0.193,
      "layer": 2,
      "closed_by": [
        19,
        33
      ]
    },
    {
      "id": 22,
      "x": -0.467,
      "y": -0.194,
      "layer": 2,
      "closed_by": [
        20,
        28
      ]
    },
    {
      "id": 23,
      "x": -0.333,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        22,
        24
      ]
    },
    {
      "id": 24,
      "x": -0.465,
      "y": 0.648,
      "layer": 2,
      "closed_by": [
        26,
        29
      ]
    },
    {
      "id": 25,
      "x": 0.469,
      "y": 0.638,
      "layer": 2,
      "closed_by": [
        27,
        30
      ]
    },
    {
      "id": 26,
      "x": -0.333,
      "y": 0.861,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 27,
      "x": 0.335,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 28,
      "x": -0.865,
      "y": -0.194,
      "layer": 3,
      "closed_by": [
        32,
        36
      ]
    },
    {
      "id": 29,
      "x": -0.865,
      "y": 0.647,
      "layer": 3,
      "closed_by": [
        37,
        40
      ]
    },
    {
      "id": 30,
      "x": 0.865,
      "y": 0.638,
      "layer": 3,
      "closed_by": [
        34,
        38
      ]
    },
    {
      "id": 31,
      "x": -1.503,
      "y": -0.199,
      "layer": 5,
      "closed_by": [
        51
      ]
    },
    {
      "id": 32,
      "x": -1.184,
      "y": -0.194,
      "layer": 4,
      "closed_by": [
        31,
        44
      ]
    },
    {
      "id": 33,
      "x": 0.865,
      "y": -0.193,
      "layer": 3,
      "closed_by": [
        35,
        39
      ]
    },
    {
      "id": 34,
      "x": 0.625,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 35,
      "x": 0.624,
      "y": -0.414,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 36,
      "x": -0.625,
      "y": -0.414,
      "layer": 4,
      "closed_by": [
        44
      ]
    },
    {
      "id": 37,
      "x": -0.625,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 38,
      "x": 1.184,
      "y": 0.638,
      "layer": 4,
      "closed_by": [
        41,
        45
      ]
    },
    {
      "id": 39,
      "x": 1.184,
      "y": -0.194,
      "layer": 4,
      "closed_by": [
        43,
        46
      ]
    },
    {
      "id": 40,
      "x": -1.184,
      "y": 0.648,
      "layer": 4,
      "closed_by": [
        42,
        47
      ]
    },
    {
      "id": 41,
      "x": 0.944,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        48
      ]
    },
    {
      "id": 42,
      "x": -0.939,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        50
      ]
    },
    {
      "id": 43,
      "x": 0.949,
      "y": -0.333,
      "layer": 5,
      "closed_by": [
        49
      ]
    },
    {
      "id": 44,
      "x": -0.947,
      "y": -0.337,
      "layer": 5,
      "closed_by": [
        51
      ]
    },
    {
      "id": 45,
      "x": 1.508,
      "y": 0.638,
      "layer": 5,
      "closed_by": [
        48
      ]
    },
    {
      "id": 46,
      "x": 1.506,
      "y": -0.187,
      "layer": 5,
      "closed_by": [
        49
      ]
    },
    {
      "id": 47,
      "x": -1.503,
      "y": 0.648,
      "layer": 5,
      "closed_by": [
        50
      ]
    },
    {
      "id": 48,
      "x": 1.269,
      "y": 0.638,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 49,
      "x": 1.264,
      "y": -0.256,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 50,
      "x": -1.248,
      "y": 0.648,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -1.271,
      "y": -0.256,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 34,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 32.5,
    "star2": 62.7,
    "star3": 4.1
  }
}