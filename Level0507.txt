{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.541,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        36,
        34
      ]
    },
    {
      "id": 50,
      "x": -0.544,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        47,
        33
      ]
    },
    {
      "id": 49,
      "x": -0.544,
      "y": 0.711,
      "layer": 1,
      "closed_by": [
        48,
        33
      ]
    },
    {
      "id": 48,
      "x": -0.273,
      "y": 1.029,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 47,
      "x": -0.275,
      "y": -0.586,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 46,
      "y": 0.906,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 45,
      "x": 1.322,
      "y": 0.479,
      "layer": 2,
      "closed_by": [
        26
      ]
    },
    {
      "id": 44,
      "x": 2.163,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        30
      ]
    },
    {
      "id": 43,
      "x": -1.32,
      "y": 0.479,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 42,
      "x": -1.439,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 41,
      "x": -1.96,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 40,
      "x": -2.061,
      "y": 0.479,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 39,
      "x": -2.171,
      "y": 0.222,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 38,
      "x": 0.541,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        37,
        34
      ]
    },
    {
      "id": 37,
      "x": 0.279,
      "y": 1.029,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 36,
      "x": 0.279,
      "y": -0.586,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 35,
      "y": -0.418,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 0.578,
      "y": 0.216,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -0.586,
      "y": 0.216,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -1.212,
      "y": 0.221,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 31,
      "x": -1.695,
      "y": 0.62,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 2.052,
      "y": 0.481,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 29,
      "x": 1.937,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 28,
      "x": 1.697,
      "y": 0.62,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 1.21,
      "y": 0.221,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 26,
      "x": 1.432,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        28
      ]
    }
  ],
  "cards_in_layout_amount": 26,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 52.5,
    "star2": 29.7,
    "star3": 16.8
  }
}