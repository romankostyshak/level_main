{
  "layout_cards": [
    {
      "id": 25,
      "x": -0.66,
      "y": 0.62,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -2.22,
      "y": -0.179,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 27,
      "x": -1.442,
      "y": 1.059,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 28,
      "x": -1.962,
      "y": 1.059,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 29,
      "x": -1.179,
      "y": -0.199,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -1.962,
      "y": 0.238,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -2.482,
      "y": 0.238,
      "layer": 2,
      "closed_by": [
        26
      ]
    },
    {
      "id": 32,
      "x": -2.22,
      "y": 0.62,
      "layer": 1,
      "closed_by": [
        28,
        31
      ]
    },
    {
      "id": 33,
      "x": -1.699,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        27,
        30
      ]
    },
    {
      "id": 34,
      "x": -0.921,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 35,
      "x": -1.442,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 36,
      "x": -0.921,
      "y": 0.238,
      "layer": 2,
      "closed_by": [
        25,
        29,
        38
      ]
    },
    {
      "id": 37,
      "x": -1.442,
      "y": 0.238,
      "layer": 2,
      "closed_by": [
        29,
        33,
        38
      ]
    },
    {
      "id": 38,
      "x": -1.179,
      "y": 0.62,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -1.699,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        35,
        37
      ]
    },
    {
      "id": 40,
      "x": -1.179,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        34,
        35,
        36,
        37
      ]
    },
    {
      "id": 41,
      "x": -0.66,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        34,
        36
      ]
    },
    {
      "id": 42,
      "x": 1.659,
      "y": 0.699,
      "angle": 300.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 43,
      "x": 1.378,
      "y": 0.699,
      "angle": 60.0,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 44,
      "x": 1.919,
      "y": 0.259,
      "angle": 300.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 45,
      "x": 1.598,
      "y": 0.259,
      "angle": 300.0,
      "layer": 3,
      "closed_by": [
        51
      ]
    },
    {
      "id": 46,
      "x": 1.44,
      "y": 0.259,
      "angle": 60.0,
      "layer": 2,
      "closed_by": [
        45,
        48
      ]
    },
    {
      "id": 47,
      "x": 2.177,
      "y": -0.179,
      "angle": 300.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 48,
      "x": 1.858,
      "y": -0.179,
      "angle": 300.0,
      "layer": 3,
      "closed_by": [
        44,
        47
      ]
    },
    {
      "id": 49,
      "x": 1.179,
      "y": -0.179,
      "angle": 60.0,
      "layer": 2,
      "closed_by": [
        45,
        48
      ]
    },
    {
      "id": 50,
      "x": 0.86,
      "y": -0.179,
      "angle": 60.0,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 51,
      "x": 1.118,
      "y": 0.259,
      "angle": 60.0,
      "layer": 4,
      "closed_by": [
        42,
        44
      ]
    }
  ],
  "level_mechanic": 1,
  "free_cards_from_stickers": [
    42,
    43,
    44,
    45,
    46,
    47,
    48,
    49,
    50,
    51
  ],
  "cards_in_layout_amount": 27,
  "cards_in_deck_amount": 28,
  "layers_in_level": 5,
  "open_cards": 7,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 83.1,
    "star2": 16.1,
    "star3": 0.0
  }
}