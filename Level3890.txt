{
  "layout_cards": [
    {
      "x": 2.5,
      "y": -0.039,
      "layer": 2,
      "closed_by": [
        1
      ]
    },
    {
      "id": 1,
      "x": 2.5,
      "y": 0.279,
      "layer": 3,
      "closed_by": [
        2
      ]
    },
    {
      "id": 2,
      "x": 2.5,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        3
      ]
    },
    {
      "id": 3,
      "x": 2.5,
      "y": 0.939,
      "layer": 5,
      "closed_by": [
        44
      ]
    },
    {
      "id": 4,
      "x": 2.5,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        0
      ]
    },
    {
      "id": 5,
      "x": 0.279,
      "y": -0.337,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 6,
      "x": -0.279,
      "y": -0.337,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 7,
      "x": 1.378,
      "y": -0.337,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 8,
      "x": 0.279,
      "y": 0.939,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 9,
      "x": -1.378,
      "y": -0.337,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 10,
      "x": -1.659,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        9
      ]
    },
    {
      "id": 11,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        5,
        6
      ]
    },
    {
      "id": 12,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        8,
        42
      ]
    },
    {
      "id": 13,
      "x": 1.659,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        7
      ]
    },
    {
      "id": 14,
      "x": -1.1,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 15,
      "x": -0.537,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 16,
      "x": -0.537,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        6
      ]
    },
    {
      "id": 17,
      "x": 1.659,
      "y": 0.939,
      "layer": 8,
      "closed_by": [
        51
      ]
    },
    {
      "id": 18,
      "x": -1.659,
      "y": 0.939,
      "layer": 8,
      "closed_by": [
        50
      ]
    },
    {
      "id": 19,
      "x": -1.1,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        9
      ]
    },
    {
      "id": 20,
      "x": 1.098,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        51
      ]
    },
    {
      "id": 21,
      "x": 0.537,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        8
      ]
    },
    {
      "id": 22,
      "x": 1.098,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        7
      ]
    },
    {
      "id": 23,
      "x": -0.259,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        11,
        16
      ]
    },
    {
      "id": 24,
      "x": 0.259,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        11,
        41
      ]
    },
    {
      "id": 25,
      "x": 0.259,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        12,
        21
      ]
    },
    {
      "id": 26,
      "x": 1.378,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        17,
        20
      ]
    },
    {
      "id": 27,
      "x": -0.259,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        12,
        15
      ]
    },
    {
      "id": 28,
      "x": -1.939,
      "y": 0.939,
      "layer": 7,
      "closed_by": [
        18
      ]
    },
    {
      "id": 29,
      "x": -1.378,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        14,
        18
      ]
    },
    {
      "id": 30,
      "x": -1.378,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        10,
        19
      ]
    },
    {
      "id": 31,
      "x": 1.939,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        13
      ]
    },
    {
      "id": 32,
      "x": 1.378,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        13,
        22
      ]
    },
    {
      "id": 33,
      "x": 1.939,
      "y": 0.939,
      "layer": 7,
      "closed_by": [
        17
      ]
    },
    {
      "id": 34,
      "x": -0.819,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        14,
        15
      ]
    },
    {
      "id": 35,
      "x": 0.819,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        20,
        21
      ]
    },
    {
      "id": 36,
      "x": -0.819,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        16,
        19
      ]
    },
    {
      "id": 37,
      "x": -1.939,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        10
      ]
    },
    {
      "id": 38,
      "x": 0.819,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        22,
        41
      ]
    },
    {
      "id": 39,
      "x": -0.819,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        34,
        36
      ]
    },
    {
      "id": 40,
      "x": 0.819,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        35,
        38
      ]
    },
    {
      "id": 41,
      "x": 0.537,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        5
      ]
    },
    {
      "id": 42,
      "x": -0.279,
      "y": 0.939,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 43,
      "x": -2.22,
      "y": 0.939,
      "layer": 6,
      "closed_by": [
        28
      ]
    },
    {
      "id": 44,
      "x": 2.22,
      "y": 0.939,
      "layer": 6,
      "closed_by": [
        33
      ]
    },
    {
      "id": 45,
      "x": -2.499,
      "y": 0.939,
      "layer": 5,
      "closed_by": [
        43
      ]
    },
    {
      "id": 46,
      "x": -2.499,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        45
      ]
    },
    {
      "id": 47,
      "x": -2.499,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": -2.499,
      "y": -0.039,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 49,
      "x": -2.499,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 50,
      "x": -1.378,
      "y": 0.939,
      "layer": 9,
      "closed_by": []
    },
    {
      "id": 51,
      "x": 1.378,
      "y": 0.939,
      "layer": 9,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 52,
  "cards_in_deck_amount": 32,
  "layers_in_level": 9,
  "open_cards": 8,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 0.1,
    "star2": 21.8,
    "star3": 77.5
  }
}