{
  "layout_cards": [
    {
      "id": 5,
      "x": -2.019,
      "y": 0.217,
      "angle": 15.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 6,
      "x": 1.939,
      "y": -0.379,
      "layer": 4,
      "closed_by": [
        47
      ]
    },
    {
      "id": 7,
      "x": -1.159,
      "y": -0.217,
      "layer": 4,
      "closed_by": [
        50
      ]
    },
    {
      "id": 8,
      "y": 0.859,
      "layer": 1,
      "closed_by": [
        14
      ]
    },
    {
      "id": 9,
      "x": 1.758,
      "y": -0.518,
      "angle": 344.997,
      "layer": 1,
      "closed_by": [
        16
      ]
    },
    {
      "id": 10,
      "x": 0.898,
      "y": -0.298,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        17
      ]
    },
    {
      "id": 11,
      "x": 1.159,
      "y": -0.217,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 12,
      "x": -0.898,
      "y": -0.298,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        15
      ]
    },
    {
      "id": 13,
      "x": -1.758,
      "y": -0.518,
      "angle": 15.0,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 14,
      "y": 0.136,
      "layer": 2,
      "closed_by": [
        23,
        24
      ]
    },
    {
      "id": 15,
      "x": -1.12,
      "y": -0.217,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 16,
      "x": 1.899,
      "y": -0.379,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 17,
      "x": 1.12,
      "y": -0.217,
      "layer": 2,
      "closed_by": [
        27
      ]
    },
    {
      "id": 18,
      "x": -2.098,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        5
      ]
    },
    {
      "id": 19,
      "x": -1.439,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        50
      ]
    },
    {
      "id": 20,
      "x": 2.098,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        47
      ]
    },
    {
      "id": 21,
      "x": 1.44,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 22,
      "x": -0.62,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        49
      ]
    },
    {
      "id": 23,
      "x": 0.537,
      "y": 0.518,
      "angle": 300.0,
      "layer": 3,
      "closed_by": [
        41,
        46
      ]
    },
    {
      "id": 24,
      "x": -0.537,
      "y": 0.518,
      "angle": 60.0,
      "layer": 3,
      "closed_by": [
        22,
        46
      ]
    },
    {
      "id": 25,
      "x": -1.258,
      "y": 0.319,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        7,
        19
      ]
    },
    {
      "id": 26,
      "x": 0.578,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 27,
      "x": 1.258,
      "y": 0.319,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        11,
        21
      ]
    },
    {
      "id": 28,
      "x": -1.978,
      "y": 0.238,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        18,
        40
      ]
    },
    {
      "id": 29,
      "x": -2.059,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 30,
      "x": 1.98,
      "y": 0.238,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        6,
        20
      ]
    },
    {
      "id": 31,
      "x": 2.059,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 32,
      "x": -1.399,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 33,
      "x": -0.74,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 34,
      "x": 1.399,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        27
      ]
    },
    {
      "id": 35,
      "x": 2.259,
      "y": 0.619,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 36,
      "x": 1.7,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        31,
        34
      ]
    },
    {
      "id": 37,
      "x": -2.259,
      "y": 0.619,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 38,
      "x": -1.7,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        29,
        32
      ]
    },
    {
      "id": 39,
      "x": -0.578,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 40,
      "x": -1.939,
      "y": -0.379,
      "layer": 4,
      "closed_by": [
        5
      ]
    },
    {
      "id": 41,
      "x": 0.62,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        48
      ]
    },
    {
      "id": 42,
      "x": 1.139,
      "y": 0.619,
      "layer": 1,
      "closed_by": [
        34,
        43
      ]
    },
    {
      "id": 43,
      "x": 0.74,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 44,
      "x": -1.139,
      "y": 0.619,
      "layer": 1,
      "closed_by": [
        32,
        33,
        49
      ]
    },
    {
      "id": 45,
      "x": -1.899,
      "y": -0.379,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 46,
      "y": 0.1,
      "layer": 4,
      "closed_by": [
        48,
        49
      ]
    },
    {
      "id": 47,
      "x": 2.019,
      "y": 0.217,
      "angle": 344.997,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 48,
      "x": 0.559,
      "y": 0.479,
      "angle": 300.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 49,
      "x": -0.56,
      "y": 0.479,
      "angle": 60.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 50,
      "x": -1.299,
      "y": 0.298,
      "angle": 30.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 51,
      "x": 1.299,
      "y": 0.298,
      "angle": 330.0,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 47,
  "cards_in_deck_amount": 32,
  "layers_in_level": 5,
  "open_cards": 6,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 8.1,
    "star2": 78.6,
    "star3": 12.7
  }
}