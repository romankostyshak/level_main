{
  "layout_cards": [
    {
      "id": 51,
      "x": -1.773,
      "y": 0.861,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 50,
      "x": -1.771,
      "y": -0.256,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 49,
      "x": -1.613,
      "y": 0.785,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 48,
      "x": -1.613,
      "y": -0.178,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "x": -1.904,
      "y": 0.3,
      "layer": 3,
      "closed_by": [
        46,
        45
      ]
    },
    {
      "id": 46,
      "x": -1.531,
      "y": 0.712,
      "layer": 4,
      "closed_by": [
        44
      ]
    },
    {
      "id": 45,
      "x": -1.529,
      "y": -0.104,
      "layer": 4,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": -1.656,
      "y": 0.298,
      "layer": 5,
      "closed_by": [
        43,
        35
      ]
    },
    {
      "id": 43,
      "x": -1.371,
      "y": -0.104,
      "layer": 6,
      "closed_by": [
        34
      ]
    },
    {
      "id": 42,
      "x": -0.384,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        23
      ]
    },
    {
      "id": 41,
      "x": -0.386,
      "y": -0.172,
      "layer": 1,
      "closed_by": [
        23
      ]
    },
    {
      "id": 40,
      "x": -0.141,
      "y": 0.712,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 39,
      "x": -0.141,
      "y": -0.108,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 38,
      "x": -0.136,
      "y": 0.31,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 37,
      "x": 0.143,
      "y": 0.62,
      "layer": 5,
      "closed_by": [
        36
      ]
    },
    {
      "id": 36,
      "x": 0.143,
      "y": -0.014,
      "layer": 6,
      "closed_by": [
        33
      ]
    },
    {
      "id": 35,
      "x": -1.373,
      "y": 0.712,
      "layer": 6,
      "closed_by": [
        34
      ]
    },
    {
      "id": 34,
      "x": -1.212,
      "y": 0.3,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 0.307,
      "y": 0.3,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 1.131,
      "y": 0.3,
      "layer": 1,
      "closed_by": [
        31,
        30
      ]
    },
    {
      "id": 31,
      "x": 1.212,
      "y": 0.707,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 30,
      "x": 1.212,
      "y": -0.112,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 29,
      "x": 1.427,
      "y": 0.3,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 23,
      "x": -0.279,
      "y": 0.307,
      "layer": 2,
      "closed_by": [
        40,
        39
      ]
    },
    {
      "id": 28,
      "x": 1.506,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 27,
      "x": 1.506,
      "y": -0.017,
      "layer": 5,
      "closed_by": [
        25
      ]
    },
    {
      "id": 25,
      "x": 1.746,
      "y": 0.544,
      "layer": 6,
      "closed_by": [
        26
      ]
    },
    {
      "id": 26,
      "x": 1.746,
      "y": 0.061,
      "layer": 7,
      "closed_by": [
        24
      ]
    },
    {
      "id": 24,
      "x": 1.906,
      "y": 0.298,
      "layer": 8,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 29,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 59.7,
    "star2": 17.9,
    "star3": 21.4
  }
}