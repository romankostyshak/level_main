{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.592,
      "y": -0.546,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 50,
      "x": 0.587,
      "y": 1.014,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        49
      ]
    },
    {
      "id": 49,
      "x": 0.694,
      "y": 0.632,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 48,
      "x": -0.693,
      "y": -0.18,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 47,
      "x": 0.787,
      "y": 0.279,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        45
      ]
    },
    {
      "id": 46,
      "x": -0.791,
      "y": 0.187,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        44
      ]
    },
    {
      "id": 45,
      "x": 0.893,
      "y": -0.114,
      "angle": 15.0,
      "layer": 5,
      "closed_by": [
        43
      ]
    },
    {
      "id": 44,
      "x": -0.898,
      "y": 0.583,
      "angle": 15.0,
      "layer": 5,
      "closed_by": [
        42
      ]
    },
    {
      "id": 43,
      "x": 1.003,
      "y": -0.518,
      "angle": 15.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -1.003,
      "y": 0.985,
      "angle": 15.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 0.18,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 35,
      "x": 0.017,
      "y": -0.578,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 1.156,
      "y": 1.011,
      "angle": 15.0,
      "layer": 1,
      "closed_by": [
        25
      ]
    },
    {
      "id": 27,
      "x": -0.5,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        51
      ]
    },
    {
      "id": 26,
      "x": -1.159,
      "y": -0.537,
      "angle": 15.0,
      "layer": 1,
      "closed_by": [
        24
      ]
    },
    {
      "id": 25,
      "x": 1.263,
      "y": 0.619,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 24,
      "x": -1.263,
      "y": -0.143,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 23,
      "x": 1.37,
      "y": 0.229,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 22,
      "x": -1.368,
      "y": 0.246,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        20
      ]
    },
    {
      "id": 21,
      "x": 1.475,
      "y": -0.158,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        19
      ]
    },
    {
      "id": 20,
      "x": -1.47,
      "y": 0.638,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        18
      ]
    },
    {
      "id": 19,
      "x": 1.531,
      "y": -0.36,
      "angle": 15.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 18,
      "x": -1.524,
      "y": 0.837,
      "angle": 15.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -0.479,
      "y": 0.238,
      "layer": 1,
      "closed_by": [
        51,
        37
      ]
    },
    {
      "id": 37,
      "x": -0.158,
      "y": 0.238,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 39,
      "x": 0.18,
      "y": 0.238,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 36,
      "x": 0.5,
      "y": 0.238,
      "layer": 1,
      "closed_by": [
        50,
        39
      ]
    },
    {
      "id": 41,
      "x": 0.337,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 34,
      "x": 0.5,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        45,
        41
      ]
    },
    {
      "id": 33,
      "x": -0.499,
      "y": 1.059,
      "layer": 1,
      "closed_by": [
        44,
        32
      ]
    },
    {
      "id": 32,
      "x": -0.337,
      "y": 1.059,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 31,
      "x": -0.18,
      "y": 1.059,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 30,
      "x": -0.039,
      "y": 1.059,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 29,
      "x": 0.479,
      "y": 1.059,
      "layer": 1,
      "closed_by": [
        50
      ]
    }
  ],
  "cards_in_layout_amount": 34,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 12.3,
    "star2": 73.2,
    "star3": 13.9
  }
}