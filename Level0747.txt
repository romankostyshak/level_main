{
  "layout_cards": [
    {
      "id": 50,
      "x": -0.898,
      "y": 0.439,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 49,
      "x": -0.898,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 47,
      "x": -1.039,
      "y": -0.379,
      "layer": 3,
      "closed_by": [
        51
      ]
    },
    {
      "id": 46,
      "x": 1.039,
      "y": -0.158,
      "layer": 4,
      "closed_by": [
        38,
        43
      ]
    },
    {
      "id": 45,
      "x": 0.333,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 41,
      "x": 1.039,
      "y": 0.439,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 40,
      "x": 1.039,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 39,
      "x": 0.898,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 38,
      "x": 0.898,
      "y": 0.439,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 37,
      "x": -1.039,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 36,
      "x": -1.039,
      "y": 0.439,
      "layer": 3,
      "closed_by": [
        51
      ]
    },
    {
      "id": 34,
      "x": 0.493,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        45,
        39
      ]
    },
    {
      "id": 30,
      "y": -0.379,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -0.418,
      "y": -0.379,
      "layer": 1,
      "closed_by": [
        30,
        28
      ]
    },
    {
      "id": 33,
      "x": -0.335,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 35,
      "x": -0.493,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        49,
        33
      ]
    },
    {
      "id": 32,
      "y": 0.781,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 0.46,
      "y": -0.379,
      "layer": 1,
      "closed_by": [
        30,
        48
      ]
    },
    {
      "id": 48,
      "x": 0.777,
      "y": -0.379,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 43,
      "x": 1.299,
      "y": -0.379,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 44,
      "x": -1.299,
      "y": -0.379,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -1.039,
      "y": -0.158,
      "layer": 4,
      "closed_by": [
        50,
        44
      ]
    },
    {
      "id": 28,
      "x": -0.777,
      "y": -0.379,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 42,
      "x": 1.039,
      "y": -0.379,
      "layer": 3,
      "closed_by": [
        46
      ]
    }
  ],
  "cards_in_layout_amount": 24,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 50.1,
    "star2": 5.9,
    "star3": 43.5
  }
}