{
  "layout_cards": [
    {
      "id": 26,
      "x": -0.939,
      "y": 0.661,
      "angle": 40.0,
      "layer": 3,
      "closed_by": [
        36,
        38
      ]
    },
    {
      "id": 27,
      "x": -0.674,
      "y": 0.846,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        26
      ]
    },
    {
      "id": 28,
      "x": -0.381,
      "y": 0.981,
      "angle": 20.0,
      "layer": 1,
      "closed_by": [
        27,
        51
      ]
    },
    {
      "id": 29,
      "x": 0.948,
      "y": 0.642,
      "angle": 320.0,
      "layer": 3,
      "closed_by": [
        40,
        42
      ]
    },
    {
      "id": 30,
      "x": 0.68,
      "y": 0.833,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 31,
      "x": 0.379,
      "y": 0.976,
      "angle": 340.0,
      "layer": 1,
      "closed_by": [
        30,
        51
      ]
    },
    {
      "id": 32,
      "y": -0.439,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -0.432,
      "y": -0.358,
      "angle": 340.0,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 34,
      "x": 0.432,
      "y": -0.358,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 35,
      "x": -0.763,
      "y": -0.209,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        33,
        36
      ]
    },
    {
      "id": 36,
      "x": -1.08,
      "y": 0.008,
      "angle": 320.0,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 37,
      "x": -1.74,
      "y": 0.017,
      "angle": 40.0,
      "layer": 6,
      "closed_by": [
        36
      ]
    },
    {
      "id": 38,
      "x": -1.478,
      "y": 0.33,
      "angle": 320.0,
      "layer": 5,
      "closed_by": [
        37
      ]
    },
    {
      "id": 39,
      "x": -1.338,
      "y": 0.34,
      "angle": 40.0,
      "layer": 4,
      "closed_by": [
        36,
        38
      ]
    },
    {
      "id": 40,
      "x": 1.059,
      "y": 0.017,
      "angle": 40.0,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 1.74,
      "angle": 320.0,
      "layer": 6,
      "closed_by": [
        40
      ]
    },
    {
      "id": 42,
      "x": 1.46,
      "y": 0.34,
      "angle": 40.0,
      "layer": 5,
      "closed_by": [
        41
      ]
    },
    {
      "id": 43,
      "x": 0.759,
      "y": -0.202,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        34,
        40
      ]
    },
    {
      "id": 44,
      "x": 1.338,
      "y": 0.319,
      "angle": 320.0,
      "layer": 4,
      "closed_by": [
        40,
        42
      ]
    },
    {
      "id": 45,
      "x": -1.863,
      "y": 0.646,
      "angle": 320.0,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 46,
      "x": 2.128,
      "y": 0.841,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 47,
      "x": -2.131,
      "y": 0.837,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 48,
      "x": 2.42,
      "y": 0.976,
      "angle": 20.0,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 49,
      "x": -2.43,
      "y": 0.98,
      "angle": 340.0,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 50,
      "x": 1.86,
      "y": 0.652,
      "angle": 40.0,
      "layer": 3,
      "closed_by": [
        41,
        44
      ]
    },
    {
      "id": 51,
      "y": 1.039,
      "layer": 2,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 26,
  "cards_in_deck_amount": 24,
  "layers_in_level": 7,
  "open_cards": 4,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 33.1,
    "star2": 0.3,
    "star3": 65.9
  }
}