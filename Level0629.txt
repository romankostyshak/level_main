{
  "layout_cards": [
    {
      "id": 51,
      "x": 1.998,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": 2.0,
      "y": 0.652,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 49,
      "x": 1.998,
      "y": -0.193,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 48,
      "x": 1.998,
      "y": 0.228,
      "layer": 3,
      "closed_by": [
        41,
        40
      ]
    },
    {
      "id": 47,
      "x": 1.998,
      "y": -0.569,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 46,
      "x": -2.012,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 45,
      "x": -2.012,
      "y": -0.187,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": -2.012,
      "y": 0.228,
      "layer": 3,
      "closed_by": [
        42,
        38
      ]
    },
    {
      "id": 43,
      "x": -2.012,
      "y": 1.023,
      "layer": 1,
      "closed_by": [
        15
      ]
    },
    {
      "id": 42,
      "x": -1.615,
      "y": 0.652,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 41,
      "x": 1.595,
      "y": 0.652,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 40,
      "x": 1.593,
      "y": -0.194,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 39,
      "x": -1.291,
      "y": 0.712,
      "layer": 5,
      "closed_by": [
        34
      ]
    },
    {
      "id": 38,
      "x": -1.616,
      "y": -0.193,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 37,
      "x": -1.292,
      "y": -0.25,
      "layer": 5,
      "closed_by": [
        32
      ]
    },
    {
      "id": 36,
      "x": 1.274,
      "y": 0.712,
      "layer": 5,
      "closed_by": [
        33
      ]
    },
    {
      "id": 35,
      "x": 1.273,
      "y": -0.252,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 34,
      "x": -0.971,
      "y": 0.791,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 0.958,
      "y": 0.79,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -0.972,
      "y": -0.331,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 0.958,
      "y": -0.331,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 0.703,
      "y": 0.87,
      "layer": 5,
      "closed_by": [
        33
      ]
    },
    {
      "id": 29,
      "x": -0.707,
      "y": 0.87,
      "layer": 5,
      "closed_by": [
        34
      ]
    },
    {
      "id": 28,
      "x": 0.702,
      "y": -0.409,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 27,
      "x": -0.545,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 26,
      "x": 0.546,
      "y": 0.949,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 25,
      "x": -0.544,
      "y": -0.495,
      "layer": 4,
      "closed_by": [
        14
      ]
    },
    {
      "id": 24,
      "x": 0.55,
      "y": -0.495,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 23,
      "x": -0.409,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 22,
      "x": 0.409,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 21,
      "x": -0.412,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 20,
      "x": -0.331,
      "y": 0.707,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 19,
      "x": 0.331,
      "y": 0.708,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 18,
      "x": -0.319,
      "y": -0.261,
      "layer": 2,
      "closed_by": [
        21
      ]
    },
    {
      "id": 17,
      "x": 0.418,
      "y": -0.335,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 16,
      "x": 0.331,
      "y": -0.261,
      "layer": 2,
      "closed_by": [
        17
      ]
    },
    {
      "id": 15,
      "x": -2.012,
      "y": 0.652,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 14,
      "x": -0.712,
      "y": -0.407,
      "layer": 5,
      "closed_by": [
        32
      ]
    }
  ],
  "cards_in_layout_amount": 38,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 6.6,
    "star2": 60.1,
    "star3": 32.7
  }
}