{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.268,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 50,
      "x": 0.272,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 49,
      "x": 0.354,
      "y": 0.958,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 48,
      "x": -0.349,
      "y": 0.961,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 47,
      "x": 0.418,
      "y": 0.875,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 46,
      "x": -0.412,
      "y": 0.883,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 45,
      "x": 0.661,
      "y": 0.703,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        42,
        32
      ]
    },
    {
      "id": 44,
      "x": -0.652,
      "y": 0.699,
      "angle": 344.997,
      "layer": 4,
      "closed_by": [
        43,
        31
      ]
    },
    {
      "id": 43,
      "x": -0.412,
      "y": 0.623,
      "layer": 5,
      "closed_by": [
        41,
        29
      ]
    },
    {
      "id": 42,
      "x": 0.412,
      "y": 0.623,
      "layer": 5,
      "closed_by": [
        41,
        30
      ]
    },
    {
      "id": 41,
      "x": -0.017,
      "y": 0.541,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 40,
      "x": -0.465,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        41,
        35
      ]
    },
    {
      "id": 39,
      "x": 0.333,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 38,
      "x": 0.418,
      "y": -0.398,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 37,
      "x": -0.414,
      "y": -0.393,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 36,
      "x": 0.465,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        41,
        39
      ]
    },
    {
      "id": 35,
      "x": -0.333,
      "y": -0.335,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 34,
      "x": 0.638,
      "y": -0.537,
      "angle": 344.997,
      "layer": 4,
      "closed_by": [
        25
      ]
    },
    {
      "id": 33,
      "x": -0.629,
      "y": -0.55,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        26
      ]
    },
    {
      "id": 32,
      "x": 0.944,
      "y": 0.62,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 31,
      "x": -0.944,
      "y": 0.62,
      "layer": 5,
      "closed_by": [
        29
      ]
    },
    {
      "id": 30,
      "x": 0.794,
      "y": 0.699,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -0.786,
      "y": 0.699,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 1.348,
      "y": 0.537,
      "layer": 4,
      "closed_by": [
        32
      ]
    },
    {
      "id": 27,
      "x": -1.345,
      "y": 0.541,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 26,
      "x": -0.731,
      "y": -0.537,
      "angle": 30.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 0.75,
      "y": -0.527,
      "angle": 330.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 1.664,
      "y": 0.537,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 23,
      "x": -1.661,
      "y": 0.537,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 22,
      "x": 1.983,
      "y": 0.537,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 21,
      "x": -1.983,
      "y": 0.537,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 20,
      "x": 2.302,
      "y": 0.537,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 19,
      "x": -2.302,
      "y": 0.537,
      "layer": 1,
      "closed_by": [
        21
      ]
    }
  ],
  "cards_in_layout_amount": 33,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 32.2,
    "star2": 62.1,
    "star3": 4.8
  }
}