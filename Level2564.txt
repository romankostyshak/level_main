{
  "layout_cards": [
    {
      "id": 20,
      "x": -1.136,
      "y": 0.699,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 21,
      "x": -0.892,
      "y": 0.782,
      "layer": 5,
      "closed_by": [
        20
      ]
    },
    {
      "id": 22,
      "x": 0.893,
      "y": 0.777,
      "layer": 5,
      "closed_by": [
        37
      ]
    },
    {
      "id": 23,
      "x": -0.652,
      "y": 0.861,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 24,
      "x": 0.643,
      "y": 0.86,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 25,
      "x": -0.546,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 26,
      "x": 0.546,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 27,
      "x": -0.303,
      "y": 1.024,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 28,
      "x": -0.002,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        27,
        36
      ]
    },
    {
      "id": 29,
      "x": -1.422,
      "y": -0.337,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 1.427,
      "y": -0.337,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -0.865,
      "y": -0.495,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 0.869,
      "y": -0.495,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 0.307,
      "y": -0.574,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -0.305,
      "y": -0.574,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -1.103,
      "y": -0.259,
      "layer": 5,
      "closed_by": [
        29,
        31
      ]
    },
    {
      "id": 36,
      "x": 0.305,
      "y": 1.023,
      "layer": 2,
      "closed_by": [
        26
      ]
    },
    {
      "id": 37,
      "x": 1.128,
      "y": 0.702,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 1.105,
      "y": -0.259,
      "layer": 5,
      "closed_by": [
        30,
        32
      ]
    },
    {
      "id": 39,
      "x": -0.546,
      "y": -0.418,
      "layer": 5,
      "closed_by": [
        31,
        34
      ]
    },
    {
      "id": 40,
      "x": 0.546,
      "y": -0.416,
      "layer": 5,
      "closed_by": [
        32,
        33
      ]
    },
    {
      "id": 41,
      "x": -0.865,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        35,
        39
      ]
    },
    {
      "id": 42,
      "x": 0.865,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        38,
        40
      ]
    },
    {
      "id": 43,
      "y": -0.497,
      "layer": 5,
      "closed_by": [
        33,
        34
      ]
    },
    {
      "id": 44,
      "x": 0.305,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        40,
        43
      ]
    },
    {
      "id": 45,
      "x": -0.545,
      "y": -0.101,
      "layer": 3,
      "closed_by": [
        41,
        50
      ]
    },
    {
      "id": 46,
      "x": 0.546,
      "y": -0.101,
      "layer": 3,
      "closed_by": [
        42,
        44
      ]
    },
    {
      "id": 47,
      "x": -0.305,
      "y": -0.017,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 48,
      "x": 0.307,
      "y": -0.017,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 49,
      "y": 0.059,
      "layer": 1,
      "closed_by": [
        44,
        47,
        48,
        50
      ]
    },
    {
      "id": 50,
      "x": -0.305,
      "y": -0.335,
      "layer": 4,
      "closed_by": [
        39,
        43
      ]
    }
  ],
  "cards_in_layout_amount": 31,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 44.7,
    "star2": 42.9,
    "star3": 11.7
  }
}