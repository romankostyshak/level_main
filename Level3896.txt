{
  "layout_cards": [
    {
      "x": 1.531,
      "y": -0.418,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 1,
      "x": 1.531,
      "y": 0.859,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 2,
      "x": -1.531,
      "y": 0.859,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 3,
      "x": -1.85,
      "y": -0.416,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 4,
      "x": -1.271,
      "y": -0.416,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 5,
      "x": 1.271,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        1
      ]
    },
    {
      "id": 6,
      "x": -1.271,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        2
      ]
    },
    {
      "id": 7,
      "x": -1.85,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        2
      ]
    },
    {
      "id": 8,
      "x": 1.85,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        1
      ]
    },
    {
      "id": 9,
      "x": 1.271,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        0
      ]
    },
    {
      "id": 10,
      "x": 1.531,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        5,
        8
      ]
    },
    {
      "id": 11,
      "x": 2.091,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        8
      ]
    },
    {
      "id": 12,
      "x": 1.531,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        9,
        17
      ]
    },
    {
      "id": 13,
      "x": 0.972,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        9
      ]
    },
    {
      "id": 14,
      "x": 0.971,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        5
      ]
    },
    {
      "id": 15,
      "x": -1.531,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        6,
        7
      ]
    },
    {
      "id": 16,
      "x": 2.092,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        17
      ]
    },
    {
      "id": 17,
      "x": 1.85,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        0
      ]
    },
    {
      "id": 18,
      "x": -2.092,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        7
      ]
    },
    {
      "id": 19,
      "x": -0.972,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        6
      ]
    },
    {
      "id": 20,
      "x": -0.972,
      "y": -0.416,
      "layer": 3,
      "closed_by": [
        4
      ]
    },
    {
      "id": 21,
      "x": -2.092,
      "y": -0.574,
      "layer": 3,
      "closed_by": [
        3
      ]
    },
    {
      "id": 22,
      "x": -1.85,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        15,
        18
      ]
    },
    {
      "id": 23,
      "x": -1.85,
      "y": -0.574,
      "layer": 2,
      "closed_by": [
        21,
        37
      ]
    },
    {
      "id": 24,
      "x": -1.292,
      "y": -0.574,
      "layer": 2,
      "closed_by": [
        20,
        37
      ]
    },
    {
      "id": 25,
      "x": -1.292,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        15,
        19
      ]
    },
    {
      "id": 26,
      "x": -0.712,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 27,
      "x": -0.712,
      "y": -0.416,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 28,
      "x": 0.712,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        13
      ]
    },
    {
      "id": 29,
      "x": 1.271,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        10,
        14
      ]
    },
    {
      "id": 30,
      "x": 1.85,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        10,
        11
      ]
    },
    {
      "id": 31,
      "x": 1.85,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        12,
        16
      ]
    },
    {
      "id": 32,
      "x": 1.271,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        12,
        13
      ]
    },
    {
      "id": 33,
      "x": -2.092,
      "y": -0.495,
      "layer": 1,
      "closed_by": [
        23
      ]
    },
    {
      "id": 34,
      "x": -2.092,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 35,
      "x": -1.531,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        22,
        25
      ]
    },
    {
      "id": 36,
      "x": 0.711,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        14
      ]
    },
    {
      "id": 37,
      "x": -1.531,
      "y": -0.574,
      "layer": 3,
      "closed_by": [
        3,
        4
      ]
    },
    {
      "id": 38,
      "x": -1.531,
      "y": -0.495,
      "layer": 1,
      "closed_by": [
        23,
        24
      ]
    },
    {
      "id": 39,
      "x": -0.972,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        25,
        26
      ]
    },
    {
      "id": 40,
      "x": -0.972,
      "y": -0.335,
      "layer": 1,
      "closed_by": [
        24,
        27
      ]
    },
    {
      "id": 41,
      "x": -0.412,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        26
      ]
    },
    {
      "id": 42,
      "x": -0.412,
      "y": -0.335,
      "layer": 1,
      "closed_by": [
        27
      ]
    },
    {
      "id": 43,
      "x": 2.091,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        30
      ]
    },
    {
      "id": 44,
      "x": 1.531,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        29,
        30
      ]
    },
    {
      "id": 45,
      "x": 0.971,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        29,
        36
      ]
    },
    {
      "id": 46,
      "x": 2.092,
      "y": -0.499,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 47,
      "x": 1.531,
      "y": -0.499,
      "layer": 1,
      "closed_by": [
        31,
        32
      ]
    },
    {
      "id": 48,
      "x": 0.412,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        28
      ]
    },
    {
      "id": 49,
      "x": 0.972,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        28,
        32
      ]
    },
    {
      "id": 50,
      "x": 0.411,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 51,
      "x": -1.531,
      "y": -0.416,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 52,
  "cards_in_deck_amount": 30,
  "layers_in_level": 5,
  "open_cards": 4,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 0.1,
    "star2": 12.7,
    "star3": 86.6
  }
}