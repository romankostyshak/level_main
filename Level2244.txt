{
  "layout_cards": [
    {
      "id": 20,
      "x": -1.825,
      "y": -0.412,
      "layer": 5,
      "closed_by": [
        23
      ]
    },
    {
      "id": 21,
      "x": 1.825,
      "y": -0.414,
      "layer": 5,
      "closed_by": [
        24
      ]
    },
    {
      "id": 22,
      "x": 1.825,
      "y": 0.86,
      "layer": 5,
      "closed_by": [
        24
      ]
    },
    {
      "id": 23,
      "x": -1.743,
      "y": 0.216,
      "layer": 6,
      "closed_by": [
        49,
        50
      ]
    },
    {
      "id": 24,
      "x": 1.746,
      "y": 0.216,
      "layer": 6,
      "closed_by": [
        40,
        48
      ]
    },
    {
      "id": 25,
      "x": -1.825,
      "y": 0.856,
      "layer": 5,
      "closed_by": [
        23
      ]
    },
    {
      "id": 27,
      "x": 0.884,
      "y": 0.256,
      "angle": 315.0,
      "layer": 4,
      "closed_by": [
        35,
        44
      ]
    },
    {
      "id": 28,
      "x": 0.003,
      "y": 0.62,
      "layer": 1,
      "closed_by": [
        29,
        30
      ]
    },
    {
      "id": 29,
      "x": 0.344,
      "y": 0.578,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        31,
        45
      ]
    },
    {
      "id": 30,
      "x": -0.331,
      "y": 0.583,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        32,
        45
      ]
    },
    {
      "id": 31,
      "x": 0.623,
      "y": 0.462,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 32,
      "x": -0.611,
      "y": 0.467,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 33,
      "x": -0.882,
      "y": 0.263,
      "angle": 45.0,
      "layer": 4,
      "closed_by": [
        34,
        43
      ]
    },
    {
      "id": 34,
      "x": -1.187,
      "y": 0.298,
      "layer": 5,
      "closed_by": [
        37,
        39
      ]
    },
    {
      "id": 35,
      "x": 1.189,
      "y": 0.298,
      "layer": 5,
      "closed_by": [
        36,
        38
      ]
    },
    {
      "id": 36,
      "x": 1.192,
      "y": -0.337,
      "layer": 6,
      "closed_by": [
        40
      ]
    },
    {
      "id": 37,
      "x": -1.189,
      "y": -0.34,
      "layer": 6,
      "closed_by": [
        49
      ]
    },
    {
      "id": 38,
      "x": 1.189,
      "y": 0.777,
      "layer": 6,
      "closed_by": [
        41,
        48
      ]
    },
    {
      "id": 39,
      "x": -1.187,
      "y": 0.777,
      "layer": 6,
      "closed_by": [
        42,
        50
      ]
    },
    {
      "id": 40,
      "x": 1.587,
      "y": -0.495,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 0.813,
      "y": 0.939,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -0.814,
      "y": 0.939,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 43,
      "x": -0.625,
      "y": -0.178,
      "layer": 5,
      "closed_by": [
        46
      ]
    },
    {
      "id": 44,
      "x": 0.625,
      "y": -0.172,
      "layer": 5,
      "closed_by": [
        47
      ]
    },
    {
      "id": 45,
      "x": -0.002,
      "y": 1.023,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 46,
      "x": -0.331,
      "y": -0.014,
      "layer": 6,
      "closed_by": [
        51
      ]
    },
    {
      "id": 47,
      "x": 0.333,
      "y": -0.014,
      "layer": 6,
      "closed_by": [
        51
      ]
    },
    {
      "id": 48,
      "x": 1.585,
      "y": 0.939,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 49,
      "x": -1.588,
      "y": -0.499,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 50,
      "x": -1.582,
      "y": 0.939,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 51,
      "x": 0.003,
      "y": 0.143,
      "layer": 7,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 31,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 44.6,
    "star2": 44.3,
    "star3": 10.5
  }
}