{
  "layout_cards": [
    {
      "id": 29,
      "y": -0.578,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 30,
      "y": 1.019,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -1.279,
      "y": 0.231,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -0.981,
      "y": 0.689,
      "angle": 30.0,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 33,
      "x": -0.986,
      "y": -0.246,
      "angle": 330.0,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 34,
      "x": -0.777,
      "y": 0.231,
      "layer": 4,
      "closed_by": [
        32,
        33
      ]
    },
    {
      "id": 35,
      "x": 1.0,
      "y": 0.689,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 36,
      "x": 1.0,
      "y": -0.246,
      "angle": 30.0,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 37,
      "x": 1.279,
      "y": 0.231,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 40,
      "x": -0.279,
      "y": -0.347,
      "angle": 60.0,
      "layer": 3,
      "closed_by": [
        34,
        44
      ]
    },
    {
      "id": 41,
      "x": 0.777,
      "y": 0.231,
      "layer": 3,
      "closed_by": [
        35,
        36,
        42,
        44
      ]
    },
    {
      "id": 42,
      "x": 0.279,
      "y": 0.791,
      "angle": 60.0,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 43,
      "x": -0.279,
      "y": 0.81,
      "angle": 300.0,
      "layer": 3,
      "closed_by": [
        34,
        42
      ]
    },
    {
      "id": 44,
      "x": 0.289,
      "y": -0.37,
      "angle": 300.0,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 45,
      "x": 0.2,
      "y": -0.207,
      "angle": 300.0,
      "layer": 2,
      "closed_by": [
        40,
        41
      ]
    },
    {
      "id": 46,
      "x": 0.2,
      "y": 0.652,
      "angle": 60.0,
      "layer": 2,
      "closed_by": [
        41,
        43
      ]
    },
    {
      "id": 47,
      "x": 0.62,
      "y": 0.231,
      "layer": 1,
      "closed_by": [
        45,
        46
      ]
    },
    {
      "id": 48,
      "x": -0.2,
      "y": 0.652,
      "angle": 300.0,
      "layer": 1,
      "closed_by": [
        46,
        50
      ]
    },
    {
      "id": 49,
      "x": -0.2,
      "y": -0.207,
      "angle": 60.0,
      "layer": 1,
      "closed_by": [
        45,
        50
      ]
    },
    {
      "id": 50,
      "x": -0.62,
      "y": 0.231,
      "layer": 2,
      "closed_by": [
        40,
        43
      ]
    }
  ],
  "cards_in_layout_amount": 20,
  "cards_in_deck_amount": 22,
  "layers_in_level": 6,
  "open_cards": 4,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 84.8,
    "star2": 14.7,
    "star3": 0.2
  }
}