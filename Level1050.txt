{
  "layout_cards": [
    {
      "id": 51,
      "x": -2.657,
      "y": -0.456,
      "angle": 20.0,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 50,
      "x": -2.023,
      "y": -0.456,
      "angle": 20.0,
      "layer": 1,
      "closed_by": [
        45,
        44
      ]
    },
    {
      "id": 49,
      "x": -1.376,
      "y": -0.458,
      "angle": 20.0,
      "layer": 1,
      "closed_by": [
        44,
        43
      ]
    },
    {
      "id": 48,
      "x": -0.712,
      "y": -0.458,
      "angle": 20.0,
      "layer": 1,
      "closed_by": [
        43,
        41
      ]
    },
    {
      "id": 47,
      "x": -0.074,
      "y": -0.456,
      "angle": 20.0,
      "layer": 1,
      "closed_by": [
        42,
        41
      ]
    },
    {
      "id": 46,
      "x": 0.564,
      "y": -0.458,
      "angle": 20.0,
      "layer": 1,
      "closed_by": [
        42,
        28
      ]
    },
    {
      "id": 45,
      "x": -2.47,
      "y": -0.342,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 44,
      "x": -1.827,
      "y": -0.342,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        40,
        39
      ]
    },
    {
      "id": 43,
      "x": -1.174,
      "y": -0.342,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        39,
        38
      ]
    },
    {
      "id": 42,
      "x": 0.128,
      "y": -0.34,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        37,
        36
      ]
    },
    {
      "id": 41,
      "x": -0.513,
      "y": -0.342,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        38,
        37
      ]
    },
    {
      "id": 40,
      "x": -2.233,
      "y": -0.206,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 39,
      "x": -1.588,
      "y": -0.202,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        35,
        34
      ]
    },
    {
      "id": 38,
      "x": -0.939,
      "y": -0.208,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        34,
        33
      ]
    },
    {
      "id": 37,
      "x": -0.275,
      "y": -0.202,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        33,
        32
      ]
    },
    {
      "id": 36,
      "x": 0.361,
      "y": -0.207,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 35,
      "x": -2.006,
      "y": -0.075,
      "angle": 20.0,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 34,
      "x": -1.353,
      "y": -0.064,
      "angle": 20.0,
      "layer": 4,
      "closed_by": [
        31,
        30
      ]
    },
    {
      "id": 33,
      "x": -0.703,
      "y": -0.07,
      "angle": 20.0,
      "layer": 4,
      "closed_by": [
        30,
        29
      ]
    },
    {
      "id": 32,
      "x": -0.035,
      "y": -0.068,
      "angle": 20.0,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 31,
      "x": -1.769,
      "y": 0.054,
      "angle": 20.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -1.115,
      "y": 0.068,
      "angle": 20.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -0.483,
      "y": 0.057,
      "angle": 20.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 0.771,
      "y": -0.337,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 27,
      "x": 1.258,
      "y": -0.158,
      "angle": 20.0,
      "layer": 1,
      "closed_by": [
        26
      ]
    },
    {
      "id": 26,
      "x": 1.491,
      "y": -0.017,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 25,
      "x": 1.725,
      "y": 0.112,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 24,
      "x": 1.963,
      "y": 0.25,
      "angle": 20.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 0.657,
      "y": 0.497,
      "angle": 20.0,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 22,
      "x": 0.893,
      "y": 0.629,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        21
      ]
    },
    {
      "id": 21,
      "x": 1.131,
      "y": 0.768,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        20
      ]
    },
    {
      "id": 20,
      "x": 1.373,
      "y": 0.907,
      "angle": 20.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 19,
      "x": -0.143,
      "y": 0.986,
      "angle": 20.0,
      "layer": 1,
      "closed_by": [
        18
      ]
    },
    {
      "id": 18,
      "x": -0.351,
      "y": 0.958,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        17
      ]
    },
    {
      "id": 17,
      "x": -0.578,
      "y": 0.925,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        16
      ]
    },
    {
      "id": 16,
      "x": -0.814,
      "y": 0.888,
      "angle": 20.0,
      "layer": 4,
      "closed_by": [
        15
      ]
    },
    {
      "id": 15,
      "x": -0.893,
      "y": 0.929,
      "angle": 20.0,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 37,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 8.8,
    "star2": 68.1,
    "star3": 22.4
  }
}