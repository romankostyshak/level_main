{
  "layout_cards": [
    {
      "id": 20,
      "x": -0.437,
      "y": -0.05,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        27,
        28
      ]
    },
    {
      "id": 21,
      "x": -0.435,
      "y": 0.451,
      "angle": 340.0,
      "layer": 1,
      "closed_by": [
        24,
        35
      ]
    },
    {
      "id": 22,
      "x": 0.437,
      "y": 0.451,
      "angle": 20.0,
      "layer": 1,
      "closed_by": [
        23,
        32
      ]
    },
    {
      "id": 23,
      "x": 0.437,
      "y": 0.057,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 24,
      "x": -0.435,
      "y": 0.056,
      "angle": 340.0,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 25,
      "x": 0.437,
      "y": -0.054,
      "angle": 340.0,
      "layer": 3,
      "closed_by": [
        26,
        29
      ]
    },
    {
      "id": 26,
      "x": 0.439,
      "y": -0.456,
      "angle": 340.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 27,
      "x": -0.435,
      "y": -0.455,
      "angle": 20.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 28,
      "x": -0.916,
      "y": 0.638,
      "angle": 340.0,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 29,
      "x": 0.921,
      "y": 0.629,
      "angle": 20.0,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 30,
      "x": 0.944,
      "y": 1.021,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 31,
      "x": -0.944,
      "y": 1.021,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 32,
      "x": 0.652,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 33,
      "x": -1.723,
      "y": -0.535,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        51
      ]
    },
    {
      "id": 34,
      "x": 1.161,
      "y": 0.158,
      "angle": 20.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -0.648,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 36,
      "x": 0.768,
      "y": -0.532,
      "angle": 340.0,
      "layer": 1,
      "closed_by": [
        23,
        38
      ]
    },
    {
      "id": 37,
      "x": -0.763,
      "y": -0.532,
      "angle": 20.0,
      "layer": 1,
      "closed_by": [
        24,
        39
      ]
    },
    {
      "id": 38,
      "x": 1.001,
      "y": -0.527,
      "angle": 340.0,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 39,
      "x": -0.984,
      "y": -0.531,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 40,
      "x": 1.172,
      "y": -0.061,
      "angle": 340.0,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 41,
      "x": -1.161,
      "y": -0.052,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 42,
      "x": -1.167,
      "y": 0.163,
      "angle": 340.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 43,
      "x": 1.57,
      "y": 0.508,
      "angle": 340.0,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 44,
      "x": 1.325,
      "y": 0.976,
      "angle": 340.0,
      "layer": 1,
      "closed_by": [
        30,
        43
      ]
    },
    {
      "id": 45,
      "x": -1.567,
      "y": 0.509,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 46,
      "x": -1.33,
      "y": 0.976,
      "angle": 20.0,
      "layer": 1,
      "closed_by": [
        31,
        45
      ]
    },
    {
      "id": 47,
      "x": 1.401,
      "y": -0.532,
      "angle": 340.0,
      "layer": 1,
      "closed_by": [
        34,
        38,
        49
      ]
    },
    {
      "id": 48,
      "x": -1.404,
      "y": -0.531,
      "angle": 20.0,
      "layer": 1,
      "closed_by": [
        33,
        39,
        42
      ]
    },
    {
      "id": 49,
      "x": 1.725,
      "y": -0.527,
      "angle": 340.0,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": 2.042,
      "y": -0.527,
      "angle": 340.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -2.042,
      "y": -0.531,
      "angle": 20.0,
      "layer": 3,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 32,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 31.1,
    "star2": 57.5,
    "star3": 10.6
  }
}