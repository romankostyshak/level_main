{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.368,
      "y": -0.527,
      "angle": 340.0,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": 0.365,
      "y": 0.017,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        47,
        33
      ]
    },
    {
      "id": 49,
      "x": 0.363,
      "y": 0.919,
      "angle": 340.0,
      "layer": 4,
      "closed_by": [
        46,
        31
      ]
    },
    {
      "id": 48,
      "x": -1.692,
      "y": 0.039,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 47,
      "y": 0.064,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 46,
      "y": 1.019,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 45,
      "y": -0.028,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 44,
      "x": -0.358,
      "y": 0.921,
      "angle": 20.0,
      "layer": 4,
      "closed_by": [
        46,
        30
      ]
    },
    {
      "id": 43,
      "x": -0.358,
      "y": -0.537,
      "angle": 20.0,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 42,
      "x": -0.358,
      "y": 0.017,
      "angle": 340.0,
      "layer": 2,
      "closed_by": [
        47,
        28
      ]
    },
    {
      "id": 41,
      "x": 1.692,
      "y": 0.439,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 40,
      "y": 0.017,
      "layer": 4,
      "closed_by": [
        45
      ]
    },
    {
      "id": 39,
      "x": 1.692,
      "y": 0.777,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 1.692,
      "y": 0.039,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 37,
      "x": 1.692,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 36,
      "x": -1.692,
      "y": 0.859,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -1.692,
      "y": 0.46,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 34,
      "x": -1.692,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 33,
      "x": 0.74,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 32,
      "x": 0.74,
      "y": 0.059,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 31,
      "x": 0.74,
      "y": 0.777,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -0.74,
      "y": 0.777,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -0.74,
      "y": 0.059,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 28,
      "x": -0.74,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        29
      ]
    }
  ],
  "cards_in_layout_amount": 24,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 59.4,
    "star2": 12.6,
    "star3": 27.3
  }
}