{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.331,
      "y": 0.223,
      "layer": 1,
      "closed_by": [
        48,
        46
      ]
    },
    {
      "id": 50,
      "x": 0.333,
      "y": 0.222,
      "layer": 1,
      "closed_by": [
        49,
        47
      ]
    },
    {
      "id": 49,
      "x": 0.418,
      "y": 0.669,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 48,
      "x": -0.416,
      "y": 0.671,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 47,
      "x": 0.414,
      "y": -0.224,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 46,
      "x": -0.414,
      "y": -0.224,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 45,
      "x": 0.349,
      "y": 0.74,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 44,
      "x": -0.34,
      "y": 0.74,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 43,
      "x": 0.351,
      "y": -0.296,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 42,
      "x": -0.351,
      "y": -0.289,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 41,
      "x": 0.36,
      "y": -0.451,
      "angle": 344.997,
      "layer": 4,
      "closed_by": [
        37,
        36
      ]
    },
    {
      "id": 40,
      "x": -0.356,
      "y": -0.453,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        37,
        35
      ]
    },
    {
      "id": 39,
      "x": 0.351,
      "y": 0.907,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        36,
        18
      ]
    },
    {
      "id": 38,
      "x": -0.34,
      "y": 0.906,
      "angle": 344.997,
      "layer": 4,
      "closed_by": [
        35,
        18
      ]
    },
    {
      "id": 37,
      "x": 0.003,
      "y": -0.495,
      "layer": 5,
      "closed_by": [
        16
      ]
    },
    {
      "id": 36,
      "x": 0.546,
      "y": 0.224,
      "layer": 5,
      "closed_by": [
        34
      ]
    },
    {
      "id": 35,
      "x": -0.544,
      "y": 0.23,
      "layer": 5,
      "closed_by": [
        33
      ]
    },
    {
      "id": 34,
      "x": 0.703,
      "y": 0.224,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -0.703,
      "y": 0.228,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 1.294,
      "y": 0.224,
      "layer": 1,
      "closed_by": [
        30
      ]
    },
    {
      "id": 31,
      "x": -1.294,
      "y": 0.224,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 30,
      "x": 1.345,
      "y": 0.381,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 29,
      "x": -1.343,
      "y": 0.381,
      "layer": 2,
      "closed_by": [
        27
      ]
    },
    {
      "id": 28,
      "x": 1.424,
      "y": 0.541,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 27,
      "x": -1.424,
      "y": 0.541,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 26,
      "x": 1.508,
      "y": 0.703,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 25,
      "x": -1.503,
      "y": 0.707,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 24,
      "x": 1.511,
      "y": 0.224,
      "layer": 5,
      "closed_by": [
        22
      ]
    },
    {
      "id": 23,
      "x": -1.503,
      "y": 0.216,
      "layer": 5,
      "closed_by": [
        21
      ]
    },
    {
      "id": 22,
      "x": 1.513,
      "y": -0.172,
      "layer": 6,
      "closed_by": [
        20
      ]
    },
    {
      "id": 21,
      "x": -1.503,
      "y": -0.171,
      "layer": 6,
      "closed_by": [
        19
      ]
    },
    {
      "id": 20,
      "x": 1.514,
      "y": -0.574,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 19,
      "x": -1.503,
      "y": -0.572,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 18,
      "x": -0.001,
      "y": 0.939,
      "layer": 5,
      "closed_by": [
        17
      ]
    },
    {
      "id": 17,
      "x": 0.002,
      "y": 1.019,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 16,
      "x": 0.003,
      "y": -0.574,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 36,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 19.3,
    "star2": 69.6,
    "star3": 10.3
  }
}