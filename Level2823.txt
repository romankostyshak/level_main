{
  "layout_cards": [
    {
      "id": 24,
      "x": -1.182,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 25,
      "x": -0.81,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        29,
        39
      ]
    },
    {
      "id": 26,
      "x": -0.81,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        24,
        36
      ]
    },
    {
      "id": 27,
      "x": 0.81,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        31,
        40
      ]
    },
    {
      "id": 28,
      "x": 0.81,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        30,
        41
      ]
    },
    {
      "id": 29,
      "x": -1.184,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 30,
      "x": 1.184,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 31,
      "x": 1.187,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 32,
      "x": 1.582,
      "y": -0.418,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 1.582,
      "y": 1.021,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -1.585,
      "y": 1.019,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -1.582,
      "y": -0.418,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -0.569,
      "y": -0.18,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 37,
      "x": -2.625,
      "y": -0.177,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -1.983,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 39,
      "x": -0.569,
      "y": 0.782,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 0.573,
      "y": 0.781,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 0.569,
      "y": -0.18,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 42,
      "x": 1.98,
      "y": 1.021,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 43,
      "x": -1.985,
      "y": 1.016,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 44,
      "x": 1.98,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 45,
      "x": 2.384,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        42,
        49
      ]
    },
    {
      "id": 46,
      "x": 2.381,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        44,
        50
      ]
    },
    {
      "id": 47,
      "x": -2.384,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        43,
        51
      ]
    },
    {
      "id": 48,
      "x": -2.381,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        37,
        38
      ]
    },
    {
      "id": 49,
      "x": 2.625,
      "y": 0.777,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 50,
      "x": 2.625,
      "y": -0.172,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -2.624,
      "y": 0.777,
      "layer": 2,
      "closed_by": []
    }
  ],
  "level_mechanic": 1,
  "free_cards_from_stickers": [
    27,
    28,
    30,
    31,
    32,
    33,
    40,
    41,
    42,
    44,
    45,
    46,
    49,
    50
  ],
  "cards_in_layout_amount": 28,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 24.2,
    "star2": 71.7,
    "star3": 3.2
  }
}