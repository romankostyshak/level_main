{
  "layout_cards": [
    {
      "id": 51,
      "x": -2.309,
      "y": 0.328,
      "angle": 9.998,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 50,
      "x": -2.407,
      "y": 0.62,
      "angle": 5.0,
      "layer": 1,
      "closed_by": [
        51
      ]
    },
    {
      "id": 49,
      "x": 2.22,
      "y": 0.119,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 48,
      "x": 2.338,
      "y": 0.319,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        49
      ]
    },
    {
      "id": 47,
      "x": 2.46,
      "y": 0.619,
      "angle": 354.997,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 46,
      "x": 0.583,
      "y": -0.059,
      "angle": 25.0,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 45,
      "x": -0.259,
      "y": 0.469,
      "angle": 354.997,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 44,
      "x": 0.3,
      "y": 0.469,
      "angle": 4.999,
      "layer": 2,
      "closed_by": [
        26
      ]
    },
    {
      "id": 43,
      "x": 0.824,
      "y": -0.36,
      "angle": 35.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -1.569,
      "y": -0.231,
      "angle": 340.0,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 41,
      "x": -1.389,
      "y": -0.028,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 40,
      "x": -1.269,
      "y": 0.17,
      "angle": 349.997,
      "layer": 1,
      "closed_by": [
        41
      ]
    },
    {
      "id": 39,
      "x": -2.167,
      "y": 0.119,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 38,
      "x": 2.0,
      "y": -0.229,
      "angle": 335.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 37,
      "x": 1.419,
      "y": -0.028,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 36,
      "x": 1.6,
      "y": -0.229,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 35,
      "x": -0.546,
      "y": -0.061,
      "angle": 335.0,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 34,
      "x": -0.787,
      "y": -0.361,
      "angle": 325.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -1.949,
      "y": -0.224,
      "angle": 25.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 1.299,
      "y": 0.17,
      "angle": 9.998,
      "layer": 1,
      "closed_by": [
        37
      ]
    },
    {
      "id": 31,
      "x": 0.546,
      "y": 0.819,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 28,
      "x": 0.014,
      "y": 0.819,
      "layer": 1,
      "closed_by": [
        45,
        44
      ]
    },
    {
      "id": 27,
      "x": -0.526,
      "y": 0.819,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 26,
      "x": 0.412,
      "y": 0.216,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 25,
      "x": -0.372,
      "y": 0.216,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 29,
      "x": -1.136,
      "y": 1.019,
      "layer": 1,
      "closed_by": [],
      "effect_id": 4
    },
    {
      "id": 30,
      "x": 1.154,
      "y": 1.019,
      "layer": 1,
      "closed_by": [],
      "effect_id": 4
    }
  ],
  "cards_in_layout_amount": 27,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 60.4,
    "star2": 30.3,
    "star3": 8.4
  }
}