{
  "layout_cards": [
    {
      "id": 19,
      "x": -1.824,
      "y": 0.785,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 20,
      "x": 1.825,
      "y": 0.782,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 22,
      "x": 1.667,
      "y": -0.492,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 23,
      "x": -1.508,
      "y": -0.097,
      "layer": 4,
      "closed_by": [
        25,
        51
      ]
    },
    {
      "id": 24,
      "x": 1.508,
      "y": -0.1,
      "layer": 4,
      "closed_by": [
        26,
        50
      ]
    },
    {
      "id": 25,
      "x": -1.485,
      "y": -0.458,
      "angle": 45.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 26,
      "x": 1.485,
      "y": -0.46,
      "angle": 315.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 27,
      "x": -1.666,
      "y": -0.497,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 28,
      "x": -0.597,
      "y": -0.372,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 29,
      "x": -0.291,
      "y": -0.555,
      "angle": 5.0,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 30,
      "x": 0.296,
      "y": -0.555,
      "angle": 354.997,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 31,
      "x": 0.414,
      "y": -0.446,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        33,
        34
      ]
    },
    {
      "id": 32,
      "x": -0.414,
      "y": -0.448,
      "angle": 9.998,
      "layer": 2,
      "closed_by": [
        28,
        34
      ]
    },
    {
      "id": 33,
      "x": 0.592,
      "y": -0.372,
      "angle": 340.0,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 34,
      "y": 0.059,
      "layer": 3,
      "closed_by": [],
      "card_type": 2
    },
    {
      "id": 35,
      "x": 0.837,
      "y": -0.207,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [
        50
      ]
    },
    {
      "id": 36,
      "x": -0.833,
      "y": -0.206,
      "angle": 30.0,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 37,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        38,
        39
      ],
      "card_type": 3
    },
    {
      "id": 38,
      "x": 0.33,
      "y": 1.021,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 39,
      "x": -0.319,
      "y": 1.019,
      "angle": 9.998,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 40,
      "x": 0.652,
      "y": 0.782,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 41,
      "x": -1.424,
      "y": 0.861,
      "layer": 3,
      "closed_by": [
        19
      ]
    },
    {
      "id": 42,
      "x": -0.651,
      "y": 0.782,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 43,
      "x": 1.069,
      "y": 0.824,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        40,
        49
      ]
    },
    {
      "id": 44,
      "x": -1.069,
      "y": 0.827,
      "angle": 340.0,
      "layer": 2,
      "closed_by": [
        41,
        42
      ]
    },
    {
      "id": 45,
      "x": 1.243,
      "y": 0.981,
      "angle": 40.0,
      "layer": 1,
      "closed_by": [
        43,
        47
      ]
    },
    {
      "id": 46,
      "x": -1.179,
      "y": 0.971,
      "angle": 325.0,
      "layer": 1,
      "closed_by": [
        44,
        48
      ]
    },
    {
      "id": 47,
      "x": 1.565,
      "y": 0.976,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        49
      ]
    },
    {
      "id": 48,
      "x": -1.562,
      "y": 0.976,
      "angle": 340.0,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 49,
      "x": 1.427,
      "y": 0.861,
      "layer": 3,
      "closed_by": [
        20
      ]
    },
    {
      "id": 50,
      "x": 1.085,
      "y": -0.057,
      "angle": 315.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -1.1,
      "y": -0.046,
      "angle": 45.0,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 32,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 39.5,
    "star2": 52.2,
    "star3": 7.9
  }
}