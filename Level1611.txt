{
  "layout_cards": [
    {
      "id": 51,
      "x": 1.213,
      "y": -0.577,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 50,
      "x": -1.215,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 49,
      "x": 1.213,
      "y": -0.18,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 48,
      "x": -1.213,
      "y": -0.18,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 47,
      "x": 1.213,
      "y": 0.136,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 46,
      "x": -1.213,
      "y": 0.141,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 45,
      "x": 0.972,
      "y": 0.625,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 44,
      "x": -0.97,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 43,
      "x": -0.573,
      "y": 0.861,
      "layer": 5,
      "closed_by": [
        41
      ]
    },
    {
      "id": 42,
      "x": 0.572,
      "y": 0.859,
      "layer": 5,
      "closed_by": [
        40
      ]
    },
    {
      "id": 41,
      "x": -0.263,
      "y": 0.861,
      "layer": 6,
      "closed_by": [
        39
      ]
    },
    {
      "id": 40,
      "x": 0.27,
      "y": 0.859,
      "layer": 6,
      "closed_by": [
        39
      ]
    },
    {
      "id": 39,
      "x": -0.002,
      "y": 0.693,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 1.534,
      "y": 1.024,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 37,
      "x": -1.531,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 36,
      "x": 1.745,
      "y": 0.615,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 35,
      "x": -1.743,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 34,
      "x": 1.746,
      "y": 0.224,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 33,
      "x": -1.745,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 32,
      "x": 1.746,
      "y": -0.172,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 31,
      "x": -1.745,
      "y": -0.172,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 30,
      "x": 1.748,
      "y": -0.574,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -1.743,
      "y": -0.572,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 0.479,
      "y": -0.035,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 27,
      "x": -0.476,
      "y": -0.039,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 24,
      "x": -0.625,
      "y": -0.578,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 0.625,
      "y": -0.578,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 22,
      "y": -0.578,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 21,
      "x": -0.333,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        27,
        22
      ]
    },
    {
      "id": 26,
      "x": -0.333,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        21
      ]
    },
    {
      "id": 20,
      "x": 0.333,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        28,
        22
      ]
    },
    {
      "id": 25,
      "x": 0.333,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        20
      ]
    }
  ],
  "cards_in_layout_amount": 32,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 23.6,
    "star2": 67.2,
    "star3": 8.7
  }
}