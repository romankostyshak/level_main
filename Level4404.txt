{
  "layout_cards": [
    {
      "id": 19,
      "x": -0.859,
      "y": 1.098,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 20,
      "x": -0.337,
      "y": 1.098,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 21,
      "x": -0.859,
      "y": -0.5,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -0.337,
      "y": -0.5,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 0.337,
      "y": -0.5,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 0.86,
      "y": 1.098,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 0.86,
      "y": -0.5,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -1.159,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        19
      ]
    },
    {
      "id": 27,
      "x": -0.56,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        19,
        20
      ]
    },
    {
      "id": 28,
      "x": 0.559,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        24,
        37
      ]
    },
    {
      "id": 29,
      "x": -1.159,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 30,
      "x": -0.56,
      "y": -0.259,
      "layer": 4,
      "closed_by": [
        21,
        22
      ]
    },
    {
      "id": 31,
      "x": 1.159,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        25
      ]
    },
    {
      "id": 32,
      "x": 0.559,
      "y": -0.259,
      "layer": 4,
      "closed_by": [
        23,
        25
      ]
    },
    {
      "id": 33,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        20,
        37
      ]
    },
    {
      "id": 34,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        22,
        23
      ]
    },
    {
      "id": 35,
      "x": -1.378,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        26,
        29
      ]
    },
    {
      "id": 36,
      "x": 1.159,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 37,
      "x": 0.337,
      "y": 1.098,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -0.859,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        26,
        27,
        29,
        30
      ]
    },
    {
      "id": 39,
      "x": -0.337,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        27,
        30,
        33,
        34
      ]
    },
    {
      "id": 40,
      "x": -1.159,
      "y": -0.017,
      "layer": 2,
      "closed_by": [
        35,
        38
      ]
    },
    {
      "id": 41,
      "x": -0.56,
      "y": -0.017,
      "layer": 2,
      "closed_by": [
        38,
        39
      ]
    },
    {
      "id": 42,
      "x": -1.159,
      "y": 0.62,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 43,
      "x": -0.56,
      "y": 0.62,
      "layer": 1,
      "closed_by": [
        41
      ]
    },
    {
      "id": 44,
      "x": 1.378,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        31,
        36
      ]
    },
    {
      "id": 45,
      "x": 0.337,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        28,
        32,
        33,
        34
      ]
    },
    {
      "id": 46,
      "x": 1.159,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        44,
        50
      ]
    },
    {
      "id": 47,
      "x": 0.559,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        45,
        50
      ]
    },
    {
      "id": 48,
      "x": 1.159,
      "y": -0.017,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 49,
      "x": 0.559,
      "y": -0.017,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 50,
      "x": 0.86,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        28,
        31,
        32,
        36
      ]
    }
  ],
  "cards_in_layout_amount": 32,
  "cards_in_deck_amount": 28,
  "layers_in_level": 5,
  "open_cards": 8,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 33.4,
    "star2": 64.0,
    "star3": 2.1
  }
}