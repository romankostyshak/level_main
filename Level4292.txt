{
  "layout_cards": [
    {
      "id": 22,
      "x": -1.498,
      "y": -0.418,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 1.539,
      "y": 0.777,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 24,
      "x": -1.498,
      "y": -0.017,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 25,
      "x": 1.539,
      "y": 0.379,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 26,
      "x": -1.498,
      "y": 0.379,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 27,
      "x": 1.539,
      "y": -0.017,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 28,
      "x": 1.539,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        27
      ]
    },
    {
      "id": 29,
      "x": -0.66,
      "y": 0.34,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 0.62,
      "y": -0.136,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -0.259,
      "y": -0.298,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 32,
      "x": -0.859,
      "y": 0.578,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 33,
      "x": -0.259,
      "y": 0.578,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 34,
      "x": 0.939,
      "y": 0.578,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 35,
      "x": 0.337,
      "y": 0.578,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 36,
      "x": 0.939,
      "y": -0.298,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 37,
      "x": 0.337,
      "y": -0.298,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 38,
      "x": -0.859,
      "y": -0.298,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 39,
      "x": -1.498,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        26
      ]
    },
    {
      "id": 41,
      "x": 0.898,
      "y": 0.537,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 42,
      "x": -0.298,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 43,
      "x": -0.62,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        32,
        33,
        38,
        42
      ]
    },
    {
      "id": 44,
      "x": 0.578,
      "y": -0.1,
      "layer": 2,
      "closed_by": [
        35,
        36,
        37,
        41
      ]
    },
    {
      "id": 45,
      "x": -0.898,
      "y": 0.537,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 46,
      "x": -0.298,
      "y": 0.537,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 47,
      "x": 0.898,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 48,
      "x": 0.298,
      "y": 0.537,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 49,
      "x": 0.298,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 50,
      "x": -0.898,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        43
      ]
    }
  ],
  "cards_in_layout_amount": 28,
  "cards_in_deck_amount": 26,
  "layers_in_level": 5,
  "open_cards": 4,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 36.7,
    "star2": 58.4,
    "star3": 4.2
  }
}