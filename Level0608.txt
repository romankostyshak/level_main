{
  "layout_cards": [
    {
      "id": 51,
      "x": -1.585,
      "y": -0.179,
      "layer": 5,
      "closed_by": [
        39
      ]
    },
    {
      "id": 50,
      "x": -1.585,
      "y": 0.785,
      "layer": 5,
      "closed_by": [
        40
      ]
    },
    {
      "id": 49,
      "x": 1.605,
      "y": -0.178,
      "layer": 5,
      "closed_by": [
        41
      ]
    },
    {
      "id": 48,
      "x": -1.09,
      "y": 0.305,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 47,
      "x": -0.828,
      "y": 0.305,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 46,
      "x": 0.837,
      "y": 0.303,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 45,
      "x": 0.569,
      "y": 0.303,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 44,
      "x": -0.568,
      "y": 0.303,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 43,
      "x": 1.605,
      "y": 0.781,
      "layer": 5,
      "closed_by": [
        42
      ]
    },
    {
      "id": 42,
      "x": 1.718,
      "y": 0.782,
      "layer": 6,
      "closed_by": [
        38
      ]
    },
    {
      "id": 41,
      "x": 1.718,
      "y": -0.178,
      "layer": 6,
      "closed_by": [
        35
      ]
    },
    {
      "id": 40,
      "x": -1.69,
      "y": 0.786,
      "layer": 6,
      "closed_by": [
        37
      ]
    },
    {
      "id": 39,
      "x": -1.692,
      "y": -0.179,
      "layer": 6,
      "closed_by": [
        36
      ]
    },
    {
      "id": 38,
      "x": 1.883,
      "y": 0.712,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 37,
      "x": -1.858,
      "y": 0.712,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -1.86,
      "y": -0.108,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 1.883,
      "y": -0.108,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -1.353,
      "y": 0.305,
      "layer": 4,
      "closed_by": [
        51,
        50
      ],
      "card_type": 6
    },
    {
      "id": 33,
      "x": 1.105,
      "y": 0.303,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 32,
      "x": 1.373,
      "y": 0.305,
      "layer": 4,
      "closed_by": [
        49,
        43
      ],
      "card_type": 6
    }
  ],
  "cards_in_layout_amount": 20,
  "cards_in_deck_amount": 22,
  "stars_statistic": {
    "star1": 7.2,
    "star2": 0.0,
    "star3": 92.8
  }
}