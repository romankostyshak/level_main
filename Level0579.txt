{
  "layout_cards": [
    {
      "id": 51,
      "y": 0.224,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        49,
        42
      ]
    },
    {
      "id": 50,
      "x": 0.451,
      "y": 0.493,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 49,
      "x": 0.272,
      "y": 0.446,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 48,
      "x": -1.478,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 47,
      "x": 1.48,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 46,
      "x": 1.764,
      "y": -0.155,
      "angle": 20.0,
      "layer": 1,
      "closed_by": [
        34
      ]
    },
    {
      "id": 45,
      "x": -1.773,
      "y": 0.759,
      "angle": 20.0,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 44,
      "x": -1.773,
      "y": -0.157,
      "angle": 340.0,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 43,
      "x": -0.458,
      "y": -0.037,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 42,
      "x": -0.27,
      "y": 0.008,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 41,
      "x": 0.273,
      "y": 0.698,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 40,
      "x": -0.27,
      "y": -0.254,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 39,
      "x": -0.27,
      "y": -0.381,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 38,
      "x": 0.275,
      "y": 0.824,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 37,
      "x": 0.273,
      "y": 0.939,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 1.212,
      "y": 0.298,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 1.74,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 33,
      "x": 1.764,
      "y": 0.757,
      "angle": 340.0,
      "layer": 1,
      "closed_by": [
        34
      ]
    },
    {
      "id": 32,
      "x": -1.213,
      "y": 0.298,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -1.741,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 36,
      "x": -0.27,
      "y": -0.493,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 21,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 39.4,
    "star2": 1.8,
    "star3": 58.3
  }
}