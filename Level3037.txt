{
  "layout_cards": [
    {
      "id": 14,
      "x": -2.065,
      "y": 1.019,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 15,
      "x": 2.065,
      "y": -0.574,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 18,
      "x": -2.065,
      "y": 0.379,
      "layer": 4,
      "closed_by": [
        14
      ]
    },
    {
      "id": 19,
      "x": 2.065,
      "y": 0.064,
      "layer": 4,
      "closed_by": [
        15
      ]
    },
    {
      "id": 20,
      "x": -2.065,
      "y": -0.256,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 21,
      "x": -1.745,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 22,
      "x": -1.746,
      "y": 0.46,
      "layer": 2,
      "closed_by": [
        20,
        21
      ]
    },
    {
      "id": 23,
      "x": -1.745,
      "y": -0.256,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 24,
      "x": 1.746,
      "y": -0.573,
      "layer": 3,
      "closed_by": [
        19
      ]
    },
    {
      "id": 25,
      "x": 1.745,
      "y": 0.064,
      "layer": 2,
      "closed_by": [
        24,
        39
      ]
    },
    {
      "id": 26,
      "x": 1.746,
      "y": 0.698,
      "layer": 1,
      "closed_by": [
        25
      ]
    },
    {
      "id": 27,
      "x": -1.187,
      "y": -0.252,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 28,
      "x": -1.187,
      "y": 0.462,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 29,
      "x": 0.865,
      "y": -0.577,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 30,
      "x": -1.184,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 31,
      "x": 1.184,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 32,
      "x": -0.865,
      "y": -0.252,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 33,
      "x": -0.865,
      "y": 0.462,
      "layer": 2,
      "closed_by": [
        30,
        32
      ]
    },
    {
      "id": 34,
      "x": -0.865,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 35,
      "x": 0.865,
      "y": 0.698,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 36,
      "x": 0.865,
      "y": 0.059,
      "layer": 2,
      "closed_by": [
        31,
        35
      ]
    },
    {
      "id": 37,
      "x": 1.184,
      "y": 0.061,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 38,
      "x": 1.184,
      "y": 0.699,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 39,
      "x": 2.065,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        19
      ]
    },
    {
      "id": 40,
      "x": 0.001,
      "y": 0.222,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 41,
      "y": 0.638,
      "layer": 5,
      "closed_by": [
        40
      ]
    },
    {
      "id": 42,
      "y": -0.187,
      "layer": 5,
      "closed_by": [
        40
      ]
    },
    {
      "id": 43,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 44,
      "x": -0.001,
      "y": 1.018,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 45,
      "x": -0.333,
      "y": 0.384,
      "layer": 2,
      "closed_by": [
        44,
        49
      ]
    },
    {
      "id": 46,
      "x": -0.333,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 47,
      "x": 0.335,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 48,
      "x": 0.333,
      "y": 0.061,
      "layer": 2,
      "closed_by": [
        41,
        43,
        47
      ]
    },
    {
      "id": 49,
      "x": -0.333,
      "y": -0.252,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 51,
      "x": 0.333,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        48
      ]
    }
  ],
  "cards_in_layout_amount": 35,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 16.9,
    "star2": 70.5,
    "star3": 12.0
  }
}