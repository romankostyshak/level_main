{
  "layout_cards": [
    {
      "x": -1.21,
      "y": 0.633,
      "layer": 4,
      "closed_by": [
        1
      ]
    },
    {
      "id": 1,
      "x": -1.052,
      "y": 0.939,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 2,
      "x": -1.985,
      "y": 0.939,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 3,
      "x": -1.822,
      "y": 0.632,
      "layer": 4,
      "closed_by": [
        2
      ]
    },
    {
      "id": 4,
      "x": -1.521,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        5
      ]
    },
    {
      "id": 5,
      "x": -1.524,
      "y": 0.632,
      "layer": 2,
      "closed_by": [
        7,
        9
      ]
    },
    {
      "id": 6,
      "x": -1.052,
      "y": -0.495,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 7,
      "x": -1.824,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        3,
        12
      ]
    },
    {
      "id": 8,
      "x": -1.985,
      "y": -0.497,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 9,
      "x": -1.212,
      "y": 0.221,
      "layer": 3,
      "closed_by": [
        0,
        13
      ]
    },
    {
      "id": 10,
      "x": -1.524,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        11
      ]
    },
    {
      "id": 11,
      "x": -1.523,
      "y": -0.187,
      "layer": 2,
      "closed_by": [
        7,
        9
      ]
    },
    {
      "id": 12,
      "x": -1.822,
      "y": -0.187,
      "layer": 4,
      "closed_by": [
        8
      ]
    },
    {
      "id": 13,
      "x": -1.213,
      "y": -0.187,
      "layer": 4,
      "closed_by": [
        6
      ]
    },
    {
      "id": 14,
      "x": 0.513,
      "y": 0.782,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 15,
      "x": 0.518,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 16,
      "x": 0.001,
      "y": -0.34,
      "layer": 2,
      "closed_by": [
        18
      ]
    },
    {
      "id": 17,
      "x": -0.523,
      "y": 0.785,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 18,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 19,
      "x": 0.518,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 20,
      "x": -0.527,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 21,
      "y": 0.782,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 22,
      "x": 0.513,
      "y": 0.864,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 23,
      "x": -0.526,
      "y": 0.864,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 24,
      "x": -0.526,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 25,
      "y": -0.574,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 26,
      "x": 0.513,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 27,
      "x": -0.527,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 28,
      "x": -0.001,
      "y": 1.021,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 29,
      "x": -0.527,
      "y": -0.5,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 0.521,
      "y": -0.574,
      "layer": 4,
      "closed_by": [
        32
      ]
    },
    {
      "id": 31,
      "x": 0.001,
      "y": 0.864,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 32,
      "x": 0.521,
      "y": -0.5,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 33,
      "y": -0.5,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 0.517,
      "y": 0.939,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 1.052,
      "y": -0.493,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -0.527,
      "y": 0.938,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 37,
      "x": -0.001,
      "y": 0.939,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 1.985,
      "y": -0.495,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 39,
      "x": 1.983,
      "y": 0.939,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 1.052,
      "y": 0.939,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 1.213,
      "y": -0.187,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 42,
      "x": 1.822,
      "y": -0.187,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 43,
      "x": 1.213,
      "y": 0.632,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 44,
      "x": 1.825,
      "y": 0.633,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 45,
      "x": 1.825,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        42,
        44
      ]
    },
    {
      "id": 46,
      "x": 1.511,
      "y": 0.632,
      "layer": 2,
      "closed_by": [
        45,
        50
      ]
    },
    {
      "id": 47,
      "x": 1.513,
      "y": -0.187,
      "layer": 2,
      "closed_by": [
        45,
        50
      ]
    },
    {
      "id": 48,
      "x": 1.513,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 49,
      "x": 1.514,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        43,
        46
      ]
    },
    {
      "id": 50,
      "x": 1.213,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        41,
        43
      ]
    },
    {
      "id": 51,
      "x": -0.527,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        20
      ]
    }
  ],
  "cards_in_layout_amount": 52,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 0.0,
    "star2": 0.4,
    "star3": 98.7
  }
}