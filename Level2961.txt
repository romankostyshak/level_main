{
  "layout_cards": [
    {
      "id": 16,
      "x": -0.298,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 17,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        18,
        19
      ],
      "card_type": 3
    },
    {
      "id": 18,
      "x": 0.388,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        21
      ]
    },
    {
      "id": 19,
      "x": -0.384,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        16
      ]
    },
    {
      "id": 20,
      "x": -0.386,
      "y": -0.256,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 21,
      "x": 0.3,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 22,
      "x": 0.307,
      "y": 0.017,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 23,
      "x": -0.303,
      "y": 0.017,
      "layer": 1,
      "closed_by": [
        24
      ]
    },
    {
      "id": 24,
      "x": -0.3,
      "y": -0.17,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 25,
      "x": 0.416,
      "y": -0.254,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 26,
      "x": 1.052,
      "y": -0.578,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 27,
      "x": -1.054,
      "y": -0.578,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 1.373,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        26,
        32
      ]
    },
    {
      "id": 29,
      "x": 0.31,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 30,
      "x": -2.335,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 31,
      "x": -1.371,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        27,
        33
      ]
    },
    {
      "id": 32,
      "x": 1.61,
      "y": -0.577,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 33,
      "x": -1.613,
      "y": -0.574,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 34,
      "x": 1.855,
      "y": -0.418,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -1.848,
      "y": -0.412,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 2.131,
      "y": -0.577,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 37,
      "x": -2.125,
      "y": -0.573,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 38,
      "x": 2.335,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 39,
      "x": 2.174,
      "y": 0.059,
      "layer": 2,
      "closed_by": [
        38,
        47
      ]
    },
    {
      "id": 40,
      "x": -2.176,
      "y": 0.063,
      "layer": 2,
      "closed_by": [
        30,
        46
      ]
    },
    {
      "id": 41,
      "x": 2.322,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 42,
      "x": -2.335,
      "y": 0.222,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 43,
      "x": 0.259,
      "y": 0.859,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 44,
      "x": -0.259,
      "y": 0.859,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 45,
      "x": 0.002,
      "y": -0.418,
      "layer": 4,
      "closed_by": [],
      "card_type": 2
    },
    {
      "id": 46,
      "x": -2.338,
      "y": 0.46,
      "layer": 3,
      "closed_by": [
        49
      ]
    },
    {
      "id": 47,
      "x": 2.325,
      "y": 0.462,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 48,
      "x": 2.173,
      "y": 0.465,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 49,
      "x": -2.17,
      "y": 0.46,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 50,
      "x": -0.814,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        20,
        27
      ]
    },
    {
      "id": 51,
      "x": 0.814,
      "y": -0.416,
      "layer": 2,
      "closed_by": [
        25,
        26
      ]
    }
  ],
  "cards_in_layout_amount": 36,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 18.8,
    "star2": 70.9,
    "star3": 9.5
  }
}