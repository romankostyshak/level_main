{
  "layout_cards": [
    {
      "id": 29,
      "x": 0.259,
      "y": 0.256,
      "angle": 30.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -0.18,
      "y": 0.216,
      "angle": 30.0,
      "layer": 5,
      "closed_by": [
        29
      ]
    },
    {
      "id": 31,
      "x": -0.18,
      "y": 0.615,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 32,
      "x": 0.259,
      "y": -0.141,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 33,
      "x": -0.18,
      "y": 1.018,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 34,
      "x": 0.259,
      "y": -0.541,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        29,
        32
      ]
    },
    {
      "id": 35,
      "x": -0.62,
      "y": 0.878,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 38,
      "x": 0.017,
      "y": 0.958,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 39,
      "x": 0.66,
      "y": 0.958,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 40,
      "x": -0.578,
      "y": -0.481,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 0.059,
      "y": -0.481,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 42,
      "x": 0.699,
      "y": -0.4,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 43,
      "x": -0.259,
      "y": 0.796,
      "layer": 1,
      "closed_by": [
        35,
        38
      ]
    },
    {
      "id": 44,
      "x": 0.337,
      "y": 0.796,
      "layer": 1,
      "closed_by": [
        29,
        38,
        39
      ]
    },
    {
      "id": 45,
      "x": 0.898,
      "y": 0.578,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 46,
      "x": -0.819,
      "y": -0.101,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 47,
      "x": -0.259,
      "y": -0.321,
      "layer": 1,
      "closed_by": [
        40,
        41
      ]
    },
    {
      "id": 48,
      "x": 0.337,
      "y": -0.321,
      "layer": 1,
      "closed_by": [
        41,
        42
      ]
    },
    {
      "id": 49,
      "x": 0.898,
      "y": -0.238,
      "layer": 1,
      "closed_by": [
        29,
        32,
        42
      ]
    },
    {
      "id": 50,
      "x": -0.819,
      "y": 0.717,
      "layer": 1,
      "closed_by": [
        31,
        35
      ]
    }
  ],
  "cards_in_layout_amount": 20,
  "cards_in_deck_amount": 24,
  "layers_in_level": 6,
  "open_cards": 3,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 72.6,
    "star2": 24.4,
    "star3": 2.6
  }
}