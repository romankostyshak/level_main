{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.001,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        48,
        47
      ]
    },
    {
      "id": 50,
      "x": 0.574,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        47,
        30
      ]
    },
    {
      "id": 49,
      "x": -0.574,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        48,
        26
      ]
    },
    {
      "id": 48,
      "x": -0.303,
      "y": 0.469,
      "layer": 2,
      "closed_by": [
        38,
        27
      ]
    },
    {
      "id": 47,
      "x": 0.305,
      "y": 0.465,
      "layer": 2,
      "closed_by": [
        37,
        28
      ]
    },
    {
      "id": 42,
      "x": -1.771,
      "y": -0.016,
      "layer": 3,
      "closed_by": [
        23,
        43
      ]
    },
    {
      "id": 41,
      "x": -1.452,
      "y": 0.462,
      "layer": 2,
      "closed_by": [
        42,
        35,
        25
      ]
    },
    {
      "id": 40,
      "x": 1.133,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        32,
        30
      ]
    },
    {
      "id": 39,
      "x": -1.133,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        41,
        26
      ]
    },
    {
      "id": 38,
      "x": -0.347,
      "y": 0.865,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 37,
      "x": 0.344,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 36,
      "x": 1.093,
      "y": 0.861,
      "layer": 3,
      "closed_by": [
        34,
        24
      ]
    },
    {
      "id": 35,
      "x": -1.09,
      "y": 0.884,
      "layer": 3,
      "closed_by": [
        33,
        23
      ]
    },
    {
      "id": 34,
      "x": 0.74,
      "y": 0.972,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -0.726,
      "y": 0.976,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 1.46,
      "y": 0.46,
      "layer": 2,
      "closed_by": [
        36,
        31,
        29
      ]
    },
    {
      "id": 31,
      "x": 1.773,
      "y": -0.017,
      "layer": 3,
      "closed_by": [
        24,
        44
      ]
    },
    {
      "id": 30,
      "x": 0.893,
      "y": 0.462,
      "layer": 2,
      "closed_by": [
        36,
        29,
        28
      ]
    },
    {
      "id": 29,
      "x": 1.179,
      "y": -0.017,
      "layer": 3,
      "closed_by": [
        24,
        45,
        44
      ]
    },
    {
      "id": 28,
      "x": 0.572,
      "y": -0.017,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 26,
      "x": -0.893,
      "y": 0.467,
      "layer": 2,
      "closed_by": [
        35,
        27,
        25
      ]
    },
    {
      "id": 24,
      "x": 1.35,
      "y": 0.634,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 23,
      "x": -1.343,
      "y": 0.62,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 27,
      "x": -0.573,
      "y": -0.017,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 43,
      "x": -1.452,
      "y": -0.418,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 46,
      "x": -0.893,
      "y": -0.418,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 45,
      "x": 0.888,
      "y": -0.418,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 44,
      "x": 1.452,
      "y": -0.418,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 25,
      "x": -1.184,
      "y": -0.017,
      "layer": 3,
      "closed_by": [
        23,
        43,
        46
      ]
    }
  ],
  "cards_in_layout_amount": 29,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 56.8,
    "star2": 28.9,
    "star3": 13.8
  }
}