{
  "layout_cards": [
    {
      "id": 5,
      "x": 0.374,
      "y": -0.577,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 6,
      "x": -0.171,
      "y": -0.574,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 7,
      "x": 1.475,
      "y": -0.574,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 8,
      "x": 0.921,
      "y": -0.574,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 9,
      "x": -0.735,
      "y": -0.578,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 10,
      "x": 0.104,
      "y": -0.414,
      "layer": 4,
      "closed_by": [
        5,
        6
      ]
    },
    {
      "id": 11,
      "x": -0.414,
      "y": -0.412,
      "layer": 4,
      "closed_by": [
        6,
        9
      ]
    },
    {
      "id": 12,
      "x": 1.159,
      "y": -0.416,
      "layer": 4,
      "closed_by": [
        7,
        8
      ]
    },
    {
      "id": 13,
      "x": -0.972,
      "y": -0.416,
      "layer": 4,
      "closed_by": [
        9
      ]
    },
    {
      "id": 14,
      "x": -0.726,
      "y": -0.416,
      "layer": 3,
      "closed_by": [
        11,
        13
      ]
    },
    {
      "id": 15,
      "x": 1.472,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        12,
        17
      ]
    },
    {
      "id": 16,
      "x": -0.172,
      "y": -0.414,
      "layer": 3,
      "closed_by": [
        10,
        11
      ]
    },
    {
      "id": 17,
      "x": 1.72,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        7
      ]
    },
    {
      "id": 18,
      "x": 0.638,
      "y": -0.414,
      "layer": 4,
      "closed_by": [
        5,
        8
      ]
    },
    {
      "id": 19,
      "x": 0.916,
      "y": -0.414,
      "layer": 3,
      "closed_by": [
        12,
        18
      ]
    },
    {
      "id": 20,
      "x": 0.37,
      "y": -0.414,
      "layer": 3,
      "closed_by": [
        10,
        18
      ]
    },
    {
      "id": 21,
      "x": 0.643,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        19,
        20
      ]
    },
    {
      "id": 22,
      "x": 1.159,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        15,
        19
      ]
    },
    {
      "id": 23,
      "x": -0.412,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        14,
        16
      ]
    },
    {
      "id": 24,
      "x": -0.971,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        14
      ]
    },
    {
      "id": 25,
      "x": 1.72,
      "y": -0.178,
      "layer": 2,
      "closed_by": [
        15
      ]
    },
    {
      "id": 26,
      "x": 1.506,
      "y": -0.097,
      "layer": 1,
      "closed_by": [
        22,
        25
      ]
    },
    {
      "id": 27,
      "x": 0.921,
      "y": -0.096,
      "layer": 1,
      "closed_by": [
        21,
        22
      ]
    },
    {
      "id": 28,
      "x": 0.372,
      "y": -0.097,
      "layer": 1,
      "closed_by": [
        21,
        38
      ]
    },
    {
      "id": 29,
      "x": -0.759,
      "y": -0.093,
      "layer": 1,
      "closed_by": [
        23,
        24
      ]
    },
    {
      "id": 30,
      "x": -1.554,
      "y": -0.307,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -0.851,
      "y": 0.819,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -1.603,
      "y": -0.256,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 33,
      "x": -0.898,
      "y": 0.87,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 34,
      "x": -1.649,
      "y": -0.202,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 35,
      "x": -0.944,
      "y": 0.921,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 36,
      "x": -1.7,
      "y": -0.15,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 37,
      "x": -0.172,
      "y": -0.093,
      "layer": 1,
      "closed_by": [
        23,
        38
      ]
    },
    {
      "id": 38,
      "x": 0.107,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        16,
        20
      ]
    },
    {
      "id": 39,
      "x": -0.99,
      "y": 0.972,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 40,
      "x": -1.034,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 41,
      "x": -1.746,
      "y": -0.1,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 42,
      "x": -1.746,
      "y": 1.018,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 43,
      "x": -1.699,
      "y": 0.967,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 44,
      "x": -0.328,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 45,
      "x": -1.651,
      "y": 0.916,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 46,
      "x": -1.605,
      "y": 0.864,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 47,
      "x": -0.136,
      "y": 0.819,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 48,
      "x": -0.185,
      "y": 0.864,
      "layer": 4,
      "closed_by": [
        47
      ]
    },
    {
      "id": 49,
      "x": -0.231,
      "y": 0.916,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 50,
      "x": -0.282,
      "y": 0.962,
      "layer": 2,
      "closed_by": [
        49
      ]
    },
    {
      "id": 51,
      "x": -1.557,
      "y": 0.81,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 47,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 0.1,
    "star2": 19.7,
    "star3": 79.7
  }
}