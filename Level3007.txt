{
  "layout_cards": [
    {
      "id": 12,
      "x": 0.115,
      "y": 0.981,
      "angle": 315.0,
      "layer": 1,
      "closed_by": [
        15
      ]
    },
    {
      "id": 13,
      "x": 0.115,
      "y": -0.303,
      "angle": 315.0,
      "layer": 1,
      "closed_by": [
        14
      ]
    },
    {
      "id": 14,
      "x": -0.092,
      "y": -0.298,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        17
      ]
    },
    {
      "id": 15,
      "x": -0.093,
      "y": 0.99,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        16
      ]
    },
    {
      "id": 16,
      "y": 1.024,
      "layer": 3,
      "closed_by": [
        24,
        27
      ]
    },
    {
      "id": 17,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        28,
        29
      ]
    },
    {
      "id": 18,
      "x": 0.716,
      "y": 0.279,
      "angle": 315.0,
      "layer": 1,
      "closed_by": [
        20
      ]
    },
    {
      "id": 19,
      "x": -0.699,
      "y": 0.291,
      "angle": 45.0,
      "layer": 1,
      "closed_by": [
        21
      ]
    },
    {
      "id": 20,
      "x": 0.712,
      "y": 0.495,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 21,
      "x": -0.698,
      "y": 0.497,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 22,
      "x": 0.726,
      "y": 0.379,
      "layer": 3,
      "closed_by": [
        24,
        28
      ]
    },
    {
      "id": 23,
      "x": -0.722,
      "y": 0.379,
      "layer": 3,
      "closed_by": [
        27,
        29
      ]
    },
    {
      "id": 24,
      "x": 0.486,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 25,
      "x": 1.768,
      "y": 0.805,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 26,
      "y": -0.089,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 27,
      "x": -0.483,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        32
      ]
    },
    {
      "id": 28,
      "x": 0.486,
      "y": -0.172,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 29,
      "x": -0.479,
      "y": -0.177,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 30,
      "x": 0.541,
      "y": -0.071,
      "angle": 315.0,
      "layer": 5,
      "closed_by": [
        26
      ]
    },
    {
      "id": 31,
      "x": 0.537,
      "y": 0.828,
      "angle": 45.0,
      "layer": 5,
      "closed_by": [
        34
      ]
    },
    {
      "id": 32,
      "x": -0.536,
      "y": 0.828,
      "angle": 315.0,
      "layer": 5,
      "closed_by": [
        34
      ]
    },
    {
      "id": 33,
      "x": -0.537,
      "y": -0.071,
      "angle": 45.0,
      "layer": 5,
      "closed_by": [
        26
      ]
    },
    {
      "id": 34,
      "y": 0.865,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -1.61,
      "y": 0.377,
      "layer": 1,
      "closed_by": [
        39,
        40
      ]
    },
    {
      "id": 36,
      "x": 1.47,
      "y": 0.837,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 37,
      "x": 1.61,
      "y": 0.381,
      "layer": 1,
      "closed_by": [
        36,
        38
      ]
    },
    {
      "id": 38,
      "x": 1.475,
      "y": -0.074,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 39,
      "x": -1.473,
      "y": 0.837,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 40,
      "x": -1.478,
      "y": -0.075,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 41,
      "x": 1.774,
      "y": -0.034,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 42,
      "x": -1.769,
      "y": 0.805,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 43,
      "x": -1.774,
      "y": -0.041,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 44,
      "x": 1.865,
      "y": -0.074,
      "angle": 315.0,
      "layer": 4,
      "closed_by": [
        49
      ]
    },
    {
      "id": 45,
      "x": -2.065,
      "y": 0.381,
      "layer": 5,
      "closed_by": [
        51
      ]
    },
    {
      "id": 46,
      "x": 1.866,
      "y": 0.85,
      "angle": 45.0,
      "layer": 4,
      "closed_by": [
        49
      ]
    },
    {
      "id": 47,
      "x": -1.883,
      "y": -0.074,
      "angle": 45.0,
      "layer": 4,
      "closed_by": [
        45
      ]
    },
    {
      "id": 48,
      "x": -1.883,
      "y": 0.837,
      "angle": 315.0,
      "layer": 4,
      "closed_by": [
        45
      ]
    },
    {
      "id": 49,
      "x": 2.065,
      "y": 0.388,
      "layer": 5,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": 2.305,
      "y": 0.388,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -2.305,
      "y": 0.381,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 40,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 3.9,
    "star2": 54.9,
    "star3": 40.5
  }
}