{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.298,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 50,
      "x": 0.546,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 49,
      "x": -0.298,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        48
      ]
    },
    {
      "id": 48,
      "x": -0.545,
      "y": -0.578,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 47,
      "x": -0.545,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        49
      ]
    },
    {
      "id": 46,
      "x": -0.298,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 45,
      "x": 0.298,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 44,
      "x": 0.298,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 43,
      "x": 0.546,
      "y": -0.578,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 42,
      "y": 0.136,
      "layer": 1,
      "closed_by": [
        51,
        49,
        46,
        45,
        44,
        33
      ]
    },
    {
      "id": 41,
      "x": 1.098,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 40,
      "x": 1.338,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 39,
      "x": -1.338,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        20
      ]
    },
    {
      "id": 38,
      "x": 1.338,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 37,
      "x": -1.338,
      "y": -0.259,
      "layer": 4,
      "closed_by": [
        17
      ]
    },
    {
      "id": 36,
      "x": -0.545,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 35,
      "x": -0.545,
      "y": 0.62,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -0.298,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 33,
      "x": -0.298,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 32,
      "x": 0.298,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        51
      ]
    },
    {
      "id": 31,
      "x": 0.546,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 30,
      "x": 0.546,
      "y": 0.62,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -1.743,
      "y": -0.578,
      "layer": 6,
      "closed_by": [
        18
      ]
    },
    {
      "id": 28,
      "x": -1.743,
      "y": 1.019,
      "layer": 6,
      "closed_by": [
        27
      ]
    },
    {
      "id": 27,
      "x": -1.743,
      "y": 0.62,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -1.139,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 24,
      "x": 1.577,
      "y": 0.777,
      "layer": 5,
      "closed_by": [
        25
      ]
    },
    {
      "id": 22,
      "x": 1.098,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        41
      ]
    },
    {
      "id": 21,
      "x": -1.139,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        26
      ]
    },
    {
      "id": 20,
      "x": -1.338,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        19
      ]
    },
    {
      "id": 18,
      "x": -1.743,
      "y": -0.259,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 17,
      "x": -1.58,
      "y": -0.418,
      "layer": 5,
      "closed_by": [
        29
      ]
    },
    {
      "id": 16,
      "x": -1.338,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 15,
      "x": -1.1,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        16
      ]
    },
    {
      "id": 14,
      "x": -1.1,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        15
      ]
    },
    {
      "id": 13,
      "x": 1.098,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        10
      ]
    },
    {
      "id": 12,
      "x": 1.098,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        13
      ]
    },
    {
      "id": 11,
      "x": 1.338,
      "y": -0.259,
      "layer": 4,
      "closed_by": [
        9
      ]
    },
    {
      "id": 10,
      "x": 1.338,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        11
      ]
    },
    {
      "id": 9,
      "x": 1.577,
      "y": -0.418,
      "layer": 5,
      "closed_by": [
        7
      ]
    },
    {
      "id": 8,
      "x": 1.745,
      "y": -0.259,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 7,
      "x": 1.745,
      "y": -0.578,
      "layer": 6,
      "closed_by": [
        8
      ]
    },
    {
      "id": 23,
      "x": 1.746,
      "y": 0.62,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 1.746,
      "y": 1.019,
      "layer": 6,
      "closed_by": [
        23
      ]
    },
    {
      "id": 19,
      "x": -1.58,
      "y": 0.777,
      "layer": 5,
      "closed_by": [
        28
      ]
    }
  ],
  "cards_in_layout_amount": 45,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 0.1,
    "star2": 19.4,
    "star3": 79.7
  }
}