{
  "layout_cards": [
    {
      "id": 10,
      "x": 1.059,
      "y": 0.279,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 11,
      "x": 0.5,
      "y": 0.279,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 12,
      "x": -1.059,
      "y": 0.279,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 13,
      "x": -2.18,
      "y": 0.679,
      "layer": 4,
      "closed_by": [
        14
      ]
    },
    {
      "id": 14,
      "x": -2.18,
      "y": 0.079,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 15,
      "x": 2.177,
      "y": 0.079,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 18,
      "x": 2.177,
      "y": 0.679,
      "layer": 4,
      "closed_by": [
        15
      ]
    },
    {
      "id": 19,
      "x": -1.779,
      "y": 0.079,
      "layer": 3,
      "closed_by": [
        13
      ]
    },
    {
      "id": 20,
      "x": 1.779,
      "y": 0.079,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 21,
      "x": 1.779,
      "y": 0.679,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 22,
      "x": -1.978,
      "y": 1.019,
      "angle": 300.0,
      "layer": 1,
      "closed_by": [
        37
      ]
    },
    {
      "id": 23,
      "x": 1.98,
      "y": -0.259,
      "angle": 60.0,
      "layer": 1,
      "closed_by": [
        15,
        20
      ]
    },
    {
      "id": 24,
      "x": -1.978,
      "y": -0.259,
      "angle": 300.0,
      "layer": 1,
      "closed_by": [
        14,
        19
      ]
    },
    {
      "id": 25,
      "x": 1.98,
      "y": 1.019,
      "angle": 60.0,
      "layer": 1,
      "closed_by": [
        18,
        21
      ]
    },
    {
      "id": 26,
      "x": -1.22,
      "y": 0.136,
      "layer": 1,
      "closed_by": [
        28,
        36
      ]
    },
    {
      "id": 27,
      "x": -1.22,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 28,
      "x": -1.139,
      "y": -0.017,
      "layer": 2,
      "closed_by": [
        33,
        34
      ]
    },
    {
      "id": 29,
      "x": 1.22,
      "y": 0.136,
      "layer": 1,
      "closed_by": [
        31,
        32
      ]
    },
    {
      "id": 30,
      "x": 1.22,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 31,
      "x": 1.139,
      "y": -0.017,
      "layer": 2,
      "closed_by": [
        35,
        38
      ]
    },
    {
      "id": 32,
      "x": 1.139,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 33,
      "x": -1.059,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        12
      ]
    },
    {
      "id": 34,
      "x": -1.059,
      "y": -0.179,
      "layer": 3,
      "closed_by": [
        12
      ]
    },
    {
      "id": 35,
      "x": 1.059,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        10
      ]
    },
    {
      "id": 36,
      "x": -1.139,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 37,
      "x": -1.779,
      "y": 0.679,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 38,
      "x": 1.059,
      "y": -0.179,
      "layer": 3,
      "closed_by": [
        10
      ]
    },
    {
      "id": 39,
      "x": -0.499,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        51
      ]
    },
    {
      "id": 40,
      "x": -0.499,
      "y": -0.179,
      "layer": 3,
      "closed_by": [
        51
      ]
    },
    {
      "id": 41,
      "x": 0.5,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        11
      ]
    },
    {
      "id": 42,
      "x": 0.5,
      "y": -0.179,
      "layer": 3,
      "closed_by": [
        11
      ]
    },
    {
      "id": 43,
      "x": -0.418,
      "y": 0.537,
      "layer": 2,
      "closed_by": [
        39,
        40
      ]
    },
    {
      "id": 44,
      "x": -0.418,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 45,
      "x": 0.418,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 46,
      "x": -0.337,
      "y": 0.379,
      "layer": 1,
      "closed_by": [
        43,
        44
      ]
    },
    {
      "id": 47,
      "x": -0.337,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 48,
      "x": 0.337,
      "y": 0.379,
      "layer": 1,
      "closed_by": [
        45,
        50
      ]
    },
    {
      "id": 49,
      "x": 0.337,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 50,
      "x": 0.418,
      "y": 0.537,
      "layer": 2,
      "closed_by": [
        41,
        42
      ]
    },
    {
      "id": 51,
      "x": -0.499,
      "y": 0.279,
      "layer": 4,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 40,
  "cards_in_deck_amount": 26,
  "layers_in_level": 5,
  "open_cards": 6,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 2.9,
    "star2": 76.2,
    "star3": 20.9
  }
}