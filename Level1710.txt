{
  "layout_cards": [
    {
      "id": 45,
      "x": 0.948,
      "y": 1.047,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 51,
      "x": 0.331,
      "y": 1.047,
      "layer": 1,
      "closed_by": [
        46,
        44
      ]
    },
    {
      "id": 48,
      "x": -0.33,
      "y": 1.047,
      "layer": 1,
      "closed_by": [
        47,
        44
      ]
    },
    {
      "id": 47,
      "x": -0.62,
      "y": 1.047,
      "layer": 2,
      "closed_by": [
        49
      ]
    },
    {
      "id": 49,
      "x": -0.944,
      "y": 1.047,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 46,
      "x": 0.625,
      "y": 1.047,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 41,
      "x": -0.259,
      "y": 0.231,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 0.944,
      "y": 0.231,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 40,
      "x": 0.62,
      "y": 0.231,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 42,
      "x": 0.259,
      "y": 0.231,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 43,
      "y": 0.665,
      "layer": 3,
      "closed_by": [
        41,
        42
      ]
    },
    {
      "id": 39,
      "x": -0.62,
      "y": 0.231,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 35,
      "x": -0.947,
      "y": 0.231,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 44,
      "y": 1.047,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 31,
      "x": 0.786,
      "y": -0.143,
      "layer": 1,
      "closed_by": [
        38,
        33
      ]
    },
    {
      "id": 33,
      "x": 0.379,
      "y": -0.143,
      "layer": 2,
      "closed_by": [
        40,
        34
      ]
    },
    {
      "id": 32,
      "x": -0.379,
      "y": -0.143,
      "layer": 2,
      "closed_by": [
        39,
        34
      ]
    },
    {
      "id": 30,
      "x": -0.787,
      "y": -0.143,
      "layer": 1,
      "closed_by": [
        35,
        32,
        36
      ]
    },
    {
      "id": 34,
      "y": -0.15,
      "layer": 3,
      "closed_by": [
        41,
        42,
        29
      ]
    },
    {
      "id": 29,
      "x": -0.001,
      "y": -0.574,
      "layer": 4,
      "closed_by": [
        28,
        36
      ]
    },
    {
      "id": 28,
      "x": 0.386,
      "y": -0.574,
      "layer": 6,
      "closed_by": [
        27
      ]
    },
    {
      "id": 36,
      "x": -0.384,
      "y": -0.574,
      "layer": 6,
      "closed_by": [
        26
      ]
    },
    {
      "id": 27,
      "x": 0.625,
      "y": -0.574,
      "layer": 7,
      "closed_by": [
        25
      ]
    },
    {
      "id": 26,
      "x": -0.625,
      "y": -0.574,
      "layer": 7,
      "closed_by": [
        24
      ]
    },
    {
      "id": 25,
      "x": 0.944,
      "y": -0.574,
      "layer": 8,
      "closed_by": []
    },
    {
      "id": 24,
      "x": -0.944,
      "y": -0.577,
      "layer": 8,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 1.633,
      "y": 0.976,
      "angle": 20.0,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 22,
      "x": 1.641,
      "y": 0.469,
      "angle": 340.0,
      "layer": 2,
      "closed_by": [
        21
      ]
    },
    {
      "id": 21,
      "x": 1.641,
      "y": -0.028,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 37,
      "x": -1.636,
      "y": 0.972,
      "angle": 340.0,
      "layer": 1,
      "closed_by": [
        20
      ]
    },
    {
      "id": 20,
      "x": -1.638,
      "y": 0.469,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 19,
      "x": -1.633,
      "y": -0.037,
      "angle": 340.0,
      "layer": 3,
      "closed_by": [
        15
      ]
    },
    {
      "id": 18,
      "x": 1.644,
      "y": -0.535,
      "angle": 340.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 15,
      "x": -1.639,
      "y": -0.537,
      "angle": 20.0,
      "layer": 4,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 34,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 17.1,
    "star2": 73.1,
    "star3": 8.9
  }
}