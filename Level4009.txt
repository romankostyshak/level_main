{
  "layout_cards": [
    {
      "x": 2.118,
      "y": 0.217,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 3,
      "x": 2.259,
      "y": 0.217,
      "layer": 3,
      "closed_by": [
        0
      ]
    },
    {
      "id": 4,
      "x": -2.319,
      "y": -0.259,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        6,
        26
      ]
    },
    {
      "id": 5,
      "x": 2.319,
      "y": -0.259,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        3,
        34
      ]
    },
    {
      "id": 6,
      "x": -2.259,
      "y": 0.217,
      "layer": 3,
      "closed_by": [
        7
      ]
    },
    {
      "id": 7,
      "x": -2.118,
      "y": 0.217,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 8,
      "x": 2.319,
      "y": 0.679,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        3,
        34
      ]
    },
    {
      "id": 9,
      "x": 2.138,
      "y": 0.217,
      "layer": 1,
      "closed_by": [
        5,
        8
      ]
    },
    {
      "id": 10,
      "x": -2.14,
      "y": 0.217,
      "layer": 1,
      "closed_by": [
        4,
        11
      ]
    },
    {
      "id": 11,
      "x": -2.319,
      "y": 0.679,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        6,
        26
      ]
    },
    {
      "id": 12,
      "x": 0.898,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        18
      ]
    },
    {
      "id": 13,
      "x": -0.898,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        14
      ]
    },
    {
      "id": 14,
      "x": -0.819,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 15,
      "x": 0.898,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        16
      ]
    },
    {
      "id": 16,
      "x": 0.819,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 17,
      "x": -0.898,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        19
      ]
    },
    {
      "id": 18,
      "x": 0.819,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 19,
      "x": -0.819,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        21
      ]
    },
    {
      "id": 20,
      "x": 0.819,
      "y": -0.5,
      "angle": 60.0,
      "layer": 3,
      "closed_by": [
        36,
        40
      ]
    },
    {
      "id": 21,
      "x": -0.819,
      "y": -0.5,
      "angle": 300.0,
      "layer": 3,
      "closed_by": [
        37,
        39
      ]
    },
    {
      "id": 22,
      "x": -0.819,
      "y": 0.944,
      "angle": 60.0,
      "layer": 3,
      "closed_by": [
        38,
        39
      ]
    },
    {
      "id": 23,
      "x": 0.819,
      "y": 0.939,
      "angle": 300.0,
      "layer": 3,
      "closed_by": [
        35,
        40
      ]
    },
    {
      "id": 24,
      "x": 1.598,
      "y": 0.98,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 25,
      "x": -0.337,
      "y": -0.418,
      "layer": 5,
      "closed_by": [
        50
      ]
    },
    {
      "id": 26,
      "x": -1.74,
      "y": 0.217,
      "layer": 3,
      "closed_by": [
        7,
        37,
        38
      ]
    },
    {
      "id": 27,
      "x": 1.598,
      "y": -0.518,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        30
      ]
    },
    {
      "id": 28,
      "x": -1.6,
      "y": -0.518,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 29,
      "x": -1.6,
      "y": 0.98,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 30,
      "x": 1.46,
      "y": -0.379,
      "layer": 2,
      "closed_by": [
        20,
        34
      ]
    },
    {
      "id": 31,
      "x": 1.46,
      "y": 0.837,
      "layer": 2,
      "closed_by": [
        23,
        34
      ]
    },
    {
      "id": 32,
      "x": -1.457,
      "y": 0.837,
      "layer": 2,
      "closed_by": [
        22,
        26
      ]
    },
    {
      "id": 33,
      "x": -1.457,
      "y": -0.379,
      "layer": 2,
      "closed_by": [
        21,
        26
      ]
    },
    {
      "id": 34,
      "x": 1.74,
      "y": 0.217,
      "layer": 3,
      "closed_by": [
        0,
        35,
        36
      ]
    },
    {
      "id": 35,
      "x": 1.279,
      "y": 0.799,
      "angle": 30.0,
      "layer": 4,
      "closed_by": [
        46
      ]
    },
    {
      "id": 36,
      "x": 1.279,
      "y": -0.337,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [
        44
      ]
    },
    {
      "id": 37,
      "x": -1.279,
      "y": -0.337,
      "angle": 30.0,
      "layer": 4,
      "closed_by": [
        47
      ]
    },
    {
      "id": 38,
      "x": -1.279,
      "y": 0.799,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [
        48
      ]
    },
    {
      "id": 39,
      "x": -0.499,
      "y": 0.217,
      "layer": 4,
      "closed_by": [
        25,
        43,
        45
      ]
    },
    {
      "id": 40,
      "x": 0.5,
      "y": 0.217,
      "layer": 4,
      "closed_by": [
        41,
        42,
        49
      ]
    },
    {
      "id": 41,
      "x": 0.337,
      "y": -0.418,
      "layer": 5,
      "closed_by": [
        50
      ]
    },
    {
      "id": 42,
      "x": 0.337,
      "y": 0.859,
      "layer": 5,
      "closed_by": [
        51
      ]
    },
    {
      "id": 43,
      "x": -0.337,
      "y": 0.859,
      "layer": 5,
      "closed_by": [
        51
      ]
    },
    {
      "id": 44,
      "x": 1.098,
      "y": -0.238,
      "angle": 330.0,
      "layer": 5,
      "closed_by": [
        49
      ]
    },
    {
      "id": 45,
      "x": -0.939,
      "y": 0.217,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 46,
      "x": 1.098,
      "y": 0.699,
      "angle": 30.0,
      "layer": 5,
      "closed_by": [
        49
      ]
    },
    {
      "id": 47,
      "x": -1.1,
      "y": -0.238,
      "angle": 30.0,
      "layer": 5,
      "closed_by": [
        45
      ]
    },
    {
      "id": 48,
      "x": -1.1,
      "y": 0.699,
      "angle": 330.0,
      "layer": 5,
      "closed_by": [
        45
      ]
    },
    {
      "id": 49,
      "x": 0.939,
      "y": 0.217,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 50,
      "y": -0.578,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 51,
      "y": 1.019,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 50,
  "cards_in_deck_amount": 36,
  "layers_in_level": 6,
  "open_cards": 6,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 22.4,
    "star2": 72.1,
    "star3": 4.7
  }
}