{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.407,
      "y": 0.865,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 50,
      "x": 0.412,
      "y": -0.268,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 49,
      "x": -0.358,
      "y": 0.813,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 48,
      "x": 0.363,
      "y": -0.209,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 47,
      "x": -0.312,
      "y": 0.759,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 46,
      "x": 0.316,
      "y": -0.155,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 45,
      "x": -0.263,
      "y": 0.712,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 44,
      "x": 0.263,
      "y": -0.104,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 43,
      "x": -0.206,
      "y": -0.043,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 42,
      "x": 1.531,
      "y": 0.657,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 41,
      "x": -1.603,
      "y": -0.061,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 1.6,
      "y": 0.661,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 39,
      "x": -1.659,
      "y": -0.061,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 38,
      "x": -1.72,
      "y": -0.059,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 37,
      "x": 1.728,
      "y": 0.662,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 36,
      "x": 1.781,
      "y": 0.657,
      "layer": 1,
      "closed_by": [
        37
      ]
    },
    {
      "id": 35,
      "x": 0.2,
      "y": 0.657,
      "layer": 5,
      "closed_by": [
        43
      ]
    },
    {
      "id": 34,
      "x": 0.256,
      "y": 0.722,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 33,
      "x": -0.259,
      "y": -0.111,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 32,
      "x": 0.319,
      "y": 0.782,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 31,
      "x": 1.666,
      "y": 0.66,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 30,
      "x": -0.321,
      "y": -0.177,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 29,
      "x": -1.779,
      "y": -0.059,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 28,
      "x": -1.838,
      "y": -0.059,
      "layer": 1,
      "closed_by": [
        29
      ]
    }
  ],
  "cards_in_layout_amount": 24,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 62.5,
    "star2": 10.5,
    "star3": 26.2
  }
}