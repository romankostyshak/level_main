{
  "layout_cards": [
    {
      "id": 9,
      "x": -1.286,
      "y": 0.216,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 10,
      "x": 1.302,
      "y": 0.216,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 11,
      "x": -1.743,
      "y": 0.223,
      "layer": 5,
      "closed_by": [
        9
      ]
    },
    {
      "id": 12,
      "x": -1.743,
      "y": -0.335,
      "layer": 4,
      "closed_by": [
        11
      ]
    },
    {
      "id": 13,
      "x": -1.743,
      "y": 0.787,
      "layer": 4,
      "closed_by": [
        11
      ]
    },
    {
      "id": 14,
      "x": 1.746,
      "y": -0.34,
      "layer": 4,
      "closed_by": [
        16
      ]
    },
    {
      "id": 15,
      "x": 1.746,
      "y": 0.782,
      "layer": 4,
      "closed_by": [
        16
      ]
    },
    {
      "id": 16,
      "x": 1.746,
      "y": 0.209,
      "layer": 5,
      "closed_by": [
        10
      ]
    },
    {
      "id": 18,
      "x": -1.743,
      "y": -0.574,
      "layer": 3,
      "closed_by": [
        12
      ]
    },
    {
      "id": 19,
      "x": -1.745,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        13
      ]
    },
    {
      "id": 20,
      "x": 1.745,
      "y": -0.569,
      "layer": 3,
      "closed_by": [
        14
      ]
    },
    {
      "id": 21,
      "x": -1.587,
      "y": -0.574,
      "layer": 2,
      "closed_by": [
        18
      ]
    },
    {
      "id": 22,
      "x": -1.585,
      "y": 1.021,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 23,
      "x": 1.587,
      "y": -0.572,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 24,
      "x": 1.587,
      "y": 1.021,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 25,
      "x": -1.345,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        21,
        29
      ]
    },
    {
      "id": 26,
      "x": -1.345,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        22,
        28
      ]
    },
    {
      "id": 27,
      "x": 1.347,
      "y": -0.573,
      "layer": 1,
      "closed_by": [
        23,
        30
      ]
    },
    {
      "id": 28,
      "x": -0.944,
      "y": 1.021,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 29,
      "x": -0.944,
      "y": -0.574,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 30,
      "x": 0.948,
      "y": -0.574,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 31,
      "x": 0.949,
      "y": 1.018,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 32,
      "x": -0.252,
      "y": 0.216,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 0.27,
      "y": 0.216,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -0.944,
      "y": -0.335,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 35,
      "x": 0.949,
      "y": -0.335,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 36,
      "x": 1.345,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        24,
        31
      ]
    },
    {
      "id": 37,
      "x": 1.745,
      "y": 1.021,
      "layer": 3,
      "closed_by": [
        15
      ]
    },
    {
      "id": 38,
      "x": -0.944,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 39,
      "x": 0.948,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 40,
      "x": -1.024,
      "y": 0.216,
      "layer": 5,
      "closed_by": [
        9
      ]
    },
    {
      "id": 41,
      "x": 1.029,
      "y": 0.216,
      "layer": 5,
      "closed_by": [
        10
      ]
    },
    {
      "id": 42,
      "x": -0.625,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        32,
        40
      ]
    },
    {
      "id": 43,
      "x": 0.625,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        33,
        41
      ]
    },
    {
      "id": 44,
      "x": 0.003,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        32,
        33
      ]
    },
    {
      "id": 45,
      "x": 0.386,
      "y": -0.185,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 46,
      "x": 0.388,
      "y": 0.633,
      "layer": 3,
      "closed_by": [
        43,
        44
      ]
    },
    {
      "id": 47,
      "x": 0.001,
      "y": 0.141,
      "layer": 2,
      "closed_by": [
        45,
        46,
        49,
        50
      ]
    },
    {
      "id": 48,
      "y": -0.412,
      "layer": 1,
      "closed_by": [
        33,
        47
      ]
    },
    {
      "id": 49,
      "x": -0.381,
      "y": -0.185,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 50,
      "x": -0.384,
      "y": 0.629,
      "layer": 3,
      "closed_by": [
        42,
        44
      ]
    }
  ],
  "cards_in_layout_amount": 41,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 14.0,
    "star2": 75.5,
    "star3": 10.0
  }
}