{
  "layout_cards": [
    {
      "id": 5,
      "y": 0.221,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 8,
      "x": 0.305,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        5
      ]
    },
    {
      "id": 9,
      "x": -1.493,
      "y": 0.216,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 10,
      "x": 1.498,
      "y": 0.216,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 11,
      "x": -2.012,
      "y": 0.216,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 12,
      "x": 1.343,
      "y": 0.221,
      "layer": 5,
      "closed_by": [
        10
      ]
    },
    {
      "id": 13,
      "x": -1.345,
      "y": 0.221,
      "layer": 5,
      "closed_by": [
        9
      ]
    },
    {
      "id": 14,
      "x": 2.174,
      "y": 0.216,
      "layer": 5,
      "closed_by": [
        16
      ]
    },
    {
      "id": 15,
      "x": -2.173,
      "y": 0.216,
      "layer": 5,
      "closed_by": [
        11
      ]
    },
    {
      "id": 16,
      "x": 2.013,
      "y": 0.216,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 17,
      "x": -0.3,
      "y": 0.222,
      "layer": 4,
      "closed_by": [
        5
      ]
    },
    {
      "id": 18,
      "x": -2.545,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        15
      ]
    },
    {
      "id": 19,
      "x": 2.549,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        14
      ]
    },
    {
      "id": 20,
      "x": 2.309,
      "y": -0.261,
      "layer": 3,
      "closed_by": [
        19
      ]
    },
    {
      "id": 21,
      "x": -2.305,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 22,
      "x": 2.302,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        19
      ]
    },
    {
      "id": 23,
      "x": -2.065,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        21
      ]
    },
    {
      "id": 24,
      "x": -2.065,
      "y": 1.021,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 25,
      "x": 2.062,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 26,
      "x": 2.065,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 27,
      "x": -1.743,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        15,
        23,
        31
      ]
    },
    {
      "id": 28,
      "x": 1.746,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        14,
        26,
        32
      ]
    },
    {
      "id": 29,
      "x": 1.746,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        25,
        33
      ]
    },
    {
      "id": 30,
      "x": -1.447,
      "y": 1.021,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 31,
      "x": -1.45,
      "y": -0.574,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 32,
      "x": 1.452,
      "y": -0.574,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 33,
      "x": 1.452,
      "y": 1.021,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 34,
      "x": -1.21,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 35,
      "x": -1.21,
      "y": -0.256,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 36,
      "x": -1.743,
      "y": 1.023,
      "layer": 1,
      "closed_by": [
        13,
        24,
        30
      ]
    },
    {
      "id": 37,
      "x": -2.305,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 38,
      "x": 1.215,
      "y": -0.254,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 39,
      "x": 1.213,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 40,
      "x": -0.944,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        13
      ]
    },
    {
      "id": 41,
      "x": 0.944,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        12
      ]
    },
    {
      "id": 42,
      "x": -0.545,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        17,
        40
      ]
    },
    {
      "id": 43,
      "x": 0.541,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        8,
        41
      ]
    },
    {
      "id": 44,
      "x": -0.305,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 45,
      "x": -0.305,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 46,
      "x": 0.307,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 47,
      "y": 1.054,
      "layer": 1,
      "closed_by": [
        45,
        46
      ]
    },
    {
      "id": 48,
      "y": 0.237,
      "layer": 1,
      "closed_by": [
        17,
        44,
        45,
        46,
        50
      ]
    },
    {
      "id": 49,
      "x": -0.002,
      "y": -0.573,
      "layer": 1,
      "closed_by": [
        17,
        44,
        50
      ]
    },
    {
      "id": 50,
      "x": 0.307,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        43
      ]
    }
  ],
  "cards_in_layout_amount": 44,
  "cards_in_deck_amount": 32,
  "stars_statistic": {
    "star1": 1.5,
    "star2": 29.4,
    "star3": 68.5
  }
}