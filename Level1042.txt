{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.388,
      "y": 0.967,
      "angle": 9.998,
      "layer": 1,
      "closed_by": [
        48,
        45
      ]
    },
    {
      "id": 50,
      "x": 0.391,
      "y": 0.966,
      "angle": 349.997,
      "layer": 1,
      "closed_by": [
        49,
        45
      ]
    },
    {
      "id": 49,
      "x": 0.699,
      "y": 0.883,
      "angle": 340.0,
      "layer": 2,
      "closed_by": [
        47,
        43
      ]
    },
    {
      "id": 48,
      "x": -0.689,
      "y": 0.888,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        46,
        44
      ]
    },
    {
      "id": 47,
      "x": 0.998,
      "y": 0.74,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 46,
      "x": -0.975,
      "y": 0.758,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 44,
      "x": -0.31,
      "y": 0.935,
      "angle": 9.998,
      "layer": 3,
      "closed_by": [
        41,
        13
      ]
    },
    {
      "id": 43,
      "x": 0.324,
      "y": 0.934,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        42,
        13
      ]
    },
    {
      "id": 42,
      "x": 0.674,
      "y": 0.842,
      "angle": 340.0,
      "layer": 4,
      "closed_by": [
        25
      ]
    },
    {
      "id": 41,
      "x": -0.651,
      "y": 0.855,
      "angle": 20.0,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 40,
      "x": -1.018,
      "y": 0.68,
      "angle": 30.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 39,
      "x": 0.002,
      "y": -0.437,
      "layer": 1,
      "closed_by": [
        38,
        35
      ]
    },
    {
      "id": 38,
      "x": 0.324,
      "y": -0.312,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        37,
        32
      ]
    },
    {
      "id": 37,
      "x": 0.634,
      "y": -0.397,
      "angle": 340.0,
      "layer": 3,
      "closed_by": [
        36,
        31
      ]
    },
    {
      "id": 36,
      "x": 0.925,
      "y": -0.537,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 35,
      "x": -0.319,
      "y": -0.303,
      "angle": 9.998,
      "layer": 2,
      "closed_by": [
        34,
        32
      ]
    },
    {
      "id": 34,
      "x": -0.62,
      "y": -0.379,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        33,
        30
      ]
    },
    {
      "id": 33,
      "x": -0.882,
      "y": -0.523,
      "angle": 30.0,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 32,
      "x": 0.008,
      "y": -0.128,
      "layer": 3,
      "closed_by": [
        31,
        30
      ]
    },
    {
      "id": 31,
      "x": 0.337,
      "y": -0.158,
      "angle": 349.997,
      "layer": 4,
      "closed_by": [
        29,
        12
      ]
    },
    {
      "id": 29,
      "x": 0.648,
      "y": -0.245,
      "angle": 340.0,
      "layer": 5,
      "closed_by": [
        27
      ]
    },
    {
      "id": 28,
      "x": -0.615,
      "y": -0.231,
      "angle": 20.0,
      "layer": 5,
      "closed_by": [
        26
      ]
    },
    {
      "id": 27,
      "x": 0.947,
      "y": -0.384,
      "angle": 330.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -0.907,
      "y": -0.368,
      "angle": 30.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 1.039,
      "y": 0.666,
      "angle": 330.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 1.437,
      "y": 0.43,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [
        25
      ]
    },
    {
      "id": 23,
      "x": -1.401,
      "y": 0.444,
      "angle": 30.0,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 22,
      "x": 1.692,
      "y": 0.252,
      "angle": 320.0,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 21,
      "x": 1.916,
      "y": 0.028,
      "angle": 310.0,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 20,
      "x": -1.667,
      "y": 0.259,
      "angle": 40.0,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 19,
      "x": -1.893,
      "y": 0.028,
      "angle": 50.0,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 18,
      "x": 2.095,
      "y": -0.229,
      "angle": 300.0,
      "layer": 1,
      "closed_by": [
        21
      ]
    },
    {
      "id": 17,
      "x": -2.072,
      "y": -0.224,
      "angle": 60.0,
      "layer": 1,
      "closed_by": [
        19
      ]
    },
    {
      "id": 45,
      "x": 0.007,
      "y": 1.065,
      "layer": 2,
      "closed_by": [
        44,
        43
      ]
    },
    {
      "id": 13,
      "x": 0.008,
      "y": 0.962,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -0.316,
      "y": -0.158,
      "angle": 9.998,
      "layer": 4,
      "closed_by": [
        28,
        12
      ]
    },
    {
      "id": 12,
      "x": 0.004,
      "y": -0.228,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 37,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 16.2,
    "star2": 69.0,
    "star3": 13.9
  }
}