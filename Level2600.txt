{
  "layout_cards": [
    {
      "id": 4,
      "x": 2.068,
      "y": -0.577,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 5,
      "x": 0.628,
      "y": 0.782,
      "layer": 3,
      "closed_by": [
        12,
        15
      ]
    },
    {
      "id": 6,
      "x": 0.002,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        9,
        10
      ]
    },
    {
      "id": 7,
      "x": -0.625,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        9
      ]
    },
    {
      "id": 8,
      "x": 0.625,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        10
      ]
    },
    {
      "id": 9,
      "x": -0.331,
      "y": 1.023,
      "layer": 2,
      "closed_by": [
        11
      ]
    },
    {
      "id": 10,
      "x": 0.333,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        5
      ]
    },
    {
      "id": 11,
      "x": -0.625,
      "y": 0.782,
      "layer": 3,
      "closed_by": [
        13,
        14
      ]
    },
    {
      "id": 12,
      "x": 0.947,
      "y": 0.782,
      "layer": 4,
      "closed_by": [
        17
      ]
    },
    {
      "id": 13,
      "x": -0.944,
      "y": 0.782,
      "layer": 4,
      "closed_by": [
        16
      ]
    },
    {
      "id": 14,
      "x": -0.409,
      "y": 0.782,
      "layer": 4,
      "closed_by": [
        16,
        21
      ]
    },
    {
      "id": 15,
      "x": 0.412,
      "y": 0.782,
      "layer": 4,
      "closed_by": [
        17,
        21
      ]
    },
    {
      "id": 16,
      "x": -0.652,
      "y": 0.703,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 17,
      "x": 0.652,
      "y": 0.703,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 18,
      "x": 0.266,
      "y": -0.578,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 19,
      "y": -0.497,
      "layer": 3,
      "closed_by": [
        30,
        31
      ]
    },
    {
      "id": 20,
      "x": -0.652,
      "y": -0.416,
      "layer": 1,
      "closed_by": [
        24
      ]
    },
    {
      "id": 21,
      "x": 0.008,
      "y": 0.703,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 22,
      "y": -0.416,
      "layer": 1,
      "closed_by": [
        24,
        25
      ]
    },
    {
      "id": 23,
      "x": 0.652,
      "y": -0.416,
      "layer": 1,
      "closed_by": [
        25
      ]
    },
    {
      "id": 24,
      "x": -0.328,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        19,
        26
      ]
    },
    {
      "id": 25,
      "x": 0.331,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        19,
        27
      ]
    },
    {
      "id": 26,
      "x": -0.569,
      "y": -0.499,
      "layer": 3,
      "closed_by": [
        29,
        30
      ]
    },
    {
      "id": 27,
      "x": 0.574,
      "y": -0.5,
      "layer": 3,
      "closed_by": [
        28,
        31
      ]
    },
    {
      "id": 28,
      "x": 0.865,
      "y": -0.574,
      "layer": 4,
      "closed_by": [
        32
      ]
    },
    {
      "id": 29,
      "x": -0.865,
      "y": -0.577,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 30,
      "x": -0.259,
      "y": -0.531,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 31,
      "x": 0.263,
      "y": -0.54,
      "layer": 4,
      "closed_by": [
        18
      ]
    },
    {
      "id": 32,
      "x": 0.947,
      "y": -0.577,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -0.944,
      "y": -0.577,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -0.259,
      "y": -0.577,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 1.531,
      "y": 0.061,
      "layer": 1,
      "closed_by": [
        37
      ]
    },
    {
      "id": 36,
      "x": -1.531,
      "y": 0.063,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 37,
      "x": 1.613,
      "y": 0.059,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 38,
      "x": 1.534,
      "y": -0.572,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -1.771,
      "y": 0.061,
      "layer": 4,
      "closed_by": [
        45
      ]
    },
    {
      "id": 40,
      "x": -1.61,
      "y": 0.061,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 41,
      "x": 1.694,
      "y": 0.061,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 42,
      "x": -1.692,
      "y": 0.061,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 43,
      "x": 1.774,
      "y": 0.059,
      "layer": 4,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": 1.855,
      "y": 0.059,
      "layer": 5,
      "closed_by": [
        4,
        38,
        46,
        50
      ]
    },
    {
      "id": 45,
      "x": -1.85,
      "y": 0.059,
      "layer": 5,
      "closed_by": [
        47,
        48,
        49,
        51
      ]
    },
    {
      "id": 46,
      "x": 1.534,
      "y": 0.782,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 47,
      "x": -1.531,
      "y": 0.781,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 48,
      "x": -2.065,
      "y": -0.574,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 49,
      "x": -1.531,
      "y": -0.578,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 50,
      "x": 2.065,
      "y": 0.781,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -2.065,
      "y": 0.782,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 48,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 0.1,
    "star2": 7.1,
    "star3": 92.4
  }
}