{
  "layout_cards": [
    {
      "id": 48,
      "x": 1.049,
      "y": 0.411,
      "angle": 345.0,
      "layer": 2,
      "closed_by": [
        47,
        41
      ]
    },
    {
      "id": 47,
      "x": 0.837,
      "y": -0.064,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 50,
      "x": -1.044,
      "y": 0.407,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        44,
        42
      ]
    },
    {
      "id": 44,
      "x": -0.832,
      "y": -0.075,
      "angle": 29.999,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 43,
      "x": -0.518,
      "y": -0.46,
      "angle": 45.0,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 46,
      "x": 0.513,
      "y": -0.46,
      "angle": 315.0,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 38,
      "x": 0.008,
      "y": 1.019,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 39,
      "x": 0.337,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 41,
      "x": 0.74,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 49,
      "x": 1.131,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 40,
      "x": -0.337,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 42,
      "x": -0.74,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 45,
      "x": -1.126,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 35,
      "x": -0.261,
      "y": -0.5,
      "layer": 5,
      "closed_by": [
        33
      ]
    },
    {
      "id": 34,
      "x": 0.252,
      "y": -0.5,
      "layer": 5,
      "closed_by": [
        33
      ]
    },
    {
      "id": 33,
      "y": 0.063,
      "layer": 6,
      "closed_by": [
        32,
        31
      ]
    },
    {
      "id": 32,
      "x": 0.386,
      "y": 0.136,
      "layer": 7,
      "closed_by": [
        30
      ]
    },
    {
      "id": 31,
      "x": -0.384,
      "y": 0.136,
      "layer": 7,
      "closed_by": [
        29
      ]
    },
    {
      "id": 30,
      "x": 0.648,
      "y": 0.136,
      "layer": 8,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -0.643,
      "y": 0.136,
      "layer": 8,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 20,
  "cards_in_deck_amount": 22,
  "stars_statistic": {
    "star1": 14.2,
    "star2": 0.1,
    "star3": 85.0
  }
}