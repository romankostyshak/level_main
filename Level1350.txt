{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.892,
      "y": -0.103,
      "layer": 1,
      "closed_by": [
        44,
        22
      ]
    },
    {
      "id": 50,
      "x": 0.414,
      "y": -0.101,
      "layer": 3,
      "closed_by": [
        48,
        43,
        19
      ]
    },
    {
      "id": 49,
      "x": -0.259,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        47,
        35
      ]
    },
    {
      "id": 48,
      "x": 0.263,
      "y": -0.34,
      "layer": 4,
      "closed_by": [
        47,
        41
      ]
    },
    {
      "id": 47,
      "x": 0.002,
      "y": -0.578,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 46,
      "x": -0.402,
      "y": -0.101,
      "layer": 3,
      "closed_by": [
        49,
        42,
        19
      ]
    },
    {
      "id": 45,
      "x": 0.898,
      "y": -0.108,
      "layer": 1,
      "closed_by": [
        39,
        30
      ]
    },
    {
      "id": 44,
      "x": -1.268,
      "y": 0.375,
      "layer": 2,
      "closed_by": [
        36,
        27
      ]
    },
    {
      "id": 43,
      "x": 0.782,
      "y": -0.331,
      "layer": 4,
      "closed_by": [
        41,
        32
      ]
    },
    {
      "id": 42,
      "x": -0.787,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        31,
        35
      ]
    },
    {
      "id": 41,
      "x": 0.55,
      "y": -0.574,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 0.335,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        39,
        25
      ]
    },
    {
      "id": 39,
      "x": 0.652,
      "y": 0.377,
      "layer": 2,
      "closed_by": [
        50,
        26,
        24
      ]
    },
    {
      "id": 38,
      "x": -0.893,
      "y": 0.781,
      "layer": 1,
      "closed_by": [
        44,
        22
      ]
    },
    {
      "id": 37,
      "x": 1.179,
      "y": -0.108,
      "layer": 3,
      "closed_by": [
        43,
        33
      ]
    },
    {
      "id": 36,
      "x": -1.184,
      "y": -0.104,
      "layer": 3,
      "closed_by": [
        42,
        34
      ]
    },
    {
      "id": 34,
      "x": -1.345,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 33,
      "x": 1.348,
      "y": -0.416,
      "layer": 4,
      "closed_by": [
        32
      ]
    },
    {
      "id": 32,
      "x": 1.133,
      "y": -0.574,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -1.131,
      "y": -0.578,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 1.269,
      "y": 0.372,
      "layer": 2,
      "closed_by": [
        37,
        26
      ]
    },
    {
      "id": 29,
      "x": 1.424,
      "y": 0.782,
      "layer": 1,
      "closed_by": [
        30
      ]
    },
    {
      "id": 28,
      "x": -1.424,
      "y": 0.781,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 27,
      "x": -1.133,
      "y": 0.86,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 26,
      "x": 1.11,
      "y": 0.861,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 25,
      "y": 0.136,
      "layer": 2,
      "closed_by": [
        50,
        46,
        24,
        20
      ]
    },
    {
      "id": 24,
      "x": 0.256,
      "y": 0.856,
      "layer": 3,
      "closed_by": [
        19
      ]
    },
    {
      "id": 23,
      "x": 0.893,
      "y": 0.781,
      "layer": 1,
      "closed_by": [
        39,
        30
      ]
    },
    {
      "id": 22,
      "x": -0.652,
      "y": 0.379,
      "layer": 2,
      "closed_by": [
        46,
        27,
        20
      ]
    },
    {
      "id": 21,
      "x": -0.331,
      "y": 0.781,
      "layer": 1,
      "closed_by": [
        25,
        22
      ]
    },
    {
      "id": 20,
      "x": -0.259,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        19
      ]
    },
    {
      "id": 19,
      "y": 0.545,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -0.546,
      "y": -0.578,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 33,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 53.9,
    "star2": 44.9,
    "star3": 1.0
  }
}