{
  "layout_cards": [
    {
      "id": 20,
      "x": 1.434,
      "y": 0.882,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 21,
      "x": -0.002,
      "y": -0.256,
      "layer": 3,
      "closed_by": [
        29,
        30
      ]
    },
    {
      "id": 22,
      "x": -0.008,
      "y": 0.708,
      "layer": 3,
      "closed_by": [
        29,
        30
      ]
    },
    {
      "id": 23,
      "x": -2.545,
      "y": -0.097,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 2.545,
      "y": 0.537,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 25,
      "x": -2.305,
      "y": -0.097,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 26,
      "x": 2.305,
      "y": 0.541,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 27,
      "x": -1.44,
      "y": 0.62,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 0.002,
      "y": 0.221,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 29,
      "x": 0.263,
      "y": 0.298,
      "layer": 5,
      "closed_by": [
        28
      ]
    },
    {
      "id": 30,
      "x": -0.259,
      "y": 0.136,
      "layer": 5,
      "closed_by": [
        28
      ]
    },
    {
      "id": 31,
      "x": 0.574,
      "y": 0.3,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 32,
      "x": -0.569,
      "y": 0.136,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 33,
      "x": -1.439,
      "y": 0.381,
      "layer": 5,
      "closed_by": [
        27
      ]
    },
    {
      "id": 34,
      "x": -1.131,
      "y": 0.136,
      "layer": 4,
      "closed_by": [
        33,
        37
      ]
    },
    {
      "id": 35,
      "x": -1.745,
      "y": 0.136,
      "layer": 4,
      "closed_by": [
        33,
        37
      ]
    },
    {
      "id": 36,
      "x": 1.439,
      "y": -0.177,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 37,
      "x": -1.442,
      "y": -0.432,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -2.065,
      "y": -0.097,
      "layer": 3,
      "closed_by": [
        25,
        35
      ]
    },
    {
      "id": 39,
      "x": -0.81,
      "y": -0.093,
      "layer": 3,
      "closed_by": [
        32,
        34
      ]
    },
    {
      "id": 40,
      "x": -1.743,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 41,
      "x": -1.133,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 42,
      "x": -1.442,
      "y": -0.577,
      "layer": 1,
      "closed_by": [
        34,
        35,
        40,
        41
      ]
    },
    {
      "id": 43,
      "x": 1.439,
      "y": 0.063,
      "layer": 5,
      "closed_by": [
        36
      ]
    },
    {
      "id": 44,
      "x": 1.746,
      "y": 0.303,
      "layer": 4,
      "closed_by": [
        20,
        43
      ]
    },
    {
      "id": 45,
      "x": 0.814,
      "y": 0.541,
      "layer": 3,
      "closed_by": [
        31,
        50
      ]
    },
    {
      "id": 46,
      "x": 2.068,
      "y": 0.541,
      "layer": 3,
      "closed_by": [
        26,
        44
      ]
    },
    {
      "id": 47,
      "x": 1.133,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 48,
      "x": 1.746,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 49,
      "x": 1.434,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        44,
        47,
        48,
        50
      ]
    },
    {
      "id": 50,
      "x": 1.133,
      "y": 0.303,
      "layer": 4,
      "closed_by": [
        20,
        43
      ]
    }
  ],
  "cards_in_layout_amount": 31,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 61.1,
    "star2": 29.2,
    "star3": 9.4
  }
}