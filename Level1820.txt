{
  "layout_cards": [
    {
      "id": 49,
      "x": 0.333,
      "y": -0.017,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 48,
      "x": -0.333,
      "y": -0.017,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 47,
      "x": 0.623,
      "y": -0.017,
      "layer": 2,
      "closed_by": [
        45,
        44,
        36,
        27
      ]
    },
    {
      "id": 46,
      "x": -0.625,
      "y": -0.017,
      "layer": 2,
      "closed_by": [
        50,
        43,
        28,
        26
      ]
    },
    {
      "id": 45,
      "x": 0.331,
      "y": 0.536,
      "layer": 3,
      "closed_by": [
        42,
        34,
        33
      ]
    },
    {
      "id": 50,
      "x": -0.333,
      "y": 0.535,
      "layer": 3,
      "closed_by": [
        41,
        34,
        33
      ]
    },
    {
      "id": 44,
      "x": 0.865,
      "y": 0.532,
      "layer": 3,
      "closed_by": [
        42,
        40,
        38
      ]
    },
    {
      "id": 43,
      "x": -0.865,
      "y": 0.535,
      "layer": 3,
      "closed_by": [
        41,
        39,
        35
      ]
    },
    {
      "id": 42,
      "x": 0.652,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 41,
      "x": -0.652,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 40,
      "x": 1.184,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        32,
        23
      ]
    },
    {
      "id": 39,
      "x": -1.184,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        31,
        22
      ]
    },
    {
      "id": 38,
      "x": 1.184,
      "y": -0.017,
      "layer": 4,
      "closed_by": [
        32,
        29,
        36
      ]
    },
    {
      "id": 35,
      "x": -1.182,
      "y": -0.017,
      "layer": 4,
      "closed_by": [
        31,
        30,
        28
      ]
    },
    {
      "id": 34,
      "y": 0.939,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 0.001,
      "y": -0.023,
      "layer": 4,
      "closed_by": [
        27,
        26
      ]
    },
    {
      "id": 32,
      "x": 1.429,
      "y": 0.532,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -1.424,
      "y": 0.535,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -1.424,
      "y": -0.416,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 29,
      "x": 1.427,
      "y": -0.416,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 28,
      "x": -0.864,
      "y": -0.418,
      "layer": 5,
      "closed_by": [
        24
      ]
    },
    {
      "id": 36,
      "x": 0.865,
      "y": -0.416,
      "layer": 5,
      "closed_by": [
        25
      ]
    },
    {
      "id": 27,
      "x": 0.333,
      "y": -0.416,
      "layer": 5,
      "closed_by": [
        25
      ]
    },
    {
      "id": 26,
      "x": -0.333,
      "y": -0.418,
      "layer": 5,
      "closed_by": [
        24
      ]
    },
    {
      "id": 25,
      "x": 0.569,
      "y": -0.497,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 24,
      "x": -0.574,
      "y": -0.497,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 0.893,
      "y": 1.021,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -0.892,
      "y": 1.023,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 28,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 63.0,
    "star2": 18.3,
    "star3": 18.2
  }
}