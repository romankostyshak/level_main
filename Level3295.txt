{
  "layout_cards": [
    {
      "id": 22,
      "x": -1.564,
      "y": -0.453,
      "angle": 45.0,
      "layer": 3,
      "closed_by": [
        51
      ]
    },
    {
      "id": 23,
      "x": 1.559,
      "y": -0.46,
      "angle": 315.0,
      "layer": 3,
      "closed_by": [
        49
      ]
    },
    {
      "id": 24,
      "x": 0.474,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 25,
      "x": -0.49,
      "y": 1.016,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 26,
      "x": -0.411,
      "y": 0.856,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 27,
      "x": -0.333,
      "y": 0.702,
      "layer": 1,
      "closed_by": [
        26
      ]
    },
    {
      "id": 28,
      "x": 0.333,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 29,
      "x": 0.402,
      "y": 0.851,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 30,
      "x": -0.1,
      "y": -0.458,
      "angle": 45.0,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 31,
      "x": -0.097,
      "y": -0.25,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 32,
      "x": 0.107,
      "y": -0.246,
      "angle": 45.0,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 33,
      "x": 0.115,
      "y": -0.469,
      "angle": 315.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -1.674,
      "y": 0.773,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -1.447,
      "y": 0.776,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 36,
      "x": -1.672,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 37,
      "x": -1.131,
      "y": -0.456,
      "angle": 45.0,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 38,
      "x": -1.45,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 39,
      "x": 1.133,
      "y": -0.458,
      "angle": 315.0,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 40,
      "x": -0.569,
      "y": 0.855,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 0.554,
      "y": 0.856,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 42,
      "x": 1.343,
      "y": -0.453,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 43,
      "x": -1.348,
      "y": -0.458,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 44,
      "x": 1.452,
      "y": 0.935,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 45,
      "x": 1.674,
      "y": 0.773,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 46,
      "x": 1.45,
      "y": 0.768,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 47,
      "x": 1.672,
      "y": 0.938,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 48,
      "x": -1.843,
      "y": -0.386,
      "angle": 315.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 49,
      "x": 1.774,
      "y": -0.456,
      "angle": 45.0,
      "layer": 4,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": 1.835,
      "y": -0.393,
      "angle": 45.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -1.771,
      "y": -0.458,
      "angle": 315.0,
      "layer": 4,
      "closed_by": [
        48
      ]
    }
  ],
  "cards_in_layout_amount": 30,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 64.7,
    "star2": 29.4,
    "star3": 5.6
  }
}