{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.398,
      "y": 0.49,
      "angle": 9.998,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": 0.331,
      "y": 0.513,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        49
      ]
    },
    {
      "id": 49,
      "x": 0.263,
      "y": 0.513,
      "angle": 30.0,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 48,
      "x": 1.054,
      "y": 0.462,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 47,
      "x": -0.971,
      "y": 0.059,
      "layer": 1,
      "closed_by": [
        23
      ]
    },
    {
      "id": 46,
      "x": 1.123,
      "y": 0.483,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 45,
      "x": 1.202,
      "y": 0.5,
      "angle": 340.0,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 44,
      "x": -1.108,
      "y": 0.104,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 43,
      "x": 1.273,
      "y": 0.493,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [
        25
      ]
    },
    {
      "id": 42,
      "x": -1.179,
      "y": 0.112,
      "angle": 29.999,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 41,
      "x": -0.386,
      "y": 0.063,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 40,
      "x": -0.326,
      "y": 0.101,
      "angle": 9.998,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 39,
      "x": -0.279,
      "y": 0.156,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 38,
      "x": -0.238,
      "y": 0.216,
      "angle": 30.0,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 37,
      "x": -0.004,
      "y": 0.379,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 0.465,
      "y": 0.462,
      "layer": 1,
      "closed_by": [
        51
      ]
    },
    {
      "id": 35,
      "x": 0.893,
      "y": -0.416,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 34,
      "x": -0.972,
      "y": 1.024,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 33,
      "x": 0.81,
      "y": -0.414,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 32,
      "x": -0.893,
      "y": 1.026,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 31,
      "x": 0.726,
      "y": -0.414,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 30,
      "x": -0.81,
      "y": 1.026,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 29,
      "x": 0.652,
      "y": -0.412,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 28,
      "x": -0.731,
      "y": 1.026,
      "layer": 4,
      "closed_by": [
        26
      ]
    },
    {
      "id": 27,
      "x": 0.572,
      "y": -0.412,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -0.652,
      "y": 1.026,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 1.468,
      "y": 0.541,
      "layer": 5,
      "closed_by": [
        22
      ]
    },
    {
      "id": 24,
      "x": -1.378,
      "y": 0.162,
      "layer": 5,
      "closed_by": [
        21
      ]
    },
    {
      "id": 23,
      "x": -1.036,
      "y": 0.086,
      "angle": 9.998,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 22,
      "x": 1.577,
      "y": 0.634,
      "angle": 20.0,
      "layer": 6,
      "closed_by": [
        20
      ]
    },
    {
      "id": 21,
      "x": -1.516,
      "y": 0.115,
      "angle": 340.0,
      "layer": 6,
      "closed_by": [
        19
      ]
    },
    {
      "id": 20,
      "x": 1.616,
      "y": 0.689,
      "angle": 30.0,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 19,
      "x": -1.582,
      "y": 0.111,
      "angle": 330.0,
      "layer": 7,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 33,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 27.7,
    "star2": 64.9,
    "star3": 6.7
  }
}