{
  "layout_cards": [
    {
      "id": 16,
      "x": -0.27,
      "y": -0.155,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 17,
      "x": -0.851,
      "y": 0.027,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 18,
      "x": 0.86,
      "y": 0.032,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 19,
      "x": 0.28,
      "y": 0.075,
      "layer": 5,
      "closed_by": [
        20
      ]
    },
    {
      "id": 20,
      "x": 0.279,
      "y": -0.158,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 21,
      "x": -0.27,
      "y": 0.074,
      "layer": 5,
      "closed_by": [
        16
      ]
    },
    {
      "id": 22,
      "x": -0.27,
      "y": -0.458,
      "layer": 1,
      "closed_by": [
        25
      ]
    },
    {
      "id": 23,
      "x": 0.282,
      "y": -0.455,
      "layer": 1,
      "closed_by": [
        24
      ]
    },
    {
      "id": 24,
      "x": 0.28,
      "y": -0.372,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 25,
      "x": -0.27,
      "y": -0.372,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 26,
      "x": -1.179,
      "y": 0.734,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        37,
        38
      ]
    },
    {
      "id": 27,
      "x": -0.001,
      "y": 0.944,
      "layer": 2,
      "closed_by": [
        35,
        36
      ]
    },
    {
      "id": 28,
      "x": 0.601,
      "y": 0.875,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        34,
        35
      ]
    },
    {
      "id": 29,
      "x": 1.189,
      "y": 0.736,
      "angle": 340.0,
      "layer": 2,
      "closed_by": [
        33,
        34
      ]
    },
    {
      "id": 30,
      "x": 1.728,
      "y": 0.479,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 31,
      "x": -0.606,
      "y": 0.878,
      "angle": 9.998,
      "layer": 2,
      "closed_by": [
        36,
        37
      ]
    },
    {
      "id": 32,
      "x": -1.72,
      "y": 0.486,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 33,
      "x": 1.434,
      "y": 0.209,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 34,
      "x": 0.855,
      "y": 0.372,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 35,
      "x": 0.28,
      "y": 0.532,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 36,
      "x": -0.272,
      "y": 0.531,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 37,
      "x": -0.851,
      "y": 0.37,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 38,
      "x": -1.437,
      "y": 0.206,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 39,
      "x": 0.703,
      "y": 0.907,
      "layer": 1,
      "closed_by": [
        28,
        29
      ]
    },
    {
      "id": 40,
      "x": 1.437,
      "y": 0.128,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 0.856,
      "y": 0.201,
      "layer": 4,
      "closed_by": [
        18
      ]
    },
    {
      "id": 42,
      "x": 0.28,
      "y": 0.305,
      "layer": 4,
      "closed_by": [
        19
      ]
    },
    {
      "id": 43,
      "x": -0.27,
      "y": 0.3,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 44,
      "x": -0.851,
      "y": 0.2,
      "layer": 4,
      "closed_by": [
        17
      ]
    },
    {
      "id": 45,
      "x": -1.434,
      "y": 0.128,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 46,
      "x": 1.343,
      "y": 0.782,
      "layer": 1,
      "closed_by": [
        29,
        30
      ]
    },
    {
      "id": 47,
      "x": 1.945,
      "y": 0.537,
      "layer": 1,
      "closed_by": [
        30
      ]
    },
    {
      "id": 48,
      "x": -0.002,
      "y": 1.016,
      "layer": 1,
      "closed_by": [
        27
      ]
    },
    {
      "id": 49,
      "x": -0.708,
      "y": 0.906,
      "layer": 1,
      "closed_by": [
        26,
        31
      ]
    },
    {
      "id": 50,
      "x": -1.342,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        26,
        32
      ]
    },
    {
      "id": 51,
      "x": -1.94,
      "y": 0.536,
      "layer": 1,
      "closed_by": [
        32
      ]
    }
  ],
  "cards_in_layout_amount": 36,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 37.9,
    "star2": 56.6,
    "star3": 4.7
  }
}