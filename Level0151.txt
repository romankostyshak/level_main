{
  "layout_cards": [
    {
      "id": 51,
      "y": 1.059,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": 0.418,
      "y": 1.059,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 49,
      "x": -0.418,
      "y": 1.059,
      "layer": 1,
      "closed_by": [
        51
      ]
    },
    {
      "id": 48,
      "x": 0.418,
      "y": 0.679,
      "layer": 4,
      "closed_by": [
        46
      ]
    },
    {
      "id": 47,
      "x": -0.418,
      "y": 0.679,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 46,
      "y": 0.679,
      "layer": 5,
      "closed_by": [
        47
      ]
    },
    {
      "id": 45,
      "x": 0.004,
      "y": -0.15,
      "layer": 1,
      "closed_by": [
        34,
        32
      ]
    },
    {
      "id": 44,
      "x": 2.263,
      "y": 1.072,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 43,
      "x": -1.975,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        42,
        28,
        27
      ]
    },
    {
      "id": 42,
      "x": -1.516,
      "y": -0.179,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 41,
      "x": -1.059,
      "y": -0.179,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 2.032,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        39,
        26,
        25
      ]
    },
    {
      "id": 39,
      "x": 1.567,
      "y": -0.179,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 38,
      "x": 1.133,
      "y": 0.961,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 37,
      "x": 1.133,
      "y": 1.052,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 36,
      "x": -1.118,
      "y": 0.874,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 35,
      "x": 1.1,
      "y": -0.179,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -0.275,
      "y": -0.279,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 33,
      "x": 0.006,
      "y": -0.495,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 0.289,
      "y": -0.279,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 31,
      "x": 1.133,
      "y": 0.873,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 30,
      "x": 1.136,
      "y": 0.773,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -2.21,
      "y": 1.07,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 28,
      "x": -2.213,
      "y": 0.259,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 27,
      "x": -2.213,
      "y": -0.569,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 26,
      "x": 2.265,
      "y": 0.259,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 2.263,
      "y": -0.569,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 2.427,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        23
      ]
    },
    {
      "id": 23,
      "x": 2.019,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        44,
        26
      ]
    },
    {
      "id": 22,
      "x": -1.957,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        29,
        28
      ]
    },
    {
      "id": 21,
      "x": -2.417,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 20,
      "x": -2.417,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 19,
      "x": 2.42,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 18,
      "x": -1.118,
      "y": 0.771,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 17,
      "x": -1.118,
      "y": 0.958,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 16,
      "x": -1.12,
      "y": 1.049,
      "layer": 1,
      "closed_by": [
        17
      ]
    }
  ],
  "cards_in_layout_amount": 36,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 1.9,
    "star2": 57.8,
    "star3": 39.5
  }
}