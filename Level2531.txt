{
  "layout_cards": [
    {
      "id": 11,
      "x": 0.187,
      "y": -0.216,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 12,
      "x": 0.001,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        11,
        16
      ]
    },
    {
      "id": 13,
      "x": -1.266,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        20
      ]
    },
    {
      "id": 14,
      "x": -1.264,
      "y": 0.86,
      "layer": 1,
      "closed_by": [
        19
      ]
    },
    {
      "id": 15,
      "x": 1.269,
      "y": 0.86,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 16,
      "x": -0.18,
      "y": 0.652,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 18,
      "x": 1.266,
      "y": -0.414,
      "layer": 1,
      "closed_by": [
        21
      ]
    },
    {
      "id": 19,
      "x": -1.585,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 20,
      "x": -1.585,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 21,
      "x": 1.585,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 22,
      "x": -1.424,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 23,
      "x": 1.424,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 24,
      "x": -1.427,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 25,
      "x": 1.424,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 26,
      "x": -0.972,
      "y": -0.337,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 27,
      "x": -0.972,
      "y": 0.777,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 0.972,
      "y": 0.782,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -1.292,
      "y": -0.261,
      "layer": 5,
      "closed_by": [
        26
      ]
    },
    {
      "id": 30,
      "x": -1.292,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        27
      ]
    },
    {
      "id": 31,
      "x": 1.296,
      "y": -0.256,
      "layer": 5,
      "closed_by": [
        37
      ]
    },
    {
      "id": 32,
      "x": 1.294,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        28
      ]
    },
    {
      "id": 33,
      "x": -0.703,
      "y": -0.259,
      "layer": 5,
      "closed_by": [
        26
      ]
    },
    {
      "id": 34,
      "x": 0.707,
      "y": -0.261,
      "layer": 5,
      "closed_by": [
        37
      ]
    },
    {
      "id": 35,
      "x": 0.707,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        28
      ]
    },
    {
      "id": 36,
      "x": -0.703,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        27
      ]
    },
    {
      "id": 37,
      "x": 0.975,
      "y": -0.335,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 1.585,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 39,
      "x": -0.545,
      "y": 0.224,
      "layer": 4,
      "closed_by": [
        16,
        33,
        36
      ]
    },
    {
      "id": 40,
      "x": 0.546,
      "y": 0.222,
      "layer": 4,
      "closed_by": [
        11,
        34,
        35
      ]
    },
    {
      "id": 41,
      "x": -0.331,
      "y": -0.261,
      "layer": 3,
      "closed_by": [
        12,
        39
      ]
    },
    {
      "id": 42,
      "x": 0.331,
      "y": -0.263,
      "layer": 3,
      "closed_by": [
        12,
        40
      ]
    },
    {
      "id": 43,
      "y": -0.423,
      "layer": 2,
      "closed_by": [
        41,
        42
      ]
    },
    {
      "id": 44,
      "x": -0.331,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 45,
      "x": -0.333,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        12,
        39
      ]
    },
    {
      "id": 46,
      "x": 0.335,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        12,
        40
      ]
    },
    {
      "id": 47,
      "x": 0.001,
      "y": 0.861,
      "layer": 2,
      "closed_by": [
        45,
        46
      ]
    },
    {
      "id": 48,
      "x": 0.333,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 49,
      "x": -0.331,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 50,
      "x": 0.333,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        34,
        43
      ]
    }
  ],
  "cards_in_layout_amount": 39,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 9.8,
    "star2": 58.7,
    "star3": 30.8
  }
}