{
  "layout_cards": [
    {
      "id": 14,
      "x": -2.499,
      "y": 0.238,
      "layer": 5,
      "closed_by": [
        25
      ]
    },
    {
      "id": 15,
      "x": 2.5,
      "y": 0.238,
      "layer": 5,
      "closed_by": [
        16
      ]
    },
    {
      "id": 16,
      "x": 2.657,
      "y": -0.158,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 18,
      "x": -2.338,
      "y": 0.638,
      "layer": 4,
      "closed_by": [
        14
      ]
    },
    {
      "id": 19,
      "x": 2.338,
      "y": 0.638,
      "layer": 4,
      "closed_by": [
        15
      ]
    },
    {
      "id": 20,
      "x": -2.18,
      "y": 1.041,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 21,
      "x": -2.019,
      "y": 0.638,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 22,
      "x": 2.019,
      "y": 0.638,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 23,
      "y": 0.081,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 25,
      "x": -2.66,
      "y": -0.158,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 26,
      "x": 0.279,
      "y": 0.321,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 27,
      "x": -0.279,
      "y": 0.321,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 28,
      "x": 1.7,
      "y": -0.178,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 29,
      "x": 1.539,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 30,
      "x": 1.378,
      "y": -0.178,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 31,
      "x": 1.22,
      "y": 0.238,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 1.059,
      "y": 0.638,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 33,
      "x": 0.898,
      "y": 1.041,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 34,
      "x": -1.858,
      "y": 0.238,
      "layer": 1,
      "closed_by": [
        21,
        35
      ]
    },
    {
      "id": 35,
      "x": -1.7,
      "y": -0.178,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 36,
      "x": 1.86,
      "y": 0.238,
      "layer": 1,
      "closed_by": [
        22,
        28,
        30
      ]
    },
    {
      "id": 37,
      "x": 2.177,
      "y": 1.041,
      "layer": 3,
      "closed_by": [
        19
      ]
    },
    {
      "id": 38,
      "x": -1.539,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 39,
      "x": -1.378,
      "y": -0.178,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 40,
      "x": -1.22,
      "y": 0.238,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 41,
      "x": -1.059,
      "y": 0.638,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 42,
      "x": -0.898,
      "y": 1.041,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 43,
      "x": -0.74,
      "y": 0.638,
      "layer": 2,
      "closed_by": [
        27,
        42
      ]
    },
    {
      "id": 44,
      "x": -0.379,
      "y": -0.178,
      "layer": 2,
      "closed_by": [
        27
      ]
    },
    {
      "id": 45,
      "x": 0.74,
      "y": 0.638,
      "layer": 2,
      "closed_by": [
        26,
        33
      ]
    },
    {
      "id": 46,
      "x": 0.578,
      "y": 0.238,
      "layer": 1,
      "closed_by": [
        45,
        50
      ]
    },
    {
      "id": 47,
      "x": -0.578,
      "y": 0.238,
      "layer": 1,
      "closed_by": [
        43,
        44
      ]
    },
    {
      "id": 48,
      "y": 0.8,
      "layer": 1,
      "closed_by": [
        26,
        27
      ]
    },
    {
      "id": 49,
      "y": -0.317,
      "layer": 1,
      "closed_by": [
        44,
        50
      ]
    },
    {
      "id": 50,
      "x": 0.379,
      "y": -0.178,
      "layer": 2,
      "closed_by": [
        26
      ]
    }
  ],
  "cards_in_layout_amount": 35,
  "cards_in_deck_amount": 26,
  "layers_in_level": 6,
  "open_cards": 5,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 16.8,
    "star2": 76.3,
    "star3": 6.8
  }
}