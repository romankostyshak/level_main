{
  "layout_cards": [
    {
      "id": 49,
      "x": 2.144,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 48,
      "x": 1.679,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 45,
      "x": 0.287,
      "y": 1.019,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -0.28,
      "y": -0.574,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 44,
      "x": -2.144,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 43,
      "x": -1.677,
      "y": -0.574,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 41,
      "x": -0.745,
      "y": -0.574,
      "layer": 4,
      "closed_by": [
        51,
        39
      ]
    },
    {
      "id": 46,
      "x": 0.74,
      "y": 1.018,
      "layer": 4,
      "closed_by": [
        45,
        40
      ]
    },
    {
      "id": 40,
      "x": 0.98,
      "y": 0.62,
      "layer": 5,
      "closed_by": [
        35
      ]
    },
    {
      "id": 39,
      "x": -0.976,
      "y": -0.179,
      "layer": 5,
      "closed_by": [
        36
      ]
    },
    {
      "id": 31,
      "x": -0.275,
      "y": 0.238,
      "layer": 6,
      "closed_by": [
        33
      ]
    },
    {
      "id": 33,
      "x": -0.578,
      "y": 0.238,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -0.976,
      "y": 0.238,
      "layer": 6,
      "closed_by": [
        33
      ]
    },
    {
      "id": 42,
      "x": -1.213,
      "y": -0.574,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 47,
      "x": 1.213,
      "y": 1.021,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 34,
      "x": 0.578,
      "y": 0.2,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 0.98,
      "y": 0.2,
      "layer": 6,
      "closed_by": [
        34
      ]
    },
    {
      "id": 32,
      "x": 0.286,
      "y": 0.2,
      "layer": 6,
      "closed_by": [
        34
      ]
    },
    {
      "id": 30,
      "x": 0.291,
      "y": -0.497,
      "layer": 5,
      "closed_by": [
        32
      ]
    },
    {
      "id": 29,
      "x": -0.277,
      "y": 0.939,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 28,
      "x": 0.707,
      "y": -0.497,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 37,
      "x": -0.703,
      "y": 0.944,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 27,
      "x": 0.976,
      "y": -0.497,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 26,
      "x": -0.98,
      "y": 0.943,
      "layer": 3,
      "closed_by": [
        37
      ]
    }
  ],
  "cards_in_layout_amount": 24,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 55.7,
    "star2": 5.8,
    "star3": 37.9
  }
}