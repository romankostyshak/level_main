{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.001,
      "y": -0.572,
      "layer": 3,
      "closed_by": [
        40,
        35
      ]
    },
    {
      "id": 50,
      "x": 0.545,
      "y": 0.231,
      "layer": 3,
      "closed_by": [
        49,
        47,
        40,
        38
      ]
    },
    {
      "id": 49,
      "x": 0.305,
      "y": 0.939,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 48,
      "x": -0.305,
      "y": 0.939,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 47,
      "x": 0.971,
      "y": 0.652,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 46,
      "x": -0.962,
      "y": -0.185,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 45,
      "x": -0.412,
      "y": 0.652,
      "layer": 2,
      "closed_by": [
        42,
        34
      ]
    },
    {
      "id": 44,
      "x": 0.418,
      "y": 0.652,
      "layer": 2,
      "closed_by": [
        50,
        34
      ]
    },
    {
      "id": 43,
      "x": 1.08,
      "y": 0.231,
      "layer": 3,
      "closed_by": [
        47,
        38
      ]
    },
    {
      "id": 42,
      "x": -0.549,
      "y": 0.231,
      "layer": 3,
      "closed_by": [
        48,
        46,
        39,
        35
      ]
    },
    {
      "id": 41,
      "x": -1.077,
      "y": 0.231,
      "layer": 3,
      "closed_by": [
        46,
        39
      ]
    },
    {
      "id": 40,
      "x": 0.305,
      "y": -0.497,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -0.962,
      "y": 0.652,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 0.972,
      "y": -0.185,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 37,
      "x": 0.418,
      "y": -0.185,
      "layer": 2,
      "closed_by": [
        51,
        50
      ]
    },
    {
      "id": 36,
      "x": -0.411,
      "y": -0.185,
      "layer": 2,
      "closed_by": [
        51,
        42
      ]
    },
    {
      "id": 35,
      "x": -0.305,
      "y": -0.495,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -0.001,
      "y": 1.021,
      "layer": 3,
      "closed_by": [
        49,
        48
      ]
    },
    {
      "id": 33,
      "x": 0.331,
      "y": 0.231,
      "layer": 1,
      "closed_by": [
        44,
        37
      ]
    },
    {
      "id": 32,
      "x": -0.324,
      "y": 0.231,
      "layer": 1,
      "closed_by": [
        45,
        36
      ]
    }
  ],
  "cards_in_layout_amount": 20,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 16.0,
    "star2": 0.0,
    "star3": 83.3
  }
}