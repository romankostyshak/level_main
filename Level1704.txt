{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.004,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": 0.004,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 49,
      "x": 0.526,
      "y": 0.221,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 48,
      "x": -0.254,
      "y": -0.172,
      "layer": 4,
      "closed_by": [
        38,
        32
      ]
    },
    {
      "id": 47,
      "x": 0.266,
      "y": -0.172,
      "layer": 4,
      "closed_by": [
        34,
        32
      ]
    },
    {
      "id": 46,
      "x": 1.184,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 45,
      "x": 1.184,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 44,
      "x": -1.179,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 43,
      "x": 1.243,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 42,
      "x": 1.243,
      "y": -0.259,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 41,
      "x": -1.24,
      "y": -0.259,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 40,
      "x": -1.457,
      "y": 0.2,
      "layer": 5,
      "closed_by": [
        22
      ]
    },
    {
      "id": 38,
      "x": -0.456,
      "y": 0.273,
      "layer": 5,
      "closed_by": [
        37
      ]
    },
    {
      "id": 37,
      "x": -0.404,
      "y": 0.328,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 0.418,
      "y": 0.326,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -0.509,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 34,
      "x": 0.479,
      "y": 0.272,
      "layer": 5,
      "closed_by": [
        36
      ]
    },
    {
      "id": 33,
      "x": 0.008,
      "y": 0.222,
      "layer": 3,
      "closed_by": [
        48,
        47
      ]
    },
    {
      "id": 32,
      "x": 0.004,
      "y": -0.518,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 1.133,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        30,
        29
      ]
    },
    {
      "id": 30,
      "x": 1.133,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 29,
      "x": 1.133,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 28,
      "x": 1.463,
      "y": 0.18,
      "layer": 5,
      "closed_by": [
        39
      ]
    },
    {
      "id": 27,
      "x": -1.12,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        26
      ]
    },
    {
      "id": 26,
      "x": -1.179,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 25,
      "x": -1.24,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 24,
      "x": -1.118,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 23,
      "x": -1.12,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        27,
        24
      ]
    },
    {
      "id": 22,
      "x": -1.503,
      "y": 0.2,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 39,
      "x": 1.508,
      "y": 0.18,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 20,
      "x": 1.822,
      "y": 0.615,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 19,
      "x": 1.825,
      "y": -0.256,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 18,
      "x": -1.827,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 15,
      "x": -1.825,
      "y": -0.254,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 14,
      "x": 1.906,
      "y": 0.142,
      "layer": 3,
      "closed_by": [
        20,
        19
      ]
    },
    {
      "id": 16,
      "x": -1.985,
      "y": 0.136,
      "layer": 2,
      "closed_by": [
        13
      ]
    },
    {
      "id": 12,
      "x": 1.985,
      "y": 0.142,
      "layer": 2,
      "closed_by": [
        39,
        14
      ]
    },
    {
      "id": 11,
      "x": 2.065,
      "y": 0.142,
      "layer": 1,
      "closed_by": [
        12
      ]
    },
    {
      "id": 13,
      "x": -1.904,
      "y": 0.136,
      "layer": 3,
      "closed_by": [
        18,
        15
      ]
    },
    {
      "id": 10,
      "x": -2.065,
      "y": 0.136,
      "layer": 1,
      "closed_by": [
        16
      ]
    }
  ],
  "cards_in_layout_amount": 40,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 3.5,
    "star2": 50.0,
    "star3": 45.5
  }
}