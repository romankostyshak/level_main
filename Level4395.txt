{
  "layout_cards": [
    {
      "id": 22,
      "x": 1.559,
      "y": 1.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 1.72,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 24,
      "x": 1.72,
      "y": 0.217,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 25,
      "x": -1.58,
      "y": -0.559,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -1.74,
      "y": 0.217,
      "layer": 2,
      "closed_by": [
        27
      ]
    },
    {
      "id": 27,
      "x": -1.74,
      "y": -0.179,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 28,
      "x": 1.559,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        24
      ]
    },
    {
      "id": 29,
      "x": -1.039,
      "y": -0.559,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 1.019,
      "y": 1.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 0.759,
      "y": 1.0,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 32,
      "x": -0.777,
      "y": -0.559,
      "layer": 5,
      "closed_by": [
        29
      ]
    },
    {
      "id": 33,
      "x": -0.518,
      "y": -0.559,
      "layer": 4,
      "closed_by": [
        32
      ]
    },
    {
      "id": 34,
      "x": 0.5,
      "y": 1.0,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 35,
      "x": 0.238,
      "y": 1.0,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 36,
      "x": -1.58,
      "y": 0.62,
      "layer": 1,
      "closed_by": [
        26
      ]
    },
    {
      "id": 38,
      "x": -0.259,
      "y": -0.559,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 39,
      "x": -1.039,
      "y": 0.62,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 1.019,
      "y": -0.179,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 41,
      "x": -0.777,
      "y": 0.62,
      "layer": 5,
      "closed_by": [
        39
      ]
    },
    {
      "id": 42,
      "x": 0.759,
      "y": -0.179,
      "layer": 5,
      "closed_by": [
        40
      ]
    },
    {
      "id": 43,
      "x": 0.5,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 44,
      "x": -0.499,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 45,
      "x": 0.68,
      "y": 0.259,
      "angle": 315.0,
      "layer": 3,
      "closed_by": [
        34,
        43
      ]
    },
    {
      "id": 46,
      "x": -0.319,
      "y": -0.238,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        38,
        50
      ]
    },
    {
      "id": 47,
      "x": 0.319,
      "y": 0.657,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        35,
        45
      ]
    },
    {
      "id": 48,
      "x": -0.499,
      "y": 0.217,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 49,
      "x": 0.5,
      "y": 0.217,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 50,
      "x": -0.68,
      "y": 0.18,
      "angle": 315.0,
      "layer": 3,
      "closed_by": [
        33,
        44
      ]
    }
  ],
  "cards_in_layout_amount": 28,
  "cards_in_deck_amount": 26,
  "layers_in_level": 6,
  "open_cards": 6,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 33.2,
    "star2": 61.5,
    "star3": 4.3
  }
}