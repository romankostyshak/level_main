{
  "layout_cards": [
    {
      "id": 12,
      "x": -1.092,
      "y": 0.842,
      "angle": 349.997,
      "layer": 1,
      "closed_by": [
        21
      ]
    },
    {
      "id": 13,
      "x": 1.274,
      "y": 0.395,
      "angle": 349.997,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 14,
      "x": 1.659,
      "y": -0.518,
      "angle": 349.997,
      "layer": 1,
      "closed_by": [
        27
      ]
    },
    {
      "id": 15,
      "x": 1.802,
      "y": 0.298,
      "angle": 349.997,
      "layer": 1,
      "closed_by": [
        28
      ]
    },
    {
      "id": 16,
      "x": 1.131,
      "y": -0.425,
      "angle": 349.997,
      "layer": 1,
      "closed_by": [
        24
      ]
    },
    {
      "id": 17,
      "x": -1.628,
      "y": 0.93,
      "angle": 349.997,
      "layer": 1,
      "closed_by": [
        20
      ]
    },
    {
      "id": 18,
      "x": -1.771,
      "y": 0.114,
      "angle": 349.997,
      "layer": 1,
      "closed_by": [
        23
      ]
    },
    {
      "id": 19,
      "x": -1.235,
      "y": 0.017,
      "angle": 349.997,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 20,
      "x": -1.664,
      "y": 0.986,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 21,
      "x": -1.042,
      "y": 0.879,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 22,
      "x": -1.202,
      "y": -0.028,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 23,
      "x": -1.824,
      "y": 0.074,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 24,
      "x": 1.077,
      "y": -0.465,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 25,
      "x": 0.014,
      "y": 0.209,
      "angle": 349.997,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -1.077,
      "y": 0.411,
      "angle": 349.997,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 27,
      "x": 1.692,
      "y": -0.574,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 28,
      "x": 1.853,
      "y": 0.34,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 29,
      "x": 1.241,
      "y": 0.451,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 30,
      "x": -1.743,
      "y": 0.526,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 31,
      "x": -1.121,
      "y": 0.421,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 32,
      "x": 1.161,
      "y": -0.016,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 33,
      "x": 1.771,
      "y": -0.134,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 34,
      "x": -1.792,
      "y": 0.536,
      "angle": 349.997,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 35,
      "x": 1.116,
      "y": -0.008,
      "angle": 349.997,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 36,
      "x": 1.817,
      "y": -0.141,
      "angle": 349.997,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 37,
      "x": 1.07,
      "y": -0.002,
      "angle": 349.997,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -1.034,
      "y": 0.402,
      "angle": 349.997,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 39,
      "x": 1.86,
      "y": -0.15,
      "angle": 349.997,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 40,
      "x": -1.835,
      "y": 0.545,
      "angle": 349.997,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 41,
      "x": -0.513,
      "y": 0.307,
      "angle": 349.997,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 42,
      "x": 0.55,
      "y": 0.104,
      "angle": 349.997,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 43,
      "x": -0.497,
      "y": 0.097,
      "angle": 9.998,
      "layer": 4,
      "closed_by": [
        25,
        38,
        41
      ]
    },
    {
      "id": 44,
      "x": 0.017,
      "y": 0.187,
      "angle": 9.998,
      "layer": 4,
      "closed_by": [
        25,
        41,
        42
      ]
    },
    {
      "id": 45,
      "x": -0.5,
      "y": 0.097,
      "angle": 9.998,
      "layer": 2,
      "closed_by": [
        47,
        48
      ]
    },
    {
      "id": 46,
      "x": 0.527,
      "y": 0.3,
      "angle": 9.998,
      "layer": 4,
      "closed_by": [
        25,
        37,
        42
      ]
    },
    {
      "id": 47,
      "x": -0.512,
      "y": 0.305,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        43,
        44
      ]
    },
    {
      "id": 48,
      "x": 0.014,
      "y": 0.206,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        43,
        44,
        46
      ]
    },
    {
      "id": 49,
      "x": 0.55,
      "y": 0.104,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        44,
        46
      ]
    },
    {
      "id": 50,
      "x": 0.016,
      "y": 0.187,
      "angle": 10.0,
      "layer": 2,
      "closed_by": [
        47,
        48,
        49
      ]
    },
    {
      "id": 51,
      "x": 0.527,
      "y": 0.3,
      "angle": 9.998,
      "layer": 2,
      "closed_by": [
        48,
        49
      ]
    }
  ],
  "cards_in_layout_amount": 40,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 14.0,
    "star2": 71.9,
    "star3": 13.8
  }
}