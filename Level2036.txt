{
  "layout_cards": [
    {
      "id": 20,
      "x": -0.337,
      "y": 0.228,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 21,
      "x": -0.3,
      "y": 0.23,
      "layer": 3,
      "closed_by": [
        20
      ]
    },
    {
      "id": 22,
      "x": 0.298,
      "y": 0.224,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 23,
      "x": -0.261,
      "y": 0.228,
      "layer": 2,
      "closed_by": [
        21
      ]
    },
    {
      "id": 24,
      "x": 0.261,
      "y": 0.224,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 25,
      "x": -0.003,
      "y": 0.229,
      "layer": 1,
      "closed_by": [
        23,
        24
      ]
    },
    {
      "id": 26,
      "y": -0.595,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 0.333,
      "y": -0.592,
      "layer": 5,
      "closed_by": [
        26
      ]
    },
    {
      "id": 28,
      "x": 0.574,
      "y": -0.592,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 29,
      "x": -0.573,
      "y": -0.592,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 30,
      "x": 0.972,
      "y": -0.592,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 31,
      "x": -0.972,
      "y": -0.592,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 32,
      "x": -1.371,
      "y": -0.592,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 33,
      "x": 1.373,
      "y": -0.592,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 34,
      "x": -0.331,
      "y": -0.592,
      "layer": 5,
      "closed_by": [
        26
      ]
    },
    {
      "id": 35,
      "x": 0.337,
      "y": 0.224,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 37,
      "x": 1.554,
      "y": -0.136,
      "angle": 315.0,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 38,
      "x": -1.559,
      "y": -0.135,
      "angle": 45.0,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 39,
      "x": -1.164,
      "y": 0.34,
      "angle": 45.0,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 1.164,
      "y": 0.333,
      "angle": 315.0,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 41,
      "x": -0.995,
      "y": 0.615,
      "angle": 25.0,
      "layer": 6,
      "closed_by": [
        39
      ]
    },
    {
      "id": 42,
      "x": 1.008,
      "y": 0.606,
      "angle": 330.0,
      "layer": 6,
      "closed_by": [
        40
      ]
    },
    {
      "id": 43,
      "x": 0.949,
      "y": 0.912,
      "angle": 344.997,
      "layer": 5,
      "closed_by": [
        42
      ]
    },
    {
      "id": 44,
      "x": -0.921,
      "y": 0.92,
      "angle": 9.998,
      "layer": 5,
      "closed_by": [
        41
      ]
    },
    {
      "id": 45,
      "x": 0.865,
      "y": 0.303,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 46,
      "x": -0.865,
      "y": 0.303,
      "layer": 4,
      "closed_by": [
        44
      ]
    },
    {
      "id": 47,
      "x": -0.864,
      "y": 1.042,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": 0.865,
      "y": 1.047,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 49,
      "x": -0.465,
      "y": 1.044,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 50,
      "x": 0.467,
      "y": 1.047,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 51,
      "y": 1.047,
      "layer": 1,
      "closed_by": [
        49,
        50
      ]
    }
  ],
  "cards_in_layout_amount": 31,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 35.6,
    "star2": 55.4,
    "star3": 8.7
  }
}