{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.386,
      "y": 0.541,
      "layer": 1,
      "closed_by": [
        49,
        42
      ]
    },
    {
      "id": 50,
      "x": 0.414,
      "y": 0.059,
      "layer": 1,
      "closed_by": [
        43,
        48
      ]
    },
    {
      "id": 49,
      "x": -0.331,
      "y": 0.379,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "x": -0.256,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        45,
        44
      ]
    },
    {
      "id": 46,
      "x": 0.259,
      "y": 0.381,
      "layer": 3,
      "closed_by": [
        45,
        44
      ]
    },
    {
      "id": 45,
      "y": 0.777,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 44,
      "y": -0.172,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 43,
      "x": 0.851,
      "y": 0.224,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 41,
      "x": 1.452,
      "y": 0.223,
      "layer": 2,
      "closed_by": [
        36,
        37
      ]
    },
    {
      "id": 39,
      "x": 2.013,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 38,
      "x": -2.012,
      "y": 0.379,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 36,
      "x": 1.771,
      "y": 0.061,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 35,
      "x": -1.131,
      "y": 0.221,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 34,
      "x": -1.745,
      "y": 0.541,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 32,
      "x": -1.411,
      "y": 0.381,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 2.065,
      "y": 0.221,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 30,
      "x": -2.065,
      "y": 0.379,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 33,
      "x": 1.414,
      "y": 0.229,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 40,
      "x": -1.45,
      "y": 0.381,
      "layer": 2,
      "closed_by": [
        35,
        34
      ]
    },
    {
      "id": 37,
      "x": 1.131,
      "y": 0.381,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 42,
      "x": -0.847,
      "y": 0.384,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 48,
      "x": 0.333,
      "y": 0.223,
      "layer": 2,
      "closed_by": [
        46
      ]
    }
  ],
  "cards_in_layout_amount": 22,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 43.5,
    "star2": 4.1,
    "star3": 51.4
  }
}