{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.652,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        41,
        32
      ]
    },
    {
      "id": 50,
      "x": 0.984,
      "y": 0.619,
      "layer": 2,
      "closed_by": [
        46,
        39
      ]
    },
    {
      "id": 49,
      "x": 0.335,
      "y": 0.619,
      "layer": 2,
      "closed_by": [
        46,
        44
      ]
    },
    {
      "id": 48,
      "x": -1.664,
      "y": 0.619,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 47,
      "x": -0.652,
      "y": 0.3,
      "layer": 3,
      "closed_by": [
        37,
        38
      ]
    },
    {
      "id": 46,
      "x": 0.652,
      "y": 0.303,
      "layer": 3,
      "closed_by": [
        36,
        26
      ]
    },
    {
      "id": 45,
      "x": 1.664,
      "y": 0.619,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 44,
      "x": -0.001,
      "y": 0.3,
      "layer": 3,
      "closed_by": [
        36,
        38
      ]
    },
    {
      "id": 43,
      "x": 0.652,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        50,
        49
      ]
    },
    {
      "id": 42,
      "x": -1.294,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        48,
        41
      ]
    },
    {
      "id": 41,
      "x": -0.986,
      "y": 0.619,
      "layer": 2,
      "closed_by": [
        47,
        35
      ]
    },
    {
      "id": 40,
      "x": 1.294,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        50,
        45
      ]
    },
    {
      "id": 39,
      "x": 1.294,
      "y": 0.303,
      "layer": 3,
      "closed_by": [
        33,
        26
      ]
    },
    {
      "id": 37,
      "x": -0.972,
      "y": 0.143,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 0.331,
      "y": 0.064,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -1.294,
      "y": 0.3,
      "layer": 3,
      "closed_by": [
        37,
        34
      ]
    },
    {
      "id": 34,
      "x": -1.664,
      "y": 0.141,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 1.666,
      "y": 0.142,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -0.323,
      "y": 0.619,
      "layer": 2,
      "closed_by": [
        47,
        44
      ]
    },
    {
      "id": 31,
      "x": -0.259,
      "y": 0.939,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 0.263,
      "y": 0.939,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 29,
      "x": 1.664,
      "y": 1.019,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 28,
      "x": -1.661,
      "y": 1.019,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 26,
      "x": 0.972,
      "y": 0.136,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 0.574,
      "y": -0.574,
      "layer": 3,
      "closed_by": [
        36,
        26
      ]
    },
    {
      "id": 24,
      "x": -0.569,
      "y": -0.574,
      "layer": 3,
      "closed_by": [
        37,
        38
      ]
    },
    {
      "id": 23,
      "x": 1.667,
      "y": -0.574,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 22,
      "x": -1.664,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 21,
      "x": 1.131,
      "y": -0.574,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 20,
      "x": -1.131,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 38,
      "x": -0.331,
      "y": 0.059,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 27,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        36,
        38
      ]
    }
  ],
  "cards_in_layout_amount": 32,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 14.0,
    "star2": 72.9,
    "star3": 12.5
  }
}