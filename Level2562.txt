{
  "layout_cards": [
    {
      "x": -1.904,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        3,
        4
      ]
    },
    {
      "id": 3,
      "x": -1.825,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 4,
      "x": -1.825,
      "y": -0.178,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 5,
      "x": 1.827,
      "y": -0.172,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 6,
      "x": 1.827,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 7,
      "x": 1.906,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        5,
        6
      ]
    },
    {
      "id": 8,
      "x": -0.532,
      "y": 0.939,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 9,
      "x": -0.531,
      "y": -0.333,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 10,
      "x": 0.523,
      "y": -0.337,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 11,
      "x": -1.667,
      "y": -0.331,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 12,
      "x": -1.667,
      "y": 0.939,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 13,
      "x": 1.669,
      "y": 0.939,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 14,
      "x": -0.384,
      "y": -0.335,
      "layer": 5,
      "closed_by": [
        9
      ]
    },
    {
      "id": 15,
      "x": 0.386,
      "y": -0.335,
      "layer": 5,
      "closed_by": [
        10
      ]
    },
    {
      "id": 16,
      "x": 1.667,
      "y": -0.333,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 17,
      "x": 0.527,
      "y": 0.939,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 18,
      "x": -0.393,
      "y": 0.939,
      "layer": 5,
      "closed_by": [
        8
      ]
    },
    {
      "id": 19,
      "x": 0.386,
      "y": 0.939,
      "layer": 5,
      "closed_by": [
        17
      ]
    },
    {
      "id": 20,
      "x": -1.424,
      "y": -0.333,
      "layer": 5,
      "closed_by": [
        11
      ]
    },
    {
      "id": 21,
      "x": 1.427,
      "y": -0.333,
      "layer": 5,
      "closed_by": [
        16
      ]
    },
    {
      "id": 22,
      "x": 1.427,
      "y": 0.939,
      "layer": 5,
      "closed_by": [
        13
      ]
    },
    {
      "id": 23,
      "x": -1.049,
      "y": -0.335,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 24,
      "x": -1.047,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 25,
      "x": 1.044,
      "y": -0.333,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 26,
      "x": 1.046,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 27,
      "x": 0.003,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        18,
        19
      ]
    },
    {
      "id": 28,
      "x": -1.309,
      "y": -0.118,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 29,
      "x": -1.307,
      "y": 0.712,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 30,
      "x": 1.304,
      "y": -0.118,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 31,
      "x": 1.307,
      "y": 0.722,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 32,
      "x": -0.782,
      "y": -0.114,
      "layer": 3,
      "closed_by": [
        14,
        23
      ]
    },
    {
      "id": 33,
      "x": -0.787,
      "y": 0.717,
      "layer": 3,
      "closed_by": [
        18,
        24
      ]
    },
    {
      "id": 34,
      "x": 0.786,
      "y": -0.123,
      "layer": 3,
      "closed_by": [
        15,
        25
      ]
    },
    {
      "id": 35,
      "x": 0.787,
      "y": 0.72,
      "layer": 3,
      "closed_by": [
        19,
        26
      ]
    },
    {
      "id": 36,
      "x": 0.002,
      "y": -0.333,
      "layer": 4,
      "closed_by": [
        14,
        15
      ]
    },
    {
      "id": 37,
      "x": -1.424,
      "y": 0.939,
      "layer": 5,
      "closed_by": [
        12
      ]
    },
    {
      "id": 38,
      "x": -0.256,
      "y": 0.717,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 39,
      "x": 0.263,
      "y": 0.717,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 40,
      "x": 0.266,
      "y": -0.119,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 41,
      "x": -0.259,
      "y": -0.118,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 42,
      "x": -1.105,
      "y": 0.3,
      "layer": 2,
      "closed_by": [
        28,
        29,
        32,
        33
      ]
    },
    {
      "id": 43,
      "x": 1.105,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        30,
        31,
        34,
        35
      ]
    },
    {
      "id": 44,
      "x": -0.944,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        42,
        46
      ]
    },
    {
      "id": 45,
      "x": 0.949,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        43,
        47
      ]
    },
    {
      "id": 46,
      "x": -0.545,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        32,
        33,
        38,
        41
      ]
    },
    {
      "id": 47,
      "x": 0.549,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        34,
        35,
        39,
        40
      ]
    },
    {
      "id": 48,
      "x": -0.331,
      "y": 0.3,
      "layer": 1,
      "closed_by": [
        46,
        50
      ]
    },
    {
      "id": 49,
      "x": 0.331,
      "y": 0.3,
      "layer": 1,
      "closed_by": [
        47,
        50
      ]
    },
    {
      "id": 50,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        38,
        39,
        40,
        41
      ]
    }
  ],
  "cards_in_layout_amount": 49,
  "cards_in_deck_amount": 32,
  "stars_statistic": {
    "star1": 0.3,
    "star2": 8.5,
    "star3": 90.7
  }
}