{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.333,
      "y": -0.495,
      "layer": 1,
      "closed_by": [
        37
      ]
    },
    {
      "id": 50,
      "x": -0.256,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        49,
        35
      ]
    },
    {
      "id": 49,
      "x": -0.703,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 48,
      "x": -0.625,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 47,
      "x": -0.331,
      "y": -0.499,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 45,
      "x": -0.331,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 42,
      "x": 0.703,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 41,
      "x": 0.703,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 40,
      "x": -0.625,
      "y": 0.861,
      "layer": 4,
      "closed_by": [
        19
      ]
    },
    {
      "id": 38,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        15
      ]
    },
    {
      "id": 37,
      "x": 0.259,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        42,
        35
      ]
    },
    {
      "id": 36,
      "x": -0.703,
      "y": 1.018,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 34,
      "x": 0.335,
      "y": 0.938,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 33,
      "x": 1.452,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 32,
      "x": 1.758,
      "y": 0.625,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 31,
      "x": -1.452,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        30
      ]
    },
    {
      "id": 30,
      "x": -1.764,
      "y": 0.624,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 29,
      "x": 1.452,
      "y": 0.224,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 28,
      "x": -1.452,
      "y": 0.228,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 27,
      "x": 1.773,
      "y": -0.172,
      "layer": 4,
      "closed_by": [
        25
      ]
    },
    {
      "id": 26,
      "x": -1.771,
      "y": -0.172,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 25,
      "x": 1.452,
      "y": -0.574,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 24,
      "x": -1.452,
      "y": -0.574,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 44,
      "x": -0.256,
      "y": 0.787,
      "layer": 2,
      "closed_by": [
        38,
        36
      ]
    },
    {
      "id": 46,
      "x": 0.261,
      "y": 0.785,
      "layer": 2,
      "closed_by": [
        41,
        38
      ]
    },
    {
      "id": 39,
      "x": 0.62,
      "y": 0.861,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 43,
      "x": 0.625,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 35,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 21,
      "x": 0.703,
      "y": -0.256,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -0.708,
      "y": -0.254,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 20,
      "x": 0.703,
      "y": 0.699,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 19,
      "x": -0.703,
      "y": 0.698,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 18,
      "x": 0.001,
      "y": -0.574,
      "layer": 4,
      "closed_by": [
        14
      ]
    },
    {
      "id": 15,
      "y": 1.026,
      "layer": 4,
      "closed_by": [
        13
      ]
    },
    {
      "id": 14,
      "y": -0.259,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 13,
      "x": 0.001,
      "y": 0.699,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 36,
  "cards_in_deck_amount": 28,
  "layers_in_level": 5,
  "open_cards": 8,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 1.5,
    "star2": 27.6,
    "star3": 70.1
  }
}