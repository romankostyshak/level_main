{
  "layout_cards": [
    {
      "id": 49,
      "x": -0.648,
      "y": 0.86,
      "layer": 1,
      "closed_by": [
        47,
        45
      ]
    },
    {
      "id": 48,
      "x": -0.004,
      "y": 0.859,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "x": -0.323,
      "y": 0.625,
      "layer": 2,
      "closed_by": [
        50,
        41
      ]
    },
    {
      "id": 45,
      "x": -0.911,
      "y": 0.625,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": -0.625,
      "y": 0.3,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 43,
      "x": 0.629,
      "y": 0.305,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 46,
      "x": 0.307,
      "y": 0.625,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -0.303,
      "y": -0.093,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 39,
      "x": 0.3,
      "y": -0.093,
      "layer": 2,
      "closed_by": [
        43,
        41
      ]
    },
    {
      "id": 40,
      "x": 0.901,
      "y": -0.09,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 44,
      "y": -0.335,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 38,
      "x": 0.625,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        39,
        40
      ]
    },
    {
      "id": 41,
      "y": 0.303,
      "layer": 3,
      "closed_by": [
        46,
        42
      ]
    },
    {
      "id": 34,
      "x": -1.213,
      "y": -0.261,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 35,
      "x": 1.215,
      "y": 0.782,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 33,
      "x": -1.452,
      "y": 0.14,
      "layer": 2,
      "closed_by": [
        30,
        28
      ]
    },
    {
      "id": 31,
      "x": 1.667,
      "y": 0.781,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 32,
      "x": 1.452,
      "y": 0.381,
      "layer": 2,
      "closed_by": [
        31,
        29
      ]
    },
    {
      "id": 30,
      "x": -1.664,
      "y": -0.261,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 29,
      "x": 1.666,
      "y": -0.093,
      "layer": 3,
      "closed_by": [
        36,
        26
      ]
    },
    {
      "id": 28,
      "x": -1.664,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        27,
        25
      ]
    },
    {
      "id": 36,
      "x": 1.455,
      "y": -0.335,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 27,
      "x": -1.452,
      "y": 0.861,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 26,
      "x": 1.745,
      "y": 0.545,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 25,
      "x": -1.743,
      "y": -0.017,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 24,
      "x": 1.743,
      "y": -0.172,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 23,
      "x": -1.745,
      "y": 0.699,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 27,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 64.5,
    "star2": 19.7,
    "star3": 14.9
  }
}