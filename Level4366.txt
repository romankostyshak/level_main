{
  "layout_cards": [
    {
      "id": 26,
      "x": -1.498,
      "y": 0.777,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 1.5,
      "y": 0.777,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 1.22,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 29,
      "x": -0.939,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 30,
      "x": 0.939,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 31,
      "y": 0.518,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -0.097,
      "y": 0.578,
      "angle": 345.0,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 33,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 34,
      "x": 0.1,
      "y": 0.578,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 35,
      "y": -0.458,
      "layer": 8,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -1.22,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        26
      ]
    },
    {
      "id": 38,
      "x": 0.18,
      "y": -0.458,
      "angle": 5.0,
      "layer": 7,
      "closed_by": [
        35
      ]
    },
    {
      "id": 39,
      "x": -0.18,
      "y": -0.458,
      "angle": 355.0,
      "layer": 6,
      "closed_by": [
        38
      ]
    },
    {
      "id": 40,
      "x": -0.479,
      "y": -0.395,
      "angle": 345.0,
      "layer": 5,
      "closed_by": [
        35,
        39
      ]
    },
    {
      "id": 41,
      "x": 0.479,
      "y": -0.395,
      "angle": 15.0,
      "layer": 5,
      "closed_by": [
        38
      ]
    },
    {
      "id": 42,
      "x": -0.759,
      "y": -0.275,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 43,
      "x": 0.759,
      "y": -0.275,
      "angle": 30.0,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 44,
      "x": -1.019,
      "y": -0.075,
      "angle": 315.0,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 45,
      "x": -0.74,
      "y": 0.398,
      "angle": 345.0,
      "layer": 2,
      "closed_by": [
        29,
        44
      ]
    },
    {
      "id": 46,
      "x": 0.74,
      "y": 0.398,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        30,
        50
      ]
    },
    {
      "id": 47,
      "x": -0.66,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 48,
      "x": 0.66,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 49,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 50,
      "x": 1.019,
      "y": -0.075,
      "angle": 45.0,
      "layer": 3,
      "closed_by": [
        43
      ]
    }
  ],
  "cards_in_layout_amount": 24,
  "cards_in_deck_amount": 24,
  "layers_in_level": 8,
  "open_cards": 4,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 46.7,
    "star2": 48.6,
    "star3": 2.8
  }
}