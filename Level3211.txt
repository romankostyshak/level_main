{
  "layout_cards": [
    {
      "id": 12,
      "x": 1.799,
      "y": -0.578,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 13,
      "x": -1.794,
      "y": -0.578,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 14,
      "x": 2.069,
      "y": -0.578,
      "layer": 5,
      "closed_by": [
        12
      ]
    },
    {
      "id": 15,
      "x": -2.065,
      "y": -0.578,
      "layer": 5,
      "closed_by": [
        13
      ]
    },
    {
      "id": 18,
      "x": -2.065,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 19,
      "x": 2.065,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 20,
      "x": -1.824,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 21,
      "x": -1.582,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        15,
        23
      ]
    },
    {
      "id": 22,
      "x": 1.587,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        14,
        24
      ]
    },
    {
      "id": 23,
      "x": -1.347,
      "y": -0.578,
      "layer": 5,
      "closed_by": [
        13,
        25
      ]
    },
    {
      "id": 24,
      "x": 1.345,
      "y": -0.578,
      "layer": 5,
      "closed_by": [
        12,
        26
      ]
    },
    {
      "id": 25,
      "x": -1.105,
      "y": -0.578,
      "layer": 6,
      "closed_by": [
        27
      ]
    },
    {
      "id": 26,
      "x": 1.108,
      "y": -0.578,
      "layer": 6,
      "closed_by": [
        36
      ]
    },
    {
      "id": 27,
      "x": -0.758,
      "y": -0.577,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 0.768,
      "y": 0.303,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -0.754,
      "y": 0.303,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 0.507,
      "y": 0.381,
      "layer": 6,
      "closed_by": [
        28
      ]
    },
    {
      "id": 31,
      "x": 1.026,
      "y": 0.381,
      "layer": 6,
      "closed_by": [
        28
      ]
    },
    {
      "id": 32,
      "x": -0.49,
      "y": 0.381,
      "layer": 6,
      "closed_by": [
        29
      ]
    },
    {
      "id": 33,
      "x": -1.013,
      "y": 0.384,
      "layer": 6,
      "closed_by": [
        29
      ]
    },
    {
      "id": 34,
      "x": 0.763,
      "y": 0.465,
      "layer": 5,
      "closed_by": [
        30,
        31
      ]
    },
    {
      "id": 35,
      "x": -0.25,
      "y": 0.465,
      "layer": 5,
      "closed_by": [
        32
      ]
    },
    {
      "id": 36,
      "x": 0.768,
      "y": -0.578,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 37,
      "x": 1.827,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 38,
      "x": -0.759,
      "y": 0.462,
      "layer": 5,
      "closed_by": [
        32,
        33
      ]
    },
    {
      "id": 39,
      "x": 0.259,
      "y": 0.462,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 40,
      "x": 1.273,
      "y": 0.462,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 41,
      "x": -1.266,
      "y": 0.465,
      "layer": 5,
      "closed_by": [
        33
      ]
    },
    {
      "id": 42,
      "x": 0.467,
      "y": 0.462,
      "layer": 4,
      "closed_by": [
        34,
        39
      ]
    },
    {
      "id": 43,
      "x": -0.465,
      "y": 0.462,
      "layer": 4,
      "closed_by": [
        35,
        38
      ]
    },
    {
      "id": 44,
      "x": -1.266,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 45,
      "x": -0.865,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        43,
        44
      ]
    },
    {
      "id": 46,
      "x": 0.869,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        42,
        50
      ]
    },
    {
      "id": 47,
      "x": -0.465,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 48,
      "x": 0.467,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 49,
      "x": 0.002,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        42,
        43,
        47,
        48
      ]
    },
    {
      "id": 50,
      "x": 1.271,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        40
      ]
    }
  ],
  "cards_in_layout_amount": 37,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 26.2,
    "star2": 68.8,
    "star3": 4.8
  }
}