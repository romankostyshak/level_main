{
  "layout_cards": [
    {
      "id": 51,
      "x": 2.019,
      "y": 0.646,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 50,
      "x": 1.491,
      "y": 0.643,
      "layer": 2,
      "closed_by": [
        43,
        31
      ]
    },
    {
      "id": 49,
      "x": -1.516,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        26,
        25
      ]
    },
    {
      "id": 48,
      "x": 1.493,
      "y": -0.18,
      "layer": 2,
      "closed_by": [
        28,
        27
      ]
    },
    {
      "id": 47,
      "x": 2.019,
      "y": -0.178,
      "layer": 2,
      "closed_by": [
        27
      ]
    },
    {
      "id": 46,
      "x": -2.045,
      "y": -0.18,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 45,
      "x": -2.045,
      "y": 0.643,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": -1.616,
      "y": 0.818,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 43,
      "x": 1.6,
      "y": 0.819,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 42,
      "x": -1.195,
      "y": -0.527,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 41,
      "x": -1.195,
      "y": 0.99,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 40,
      "x": 1.199,
      "y": -0.526,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 39,
      "x": 0.694,
      "y": 0.981,
      "layer": 4,
      "closed_by": [
        36,
        24
      ]
    },
    {
      "id": 38,
      "x": 1.213,
      "y": 0.99,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 37,
      "x": -0.777,
      "y": 1.041,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 0.787,
      "y": 1.041,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -1.516,
      "y": 0.646,
      "layer": 2,
      "closed_by": [
        44,
        30
      ]
    },
    {
      "id": 34,
      "x": 0.261,
      "y": -0.572,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -0.254,
      "y": 1.041,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -0.256,
      "y": -0.572,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 1.08,
      "y": 0.819,
      "layer": 3,
      "closed_by": [
        39,
        38
      ]
    },
    {
      "id": 30,
      "x": -1.098,
      "y": 0.819,
      "layer": 3,
      "closed_by": [
        41,
        29
      ]
    },
    {
      "id": 29,
      "x": -0.685,
      "y": 0.981,
      "layer": 4,
      "closed_by": [
        37,
        33
      ]
    },
    {
      "id": 28,
      "x": 1.077,
      "y": -0.342,
      "layer": 3,
      "closed_by": [
        40,
        23
      ]
    },
    {
      "id": 27,
      "x": 1.597,
      "y": -0.344,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 26,
      "x": -1.093,
      "y": -0.34,
      "layer": 3,
      "closed_by": [
        42,
        22
      ]
    },
    {
      "id": 25,
      "x": -1.61,
      "y": -0.34,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 24,
      "x": 0.266,
      "y": 1.039,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 0.671,
      "y": -0.512,
      "layer": 4,
      "closed_by": [
        34,
        20
      ]
    },
    {
      "id": 22,
      "x": -0.68,
      "y": -0.512,
      "layer": 4,
      "closed_by": [
        32,
        21
      ]
    },
    {
      "id": 21,
      "x": -0.777,
      "y": -0.572,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 20,
      "x": 0.781,
      "y": -0.572,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 19,
      "x": -1.779,
      "y": 0.231,
      "layer": 1,
      "closed_by": [
        49,
        46,
        45,
        35
      ]
    },
    {
      "id": 18,
      "x": 1.753,
      "y": 0.231,
      "layer": 1,
      "closed_by": [
        51,
        50,
        48,
        47
      ]
    }
  ],
  "cards_in_layout_amount": 34,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 21.7,
    "star2": 71.0,
    "star3": 7.1
  }
}