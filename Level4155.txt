{
  "layout_cards": [
    {
      "id": 21,
      "x": -2.22,
      "y": -0.017,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -1.858,
      "y": -0.017,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 23,
      "x": -2.22,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 24,
      "x": -1.858,
      "y": 0.62,
      "layer": 1,
      "closed_by": [
        23
      ]
    },
    {
      "id": 25,
      "x": 1.86,
      "y": -0.017,
      "layer": 3,
      "closed_by": [
        51
      ]
    },
    {
      "id": 26,
      "x": 2.22,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 28,
      "y": 1.059,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 29,
      "y": -0.537,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -1.258,
      "y": 0.238,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 1.258,
      "y": 0.238,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -0.298,
      "y": -0.298,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 33,
      "x": -0.298,
      "y": 0.819,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 34,
      "x": 0.298,
      "y": 0.799,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 35,
      "x": 0.298,
      "y": -0.298,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 36,
      "x": 1.86,
      "y": 0.619,
      "layer": 1,
      "closed_by": [
        26
      ]
    },
    {
      "id": 38,
      "y": 0.259,
      "layer": 2,
      "closed_by": [
        32,
        33,
        34,
        35
      ]
    },
    {
      "id": 39,
      "x": -0.976,
      "y": 1.0,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 40,
      "x": -0.976,
      "y": -0.518,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 41,
      "x": 0.98,
      "y": 1.0,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 42,
      "y": 0.657,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 43,
      "x": -0.518,
      "y": 0.657,
      "layer": 1,
      "closed_by": [
        33,
        49
      ]
    },
    {
      "id": 44,
      "x": 0.98,
      "y": 0.238,
      "layer": 2,
      "closed_by": [
        41,
        48
      ]
    },
    {
      "id": 45,
      "x": -0.518,
      "y": -0.158,
      "layer": 1,
      "closed_by": [
        32,
        49
      ]
    },
    {
      "id": 46,
      "y": -0.158,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 47,
      "x": 0.518,
      "y": -0.158,
      "layer": 1,
      "closed_by": [
        35,
        44
      ]
    },
    {
      "id": 48,
      "x": 0.98,
      "y": -0.518,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 49,
      "x": -0.976,
      "y": 0.238,
      "layer": 2,
      "closed_by": [
        39,
        40
      ]
    },
    {
      "id": 50,
      "x": 0.518,
      "y": 0.657,
      "layer": 1,
      "closed_by": [
        34,
        44
      ]
    },
    {
      "id": 51,
      "x": 2.22,
      "y": -0.017,
      "layer": 4,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 29,
  "cards_in_deck_amount": 26,
  "layers_in_level": 4,
  "open_cards": 6,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 27.0,
    "star2": 68.4,
    "star3": 4.4
  }
}