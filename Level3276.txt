{
  "layout_cards": [
    {
      "id": 31,
      "x": -1.677,
      "y": 0.307,
      "layer": 1,
      "closed_by": [
        33,
        34
      ]
    },
    {
      "id": 32,
      "x": -1.169,
      "y": -0.5,
      "layer": 1,
      "closed_by": [
        33,
        34
      ]
    },
    {
      "id": 33,
      "x": -1.2,
      "y": 0.068,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 34,
      "x": -1.656,
      "y": -0.187,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 35,
      "x": -0.469,
      "y": 0.49,
      "angle": 330.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 1.95,
      "y": 0.634,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 37,
      "x": -1.472,
      "y": -0.093,
      "angle": 330.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 0.952,
      "y": 0.071,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 39,
      "x": 1.424,
      "y": 0.337,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [],
      "card_type": 2
    },
    {
      "id": 40,
      "x": 0.768,
      "y": -0.026,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 41,
      "x": 0.73,
      "y": 0.479,
      "layer": 1,
      "closed_by": [
        40,
        43
      ]
    },
    {
      "id": 42,
      "x": 2.161,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        45,
        46
      ]
    },
    {
      "id": 43,
      "x": 1.223,
      "y": 0.224,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 44,
      "x": 1.235,
      "y": -0.335,
      "layer": 1,
      "closed_by": [
        40,
        43
      ]
    },
    {
      "id": 45,
      "x": 2.128,
      "y": 0.74,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 46,
      "x": 1.672,
      "y": 0.483,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 47,
      "x": 1.649,
      "y": 1.023,
      "layer": 1,
      "closed_by": [
        45,
        46
      ]
    },
    {
      "id": 48,
      "x": -0.252,
      "y": 0.057,
      "layer": 1,
      "closed_by": [
        50,
        51
      ]
    },
    {
      "id": 49,
      "x": -0.768,
      "y": 0.864,
      "layer": 1,
      "closed_by": [
        50,
        51
      ],
      "card_type": 3
    },
    {
      "id": 50,
      "x": -0.75,
      "y": 0.33,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 51,
      "x": -0.298,
      "y": 0.592,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        35
      ]
    }
  ],
  "cards_in_layout_amount": 21,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 2.5,
    "star2": 0.0,
    "star3": 96.8
  }
}