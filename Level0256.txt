{
  "layout_cards": [
    {
      "id": 50,
      "x": -0.74,
      "y": -0.136,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 49,
      "x": -1.904,
      "y": 0.615,
      "layer": 2,
      "closed_by": [
        41,
        18
      ]
    },
    {
      "id": 48,
      "x": -1.378,
      "y": 0.444,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 47,
      "x": 0.74,
      "y": -0.136,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 46,
      "x": 1.077,
      "y": -0.017,
      "layer": 3,
      "closed_by": [
        47,
        21
      ]
    },
    {
      "id": 45,
      "x": 1.598,
      "y": 0.158,
      "layer": 1,
      "closed_by": [
        27,
        25,
        15
      ]
    },
    {
      "id": 44,
      "x": 1.796,
      "y": -0.059,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 43,
      "x": -1.503,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        20,
        18
      ]
    },
    {
      "id": 41,
      "x": -1.61,
      "y": 1.021,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 40,
      "x": -2.16,
      "y": 1.039,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 38,
      "x": -2.16,
      "y": -0.595,
      "layer": 2,
      "closed_by": [
        18
      ]
    },
    {
      "id": 37,
      "x": 0.902,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 36,
      "x": -0.412,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 35,
      "x": -0.384,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        50,
        34
      ]
    },
    {
      "id": 34,
      "x": 0.002,
      "y": 0.319,
      "layer": 4,
      "closed_by": [
        51,
        23
      ]
    },
    {
      "id": 32,
      "x": 1.592,
      "y": 1.021,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 31,
      "x": 2.141,
      "y": 1.034,
      "layer": 1,
      "closed_by": [
        25
      ]
    },
    {
      "id": 28,
      "x": 0.414,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 27,
      "x": 1.22,
      "y": 0.819,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 26,
      "x": -1.22,
      "y": 0.819,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 25,
      "x": 1.878,
      "y": 0.615,
      "layer": 2,
      "closed_by": [
        44,
        32
      ]
    },
    {
      "id": 24,
      "x": -0.625,
      "y": 0.418,
      "layer": 2,
      "closed_by": [
        36,
        35,
        20
      ]
    },
    {
      "id": 22,
      "x": -1.577,
      "y": 0.158,
      "layer": 1,
      "closed_by": [
        49,
        43,
        26
      ]
    },
    {
      "id": 21,
      "x": 1.358,
      "y": 0.444,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 20,
      "x": -1.059,
      "y": -0.017,
      "layer": 3,
      "closed_by": [
        50,
        48
      ]
    },
    {
      "id": 19,
      "x": 0.638,
      "y": 0.418,
      "layer": 2,
      "closed_by": [
        46,
        28,
        17
      ]
    },
    {
      "id": 18,
      "x": -1.812,
      "y": -0.059,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 17,
      "x": 0.379,
      "y": -0.261,
      "layer": 3,
      "closed_by": [
        47,
        34
      ]
    },
    {
      "id": 16,
      "x": 2.144,
      "y": -0.595,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 15,
      "x": 1.514,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        46,
        44
      ]
    },
    {
      "id": 13,
      "x": -0.906,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 51,
      "x": -0.331,
      "y": 0.699,
      "layer": 5,
      "closed_by": [],
      "effect_id": 6
    },
    {
      "id": 23,
      "x": 0.335,
      "y": 0.699,
      "layer": 5,
      "closed_by": [],
      "effect_id": 6
    },
    {
      "id": 33,
      "x": -0.316,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        35
      ],
      "effect_id": 2
    },
    {
      "id": 14,
      "x": 0.319,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        17
      ],
      "effect_id": 2
    },
    {
      "id": 30,
      "x": 1.052,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        27,
        19
      ],
      "effect_id": 2
    },
    {
      "id": 29,
      "x": -1.054,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        26,
        24
      ],
      "effect_id": 2
    },
    {
      "id": 42,
      "x": 2.144,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        25
      ],
      "effect_id": 2
    },
    {
      "id": 39,
      "x": -2.16,
      "y": 0.224,
      "layer": 1,
      "closed_by": [
        49
      ],
      "effect_id": 2
    }
  ],
  "cards_in_layout_amount": 39,
  "cards_in_deck_amount": 32,
  "stars_statistic": {
    "star1": 6.5,
    "star2": 56.0,
    "star3": 36.8
  }
}