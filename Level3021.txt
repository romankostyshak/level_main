{
  "layout_cards": [
    {
      "x": -1.825,
      "y": -0.591,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 1,
      "x": 1.746,
      "y": 1.041,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 2,
      "x": -1.745,
      "y": 1.041,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 3,
      "x": -1.182,
      "y": 1.039,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 4,
      "x": 1.202,
      "y": -0.592,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 5,
      "x": -1.202,
      "y": -0.592,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 6,
      "x": 1.184,
      "y": 1.042,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 7,
      "x": 1.825,
      "y": -0.596,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 9,
      "x": 0.333,
      "y": 0.377,
      "layer": 2,
      "closed_by": [
        13,
        47
      ]
    },
    {
      "id": 10,
      "x": -0.33,
      "y": 1.039,
      "layer": 4,
      "closed_by": [
        19,
        51
      ]
    },
    {
      "id": 11,
      "x": -0.333,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        10
      ]
    },
    {
      "id": 12,
      "x": 0.333,
      "y": 0.061,
      "layer": 1,
      "closed_by": [
        9,
        25
      ]
    },
    {
      "id": 13,
      "x": 0.333,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        17
      ]
    },
    {
      "id": 14,
      "x": -0.335,
      "y": 0.384,
      "layer": 2,
      "closed_by": [
        11,
        47
      ]
    },
    {
      "id": 15,
      "x": -0.333,
      "y": 0.061,
      "layer": 1,
      "closed_by": [
        14,
        30
      ]
    },
    {
      "id": 16,
      "x": 0.893,
      "y": 0.136,
      "layer": 1,
      "closed_by": [
        20
      ]
    },
    {
      "id": 17,
      "x": 0.333,
      "y": 1.036,
      "layer": 4,
      "closed_by": [
        18,
        51
      ]
    },
    {
      "id": 18,
      "x": 0.629,
      "y": 1.039,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 19,
      "x": -0.625,
      "y": 1.041,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 20,
      "x": 0.893,
      "y": -0.097,
      "layer": 2,
      "closed_by": [
        21
      ]
    },
    {
      "id": 21,
      "x": 0.896,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 22,
      "x": -1.746,
      "y": 0.541,
      "layer": 4,
      "closed_by": [
        2,
        40
      ]
    },
    {
      "id": 23,
      "x": -0.944,
      "y": 1.039,
      "layer": 4,
      "closed_by": [
        3,
        19
      ]
    },
    {
      "id": 24,
      "x": 0.893,
      "y": -0.592,
      "layer": 4,
      "closed_by": [
        4,
        25
      ]
    },
    {
      "id": 25,
      "x": 0.574,
      "y": -0.592,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -0.893,
      "y": 0.136,
      "layer": 1,
      "closed_by": [
        27
      ]
    },
    {
      "id": 27,
      "x": -0.893,
      "y": -0.097,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 28,
      "x": -0.893,
      "y": -0.432,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 29,
      "x": -0.893,
      "y": -0.591,
      "layer": 4,
      "closed_by": [
        5,
        30
      ]
    },
    {
      "id": 30,
      "x": -0.573,
      "y": -0.592,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 0.947,
      "y": 1.039,
      "layer": 4,
      "closed_by": [
        6,
        18
      ]
    },
    {
      "id": 32,
      "x": 1.184,
      "y": 1.041,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 33,
      "x": -1.187,
      "y": 1.039,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 34,
      "x": 1.427,
      "y": 1.039,
      "layer": 2,
      "closed_by": [
        32,
        36
      ]
    },
    {
      "id": 35,
      "x": -1.424,
      "y": 1.039,
      "layer": 2,
      "closed_by": [
        33,
        37
      ]
    },
    {
      "id": 36,
      "x": 1.743,
      "y": 1.041,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 37,
      "x": -1.745,
      "y": 1.041,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 38,
      "x": 1.746,
      "y": 0.541,
      "layer": 4,
      "closed_by": [
        1,
        39
      ]
    },
    {
      "id": 39,
      "x": 1.748,
      "y": 0.221,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 40,
      "x": -1.745,
      "y": 0.224,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 1.534,
      "y": -0.416,
      "layer": 4,
      "closed_by": [
        4,
        7,
        39
      ]
    },
    {
      "id": 42,
      "x": -2.065,
      "y": -0.414,
      "layer": 2,
      "closed_by": [
        49
      ]
    },
    {
      "id": 43,
      "x": -0.001,
      "y": -0.592,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 44,
      "x": -1.531,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        0,
        5,
        40
      ]
    },
    {
      "id": 45,
      "x": 1.534,
      "y": -0.592,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 46,
      "x": -1.531,
      "y": -0.587,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 47,
      "y": -0.179,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 48,
      "x": 2.062,
      "y": 0.14,
      "layer": 3,
      "closed_by": [
        7,
        38
      ]
    },
    {
      "id": 49,
      "x": -2.065,
      "y": 0.141,
      "layer": 3,
      "closed_by": [
        0,
        22
      ]
    },
    {
      "id": 50,
      "x": 2.065,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        39,
        48
      ]
    },
    {
      "id": 51,
      "x": -0.001,
      "y": 1.039,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 51,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 0.0,
    "star2": 0.4,
    "star3": 98.6
  }
}