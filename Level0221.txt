{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.957,
      "y": 0.773,
      "angle": 9.998,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 50,
      "x": -1.677,
      "y": 0.773,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 49,
      "x": -0.651,
      "y": -0.165,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 48,
      "x": -1.317,
      "y": 0.734,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 47,
      "x": -0.958,
      "y": -0.165,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 46,
      "x": -1.677,
      "y": -0.165,
      "angle": 9.998,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 45,
      "x": -1.317,
      "y": -0.128,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 44,
      "x": 2.115,
      "y": 0.794,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 43,
      "x": 2.114,
      "y": -0.335,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 42,
      "x": 1.218,
      "y": -0.216,
      "layer": 2,
      "closed_by": [
        30,
        26
      ]
    },
    {
      "id": 41,
      "x": 0.902,
      "y": 0.794,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 40,
      "x": -1.317,
      "y": 0.298,
      "layer": 4,
      "closed_by": [],
      "card_type": 2
    },
    {
      "id": 39,
      "x": 1.503,
      "y": 0.938,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 0.958,
      "y": 0.237,
      "layer": 1,
      "closed_by": [
        42,
        29
      ],
      "card_type": 6
    },
    {
      "id": 37,
      "x": -0.652,
      "y": 0.785,
      "layer": 1,
      "closed_by": [
        51
      ]
    },
    {
      "id": 36,
      "x": -1.917,
      "y": 0.786,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 35,
      "x": -1.917,
      "y": -0.164,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 34,
      "x": 2.381,
      "y": 0.671,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 0.648,
      "y": 0.674,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 2.384,
      "y": -0.194,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 0.646,
      "y": -0.194,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 0.906,
      "y": -0.335,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 29,
      "x": 1.22,
      "y": 0.689,
      "layer": 2,
      "closed_by": [
        41,
        39
      ]
    },
    {
      "id": 28,
      "x": 1.804,
      "y": 0.689,
      "layer": 2,
      "closed_by": [
        44,
        39
      ]
    },
    {
      "id": 27,
      "x": 1.802,
      "y": -0.216,
      "layer": 2,
      "closed_by": [
        43,
        26
      ]
    },
    {
      "id": 26,
      "x": 1.508,
      "y": -0.493,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 1.506,
      "y": 0.238,
      "layer": 1,
      "closed_by": [
        42,
        29,
        28,
        27
      ],
      "card_type": 3
    },
    {
      "id": 24,
      "x": 2.055,
      "y": 0.238,
      "layer": 1,
      "closed_by": [
        28,
        27
      ],
      "card_type": 6
    }
  ],
  "cards_in_layout_amount": 28,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 38.9,
    "star2": 1.4,
    "star3": 58.7
  }
}