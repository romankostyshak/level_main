{
  "layout_cards": [
    {
      "id": 23,
      "x": 1.57,
      "y": -0.178,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 1.569,
      "y": 0.777,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 2.611,
      "y": -0.142,
      "angle": 45.0,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 26,
      "x": 2.614,
      "y": 0.74,
      "angle": 315.0,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 0.504,
      "y": -0.141,
      "angle": 315.0,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 0.504,
      "y": 0.749,
      "angle": 45.0,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 29,
      "x": 2.706,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        25,
        26
      ]
    },
    {
      "id": 30,
      "x": 1.292,
      "y": 0.3,
      "layer": 1,
      "closed_by": [
        23,
        24
      ]
    },
    {
      "id": 31,
      "x": 0.412,
      "y": 0.303,
      "layer": 1,
      "closed_by": [
        27,
        28
      ]
    },
    {
      "id": 32,
      "x": -1.338,
      "y": -0.179,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -1.338,
      "y": 0.777,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -2.059,
      "y": -0.179,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -2.059,
      "y": 0.777,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -0.66,
      "y": 0.777,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 37,
      "x": -0.66,
      "y": -0.179,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 1.853,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        23,
        24
      ]
    },
    {
      "id": 39,
      "x": -2.141,
      "y": -0.142,
      "angle": 315.0,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 40,
      "x": -2.147,
      "y": 0.748,
      "angle": 45.0,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 41,
      "x": -0.574,
      "y": 0.74,
      "angle": 315.0,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 42,
      "x": -2.226,
      "y": 0.3,
      "layer": 2,
      "closed_by": [
        39,
        40
      ]
    },
    {
      "id": 43,
      "x": -0.493,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        41,
        44
      ]
    },
    {
      "id": 44,
      "x": -0.582,
      "y": -0.142,
      "angle": 45.0,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 45,
      "x": -1.659,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        32,
        39
      ]
    },
    {
      "id": 46,
      "x": -1.019,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        33,
        41
      ]
    },
    {
      "id": 47,
      "x": -1.659,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        33,
        40
      ]
    },
    {
      "id": 48,
      "x": -1.906,
      "y": 0.3,
      "layer": 1,
      "closed_by": [
        42,
        45,
        47
      ]
    },
    {
      "id": 49,
      "x": -0.785,
      "y": 0.3,
      "layer": 1,
      "closed_by": [
        43,
        46,
        51
      ]
    },
    {
      "id": 50,
      "x": -1.347,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        45,
        46,
        47,
        51
      ],
      "card_type": 8
    },
    {
      "id": 51,
      "x": -1.019,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        32,
        44
      ]
    }
  ],
  "level_mechanic": 2,
  "free_cards_from_stickers": [
    32,
    33,
    34,
    35,
    36,
    37,
    39,
    40,
    41,
    42,
    43,
    44,
    45,
    46,
    47,
    48,
    49,
    50,
    51
  ],
  "cards_in_layout_amount": 29,
  "cards_in_deck_amount": 22,
  "stars_statistic": {
    "star1": 41.9,
    "star2": 53.8,
    "star3": 3.6
  }
}