{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.333,
      "y": -0.18,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 50,
      "x": -0.333,
      "y": -0.18,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 49,
      "x": 0.416,
      "y": -0.061,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": -0.418,
      "y": -0.059,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "x": -0.569,
      "y": 0.177,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        43,
        41,
        40,
        26
      ]
    },
    {
      "id": 46,
      "x": 0.578,
      "y": 0.171,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        45,
        44,
        42,
        39
      ]
    },
    {
      "id": 45,
      "x": 0.331,
      "y": 0.86,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 44,
      "x": 0.888,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        38,
        36
      ]
    },
    {
      "id": 43,
      "x": -0.892,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        37,
        35
      ]
    },
    {
      "id": 42,
      "x": 0.892,
      "y": -0.017,
      "layer": 4,
      "closed_by": [
        36,
        34
      ]
    },
    {
      "id": 41,
      "x": -0.888,
      "y": -0.017,
      "layer": 4,
      "closed_by": [
        35,
        33
      ]
    },
    {
      "id": 40,
      "x": -0.331,
      "y": -0.335,
      "layer": 4,
      "closed_by": [
        33,
        32
      ]
    },
    {
      "id": 39,
      "x": 0.333,
      "y": -0.333,
      "layer": 4,
      "closed_by": [
        34,
        32
      ]
    },
    {
      "id": 38,
      "x": 0.572,
      "y": 1.019,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 37,
      "x": -0.569,
      "y": 1.023,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 36,
      "x": 1.105,
      "y": 0.458,
      "layer": 5,
      "closed_by": [
        31,
        29
      ]
    },
    {
      "id": 35,
      "x": -1.103,
      "y": 0.46,
      "layer": 5,
      "closed_by": [
        30,
        28
      ]
    },
    {
      "id": 34,
      "x": 0.546,
      "y": -0.256,
      "layer": 5,
      "closed_by": [
        29,
        24
      ]
    },
    {
      "id": 33,
      "x": -0.546,
      "y": -0.254,
      "layer": 5,
      "closed_by": [
        28,
        27
      ]
    },
    {
      "id": 32,
      "x": 0.004,
      "y": -0.497,
      "layer": 5,
      "closed_by": [
        27,
        24
      ]
    },
    {
      "id": 31,
      "x": 0.949,
      "y": 0.939,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -0.944,
      "y": 0.939,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 29,
      "x": 0.81,
      "y": 0.063,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 28,
      "x": -0.8,
      "y": 0.052,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 27,
      "x": -0.259,
      "y": -0.008,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -0.331,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 24,
      "x": 0.261,
      "y": -0.013,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 27,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 65.9,
    "star2": 9.9,
    "star3": 24.1
  }
}