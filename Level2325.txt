{
  "layout_cards": [
    {
      "id": 24,
      "x": 0.412,
      "y": -0.256,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 1.105,
      "y": 0.699,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 26,
      "x": 2.467,
      "y": 0.703,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 2.467,
      "y": -0.25,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 1.934,
      "y": -0.252,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 29,
      "x": 1.932,
      "y": 0.699,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 1.692,
      "y": 0.221,
      "layer": 1,
      "closed_by": [
        28,
        29
      ]
    },
    {
      "id": 31,
      "x": 2.226,
      "y": 0.224,
      "layer": 1,
      "closed_by": [
        26,
        27,
        28,
        29
      ]
    },
    {
      "id": 32,
      "x": -2.782,
      "y": 0.782,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -1.21,
      "y": -0.1,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -2.463,
      "y": 0.541,
      "layer": 1,
      "closed_by": [
        32,
        45,
        51
      ]
    },
    {
      "id": 35,
      "x": 1.108,
      "y": -0.256,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 36,
      "x": -0.569,
      "y": 0.3,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 37,
      "x": -0.893,
      "y": 0.54,
      "layer": 1,
      "closed_by": [
        33,
        36,
        44
      ]
    },
    {
      "id": 38,
      "x": -0.888,
      "y": -0.34,
      "layer": 1,
      "closed_by": [
        33,
        36,
        39
      ]
    },
    {
      "id": 39,
      "x": -0.572,
      "y": -0.578,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 1.026,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 41,
      "x": 0.412,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 42,
      "x": 0.493,
      "y": 0.222,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 43,
      "x": -3.101,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 44,
      "x": -1.21,
      "y": 0.777,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 45,
      "x": -2.144,
      "y": 0.305,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 46,
      "x": -2.144,
      "y": -0.577,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 47,
      "x": -1.506,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 48,
      "x": -3.101,
      "y": 0.143,
      "layer": 1,
      "closed_by": [
        32,
        51
      ]
    },
    {
      "id": 49,
      "x": -2.463,
      "y": -0.335,
      "layer": 1,
      "closed_by": [
        45,
        46,
        51
      ]
    },
    {
      "id": 50,
      "x": -1.508,
      "y": 0.141,
      "layer": 1,
      "closed_by": [
        33,
        44
      ]
    },
    {
      "id": 51,
      "x": -2.782,
      "y": -0.093,
      "layer": 2,
      "closed_by": []
    }
  ],
  "level_mechanic": 1,
  "free_cards_from_stickers": [
    24,
    25,
    26,
    27,
    28,
    29,
    30,
    31,
    35,
    40,
    41,
    42
  ],
  "cards_in_layout_amount": 28,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 23.1,
    "star2": 72.5,
    "star3": 3.7
  }
}