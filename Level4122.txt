{
  "layout_cards": [
    {
      "id": 18,
      "x": -0.458,
      "y": -0.186,
      "angle": 300.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 19,
      "x": -0.458,
      "y": 0.648,
      "angle": 60.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 20,
      "x": 0.46,
      "y": 0.638,
      "angle": 300.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 21,
      "x": -1.659,
      "y": 0.72,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -1.659,
      "y": -0.259,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 1.659,
      "y": 0.72,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 1.659,
      "y": -0.259,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 25,
      "x": -1.519,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        21,
        22
      ]
    },
    {
      "id": 26,
      "x": 1.519,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        23,
        24
      ]
    },
    {
      "id": 27,
      "x": -0.638,
      "y": 0.638,
      "layer": 4,
      "closed_by": [
        19
      ]
    },
    {
      "id": 28,
      "x": 0.638,
      "y": 0.638,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 29,
      "x": 0.638,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 30,
      "y": 0.238,
      "layer": 4,
      "closed_by": [
        18,
        19,
        20,
        37
      ]
    },
    {
      "id": 31,
      "x": -0.259,
      "y": 0.638,
      "layer": 3,
      "closed_by": [
        27,
        30
      ]
    },
    {
      "id": 32,
      "x": -0.259,
      "y": -0.179,
      "layer": 3,
      "closed_by": [
        30,
        36
      ]
    },
    {
      "id": 33,
      "x": 0.259,
      "y": 0.638,
      "layer": 3,
      "closed_by": [
        28,
        30
      ]
    },
    {
      "id": 34,
      "x": 0.259,
      "y": -0.179,
      "layer": 3,
      "closed_by": [
        29,
        30
      ]
    },
    {
      "id": 35,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        32,
        34
      ]
    },
    {
      "id": 36,
      "x": -0.638,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        18
      ]
    },
    {
      "id": 37,
      "x": 0.46,
      "y": -0.179,
      "angle": 60.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 38,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        31,
        33
      ]
    },
    {
      "id": 39,
      "x": -1.258,
      "y": 0.72,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 40,
      "x": -1.258,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 41,
      "x": 1.279,
      "y": 0.72,
      "layer": 2,
      "closed_by": [
        26
      ]
    },
    {
      "id": 42,
      "x": 1.279,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        26
      ]
    },
    {
      "id": 43,
      "x": -1.019,
      "y": 0.638,
      "layer": 1,
      "closed_by": [
        27,
        39
      ]
    },
    {
      "id": 44,
      "x": -1.019,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        36,
        40
      ]
    },
    {
      "id": 45,
      "x": 0.259,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 46,
      "x": 1.019,
      "y": 0.638,
      "layer": 1,
      "closed_by": [
        28,
        41
      ]
    },
    {
      "id": 47,
      "x": 1.019,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        29,
        42
      ]
    },
    {
      "id": 48,
      "x": -0.259,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 49,
      "x": 0.259,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        18,
        35
      ]
    },
    {
      "id": 50,
      "x": -0.259,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        38
      ]
    }
  ],
  "cards_in_layout_amount": 33,
  "cards_in_deck_amount": 26,
  "layers_in_level": 5,
  "open_cards": 8,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 40.1,
    "star2": 55.3,
    "star3": 3.6
  }
}