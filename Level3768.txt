{
  "layout_cards": [
    {
      "x": 2.177,
      "y": -0.492,
      "layer": 1,
      "closed_by": [
        1,
        3
      ]
    },
    {
      "id": 1,
      "x": 2.591,
      "y": -0.256,
      "layer": 2,
      "closed_by": [
        2
      ]
    },
    {
      "id": 2,
      "x": 2.177,
      "y": 0.061,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 3,
      "x": 1.769,
      "y": -0.256,
      "layer": 2,
      "closed_by": [
        2
      ]
    },
    {
      "id": 28,
      "x": -1.764,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 29,
      "x": -2.173,
      "y": -0.492,
      "layer": 1,
      "closed_by": [
        28,
        30
      ]
    },
    {
      "id": 30,
      "x": -2.585,
      "y": -0.256,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 31,
      "x": -2.174,
      "y": 0.061,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -0.414,
      "y": -0.256,
      "layer": 2,
      "closed_by": [
        36,
        41
      ]
    },
    {
      "id": 35,
      "x": 0.002,
      "y": -0.493,
      "layer": 1,
      "closed_by": [
        34,
        39
      ]
    },
    {
      "id": 36,
      "x": 0.002,
      "y": 0.059,
      "layer": 3,
      "closed_by": [
        42,
        43
      ]
    },
    {
      "id": 37,
      "x": 0.818,
      "y": 0.62,
      "layer": 5,
      "closed_by": [
        48
      ]
    },
    {
      "id": 38,
      "x": 1.228,
      "y": 0.293,
      "layer": 4,
      "closed_by": [
        37,
        45
      ]
    },
    {
      "id": 39,
      "x": 0.411,
      "y": -0.256,
      "layer": 2,
      "closed_by": [
        36,
        40
      ]
    },
    {
      "id": 40,
      "x": 0.819,
      "y": 0.057,
      "layer": 3,
      "closed_by": [
        38,
        42
      ]
    },
    {
      "id": 41,
      "x": -0.81,
      "y": 0.059,
      "layer": 3,
      "closed_by": [
        43,
        44
      ]
    },
    {
      "id": 42,
      "x": 0.412,
      "y": 0.296,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 43,
      "x": -0.412,
      "y": 0.296,
      "layer": 4,
      "closed_by": [
        46
      ]
    },
    {
      "id": 44,
      "x": -1.225,
      "y": 0.293,
      "layer": 4,
      "closed_by": [
        46,
        47
      ]
    },
    {
      "id": 45,
      "x": 1.631,
      "y": 0.623,
      "layer": 5,
      "closed_by": [
        48,
        49
      ]
    },
    {
      "id": 46,
      "x": -0.814,
      "y": 0.62,
      "layer": 5,
      "closed_by": [
        50
      ]
    },
    {
      "id": 47,
      "x": -1.626,
      "y": 0.62,
      "layer": 5,
      "closed_by": [
        50,
        51
      ]
    },
    {
      "id": 48,
      "x": 1.228,
      "y": 0.939,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 49,
      "x": 2.046,
      "y": 0.939,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 50,
      "x": -1.225,
      "y": 0.943,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -2.049,
      "y": 0.935,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 26,
  "cards_in_deck_amount": 24,
  "layers_in_level": 6,
  "open_cards": 6,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 59.3,
    "star2": 39.7,
    "star3": 0.9
  }
}