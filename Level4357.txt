{
  "layout_cards": [
    {
      "id": 12,
      "x": 1.98,
      "y": 0.136,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 16,
      "x": 0.777,
      "y": 0.259,
      "layer": 3,
      "closed_by": [
        42,
        43
      ]
    },
    {
      "id": 17,
      "x": -1.457,
      "y": 0.859,
      "layer": 1,
      "closed_by": [
        27,
        28
      ]
    },
    {
      "id": 18,
      "x": -0.259,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        20,
        38
      ]
    },
    {
      "id": 19,
      "x": -0.259,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        20,
        38
      ]
    },
    {
      "id": 20,
      "x": -0.418,
      "y": 0.259,
      "layer": 2,
      "closed_by": [
        44,
        45
      ]
    },
    {
      "id": 21,
      "x": -0.859,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        20,
        27
      ]
    },
    {
      "id": 22,
      "x": -0.859,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        20,
        27
      ]
    },
    {
      "id": 23,
      "x": -2.059,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        28,
        31
      ]
    },
    {
      "id": 24,
      "x": -1.457,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        27,
        28
      ]
    },
    {
      "id": 25,
      "x": -2.059,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        28,
        31
      ]
    },
    {
      "id": 26,
      "x": -1.319,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        50,
        51
      ]
    },
    {
      "id": 27,
      "x": -1.019,
      "y": 0.259,
      "layer": 2,
      "closed_by": [
        26,
        45
      ]
    },
    {
      "id": 28,
      "x": -1.62,
      "y": 0.259,
      "layer": 2,
      "closed_by": [
        26,
        46
      ]
    },
    {
      "id": 29,
      "x": 0.939,
      "y": 0.537,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 30,
      "x": 0.337,
      "y": 0.62,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 31,
      "x": -2.22,
      "y": 0.259,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 32,
      "x": 0.337,
      "y": -0.1,
      "layer": 2,
      "closed_by": [
        16,
        38
      ]
    },
    {
      "id": 33,
      "x": 0.939,
      "y": -0.017,
      "layer": 2,
      "closed_by": [
        16,
        39
      ]
    },
    {
      "id": 34,
      "x": 1.539,
      "y": 0.46,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 35,
      "x": 1.539,
      "y": 0.059,
      "layer": 2,
      "closed_by": [
        39,
        40
      ]
    },
    {
      "id": 36,
      "x": 2.138,
      "y": 0.379,
      "layer": 1,
      "closed_by": [
        37
      ]
    },
    {
      "id": 37,
      "x": 2.138,
      "y": 0.136,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 38,
      "x": 0.18,
      "y": 0.259,
      "layer": 3,
      "closed_by": [
        43,
        44
      ]
    },
    {
      "id": 39,
      "x": 1.378,
      "y": 0.259,
      "layer": 3,
      "closed_by": [
        41,
        42
      ]
    },
    {
      "id": 40,
      "x": 1.98,
      "y": 0.259,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 41,
      "x": 1.679,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        12,
        47
      ]
    },
    {
      "id": 42,
      "x": 1.077,
      "y": 0.136,
      "layer": 4,
      "closed_by": [
        47,
        48
      ]
    },
    {
      "id": 43,
      "x": 0.479,
      "y": 0.059,
      "layer": 4,
      "closed_by": [
        48
      ]
    },
    {
      "id": 44,
      "x": -0.119,
      "y": 0.537,
      "layer": 4,
      "closed_by": [
        49
      ]
    },
    {
      "id": 45,
      "x": -0.72,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        49,
        50
      ]
    },
    {
      "id": 46,
      "x": -1.919,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 47,
      "x": 1.378,
      "y": 0.059,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 48,
      "x": 0.777,
      "y": -0.017,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 49,
      "x": -0.418,
      "y": 0.699,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 50,
      "x": -1.019,
      "y": 0.777,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -1.62,
      "y": 0.859,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 37,
  "cards_in_deck_amount": 30,
  "layers_in_level": 5,
  "open_cards": 6,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 33.0,
    "star2": 63.1,
    "star3": 2.2
  }
}