{
  "layout_cards": [
    {
      "id": 49,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        48,
        47
      ]
    },
    {
      "id": 48,
      "x": 0.31,
      "y": -0.093,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 47,
      "x": -0.303,
      "y": -0.096,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 46,
      "y": -0.497,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 45,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "y": -0.337,
      "layer": 5,
      "closed_by": [
        44,
        43
      ]
    },
    {
      "id": 44,
      "x": 0.388,
      "y": -0.017,
      "layer": 6,
      "closed_by": [
        42
      ]
    },
    {
      "id": 43,
      "x": -0.386,
      "y": -0.017,
      "layer": 6,
      "closed_by": [
        41
      ]
    },
    {
      "id": 42,
      "x": 0.462,
      "y": 0.064,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 41,
      "x": -0.465,
      "y": 0.061,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 40,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        39,
        38
      ]
    },
    {
      "id": 39,
      "x": 0.465,
      "y": 0.944,
      "layer": 2,
      "closed_by": [
        35,
        25
      ]
    },
    {
      "id": 38,
      "x": -0.462,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        34,
        26
      ]
    },
    {
      "id": 35,
      "x": 0.87,
      "y": 0.943,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 34,
      "x": -0.865,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 33,
      "x": 1.029,
      "y": 0.462,
      "layer": 4,
      "closed_by": [
        31,
        36
      ]
    },
    {
      "id": 32,
      "x": -1.026,
      "y": 0.465,
      "layer": 4,
      "closed_by": [
        30,
        27
      ]
    },
    {
      "id": 31,
      "x": 1.026,
      "y": -0.179,
      "layer": 5,
      "closed_by": [
        29
      ]
    },
    {
      "id": 30,
      "x": -1.026,
      "y": -0.178,
      "layer": 5,
      "closed_by": [
        28
      ]
    },
    {
      "id": 29,
      "x": 1.029,
      "y": -0.578,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 28,
      "x": -1.026,
      "y": -0.578,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 1.031,
      "y": 0.939,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 27,
      "x": -1.026,
      "y": 0.943,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -0.335,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 25,
      "x": 0.33,
      "y": 0.944,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 24,
      "x": -0.001,
      "y": 0.861,
      "layer": 4,
      "closed_by": [
        42,
        41
      ]
    },
    {
      "id": 15,
      "x": -2.654,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        13
      ]
    },
    {
      "id": 23,
      "x": 2.654,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        21
      ]
    },
    {
      "id": 21,
      "x": 2.654,
      "y": 0.638,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 20,
      "x": 2.654,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 18,
      "x": 2.654,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        14
      ]
    },
    {
      "id": 14,
      "x": 2.654,
      "y": -0.578,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -2.654,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        37
      ]
    },
    {
      "id": 37,
      "x": -2.654,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 19,
      "x": -2.654,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        15
      ]
    },
    {
      "id": 13,
      "x": -2.654,
      "y": -0.578,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 36,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 13.6,
    "star2": 70.1,
    "star3": 15.6
  }
}