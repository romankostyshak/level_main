{
  "layout_cards": [
    {
      "id": 5,
      "x": -1.745,
      "y": 0.305,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 8,
      "x": 1.669,
      "y": 0.298,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 9,
      "x": -1.743,
      "y": 0.859,
      "layer": 6,
      "closed_by": [
        5
      ]
    },
    {
      "id": 10,
      "x": 1.667,
      "y": -0.254,
      "layer": 6,
      "closed_by": [
        8
      ]
    },
    {
      "id": 11,
      "x": 1.664,
      "y": 0.86,
      "layer": 6,
      "closed_by": [
        8
      ]
    },
    {
      "id": 12,
      "x": -1.559,
      "y": -0.229,
      "angle": 10.0,
      "layer": 5,
      "closed_by": [
        17
      ]
    },
    {
      "id": 13,
      "x": 1.567,
      "y": -0.224,
      "angle": 350.0,
      "layer": 5,
      "closed_by": [
        10
      ]
    },
    {
      "id": 14,
      "x": 1.572,
      "y": 0.832,
      "angle": 10.0,
      "layer": 5,
      "closed_by": [
        11
      ]
    },
    {
      "id": 15,
      "x": -1.424,
      "y": -0.259,
      "layer": 4,
      "closed_by": [
        12
      ]
    },
    {
      "id": 16,
      "x": -1.574,
      "y": 0.833,
      "angle": 350.0,
      "layer": 5,
      "closed_by": [
        9
      ]
    },
    {
      "id": 17,
      "x": -1.745,
      "y": -0.254,
      "layer": 6,
      "closed_by": [
        5
      ]
    },
    {
      "id": 18,
      "x": -1.424,
      "y": 0.861,
      "layer": 4,
      "closed_by": [
        16
      ]
    },
    {
      "id": 19,
      "x": 1.424,
      "y": -0.254,
      "layer": 4,
      "closed_by": [
        13
      ]
    },
    {
      "id": 20,
      "x": 1.427,
      "y": 0.86,
      "layer": 4,
      "closed_by": [
        14
      ]
    },
    {
      "id": 21,
      "y": -0.416,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -0.331,
      "y": -0.335,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 23,
      "x": 0.331,
      "y": -0.335,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 24,
      "x": 0.333,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 25,
      "x": -0.331,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 26,
      "y": -0.178,
      "layer": 2,
      "closed_by": [
        32,
        33
      ]
    },
    {
      "id": 27,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        38,
        39
      ]
    },
    {
      "id": 28,
      "x": -0.331,
      "y": 0.3,
      "layer": 1,
      "closed_by": [
        26,
        27,
        43,
        47
      ]
    },
    {
      "id": 29,
      "x": -1.424,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        41,
        42
      ]
    },
    {
      "id": 30,
      "x": 1.424,
      "y": 0.3,
      "layer": 1,
      "closed_by": [
        45,
        50
      ]
    },
    {
      "id": 31,
      "x": -1.292,
      "y": -0.228,
      "angle": 350.0,
      "layer": 3,
      "closed_by": [
        15
      ]
    },
    {
      "id": 32,
      "x": -0.469,
      "y": -0.228,
      "angle": 10.0,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 33,
      "x": 0.469,
      "y": -0.231,
      "angle": 350.0,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 34,
      "x": 1.292,
      "y": -0.229,
      "angle": 10.0,
      "layer": 3,
      "closed_by": [
        19
      ]
    },
    {
      "id": 35,
      "x": 1.297,
      "y": 0.832,
      "angle": 350.0,
      "layer": 3,
      "closed_by": [
        20
      ]
    },
    {
      "id": 36,
      "x": 0.333,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        26,
        27,
        44,
        46
      ]
    },
    {
      "id": 37,
      "x": 0.004,
      "y": 1.026,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 0.467,
      "y": 0.832,
      "angle": 10.0,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 39,
      "x": -0.467,
      "y": 0.833,
      "angle": 350.0,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 40,
      "x": -1.289,
      "y": 0.833,
      "angle": 10.0,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 41,
      "x": -1.184,
      "y": -0.178,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 42,
      "x": -1.184,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 43,
      "x": -0.573,
      "y": -0.18,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 44,
      "x": 0.573,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 45,
      "x": 1.184,
      "y": 0.782,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 46,
      "x": 0.574,
      "y": 0.781,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 47,
      "x": -0.573,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 48,
      "x": -0.879,
      "y": 0.3,
      "layer": 1,
      "closed_by": [
        41,
        42,
        43,
        47
      ]
    },
    {
      "id": 49,
      "x": 0.875,
      "y": 0.3,
      "layer": 1,
      "closed_by": [
        44,
        45,
        46,
        50
      ]
    },
    {
      "id": 50,
      "x": 1.187,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        34
      ]
    }
  ],
  "cards_in_layout_amount": 44,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 0.4,
    "star2": 22.5,
    "star3": 76.1
  }
}