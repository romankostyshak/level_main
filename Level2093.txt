{
  "layout_cards": [
    {
      "id": 15,
      "x": -1.052,
      "y": -0.574,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 18,
      "x": 1.054,
      "y": -0.574,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 19,
      "x": -1.131,
      "y": -0.574,
      "layer": 3,
      "closed_by": [
        15
      ]
    },
    {
      "id": 20,
      "x": 1.136,
      "y": -0.574,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 21,
      "x": -1.213,
      "y": -0.573,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 22,
      "x": -1.741,
      "y": 0.3,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 1.751,
      "y": 0.298,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 24,
      "x": -1.983,
      "y": 0.3,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 25,
      "x": -1.45,
      "y": 0.3,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 26,
      "x": 1.452,
      "y": 0.296,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 27,
      "x": 1.746,
      "y": 0.933,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 28,
      "x": 1.452,
      "y": 0.933,
      "layer": 1,
      "closed_by": [
        27
      ]
    },
    {
      "id": 29,
      "x": 1.983,
      "y": 0.296,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 30,
      "x": -1.455,
      "y": 0.938,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 31,
      "x": 1.751,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        26,
        29
      ]
    },
    {
      "id": 32,
      "x": -1.746,
      "y": 0.938,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 33,
      "x": -1.743,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        24,
        25
      ]
    },
    {
      "id": 34,
      "x": 1.215,
      "y": -0.573,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 36,
      "x": -1.745,
      "y": -0.266,
      "layer": 3,
      "closed_by": [
        24,
        25
      ]
    },
    {
      "id": 37,
      "x": 1.745,
      "y": -0.266,
      "layer": 3,
      "closed_by": [
        26,
        29
      ]
    },
    {
      "id": 38,
      "x": -1.743,
      "y": -0.495,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 39,
      "x": 1.743,
      "y": -0.499,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 40,
      "x": -1.452,
      "y": -0.412,
      "layer": 1,
      "closed_by": [
        21,
        38
      ]
    },
    {
      "id": 41,
      "x": 1.452,
      "y": -0.414,
      "layer": 1,
      "closed_by": [
        34,
        39
      ]
    },
    {
      "id": 42,
      "x": -0.002,
      "y": 1.019,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 43,
      "x": -0.002,
      "y": -0.093,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 44,
      "x": -0.001,
      "y": 0.136,
      "layer": 5,
      "closed_by": [
        43
      ]
    },
    {
      "id": 45,
      "x": -0.001,
      "y": 0.782,
      "layer": 4,
      "closed_by": [
        42,
        44
      ]
    },
    {
      "id": 46,
      "x": 0.333,
      "y": 0.136,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 47,
      "x": -0.331,
      "y": 0.14,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 48,
      "x": 0.333,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 49,
      "x": -0.333,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 50,
      "x": 0.574,
      "y": 0.46,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 51,
      "x": -0.574,
      "y": 0.46,
      "layer": 1,
      "closed_by": [
        49
      ]
    }
  ],
  "cards_in_layout_amount": 34,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 24.3,
    "star2": 68.6,
    "star3": 6.5
  }
}