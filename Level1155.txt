{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.263,
      "y": -0.187,
      "layer": 1,
      "closed_by": [
        41,
        42
      ]
    },
    {
      "id": 50,
      "x": -0.259,
      "y": -0.187,
      "layer": 1,
      "closed_by": [
        43,
        41
      ]
    },
    {
      "id": 49,
      "x": 0.785,
      "y": -0.187,
      "layer": 1,
      "closed_by": [
        39,
        42
      ]
    },
    {
      "id": 48,
      "x": -0.786,
      "y": -0.187,
      "layer": 1,
      "closed_by": [
        43,
        38
      ]
    },
    {
      "id": 45,
      "x": 1.041,
      "y": 0.634,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 44,
      "x": -1.042,
      "y": 0.634,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 43,
      "x": -0.518,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        36,
        34
      ]
    },
    {
      "id": 41,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        37,
        36,
        35,
        34
      ]
    },
    {
      "id": 39,
      "x": 1.182,
      "y": 0.223,
      "layer": 2,
      "closed_by": [
        33,
        32
      ]
    },
    {
      "id": 38,
      "x": -1.184,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        31,
        30
      ]
    },
    {
      "id": 37,
      "x": 0.333,
      "y": 0.86,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 36,
      "x": -0.333,
      "y": 0.86,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 35,
      "x": 0.333,
      "y": -0.412,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 34,
      "x": -0.328,
      "y": -0.416,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 33,
      "x": 1.184,
      "y": -0.412,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 32,
      "x": 1.182,
      "y": 0.856,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 31,
      "x": -1.184,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 30,
      "x": -1.184,
      "y": -0.414,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 29,
      "x": 0.731,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        25
      ]
    },
    {
      "id": 28,
      "x": -0.734,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 27,
      "x": 0.736,
      "y": -0.493,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 26,
      "x": -0.73,
      "y": -0.493,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 25,
      "x": 0.735,
      "y": 1.024,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 24,
      "x": -0.731,
      "y": 1.021,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 0.736,
      "y": -0.574,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -0.73,
      "y": -0.573,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 42,
      "x": 0.518,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        37,
        35
      ]
    },
    {
      "id": 40,
      "x": 0.518,
      "y": 0.638,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 46,
      "x": -0.518,
      "y": 0.638,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 47,
      "y": 0.638,
      "layer": 1,
      "closed_by": [
        41
      ]
    }
  ],
  "cards_in_layout_amount": 30,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 50.0,
    "star2": 30.3,
    "star3": 19.3
  }
}