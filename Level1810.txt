{
  "layout_cards": [
    {
      "id": 49,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        48,
        47
      ]
    },
    {
      "id": 48,
      "x": 0.331,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        46,
        41
      ]
    },
    {
      "id": 47,
      "x": -0.33,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        45,
        41
      ]
    },
    {
      "id": 46,
      "x": 0.574,
      "y": 1.021,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 45,
      "x": -0.573,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 50,
      "x": 0.944,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        43
      ]
    },
    {
      "id": 44,
      "x": -0.944,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        42
      ]
    },
    {
      "id": 42,
      "x": -0.944,
      "y": 0.224,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 0.001,
      "y": 0.136,
      "layer": 3,
      "closed_by": [
        39,
        40
      ]
    },
    {
      "id": 39,
      "x": -0.331,
      "y": -0.172,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 38,
      "x": -0.001,
      "y": -0.574,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 43,
      "x": 0.944,
      "y": 0.216,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -0.944,
      "y": -0.335,
      "layer": 5,
      "closed_by": [
        42
      ]
    },
    {
      "id": 40,
      "x": 0.337,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 35,
      "x": 0.944,
      "y": -0.337,
      "layer": 5,
      "closed_by": [
        43
      ]
    },
    {
      "id": 33,
      "x": 1.345,
      "y": -0.333,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 32,
      "x": -1.343,
      "y": -0.333,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 31,
      "x": 1.582,
      "y": 0.063,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 30,
      "x": -1.585,
      "y": 0.061,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 29,
      "x": 1.825,
      "y": 0.462,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 28,
      "x": -1.824,
      "y": 0.462,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 36,
      "x": 2.065,
      "y": 0.856,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 27,
      "x": -2.065,
      "y": 0.864,
      "layer": 1,
      "closed_by": [
        28
      ]
    }
  ],
  "cards_in_layout_amount": 23,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 49.9,
    "star2": 4.0,
    "star3": 45.7
  }
}