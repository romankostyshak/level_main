{
  "layout_cards": [
    {
      "id": 49,
      "x": 0.004,
      "y": 0.3,
      "layer": 1,
      "closed_by": [
        48,
        47,
        46,
        45
      ]
    },
    {
      "id": 48,
      "x": 0.263,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 47,
      "x": -0.259,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 46,
      "x": 0.263,
      "y": -0.104,
      "layer": 2,
      "closed_by": [
        50,
        44
      ]
    },
    {
      "id": 45,
      "x": -0.256,
      "y": -0.107,
      "layer": 2,
      "closed_by": [
        50,
        43
      ]
    },
    {
      "id": 50,
      "x": -0.001,
      "y": -0.263,
      "layer": 3,
      "closed_by": [
        42,
        41
      ]
    },
    {
      "id": 44,
      "x": 0.546,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 43,
      "x": -0.546,
      "y": -0.256,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 38,
      "x": 0.004,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        39,
        40
      ]
    },
    {
      "id": 39,
      "x": -0.384,
      "y": 0.939,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 0.391,
      "y": 0.939,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -0.312,
      "y": -0.414,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 0.305,
      "y": -0.418,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 1.133,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        33,
        29
      ]
    },
    {
      "id": 33,
      "x": 1.133,
      "y": 0.377,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 32,
      "x": -1.133,
      "y": 0.379,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 31,
      "x": 1.133,
      "y": 0.938,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 30,
      "x": -1.133,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 28,
      "x": -1.131,
      "y": -0.574,
      "layer": 2,
      "closed_by": [
        21
      ]
    },
    {
      "id": 34,
      "x": -1.131,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        32,
        28
      ]
    },
    {
      "id": 29,
      "x": 1.133,
      "y": -0.572,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 36,
      "x": 1.506,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        25
      ]
    },
    {
      "id": 27,
      "x": -1.506,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        26
      ]
    },
    {
      "id": 25,
      "x": 1.906,
      "y": 0.781,
      "layer": 5,
      "closed_by": [
        24
      ]
    },
    {
      "id": 26,
      "x": -1.901,
      "y": 0.777,
      "layer": 5,
      "closed_by": [
        23
      ]
    },
    {
      "id": 24,
      "x": 2.302,
      "y": 0.699,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 23,
      "x": -2.223,
      "y": 0.699,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 22,
      "x": 1.506,
      "y": -0.493,
      "layer": 3,
      "closed_by": [
        51
      ]
    },
    {
      "id": 21,
      "x": -1.503,
      "y": -0.497,
      "layer": 3,
      "closed_by": [
        20
      ]
    },
    {
      "id": 51,
      "x": 1.904,
      "y": -0.412,
      "layer": 4,
      "closed_by": [
        19
      ]
    },
    {
      "id": 20,
      "x": -1.906,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        18
      ]
    },
    {
      "id": 19,
      "x": 2.305,
      "y": -0.331,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 18,
      "x": -2.223,
      "y": -0.335,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 33,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 28.1,
    "star2": 64.6,
    "star3": 7.2
  }
}