{
  "layout_cards": [
    {
      "id": 25,
      "x": 1.151,
      "y": -0.143,
      "angle": 344.997,
      "layer": 5,
      "closed_by": [
        26
      ]
    },
    {
      "id": 26,
      "x": 0.873,
      "y": -0.416,
      "layer": 6,
      "closed_by": [
        51
      ]
    },
    {
      "id": 27,
      "x": 0.293,
      "y": -0.418,
      "layer": 6,
      "closed_by": [
        50,
        51
      ]
    },
    {
      "id": 28,
      "x": -0.284,
      "y": -0.412,
      "layer": 6,
      "closed_by": [
        49,
        50
      ]
    },
    {
      "id": 29,
      "x": 1.24,
      "y": 0.23,
      "layer": 4,
      "closed_by": [
        25
      ]
    },
    {
      "id": 30,
      "x": -1.22,
      "y": 0.23,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 31,
      "x": 0.939,
      "y": 0.469,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 32,
      "x": -0.939,
      "y": 0.469,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 33,
      "x": -1.519,
      "y": 0.469,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 34,
      "x": -0.85,
      "y": -0.414,
      "layer": 6,
      "closed_by": [
        49
      ]
    },
    {
      "id": 35,
      "x": -1.139,
      "y": -0.128,
      "angle": 15.0,
      "layer": 5,
      "closed_by": [
        34
      ]
    },
    {
      "id": 36,
      "x": 1.84,
      "y": 0.708,
      "angle": 300.0,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 37,
      "x": 1.526,
      "y": 0.469,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 38,
      "x": 1.24,
      "y": 0.73,
      "layer": 2,
      "closed_by": [
        31,
        37
      ]
    },
    {
      "id": 39,
      "x": -0.638,
      "y": 0.717,
      "angle": 300.0,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 40,
      "x": -1.22,
      "y": 0.73,
      "layer": 2,
      "closed_by": [
        32,
        33
      ]
    },
    {
      "id": 41,
      "x": 0.625,
      "y": 0.73,
      "angle": 60.0,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 42,
      "x": -1.819,
      "y": 0.73,
      "angle": 60.0,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 43,
      "x": 2.177,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 44,
      "x": -2.16,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 45,
      "x": -0.421,
      "y": -0.157,
      "angle": 340.0,
      "layer": 5,
      "closed_by": [
        28,
        34
      ]
    },
    {
      "id": 46,
      "x": 0.423,
      "y": -0.156,
      "angle": 20.0,
      "layer": 5,
      "closed_by": [
        26,
        27
      ]
    },
    {
      "id": 47,
      "x": -0.335,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 48,
      "x": 0.331,
      "y": 0.944,
      "layer": 1,
      "closed_by": [
        41
      ]
    },
    {
      "id": 49,
      "x": -0.583,
      "y": -0.495,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 50,
      "y": -0.497,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 51,
      "x": 0.596,
      "y": -0.495,
      "layer": 7,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 27,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 59.6,
    "star2": 5.7,
    "star3": 33.7
  }
}