{
  "layout_cards": [
    {
      "id": 8,
      "x": -0.546,
      "y": 0.303,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 9,
      "x": 0.002,
      "y": 0.307,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 10,
      "x": 0.546,
      "y": 0.307,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 12,
      "x": -1.523,
      "y": -0.165,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        15
      ]
    },
    {
      "id": 13,
      "x": -1.524,
      "y": 0.768,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        16
      ]
    },
    {
      "id": 14,
      "x": 1.524,
      "y": 0.762,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 15,
      "x": -1.463,
      "y": -0.377,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        20
      ]
    },
    {
      "id": 16,
      "x": -1.463,
      "y": 0.897,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        20
      ]
    },
    {
      "id": 17,
      "x": 1.457,
      "y": -0.293,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 18,
      "x": 1.518,
      "y": -0.158,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        17
      ]
    },
    {
      "id": 19,
      "x": 1.457,
      "y": 0.901,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 20,
      "x": -1.373,
      "y": 0.3,
      "layer": 4,
      "closed_by": [
        22,
        38
      ]
    },
    {
      "id": 21,
      "x": 1.373,
      "y": 0.3,
      "layer": 4,
      "closed_by": [
        23,
        24
      ]
    },
    {
      "id": 22,
      "x": -1.371,
      "y": 0.939,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 1.373,
      "y": -0.418,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 1.373,
      "y": 0.939,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 25,
      "x": -0.787,
      "y": -0.18,
      "layer": 5,
      "closed_by": [
        8
      ]
    },
    {
      "id": 26,
      "x": -0.786,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        8
      ]
    },
    {
      "id": 27,
      "x": 0.782,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        10
      ]
    },
    {
      "id": 28,
      "x": 0.787,
      "y": -0.18,
      "layer": 5,
      "closed_by": [
        10
      ]
    },
    {
      "id": 29,
      "x": 0.263,
      "y": -0.178,
      "layer": 5,
      "closed_by": [
        9,
        10
      ]
    },
    {
      "id": 30,
      "x": -0.256,
      "y": 0.697,
      "layer": 5,
      "closed_by": [
        8,
        9
      ]
    },
    {
      "id": 31,
      "x": -0.256,
      "y": -0.179,
      "layer": 5,
      "closed_by": [
        8,
        9
      ]
    },
    {
      "id": 32,
      "x": -0.546,
      "y": -0.416,
      "layer": 4,
      "closed_by": [
        25,
        31
      ]
    },
    {
      "id": 33,
      "x": 0.546,
      "y": -0.416,
      "layer": 4,
      "closed_by": [
        28,
        29
      ]
    },
    {
      "id": 34,
      "x": -0.546,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        26,
        30
      ]
    },
    {
      "id": 35,
      "x": 0.546,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        27,
        37
      ]
    },
    {
      "id": 36,
      "y": -0.416,
      "layer": 4,
      "closed_by": [
        29,
        31
      ]
    },
    {
      "id": 37,
      "x": 0.263,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        9,
        10
      ]
    },
    {
      "id": 38,
      "x": -1.371,
      "y": -0.416,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 39,
      "x": 0.002,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        30,
        37
      ]
    },
    {
      "id": 40,
      "x": -0.305,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        32,
        36
      ]
    },
    {
      "id": 41,
      "x": 0.305,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        33,
        36
      ]
    },
    {
      "id": 42,
      "x": -0.305,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        34,
        39
      ]
    },
    {
      "id": 43,
      "x": 0.307,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        35,
        39
      ]
    },
    {
      "id": 44,
      "x": -0.305,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 45,
      "x": 0.305,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 46,
      "x": 0.307,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 47,
      "x": 0.305,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 48,
      "x": 0.307,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 49,
      "x": -0.305,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 50,
      "x": -0.305,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        51
      ]
    },
    {
      "id": 51,
      "x": -0.305,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        42
      ]
    }
  ],
  "cards_in_layout_amount": 43,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 1.5,
    "star2": 34.7,
    "star3": 63.3
  }
}