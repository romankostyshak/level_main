{
  "layout_cards": [
    {
      "id": 49,
      "x": 0.305,
      "y": 0.296,
      "layer": 1,
      "closed_by": [
        46,
        47
      ]
    },
    {
      "id": 48,
      "x": -0.303,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        46,
        47
      ]
    },
    {
      "id": 46,
      "x": -0.004,
      "y": -0.178,
      "layer": 2,
      "closed_by": [
        44,
        43
      ]
    },
    {
      "id": 47,
      "x": -0.002,
      "y": 0.694,
      "layer": 2,
      "closed_by": [
        45,
        50
      ]
    },
    {
      "id": 45,
      "x": 0.358,
      "y": 0.712,
      "angle": 5.0,
      "layer": 3,
      "closed_by": [
        42,
        36
      ]
    },
    {
      "id": 44,
      "x": 0.356,
      "y": -0.193,
      "angle": 355.0,
      "layer": 3,
      "closed_by": [
        40,
        28
      ]
    },
    {
      "id": 42,
      "x": 0.703,
      "y": 0.759,
      "angle": 10.0,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 41,
      "x": -0.708,
      "y": 0.758,
      "angle": 350.0,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 40,
      "x": 0.712,
      "y": -0.238,
      "angle": 350.0,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 39,
      "x": -0.717,
      "y": -0.238,
      "angle": 10.0,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 38,
      "x": 1.059,
      "y": 0.841,
      "angle": 15.0,
      "layer": 5,
      "closed_by": [
        32
      ]
    },
    {
      "id": 35,
      "x": -1.059,
      "y": 0.833,
      "angle": 345.0,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 34,
      "x": 1.082,
      "y": -0.326,
      "angle": 345.0,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 33,
      "x": -1.057,
      "y": -0.314,
      "angle": 15.0,
      "layer": 5,
      "closed_by": [
        29
      ]
    },
    {
      "id": 32,
      "x": 1.421,
      "y": 0.957,
      "angle": 20.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -1.411,
      "y": 0.944,
      "angle": 340.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 1.447,
      "y": -0.444,
      "angle": 340.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -1.417,
      "y": -0.425,
      "angle": 20.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 36,
      "y": 0.859,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 28,
      "x": -0.001,
      "y": -0.337,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 43,
      "x": -0.358,
      "y": -0.194,
      "angle": 5.0,
      "layer": 3,
      "closed_by": [
        39,
        28
      ]
    },
    {
      "id": 50,
      "x": -0.354,
      "y": 0.711,
      "angle": 355.0,
      "layer": 3,
      "closed_by": [
        41,
        36
      ]
    },
    {
      "id": 27,
      "x": 1.799,
      "y": 0.828,
      "angle": 25.0,
      "layer": 5,
      "closed_by": [
        32
      ]
    },
    {
      "id": 26,
      "x": 1.804,
      "y": -0.303,
      "angle": 335.0,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 25,
      "x": -1.802,
      "y": 0.833,
      "angle": 335.0,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 24,
      "x": -1.804,
      "y": -0.303,
      "angle": 25.0,
      "layer": 5,
      "closed_by": [
        29
      ]
    },
    {
      "id": 23,
      "x": 2.006,
      "y": -0.207,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [
        26
      ]
    },
    {
      "id": 22,
      "x": 1.998,
      "y": 0.736,
      "angle": 30.0,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 21,
      "x": -2.006,
      "y": 0.74,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [
        25
      ]
    },
    {
      "id": 37,
      "x": -2.009,
      "y": -0.209,
      "angle": 30.0,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 20,
      "x": 2.151,
      "y": -0.206,
      "angle": 325.0,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 19,
      "x": 2.144,
      "y": 0.731,
      "angle": 35.0,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 18,
      "x": -2.15,
      "y": -0.209,
      "angle": 35.0,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 15,
      "x": -2.148,
      "y": 0.731,
      "angle": 325.0,
      "layer": 2,
      "closed_by": [
        21
      ]
    },
    {
      "id": 14,
      "x": 2.298,
      "y": -0.2,
      "angle": 320.0,
      "layer": 1,
      "closed_by": [
        20
      ]
    },
    {
      "id": 13,
      "x": 2.288,
      "y": 0.736,
      "angle": 40.0,
      "layer": 1,
      "closed_by": [
        19
      ]
    },
    {
      "id": 12,
      "x": -2.296,
      "y": -0.209,
      "angle": 40.0,
      "layer": 1,
      "closed_by": [
        18
      ]
    },
    {
      "id": 17,
      "x": -2.292,
      "y": 0.731,
      "angle": 320.0,
      "layer": 1,
      "closed_by": [
        15
      ]
    }
  ],
  "cards_in_layout_amount": 38,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 4.0,
    "star2": 59.9,
    "star3": 35.3
  }
}