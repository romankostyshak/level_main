{
  "layout_cards": [
    {
      "id": 4,
      "x": -1.503,
      "y": 0.86,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 5,
      "x": 1.503,
      "y": -0.421,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 6,
      "x": -1.503,
      "y": -0.416,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 8,
      "x": 1.506,
      "y": 0.861,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 9,
      "x": 1.746,
      "y": -0.5,
      "layer": 5,
      "closed_by": [
        5
      ]
    },
    {
      "id": 10,
      "x": -1.745,
      "y": 0.943,
      "layer": 5,
      "closed_by": [
        4
      ]
    },
    {
      "id": 11,
      "x": 1.745,
      "y": 0.944,
      "layer": 5,
      "closed_by": [
        8
      ]
    },
    {
      "id": 12,
      "x": -1.131,
      "y": 0.782,
      "layer": 5,
      "closed_by": [
        4
      ]
    },
    {
      "id": 13,
      "x": 1.133,
      "y": 0.782,
      "layer": 5,
      "closed_by": [
        8
      ]
    },
    {
      "id": 14,
      "x": 1.141,
      "y": -0.34,
      "layer": 5,
      "closed_by": [
        5
      ]
    },
    {
      "id": 15,
      "x": -0.002,
      "y": 0.694,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 16,
      "x": -1.133,
      "y": -0.335,
      "layer": 5,
      "closed_by": [
        6
      ]
    },
    {
      "id": 17,
      "x": -1.746,
      "y": -0.495,
      "layer": 5,
      "closed_by": [
        6
      ]
    },
    {
      "id": 18,
      "x": 0.001,
      "y": -0.259,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 19,
      "x": 0.004,
      "y": -0.573,
      "layer": 4,
      "closed_by": [
        18
      ]
    },
    {
      "id": 20,
      "x": -0.001,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        15
      ]
    },
    {
      "id": 21,
      "x": 0.333,
      "y": -0.495,
      "layer": 3,
      "closed_by": [
        19
      ]
    },
    {
      "id": 22,
      "x": 0.331,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        20
      ]
    },
    {
      "id": 23,
      "x": -0.333,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        20
      ]
    },
    {
      "id": 24,
      "x": -1.503,
      "y": -0.416,
      "layer": 3,
      "closed_by": [
        16,
        17
      ]
    },
    {
      "id": 25,
      "x": -1.503,
      "y": 0.86,
      "layer": 3,
      "closed_by": [
        10,
        12
      ]
    },
    {
      "id": 26,
      "x": 1.506,
      "y": 0.864,
      "layer": 3,
      "closed_by": [
        11,
        13
      ]
    },
    {
      "id": 27,
      "x": 1.503,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        9,
        14
      ]
    },
    {
      "id": 28,
      "x": 0.92,
      "y": -0.238,
      "layer": 3,
      "closed_by": [
        14
      ]
    },
    {
      "id": 29,
      "x": 0.935,
      "y": 0.68,
      "layer": 3,
      "closed_by": [
        13
      ]
    },
    {
      "id": 30,
      "x": -0.925,
      "y": 0.685,
      "layer": 3,
      "closed_by": [
        12
      ]
    },
    {
      "id": 31,
      "x": -0.421,
      "y": 0.731,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        23,
        30
      ]
    },
    {
      "id": 32,
      "x": 0.428,
      "y": 0.726,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        22,
        29
      ]
    },
    {
      "id": 33,
      "x": -0.439,
      "y": -0.289,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        36,
        37
      ]
    },
    {
      "id": 34,
      "x": 0.416,
      "y": -0.286,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        21,
        28
      ]
    },
    {
      "id": 35,
      "x": -1.121,
      "y": 0.736,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        25,
        30
      ]
    },
    {
      "id": 36,
      "x": -0.925,
      "y": -0.244,
      "layer": 3,
      "closed_by": [
        16
      ]
    },
    {
      "id": 37,
      "x": -0.331,
      "y": -0.493,
      "layer": 3,
      "closed_by": [
        19
      ]
    },
    {
      "id": 38,
      "x": 1.126,
      "y": 0.731,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        26,
        29
      ]
    },
    {
      "id": 39,
      "x": 1.116,
      "y": -0.289,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        27,
        28
      ]
    },
    {
      "id": 40,
      "x": -1.123,
      "y": -0.298,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        24,
        36
      ]
    },
    {
      "id": 41,
      "x": -1.131,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 42,
      "x": -1.131,
      "y": 0.782,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 43,
      "x": 1.133,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 44,
      "x": 1.133,
      "y": 0.785,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 45,
      "x": -0.569,
      "y": 0.782,
      "layer": 1,
      "closed_by": [
        31,
        35
      ]
    },
    {
      "id": 46,
      "x": 0.573,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        34,
        39
      ]
    },
    {
      "id": 47,
      "x": 0.574,
      "y": 0.781,
      "layer": 1,
      "closed_by": [
        32,
        38
      ]
    },
    {
      "id": 48,
      "x": -0.003,
      "y": 0.785,
      "layer": 1,
      "closed_by": [
        31,
        32
      ]
    },
    {
      "id": 49,
      "x": -0.012,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        33,
        34
      ]
    },
    {
      "id": 50,
      "x": -0.569,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        33,
        40
      ]
    }
  ],
  "cards_in_layout_amount": 46,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 0.2,
    "star2": 13.4,
    "star3": 86.1
  }
}