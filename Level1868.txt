{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.004,
      "y": -0.252,
      "layer": 1,
      "closed_by": [
        41,
        40
      ]
    },
    {
      "id": 50,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        41,
        40
      ]
    },
    {
      "id": 49,
      "x": -0.546,
      "y": 0.782,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": 0.546,
      "y": -0.335,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "x": 0.172,
      "y": -0.335,
      "layer": 4,
      "closed_by": [
        44,
        39
      ]
    },
    {
      "id": 46,
      "x": -0.165,
      "y": 0.782,
      "layer": 4,
      "closed_by": [
        45,
        38
      ]
    },
    {
      "id": 45,
      "x": 0.228,
      "y": 0.781,
      "layer": 5,
      "closed_by": [
        43
      ]
    },
    {
      "id": 44,
      "x": -0.224,
      "y": -0.337,
      "layer": 5,
      "closed_by": [
        42
      ]
    },
    {
      "id": 43,
      "x": 0.625,
      "y": 0.781,
      "layer": 6,
      "closed_by": [
        39
      ]
    },
    {
      "id": 42,
      "x": -0.625,
      "y": -0.335,
      "layer": 6,
      "closed_by": [
        38
      ]
    },
    {
      "id": 41,
      "x": 0.397,
      "y": 0.254,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        48,
        46
      ]
    },
    {
      "id": 40,
      "x": -0.398,
      "y": 0.2,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        49,
        47
      ]
    },
    {
      "id": 39,
      "x": 0.386,
      "y": 0.23,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -0.386,
      "y": 0.224,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -1.899,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        32
      ]
    },
    {
      "id": 30,
      "x": -1.419,
      "y": -0.337,
      "layer": 6,
      "closed_by": [
        31
      ]
    },
    {
      "id": 29,
      "x": 1.213,
      "y": 0.777,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 1.659,
      "y": 0.777,
      "layer": 5,
      "closed_by": [
        37
      ]
    },
    {
      "id": 37,
      "x": 1.419,
      "y": 0.777,
      "layer": 6,
      "closed_by": [
        29
      ]
    },
    {
      "id": 27,
      "x": 1.899,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 26,
      "x": 2.138,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 25,
      "x": 2.38,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        26
      ]
    },
    {
      "id": 24,
      "x": 2.628,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        25
      ]
    },
    {
      "id": 31,
      "x": -1.212,
      "y": -0.337,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -1.659,
      "y": -0.337,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 36,
      "x": -2.378,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 34,
      "x": -2.14,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 35,
      "x": -2.625,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        36
      ]
    }
  ],
  "cards_in_layout_amount": 28,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 60.2,
    "star2": 19.0,
    "star3": 20.2
  }
}