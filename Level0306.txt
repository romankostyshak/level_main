{
  "layout_cards": [
    {
      "id": 51,
      "x": -1.514,
      "y": 0.456,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 50,
      "x": -2.029,
      "y": 0.456,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 49,
      "x": -2.551,
      "y": 0.453,
      "layer": 1,
      "closed_by": [
        27
      ]
    },
    {
      "id": 48,
      "x": -2.552,
      "y": 0.456,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 47,
      "x": -2.029,
      "y": -0.172,
      "layer": 3,
      "closed_by": [
        31,
        28
      ]
    },
    {
      "id": 46,
      "x": -1.508,
      "y": 0.456,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 45,
      "x": 2.581,
      "y": 0.453,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 44,
      "x": -0.522,
      "y": 0.72,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 43,
      "x": -0.518,
      "y": 0.717,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 42,
      "x": 2.061,
      "y": -0.18,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 41,
      "y": 0.075,
      "layer": 3,
      "closed_by": [
        38,
        33
      ]
    },
    {
      "id": 40,
      "y": 0.717,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 39,
      "x": 2.585,
      "y": 0.458,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 38,
      "x": -0.27,
      "y": 0.43,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 37,
      "x": 2.059,
      "y": -0.172,
      "layer": 3,
      "closed_by": [
        34,
        22
      ]
    },
    {
      "id": 36,
      "x": 2.059,
      "y": 0.458,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 35,
      "x": 2.585,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 34,
      "x": 2.398,
      "y": 0.229,
      "angle": 45.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 0.279,
      "y": 0.43,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -0.522,
      "y": 0.075,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 31,
      "x": -2.368,
      "y": 0.224,
      "angle": 315.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -2.029,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 29,
      "x": -1.508,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 28,
      "x": -1.69,
      "y": -0.007,
      "angle": 315.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 27,
      "x": -2.551,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 26,
      "y": 0.079,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 25,
      "x": 1.536,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 24,
      "x": 1.534,
      "y": 0.46,
      "layer": 1,
      "closed_by": [
        25
      ]
    },
    {
      "id": 23,
      "x": 1.534,
      "y": 0.46,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 22,
      "x": 1.717,
      "angle": 45.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 19,
      "x": 0.518,
      "y": 0.72,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 21,
      "x": 0.518,
      "y": 0.079,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 20,
      "x": 0.518,
      "y": 0.72,
      "layer": 1,
      "closed_by": [
        21
      ]
    }
  ],
  "cards_in_layout_amount": 33,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 12.9,
    "star2": 72.1,
    "star3": 14.2
  }
}