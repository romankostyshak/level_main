{
  "layout_cards": [
    {
      "id": 48,
      "x": -0.948,
      "y": 0.321,
      "angle": 5.0,
      "layer": 1,
      "closed_by": [
        49,
        39
      ]
    },
    {
      "id": 49,
      "x": -0.81,
      "y": 0.671,
      "angle": 10.0,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "x": -0.509,
      "y": 0.754,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 46,
      "x": -0.412,
      "y": 0.824,
      "angle": 30.0,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 45,
      "x": 0.947,
      "y": 0.321,
      "angle": 355.0,
      "layer": 1,
      "closed_by": [
        50,
        40
      ]
    },
    {
      "id": 50,
      "x": 0.814,
      "y": 0.674,
      "angle": 350.0,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 43,
      "x": 0.414,
      "y": 0.818,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 44,
      "x": 0.513,
      "y": 0.754,
      "angle": 340.0,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 41,
      "x": 0.273,
      "y": 0.87,
      "layer": 5,
      "closed_by": [
        27
      ]
    },
    {
      "id": 40,
      "x": 1.151,
      "y": -0.157,
      "angle": 355.0,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 39,
      "x": -1.149,
      "y": -0.165,
      "angle": 5.0,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 38,
      "x": 0.865,
      "y": -0.228,
      "angle": 350.0,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 35,
      "x": -0.861,
      "y": -0.221,
      "angle": 10.0,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 34,
      "x": 0.794,
      "y": -0.291,
      "angle": 340.0,
      "layer": 4,
      "closed_by": [
        32
      ]
    },
    {
      "id": 33,
      "x": -0.787,
      "y": -0.289,
      "angle": 20.0,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 32,
      "x": 0.708,
      "y": -0.361,
      "angle": 330.0,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 31,
      "x": -0.697,
      "y": -0.367,
      "angle": 30.0,
      "layer": 5,
      "closed_by": [
        29
      ]
    },
    {
      "id": 30,
      "x": 0.492,
      "y": -0.412,
      "layer": 6,
      "closed_by": [
        36
      ]
    },
    {
      "id": 29,
      "x": -0.492,
      "y": -0.416,
      "layer": 6,
      "closed_by": [
        28
      ]
    },
    {
      "id": 28,
      "x": -0.268,
      "y": -0.497,
      "layer": 7,
      "closed_by": [
        26
      ]
    },
    {
      "id": 36,
      "x": 0.275,
      "y": -0.497,
      "layer": 7,
      "closed_by": [
        26
      ]
    },
    {
      "id": 26,
      "y": -0.573,
      "layer": 8,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -0.27,
      "y": 0.875,
      "layer": 5,
      "closed_by": [
        27
      ]
    },
    {
      "id": 27,
      "y": 0.939,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 24,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 49.1,
    "star2": 6.4,
    "star3": 43.9
  }
}