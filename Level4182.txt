{
  "layout_cards": [
    {
      "id": 16,
      "x": 0.623,
      "y": 0.74,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 17,
      "x": 0.503,
      "y": 0.041,
      "layer": 1,
      "closed_by": [
        21
      ]
    },
    {
      "id": 18,
      "x": 1.023,
      "y": 0.041,
      "layer": 1,
      "closed_by": [
        21
      ]
    },
    {
      "id": 19,
      "x": 1.023,
      "y": 0.861,
      "layer": 1,
      "closed_by": [
        21
      ]
    },
    {
      "id": 20,
      "x": 0.503,
      "y": 0.861,
      "layer": 1,
      "closed_by": [
        21
      ]
    },
    {
      "id": 21,
      "x": 0.902,
      "y": 0.158,
      "layer": 2,
      "closed_by": [
        16
      ]
    },
    {
      "id": 22,
      "x": 0.902,
      "y": 0.74,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 23,
      "x": 0.623,
      "y": 0.158,
      "layer": 5,
      "closed_by": [
        24
      ]
    },
    {
      "id": 24,
      "x": 0.759,
      "y": 0.439,
      "layer": 6,
      "closed_by": [],
      "card_type": 2
    },
    {
      "id": 25,
      "x": -2.374,
      "y": 0.537,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 26,
      "x": -2.374,
      "y": -0.282,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 27,
      "x": -1.853,
      "y": -0.282,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 28,
      "x": -1.853,
      "y": 0.537,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 29,
      "x": -2.273,
      "y": -0.158,
      "layer": 2,
      "closed_by": [
        30
      ],
      "card_type": 3
    },
    {
      "id": 30,
      "x": -1.973,
      "y": 0.416,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 31,
      "x": -1.973,
      "y": -0.162,
      "layer": 4,
      "closed_by": [
        32
      ]
    },
    {
      "id": 32,
      "x": -2.273,
      "y": 0.416,
      "layer": 5,
      "closed_by": [
        33
      ]
    },
    {
      "id": 33,
      "x": -2.134,
      "y": 0.115,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -0.694,
      "y": 0.282,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -0.935,
      "y": -0.115,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 36,
      "x": -0.833,
      "y": 0.002,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 37,
      "x": -0.414,
      "y": -0.115,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 38,
      "x": -0.535,
      "y": 0.002,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 39,
      "x": -0.935,
      "y": 0.702,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 40,
      "x": -0.833,
      "y": 0.582,
      "layer": 5,
      "closed_by": [
        34
      ]
    },
    {
      "id": 41,
      "x": -0.535,
      "y": 0.582,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 42,
      "x": -0.414,
      "y": 0.702,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 43,
      "x": 2.18,
      "y": 0.601,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 44,
      "x": 1.94,
      "y": 0.202,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 45,
      "x": 2.039,
      "y": 0.324,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 46,
      "x": 2.46,
      "y": 0.202,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 47,
      "x": 2.341,
      "y": 0.324,
      "layer": 4,
      "closed_by": [
        49
      ]
    },
    {
      "id": 48,
      "x": 1.94,
      "y": 1.024,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 49,
      "x": 2.039,
      "y": 0.902,
      "layer": 5,
      "closed_by": [
        43
      ]
    },
    {
      "id": 50,
      "x": 2.341,
      "y": 0.902,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 51,
      "x": 2.46,
      "y": 1.024,
      "layer": 1,
      "closed_by": [
        45
      ]
    }
  ],
  "cards_in_layout_amount": 36,
  "cards_in_deck_amount": 30,
  "layers_in_level": 6,
  "open_cards": 4,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 43.4,
    "star2": 54.2,
    "star3": 2.2
  }
}