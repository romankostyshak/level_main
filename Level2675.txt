{
  "layout_cards": [
    {
      "id": 19,
      "x": -2.93,
      "y": 0.777,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 20,
      "x": -0.587,
      "y": 0.777,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 21,
      "x": 0.425,
      "y": -0.337,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 22,
      "x": 0.947,
      "y": -0.335,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 2.253,
      "y": -0.335,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 1.044,
      "y": -0.256,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 25,
      "x": 2.134,
      "y": -0.256,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 26,
      "x": 1.284,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 27,
      "x": 1.898,
      "y": -0.177,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 28,
      "x": 1.598,
      "y": 0.782,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 29,
      "x": 1.927,
      "y": 0.832,
      "angle": 15.0,
      "layer": 1,
      "closed_by": [
        28,
        31
      ]
    },
    {
      "id": 30,
      "x": 1.251,
      "y": 0.824,
      "angle": 345.0,
      "layer": 1,
      "closed_by": [
        28,
        32
      ]
    },
    {
      "id": 31,
      "x": 2.19,
      "y": 0.902,
      "angle": 15.0,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 0.995,
      "y": 0.896,
      "angle": 345.0,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 0.731,
      "y": 0.967,
      "angle": 345.0,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 34,
      "x": 2.444,
      "y": 0.971,
      "angle": 15.0,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 35,
      "x": -2.41,
      "y": 0.777,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 37,
      "x": 2.769,
      "y": -0.337,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -1.108,
      "y": 0.782,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -2.305,
      "y": 0.703,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 40,
      "x": -1.21,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 41,
      "x": -2.062,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 42,
      "x": -1.452,
      "y": 0.623,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 43,
      "x": -1.758,
      "y": 0.545,
      "layer": 1,
      "closed_by": [
        41,
        42
      ]
    },
    {
      "id": 44,
      "x": -1.758,
      "y": -0.34,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 45,
      "x": -0.912,
      "y": -0.523,
      "angle": 345.0,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 46,
      "x": -2.354,
      "y": -0.446,
      "angle": 15.0,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 47,
      "x": -1.151,
      "y": -0.456,
      "angle": 345.0,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 48,
      "x": -1.416,
      "y": -0.384,
      "angle": 345.0,
      "layer": 1,
      "closed_by": [
        44,
        47
      ]
    },
    {
      "id": 49,
      "x": -2.098,
      "y": -0.377,
      "angle": 15.0,
      "layer": 1,
      "closed_by": [
        44,
        46
      ]
    },
    {
      "id": 50,
      "x": -2.605,
      "y": -0.513,
      "angle": 15.0,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 51,
      "x": 1.59,
      "y": -0.093,
      "layer": 1,
      "closed_by": [
        26,
        27
      ]
    }
  ],
  "level_mechanic": 1,
  "free_cards_from_stickers": [
    21,
    22,
    23,
    24,
    25,
    26,
    27,
    28,
    29,
    30,
    31,
    32,
    33,
    34,
    37,
    51
  ],
  "cards_in_layout_amount": 32,
  "cards_in_deck_amount": 24,
  "layers_in_level": 5,
  "open_cards": 14,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 0.1,
    "star2": 16.6,
    "star3": 82.3
  }
}