{
  "layout_cards": [
    {
      "id": 13,
      "x": -1.11,
      "y": -0.499,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 14,
      "x": -1.116,
      "y": 0.939,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 15,
      "x": -0.712,
      "y": 0.224,
      "layer": 4,
      "closed_by": [
        13,
        14
      ]
    },
    {
      "id": 16,
      "x": 0.041,
      "y": -0.279,
      "angle": 20.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 17,
      "x": 0.05,
      "y": 0.722,
      "angle": 340.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 18,
      "x": -0.216,
      "y": -0.374,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        15,
        16
      ]
    },
    {
      "id": 19,
      "x": -0.209,
      "y": 0.818,
      "angle": 340.0,
      "layer": 3,
      "closed_by": [
        15,
        17
      ]
    },
    {
      "id": 20,
      "x": -0.314,
      "y": 0.221,
      "layer": 1,
      "closed_by": [
        21,
        22
      ]
    },
    {
      "id": 21,
      "x": -0.717,
      "y": -0.497,
      "layer": 2,
      "closed_by": [
        18,
        24
      ]
    },
    {
      "id": 22,
      "x": -0.712,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        19,
        24
      ]
    },
    {
      "id": 23,
      "x": -1.514,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 24,
      "x": -1.116,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        15,
        25,
        26
      ]
    },
    {
      "id": 25,
      "x": -1.514,
      "y": -0.497,
      "layer": 4,
      "closed_by": [
        13,
        31
      ]
    },
    {
      "id": 26,
      "x": -1.514,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        14,
        31
      ]
    },
    {
      "id": 27,
      "x": -2.16,
      "y": -0.337,
      "angle": 340.0,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 28,
      "x": -2.154,
      "y": 0.776,
      "angle": 20.0,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 29,
      "x": -2.22,
      "y": -0.379,
      "angle": 340.0,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 30,
      "x": -2.029,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        28,
        29
      ]
    },
    {
      "id": 31,
      "x": -1.776,
      "y": 0.223,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -2.22,
      "y": 0.824,
      "angle": 20.0,
      "layer": 1,
      "closed_by": [
        30
      ]
    },
    {
      "id": 33,
      "x": 1.506,
      "y": 0.698,
      "layer": 2,
      "closed_by": [
        42,
        45
      ]
    },
    {
      "id": 34,
      "x": 0.55,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        35,
        39
      ]
    },
    {
      "id": 35,
      "x": 0.333,
      "y": 0.223,
      "layer": 4,
      "closed_by": [
        16,
        17
      ]
    },
    {
      "id": 36,
      "x": 0.55,
      "y": -0.256,
      "layer": 3,
      "closed_by": [
        35,
        40
      ]
    },
    {
      "id": 37,
      "x": 0.947,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        34,
        42
      ]
    },
    {
      "id": 38,
      "x": 0.947,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        36,
        42
      ]
    },
    {
      "id": 39,
      "x": 0.949,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 40,
      "x": 0.947,
      "y": -0.256,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 41,
      "x": 0.87,
      "y": 0.228,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 42,
      "x": 1.248,
      "y": 0.224,
      "layer": 3,
      "closed_by": [
        39,
        40,
        47,
        48
      ]
    },
    {
      "id": 43,
      "x": 1.503,
      "y": -0.254,
      "layer": 2,
      "closed_by": [
        42,
        45
      ]
    },
    {
      "id": 44,
      "x": 1.746,
      "y": 0.221,
      "layer": 1,
      "closed_by": [
        33,
        43,
        49
      ]
    },
    {
      "id": 45,
      "x": 1.786,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        47,
        48,
        50
      ]
    },
    {
      "id": 47,
      "x": 1.465,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 48,
      "x": 1.463,
      "y": -0.256,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 49,
      "x": 2.019,
      "y": 0.703,
      "layer": 2,
      "closed_by": [
        45,
        51
      ]
    },
    {
      "id": 50,
      "x": 2.019,
      "y": -0.254,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 51,
      "x": 1.74,
      "y": 0.222,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 38,
  "cards_in_deck_amount": 32,
  "stars_statistic": {
    "star1": 11.0,
    "star2": 68.9,
    "star3": 19.6
  }
}