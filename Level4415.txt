{
  "layout_cards": [
    {
      "x": 2.137,
      "y": 0.303,
      "layer": 3,
      "closed_by": [
        8,
        12
      ]
    },
    {
      "id": 1,
      "x": 2.377,
      "y": 0.623,
      "layer": 1,
      "closed_by": [
        2
      ]
    },
    {
      "id": 2,
      "x": 2.256,
      "y": 0.462,
      "layer": 2,
      "closed_by": [
        0
      ]
    },
    {
      "id": 3,
      "x": 2.076,
      "y": 0.103,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 4,
      "x": 2.137,
      "y": -0.416,
      "layer": 2,
      "closed_by": [
        0,
        11
      ]
    },
    {
      "id": 5,
      "x": 2.377,
      "y": -0.416,
      "layer": 1,
      "closed_by": [
        3,
        4
      ]
    },
    {
      "id": 6,
      "x": 1.217,
      "y": 0.623,
      "layer": 1,
      "closed_by": [
        7
      ]
    },
    {
      "id": 7,
      "x": 1.337,
      "y": 0.462,
      "layer": 2,
      "closed_by": [
        9
      ]
    },
    {
      "id": 8,
      "x": 1.797,
      "y": 0.623,
      "layer": 4,
      "closed_by": [
        3,
        10
      ]
    },
    {
      "id": 9,
      "x": 1.457,
      "y": 0.303,
      "layer": 3,
      "closed_by": [
        8,
        12
      ]
    },
    {
      "id": 10,
      "x": 1.516,
      "y": 0.103,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 11,
      "x": 1.799,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        12
      ]
    },
    {
      "id": 12,
      "x": 1.797,
      "y": -0.416,
      "layer": 4,
      "closed_by": [
        3,
        10
      ]
    },
    {
      "id": 13,
      "x": 1.217,
      "y": -0.416,
      "layer": 1,
      "closed_by": [
        14
      ]
    },
    {
      "id": 14,
      "x": 1.457,
      "y": -0.416,
      "layer": 2,
      "closed_by": [
        9,
        11
      ]
    },
    {
      "id": 21,
      "x": -1.797,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        22,
        51
      ]
    },
    {
      "id": 22,
      "x": -1.516,
      "y": 0.1,
      "layer": 5,
      "closed_by": [],
      "card_value": "",
      "effect_id": 0,
      "card_type": 0
    },
    {
      "id": 23,
      "x": -1.797,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 24,
      "x": -1.797,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        22,
        51
      ],
      "card_value": "",
      "effect_id": 0,
      "card_type": 0
    },
    {
      "id": 25,
      "x": -2.137,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        21,
        24
      ]
    },
    {
      "id": 26,
      "x": -1.337,
      "y": 0.46,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 27,
      "x": -2.256,
      "y": 0.46,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 28,
      "x": -2.377,
      "y": 0.62,
      "layer": 1,
      "closed_by": [
        27,
        51
      ]
    },
    {
      "id": 30,
      "x": -1.457,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        23,
        37
      ]
    },
    {
      "id": 31,
      "x": -2.137,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        23,
        25
      ]
    },
    {
      "id": 32,
      "x": -1.217,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        30
      ]
    },
    {
      "id": 33,
      "x": -2.377,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        31,
        51
      ]
    },
    {
      "id": 34,
      "x": -0.279,
      "y": 0.34,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 0.279,
      "y": 0.34,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -1.217,
      "y": 0.62,
      "layer": 1,
      "closed_by": [
        26
      ]
    },
    {
      "id": 37,
      "x": -1.457,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        21,
        24
      ]
    },
    {
      "id": 38,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        34,
        35
      ]
    },
    {
      "id": 39,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 40,
      "x": -0.579,
      "y": 0.859,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 41,
      "x": 0.578,
      "y": 0.859,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 42,
      "x": -0.338,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        39,
        50
      ]
    },
    {
      "id": 43,
      "x": 0.338,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        39,
        45
      ]
    },
    {
      "id": 44,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        34,
        35
      ]
    },
    {
      "id": 45,
      "x": 0.338,
      "y": 0.138,
      "layer": 3,
      "closed_by": [
        38,
        44
      ]
    },
    {
      "id": 46,
      "x": -0.458,
      "y": -0.018,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 47,
      "x": 0.46,
      "y": -0.018,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 48,
      "x": -0.579,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 49,
      "x": 0.578,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        35,
        47
      ]
    },
    {
      "id": 50,
      "x": -0.338,
      "y": 0.138,
      "layer": 3,
      "closed_by": [
        38,
        44
      ]
    },
    {
      "id": 51,
      "x": -2.076,
      "y": 0.1,
      "layer": 5,
      "closed_by": [],
      "card_value": "",
      "effect_id": 0,
      "card_type": 0
    }
  ],
  "free_cards_from_stickers": [],
  "cards_in_layout_amount": 45,
  "cards_in_deck_amount": 32,
  "layers_in_level": 5,
  "open_cards": 6,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 15.1,
    "star2": 79.0,
    "star3": 5.5
  }
}