{
  "layout_cards": [
    {
      "id": 15,
      "x": 0.578,
      "y": 0.939,
      "angle": 30.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 18,
      "x": -0.579,
      "y": -0.5,
      "angle": 30.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 19,
      "x": 0.298,
      "y": 0.819,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        15
      ]
    },
    {
      "id": 20,
      "x": -0.298,
      "y": -0.379,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        18
      ]
    },
    {
      "id": 22,
      "x": -2.134,
      "y": -0.536,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        51
      ]
    },
    {
      "id": 23,
      "x": -0.314,
      "y": 0.74,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 24,
      "x": 0.014,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        20,
        26
      ]
    },
    {
      "id": 25,
      "x": -0.014,
      "y": 0.782,
      "layer": 1,
      "closed_by": [
        19,
        23
      ]
    },
    {
      "id": 26,
      "x": 0.314,
      "y": -0.298,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        27
      ]
    },
    {
      "id": 27,
      "x": 0.595,
      "y": -0.178,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 28,
      "x": 0.832,
      "y": 0.001,
      "angle": 45.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -0.595,
      "y": 0.623,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 30,
      "x": -0.833,
      "y": 0.442,
      "angle": 45.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 1.075,
      "y": 0.18,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 32,
      "x": -1.075,
      "y": 0.263,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 33,
      "x": 1.375,
      "y": 0.3,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 34,
      "x": -1.375,
      "y": 0.143,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 35,
      "x": 1.679,
      "y": 0.34,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 36,
      "x": -2.733,
      "y": 0.74,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        46
      ]
    },
    {
      "id": 37,
      "x": -1.674,
      "y": 0.103,
      "layer": 1,
      "closed_by": [
        34
      ]
    },
    {
      "id": 38,
      "x": 2.22,
      "y": 0.34,
      "layer": 1,
      "closed_by": [
        40,
        47
      ]
    },
    {
      "id": 39,
      "x": -2.213,
      "y": 0.103,
      "layer": 1,
      "closed_by": [
        22,
        41
      ]
    },
    {
      "id": 40,
      "x": 2.134,
      "y": -0.298,
      "angle": 344.998,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 41,
      "x": -2.134,
      "y": 0.74,
      "angle": 344.998,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 42,
      "x": 2.44,
      "y": -0.338,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 43,
      "x": -2.434,
      "y": 0.782,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 44,
      "x": 2.713,
      "y": -0.298,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        45
      ]
    },
    {
      "id": 45,
      "x": 2.634,
      "y": 0.34,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 46,
      "x": -2.654,
      "y": 0.103,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 47,
      "x": 2.134,
      "y": 0.981,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 48,
      "x": 2.434,
      "y": 1.021,
      "layer": 3,
      "closed_by": [
        49
      ]
    },
    {
      "id": 49,
      "x": 2.713,
      "y": 0.981,
      "angle": 344.998,
      "layer": 4,
      "closed_by": [
        45
      ]
    },
    {
      "id": 50,
      "x": -2.733,
      "y": -0.536,
      "angle": 344.998,
      "layer": 4,
      "closed_by": [
        46
      ],
      "card_value": "",
      "effect_id": 0,
      "card_type": 0
    },
    {
      "id": 51,
      "x": -2.434,
      "y": -0.574,
      "layer": 3,
      "closed_by": [
        50
      ]
    }
  ],
  "free_cards_from_stickers": [],
  "cards_in_layout_amount": 34,
  "cards_in_deck_amount": 28,
  "layers_in_level": 5,
  "open_cards": 6,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 25.4,
    "star2": 70.8,
    "star3": 2.7
  }
}