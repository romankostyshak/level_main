{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.316,
      "y": -0.546,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 50,
      "x": 0.328,
      "y": -0.549,
      "angle": 9.998,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 49,
      "x": 0.004,
      "y": -0.577,
      "layer": 1,
      "closed_by": [
        51,
        50
      ]
    },
    {
      "id": 48,
      "x": 0.638,
      "y": -0.465,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 47,
      "x": -0.629,
      "y": -0.458,
      "angle": 340.0,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 46,
      "x": 0.907,
      "y": -0.335,
      "angle": 30.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 45,
      "x": -0.897,
      "y": -0.326,
      "angle": 330.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 44,
      "y": 0.381,
      "layer": 1,
      "closed_by": [
        43,
        42
      ]
    },
    {
      "id": 43,
      "x": 0.305,
      "y": 0.46,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 42,
      "x": -0.303,
      "y": 0.46,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 41,
      "x": 0.546,
      "y": 0.545,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 40,
      "x": -0.546,
      "y": 0.541,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 39,
      "x": 0.787,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 38,
      "x": -0.786,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 37,
      "x": 1.026,
      "y": 0.702,
      "layer": 5,
      "closed_by": [
        34
      ]
    },
    {
      "id": 36,
      "x": -1.026,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        35
      ]
    },
    {
      "id": 35,
      "x": -0.896,
      "y": 0.777,
      "layer": 6,
      "closed_by": [
        33
      ]
    },
    {
      "id": 34,
      "x": 0.893,
      "y": 0.782,
      "layer": 6,
      "closed_by": [
        32
      ]
    },
    {
      "id": 33,
      "x": -0.651,
      "y": 0.861,
      "layer": 7,
      "closed_by": [
        31
      ]
    },
    {
      "id": 32,
      "x": 0.651,
      "y": 0.865,
      "layer": 7,
      "closed_by": [
        30
      ]
    },
    {
      "id": 31,
      "x": -0.412,
      "y": 0.939,
      "layer": 8,
      "closed_by": [
        29
      ]
    },
    {
      "id": 30,
      "x": 0.414,
      "y": 0.939,
      "layer": 8,
      "closed_by": [
        29
      ]
    },
    {
      "id": 29,
      "x": -0.001,
      "y": 1.019,
      "layer": 9,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 1.621,
      "y": -0.059,
      "angle": 40.0,
      "layer": 5,
      "closed_by": [
        26
      ]
    },
    {
      "id": 27,
      "x": -1.626,
      "y": -0.052,
      "angle": 320.0,
      "layer": 5,
      "closed_by": [
        25
      ]
    },
    {
      "id": 26,
      "x": 1.838,
      "y": 0.15,
      "angle": 50.0,
      "layer": 6,
      "closed_by": [
        24
      ]
    },
    {
      "id": 25,
      "x": -1.843,
      "y": 0.158,
      "angle": 310.0,
      "layer": 6,
      "closed_by": [
        23
      ]
    },
    {
      "id": 23,
      "x": -2.029,
      "y": 0.391,
      "angle": 300.0,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 2.032,
      "y": 0.414,
      "angle": 65.0,
      "layer": 7,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 29,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 54.6,
    "star2": 34.6,
    "star3": 10.1
  }
}