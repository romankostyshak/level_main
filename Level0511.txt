{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.275,
      "y": -0.569,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": 0.523,
      "y": -0.483,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 49,
      "x": -0.513,
      "y": -0.481,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 48,
      "x": 0.79,
      "y": -0.388,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 47,
      "x": -0.787,
      "y": -0.388,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 46,
      "x": 0.523,
      "y": -0.273,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 45,
      "x": -0.513,
      "y": -0.275,
      "layer": 4,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": -0.268,
      "y": -0.179,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 43,
      "x": 0.272,
      "y": -0.179,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 42,
      "x": 0.001,
      "y": 1.023,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 0.545,
      "y": 1.023,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 40,
      "x": -0.545,
      "y": 1.021,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 39,
      "x": 0.279,
      "y": 0.657,
      "layer": 3,
      "closed_by": [
        42,
        41
      ]
    },
    {
      "id": 38,
      "x": -0.531,
      "y": 0.657,
      "layer": 2,
      "closed_by": [
        37,
        35
      ]
    },
    {
      "id": 37,
      "x": -0.27,
      "y": 0.657,
      "layer": 3,
      "closed_by": [
        42,
        40
      ]
    },
    {
      "id": 36,
      "x": -0.8,
      "y": 0.657,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 35,
      "x": -0.8,
      "y": 0.657,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 34,
      "x": -0.268,
      "y": -0.572,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 33,
      "x": 0.805,
      "y": 0.657,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 32,
      "x": 0.808,
      "y": 0.657,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 31,
      "x": 0.531,
      "y": 0.657,
      "layer": 2,
      "closed_by": [
        39,
        32
      ]
    }
  ],
  "cards_in_layout_amount": 21,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 41.0,
    "star2": 1.1,
    "star3": 57.6
  }
}