{
  "layout_cards": [
    {
      "id": 50,
      "x": -1.2,
      "y": -0.1,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 49,
      "x": 0.939,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        47,
        34
      ]
    },
    {
      "id": 48,
      "x": -1.338,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        37,
        40
      ]
    },
    {
      "id": 47,
      "x": 1.338,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        46
      ]
    },
    {
      "id": 46,
      "x": 0.893,
      "y": 0.859,
      "layer": 5,
      "closed_by": [
        24
      ]
    },
    {
      "id": 42,
      "x": 1.746,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 41,
      "x": -1.745,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 39,
      "x": 1.746,
      "y": 0.537,
      "layer": 3,
      "closed_by": [
        47,
        45
      ]
    },
    {
      "id": 38,
      "x": 1.338,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        49,
        39
      ]
    },
    {
      "id": 37,
      "x": -0.939,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        36,
        35
      ]
    },
    {
      "id": 36,
      "x": -1.338,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        25
      ]
    },
    {
      "id": 35,
      "x": -0.572,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        25
      ]
    },
    {
      "id": 34,
      "x": 0.574,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        46
      ]
    },
    {
      "id": 33,
      "x": -0.303,
      "y": 0.158,
      "layer": 5,
      "closed_by": [
        23
      ]
    },
    {
      "id": 32,
      "x": -0.303,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 31,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        28
      ]
    },
    {
      "id": 30,
      "x": 0.305,
      "y": 0.136,
      "layer": 5,
      "closed_by": [
        24
      ]
    },
    {
      "id": 29,
      "x": 0.305,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 28,
      "y": 0.398,
      "layer": 2,
      "closed_by": [
        27
      ]
    },
    {
      "id": 27,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        32,
        29
      ]
    },
    {
      "id": 26,
      "x": -1.498,
      "y": -0.1,
      "layer": 4,
      "closed_by": [
        44
      ]
    },
    {
      "id": 25,
      "x": -0.896,
      "y": 0.859,
      "layer": 5,
      "closed_by": [
        23
      ]
    },
    {
      "id": 23,
      "x": -0.46,
      "y": 0.785,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 45,
      "x": 1.5,
      "y": -0.1,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 21,
      "x": 1.179,
      "y": -0.1,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 22,
      "x": 0.893,
      "y": -0.1,
      "layer": 6,
      "closed_by": [
        21
      ]
    },
    {
      "id": 43,
      "x": -0.893,
      "y": -0.1,
      "layer": 6,
      "closed_by": [
        50
      ]
    },
    {
      "id": 40,
      "x": -1.745,
      "y": 0.537,
      "layer": 3,
      "closed_by": [
        36,
        26
      ]
    },
    {
      "id": 51,
      "x": 1.179,
      "y": -0.578,
      "layer": 5,
      "closed_by": [
        22
      ]
    },
    {
      "id": 44,
      "x": -1.179,
      "y": -0.578,
      "layer": 5,
      "closed_by": [
        43
      ]
    },
    {
      "id": 24,
      "x": 0.462,
      "y": 0.785,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 31,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 48.0,
    "star2": 40.1,
    "star3": 11.1
  }
}