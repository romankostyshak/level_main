{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.412,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 50,
      "x": 0.837,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 49,
      "x": -0.002,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        47,
        48
      ]
    },
    {
      "id": 47,
      "x": 0.412,
      "y": -0.5,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 46,
      "x": 1.184,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 45,
      "x": -1.167,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 43,
      "x": 1.588,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 42,
      "x": 0.412,
      "y": -0.017,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 41,
      "x": -1.575,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 40,
      "y": 0.859,
      "layer": 1,
      "closed_by": [
        51,
        38
      ]
    },
    {
      "id": 39,
      "y": 0.859,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -0.398,
      "y": 1.021,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 37,
      "x": -0.819,
      "y": 0.859,
      "layer": 1,
      "closed_by": [
        45,
        38
      ]
    },
    {
      "id": 36,
      "x": 0.837,
      "y": 0.859,
      "layer": 1,
      "closed_by": [
        51,
        46
      ]
    },
    {
      "id": 33,
      "x": -0.819,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 35,
      "x": -0.398,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        49,
        33
      ]
    },
    {
      "id": 44,
      "x": 0.412,
      "y": -0.497,
      "layer": 2,
      "closed_by": [
        50,
        49
      ]
    },
    {
      "id": 34,
      "x": -0.398,
      "y": -0.017,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 48,
      "x": -0.398,
      "y": -0.499,
      "layer": 4,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 19,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 18.4,
    "star2": 0.1,
    "star3": 80.6
  }
}