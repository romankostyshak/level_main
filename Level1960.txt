{
  "layout_cards": [
    {
      "id": 49,
      "x": 0.305,
      "y": -0.1,
      "layer": 2,
      "closed_by": [
        51
      ]
    },
    {
      "id": 47,
      "x": 0.1,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        46
      ]
    },
    {
      "id": 39,
      "x": 0.879,
      "y": -0.1,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 22,
      "x": -1.294,
      "y": -0.1,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 18,
      "x": -1.855,
      "y": -0.1,
      "layer": 3,
      "closed_by": [
        19
      ]
    },
    {
      "id": 17,
      "x": -1.855,
      "y": -0.499,
      "layer": 2,
      "closed_by": [
        18
      ]
    },
    {
      "id": 19,
      "x": -1.855,
      "y": 0.3,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 21,
      "x": -1.294,
      "y": -0.5,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 23,
      "x": -1.294,
      "y": 0.298,
      "layer": 5,
      "closed_by": [
        24
      ]
    },
    {
      "id": 20,
      "x": -1.855,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        24
      ]
    },
    {
      "id": 24,
      "x": -1.498,
      "y": 0.699,
      "layer": 6,
      "closed_by": [
        25
      ]
    },
    {
      "id": 25,
      "x": -1.1,
      "y": 0.699,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 41,
      "x": -0.699,
      "y": 0.216,
      "layer": 5,
      "closed_by": [
        26
      ]
    },
    {
      "id": 42,
      "x": -0.699,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 26,
      "x": -0.699,
      "y": 0.699,
      "layer": 6,
      "closed_by": [
        25
      ]
    },
    {
      "id": 43,
      "x": -0.458,
      "y": -0.179,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 45,
      "x": -0.216,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": -0.216,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 46,
      "x": -0.143,
      "y": 0.699,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 50,
      "x": 0.305,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        47
      ]
    },
    {
      "id": 51,
      "x": 0.305,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 48,
      "x": 0.305,
      "y": -0.5,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 36,
      "x": 0.879,
      "y": 1.019,
      "layer": 5,
      "closed_by": [
        35
      ]
    },
    {
      "id": 37,
      "x": 0.879,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 38,
      "x": 0.879,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 40,
      "x": 0.879,
      "y": -0.5,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 35,
      "x": 1.098,
      "y": 0.699,
      "layer": 6,
      "closed_by": [
        34
      ]
    },
    {
      "id": 34,
      "x": 1.427,
      "y": 0.699,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 2.013,
      "y": 0.699,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 2.013,
      "y": 0.059,
      "layer": 5,
      "closed_by": [
        27
      ]
    },
    {
      "id": 31,
      "x": 2.013,
      "y": -0.577,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 32,
      "x": 1.74,
      "y": -0.577,
      "layer": 2,
      "closed_by": [
        31,
        30
      ]
    },
    {
      "id": 30,
      "x": 1.455,
      "y": -0.577,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 33,
      "x": 1.455,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 29,
      "x": 1.82,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        28
      ]
    }
  ],
  "cards_in_layout_amount": 35,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 15.1,
    "star2": 73.9,
    "star3": 10.7
  }
}