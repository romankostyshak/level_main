{
  "layout_cards": [
    {
      "id": 8,
      "x": -0.652,
      "y": 0.384,
      "layer": 4,
      "closed_by": [
        26,
        28
      ]
    },
    {
      "id": 9,
      "x": -1.22,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        10,
        11
      ]
    },
    {
      "id": 10,
      "x": -1.582,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        13,
        35
      ]
    },
    {
      "id": 11,
      "x": -0.813,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        12,
        35
      ]
    },
    {
      "id": 12,
      "x": -0.652,
      "y": 0.703,
      "layer": 3,
      "closed_by": [
        8
      ]
    },
    {
      "id": 13,
      "x": -1.748,
      "y": 0.707,
      "layer": 3,
      "closed_by": [
        14
      ]
    },
    {
      "id": 14,
      "x": -1.746,
      "y": 0.381,
      "layer": 4,
      "closed_by": [
        30,
        36
      ]
    },
    {
      "id": 15,
      "x": -1.2,
      "y": 0.143,
      "layer": 1,
      "closed_by": [
        16
      ]
    },
    {
      "id": 16,
      "x": -1.2,
      "y": -0.089,
      "layer": 2,
      "closed_by": [
        17,
        18
      ]
    },
    {
      "id": 17,
      "x": -0.939,
      "y": -0.497,
      "layer": 3,
      "closed_by": [
        19,
        31
      ]
    },
    {
      "id": 18,
      "x": -1.483,
      "y": -0.493,
      "layer": 3,
      "closed_by": [
        20,
        31
      ]
    },
    {
      "id": 19,
      "x": -0.652,
      "y": -0.497,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 20,
      "x": -1.75,
      "y": -0.495,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 21,
      "x": -1.985,
      "y": -0.497,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -0.411,
      "y": -0.497,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 0.414,
      "y": -0.5,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 1.985,
      "y": -0.5,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 0.414,
      "y": 0.377,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -0.411,
      "y": 0.384,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 1.983,
      "y": 0.381,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 28,
      "x": -0.939,
      "y": 1.019,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 29,
      "x": 1.202,
      "y": 0.136,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -1.983,
      "y": 0.381,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -1.2,
      "y": -0.493,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 32,
      "x": 1.2,
      "y": -0.499,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 33,
      "x": -1.202,
      "y": 0.143,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 1.2,
      "y": 1.018,
      "layer": 3,
      "closed_by": [
        37,
        38
      ]
    },
    {
      "id": 35,
      "x": -1.202,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        28,
        36
      ]
    },
    {
      "id": 36,
      "x": -1.465,
      "y": 1.019,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 37,
      "x": 0.93,
      "y": 1.019,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 1.475,
      "y": 1.019,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 39,
      "x": 1.2,
      "y": 0.136,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 40,
      "x": 0.939,
      "y": -0.499,
      "layer": 3,
      "closed_by": [
        32,
        43
      ]
    },
    {
      "id": 41,
      "x": 1.457,
      "y": -0.497,
      "layer": 3,
      "closed_by": [
        32,
        51
      ]
    },
    {
      "id": 42,
      "x": 1.2,
      "y": -0.096,
      "layer": 2,
      "closed_by": [
        40,
        41
      ]
    },
    {
      "id": 43,
      "x": 0.652,
      "y": -0.497,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 44,
      "x": 0.652,
      "y": 0.379,
      "layer": 4,
      "closed_by": [
        25,
        37
      ]
    },
    {
      "id": 45,
      "x": 0.652,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 46,
      "x": 0.814,
      "y": 1.021,
      "layer": 2,
      "closed_by": [
        34,
        45
      ]
    },
    {
      "id": 47,
      "x": 1.59,
      "y": 1.018,
      "layer": 2,
      "closed_by": [
        34,
        49
      ]
    },
    {
      "id": 48,
      "x": 1.184,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        46,
        47
      ]
    },
    {
      "id": 49,
      "x": 1.746,
      "y": 0.697,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": 1.748,
      "y": 0.381,
      "layer": 4,
      "closed_by": [
        27,
        38
      ]
    },
    {
      "id": 51,
      "x": 1.745,
      "y": -0.5,
      "layer": 4,
      "closed_by": [
        24
      ]
    }
  ],
  "cards_in_layout_amount": 44,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 1.1,
    "star2": 36.7,
    "star3": 61.5
  }
}