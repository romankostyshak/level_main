{
  "layout_cards": [
    {
      "id": 51,
      "x": -1.2,
      "y": 0.86,
      "layer": 1,
      "closed_by": [
        47,
        46
      ]
    },
    {
      "id": 50,
      "x": -0.625,
      "y": 0.856,
      "layer": 1,
      "closed_by": [
        48,
        47
      ]
    },
    {
      "id": 49,
      "x": -1.773,
      "y": 0.856,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": -0.465,
      "y": 0.81,
      "angle": 25.0,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 47,
      "x": -1.042,
      "y": 0.813,
      "angle": 25.0,
      "layer": 2,
      "closed_by": [
        44,
        43
      ]
    },
    {
      "id": 46,
      "x": -1.615,
      "y": 0.813,
      "angle": 25.0,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 45,
      "x": -0.333,
      "y": 0.243,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 44,
      "x": -1.488,
      "y": 0.254,
      "layer": 3,
      "closed_by": [
        32,
        31
      ]
    },
    {
      "id": 43,
      "x": -0.916,
      "y": 0.245,
      "layer": 3,
      "closed_by": [
        36,
        32
      ]
    },
    {
      "id": 42,
      "x": 0.615,
      "y": 0.861,
      "layer": 1,
      "closed_by": [
        39,
        38
      ]
    },
    {
      "id": 41,
      "x": 1.19,
      "y": 0.856,
      "layer": 1,
      "closed_by": [
        38,
        37
      ]
    },
    {
      "id": 40,
      "x": 1.766,
      "y": 0.86,
      "layer": 1,
      "closed_by": [
        37
      ]
    },
    {
      "id": 39,
      "x": 0.458,
      "y": 0.814,
      "angle": 335.0,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 38,
      "x": 1.031,
      "y": 0.81,
      "angle": 335.0,
      "layer": 2,
      "closed_by": [
        27
      ]
    },
    {
      "id": 37,
      "x": 1.608,
      "y": 0.814,
      "angle": 335.0,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 36,
      "x": -0.625,
      "y": 0.057,
      "layer": 4,
      "closed_by": [
        35,
        34
      ]
    },
    {
      "id": 35,
      "x": -1.042,
      "y": -0.321,
      "angle": 335.0,
      "layer": 5,
      "closed_by": [
        21,
        20
      ]
    },
    {
      "id": 34,
      "x": -0.456,
      "y": -0.321,
      "angle": 335.0,
      "layer": 5,
      "closed_by": [
        21
      ]
    },
    {
      "id": 33,
      "x": -1.623,
      "y": -0.319,
      "angle": 335.0,
      "layer": 5,
      "closed_by": [
        20,
        19
      ]
    },
    {
      "id": 32,
      "x": -1.199,
      "y": 0.057,
      "layer": 4,
      "closed_by": [
        35,
        33
      ]
    },
    {
      "id": 31,
      "x": -1.773,
      "y": 0.057,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 30,
      "x": 1.485,
      "y": 0.256,
      "layer": 3,
      "closed_by": [
        23,
        22
      ]
    },
    {
      "id": 29,
      "x": 0.333,
      "y": 0.246,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 28,
      "x": 0.458,
      "y": -0.319,
      "angle": 25.0,
      "layer": 5,
      "closed_by": [
        18
      ]
    },
    {
      "id": 27,
      "x": 0.907,
      "y": 0.25,
      "layer": 3,
      "closed_by": [
        24,
        23
      ]
    },
    {
      "id": 26,
      "x": 1.036,
      "y": -0.319,
      "angle": 25.0,
      "layer": 5,
      "closed_by": [
        18,
        17
      ]
    },
    {
      "id": 25,
      "x": 1.61,
      "y": -0.317,
      "angle": 25.0,
      "layer": 5,
      "closed_by": [
        17,
        16
      ]
    },
    {
      "id": 24,
      "x": 0.619,
      "y": 0.061,
      "layer": 4,
      "closed_by": [
        28,
        26
      ]
    },
    {
      "id": 23,
      "x": 1.19,
      "y": 0.064,
      "layer": 4,
      "closed_by": [
        26,
        25
      ]
    },
    {
      "id": 22,
      "x": 1.771,
      "y": 0.057,
      "layer": 4,
      "closed_by": [
        25
      ]
    },
    {
      "id": 21,
      "x": -0.629,
      "y": -0.37,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 20,
      "x": -1.202,
      "y": -0.37,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 19,
      "x": -1.774,
      "y": -0.368,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 18,
      "x": 0.624,
      "y": -0.37,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 17,
      "x": 1.197,
      "y": -0.372,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 16,
      "x": 1.769,
      "y": -0.37,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 36,
  "cards_in_deck_amount": 32,
  "stars_statistic": {
    "star1": 35.3,
    "star2": 59.5,
    "star3": 4.9
  }
}