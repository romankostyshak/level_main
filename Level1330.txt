{
  "layout_cards": [
    {
      "id": 51,
      "x": -1.529,
      "y": 0.897,
      "angle": 30.0,
      "layer": 5,
      "closed_by": [
        40
      ]
    },
    {
      "id": 50,
      "x": -1.324,
      "y": 0.537,
      "angle": 30.0,
      "layer": 4,
      "closed_by": [
        51,
        28
      ]
    },
    {
      "id": 49,
      "x": -1.128,
      "y": 0.2,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        50,
        30
      ]
    },
    {
      "id": 48,
      "x": -0.919,
      "y": -0.158,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        49,
        32
      ]
    },
    {
      "id": 47,
      "x": -0.712,
      "y": -0.518,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 46,
      "x": 0.722,
      "y": -0.518,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 45,
      "x": 0.92,
      "y": -0.179,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        44,
        34
      ]
    },
    {
      "id": 44,
      "x": 1.126,
      "y": 0.18,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        43,
        31
      ]
    },
    {
      "id": 43,
      "x": 1.327,
      "y": 0.527,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [
        42,
        26
      ]
    },
    {
      "id": 42,
      "x": 1.536,
      "y": 0.888,
      "angle": 330.0,
      "layer": 5,
      "closed_by": [
        41
      ]
    },
    {
      "id": 41,
      "x": 1.281,
      "y": 0.986,
      "angle": 349.997,
      "layer": 6,
      "closed_by": [
        37
      ]
    },
    {
      "id": 34,
      "x": 0.518,
      "y": -0.574,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 33,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 32,
      "x": -0.518,
      "y": -0.574,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 31,
      "x": 0.574,
      "y": -0.172,
      "layer": 4,
      "closed_by": [
        29,
        26
      ]
    },
    {
      "id": 30,
      "x": -0.572,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        28,
        27
      ]
    },
    {
      "id": 29,
      "x": 0.27,
      "y": 0.136,
      "layer": 5,
      "closed_by": [
        20
      ]
    },
    {
      "id": 28,
      "x": -0.81,
      "y": 0.143,
      "layer": 5,
      "closed_by": [
        21
      ]
    },
    {
      "id": 27,
      "x": -0.268,
      "y": 0.136,
      "layer": 5,
      "closed_by": [
        21
      ]
    },
    {
      "id": 26,
      "x": 0.819,
      "y": 0.136,
      "layer": 5,
      "closed_by": [
        20
      ]
    },
    {
      "id": 24,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        29,
        27,
        39
      ]
    },
    {
      "id": 23,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 22,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        23
      ]
    },
    {
      "id": 21,
      "x": -0.527,
      "y": 0.142,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 20,
      "x": 0.537,
      "y": 0.136,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 40,
      "x": -1.258,
      "y": 0.995,
      "angle": 10.0,
      "layer": 6,
      "closed_by": [
        36
      ]
    },
    {
      "id": 39,
      "y": 1.019,
      "layer": 5,
      "closed_by": [
        38,
        25
      ]
    },
    {
      "id": 36,
      "x": -0.972,
      "y": 1.021,
      "layer": 7,
      "closed_by": [
        14
      ]
    },
    {
      "id": 38,
      "x": -0.305,
      "y": 1.019,
      "layer": 6,
      "closed_by": [
        35
      ]
    },
    {
      "id": 25,
      "x": 0.307,
      "y": 1.019,
      "layer": 6,
      "closed_by": [
        19
      ]
    },
    {
      "id": 37,
      "x": 0.972,
      "y": 1.019,
      "layer": 7,
      "closed_by": [
        15
      ]
    },
    {
      "id": 19,
      "x": 0.386,
      "y": 1.019,
      "layer": 7,
      "closed_by": [
        15
      ]
    },
    {
      "id": 35,
      "x": -0.384,
      "y": 1.019,
      "layer": 7,
      "closed_by": [
        14
      ]
    },
    {
      "id": 15,
      "x": 0.652,
      "y": 1.019,
      "layer": 8,
      "closed_by": []
    },
    {
      "id": 14,
      "x": -0.638,
      "y": 1.019,
      "layer": 8,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 35,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 31.2,
    "star2": 64.3,
    "star3": 3.8
  }
}