{
  "layout_cards": [
    {
      "id": 22,
      "x": 1.824,
      "y": -0.414,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 1.213,
      "y": -0.416,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 1.825,
      "y": 0.935,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 1.215,
      "y": 0.939,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 26,
      "x": 2.786,
      "y": 0.216,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 0.331,
      "y": 0.216,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 2.627,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        26
      ]
    },
    {
      "id": 29,
      "x": 2.065,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 30,
      "x": 2.065,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        24
      ]
    },
    {
      "id": 31,
      "x": 1.506,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        22,
        23
      ]
    },
    {
      "id": 32,
      "x": 1.503,
      "y": 0.703,
      "layer": 1,
      "closed_by": [
        24,
        25
      ]
    },
    {
      "id": 33,
      "x": 0.948,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        23
      ]
    },
    {
      "id": 34,
      "x": 0.386,
      "y": -0.165,
      "layer": 1,
      "closed_by": [
        27
      ]
    },
    {
      "id": 35,
      "x": 0.944,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        25
      ]
    },
    {
      "id": 38,
      "x": 0.388,
      "y": 0.703,
      "layer": 1,
      "closed_by": [
        27
      ]
    },
    {
      "id": 39,
      "x": -0.73,
      "y": -0.337,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 40,
      "x": -0.944,
      "y": -0.256,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 41,
      "x": -1.529,
      "y": -0.34,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -2.332,
      "y": -0.342,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 43,
      "x": -1.746,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        41
      ]
    },
    {
      "id": 44,
      "x": -2.546,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 45,
      "x": -0.944,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 46,
      "x": -1.529,
      "y": 0.86,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 47,
      "x": -1.746,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": -2.332,
      "y": 0.859,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 49,
      "x": -2.546,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 50,
      "x": -0.731,
      "y": 0.861,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 51,
      "x": 2.625,
      "y": -0.178,
      "layer": 1,
      "closed_by": [
        26
      ]
    }
  ],
  "level_mechanic": 1,
  "free_cards_from_stickers": [
    22,
    23,
    24,
    25,
    26,
    27,
    28,
    29,
    30,
    31,
    32,
    33,
    34,
    35,
    38,
    51
  ],
  "cards_in_layout_amount": 28,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 10.9,
    "star2": 83.3,
    "star3": 4.8
  }
}