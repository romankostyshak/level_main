{
  "layout_cards": [
    {
      "id": 21,
      "x": 0.865,
      "y": 0.221,
      "layer": 1,
      "closed_by": [
        31,
        33
      ]
    },
    {
      "id": 22,
      "x": -0.865,
      "y": 1.039,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 23,
      "x": -0.333,
      "y": 1.036,
      "layer": 1,
      "closed_by": [
        32,
        48
      ]
    },
    {
      "id": 24,
      "x": -0.865,
      "y": -0.596,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 25,
      "x": 0.333,
      "y": 1.036,
      "layer": 1,
      "closed_by": [
        31,
        48
      ]
    },
    {
      "id": 26,
      "x": 0.869,
      "y": 1.036,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 27,
      "x": 0.865,
      "y": -0.595,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 28,
      "x": -0.865,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        32,
        36
      ]
    },
    {
      "id": 29,
      "x": -0.335,
      "y": -0.592,
      "layer": 1,
      "closed_by": [
        36,
        49
      ]
    },
    {
      "id": 30,
      "x": 0.333,
      "y": -0.591,
      "layer": 1,
      "closed_by": [
        33,
        49
      ]
    },
    {
      "id": 31,
      "x": 0.574,
      "y": 0.637,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 32,
      "x": -0.574,
      "y": 0.634,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 33,
      "x": 0.574,
      "y": -0.187,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 34,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        51
      ]
    },
    {
      "id": 35,
      "x": -1.268,
      "y": -0.597,
      "layer": 4,
      "closed_by": [
        47
      ]
    },
    {
      "id": 36,
      "x": -0.572,
      "y": -0.187,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 37,
      "x": 0.949,
      "y": -0.187,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 38,
      "x": 0.948,
      "y": 0.638,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 39,
      "x": -0.949,
      "y": 0.634,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 40,
      "x": -0.949,
      "y": -0.187,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 41,
      "x": 1.264,
      "y": 1.036,
      "layer": 4,
      "closed_by": [
        44
      ]
    },
    {
      "id": 42,
      "x": 1.266,
      "y": -0.597,
      "layer": 4,
      "closed_by": [
        45
      ]
    },
    {
      "id": 43,
      "x": -1.266,
      "y": 1.039,
      "layer": 4,
      "closed_by": [
        46
      ]
    },
    {
      "id": 44,
      "x": 1.529,
      "y": 0.861,
      "angle": 10.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 45,
      "x": 1.529,
      "y": -0.418,
      "angle": 350.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 46,
      "x": -1.531,
      "y": 0.861,
      "angle": 350.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 47,
      "x": -1.531,
      "y": -0.418,
      "angle": 10.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 48,
      "x": 0.001,
      "y": 1.036,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 49,
      "y": -0.587,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 50,
      "y": 0.782,
      "layer": 3,
      "closed_by": [
        51
      ]
    },
    {
      "id": 51,
      "x": 0.001,
      "y": 0.216,
      "layer": 4,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 31,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 36.6,
    "star2": 54.5,
    "star3": 7.9
  }
}