{
  "layout_cards": [
    {
      "id": 51,
      "x": 1.478,
      "y": 0.943,
      "angle": 300.0,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 50,
      "x": 0.263,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 49,
      "x": 0.003,
      "y": -0.261,
      "layer": 2,
      "closed_by": [
        33,
        32
      ]
    },
    {
      "id": 48,
      "x": 1.031,
      "y": 0.379,
      "angle": 30.0,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 47,
      "x": -1.386,
      "y": 0.282,
      "angle": 60.0,
      "layer": 1,
      "closed_by": [
        46,
        37
      ]
    },
    {
      "id": 46,
      "x": -1.735,
      "y": 0.483,
      "angle": 60.0,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 45,
      "x": -1.118,
      "y": 0.74,
      "angle": 60.0,
      "layer": 1,
      "closed_by": [
        39,
        37
      ]
    },
    {
      "id": 44,
      "x": 1.743,
      "y": 0.483,
      "angle": 300.0,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 43,
      "x": 0.263,
      "y": 0.745,
      "layer": 1,
      "closed_by": [
        34
      ]
    },
    {
      "id": 42,
      "x": -0.266,
      "y": 0.745,
      "layer": 1,
      "closed_by": [
        34
      ]
    },
    {
      "id": 41,
      "x": -0.266,
      "y": -0.261,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 40,
      "x": 1.123,
      "y": 0.736,
      "angle": 300.0,
      "layer": 1,
      "closed_by": [
        51,
        48
      ]
    },
    {
      "id": 39,
      "x": -1.47,
      "y": 0.944,
      "angle": 60.0,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 38,
      "x": 1.394,
      "y": 0.282,
      "angle": 300.0,
      "layer": 1,
      "closed_by": [
        48,
        44
      ]
    },
    {
      "id": 37,
      "x": -1.031,
      "y": 0.367,
      "angle": 330.0,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -1.728,
      "y": 0.786,
      "angle": 330.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 1.738,
      "y": 0.791,
      "angle": 30.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 0.002,
      "y": 0.748,
      "layer": 2,
      "closed_by": [
        33,
        32
      ]
    },
    {
      "id": 33,
      "x": -0.266,
      "y": 0.252,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 32,
      "x": 0.263,
      "y": 0.25,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 31,
      "x": 0.002,
      "y": 0.252,
      "layer": 4,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 21,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 33.2,
    "star2": 1.3,
    "star3": 64.8
  }
}