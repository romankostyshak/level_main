{
  "layout_cards": [
    {
      "id": 47,
      "x": -0.651,
      "y": 1.024,
      "layer": 1,
      "closed_by": [
        50,
        34
      ]
    },
    {
      "id": 49,
      "x": -0.652,
      "y": -0.421,
      "layer": 1,
      "closed_by": [
        43,
        34
      ]
    },
    {
      "id": 46,
      "x": 0.652,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        44,
        35
      ]
    },
    {
      "id": 45,
      "x": 0.948,
      "y": 1.026,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 50,
      "x": -0.944,
      "y": 1.024,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 44,
      "x": 0.947,
      "y": -0.421,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 43,
      "x": -0.947,
      "y": -0.421,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 41,
      "x": 0.813,
      "y": -0.18,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 40,
      "x": -0.81,
      "y": 0.786,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 39,
      "x": -0.81,
      "y": -0.18,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 48,
      "x": 0.652,
      "y": 1.026,
      "layer": 1,
      "closed_by": [
        45,
        35
      ]
    },
    {
      "id": 34,
      "x": -0.266,
      "y": 0.3,
      "layer": 2,
      "closed_by": [
        36,
        26
      ]
    },
    {
      "id": 38,
      "y": 0.303,
      "layer": 1,
      "closed_by": [
        34,
        35
      ]
    },
    {
      "id": 35,
      "x": 0.275,
      "y": 0.3,
      "layer": 2,
      "closed_by": [
        27,
        25
      ]
    },
    {
      "id": 42,
      "x": 0.814,
      "y": 0.782,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 33,
      "x": 0.972,
      "y": 0.3,
      "layer": 4,
      "closed_by": [
        31,
        30
      ]
    },
    {
      "id": 32,
      "x": -0.971,
      "y": 0.303,
      "layer": 4,
      "closed_by": [
        29,
        28
      ]
    },
    {
      "id": 31,
      "x": 1.026,
      "y": -0.18,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 1.026,
      "y": 0.782,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -1.026,
      "y": 0.787,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 28,
      "x": -1.024,
      "y": -0.18,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -0.268,
      "y": -0.172,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 27,
      "x": 0.275,
      "y": -0.177,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 26,
      "x": -0.263,
      "y": 0.781,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 25,
      "x": 0.275,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 24,
      "x": 0.002,
      "y": -0.412,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 23,
      "y": 1.018,
      "layer": 4,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 27,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 67.5,
    "star2": 13.0,
    "star3": 18.9
  }
}