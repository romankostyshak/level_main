{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.298,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 50,
      "x": -0.632,
      "y": 0.699,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 46,
      "x": 1.419,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        49
      ]
    },
    {
      "id": 44,
      "x": -1.919,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 40,
      "x": -1.439,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 39,
      "x": 0.623,
      "y": 0.699,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 0.623,
      "y": 0.859,
      "layer": 5,
      "closed_by": [
        39
      ]
    },
    {
      "id": 37,
      "x": 0.319,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 36,
      "x": -0.632,
      "y": 0.859,
      "layer": 5,
      "closed_by": [
        50
      ]
    },
    {
      "id": 35,
      "x": -0.625,
      "y": -0.238,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -0.625,
      "y": -0.414,
      "layer": 5,
      "closed_by": [
        35
      ]
    },
    {
      "id": 33,
      "x": -0.298,
      "y": -0.414,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 32,
      "x": 0.298,
      "y": -0.414,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 31,
      "x": 0.623,
      "y": -0.414,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 30,
      "x": 0.623,
      "y": -0.238,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 29,
      "x": 0.298,
      "y": -0.238,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 28,
      "x": -0.298,
      "y": -0.238,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 27,
      "x": -0.298,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 26,
      "x": 0.298,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 25,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        51,
        37
      ]
    },
    {
      "id": 24,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        29,
        28,
        27,
        26
      ]
    },
    {
      "id": 23,
      "x": -0.001,
      "y": -0.34,
      "layer": 3,
      "closed_by": [
        33,
        32
      ]
    },
    {
      "id": 20,
      "x": 1.215,
      "y": -0.1,
      "layer": 5,
      "closed_by": [
        18
      ]
    },
    {
      "id": 19,
      "x": -1.215,
      "y": -0.1,
      "layer": 5,
      "closed_by": [
        17
      ]
    },
    {
      "id": 49,
      "x": 1.659,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        47,
        22
      ]
    },
    {
      "id": 47,
      "x": 1.899,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        48
      ]
    },
    {
      "id": 48,
      "x": 2.144,
      "y": 0.699,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 45,
      "x": 1.215,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 22,
      "x": 1.215,
      "y": 0.298,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 18,
      "x": 1.215,
      "y": -0.418,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 17,
      "x": -1.215,
      "y": -0.418,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 21,
      "x": -1.215,
      "y": 0.298,
      "layer": 4,
      "closed_by": [
        19
      ]
    },
    {
      "id": 41,
      "x": -1.215,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 42,
      "x": -1.659,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        44,
        21
      ]
    },
    {
      "id": 43,
      "x": -2.144,
      "y": 0.699,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 35,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 15.6,
    "star2": 73.4,
    "star3": 10.8
  }
}