{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.246,
      "y": -0.224,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 50,
      "x": 0.333,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        49,
        45
      ]
    },
    {
      "id": 49,
      "x": 0.578,
      "y": -0.31,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 48,
      "x": 0.259,
      "y": 0.569,
      "layer": 3,
      "closed_by": [
        35,
        34
      ]
    },
    {
      "id": 47,
      "x": 0.818,
      "y": 0.104,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 46,
      "x": -0.813,
      "y": 0.224,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 45,
      "x": 0.001,
      "y": 0.171,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 44,
      "x": -1.615,
      "y": 0.216,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 43,
      "x": 1.608,
      "y": 0.096,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -1.536,
      "y": 0.324,
      "layer": 6,
      "closed_by": [
        44
      ]
    },
    {
      "id": 41,
      "x": 1.529,
      "y": 0.017,
      "layer": 6,
      "closed_by": [
        43
      ]
    },
    {
      "id": 40,
      "x": 1.455,
      "y": -0.07,
      "layer": 5,
      "closed_by": [
        41
      ]
    },
    {
      "id": 39,
      "x": -1.45,
      "y": 0.409,
      "layer": 5,
      "closed_by": [
        42
      ]
    },
    {
      "id": 38,
      "x": -1.373,
      "y": 0.49,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 37,
      "x": 1.376,
      "y": -0.15,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 36,
      "x": -0.319,
      "y": 1.024,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 35,
      "y": 0.967,
      "layer": 4,
      "closed_by": [
        32
      ]
    },
    {
      "id": 34,
      "x": 0.559,
      "y": 0.488,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 33,
      "x": -0.578,
      "y": 0.648,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 32,
      "x": -0.259,
      "y": 0.569,
      "layer": 5,
      "closed_by": [
        29
      ]
    },
    {
      "id": 31,
      "x": 0.298,
      "y": 0.068,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 30,
      "x": 0.039,
      "y": -0.523,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -0.518,
      "y": 0.165,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 23,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 53.8,
    "star2": 4.3,
    "star3": 41.1
  }
}