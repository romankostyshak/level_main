{
  "layout_cards": [
    {
      "id": 20,
      "x": -1.878,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        34,
        48
      ]
    },
    {
      "id": 21,
      "x": 1.84,
      "y": -0.279,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 22,
      "x": 1.039,
      "y": 0.398,
      "layer": 1,
      "closed_by": [
        26,
        27
      ]
    },
    {
      "id": 23,
      "x": 1.48,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        21,
        26
      ]
    },
    {
      "id": 24,
      "x": 1.899,
      "y": 0.398,
      "layer": 1,
      "closed_by": [
        21,
        25
      ]
    },
    {
      "id": 25,
      "x": 1.878,
      "y": 0.98,
      "angle": 5.0,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 26,
      "x": 1.098,
      "y": -0.279,
      "angle": 9.998,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 27,
      "x": 1.059,
      "y": 0.98,
      "angle": 354.997,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 28,
      "x": 1.46,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        29,
        30
      ]
    },
    {
      "id": 29,
      "x": 1.779,
      "y": 0.34,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 1.179,
      "y": 0.34,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 1.48,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        29,
        30
      ]
    },
    {
      "id": 32,
      "x": 2.5,
      "y": -0.1,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 33,
      "x": 2.5,
      "y": 0.859,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 34,
      "x": -1.58,
      "y": 0.859,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 2.657,
      "y": 0.379,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 0.46,
      "y": -0.1,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 37,
      "x": 0.46,
      "y": 0.859,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 38,
      "x": 0.319,
      "y": 0.379,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -1.58,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        20,
        42,
        43,
        44
      ],
      "card_type": 7
    },
    {
      "id": 40,
      "x": -2.18,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        20,
        42,
        45,
        46
      ]
    },
    {
      "id": 41,
      "x": -0.939,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        43,
        44
      ]
    },
    {
      "id": 42,
      "x": -1.878,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        47,
        49
      ]
    },
    {
      "id": 43,
      "x": -1.299,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        49
      ]
    },
    {
      "id": 44,
      "x": -1.299,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 45,
      "x": -2.459,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 46,
      "x": -2.459,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 47,
      "x": -2.18,
      "y": -0.259,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 48,
      "x": -2.18,
      "y": 0.859,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 49,
      "x": -1.58,
      "y": -0.259,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -2.819,
      "y": 0.379,
      "layer": 1,
      "closed_by": [
        45,
        46
      ]
    }
  ],
  "level_mechanic": 2,
  "free_cards_from_stickers": [
    20,
    34,
    39,
    40,
    41,
    42,
    43,
    44,
    45,
    46,
    47,
    48,
    49,
    51
  ],
  "cards_in_layout_amount": 31,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 64.4,
    "star2": 29.8,
    "star3": 5.4
  }
}