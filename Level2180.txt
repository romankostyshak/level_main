{
  "layout_cards": [
    {
      "id": 49,
      "x": 0.787,
      "y": -0.499,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": -0.786,
      "y": -0.499,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "x": -0.546,
      "y": -0.256,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 46,
      "x": 0.546,
      "y": -0.261,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 43,
      "x": 0.305,
      "y": 0.059,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 41,
      "x": 0.305,
      "y": 0.136,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 50,
      "x": 0.305,
      "y": -0.017,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 42,
      "x": -0.305,
      "y": 0.136,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 44,
      "x": -0.305,
      "y": 0.059,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 45,
      "x": -0.305,
      "y": -0.017,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 40,
      "x": -0.305,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 39,
      "x": 0.307,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 38,
      "x": 0.628,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 35,
      "x": -0.625,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 34,
      "x": 0.865,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 33,
      "x": -0.865,
      "y": 0.781,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 32,
      "x": 1.105,
      "y": 0.544,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 31,
      "x": -1.105,
      "y": 0.54,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 30,
      "x": 1.105,
      "y": 0.46,
      "layer": 5,
      "closed_by": [
        28
      ]
    },
    {
      "id": 29,
      "x": -1.105,
      "y": 0.462,
      "layer": 5,
      "closed_by": [
        36
      ]
    },
    {
      "id": 28,
      "x": 1.105,
      "y": 0.381,
      "layer": 6,
      "closed_by": [
        14
      ]
    },
    {
      "id": 36,
      "x": -1.105,
      "y": 0.384,
      "layer": 6,
      "closed_by": [
        13
      ]
    },
    {
      "id": 27,
      "x": 1.348,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        25
      ]
    },
    {
      "id": 26,
      "x": -1.347,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        24
      ]
    },
    {
      "id": 25,
      "x": 1.61,
      "y": -0.414,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 24,
      "x": -1.61,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 37,
      "x": -1.824,
      "y": -0.017,
      "layer": 4,
      "closed_by": [
        19
      ]
    },
    {
      "id": 20,
      "x": 1.827,
      "y": 0.059,
      "layer": 5,
      "closed_by": [
        18
      ]
    },
    {
      "id": 19,
      "x": -1.824,
      "y": 0.059,
      "layer": 5,
      "closed_by": [
        15
      ]
    },
    {
      "id": 22,
      "x": -1.824,
      "y": -0.1,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 23,
      "x": 1.827,
      "y": -0.1,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 21,
      "x": 1.83,
      "y": -0.017,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 18,
      "x": 1.827,
      "y": 0.136,
      "layer": 6,
      "closed_by": [
        14
      ]
    },
    {
      "id": 15,
      "x": -1.825,
      "y": 0.136,
      "layer": 6,
      "closed_by": [
        13
      ]
    },
    {
      "id": 14,
      "x": 1.503,
      "y": 0.3,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 13,
      "x": -1.506,
      "y": 0.298,
      "layer": 7,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 36,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 15.2,
    "star2": 70.9,
    "star3": 13.6
  }
}