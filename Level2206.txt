{
  "layout_cards": [
    {
      "id": 19,
      "x": 0.865,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        26,
        43
      ]
    },
    {
      "id": 20,
      "x": -0.409,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 21,
      "x": 0.414,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        23
      ]
    },
    {
      "id": 22,
      "x": -0.625,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 23,
      "x": 0.638,
      "y": 0.938,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 24,
      "x": -0.865,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        25,
        44
      ]
    },
    {
      "id": 25,
      "x": -0.625,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 26,
      "x": 0.638,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 27,
      "x": 0.412,
      "y": 0.859,
      "layer": 5,
      "closed_by": [
        29
      ]
    },
    {
      "id": 28,
      "x": -0.409,
      "y": 0.86,
      "layer": 5,
      "closed_by": [
        29
      ]
    },
    {
      "id": 29,
      "y": 0.86,
      "layer": 6,
      "closed_by": [
        30
      ]
    },
    {
      "id": 30,
      "y": 0.305,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 31,
      "y": -0.254,
      "layer": 6,
      "closed_by": [
        30
      ]
    },
    {
      "id": 32,
      "x": 1.427,
      "y": 1.021,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 0.412,
      "y": -0.416,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 34,
      "x": 0.412,
      "y": -0.256,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 35,
      "x": -0.409,
      "y": -0.256,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 36,
      "x": 0.62,
      "y": -0.259,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 37,
      "x": -0.62,
      "y": -0.259,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 38,
      "x": 0.865,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        36,
        43
      ]
    },
    {
      "id": 39,
      "x": -0.865,
      "y": -0.261,
      "layer": 3,
      "closed_by": [
        37,
        44
      ]
    },
    {
      "id": 40,
      "x": 0.62,
      "y": -0.34,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 41,
      "x": -0.62,
      "y": -0.34,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 42,
      "x": -0.412,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        41
      ]
    },
    {
      "id": 43,
      "x": 1.264,
      "y": 0.3,
      "layer": 4,
      "closed_by": [
        45,
        46
      ]
    },
    {
      "id": 44,
      "x": -1.264,
      "y": 0.3,
      "layer": 4,
      "closed_by": [
        47,
        48
      ]
    },
    {
      "id": 45,
      "x": 1.268,
      "y": 0.859,
      "layer": 5,
      "closed_by": [
        32
      ]
    },
    {
      "id": 46,
      "x": 1.266,
      "y": -0.256,
      "layer": 5,
      "closed_by": [
        49
      ]
    },
    {
      "id": 47,
      "x": -1.264,
      "y": 0.859,
      "layer": 5,
      "closed_by": [
        50
      ]
    },
    {
      "id": 48,
      "x": -1.264,
      "y": -0.256,
      "layer": 5,
      "closed_by": [
        51
      ]
    },
    {
      "id": 49,
      "x": 1.427,
      "y": -0.414,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 50,
      "x": -1.424,
      "y": 1.019,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -1.424,
      "y": -0.416,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 33,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 47.8,
    "star2": 49.6,
    "star3": 2.1
  }
}