{
  "layout_cards": [
    {
      "id": 14,
      "x": 0.787,
      "y": 0.307,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 15,
      "x": -1.534,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        21
      ]
    },
    {
      "id": 16,
      "x": 1.531,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        20
      ]
    },
    {
      "id": 17,
      "x": -0.652,
      "y": 0.458,
      "layer": 1,
      "closed_by": [
        19
      ]
    },
    {
      "id": 18,
      "x": 0.648,
      "y": 0.462,
      "layer": 1,
      "closed_by": [
        14
      ]
    },
    {
      "id": 19,
      "x": -0.787,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 20,
      "x": 1.659,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        24,
        43
      ]
    },
    {
      "id": 21,
      "x": -1.659,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        25,
        27
      ]
    },
    {
      "id": 22,
      "x": 0.574,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 23,
      "x": -0.574,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 24,
      "x": 1.375,
      "y": 0.537,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 25,
      "x": -1.373,
      "y": 0.537,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 26,
      "x": -0.001,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 27,
      "x": -1.899,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 28,
      "x": 0.006,
      "y": 0.861,
      "layer": 5,
      "closed_by": [
        47,
        51
      ]
    },
    {
      "id": 29,
      "x": 0.298,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        26,
        31
      ]
    },
    {
      "id": 30,
      "x": -0.298,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        26,
        32
      ]
    },
    {
      "id": 31,
      "x": 0.537,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 32,
      "x": -0.537,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 33,
      "x": 0.777,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 34,
      "x": -0.777,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 35,
      "x": 0.001,
      "y": 0.061,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 36,
      "x": 0.004,
      "y": 0.46,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 37,
      "x": 1.019,
      "y": -0.578,
      "layer": 5,
      "closed_by": [
        39
      ]
    },
    {
      "id": 38,
      "x": -1.019,
      "y": -0.578,
      "layer": 5,
      "closed_by": [
        40
      ]
    },
    {
      "id": 39,
      "x": 1.266,
      "y": -0.578,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 40,
      "x": -1.266,
      "y": -0.578,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 1.375,
      "y": 0.379,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -1.373,
      "y": 0.379,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 43,
      "x": 1.899,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": 2.065,
      "y": 0.859,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 45,
      "x": -2.065,
      "y": 0.859,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 46,
      "x": 0.574,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        49
      ]
    },
    {
      "id": 47,
      "x": -0.331,
      "y": 1.023,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 48,
      "x": -0.572,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        50
      ]
    },
    {
      "id": 49,
      "x": 0.573,
      "y": 0.86,
      "layer": 5,
      "closed_by": [
        51
      ]
    },
    {
      "id": 50,
      "x": -0.573,
      "y": 0.859,
      "layer": 5,
      "closed_by": [
        47
      ]
    },
    {
      "id": 51,
      "x": 0.333,
      "y": 1.019,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 38,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 7.8,
    "star2": 63.7,
    "star3": 27.8
  }
}