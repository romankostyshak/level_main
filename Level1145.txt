{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.298,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "y": -0.179,
      "layer": 3,
      "closed_by": [
        44,
        36
      ]
    },
    {
      "id": 49,
      "x": 0.731,
      "y": 0.703,
      "layer": 6,
      "closed_by": [
        47
      ]
    },
    {
      "id": 48,
      "x": -0.731,
      "y": 0.702,
      "layer": 6,
      "closed_by": [
        46
      ]
    },
    {
      "id": 47,
      "x": 0.888,
      "y": 1.029,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 46,
      "x": -0.893,
      "y": 1.021,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 44,
      "x": -0.416,
      "y": 0.059,
      "layer": 4,
      "closed_by": [
        43,
        33
      ]
    },
    {
      "id": 43,
      "x": 0.001,
      "y": 0.141,
      "layer": 5,
      "closed_by": [
        42
      ]
    },
    {
      "id": 42,
      "y": 0.46,
      "layer": 6,
      "closed_by": [
        41
      ]
    },
    {
      "id": 41,
      "x": -0.001,
      "y": 0.777,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 0.577,
      "y": 0.379,
      "layer": 5,
      "closed_by": [
        49
      ]
    },
    {
      "id": 39,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        51,
        38
      ]
    },
    {
      "id": 38,
      "x": 0.303,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 37,
      "x": 0.949,
      "y": 0.059,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 36,
      "x": 0.418,
      "y": 0.059,
      "layer": 4,
      "closed_by": [
        43,
        40
      ]
    },
    {
      "id": 35,
      "x": 1.187,
      "y": -0.093,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 33,
      "x": -0.574,
      "y": 0.388,
      "layer": 5,
      "closed_by": [
        48
      ]
    },
    {
      "id": 31,
      "x": 0.939,
      "y": -0.256,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 30,
      "x": 1.182,
      "y": -0.416,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 45,
      "x": -0.939,
      "y": 0.059,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 34,
      "x": -1.187,
      "y": -0.1,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 29,
      "x": -1.184,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 32,
      "x": -0.944,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        34
      ]
    }
  ],
  "cards_in_layout_amount": 23,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 44.1,
    "star2": 1.9,
    "star3": 53.6
  }
}