{
  "layout_cards": [
    {
      "id": 17,
      "x": 1.667,
      "y": -0.256,
      "layer": 1,
      "closed_by": [
        22,
        45
      ]
    },
    {
      "id": 18,
      "x": -1.666,
      "y": -0.256,
      "layer": 1,
      "closed_by": [
        19,
        46
      ]
    },
    {
      "id": 19,
      "x": -1.488,
      "y": -0.209,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        20,
        49
      ]
    },
    {
      "id": 20,
      "x": -1.427,
      "y": 0.143,
      "layer": 3,
      "closed_by": [
        23,
        47
      ]
    },
    {
      "id": 21,
      "x": 1.429,
      "y": 0.14,
      "layer": 3,
      "closed_by": [
        24,
        30
      ]
    },
    {
      "id": 22,
      "x": 1.475,
      "y": -0.216,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        21,
        48
      ]
    },
    {
      "id": 23,
      "x": -1.116,
      "y": 0.128,
      "angle": 45.0,
      "layer": 4,
      "closed_by": [
        25,
        51
      ]
    },
    {
      "id": 24,
      "x": 1.11,
      "y": 0.128,
      "angle": 315.0,
      "layer": 4,
      "closed_by": [
        26,
        50
      ]
    },
    {
      "id": 25,
      "x": -1.115,
      "y": 0.46,
      "layer": 5,
      "closed_by": [
        27
      ]
    },
    {
      "id": 26,
      "x": 1.12,
      "y": 0.46,
      "layer": 5,
      "closed_by": [
        28
      ]
    },
    {
      "id": 27,
      "x": -0.745,
      "y": 0.474,
      "angle": 45.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 0.74,
      "y": 0.469,
      "angle": 315.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -0.441,
      "y": 0.773,
      "angle": 45.0,
      "layer": 5,
      "closed_by": [
        27
      ]
    },
    {
      "id": 30,
      "x": 1.417,
      "y": 0.796,
      "angle": 45.0,
      "layer": 4,
      "closed_by": [
        26,
        50
      ]
    },
    {
      "id": 31,
      "x": -0.331,
      "y": -0.499,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 32,
      "x": 0.439,
      "y": 0.763,
      "angle": 315.0,
      "layer": 5,
      "closed_by": [
        28
      ]
    },
    {
      "id": 33,
      "x": -0.256,
      "y": 0.864,
      "layer": 4,
      "closed_by": [
        29,
        32
      ]
    },
    {
      "id": 34,
      "x": 0.266,
      "y": 0.864,
      "layer": 4,
      "closed_by": [
        29,
        32
      ]
    },
    {
      "id": 35,
      "x": -0.001,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        33,
        34
      ]
    },
    {
      "id": 36,
      "x": -1.133,
      "y": -0.335,
      "layer": 1,
      "closed_by": [
        19
      ]
    },
    {
      "id": 37,
      "x": 1.133,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 38,
      "x": -0.615,
      "y": -0.412,
      "layer": 3,
      "closed_by": [
        23,
        42
      ]
    },
    {
      "id": 39,
      "x": 0.619,
      "y": -0.414,
      "layer": 3,
      "closed_by": [
        24,
        43
      ]
    },
    {
      "id": 40,
      "x": 0.333,
      "y": -0.497,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 41,
      "x": -0.001,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        31,
        40,
        42,
        43
      ]
    },
    {
      "id": 42,
      "x": -0.259,
      "y": -0.34,
      "layer": 4,
      "closed_by": [
        44
      ]
    },
    {
      "id": 43,
      "x": 0.266,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": -0.001,
      "y": -0.18,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 45,
      "x": 1.952,
      "y": 0.256,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        21,
        48
      ]
    },
    {
      "id": 46,
      "x": -1.957,
      "y": 0.261,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        20,
        49
      ]
    },
    {
      "id": 47,
      "x": -1.424,
      "y": 0.791,
      "angle": 315.0,
      "layer": 4,
      "closed_by": [
        25,
        51
      ]
    },
    {
      "id": 48,
      "x": 1.985,
      "y": -0.256,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 49,
      "x": -1.985,
      "y": -0.254,
      "layer": 3,
      "closed_by": [
        51
      ]
    },
    {
      "id": 50,
      "x": 1.631,
      "y": 0.46,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -1.633,
      "y": 0.46,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 35,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 53.2,
    "star2": 37.8,
    "star3": 8.1
  }
}