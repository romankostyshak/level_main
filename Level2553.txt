{
  "layout_cards": [
    {
      "id": 3,
      "x": 2.467,
      "y": 0.298,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 4,
      "x": -2.305,
      "y": 0.698,
      "layer": 3,
      "closed_by": [
        6
      ]
    },
    {
      "id": 5,
      "x": 2.305,
      "y": -0.172,
      "layer": 3,
      "closed_by": [
        3
      ]
    },
    {
      "id": 6,
      "x": -2.463,
      "y": 0.3,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 8,
      "x": 2.308,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        3
      ]
    },
    {
      "id": 9,
      "x": 2.148,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        5,
        8
      ]
    },
    {
      "id": 10,
      "x": -2.144,
      "y": -0.172,
      "layer": 1,
      "closed_by": [
        17
      ]
    },
    {
      "id": 11,
      "x": -2.144,
      "y": 0.698,
      "layer": 1,
      "closed_by": [
        17
      ]
    },
    {
      "id": 12,
      "x": 2.148,
      "y": 0.698,
      "layer": 1,
      "closed_by": [
        9
      ]
    },
    {
      "id": 13,
      "x": -1.345,
      "y": -0.335,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 14,
      "x": -1.345,
      "y": 0.861,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 15,
      "x": 1.348,
      "y": 0.86,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 16,
      "x": 1.348,
      "y": -0.335,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 17,
      "x": -2.144,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        4,
        19
      ]
    },
    {
      "id": 18,
      "x": -1.052,
      "y": 0.939,
      "layer": 5,
      "closed_by": [
        14
      ]
    },
    {
      "id": 19,
      "x": -2.305,
      "y": -0.172,
      "layer": 3,
      "closed_by": [
        6
      ]
    },
    {
      "id": 20,
      "x": 1.052,
      "y": 0.939,
      "layer": 5,
      "closed_by": [
        15
      ]
    },
    {
      "id": 21,
      "x": 1.052,
      "y": -0.418,
      "layer": 5,
      "closed_by": [
        16
      ]
    },
    {
      "id": 22,
      "x": -1.585,
      "y": -0.172,
      "layer": 5,
      "closed_by": [
        13
      ]
    },
    {
      "id": 23,
      "x": -1.582,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        14
      ]
    },
    {
      "id": 24,
      "x": 1.585,
      "y": -0.177,
      "layer": 5,
      "closed_by": [
        16
      ]
    },
    {
      "id": 25,
      "x": 1.587,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        15
      ]
    },
    {
      "id": 26,
      "x": -1.347,
      "y": -0.178,
      "layer": 4,
      "closed_by": [
        22,
        37
      ]
    },
    {
      "id": 27,
      "x": -1.345,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        18,
        23
      ]
    },
    {
      "id": 28,
      "x": 1.347,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        20,
        25
      ]
    },
    {
      "id": 29,
      "x": -1.105,
      "y": -0.179,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 30,
      "x": -1.108,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 31,
      "x": 1.105,
      "y": -0.178,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 32,
      "x": 1.11,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 33,
      "x": -0.864,
      "y": -0.178,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 34,
      "x": -0.865,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 35,
      "x": 0.864,
      "y": -0.177,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 36,
      "x": 1.343,
      "y": -0.178,
      "layer": 4,
      "closed_by": [
        21,
        24
      ]
    },
    {
      "id": 37,
      "x": -1.052,
      "y": -0.414,
      "layer": 5,
      "closed_by": [
        13
      ]
    },
    {
      "id": 38,
      "x": 0.865,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 39,
      "x": -0.572,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        18,
        34,
        50
      ]
    },
    {
      "id": 40,
      "x": 0.006,
      "y": 0.216,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 41,
      "x": -0.307,
      "y": -0.09,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 42,
      "x": 0.305,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 43,
      "x": -0.307,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 44,
      "x": 0.305,
      "y": -0.093,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 45,
      "x": 0.303,
      "y": 0.221,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 46,
      "x": 0.574,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        21,
        35,
        44,
        45
      ]
    },
    {
      "id": 47,
      "x": 0.573,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        20,
        38,
        44,
        45
      ]
    },
    {
      "id": 48,
      "y": 0.221,
      "layer": 1,
      "closed_by": [
        44,
        45,
        50
      ]
    },
    {
      "id": 49,
      "x": -0.573,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        33,
        37,
        50
      ]
    },
    {
      "id": 50,
      "x": -0.307,
      "y": 0.221,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 51,
      "x": 2.148,
      "y": -0.172,
      "layer": 1,
      "closed_by": [
        9
      ]
    }
  ],
  "cards_in_layout_amount": 48,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 0.2,
    "star2": 4.2,
    "star3": 95.5
  }
}