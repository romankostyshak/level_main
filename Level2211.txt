{
  "layout_cards": [
    {
      "id": 27,
      "x": -1.105,
      "y": -0.416,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 28,
      "x": -0.33,
      "y": -0.577,
      "layer": 1,
      "closed_by": [
        31,
        35
      ]
    },
    {
      "id": 29,
      "x": 0.333,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        30,
        35
      ]
    },
    {
      "id": 30,
      "x": 0.708,
      "y": -0.495,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 31,
      "x": -0.703,
      "y": -0.495,
      "layer": 2,
      "closed_by": [
        27
      ]
    },
    {
      "id": 32,
      "x": 1.108,
      "y": -0.414,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 33,
      "x": 1.503,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        50
      ]
    },
    {
      "id": 34,
      "x": -1.503,
      "y": -0.335,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 35,
      "x": -0.004,
      "y": -0.574,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 36,
      "y": 0.782,
      "layer": 3,
      "closed_by": [
        37,
        38
      ]
    },
    {
      "id": 37,
      "x": 0.386,
      "y": 0.86,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 38,
      "x": -0.386,
      "y": 0.86,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 39,
      "x": 0.786,
      "y": 0.777,
      "layer": 5,
      "closed_by": [
        42
      ]
    },
    {
      "id": 40,
      "x": 1.745,
      "y": 0.467,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 41,
      "x": -0.787,
      "y": 0.781,
      "layer": 5,
      "closed_by": [
        43
      ]
    },
    {
      "id": 42,
      "x": 1.105,
      "y": 0.699,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 43,
      "x": -1.184,
      "y": 0.699,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 46,
      "y": 0.944,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 47,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": -0.003,
      "y": -0.418,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 49,
      "x": -1.746,
      "y": 0.465,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 50,
      "x": 1.585,
      "y": -0.172,
      "layer": 5,
      "closed_by": [
        40
      ]
    },
    {
      "id": 51,
      "x": -1.585,
      "y": -0.172,
      "layer": 5,
      "closed_by": [
        49
      ]
    }
  ],
  "cards_in_layout_amount": 23,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 53.5,
    "star2": 3.6,
    "star3": 42.6
  }
}