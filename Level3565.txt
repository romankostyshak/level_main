{
  "layout_cards": [
    {
      "id": 19,
      "x": 0.638,
      "y": 1.023,
      "layer": 1,
      "closed_by": [
        20,
        23,
        24
      ]
    },
    {
      "id": 20,
      "x": 0.638,
      "y": 0.381,
      "layer": 2,
      "closed_by": [
        21,
        22
      ]
    },
    {
      "id": 21,
      "x": 0.194,
      "y": 0.416,
      "angle": 45.0,
      "layer": 3,
      "closed_by": [
        31,
        34
      ]
    },
    {
      "id": 22,
      "x": 1.088,
      "y": 0.416,
      "angle": 315.0,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 23,
      "x": 0.194,
      "y": 0.976,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        21,
        35
      ]
    },
    {
      "id": 24,
      "x": 1.08,
      "y": 0.986,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 25,
      "x": -0.648,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        26,
        27,
        28
      ]
    },
    {
      "id": 26,
      "x": -0.647,
      "y": 0.059,
      "layer": 2,
      "closed_by": [
        29,
        30
      ]
    },
    {
      "id": 27,
      "x": -0.194,
      "y": -0.537,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        29,
        36
      ]
    },
    {
      "id": 28,
      "x": -1.1,
      "y": -0.54,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 29,
      "x": -0.2,
      "y": 0.028,
      "angle": 45.0,
      "layer": 3,
      "closed_by": [
        31,
        34
      ]
    },
    {
      "id": 30,
      "x": -1.087,
      "y": 0.023,
      "angle": 315.0,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 31,
      "x": -0.002,
      "y": 0.633,
      "layer": 4,
      "closed_by": [
        39,
        51
      ]
    },
    {
      "id": 32,
      "x": -2.059,
      "y": -0.545,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 33,
      "x": 1.585,
      "y": 0.699,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -0.002,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        39,
        50
      ]
    },
    {
      "id": 35,
      "x": 1.082,
      "y": 0.986,
      "angle": 45.0,
      "layer": 4,
      "closed_by": [
        47
      ]
    },
    {
      "id": 36,
      "x": -1.1,
      "y": -0.537,
      "angle": 45.0,
      "layer": 4,
      "closed_by": [
        48
      ]
    },
    {
      "id": 37,
      "x": 1.587,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        24,
        40
      ]
    },
    {
      "id": 38,
      "x": -1.585,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        28,
        32
      ]
    },
    {
      "id": 39,
      "x": -0.003,
      "y": 0.224,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 2.082,
      "y": 0.984,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 41,
      "x": 2.065,
      "y": 0.412,
      "angle": 45.0,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 42,
      "x": -2.065,
      "y": 0.027,
      "angle": 45.0,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 43,
      "x": 2.308,
      "y": 0.693,
      "layer": 4,
      "closed_by": [
        45
      ]
    },
    {
      "id": 44,
      "x": -2.305,
      "y": -0.259,
      "layer": 4,
      "closed_by": [
        46
      ]
    },
    {
      "id": 45,
      "x": 2.049,
      "y": 0.693,
      "layer": 5,
      "closed_by": [
        33
      ]
    },
    {
      "id": 46,
      "x": -2.042,
      "y": -0.259,
      "layer": 5,
      "closed_by": [
        49
      ]
    },
    {
      "id": 47,
      "x": 1.118,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        33
      ]
    },
    {
      "id": 48,
      "x": -1.123,
      "y": -0.259,
      "layer": 5,
      "closed_by": [
        49
      ]
    },
    {
      "id": 49,
      "x": -1.582,
      "y": -0.259,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 50,
      "x": -0.004,
      "y": -0.583,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -0.008,
      "y": 1.036,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 33,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 62.1,
    "star2": 19.9,
    "star3": 17.1
  }
}