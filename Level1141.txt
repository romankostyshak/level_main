{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.412,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        49,
        45
      ]
    },
    {
      "id": 50,
      "x": 0.412,
      "y": -0.256,
      "layer": 1,
      "closed_by": [
        48,
        45
      ]
    },
    {
      "id": 49,
      "x": -0.652,
      "y": 0.14,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 48,
      "x": 0.652,
      "y": 0.142,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "x": 0.335,
      "y": 0.625,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 46,
      "x": 0.625,
      "y": 1.026,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 45,
      "x": 0.007,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": 0.006,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 43,
      "x": 0.008,
      "y": -0.495,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 42,
      "x": 1.452,
      "y": 1.019,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 41,
      "x": -1.455,
      "y": 1.018,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 1.452,
      "y": 0.694,
      "layer": 5,
      "closed_by": [
        42
      ]
    },
    {
      "id": 39,
      "x": -1.457,
      "y": 0.693,
      "layer": 5,
      "closed_by": [
        41
      ]
    },
    {
      "id": 38,
      "x": 1.669,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 37,
      "x": -1.669,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 36,
      "x": 1.452,
      "y": 0.3,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 35,
      "x": -1.455,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 34,
      "x": 1.669,
      "y": 0.3,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 33,
      "x": -1.667,
      "y": 0.3,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 32,
      "x": 1.452,
      "y": -0.093,
      "layer": 1,
      "closed_by": [
        34
      ]
    },
    {
      "id": 31,
      "x": -1.452,
      "y": -0.092,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 30,
      "x": 0.864,
      "y": 0.142,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -0.333,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 28,
      "x": -0.62,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        25
      ]
    },
    {
      "id": 27,
      "x": 0.791,
      "y": 1.026,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -0.865,
      "y": 0.136,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 25,
      "x": -0.786,
      "y": 1.019,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 27,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 62.7,
    "star2": 26.8,
    "star3": 9.5
  }
}