{
  "layout_cards": [
    {
      "id": 14,
      "x": 0.279,
      "y": 1.019,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 15,
      "x": -0.238,
      "y": 1.019,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 18,
      "x": 0.518,
      "y": 1.019,
      "layer": 5,
      "closed_by": [
        14
      ]
    },
    {
      "id": 19,
      "x": -0.479,
      "y": 1.019,
      "layer": 5,
      "closed_by": [
        15
      ]
    },
    {
      "id": 20,
      "x": 0.017,
      "y": -0.578,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 21,
      "x": -0.238,
      "y": -0.578,
      "layer": 5,
      "closed_by": [
        20,
        23
      ]
    },
    {
      "id": 22,
      "x": 0.578,
      "y": -0.578,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 23,
      "x": -0.537,
      "y": -0.578,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 0.837,
      "y": -0.578,
      "layer": 5,
      "closed_by": [
        22
      ]
    },
    {
      "id": 25,
      "x": -0.799,
      "y": -0.578,
      "layer": 5,
      "closed_by": [
        23
      ]
    },
    {
      "id": 26,
      "x": 1.098,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 27,
      "x": -1.059,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        25
      ]
    },
    {
      "id": 28,
      "x": 1.878,
      "y": -0.1,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 29,
      "x": 1.299,
      "y": 0.479,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -1.258,
      "y": 0.479,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -1.58,
      "y": -0.1,
      "layer": 4,
      "closed_by": [
        30,
        36
      ]
    },
    {
      "id": 32,
      "x": 1.618,
      "y": -0.1,
      "layer": 4,
      "closed_by": [
        28,
        29
      ]
    },
    {
      "id": 33,
      "x": -0.999,
      "y": 0.479,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 34,
      "x": 1.039,
      "y": 0.479,
      "layer": 5,
      "closed_by": [
        29
      ]
    },
    {
      "id": 35,
      "x": -1.319,
      "y": -0.1,
      "layer": 3,
      "closed_by": [
        27,
        31,
        33
      ]
    },
    {
      "id": 36,
      "x": -1.838,
      "y": -0.1,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 37,
      "x": 0.279,
      "y": -0.578,
      "layer": 5,
      "closed_by": [
        20,
        22
      ]
    },
    {
      "id": 38,
      "x": -0.74,
      "y": 0.479,
      "layer": 4,
      "closed_by": [
        19,
        33
      ]
    },
    {
      "id": 39,
      "x": 0.777,
      "y": 0.479,
      "layer": 4,
      "closed_by": [
        18,
        34
      ]
    },
    {
      "id": 40,
      "x": -1.059,
      "y": -0.1,
      "layer": 2,
      "closed_by": [
        35,
        38
      ]
    },
    {
      "id": 41,
      "x": -0.479,
      "y": 0.479,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 42,
      "x": 1.358,
      "y": -0.1,
      "layer": 3,
      "closed_by": [
        26,
        32,
        34
      ]
    },
    {
      "id": 43,
      "x": 0.518,
      "y": 0.479,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 44,
      "x": 1.098,
      "y": -0.1,
      "layer": 2,
      "closed_by": [
        39,
        42
      ]
    },
    {
      "id": 45,
      "x": -0.66,
      "y": 0.039,
      "angle": 315.0,
      "layer": 1,
      "closed_by": [
        40,
        41
      ]
    },
    {
      "id": 46,
      "x": -0.079,
      "y": 0.62,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        18,
        41,
        43
      ]
    },
    {
      "id": 47,
      "x": 0.119,
      "y": 0.62,
      "angle": 45.0,
      "layer": 1,
      "closed_by": [
        19,
        46
      ]
    },
    {
      "id": 48,
      "x": 0.699,
      "y": 0.039,
      "angle": 45.0,
      "layer": 1,
      "closed_by": [
        43,
        44
      ]
    },
    {
      "id": 49,
      "x": 1.279,
      "y": -0.537,
      "angle": 45.0,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 50,
      "x": -1.24,
      "y": -0.537,
      "angle": 315.0,
      "layer": 1,
      "closed_by": [
        40
      ]
    }
  ],
  "cards_in_layout_amount": 35,
  "cards_in_deck_amount": 30,
  "layers_in_level": 6,
  "open_cards": 9,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 30.1,
    "star2": 67.0,
    "star3": 2.7
  }
}