{
  "layout_cards": [
    {
      "id": 24,
      "x": 2.092,
      "y": 0.698,
      "layer": 2,
      "closed_by": [
        32,
        33
      ]
    },
    {
      "id": 25,
      "x": -2.094,
      "y": 1.024,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 26,
      "x": 2.091,
      "y": 1.026,
      "layer": 1,
      "closed_by": [
        24
      ]
    },
    {
      "id": 27,
      "x": 2.092,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 28,
      "x": -2.094,
      "y": -0.583,
      "layer": 1,
      "closed_by": [
        30
      ]
    },
    {
      "id": 29,
      "x": 2.092,
      "y": -0.18,
      "layer": 2,
      "closed_by": [
        34,
        35
      ]
    },
    {
      "id": 30,
      "x": -2.094,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        38,
        40
      ]
    },
    {
      "id": 31,
      "x": -2.092,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        36,
        39
      ]
    },
    {
      "id": 32,
      "x": 2.384,
      "y": 0.859,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 1.804,
      "y": 0.861,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 2.384,
      "y": -0.337,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 1.807,
      "y": -0.337,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -2.384,
      "y": 0.861,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 37,
      "x": 0.731,
      "y": -0.337,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -2.384,
      "y": -0.337,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -1.802,
      "y": 0.861,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 40,
      "x": -1.807,
      "y": -0.337,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 41,
      "x": -1.268,
      "y": 0.861,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -1.266,
      "y": -0.337,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 43,
      "x": 1.264,
      "y": 0.861,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 44,
      "x": 1.263,
      "y": -0.337,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 45,
      "x": -0.731,
      "y": -0.337,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 46,
      "x": 0.734,
      "y": 0.859,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 47,
      "x": -0.731,
      "y": 0.859,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 48,
      "x": -1.003,
      "y": 0.861,
      "layer": 3,
      "closed_by": [
        41,
        47
      ]
    },
    {
      "id": 49,
      "x": 0.99,
      "y": 0.856,
      "layer": 3,
      "closed_by": [
        43,
        46
      ]
    },
    {
      "id": 50,
      "x": 0.995,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        37,
        44
      ]
    },
    {
      "id": 51,
      "x": -1.003,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        42,
        45
      ]
    }
  ],
  "level_mechanic": 1,
  "free_cards_from_stickers": [
    24,
    26,
    27,
    29,
    32,
    33,
    34,
    35,
    37,
    43,
    44,
    46,
    49,
    50
  ],
  "cards_in_layout_amount": 28,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 22.7,
    "star2": 73.6,
    "star3": 3.0
  }
}