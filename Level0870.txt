{
  "layout_cards": [
    {
      "id": 50,
      "x": 0.574,
      "y": -0.577,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 49,
      "x": -0.573,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 47,
      "x": 0.573,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 46,
      "x": -0.572,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        34
      ]
    },
    {
      "id": 45,
      "x": 0.412,
      "y": -0.335,
      "layer": 2,
      "closed_by": [
        39,
        42
      ]
    },
    {
      "id": 43,
      "x": -0.414,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        33,
        42
      ]
    },
    {
      "id": 40,
      "x": 0.813,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 39,
      "x": 0.813,
      "y": -0.017,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 1.345,
      "y": 0.62,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 37,
      "x": 1.345,
      "y": -0.017,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 36,
      "x": 1.878,
      "y": -0.017,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 1.878,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 34,
      "x": -0.813,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 33,
      "x": -0.813,
      "y": -0.017,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -1.345,
      "y": 0.62,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -1.345,
      "y": -0.017,
      "layer": 4,
      "closed_by": [
        32
      ]
    },
    {
      "id": 30,
      "x": -1.876,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 29,
      "x": -1.876,
      "y": -0.017,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 41,
      "y": -0.439,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 42,
      "y": 0.136,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 44,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 48,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 51,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        45,
        43
      ]
    }
  ],
  "cards_in_layout_amount": 23,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 63.2,
    "star2": 10.6,
    "star3": 25.8
  }
}