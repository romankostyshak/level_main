{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.344,
      "y": 0.912,
      "angle": 344.997,
      "layer": 1,
      "closed_by": [
        40,
        39
      ]
    },
    {
      "id": 50,
      "x": 0.559,
      "y": -0.537,
      "angle": 344.997,
      "layer": 1,
      "closed_by": [
        49,
        41
      ]
    },
    {
      "id": 49,
      "x": 0.851,
      "y": -0.17,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        47,
        45
      ]
    },
    {
      "id": 48,
      "x": 0.143,
      "y": 0.187,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        43,
        42
      ]
    },
    {
      "id": 47,
      "x": 0.657,
      "y": 0.05,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        42,
        37
      ]
    },
    {
      "id": 46,
      "x": -0.358,
      "y": 0.331,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        43,
        38
      ]
    },
    {
      "id": 45,
      "x": 1.159,
      "y": -0.082,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 44,
      "x": -0.865,
      "y": 0.472,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 43,
      "x": -0.119,
      "y": 0.31,
      "angle": 344.997,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 42,
      "x": 0.384,
      "y": 0.082,
      "angle": 344.997,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 0.342,
      "y": -0.028,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        48,
        47
      ]
    },
    {
      "id": 40,
      "x": -0.596,
      "y": 0.546,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        46,
        44
      ]
    },
    {
      "id": 39,
      "x": -0.086,
      "y": 0.423,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        48,
        46
      ]
    },
    {
      "id": 38,
      "x": -0.652,
      "y": 0.372,
      "angle": 344.997,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 37,
      "x": 0.911,
      "y": 0.032,
      "angle": 344.997,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 36,
      "x": -0.358,
      "y": 0.023,
      "angle": 344.997,
      "layer": 1,
      "closed_by": [
        40,
        39,
        33
      ]
    },
    {
      "id": 35,
      "x": 0.578,
      "y": 0.34,
      "angle": 344.997,
      "layer": 1,
      "closed_by": [
        49,
        41,
        34
      ]
    },
    {
      "id": 34,
      "x": 0.754,
      "y": 0.99,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 33,
      "x": -0.507,
      "y": -0.527,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 32,
      "x": 1.011,
      "y": 0.98,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 31,
      "x": -0.759,
      "y": -0.544,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 30,
      "x": 1.261,
      "y": 0.859,
      "angle": 344.997,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 29,
      "x": -1.008,
      "y": -0.425,
      "angle": 344.997,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 27,
      "x": -0.892,
      "y": 0.008,
      "angle": 344.997,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 1.136,
      "y": 0.393,
      "angle": 344.997,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 25,
  "cards_in_deck_amount": 26,
  "layers_in_level": 5,
  "open_cards": 4,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 37.7,
    "star2": 37.0,
    "star3": 24.4
  }
}