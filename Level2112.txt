{
  "layout_cards": [
    {
      "id": 24,
      "x": -0.467,
      "y": -0.578,
      "layer": 5,
      "closed_by": [
        41
      ]
    },
    {
      "id": 25,
      "x": -0.333,
      "y": -0.259,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -0.333,
      "y": 0.379,
      "layer": 6,
      "closed_by": [
        25,
        27
      ]
    },
    {
      "id": 27,
      "x": -0.333,
      "y": 1.019,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 0.465,
      "y": -0.577,
      "layer": 5,
      "closed_by": [
        40
      ]
    },
    {
      "id": 29,
      "x": 0.333,
      "y": -0.259,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 0.333,
      "y": 0.379,
      "layer": 6,
      "closed_by": [
        29,
        31
      ]
    },
    {
      "id": 31,
      "x": 0.333,
      "y": 1.019,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 1.756,
      "y": 0.976,
      "angle": 15.0,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 33,
      "x": 1.503,
      "y": 0.912,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 34,
      "x": 1.256,
      "y": 0.845,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        51
      ]
    },
    {
      "id": 35,
      "x": -1.273,
      "y": 0.837,
      "angle": 345.0,
      "layer": 3,
      "closed_by": [
        49
      ]
    },
    {
      "id": 36,
      "x": -1.526,
      "y": 0.902,
      "angle": 345.0,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 37,
      "x": -1.779,
      "y": 0.972,
      "angle": 345.0,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 38,
      "x": -0.625,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 39,
      "x": 0.629,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 40,
      "x": 0.333,
      "y": -0.578,
      "layer": 6,
      "closed_by": [
        29
      ]
    },
    {
      "id": 41,
      "x": -0.333,
      "y": -0.578,
      "layer": 6,
      "closed_by": [
        25
      ]
    },
    {
      "id": 42,
      "x": -1.804,
      "y": -0.224,
      "angle": 15.0,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 43,
      "x": 1.794,
      "y": -0.216,
      "angle": 345.0,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 44,
      "x": -1.559,
      "y": -0.158,
      "angle": 15.0,
      "layer": 6,
      "closed_by": [
        42
      ]
    },
    {
      "id": 45,
      "x": 1.544,
      "y": -0.15,
      "angle": 345.0,
      "layer": 6,
      "closed_by": [
        43
      ]
    },
    {
      "id": 46,
      "x": -1.297,
      "y": -0.09,
      "angle": 15.0,
      "layer": 5,
      "closed_by": [
        44
      ]
    },
    {
      "id": 47,
      "x": 1.294,
      "y": -0.082,
      "angle": 345.0,
      "layer": 5,
      "closed_by": [
        45
      ]
    },
    {
      "id": 48,
      "x": -0.726,
      "y": 0.381,
      "layer": 5,
      "closed_by": [
        26
      ]
    },
    {
      "id": 49,
      "x": -1.128,
      "y": 0.381,
      "layer": 4,
      "closed_by": [
        46,
        48
      ]
    },
    {
      "id": 50,
      "x": 0.736,
      "y": 0.381,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 51,
      "x": 1.133,
      "y": 0.381,
      "layer": 4,
      "closed_by": [
        47,
        50
      ]
    }
  ],
  "cards_in_layout_amount": 28,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 60.0,
    "star2": 26.9,
    "star3": 12.7
  }
}