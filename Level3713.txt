{
  "layout_cards": [
    {
      "x": 2.759,
      "y": 0.298,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 1,
      "x": -2.417,
      "y": 0.34,
      "angle": 345.0,
      "layer": 2,
      "closed_by": [
        51
      ]
    },
    {
      "id": 3,
      "y": 0.217,
      "layer": 7,
      "closed_by": [],
      "card_type": 2
    },
    {
      "id": 4,
      "x": -2.759,
      "y": 0.298,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 5,
      "x": 2.42,
      "y": 0.34,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        7
      ]
    },
    {
      "id": 6,
      "x": -2.259,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        1
      ]
    },
    {
      "id": 7,
      "x": 2.578,
      "y": 0.379,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        0
      ]
    },
    {
      "id": 8,
      "x": 2.259,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        5
      ],
      "card_type": 3
    },
    {
      "id": 9,
      "x": -1.498,
      "y": 1.019,
      "angle": 345.0,
      "layer": 4,
      "closed_by": [
        17
      ]
    },
    {
      "id": 10,
      "x": -1.358,
      "y": 0.98,
      "angle": 345.0,
      "layer": 3,
      "closed_by": [
        9
      ]
    },
    {
      "id": 11,
      "x": -1.22,
      "y": 0.939,
      "angle": 345.0,
      "layer": 2,
      "closed_by": [
        10
      ]
    },
    {
      "id": 12,
      "x": -1.498,
      "y": -0.418,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        16
      ]
    },
    {
      "id": 13,
      "x": -1.358,
      "y": -0.379,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        12
      ]
    },
    {
      "id": 14,
      "x": -1.22,
      "y": -0.337,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        13
      ]
    },
    {
      "id": 15,
      "x": 1.639,
      "y": -0.259,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 16,
      "x": -1.639,
      "y": -0.259,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 17,
      "x": -1.639,
      "y": 0.859,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 18,
      "x": 1.5,
      "y": -0.418,
      "angle": 345.0,
      "layer": 4,
      "closed_by": [
        15
      ]
    },
    {
      "id": 19,
      "x": 1.358,
      "y": -0.379,
      "angle": 345.0,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 20,
      "x": 1.22,
      "y": -0.337,
      "angle": 345.0,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 21,
      "x": 1.5,
      "y": 1.019,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 22,
      "x": 1.358,
      "y": 0.98,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 23,
      "x": 1.22,
      "y": 0.939,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 24,
      "x": -1.139,
      "y": 0.859,
      "layer": 1,
      "closed_by": [
        11
      ]
    },
    {
      "id": 25,
      "x": -1.139,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        14
      ]
    },
    {
      "id": 26,
      "x": 1.139,
      "y": 0.859,
      "layer": 1,
      "closed_by": [
        23
      ]
    },
    {
      "id": 27,
      "x": 1.139,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        20
      ]
    },
    {
      "id": 28,
      "x": 0.259,
      "y": -0.259,
      "layer": 6,
      "closed_by": [
        3
      ]
    },
    {
      "id": 29,
      "y": -0.337,
      "layer": 5,
      "closed_by": [
        28,
        36
      ]
    },
    {
      "id": 30,
      "x": -0.259,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 31,
      "x": 0.259,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 32,
      "x": -0.259,
      "y": -0.5,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 33,
      "x": 0.259,
      "y": -0.5,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 34,
      "x": -0.379,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 35,
      "x": 0.379,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 36,
      "x": -0.259,
      "y": -0.259,
      "layer": 6,
      "closed_by": [
        3
      ]
    },
    {
      "id": 37,
      "x": 1.639,
      "y": 0.859,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 0.259,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 39,
      "x": -0.259,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        34
      ]
    },
    {
      "id": 40,
      "x": -0.259,
      "y": 0.699,
      "layer": 6,
      "closed_by": [
        3
      ]
    },
    {
      "id": 41,
      "x": 0.259,
      "y": 0.699,
      "layer": 6,
      "closed_by": [
        3
      ]
    },
    {
      "id": 42,
      "y": 0.777,
      "layer": 5,
      "closed_by": [
        40,
        41
      ]
    },
    {
      "id": 43,
      "x": -0.259,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 44,
      "x": 0.259,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 45,
      "x": 0.259,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 46,
      "x": -0.379,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 47,
      "x": 0.379,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 48,
      "x": -0.259,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 49,
      "x": 0.259,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 50,
      "x": -0.259,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 51,
      "x": -2.578,
      "y": 0.379,
      "angle": 345.0,
      "layer": 3,
      "closed_by": [
        4
      ]
    }
  ],
  "cards_in_layout_amount": 51,
  "cards_in_deck_amount": 34,
  "stars_statistic": {
    "star1": 13.2,
    "star2": 75.9,
    "star3": 9.9
  }
}