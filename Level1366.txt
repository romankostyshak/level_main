{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.002,
      "y": 0.238,
      "layer": 4,
      "closed_by": [
        49,
        48
      ]
    },
    {
      "id": 50,
      "x": -0.337,
      "y": 0.238,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 49,
      "y": -0.172,
      "layer": 5,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": 0.001,
      "y": 0.638,
      "layer": 5,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "y": 0.939,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 46,
      "x": 0.002,
      "y": -0.497,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 45,
      "x": 0.625,
      "y": 0.238,
      "layer": 5,
      "closed_by": [
        26
      ]
    },
    {
      "id": 35,
      "x": -0.624,
      "y": 0.638,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 34,
      "x": -0.624,
      "y": 0.238,
      "layer": 5,
      "closed_by": [
        33
      ]
    },
    {
      "id": 33,
      "x": -0.624,
      "y": -0.179,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -0.337,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 31,
      "x": -0.337,
      "y": 0.638,
      "layer": 3,
      "closed_by": [
        51,
        35
      ]
    },
    {
      "id": 27,
      "x": 0.625,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        45
      ]
    },
    {
      "id": 26,
      "x": 0.625,
      "y": 0.638,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 0.337,
      "y": 0.638,
      "layer": 1,
      "closed_by": [
        24
      ]
    },
    {
      "id": 24,
      "x": 0.337,
      "y": 0.238,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 23,
      "x": 0.337,
      "y": -0.179,
      "layer": 3,
      "closed_by": [
        51,
        27
      ]
    },
    {
      "id": 39,
      "x": -1.215,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 29,
      "x": -1.215,
      "y": 0.518,
      "layer": 5,
      "closed_by": [
        36
      ]
    },
    {
      "id": 36,
      "x": -1.215,
      "y": 0.1,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -1.215,
      "y": 0.638,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 40,
      "x": -1.215,
      "y": -0.039,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 28,
      "x": -1.215,
      "y": -0.179,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 41,
      "x": 1.213,
      "y": -0.039,
      "layer": 5,
      "closed_by": [
        37
      ]
    },
    {
      "id": 38,
      "x": 1.213,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": 1.213,
      "y": 0.238,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 43,
      "x": 1.213,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 42,
      "x": 1.213,
      "y": 0.5,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 37,
      "x": 1.213,
      "y": 0.379,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 29,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 51.5,
    "star2": 35.7,
    "star3": 11.8
  }
}