{
  "layout_cards": [
    {
      "id": 14,
      "x": -1.506,
      "y": 0.624,
      "layer": 3,
      "closed_by": [
        26,
        50
      ]
    },
    {
      "id": 15,
      "x": -0.962,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        17,
        18
      ]
    },
    {
      "id": 16,
      "x": -0.966,
      "y": 0.625,
      "layer": 3,
      "closed_by": [
        26,
        29
      ]
    },
    {
      "id": 17,
      "x": -0.569,
      "y": 1.021,
      "layer": 2,
      "closed_by": [
        16,
        19
      ]
    },
    {
      "id": 18,
      "x": -1.343,
      "y": 1.021,
      "layer": 2,
      "closed_by": [
        14,
        16
      ]
    },
    {
      "id": 19,
      "x": -0.411,
      "y": 0.623,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 20,
      "x": 0.953,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        21,
        22
      ]
    },
    {
      "id": 21,
      "x": 1.348,
      "y": -0.574,
      "layer": 2,
      "closed_by": [
        23,
        24
      ]
    },
    {
      "id": 22,
      "x": 0.572,
      "y": -0.574,
      "layer": 2,
      "closed_by": [
        23,
        25
      ]
    },
    {
      "id": 23,
      "x": 0.957,
      "y": -0.172,
      "layer": 3,
      "closed_by": [
        30,
        31
      ]
    },
    {
      "id": 24,
      "x": 1.506,
      "y": -0.172,
      "layer": 3,
      "closed_by": [
        31,
        49
      ]
    },
    {
      "id": 25,
      "x": 0.412,
      "y": -0.172,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 26,
      "x": -1.347,
      "y": 0.458,
      "layer": 4,
      "closed_by": [
        32,
        47
      ]
    },
    {
      "id": 27,
      "x": -1.263,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 28,
      "x": 0.958,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        38,
        41
      ],
      "card_type": 3
    },
    {
      "id": 29,
      "x": -0.572,
      "y": 0.46,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 30,
      "x": 0.574,
      "y": -0.017,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 31,
      "x": 1.35,
      "y": -0.017,
      "layer": 4,
      "closed_by": [
        35,
        51
      ]
    },
    {
      "id": 32,
      "x": -1.223,
      "y": 0.456,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -0.699,
      "y": 0.46,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 0.694,
      "y": -0.017,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 1.223,
      "y": -0.017,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -0.967,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        37,
        42
      ]
    },
    {
      "id": 37,
      "x": -0.573,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 38,
      "x": 0.573,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 39,
      "x": 0.171,
      "y": 1.021,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 40,
      "x": -0.171,
      "y": -0.578,
      "layer": 3,
      "closed_by": [],
      "card_type": 2
    },
    {
      "id": 41,
      "x": 1.105,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 42,
      "x": -1.105,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        27
      ]
    },
    {
      "id": 43,
      "x": 1.263,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": 1.427,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        46
      ]
    },
    {
      "id": 45,
      "x": -1.424,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        48
      ]
    },
    {
      "id": 46,
      "x": 1.506,
      "y": 1.019,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 47,
      "x": -1.774,
      "y": 0.3,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 48,
      "x": -1.506,
      "y": -0.578,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 49,
      "x": 1.906,
      "y": -0.017,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 50,
      "x": -1.906,
      "y": 0.46,
      "layer": 4,
      "closed_by": [
        47
      ]
    },
    {
      "id": 51,
      "x": 1.771,
      "y": 0.14,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 38,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 22.0,
    "star2": 66.4,
    "star3": 10.9
  }
}