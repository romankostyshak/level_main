{
  "layout_cards": [
    {
      "id": 48,
      "x": -1.296,
      "y": 1.024,
      "layer": 1,
      "closed_by": [
        45,
        39
      ]
    },
    {
      "id": 47,
      "x": 1.294,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        40,
        50
      ]
    },
    {
      "id": 49,
      "x": -1.61,
      "y": 0.142,
      "layer": 1,
      "closed_by": [
        43,
        39
      ]
    },
    {
      "id": 44,
      "x": 1.375,
      "y": -0.261,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 43,
      "x": -1.373,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 42,
      "x": 1.054,
      "y": -0.093,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 45,
      "x": -1.049,
      "y": 0.624,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 41,
      "x": -1.052,
      "y": -0.1,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 40,
      "x": 1.615,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 35,
      "x": 0.467,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 32,
      "x": 0.467,
      "y": -0.017,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 30,
      "x": 0.33,
      "y": -0.414,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 28,
      "x": 0.272,
      "y": -0.261,
      "layer": 5,
      "closed_by": [
        36
      ]
    },
    {
      "id": 39,
      "x": -1.61,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        26
      ]
    },
    {
      "id": 26,
      "x": -1.57,
      "y": 0.735,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 25,
      "x": 1.569,
      "y": 0.731,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 24,
      "x": 1.307,
      "y": 0.319,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 23,
      "x": -1.302,
      "y": 0.317,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 46,
      "x": 1.613,
      "y": 0.14,
      "layer": 1,
      "closed_by": [
        44,
        40
      ]
    },
    {
      "id": 50,
      "x": 1.054,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 22,
      "x": 1.174,
      "y": 0.123,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 21,
      "x": -1.172,
      "y": 0.118,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 0.001,
      "y": 0.782,
      "layer": 1,
      "closed_by": [
        35,
        34
      ]
    },
    {
      "id": 34,
      "x": -0.465,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 33,
      "x": -0.465,
      "y": -0.017,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 31,
      "x": -0.328,
      "y": -0.416,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 29,
      "x": -0.268,
      "y": -0.259,
      "layer": 5,
      "closed_by": [
        27
      ]
    },
    {
      "id": 27,
      "x": -0.268,
      "y": 0.418,
      "layer": 6,
      "closed_by": [
        37
      ]
    },
    {
      "id": 36,
      "x": 0.273,
      "y": 0.418,
      "layer": 6,
      "closed_by": [
        37
      ]
    },
    {
      "id": 37,
      "x": -0.002,
      "y": -0.263,
      "layer": 7,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 30,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 46.9,
    "star2": 41.2,
    "star3": 10.9
  }
}