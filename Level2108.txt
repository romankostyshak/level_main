{
  "layout_cards": [
    {
      "id": 51,
      "y": 0.785,
      "layer": 1,
      "closed_by": [
        50,
        49
      ]
    },
    {
      "id": 50,
      "x": 0.388,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 49,
      "x": -0.384,
      "y": 0.702,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 48,
      "x": 0.787,
      "y": 0.782,
      "layer": 3,
      "closed_by": [
        40,
        17
      ]
    },
    {
      "id": 47,
      "x": -0.782,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        35,
        16
      ]
    },
    {
      "id": 46,
      "x": 0.003,
      "y": -0.333,
      "layer": 1,
      "closed_by": [
        45,
        44
      ]
    },
    {
      "id": 45,
      "x": 0.388,
      "y": -0.252,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 44,
      "x": -0.386,
      "y": -0.254,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 43,
      "x": 0.787,
      "y": -0.331,
      "layer": 3,
      "closed_by": [
        39,
        17
      ]
    },
    {
      "id": 42,
      "x": -0.804,
      "y": -0.333,
      "layer": 3,
      "closed_by": [
        41,
        16
      ]
    },
    {
      "id": 41,
      "x": -1.182,
      "y": -0.256,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 40,
      "x": 1.187,
      "y": 0.702,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 39,
      "x": 1.187,
      "y": -0.256,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 38,
      "x": 0.925,
      "y": 0.222,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 37,
      "x": 0.628,
      "y": 0.222,
      "layer": 5,
      "closed_by": [
        38
      ]
    },
    {
      "id": 36,
      "x": 1.187,
      "y": 0.222,
      "layer": 5,
      "closed_by": [
        38
      ]
    },
    {
      "id": 35,
      "x": -1.182,
      "y": 0.702,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 34,
      "x": -0.624,
      "y": 0.222,
      "layer": 5,
      "closed_by": [
        32
      ]
    },
    {
      "id": 33,
      "x": -1.184,
      "y": 0.222,
      "layer": 5,
      "closed_by": [
        32
      ]
    },
    {
      "id": 32,
      "x": -0.916,
      "y": 0.222,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 1.773,
      "y": 1.016,
      "layer": 1,
      "closed_by": [
        30
      ]
    },
    {
      "id": 30,
      "x": 1.773,
      "y": 0.623,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 29,
      "x": -1.774,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 28,
      "x": 1.773,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 27,
      "x": 1.773,
      "y": -0.172,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 26,
      "x": -1.774,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 25,
      "x": -1.774,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 24,
      "x": -1.774,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        29,
        20
      ]
    },
    {
      "id": 23,
      "x": 1.83,
      "y": -0.107,
      "layer": 5,
      "closed_by": [
        21
      ]
    },
    {
      "id": 22,
      "x": -1.838,
      "y": 0.546,
      "layer": 5,
      "closed_by": [
        20
      ]
    },
    {
      "id": 21,
      "x": 1.832,
      "y": 0.298,
      "layer": 6,
      "closed_by": [
        19
      ]
    },
    {
      "id": 20,
      "x": -1.837,
      "y": 0.141,
      "layer": 6,
      "closed_by": [
        18
      ]
    },
    {
      "id": 19,
      "x": 1.832,
      "y": 0.689,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 18,
      "x": -1.835,
      "y": -0.263,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 17,
      "x": 0.391,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        37,
        15
      ]
    },
    {
      "id": 16,
      "x": -0.386,
      "y": 0.221,
      "layer": 4,
      "closed_by": [
        34,
        15
      ]
    },
    {
      "id": 15,
      "x": 0.001,
      "y": 0.216,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 37,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 19.1,
    "star2": 67.2,
    "star3": 13.2
  }
}