{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.819,
      "y": -0.587,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 50,
      "y": 0.224,
      "layer": 1,
      "closed_by": [
        49,
        45
      ]
    },
    {
      "id": 49,
      "x": -0.379,
      "y": 0.224,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 48,
      "x": -0.777,
      "y": 0.224,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "x": -1.179,
      "y": 0.224,
      "layer": 4,
      "closed_by": [
        46
      ]
    },
    {
      "id": 46,
      "x": -1.582,
      "y": 0.224,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 45,
      "x": 0.379,
      "y": 0.224,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": 0.777,
      "y": 0.224,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 43,
      "x": 1.179,
      "y": 0.224,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 42,
      "x": 1.587,
      "y": 0.224,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 1.587,
      "y": -0.587,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 40,
      "x": 1.2,
      "y": -0.587,
      "layer": 2,
      "closed_by": [
        51
      ]
    },
    {
      "id": 39,
      "x": 0.418,
      "y": -0.587,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 38,
      "y": -0.587,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 37,
      "x": -0.418,
      "y": -0.587,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 36,
      "x": -0.819,
      "y": -0.587,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 35,
      "x": -1.22,
      "y": -0.587,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 34,
      "x": -1.582,
      "y": -0.587,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 33,
      "x": -1.582,
      "y": 1.039,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 32,
      "x": -1.179,
      "y": 1.039,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 31,
      "x": -0.777,
      "y": 1.039,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -0.337,
      "y": 1.039,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 29,
      "y": 1.039,
      "layer": 1,
      "closed_by": [
        30,
        28
      ]
    },
    {
      "id": 28,
      "x": 0.337,
      "y": 1.039,
      "layer": 2,
      "closed_by": [
        27
      ]
    },
    {
      "id": 27,
      "x": 0.777,
      "y": 1.039,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 26,
      "x": 1.2,
      "y": 1.039,
      "layer": 2,
      "closed_by": [
        27
      ]
    },
    {
      "id": 25,
      "x": 1.587,
      "y": 1.039,
      "layer": 1,
      "closed_by": [
        26
      ]
    },
    {
      "id": 24,
      "x": -2.144,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        16
      ]
    },
    {
      "id": 23,
      "x": 2.144,
      "y": 0.379,
      "layer": 5,
      "closed_by": [
        22
      ]
    },
    {
      "id": 22,
      "x": 2.144,
      "y": 0.216,
      "layer": 6,
      "closed_by": [
        20
      ]
    },
    {
      "id": 21,
      "x": -2.144,
      "y": 0.216,
      "layer": 6,
      "closed_by": [
        19
      ]
    },
    {
      "id": 20,
      "x": 2.144,
      "y": 0.059,
      "layer": 7,
      "closed_by": [
        18
      ]
    },
    {
      "id": 19,
      "x": -2.144,
      "y": 0.379,
      "layer": 7,
      "closed_by": [
        6
      ]
    },
    {
      "id": 18,
      "x": 2.144,
      "y": -0.1,
      "layer": 8,
      "closed_by": []
    },
    {
      "id": 16,
      "x": -2.144,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        7
      ]
    },
    {
      "id": 15,
      "x": -2.144,
      "y": 0.059,
      "layer": 5,
      "closed_by": [
        21
      ]
    },
    {
      "id": 14,
      "x": 2.144,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        13
      ]
    },
    {
      "id": 13,
      "x": 2.144,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        12
      ]
    },
    {
      "id": 12,
      "x": 2.144,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        11
      ]
    },
    {
      "id": 11,
      "x": 2.144,
      "y": 0.537,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 8,
      "x": -2.144,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        24
      ]
    },
    {
      "id": 7,
      "x": -2.144,
      "y": -0.1,
      "layer": 4,
      "closed_by": [
        15
      ]
    },
    {
      "id": 6,
      "x": -2.144,
      "y": 0.537,
      "layer": 8,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 43,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 0.6,
    "star2": 24.5,
    "star3": 74.3
  }
}