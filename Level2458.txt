{
  "layout_cards": [
    {
      "id": 10,
      "x": -1.516,
      "y": 0.702,
      "layer": 1,
      "closed_by": [
        16
      ]
    },
    {
      "id": 11,
      "x": 1.531,
      "y": 0.702,
      "layer": 1,
      "closed_by": [
        12
      ]
    },
    {
      "id": 12,
      "x": 1.531,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        14
      ]
    },
    {
      "id": 13,
      "x": -1.519,
      "y": 0.537,
      "layer": 3,
      "closed_by": [
        15
      ]
    },
    {
      "id": 14,
      "x": 1.531,
      "y": 0.541,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 15,
      "x": -1.519,
      "y": 0.46,
      "layer": 4,
      "closed_by": [
        21,
        51
      ]
    },
    {
      "id": 16,
      "x": -1.521,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        13
      ]
    },
    {
      "id": 18,
      "x": 1.531,
      "y": 0.46,
      "layer": 4,
      "closed_by": [
        22,
        50
      ]
    },
    {
      "id": 19,
      "x": -2.065,
      "y": -0.017,
      "layer": 4,
      "closed_by": [
        21,
        24
      ]
    },
    {
      "id": 20,
      "x": 2.062,
      "y": -0.017,
      "layer": 4,
      "closed_by": [
        22,
        23
      ]
    },
    {
      "id": 21,
      "x": -1.774,
      "y": 0.381,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 22,
      "x": 1.771,
      "y": 0.381,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 1.774,
      "y": -0.499,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 24,
      "x": -1.769,
      "y": -0.497,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -0.307,
      "y": -0.377,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 27,
      "y": 0.86,
      "layer": 1,
      "closed_by": [
        29,
        30
      ]
    },
    {
      "id": 28,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        26,
        31
      ]
    },
    {
      "id": 29,
      "x": 0.305,
      "y": 0.818,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 30,
      "x": -0.305,
      "y": 0.814,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 31,
      "x": 0.305,
      "y": -0.375,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 32,
      "x": 0.386,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 33,
      "x": -0.386,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 34,
      "x": 0.388,
      "y": -0.497,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 35,
      "x": -0.384,
      "y": -0.497,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 36,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 37,
      "x": 0.004,
      "y": -0.574,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 38,
      "y": 0.781,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -0.944,
      "y": -0.254,
      "layer": 4,
      "closed_by": [
        48,
        51
      ]
    },
    {
      "id": 40,
      "x": 0.004,
      "y": -0.337,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 0.947,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 42,
      "x": 0.944,
      "y": 0.537,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 43,
      "x": -0.944,
      "y": 0.541,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 44,
      "x": -0.944,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 45,
      "x": 0.944,
      "y": 0.143,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 46,
      "x": -0.944,
      "y": 0.143,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 47,
      "x": 0.947,
      "y": -0.252,
      "layer": 4,
      "closed_by": [
        49,
        50
      ]
    },
    {
      "id": 48,
      "x": -1.184,
      "y": -0.5,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 49,
      "x": 1.184,
      "y": -0.497,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 50,
      "x": 1.187,
      "y": 0.381,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -1.184,
      "y": 0.381,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 40,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 1.6,
    "star2": 42.9,
    "star3": 55.3
  }
}