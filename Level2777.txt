{
  "layout_cards": [
    {
      "id": 3,
      "x": 2.117,
      "y": -0.303,
      "angle": 45.0,
      "layer": 1,
      "closed_by": [
        4
      ]
    },
    {
      "id": 4,
      "x": 1.878,
      "y": -0.303,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        8
      ]
    },
    {
      "id": 5,
      "x": -1.72,
      "y": -0.379,
      "angle": 315.0,
      "layer": 3,
      "closed_by": [
        16
      ]
    },
    {
      "id": 6,
      "x": -1.965,
      "y": -0.384,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        5,
        10
      ]
    },
    {
      "id": 7,
      "x": -2.203,
      "y": -0.379,
      "angle": 315.0,
      "layer": 1,
      "closed_by": [
        6,
        17
      ]
    },
    {
      "id": 8,
      "x": 1.636,
      "y": -0.298,
      "angle": 45.0,
      "layer": 3,
      "closed_by": [
        12
      ]
    },
    {
      "id": 9,
      "x": 1.585,
      "y": -0.09,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 10,
      "x": -1.345,
      "y": -0.097,
      "layer": 5,
      "closed_by": [
        17
      ]
    },
    {
      "id": 11,
      "x": 1.345,
      "y": -0.09,
      "layer": 5,
      "closed_by": [
        9
      ]
    },
    {
      "id": 12,
      "x": 1.108,
      "y": -0.092,
      "layer": 4,
      "closed_by": [
        11
      ]
    },
    {
      "id": 13,
      "x": -1.825,
      "y": 0.777,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 14,
      "x": 1.825,
      "y": 0.777,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 15,
      "x": -1.585,
      "y": 0.861,
      "layer": 4,
      "closed_by": [
        13
      ]
    },
    {
      "id": 16,
      "x": -1.105,
      "y": -0.093,
      "layer": 4,
      "closed_by": [
        10
      ]
    },
    {
      "id": 17,
      "x": -1.582,
      "y": -0.1,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 18,
      "x": 1.585,
      "y": 0.861,
      "layer": 4,
      "closed_by": [
        14
      ]
    },
    {
      "id": 19,
      "x": -1.266,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        15
      ]
    },
    {
      "id": 20,
      "x": 1.266,
      "y": 0.943,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 21,
      "y": 0.777,
      "layer": 5,
      "closed_by": [
        37
      ]
    },
    {
      "id": 22,
      "x": -1.424,
      "y": 1.018,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 23,
      "x": 1.427,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 24,
      "x": -0.892,
      "y": 1.016,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 25,
      "x": 0.893,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 26,
      "x": -1.082,
      "y": 0.827,
      "angle": 45.0,
      "layer": 1,
      "closed_by": [
        22,
        24
      ]
    },
    {
      "id": 27,
      "x": 1.072,
      "y": 0.822,
      "angle": 315.0,
      "layer": 1,
      "closed_by": [
        23,
        25
      ]
    },
    {
      "id": 28,
      "x": -0.303,
      "y": 0.888,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 29,
      "x": 0.314,
      "y": 0.93,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 30,
      "x": -0.305,
      "y": 0.934,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 31,
      "x": 0.312,
      "y": 0.976,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 32,
      "x": -0.303,
      "y": 0.976,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 33,
      "x": 0.31,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 34,
      "x": -0.307,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 35,
      "x": -0.893,
      "y": -0.092,
      "layer": 3,
      "closed_by": [
        16
      ]
    },
    {
      "id": 36,
      "x": 0.31,
      "y": 0.887,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 37,
      "y": 0.136,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 0.892,
      "y": -0.093,
      "layer": 3,
      "closed_by": [
        12
      ]
    },
    {
      "id": 39,
      "x": 0.312,
      "y": 0.057,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 40,
      "x": -0.305,
      "y": 0.057,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 41,
      "x": -0.888,
      "y": -0.256,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 42,
      "x": 0.892,
      "y": -0.256,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 43,
      "x": 0.31,
      "y": -0.097,
      "layer": 3,
      "closed_by": [
        37,
        39
      ]
    },
    {
      "id": 44,
      "x": -0.305,
      "y": -0.097,
      "layer": 3,
      "closed_by": [
        37,
        40
      ]
    },
    {
      "id": 45,
      "x": -0.307,
      "y": -0.256,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 46,
      "x": -0.892,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        41
      ]
    },
    {
      "id": 47,
      "x": 0.893,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 48,
      "x": -0.305,
      "y": -0.416,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 49,
      "x": 0.307,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": 0.307,
      "y": -0.256,
      "layer": 2,
      "closed_by": [
        43
      ]
    }
  ],
  "cards_in_layout_amount": 48,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 0.1,
    "star2": 5.0,
    "star3": 94.6
  }
}