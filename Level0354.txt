{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.007,
      "y": 0.104,
      "layer": 1,
      "closed_by": [
        40,
        49
      ]
    },
    {
      "id": 50,
      "x": 0.6,
      "y": 0.1,
      "layer": 3,
      "closed_by": [
        48,
        46
      ]
    },
    {
      "id": 48,
      "x": 0.337,
      "y": -0.337,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 46,
      "x": 0.86,
      "y": -0.337,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 45,
      "x": -0.836,
      "y": -0.337,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 44,
      "x": -0.312,
      "y": -0.337,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 43,
      "x": -0.578,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        47,
        49
      ]
    },
    {
      "id": 41,
      "x": 0.86,
      "y": 0.56,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 40,
      "x": 0.337,
      "y": 0.56,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 39,
      "x": -0.574,
      "y": 0.1,
      "layer": 3,
      "closed_by": [
        45,
        44
      ]
    },
    {
      "id": 38,
      "x": -1.498,
      "y": 0.56,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 37,
      "x": -1.498,
      "y": 0.136,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 36,
      "x": -1.5,
      "y": -0.337,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -1.5,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 34,
      "x": 1.539,
      "y": -0.335,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 1.539,
      "y": 0.136,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 32,
      "x": 1.539,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 31,
      "x": 1.539,
      "y": 0.56,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 30,
      "x": 1.2,
      "y": 0.136,
      "layer": 1,
      "closed_by": [
        41,
        31
      ]
    },
    {
      "id": 29,
      "x": -1.179,
      "y": 0.136,
      "layer": 1,
      "closed_by": [
        38,
        47
      ]
    },
    {
      "id": 42,
      "x": 0.6,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        41,
        40
      ]
    },
    {
      "id": 47,
      "x": -0.833,
      "y": 0.56,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 49,
      "x": -0.312,
      "y": 0.56,
      "layer": 2,
      "closed_by": [
        39
      ]
    }
  ],
  "cards_in_layout_amount": 23,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 56.0,
    "star2": 4.6,
    "star3": 38.7
  }
}