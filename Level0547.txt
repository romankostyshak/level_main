{
  "layout_cards": [
    {
      "id": 30,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 29,
      "x": 0.574,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        27
      ]
    },
    {
      "id": 28,
      "x": -0.574,
      "y": 0.3,
      "layer": 1,
      "closed_by": [
        26
      ]
    },
    {
      "id": 31,
      "x": 0.001,
      "y": 0.298,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 0.574,
      "y": 0.298,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -0.573,
      "y": 0.298,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 39,
      "x": 1.613,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 42,
      "x": 2.052,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 36,
      "x": 1.213,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 45,
      "x": -1.213,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 46,
      "x": -1.613,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 51,
      "x": -2.052,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 40,
      "x": 2.052,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        39,
        38
      ]
    },
    {
      "id": 37,
      "x": 1.613,
      "y": 0.298,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 1.213,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        36,
        35
      ]
    },
    {
      "id": 35,
      "x": 1.213,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 38,
      "x": 1.613,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 41,
      "x": 2.052,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 44,
      "x": -1.213,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 48,
      "x": -1.613,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 49,
      "x": -2.052,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": -2.052,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        46,
        48
      ]
    },
    {
      "id": 47,
      "x": -1.613,
      "y": 0.298,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 43,
      "x": -1.213,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        45,
        44
      ]
    }
  ],
  "cards_in_layout_amount": 24,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 56.6,
    "star2": 10.0,
    "star3": 32.9
  }
}