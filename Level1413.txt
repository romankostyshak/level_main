{
  "layout_cards": [
    {
      "id": 48,
      "x": 0.333,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        47,
        29
      ]
    },
    {
      "id": 47,
      "x": 0.625,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        45,
        36
      ]
    },
    {
      "id": 45,
      "x": 0.944,
      "y": -0.09,
      "layer": 3,
      "closed_by": [
        44,
        30
      ]
    },
    {
      "id": 43,
      "x": -1.146,
      "y": -0.335,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 40,
      "x": -0.624,
      "y": 0.856,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 0.333,
      "y": 0.939,
      "layer": 5,
      "closed_by": [
        39,
        23
      ]
    },
    {
      "id": 33,
      "x": -1.264,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 44,
      "x": 1.149,
      "y": -0.34,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 31,
      "x": -1.585,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        43,
        33
      ]
    },
    {
      "id": 49,
      "x": -0.33,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        29,
        46
      ]
    },
    {
      "id": 29,
      "x": -0.001,
      "y": -0.495,
      "layer": 2,
      "closed_by": [
        36,
        28
      ]
    },
    {
      "id": 36,
      "x": 0.412,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        30,
        24
      ]
    },
    {
      "id": 34,
      "x": 1.268,
      "y": 0.856,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 26,
      "x": 1.159,
      "y": 0.856,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 25,
      "x": -1.159,
      "y": 0.859,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 39,
      "x": 0.624,
      "y": 0.859,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 46,
      "x": -0.624,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        50,
        28
      ]
    },
    {
      "id": 50,
      "x": -0.939,
      "y": -0.1,
      "layer": 3,
      "closed_by": [
        43,
        27
      ]
    },
    {
      "id": 27,
      "x": -0.532,
      "y": 0.291,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 42,
      "x": 1.057,
      "y": 0.319,
      "layer": 5,
      "closed_by": [
        26,
        39
      ]
    },
    {
      "id": 32,
      "x": 1.585,
      "y": 0.317,
      "layer": 3,
      "closed_by": [
        44,
        34
      ]
    },
    {
      "id": 41,
      "x": -1.054,
      "y": 0.298,
      "layer": 5,
      "closed_by": [
        40,
        25
      ]
    },
    {
      "id": 30,
      "x": 0.537,
      "y": 0.317,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 38,
      "x": -0.333,
      "y": 0.939,
      "layer": 5,
      "closed_by": [
        40,
        23
      ]
    },
    {
      "id": 23,
      "x": 0.013,
      "y": 0.861,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 28,
      "x": -0.418,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        27,
        24
      ]
    },
    {
      "id": 24,
      "y": 0.05,
      "layer": 4,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 27,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 65.6,
    "star2": 10.0,
    "star3": 23.4
  }
}