{
  "layout_cards": [
    {
      "id": 8,
      "x": 0.224,
      "y": -0.597,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 9,
      "x": 1.835,
      "y": -0.578,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 10,
      "x": -1.825,
      "y": -0.601,
      "layer": 4,
      "closed_by": [
        13
      ]
    },
    {
      "id": 11,
      "x": -1.299,
      "y": -0.601,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 12,
      "x": -0.773,
      "y": -0.6,
      "layer": 1,
      "closed_by": [
        14
      ]
    },
    {
      "id": 13,
      "x": -1.582,
      "y": -0.601,
      "layer": 5,
      "closed_by": [
        11
      ]
    },
    {
      "id": 14,
      "x": -0.488,
      "y": -0.597,
      "layer": 2,
      "closed_by": [
        15
      ]
    },
    {
      "id": 15,
      "x": -0.25,
      "y": -0.6,
      "layer": 3,
      "closed_by": [
        16
      ]
    },
    {
      "id": 16,
      "x": -0.013,
      "y": -0.194,
      "layer": 4,
      "closed_by": [
        8,
        18
      ]
    },
    {
      "id": 17,
      "x": -2.068,
      "y": -0.199,
      "layer": 3,
      "closed_by": [
        10
      ]
    },
    {
      "id": 18,
      "x": 0.224,
      "y": 0.224,
      "layer": 5,
      "closed_by": [
        21
      ]
    },
    {
      "id": 19,
      "x": -2.305,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        17
      ]
    },
    {
      "id": 20,
      "x": -1.934,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        19,
        24
      ]
    },
    {
      "id": 21,
      "x": -0.143,
      "y": 0.224,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -0.549,
      "y": 0.222,
      "layer": 5,
      "closed_by": [
        21
      ]
    },
    {
      "id": 23,
      "x": 0.628,
      "y": 1.039,
      "layer": 4,
      "closed_by": [
        31,
        51
      ]
    },
    {
      "id": 24,
      "x": -1.534,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 25,
      "x": -1.213,
      "y": 0.221,
      "layer": 3,
      "closed_by": [
        26,
        50
      ]
    },
    {
      "id": 26,
      "x": -0.892,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 27,
      "x": -1.343,
      "y": 1.042,
      "layer": 1,
      "closed_by": [
        28,
        45
      ]
    },
    {
      "id": 28,
      "x": -0.972,
      "y": 1.039,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 29,
      "x": -0.569,
      "y": 1.039,
      "layer": 3,
      "closed_by": [
        30,
        49
      ]
    },
    {
      "id": 30,
      "x": -0.165,
      "y": 1.039,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 31,
      "x": 0.224,
      "y": 1.039,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 1.029,
      "y": 1.039,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 33,
      "x": 1.427,
      "y": 1.039,
      "layer": 2,
      "closed_by": [
        32,
        36,
        51
      ]
    },
    {
      "id": 34,
      "x": 1.827,
      "y": 1.039,
      "layer": 1,
      "closed_by": [
        33,
        35
      ]
    },
    {
      "id": 35,
      "x": 2.226,
      "y": 1.039,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 36,
      "x": 1.832,
      "y": 0.624,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 37,
      "x": 1.835,
      "y": 0.231,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 38,
      "x": 1.832,
      "y": -0.179,
      "layer": 5,
      "closed_by": [
        9
      ]
    },
    {
      "id": 39,
      "x": 1.582,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 40,
      "x": 1.338,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        39,
        43
      ]
    },
    {
      "id": 42,
      "x": -2.302,
      "y": 1.044,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 43,
      "x": 0.972,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": 0.814,
      "y": -0.578,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 45,
      "x": -1.664,
      "y": 1.042,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 46,
      "x": -1.906,
      "y": 1.044,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 47,
      "x": 1.269,
      "y": -0.177,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 48,
      "x": 1.268,
      "y": 0.136,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 49,
      "x": -0.971,
      "y": 1.042,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 50,
      "x": -1.529,
      "y": 0.216,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 51,
      "x": 1.034,
      "y": 1.036,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 43,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 0.7,
    "star2": 21.7,
    "star3": 76.8
  }
}