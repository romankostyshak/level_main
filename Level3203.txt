{
  "layout_cards": [
    {
      "id": 18,
      "x": -0.703,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        19,
        20
      ]
    },
    {
      "id": 19,
      "x": -0.625,
      "y": 0.625,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 20,
      "x": -0.628,
      "y": -0.18,
      "layer": 2,
      "closed_by": [
        21
      ]
    },
    {
      "id": 21,
      "x": -0.569,
      "y": -0.5,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 22,
      "x": -0.49,
      "y": -0.18,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 23,
      "x": -0.411,
      "y": 0.221,
      "layer": 5,
      "closed_by": [
        24
      ]
    },
    {
      "id": 24,
      "x": -0.49,
      "y": 0.629,
      "layer": 6,
      "closed_by": [
        25
      ]
    },
    {
      "id": 25,
      "x": -0.546,
      "y": 0.939,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 26,
      "x": 0.708,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        27,
        28
      ]
    },
    {
      "id": 27,
      "x": 0.629,
      "y": 0.625,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 28,
      "x": 0.628,
      "y": -0.18,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 29,
      "x": 0.573,
      "y": 0.943,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 30,
      "x": 0.493,
      "y": 0.629,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 31,
      "x": -1.906,
      "y": -0.177,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -1.743,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        31,
        37
      ]
    },
    {
      "id": 33,
      "x": 0.412,
      "y": 0.216,
      "layer": 5,
      "closed_by": [
        34
      ]
    },
    {
      "id": 34,
      "x": 0.492,
      "y": -0.184,
      "layer": 6,
      "closed_by": [
        35
      ]
    },
    {
      "id": 35,
      "x": 0.546,
      "y": -0.495,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 1.371,
      "y": 0.216,
      "layer": 5,
      "closed_by": [
        38
      ]
    },
    {
      "id": 37,
      "x": -1.371,
      "y": 0.216,
      "layer": 5,
      "closed_by": [
        39
      ]
    },
    {
      "id": 38,
      "x": 1.371,
      "y": -0.495,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -1.371,
      "y": 0.944,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 1.743,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        36,
        47
      ]
    },
    {
      "id": 41,
      "x": 1.531,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 42,
      "x": -1.529,
      "y": -0.178,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 43,
      "x": 1.746,
      "y": 0.619,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 44,
      "x": -1.746,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 45,
      "x": 1.746,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 46,
      "x": -1.743,
      "y": -0.495,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 47,
      "x": 1.906,
      "y": 0.62,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 48,
      "x": 1.743,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 49,
      "x": -1.745,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 50,
      "x": 1.743,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 51,
      "x": -1.746,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        49
      ]
    }
  ],
  "cards_in_layout_amount": 34,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 40.2,
    "star2": 51.3,
    "star3": 7.5
  }
}