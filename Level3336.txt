{
  "layout_cards": [
    {
      "id": 18,
      "x": -2.069,
      "y": 0.782,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 19,
      "x": -2.065,
      "y": -0.416,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 20,
      "x": -1.667,
      "y": -0.495,
      "layer": 4,
      "closed_by": [
        19,
        21
      ]
    },
    {
      "id": 21,
      "x": -1.373,
      "y": -0.416,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -1.375,
      "y": 0.777,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 23,
      "x": -1.906,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        26
      ]
    },
    {
      "id": 24,
      "x": -1.906,
      "y": 0.62,
      "layer": 1,
      "closed_by": [
        25
      ]
    },
    {
      "id": 25,
      "x": -1.511,
      "y": 0.615,
      "layer": 2,
      "closed_by": [
        37,
        38
      ]
    },
    {
      "id": 26,
      "x": -1.508,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        20,
        35
      ]
    },
    {
      "id": 27,
      "x": -0.791,
      "y": -0.337,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 28,
      "x": -0.386,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 29,
      "x": -0.388,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 30,
      "x": 1.972,
      "y": -0.136,
      "angle": 345.0,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 31,
      "x": 1.975,
      "y": 0.43,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 32,
      "x": 1.666,
      "y": -0.059,
      "angle": 345.0,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 33,
      "x": 1.659,
      "y": 0.342,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 34,
      "x": 1.422,
      "y": 0.216,
      "layer": 5,
      "closed_by": [
        48
      ]
    },
    {
      "id": 35,
      "x": -1.108,
      "y": -0.259,
      "layer": 4,
      "closed_by": [
        21,
        27
      ]
    },
    {
      "id": 36,
      "x": -0.791,
      "y": 0.702,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 37,
      "x": -1.667,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        18,
        22
      ]
    },
    {
      "id": 38,
      "x": -1.108,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        22,
        36
      ]
    },
    {
      "id": 39,
      "x": -0.731,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        29,
        35
      ]
    },
    {
      "id": 40,
      "x": -0.734,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        28,
        38
      ]
    },
    {
      "id": 41,
      "x": -0.386,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 42,
      "x": -0.386,
      "y": 0.62,
      "layer": 1,
      "closed_by": [
        40,
        51
      ]
    },
    {
      "id": 44,
      "x": 0.675,
      "y": 0.479,
      "angle": 344.997,
      "layer": 5,
      "closed_by": [
        48
      ]
    },
    {
      "id": 45,
      "x": 0.008,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        29,
        41,
        46
      ]
    },
    {
      "id": 46,
      "x": 0.303,
      "y": -0.216,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        49
      ]
    },
    {
      "id": 47,
      "x": 0.68,
      "y": -0.115,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        44
      ]
    },
    {
      "id": 48,
      "x": 1.077,
      "y": -0.017,
      "angle": 15.0,
      "layer": 6,
      "closed_by": [
        50
      ]
    },
    {
      "id": 49,
      "x": 0.293,
      "y": 0.578,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 50,
      "x": 1.082,
      "y": 0.367,
      "angle": 344.997,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 51,
      "x": 0.008,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        28,
        49
      ]
    }
  ],
  "cards_in_layout_amount": 33,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 60.6,
    "star2": 21.2,
    "star3": 17.4
  }
}