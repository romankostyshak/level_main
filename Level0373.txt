{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.472,
      "y": 0.967,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        50,
        36
      ]
    },
    {
      "id": 50,
      "x": -0.638,
      "y": 0.851,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 49,
      "x": 0.469,
      "y": 0.972,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        48,
        36
      ]
    },
    {
      "id": 48,
      "x": 0.638,
      "y": 0.855,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "x": 0.731,
      "y": 0.712,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 45,
      "x": -1.016,
      "y": 0.303,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 44,
      "x": 1.48,
      "y": 0.763,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 43,
      "x": 1.36,
      "y": 0.712,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 42,
      "x": 1.039,
      "y": 0.298,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 1.608,
      "y": -0.165,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        25
      ]
    },
    {
      "id": 38,
      "x": -1.473,
      "y": -0.15,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 37,
      "x": -1.6,
      "y": 0.777,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        24
      ]
    },
    {
      "id": 36,
      "x": -0.002,
      "y": 0.861,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -1.358,
      "y": -0.108,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 34,
      "x": -1.358,
      "y": 0.712,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 32,
      "x": 0.651,
      "y": -0.361,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 31,
      "y": -0.418,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 1.608,
      "y": 0.776,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 29,
      "x": -1.595,
      "y": -0.172,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 28,
      "x": -0.657,
      "y": -0.358,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        27
      ]
    },
    {
      "id": 27,
      "x": -0.734,
      "y": -0.108,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 26,
      "x": 1.363,
      "y": -0.108,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 25,
      "x": 1.48,
      "y": -0.148,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        26
      ]
    },
    {
      "id": 24,
      "x": -1.47,
      "y": 0.767,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 39,
      "x": 0.734,
      "y": -0.107,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 46,
      "x": -0.734,
      "y": 0.717,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 33,
      "x": 0.518,
      "y": -0.527,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        32,
        31
      ]
    },
    {
      "id": 40,
      "x": -0.523,
      "y": -0.527,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        31,
        28
      ]
    }
  ],
  "cards_in_layout_amount": 28,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 39.6,
    "star2": 55.3,
    "star3": 4.1
  }
}