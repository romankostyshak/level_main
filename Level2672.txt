{
  "layout_cards": [
    {
      "id": 20,
      "x": -2.065,
      "y": 0.231,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 21,
      "x": -1.993,
      "y": 0.231,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 22,
      "x": -1.932,
      "y": 0.231,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 24,
      "x": 2.653,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        37
      ]
    },
    {
      "id": 25,
      "x": -2.285,
      "y": -0.546,
      "angle": 45.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -1.993,
      "y": 1.054,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 27,
      "x": -1.985,
      "y": -0.587,
      "layer": 1,
      "closed_by": [
        30
      ]
    },
    {
      "id": 28,
      "x": -1.702,
      "y": 1.021,
      "angle": 45.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -2.282,
      "y": 1.019,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 30,
      "x": -1.689,
      "y": -0.554,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 31,
      "x": -0.865,
      "y": 0.785,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 32,
      "x": -3.131,
      "y": -0.335,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 33,
      "x": -0.865,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 34,
      "x": -3.134,
      "y": 0.773,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 35,
      "x": -0.864,
      "y": -0.578,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -3.134,
      "y": 1.019,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 37,
      "x": 2.338,
      "y": -0.256,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -3.131,
      "y": -0.574,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 39,
      "x": 0.386,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 40,
      "x": 0.962,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        46,
        47
      ]
    },
    {
      "id": 41,
      "x": 1.519,
      "y": 1.026,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 42,
      "x": 2.084,
      "y": 0.223,
      "layer": 1,
      "closed_by": [
        37,
        48
      ]
    },
    {
      "id": 43,
      "x": 2.654,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 44,
      "x": 0.386,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 45,
      "x": 1.519,
      "y": -0.577,
      "layer": 1,
      "closed_by": [
        51
      ]
    },
    {
      "id": 46,
      "x": 0.707,
      "y": 0.699,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 47,
      "x": 0.703,
      "y": -0.256,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 48,
      "x": 2.344,
      "y": 0.699,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 49,
      "x": -0.864,
      "y": 1.023,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 50,
      "x": 1.521,
      "y": 0.712,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 51,
      "x": 1.519,
      "y": -0.259,
      "layer": 2,
      "closed_by": []
    }
  ],
  "level_mechanic": 2,
  "free_cards_from_stickers": [
    20,
    21,
    22,
    25,
    26,
    27,
    28,
    29,
    30,
    31,
    32,
    33,
    34,
    35,
    36,
    38,
    49
  ],
  "cards_in_layout_amount": 31,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 3.1,
    "star2": 73.3,
    "star3": 22.8
  }
}