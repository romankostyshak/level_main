{
  "layout_cards": [
    {
      "id": 13,
      "x": 0.731,
      "y": 0.221,
      "layer": 2,
      "closed_by": [
        21,
        22,
        24,
        28
      ]
    },
    {
      "id": 14,
      "x": -0.342,
      "y": 0.62,
      "layer": 1,
      "closed_by": [
        15
      ]
    },
    {
      "id": 15,
      "x": -0.721,
      "y": 0.224,
      "layer": 2,
      "closed_by": [
        17,
        18,
        23,
        25
      ]
    },
    {
      "id": 16,
      "x": -1.105,
      "y": -0.172,
      "layer": 1,
      "closed_by": [
        15
      ]
    },
    {
      "id": 17,
      "x": -0.3,
      "y": 0.666,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 18,
      "x": -1.144,
      "y": -0.216,
      "layer": 3,
      "closed_by": [
        35,
        39
      ]
    },
    {
      "id": 19,
      "x": 0.349,
      "y": 0.62,
      "layer": 1,
      "closed_by": [
        13
      ]
    },
    {
      "id": 20,
      "x": 1.105,
      "y": -0.172,
      "layer": 1,
      "closed_by": [
        13
      ]
    },
    {
      "id": 21,
      "x": 0.307,
      "y": 0.67,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 22,
      "x": 1.149,
      "y": -0.216,
      "layer": 3,
      "closed_by": [
        27,
        38
      ]
    },
    {
      "id": 23,
      "x": -1.184,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 24,
      "x": 1.187,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 25,
      "x": -0.331,
      "y": -0.495,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 26,
      "x": -1.582,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        44
      ]
    },
    {
      "id": 27,
      "x": 1.026,
      "y": 0.216,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 0.335,
      "y": -0.493,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 29,
      "x": 1.23,
      "y": 0.985,
      "layer": 4,
      "closed_by": [
        27,
        42
      ]
    },
    {
      "id": 30,
      "x": -1.225,
      "y": 0.986,
      "layer": 4,
      "closed_by": [
        26,
        35
      ]
    },
    {
      "id": 31,
      "x": -0.289,
      "y": -0.541,
      "layer": 4,
      "closed_by": [
        33,
        51
      ]
    },
    {
      "id": 32,
      "x": 0.291,
      "y": -0.541,
      "layer": 4,
      "closed_by": [
        34,
        51
      ]
    },
    {
      "id": 33,
      "x": -0.507,
      "y": 0.224,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 0.512,
      "y": 0.221,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -1.024,
      "y": 0.224,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -0.261,
      "y": 0.703,
      "layer": 4,
      "closed_by": [
        33,
        51
      ]
    },
    {
      "id": 37,
      "x": 0.27,
      "y": 0.703,
      "layer": 4,
      "closed_by": [
        34,
        51
      ]
    },
    {
      "id": 38,
      "x": 1.588,
      "y": -0.574,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 39,
      "x": -1.585,
      "y": -0.577,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 40,
      "x": 1.628,
      "y": -0.527,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 41,
      "x": -1.631,
      "y": -0.526,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 42,
      "x": 1.588,
      "y": 0.703,
      "layer": 5,
      "closed_by": [
        43
      ]
    },
    {
      "id": 43,
      "x": 1.631,
      "y": 0.652,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 44,
      "x": -1.626,
      "y": 0.652,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 45,
      "x": 1.863,
      "y": 0.462,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 46,
      "x": -1.947,
      "y": 0.363,
      "layer": 2,
      "closed_by": [
        49
      ]
    },
    {
      "id": 47,
      "x": -1.863,
      "y": 0.46,
      "layer": 4,
      "closed_by": [
        26
      ]
    },
    {
      "id": 48,
      "x": 1.904,
      "y": 0.412,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 49,
      "x": -1.906,
      "y": 0.412,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 50,
      "x": 1.947,
      "y": 0.363,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 51,
      "x": 0.003,
      "y": 0.216,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 39,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 22.6,
    "star2": 69.9,
    "star3": 6.5
  }
}