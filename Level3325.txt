{
  "layout_cards": [
    {
      "id": 3,
      "x": 0.002,
      "y": -0.518,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 4,
      "x": 0.004,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        3,
        8
      ]
    },
    {
      "id": 5,
      "x": -1.399,
      "y": 1.039,
      "layer": 2,
      "closed_by": [
        10,
        29
      ]
    },
    {
      "id": 6,
      "x": 1.399,
      "y": -0.597,
      "layer": 2,
      "closed_by": [
        11,
        31
      ]
    },
    {
      "id": 7,
      "x": -1.399,
      "y": -0.597,
      "layer": 2,
      "closed_by": [
        18,
        37
      ]
    },
    {
      "id": 8,
      "x": 0.002,
      "y": 0.967,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 9,
      "x": 1.399,
      "y": 1.039,
      "layer": 2,
      "closed_by": [
        12,
        30
      ]
    },
    {
      "id": 10,
      "x": -1.659,
      "y": 1.039,
      "layer": 3,
      "closed_by": [
        19
      ]
    },
    {
      "id": 11,
      "x": 1.659,
      "y": -0.597,
      "layer": 3,
      "closed_by": [
        20
      ]
    },
    {
      "id": 12,
      "x": 1.659,
      "y": 1.039,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 13,
      "x": -1.539,
      "y": 0.72,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 14,
      "x": 1.539,
      "y": -0.279,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 15,
      "x": 1.539,
      "y": 0.72,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 16,
      "x": -1.659,
      "y": -0.279,
      "layer": 4,
      "closed_by": [
        17
      ]
    },
    {
      "id": 17,
      "x": -1.539,
      "y": -0.279,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 18,
      "x": -1.659,
      "y": -0.597,
      "layer": 3,
      "closed_by": [
        16
      ]
    },
    {
      "id": 19,
      "x": -1.659,
      "y": 0.72,
      "layer": 4,
      "closed_by": [
        13
      ]
    },
    {
      "id": 20,
      "x": 1.659,
      "y": -0.279,
      "layer": 4,
      "closed_by": [
        14
      ]
    },
    {
      "id": 21,
      "x": 1.659,
      "y": 0.72,
      "layer": 4,
      "closed_by": [
        15
      ]
    },
    {
      "id": 22,
      "x": -1.019,
      "y": 0.72,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 1.019,
      "y": 0.72,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 1.019,
      "y": -0.279,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 25,
      "x": -1.019,
      "y": 1.039,
      "layer": 5,
      "closed_by": [
        22
      ]
    },
    {
      "id": 26,
      "x": 1.019,
      "y": 1.039,
      "layer": 5,
      "closed_by": [
        23
      ]
    },
    {
      "id": 27,
      "x": -1.019,
      "y": -0.597,
      "layer": 5,
      "closed_by": [
        38
      ]
    },
    {
      "id": 28,
      "x": 1.019,
      "y": -0.597,
      "layer": 5,
      "closed_by": [
        24
      ]
    },
    {
      "id": 29,
      "x": -1.139,
      "y": 0.72,
      "layer": 4,
      "closed_by": [
        13,
        25
      ]
    },
    {
      "id": 30,
      "x": 1.139,
      "y": 0.72,
      "layer": 4,
      "closed_by": [
        15,
        26
      ]
    },
    {
      "id": 31,
      "x": 1.139,
      "y": -0.279,
      "layer": 4,
      "closed_by": [
        14,
        28
      ]
    },
    {
      "id": 32,
      "x": -0.62,
      "y": -0.279,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 33,
      "x": -0.62,
      "y": 0.72,
      "layer": 4,
      "closed_by": [
        25
      ]
    },
    {
      "id": 34,
      "x": 0.625,
      "y": -0.282,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 35,
      "x": 0.62,
      "y": 0.72,
      "layer": 4,
      "closed_by": [
        26
      ]
    },
    {
      "id": 36,
      "x": 0.799,
      "y": 1.039,
      "layer": 3,
      "closed_by": [
        30,
        35
      ]
    },
    {
      "id": 37,
      "x": -1.139,
      "y": -0.279,
      "layer": 4,
      "closed_by": [
        17,
        27
      ]
    },
    {
      "id": 38,
      "x": -1.019,
      "y": -0.279,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 39,
      "x": 0.777,
      "y": -0.597,
      "layer": 3,
      "closed_by": [
        31,
        34
      ]
    },
    {
      "id": 40,
      "x": 0.337,
      "y": -0.597,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 41,
      "x": 0.337,
      "y": 1.039,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 42,
      "x": 0.337,
      "y": -0.199,
      "layer": 2,
      "closed_by": [
        39,
        44
      ]
    },
    {
      "id": 43,
      "x": 0.337,
      "y": 0.638,
      "layer": 2,
      "closed_by": [
        36,
        44
      ]
    },
    {
      "id": 44,
      "x": 0.337,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        4,
        34,
        35
      ]
    },
    {
      "id": 45,
      "x": -0.777,
      "y": 1.039,
      "layer": 3,
      "closed_by": [
        29,
        33
      ]
    },
    {
      "id": 46,
      "x": -0.337,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        4,
        32,
        33
      ]
    },
    {
      "id": 47,
      "x": -0.337,
      "y": 0.638,
      "layer": 2,
      "closed_by": [
        45,
        46
      ]
    },
    {
      "id": 48,
      "x": -0.799,
      "y": -0.597,
      "layer": 3,
      "closed_by": [
        32,
        37
      ]
    },
    {
      "id": 49,
      "x": -0.337,
      "y": -0.199,
      "layer": 2,
      "closed_by": [
        46,
        48
      ]
    },
    {
      "id": 50,
      "x": -0.337,
      "y": -0.597,
      "layer": 1,
      "closed_by": [
        3,
        49
      ]
    },
    {
      "id": 51,
      "x": -0.337,
      "y": 1.039,
      "layer": 1,
      "closed_by": [
        8,
        47
      ]
    }
  ],
  "cards_in_layout_amount": 49,
  "cards_in_deck_amount": 32,
  "stars_statistic": {
    "star1": 1.2,
    "star2": 34.4,
    "star3": 64.0
  }
}