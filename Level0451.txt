{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.86,
      "y": -0.34,
      "layer": 2,
      "closed_by": [
        49
      ]
    },
    {
      "id": 50,
      "x": 0.391,
      "y": -0.5,
      "layer": 4,
      "closed_by": [
        47
      ]
    },
    {
      "id": 49,
      "x": -0.647,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 48,
      "x": -0.407,
      "y": -0.5,
      "layer": 4,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "y": -0.578,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 46,
      "x": 1.143,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        38,
        33
      ]
    },
    {
      "id": 45,
      "x": -1.118,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        51,
        31
      ]
    },
    {
      "id": 44,
      "x": -0.632,
      "y": 0.828,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 43,
      "x": 0.625,
      "y": 0.828,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 42,
      "x": -0.305,
      "y": 0.912,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 41,
      "y": 1.023,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 40,
      "x": -0.953,
      "y": 0.75,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 39,
      "x": 0.944,
      "y": 0.748,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 38,
      "x": 1.554,
      "y": -0.093,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 37,
      "x": -1.751,
      "y": 0.081,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 36,
      "x": 2.036,
      "y": 0.222,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -1.993,
      "y": 0.221,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 0.307,
      "y": 0.916,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 33,
      "x": 0.887,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 32,
      "x": 0.643,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 31,
      "x": -1.519,
      "y": -0.096,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 30,
      "x": 1.799,
      "y": 0.082,
      "layer": 3,
      "closed_by": [
        36
      ]
    }
  ],
  "cards_in_layout_amount": 22,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 44.7,
    "star2": 5.6,
    "star3": 48.9
  }
}