{
  "layout_cards": [
    {
      "id": 51,
      "x": -1.406,
      "y": 0.994,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 50,
      "x": 1.424,
      "y": -0.549,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 49,
      "x": -0.289,
      "y": 0.74,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 48,
      "x": 0.856,
      "y": -0.293,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 47,
      "x": -0.847,
      "y": 0.75,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 46,
      "x": -0.289,
      "y": 0.994,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 45,
      "x": 0.856,
      "y": -0.546,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 44,
      "x": -0.289,
      "y": 0.486,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 43,
      "x": -1.404,
      "y": 0.739,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 42,
      "x": 1.424,
      "y": -0.296,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 0.856,
      "y": -0.035,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 1.985,
      "y": -0.55,
      "layer": 1,
      "closed_by": []
    },
    {
      "id": 39,
      "x": 0.293,
      "y": -0.039,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 38,
      "x": 0.293,
      "y": -0.55,
      "layer": 1,
      "closed_by": [
        37
      ]
    },
    {
      "id": 37,
      "x": 0.293,
      "y": -0.293,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 36,
      "x": 0.293,
      "y": 0.216,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -0.287,
      "y": 0.224,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -0.847,
      "y": 0.994,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 33,
      "x": -0.847,
      "y": 0.493,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -1.96,
      "y": 0.99,
      "layer": 1,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 20,
  "cards_in_deck_amount": 22,
  "stars_statistic": {
    "star1": 43.1,
    "star2": 2.3,
    "star3": 53.7
  }
}