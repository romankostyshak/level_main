{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.305,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        50,
        49,
        48,
        24
      ]
    },
    {
      "id": 50,
      "x": 0.001,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        34,
        33
      ]
    },
    {
      "id": 49,
      "x": -0.001,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        43,
        42
      ]
    },
    {
      "id": 48,
      "x": 0.546,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        43,
        40
      ]
    },
    {
      "id": 47,
      "x": -0.545,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        39,
        34
      ]
    },
    {
      "id": 46,
      "x": -0.544,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        42,
        38
      ]
    },
    {
      "id": 45,
      "x": 0.865,
      "y": 0.305,
      "layer": 1,
      "closed_by": [
        48,
        24
      ]
    },
    {
      "id": 44,
      "x": -0.865,
      "y": 0.305,
      "layer": 1,
      "closed_by": [
        47,
        46
      ]
    },
    {
      "id": 43,
      "x": 0.305,
      "y": -0.412,
      "layer": 3,
      "closed_by": [
        32,
        29
      ]
    },
    {
      "id": 42,
      "x": -0.303,
      "y": -0.412,
      "layer": 3,
      "closed_by": [
        35,
        27
      ]
    },
    {
      "id": 41,
      "x": 0.865,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 40,
      "x": 0.865,
      "y": -0.414,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 39,
      "x": -0.865,
      "y": 1.018,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 38,
      "x": -0.865,
      "y": -0.412,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 37,
      "x": -0.259,
      "y": 0.72,
      "layer": 4,
      "closed_by": [
        22,
        21
      ]
    },
    {
      "id": 36,
      "x": 0.263,
      "y": 0.72,
      "layer": 4,
      "closed_by": [
        23,
        22
      ]
    },
    {
      "id": 35,
      "x": -0.259,
      "y": -0.1,
      "layer": 4,
      "closed_by": [
        22,
        21
      ]
    },
    {
      "id": 34,
      "x": -0.305,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        37,
        28
      ]
    },
    {
      "id": 33,
      "x": 0.307,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        36,
        30
      ]
    },
    {
      "id": 32,
      "x": 0.259,
      "y": -0.1,
      "layer": 4,
      "closed_by": [
        23,
        22
      ]
    },
    {
      "id": 31,
      "x": -0.305,
      "y": 0.296,
      "layer": 1,
      "closed_by": [
        50,
        49,
        47,
        46
      ]
    },
    {
      "id": 30,
      "x": 0.777,
      "y": 0.722,
      "layer": 4,
      "closed_by": [
        26,
        23
      ]
    },
    {
      "id": 29,
      "x": 0.777,
      "y": -0.096,
      "layer": 4,
      "closed_by": [
        26,
        23
      ]
    },
    {
      "id": 28,
      "x": -0.773,
      "y": 0.72,
      "layer": 4,
      "closed_by": [
        25,
        21
      ]
    },
    {
      "id": 27,
      "x": -0.776,
      "y": -0.097,
      "layer": 4,
      "closed_by": [
        25,
        21
      ]
    },
    {
      "id": 26,
      "x": 1.042,
      "y": 0.3,
      "layer": 5,
      "closed_by": [
        14,
        19
      ]
    },
    {
      "id": 25,
      "x": -1.036,
      "y": 0.298,
      "layer": 5,
      "closed_by": [
        16,
        15
      ]
    },
    {
      "id": 24,
      "x": 0.541,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        41,
        33
      ]
    },
    {
      "id": 23,
      "x": 0.518,
      "y": 0.307,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 22,
      "y": 0.31,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 21,
      "x": -0.518,
      "y": 0.31,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 14,
      "x": 1.299,
      "y": 1.019,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 19,
      "x": 1.299,
      "y": -0.416,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 16,
      "x": -1.297,
      "y": 1.019,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 15,
      "x": -1.297,
      "y": -0.412,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 35,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 50.0,
    "star2": 47.8,
    "star3": 1.6
  }
}