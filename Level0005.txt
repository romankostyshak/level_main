{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.319,
      "y": 0.55,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 50,
      "x": 1.534,
      "y": 1.049,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 49,
      "x": -1.531,
      "y": -0.499,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 48,
      "x": 1.536,
      "y": -0.497,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 47,
      "x": -0.319,
      "y": 0.55,
      "angle": 9.998,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 46,
      "x": -0.374,
      "y": 0.402,
      "angle": 20.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 45,
      "x": 0.372,
      "y": 0.398,
      "angle": 340.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 44,
      "x": -1.536,
      "y": 1.049,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 43,
      "x": -1.595,
      "y": 1.016,
      "angle": 9.998,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 42,
      "x": 1.592,
      "y": 1.014,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 41,
      "x": 1.639,
      "y": 0.967,
      "angle": 340.0,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 40,
      "x": 1.593,
      "y": -0.46,
      "angle": 9.998,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 39,
      "x": 1.644,
      "y": -0.409,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 38,
      "x": -1.639,
      "y": -0.414,
      "angle": 340.0,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 37,
      "x": -1.644,
      "y": 0.971,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 35,
      "x": -1.679,
      "y": 0.912,
      "angle": 30.0,
      "layer": 4,
      "closed_by": [],
      "effect_id": 2
    },
    {
      "id": 34,
      "x": 1.679,
      "y": 0.915,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [],
      "effect_id": 2
    },
    {
      "id": 33,
      "x": -1.59,
      "y": -0.462,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 32,
      "x": -1.679,
      "y": -0.358,
      "angle": 330.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 1.684,
      "y": -0.356,
      "angle": 30.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 36,
      "y": 0.652,
      "layer": 1,
      "closed_by": [
        51,
        47
      ]
    }
  ],
  "cards_in_layout_amount": 21,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 42.4,
    "star2": 1.4,
    "star3": 55.7
  }
}