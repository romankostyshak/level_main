{
  "layout_cards": [
    {
      "id": 51,
      "x": -1.922,
      "y": 0.712,
      "angle": 90.0,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 50,
      "x": 2.072,
      "y": 0.15,
      "angle": 90.0,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 49,
      "x": -0.541,
      "y": 0.962,
      "angle": 25.0,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": 0.537,
      "y": 0.958,
      "angle": 335.0,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "x": 0.483,
      "y": 0.907,
      "angle": 340.0,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 46,
      "x": -0.481,
      "y": 0.91,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 45,
      "x": -0.398,
      "y": 0.708,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 44,
      "x": 0.4,
      "y": 0.703,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 43,
      "x": -0.398,
      "y": -0.187,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 42,
      "x": 0.4,
      "y": -0.187,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 41,
      "x": 0.555,
      "y": -0.486,
      "angle": 25.0,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 40,
      "x": 1.769,
      "y": 0.46,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 39,
      "x": 2.388,
      "y": 0.46,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 38,
      "x": 2.075,
      "y": 0.708,
      "angle": 270.0,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 37,
      "x": -1.919,
      "y": 0.149,
      "angle": 270.0,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 35,
      "x": -1.61,
      "y": 0.46,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 34,
      "x": -0.549,
      "y": -0.488,
      "angle": 335.0,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 33,
      "x": -0.49,
      "y": -0.409,
      "angle": 340.0,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 31,
      "x": 0.49,
      "y": -0.414,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 36,
      "x": -2.229,
      "y": 0.46,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 28,
      "x": -1.371,
      "y": 0.458,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 1.531,
      "y": 0.458,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 32,
      "y": 0.268,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 26,
      "x": 0.546,
      "y": 0.266,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 25,
      "x": -0.546,
      "y": 0.277,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 25,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 55.5,
    "star2": 23.2,
    "star3": 20.7
  }
}