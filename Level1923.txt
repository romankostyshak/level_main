{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.333,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": 0.331,
      "y": -0.093,
      "layer": 2,
      "closed_by": [
        49
      ]
    },
    {
      "id": 49,
      "x": 0.545,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 48,
      "x": 0.87,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "x": 0.869,
      "y": -0.097,
      "layer": 5,
      "closed_by": [
        46
      ]
    },
    {
      "id": 46,
      "x": 0.545,
      "y": -0.017,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 45,
      "x": -0.328,
      "y": -0.093,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": -0.331,
      "y": -0.497,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 43,
      "x": -0.328,
      "y": 0.221,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 42,
      "x": -0.707,
      "y": 0.939,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 41,
      "x": -0.331,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        40,
        31
      ]
    },
    {
      "id": 40,
      "x": 0.064,
      "y": 0.939,
      "layer": 5,
      "closed_by": [
        39
      ]
    },
    {
      "id": 39,
      "x": 0.462,
      "y": 0.938,
      "layer": 6,
      "closed_by": [
        38
      ]
    },
    {
      "id": 38,
      "x": 0.87,
      "y": 1.019,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 37,
      "x": 1.455,
      "y": 0.224,
      "layer": 5,
      "closed_by": [
        36,
        35
      ]
    },
    {
      "id": 36,
      "x": 1.264,
      "y": 0.939,
      "layer": 6,
      "closed_by": [
        38
      ]
    },
    {
      "id": 35,
      "x": 1.827,
      "y": 0.939,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 1.452,
      "y": -0.017,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 33,
      "x": 1.452,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 32,
      "x": 1.452,
      "y": -0.574,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 31,
      "x": -0.708,
      "y": 0.221,
      "layer": 5,
      "closed_by": [
        42
      ]
    },
    {
      "id": 30,
      "x": -1.103,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 27,
      "x": -1.771,
      "y": 0.381,
      "layer": 1,
      "closed_by": [
        26
      ]
    },
    {
      "id": 25,
      "x": -2.226,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 24,
      "x": -1.69,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 21,
      "x": -2.223,
      "y": 0.063,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 22,
      "x": -2.069,
      "y": 0.698,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -1.98,
      "y": 0.558,
      "layer": 2,
      "closed_by": [
        25,
        24,
        21
      ]
    },
    {
      "id": 28,
      "x": -1.424,
      "y": -0.495,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 29,
      "x": -1.1,
      "y": -0.488,
      "layer": 3,
      "closed_by": [
        30
      ]
    }
  ],
  "cards_in_layout_amount": 30,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 44.3,
    "star2": 44.8,
    "star3": 10.9
  }
}