{
  "layout_cards": [
    {
      "id": 51,
      "x": -1.264,
      "y": 0.545,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        37,
        36
      ]
    },
    {
      "id": 50,
      "x": 2.24,
      "y": 0.577,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        49
      ]
    },
    {
      "id": 49,
      "x": 1.886,
      "y": 0.725,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 48,
      "x": 1.253,
      "y": 0.549,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        26,
        25
      ]
    },
    {
      "id": 47,
      "x": -0.601,
      "y": 0.361,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 46,
      "x": -1.906,
      "y": 0.721,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 45,
      "x": -1.651,
      "y": 0.381,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        51,
        46
      ]
    },
    {
      "id": 44,
      "y": 0.165,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 43,
      "x": -0.358,
      "y": 0.004,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        47,
        44
      ]
    },
    {
      "id": 42,
      "x": -0.999,
      "y": 0.202,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        51,
        47
      ]
    },
    {
      "id": 41,
      "x": -1.09,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 40,
      "x": -1.758,
      "y": -0.574,
      "layer": 1,
      "closed_by": []
    },
    {
      "id": 39,
      "x": 2.387,
      "y": -0.574,
      "layer": 1,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -1.151,
      "y": 0.935,
      "angle": 344.997,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 37,
      "x": -0.837,
      "y": 0.684,
      "angle": 344.997,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 36,
      "x": -1.554,
      "y": 0.884,
      "angle": 344.997,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 35,
      "x": 0.36,
      "y": 0.007,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        44,
        27
      ]
    },
    {
      "id": 34,
      "x": -2.403,
      "y": -0.574,
      "layer": 1,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -0.432,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 32,
      "x": 0.432,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 31,
      "x": 1.09,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        23
      ]
    },
    {
      "id": 30,
      "x": 1.745,
      "y": -0.574,
      "layer": 1,
      "closed_by": []
    },
    {
      "id": 29,
      "x": 1.618,
      "y": 0.384,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        49,
        48
      ]
    },
    {
      "id": 28,
      "x": -2.273,
      "y": 0.574,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 27,
      "x": 0.606,
      "y": 0.365,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 26,
      "x": 0.837,
      "y": 0.689,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 25,
      "x": 1.534,
      "y": 0.888,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 24,
      "x": 1.144,
      "y": 0.944,
      "angle": 15.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 0.994,
      "y": 0.202,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        48,
        27
      ]
    }
  ],
  "cards_in_layout_amount": 29,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 45.7,
    "star2": 44.3,
    "star3": 9.2
  }
}