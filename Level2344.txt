{
  "layout_cards": [
    {
      "id": 20,
      "x": -2.065,
      "y": 0.379,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 21,
      "x": -1.906,
      "y": 0.46,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 22,
      "x": 1.906,
      "y": 0.46,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 23,
      "x": -1.743,
      "y": 0.537,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 24,
      "x": 1.748,
      "y": 0.541,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 25,
      "x": -1.582,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 26,
      "x": 1.585,
      "y": 0.615,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 27,
      "x": -0.731,
      "y": 0.944,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 28,
      "x": -0.888,
      "y": 0.861,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 29,
      "x": 0.893,
      "y": 0.86,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 30,
      "x": -1.052,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 31,
      "x": 1.054,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 32,
      "x": -1.223,
      "y": 0.573,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        25,
        30
      ]
    },
    {
      "id": 33,
      "x": 1.223,
      "y": 0.574,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        26,
        31
      ]
    },
    {
      "id": 34,
      "x": 0.035,
      "y": 0.824,
      "angle": 345.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -0.064,
      "y": 0.634,
      "angle": 15.0,
      "layer": 5,
      "closed_by": [
        34
      ]
    },
    {
      "id": 36,
      "x": 0.735,
      "y": 0.939,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 37,
      "x": 2.072,
      "y": 0.379,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 0.028,
      "y": 0.416,
      "angle": 345.0,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 39,
      "x": -0.059,
      "y": 0.18,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 40,
      "x": -0.003,
      "y": -0.335,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 41,
      "x": -0.813,
      "y": -0.136,
      "angle": 30.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 42,
      "x": 0.81,
      "y": -0.136,
      "angle": 330.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 43,
      "x": -0.689,
      "y": -0.298,
      "angle": 20.0,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 44,
      "x": 0.689,
      "y": -0.296,
      "angle": 340.0,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 45,
      "x": -0.546,
      "y": -0.573,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 46,
      "x": 0.601,
      "y": -0.442,
      "angle": 350.0,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 47,
      "x": 0.546,
      "y": -0.577,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": 0.333,
      "y": -0.493,
      "layer": 1,
      "closed_by": [
        40,
        47
      ]
    },
    {
      "id": 49,
      "x": -0.331,
      "y": -0.493,
      "layer": 1,
      "closed_by": [
        40,
        45
      ]
    },
    {
      "id": 50,
      "x": -0.606,
      "y": -0.442,
      "angle": 10.0,
      "layer": 3,
      "closed_by": [
        43
      ]
    }
  ],
  "cards_in_layout_amount": 31,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 32.2,
    "star2": 60.4,
    "star3": 6.7
  }
}