{
  "layout_cards": [
    {
      "x": -2.7,
      "y": -0.379,
      "angle": 345.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 1,
      "x": -2.7,
      "y": 0.819,
      "angle": 15.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 2,
      "y": -0.439,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 3,
      "x": 2.697,
      "y": 0.819,
      "angle": 345.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 4,
      "x": -2.539,
      "y": 0.217,
      "layer": 3,
      "closed_by": [
        0,
        1
      ]
    },
    {
      "id": 5,
      "x": -2.479,
      "y": -0.5,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        4
      ]
    },
    {
      "id": 6,
      "x": 2.539,
      "y": 0.217,
      "layer": 3,
      "closed_by": [
        3,
        9
      ]
    },
    {
      "id": 7,
      "y": 0.879,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 8,
      "x": -2.479,
      "y": 0.939,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        4
      ]
    },
    {
      "id": 9,
      "x": 2.697,
      "y": -0.379,
      "angle": 15.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 10,
      "x": -2.299,
      "y": 0.217,
      "layer": 1,
      "closed_by": [
        5,
        8
      ]
    },
    {
      "id": 11,
      "x": 2.299,
      "y": 0.217,
      "layer": 1,
      "closed_by": [
        12,
        13
      ]
    },
    {
      "id": 12,
      "x": 2.48,
      "y": 0.939,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        6
      ]
    },
    {
      "id": 13,
      "x": 2.48,
      "y": -0.5,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        6
      ]
    },
    {
      "id": 14,
      "x": -0.279,
      "y": -0.194,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 15,
      "x": -0.499,
      "y": -0.497,
      "layer": 1,
      "closed_by": [
        14,
        24,
        39
      ]
    },
    {
      "id": 16,
      "x": 0.5,
      "y": -0.497,
      "layer": 1,
      "closed_by": [
        19,
        22
      ]
    },
    {
      "id": 17,
      "x": 0.5,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        20,
        23
      ]
    },
    {
      "id": 18,
      "x": -0.499,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        21,
        25
      ]
    },
    {
      "id": 19,
      "x": 0.279,
      "y": -0.194,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 20,
      "x": 0.279,
      "y": 0.642,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 21,
      "x": -0.279,
      "y": 0.642,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 22,
      "x": 0.819,
      "y": -0.194,
      "layer": 2,
      "closed_by": [
        31,
        41
      ]
    },
    {
      "id": 23,
      "x": 0.819,
      "y": 0.642,
      "layer": 2,
      "closed_by": [
        26,
        43
      ]
    },
    {
      "id": 24,
      "x": -0.819,
      "y": -0.194,
      "layer": 2,
      "closed_by": [
        32,
        42
      ]
    },
    {
      "id": 25,
      "x": -0.819,
      "y": 0.642,
      "layer": 2,
      "closed_by": [
        27,
        30
      ]
    },
    {
      "id": 26,
      "x": 1.039,
      "y": 0.702,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 27,
      "x": -0.398,
      "y": 0.819,
      "layer": 5,
      "closed_by": [
        7
      ]
    },
    {
      "id": 28,
      "x": -1.258,
      "y": 0.762,
      "layer": 4,
      "closed_by": [
        48
      ]
    },
    {
      "id": 29,
      "y": 0.702,
      "layer": 3,
      "closed_by": [
        38,
        40
      ]
    },
    {
      "id": 30,
      "x": -1.039,
      "y": 0.702,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 31,
      "x": 1.039,
      "y": -0.256,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 32,
      "x": -1.039,
      "y": -0.256,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 33,
      "y": -0.256,
      "layer": 3,
      "closed_by": [
        37,
        39
      ]
    },
    {
      "id": 34,
      "x": 1.258,
      "y": -0.317,
      "layer": 4,
      "closed_by": [
        46
      ]
    },
    {
      "id": 35,
      "x": -1.258,
      "y": -0.317,
      "layer": 4,
      "closed_by": [
        45
      ]
    },
    {
      "id": 36,
      "x": 1.258,
      "y": 0.762,
      "layer": 4,
      "closed_by": [
        44
      ]
    },
    {
      "id": 37,
      "x": 0.279,
      "y": -0.317,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 38,
      "x": 0.279,
      "y": 0.762,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 39,
      "x": -0.279,
      "y": -0.317,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 40,
      "x": -0.279,
      "y": 0.762,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 41,
      "x": 0.4,
      "y": -0.379,
      "layer": 5,
      "closed_by": [
        2
      ]
    },
    {
      "id": 42,
      "x": -0.398,
      "y": -0.379,
      "layer": 5,
      "closed_by": [
        2
      ]
    },
    {
      "id": 43,
      "x": 0.4,
      "y": 0.819,
      "layer": 5,
      "closed_by": [
        7
      ]
    },
    {
      "id": 44,
      "x": 1.338,
      "y": 0.822,
      "angle": 15.0,
      "layer": 5,
      "closed_by": [
        49
      ]
    },
    {
      "id": 45,
      "x": -1.338,
      "y": -0.377,
      "angle": 15.0,
      "layer": 5,
      "closed_by": [
        51
      ]
    },
    {
      "id": 46,
      "x": 1.338,
      "y": -0.377,
      "angle": 344.997,
      "layer": 5,
      "closed_by": [
        50
      ]
    },
    {
      "id": 47,
      "x": -1.639,
      "y": 0.939,
      "angle": 330.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 48,
      "x": -1.338,
      "y": 0.822,
      "angle": 344.997,
      "layer": 5,
      "closed_by": [
        47
      ]
    },
    {
      "id": 49,
      "x": 1.639,
      "y": 0.939,
      "angle": 30.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 50,
      "x": 1.639,
      "y": -0.497,
      "angle": 330.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -1.639,
      "y": -0.497,
      "angle": 30.0,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 52,
  "cards_in_deck_amount": 30,
  "layers_in_level": 6,
  "open_cards": 10,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 0.1,
    "star2": 51.0,
    "star3": 48.1
  }
}