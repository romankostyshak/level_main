{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.521,
      "y": -0.017,
      "layer": 4,
      "closed_by": [
        44
      ]
    },
    {
      "id": 50,
      "x": -0.763,
      "y": -0.017,
      "layer": 3,
      "closed_by": [
        49,
        48,
        24
      ]
    },
    {
      "id": 49,
      "x": -0.5,
      "y": -0.017,
      "layer": 4,
      "closed_by": [
        47
      ]
    },
    {
      "id": 48,
      "x": -1.151,
      "y": 0.409,
      "angle": 335.0,
      "layer": 4,
      "closed_by": [
        45
      ]
    },
    {
      "id": 47,
      "x": -0.375,
      "y": 0.33,
      "angle": 335.0,
      "layer": 5,
      "closed_by": [
        33,
        36
      ]
    },
    {
      "id": 46,
      "x": 1.164,
      "y": 0.414,
      "angle": 25.0,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 45,
      "x": -1.003,
      "y": 0.726,
      "angle": 335.0,
      "layer": 5,
      "closed_by": [
        33
      ]
    },
    {
      "id": 44,
      "x": 0.386,
      "y": 0.333,
      "angle": 25.0,
      "layer": 5,
      "closed_by": [
        37,
        34
      ]
    },
    {
      "id": 43,
      "x": 1.023,
      "y": 0.73,
      "angle": 25.0,
      "layer": 5,
      "closed_by": [
        34
      ]
    },
    {
      "id": 42,
      "x": 0.777,
      "y": -0.017,
      "layer": 3,
      "closed_by": [
        51,
        46,
        25
      ]
    },
    {
      "id": 41,
      "x": 1.034,
      "y": -0.017,
      "layer": 2,
      "closed_by": [
        42,
        27
      ]
    },
    {
      "id": 40,
      "x": 1.345,
      "y": -0.017,
      "layer": 1,
      "closed_by": [
        41
      ]
    },
    {
      "id": 39,
      "x": -1.343,
      "y": -0.017,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 38,
      "x": -1.011,
      "y": -0.017,
      "layer": 2,
      "closed_by": [
        50,
        26
      ]
    },
    {
      "id": 37,
      "x": 0.391,
      "y": -0.128,
      "angle": 335.0,
      "layer": 6,
      "closed_by": [
        29
      ]
    },
    {
      "id": 34,
      "x": 0.842,
      "y": 0.777,
      "layer": 6,
      "closed_by": [
        31
      ]
    },
    {
      "id": 33,
      "x": -0.823,
      "y": 0.782,
      "layer": 6,
      "closed_by": [
        32
      ]
    },
    {
      "id": 31,
      "x": 0.414,
      "y": 0.86,
      "layer": 7,
      "closed_by": [
        28
      ]
    },
    {
      "id": 32,
      "x": -0.414,
      "y": 0.86,
      "layer": 7,
      "closed_by": [
        28
      ]
    },
    {
      "id": 28,
      "x": -0.001,
      "y": 0.939,
      "layer": 8,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -0.377,
      "y": -0.128,
      "angle": 25.0,
      "layer": 6,
      "closed_by": [
        29
      ]
    },
    {
      "id": 29,
      "y": -0.574,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 1.345,
      "y": -0.497,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 26,
      "x": -1.345,
      "y": -0.495,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 25,
      "x": 1.054,
      "y": -0.497,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 24,
      "x": -1.052,
      "y": -0.495,
      "layer": 4,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 26,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 52.4,
    "star2": 10.0,
    "star3": 37.2
  }
}