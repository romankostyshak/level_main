{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.333,
      "y": 1.019,
      "angle": 354.997,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": 0.36,
      "y": 0.845,
      "angle": 5.0,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 49,
      "x": -0.479,
      "y": 0.36,
      "angle": 344.997,
      "layer": 4,
      "closed_by": [
        48
      ]
    },
    {
      "id": 48,
      "x": -0.587,
      "y": 0.114,
      "angle": 340.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 47,
      "x": 0.587,
      "y": 0.104,
      "angle": 20.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 46,
      "x": 0.411,
      "y": 0.601,
      "angle": 9.998,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 45,
      "x": 0.488,
      "y": 0.36,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        47
      ]
    },
    {
      "id": 44,
      "x": -0.458,
      "y": -0.418,
      "angle": 60.0,
      "layer": 4,
      "closed_by": [
        48,
        20
      ]
    },
    {
      "id": 43,
      "y": -0.333,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 42,
      "y": -0.254,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 41,
      "x": 1.161,
      "y": -0.293,
      "angle": 30.0,
      "layer": 4,
      "closed_by": [
        47
      ]
    },
    {
      "id": 40,
      "x": -1.151,
      "y": -0.284,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [
        48
      ]
    },
    {
      "id": 39,
      "x": 1.419,
      "y": -0.111,
      "angle": 40.0,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 38,
      "x": -1.411,
      "y": -0.097,
      "angle": 320.0,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 37,
      "x": -1.644,
      "y": 0.128,
      "angle": 310.0,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 36,
      "x": 1.639,
      "y": 0.112,
      "angle": 50.0,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 35,
      "x": 1.82,
      "y": 0.347,
      "angle": 55.0,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 34,
      "x": -1.82,
      "y": 0.368,
      "angle": 305.0,
      "layer": 1,
      "closed_by": [
        37
      ]
    },
    {
      "id": 33,
      "x": -0.333,
      "y": 1.031,
      "angle": 5.0,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 32,
      "x": -0.361,
      "y": 0.837,
      "angle": 354.997,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 31,
      "x": -0.414,
      "y": 0.601,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        49
      ]
    },
    {
      "id": 30,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        44,
        21
      ]
    },
    {
      "id": 29,
      "x": 1.003,
      "y": 0.995,
      "angle": 349.997,
      "layer": 1,
      "closed_by": [
        27
      ]
    },
    {
      "id": 28,
      "x": -0.99,
      "y": 0.998,
      "angle": 9.998,
      "layer": 1,
      "closed_by": [
        26
      ]
    },
    {
      "id": 27,
      "x": 1.08,
      "y": 0.984,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 26,
      "x": -1.075,
      "y": 0.99,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 25,
      "x": 1.156,
      "y": 0.97,
      "angle": 340.0,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 24,
      "x": -1.156,
      "y": 0.976,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 23,
      "x": 1.235,
      "y": 0.949,
      "angle": 335.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -1.238,
      "y": 0.957,
      "angle": 25.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 21,
      "x": 0.46,
      "y": -0.418,
      "angle": 300.0,
      "layer": 4,
      "closed_by": [
        47,
        20
      ]
    },
    {
      "id": 20,
      "y": -0.572,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 32,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 20.8,
    "star2": 69.3,
    "star3": 9.2
  }
}