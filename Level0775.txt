{
  "layout_cards": [
    {
      "id": 50,
      "x": 2.174,
      "y": 0.298,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 49,
      "x": -0.002,
      "y": -0.178,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 48,
      "x": -2.466,
      "y": -0.172,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 47,
      "x": -2.174,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 46,
      "x": -2.466,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        47,
        45
      ]
    },
    {
      "id": 45,
      "x": -2.466,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": -2.174,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 43,
      "x": -1.345,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 42,
      "x": -1.345,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        43,
        38
      ]
    },
    {
      "id": 41,
      "x": -1.345,
      "y": -0.172,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 40,
      "x": -1.054,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 39,
      "x": -1.054,
      "y": 0.298,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -1.054,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 36,
      "x": -0.001,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 35,
      "x": -0.001,
      "y": 0.379,
      "layer": 2,
      "closed_by": [
        22,
        20,
        51
      ]
    },
    {
      "id": 34,
      "x": 1.054,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 33,
      "x": 1.054,
      "y": 0.298,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 1.054,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 31,
      "x": 1.343,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 30,
      "x": 1.343,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        32,
        31
      ]
    },
    {
      "id": 29,
      "x": 1.343,
      "y": -0.172,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 28,
      "x": 2.174,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 27,
      "x": 2.174,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 26,
      "x": 2.463,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 25,
      "x": 2.463,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        27,
        26
      ]
    },
    {
      "id": 24,
      "x": 2.463,
      "y": -0.172,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 23,
      "x": -2.174,
      "y": 0.319,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 22,
      "x": 0.305,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 21,
      "x": 0.305,
      "y": 0.14,
      "layer": 4,
      "closed_by": [
        49
      ]
    },
    {
      "id": 20,
      "x": -0.001,
      "y": -0.039,
      "layer": 3,
      "closed_by": [
        21,
        37
      ]
    },
    {
      "id": 51,
      "x": -0.307,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 37,
      "x": -0.307,
      "y": 0.136,
      "layer": 4,
      "closed_by": [
        49
      ]
    }
  ],
  "cards_in_layout_amount": 32,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 19.4,
    "star2": 71.3,
    "star3": 8.9
  }
}