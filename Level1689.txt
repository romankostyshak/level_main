{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.333,
      "y": -0.263,
      "layer": 1,
      "closed_by": [
        48,
        39
      ]
    },
    {
      "id": 50,
      "x": -0.333,
      "y": -0.263,
      "layer": 1,
      "closed_by": [
        49,
        40
      ]
    },
    {
      "id": 49,
      "x": -0.411,
      "y": -0.577,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": 0.414,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "x": 0.703,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 46,
      "x": -0.703,
      "y": -0.574,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 45,
      "x": 1.026,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 44,
      "x": -1.026,
      "y": -0.574,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 42,
      "x": -1.105,
      "y": -0.261,
      "layer": 5,
      "closed_by": [
        41
      ]
    },
    {
      "id": 41,
      "x": -1.105,
      "y": -0.017,
      "layer": 6,
      "closed_by": [
        28
      ]
    },
    {
      "id": 40,
      "x": -0.331,
      "y": 0.296,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 39,
      "x": 0.333,
      "y": 0.296,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 38,
      "x": -0.409,
      "y": 0.615,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 37,
      "x": 0.412,
      "y": 0.615,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 36,
      "x": 0.488,
      "y": 0.697,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 35,
      "x": -0.493,
      "y": 0.697,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 34,
      "x": 0.569,
      "y": 0.777,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 33,
      "x": -0.569,
      "y": 0.777,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 31,
      "x": 0.648,
      "y": 0.856,
      "layer": 6,
      "closed_by": [
        29
      ]
    },
    {
      "id": 30,
      "x": -0.648,
      "y": 0.861,
      "layer": 6,
      "closed_by": [
        28
      ]
    },
    {
      "id": 28,
      "x": -1.105,
      "y": 0.699,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 1.105,
      "y": -0.023,
      "layer": 6,
      "closed_by": [
        29
      ]
    },
    {
      "id": 43,
      "x": 1.105,
      "y": -0.263,
      "layer": 5,
      "closed_by": [
        32
      ]
    },
    {
      "id": 29,
      "x": 1.105,
      "y": 0.699,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 1.552,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 23,
      "x": -1.549,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 24,
      "x": 1.552,
      "y": -0.017,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 22,
      "x": -1.552,
      "y": -0.023,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 21,
      "x": 1.547,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        32,
        24
      ]
    },
    {
      "id": 26,
      "x": -1.549,
      "y": 0.702,
      "layer": 2,
      "closed_by": [
        41,
        22
      ]
    },
    {
      "id": 20,
      "x": 1.544,
      "y": 1.023,
      "layer": 1,
      "closed_by": [
        29,
        21
      ]
    },
    {
      "id": 19,
      "x": -1.554,
      "y": 1.018,
      "layer": 1,
      "closed_by": [
        28,
        26
      ]
    }
  ],
  "cards_in_layout_amount": 32,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 36.4,
    "star2": 50.7,
    "star3": 12.2
  }
}