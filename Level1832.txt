{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.333,
      "y": -0.1,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": 0.74,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        49,
        42
      ]
    },
    {
      "id": 49,
      "x": 1.187,
      "y": 0.216,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 48,
      "x": -0.864,
      "y": 0.537,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 47,
      "x": 0.623,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        51,
        46
      ]
    },
    {
      "id": 46,
      "x": 0.865,
      "y": -0.1,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 45,
      "x": -0.624,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        48,
        39
      ]
    },
    {
      "id": 44,
      "x": -1.184,
      "y": 0.216,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 43,
      "x": -0.74,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        44,
        38
      ]
    },
    {
      "id": 42,
      "x": 0.333,
      "y": 0.216,
      "layer": 5,
      "closed_by": [
        37
      ]
    },
    {
      "id": 41,
      "x": 0.333,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 40,
      "x": -0.333,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 39,
      "x": -0.333,
      "y": 0.537,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 38,
      "x": -0.333,
      "y": 0.216,
      "layer": 5,
      "closed_by": [
        37
      ]
    },
    {
      "id": 37,
      "y": 0.216,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 1.774,
      "y": 0.221,
      "layer": 1,
      "closed_by": [
        34
      ]
    },
    {
      "id": 35,
      "x": -1.773,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 34,
      "x": 1.774,
      "y": 0.619,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 33,
      "x": -1.774,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 32,
      "x": 1.774,
      "y": 1.018,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 31,
      "x": -1.771,
      "y": -0.574,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 30,
      "x": 2.144,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        26
      ]
    },
    {
      "id": 29,
      "x": -2.148,
      "y": -0.172,
      "layer": 4,
      "closed_by": [
        25
      ]
    },
    {
      "id": 26,
      "x": 2.147,
      "y": 0.216,
      "layer": 5,
      "closed_by": [
        24
      ]
    },
    {
      "id": 25,
      "x": -2.147,
      "y": 0.216,
      "layer": 5,
      "closed_by": [
        23
      ]
    },
    {
      "id": 24,
      "x": 2.546,
      "y": 0.222,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 23,
      "x": -2.545,
      "y": 0.216,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 27,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 67.5,
    "star2": 14.9,
    "star3": 17.2
  }
}