{
  "layout_cards": [
    {
      "id": 28,
      "x": 0.479,
      "y": 0.859,
      "angle": 300.0,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 29,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        31,
        32
      ]
    },
    {
      "id": 30,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        28,
        33
      ]
    },
    {
      "id": 31,
      "x": 0.418,
      "y": -0.039,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 32,
      "x": -0.418,
      "y": -0.039,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 33,
      "x": -0.479,
      "y": 0.859,
      "angle": 60.0,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 34,
      "x": 0.518,
      "y": -0.059,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 35,
      "x": -0.518,
      "y": -0.059,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 36,
      "x": 0.479,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 37,
      "x": -0.479,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 38,
      "x": 0.479,
      "y": 0.859,
      "angle": 60.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -0.479,
      "y": 0.859,
      "angle": 300.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 0.62,
      "y": 0.017,
      "angle": 60.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 1.878,
      "y": 0.2,
      "angle": 60.0,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 42,
      "x": -0.62,
      "y": 0.017,
      "angle": 300.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 43,
      "x": 1.299,
      "y": 0.859,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 44,
      "x": -1.299,
      "y": 0.859,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 45,
      "x": 1.378,
      "y": 0.238,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 46,
      "x": -1.378,
      "y": 0.238,
      "angle": 344.997,
      "layer": 4,
      "closed_by": [
        44
      ]
    },
    {
      "id": 47,
      "x": 1.48,
      "y": 0.2,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 48,
      "x": -1.478,
      "y": 0.2,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 49,
      "x": -1.878,
      "y": 0.2,
      "angle": 300.0,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 50,
      "x": 2.078,
      "y": -0.158,
      "layer": 1,
      "closed_by": [
        41
      ]
    },
    {
      "id": 51,
      "x": -2.078,
      "y": -0.158,
      "layer": 1,
      "closed_by": [
        49
      ]
    }
  ],
  "cards_in_layout_amount": 24,
  "cards_in_deck_amount": 22,
  "layers_in_level": 5,
  "open_cards": 6,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 40.1,
    "star2": 55.6,
    "star3": 4.0
  }
}