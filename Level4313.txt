{
  "layout_cards": [
    {
      "id": 24,
      "x": -0.298,
      "y": -0.418,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 0.298,
      "y": -0.418,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -0.898,
      "y": 0.898,
      "angle": 345.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 0.898,
      "y": 0.898,
      "angle": 15.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 28,
      "x": -1.139,
      "y": 0.323,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [
        26
      ]
    },
    {
      "id": 29,
      "x": -1.478,
      "y": -0.259,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 30,
      "x": 1.48,
      "y": -0.259,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 31,
      "y": 0.518,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -0.358,
      "y": 0.578,
      "angle": 345.0,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 33,
      "x": 0.36,
      "y": 0.578,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 34,
      "x": -0.759,
      "y": 0.74,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        28,
        32
      ]
    },
    {
      "id": 35,
      "x": 0.759,
      "y": 0.74,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        33,
        36
      ]
    },
    {
      "id": 36,
      "x": 1.139,
      "y": 0.319,
      "angle": 30.0,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 38,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        32,
        33
      ]
    },
    {
      "id": 39,
      "x": 0.017,
      "y": 0.98,
      "angle": 345.0,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 40,
      "x": -0.017,
      "y": 0.98,
      "angle": 15.0,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 41,
      "x": 0.337,
      "y": 0.017,
      "layer": 2,
      "closed_by": [
        25,
        33
      ]
    },
    {
      "id": 42,
      "x": -0.337,
      "y": 0.017,
      "layer": 2,
      "closed_by": [
        24,
        32
      ]
    },
    {
      "id": 43,
      "x": -1.039,
      "y": -0.518,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": -0.898,
      "y": 0.017,
      "layer": 2,
      "closed_by": [
        29,
        34
      ]
    },
    {
      "id": 45,
      "x": -0.298,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 46,
      "x": 0.699,
      "y": 0.36,
      "angle": 60.0,
      "layer": 1,
      "closed_by": [
        41,
        47
      ]
    },
    {
      "id": 47,
      "x": 0.898,
      "y": 0.017,
      "layer": 2,
      "closed_by": [
        30,
        35
      ]
    },
    {
      "id": 48,
      "x": 1.039,
      "y": -0.518,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 49,
      "x": 0.298,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        41
      ]
    },
    {
      "id": 50,
      "x": -0.699,
      "y": 0.36,
      "angle": 300.0,
      "layer": 1,
      "closed_by": [
        42,
        44
      ]
    }
  ],
  "cards_in_layout_amount": 26,
  "cards_in_deck_amount": 26,
  "layers_in_level": 5,
  "open_cards": 5,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 59.2,
    "star2": 38.9,
    "star3": 1.9
  }
}