{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.893,
      "y": -0.017,
      "layer": 1,
      "closed_by": [
        30
      ]
    },
    {
      "id": 50,
      "x": 0.949,
      "y": -0.074,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 45,
      "x": 0.592,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 40,
      "x": -0.25,
      "y": 0.859,
      "layer": 5,
      "closed_by": [
        39
      ]
    },
    {
      "id": 39,
      "x": -0.259,
      "y": 0.142,
      "layer": 6,
      "closed_by": [
        38,
        14
      ]
    },
    {
      "id": 38,
      "x": -0.259,
      "y": -0.574,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -0.79,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 32,
      "x": -0.813,
      "y": -0.017,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 31,
      "x": -0.944,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 30,
      "x": 1.042,
      "y": -0.18,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 29,
      "x": -0.865,
      "y": -0.074,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 28,
      "x": 1.105,
      "y": -0.261,
      "layer": 4,
      "closed_by": [
        26
      ]
    },
    {
      "id": 27,
      "x": -1.029,
      "y": -0.252,
      "layer": 4,
      "closed_by": [
        25
      ]
    },
    {
      "id": 26,
      "x": 1.266,
      "y": -0.421,
      "layer": 5,
      "closed_by": [
        24
      ]
    },
    {
      "id": 25,
      "x": -1.187,
      "y": -0.421,
      "layer": 5,
      "closed_by": [
        23
      ]
    },
    {
      "id": 24,
      "x": 1.151,
      "y": -0.312,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 23,
      "x": -1.072,
      "y": -0.305,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 20,
      "x": -1.59,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        18
      ]
    },
    {
      "id": 41,
      "x": 0.432,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        44
      ]
    },
    {
      "id": 43,
      "x": 0.266,
      "y": 0.136,
      "layer": 6,
      "closed_by": [
        42,
        14
      ]
    },
    {
      "id": 42,
      "x": 0.266,
      "y": -0.578,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 18,
      "x": -1.753,
      "y": 0.859,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -1.429,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        20
      ]
    },
    {
      "id": 33,
      "x": -1.309,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 34,
      "x": -0.949,
      "y": 0.859,
      "layer": 1,
      "closed_by": [
        35,
        33
      ]
    },
    {
      "id": 37,
      "x": -0.61,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 36,
      "x": -0.43,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 44,
      "x": 0.268,
      "y": 0.859,
      "layer": 5,
      "closed_by": [
        43
      ]
    },
    {
      "id": 47,
      "x": 0.753,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 49,
      "x": 1.273,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 46,
      "x": 0.912,
      "y": 0.859,
      "layer": 1,
      "closed_by": [
        47,
        49
      ]
    },
    {
      "id": 21,
      "x": 1.593,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        19
      ]
    },
    {
      "id": 19,
      "x": 1.753,
      "y": 0.859,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 48,
      "x": 1.406,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 14,
      "y": 0.859,
      "layer": 7,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 35,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 17.2,
    "star2": 71.2,
    "star3": 10.6
  }
}