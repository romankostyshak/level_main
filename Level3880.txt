{
  "layout_cards": [
    {
      "id": 5,
      "x": -2.098,
      "y": -0.337,
      "angle": 15.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 6,
      "x": -2.358,
      "y": 0.6,
      "angle": 15.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 9,
      "x": 2.42,
      "y": 0.079,
      "angle": 10.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 10,
      "x": -2.059,
      "y": 0.6,
      "angle": 15.0,
      "layer": 5,
      "closed_by": [
        6
      ]
    },
    {
      "id": 11,
      "x": -2.598,
      "y": 0.46,
      "angle": 15.0,
      "layer": 5,
      "closed_by": [
        6
      ]
    },
    {
      "id": 12,
      "x": -2.378,
      "y": -0.337,
      "angle": 15.0,
      "layer": 5,
      "closed_by": [
        5
      ]
    },
    {
      "id": 13,
      "x": 2.138,
      "y": 0.1,
      "angle": 10.0,
      "layer": 5,
      "closed_by": [
        9
      ]
    },
    {
      "id": 14,
      "x": 2.677,
      "y": 0.2,
      "angle": 10.0,
      "layer": 5,
      "closed_by": [
        9
      ]
    },
    {
      "id": 15,
      "x": 2.0,
      "y": 0.92,
      "angle": 10.0,
      "layer": 5,
      "closed_by": [
        17
      ]
    },
    {
      "id": 16,
      "x": -1.838,
      "y": -0.199,
      "angle": 15.0,
      "layer": 5,
      "closed_by": [
        5
      ]
    },
    {
      "id": 17,
      "x": 2.24,
      "y": 1.039,
      "angle": 10.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 18,
      "x": 2.539,
      "y": 1.019,
      "angle": 10.0,
      "layer": 5,
      "closed_by": [
        17
      ]
    },
    {
      "id": 20,
      "x": 0.079,
      "y": 0.079,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 21,
      "x": -0.039,
      "y": 0.587,
      "angle": 45.0,
      "layer": 1,
      "closed_by": [
        20,
        25,
        26
      ]
    },
    {
      "id": 22,
      "x": 0.36,
      "y": 0.99,
      "angle": 45.0,
      "layer": 1,
      "closed_by": [
        25
      ]
    },
    {
      "id": 23,
      "x": 0.98,
      "y": 0.37,
      "angle": 45.0,
      "layer": 1,
      "closed_by": [
        25,
        41
      ]
    },
    {
      "id": 24,
      "x": 0.578,
      "y": -0.028,
      "angle": 45.0,
      "layer": 1,
      "closed_by": [
        20,
        25,
        38
      ]
    },
    {
      "id": 25,
      "x": 0.479,
      "y": 0.479,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 26,
      "x": -0.537,
      "y": 0.699,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        29,
        34
      ]
    },
    {
      "id": 27,
      "x": -0.319,
      "y": -0.319,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 28,
      "x": -0.939,
      "y": 0.298,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        29,
        44
      ]
    },
    {
      "id": 29,
      "x": -0.418,
      "y": 0.2,
      "angle": 45.0,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 30,
      "x": 0.601,
      "y": 0.56,
      "angle": 35.0,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 31,
      "x": 0.75,
      "y": 0.629,
      "angle": 25.0,
      "layer": 5,
      "closed_by": [
        32
      ]
    },
    {
      "id": 32,
      "x": 0.911,
      "y": 0.67,
      "angle": 15.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -1.94,
      "y": 0.223,
      "angle": 35.0,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 34,
      "x": -1.202,
      "y": 0.805,
      "angle": 25.0,
      "layer": 5,
      "closed_by": [
        42
      ]
    },
    {
      "id": 35,
      "x": -0.541,
      "y": 0.119,
      "angle": 35.0,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 36,
      "x": -0.675,
      "y": 0.061,
      "angle": 25.0,
      "layer": 5,
      "closed_by": [
        37
      ]
    },
    {
      "id": 37,
      "x": -0.833,
      "y": 0.019,
      "angle": 15.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 1.284,
      "y": -0.104,
      "angle": 25.0,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 39,
      "x": 1.139,
      "y": -0.136,
      "angle": 15.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 1.424,
      "y": -0.043,
      "angle": 35.0,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 41,
      "x": 1.547,
      "y": 0.035,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        13,
        40
      ]
    },
    {
      "id": 42,
      "x": -1.059,
      "y": 0.837,
      "angle": 15.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 43,
      "x": -1.332,
      "y": 0.754,
      "angle": 35.0,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 44,
      "x": -1.45,
      "y": 0.68,
      "angle": 45.0,
      "layer": 3,
      "closed_by": [
        10,
        43
      ]
    },
    {
      "id": 45,
      "x": 1.919,
      "y": 0.398,
      "angle": 45.0,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 46,
      "x": 2.029,
      "y": 0.469,
      "angle": 35.0,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "x": 2.157,
      "y": 0.518,
      "angle": 25.0,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 48,
      "x": 2.312,
      "y": 0.546,
      "angle": 10.0,
      "layer": 4,
      "closed_by": [
        13,
        14,
        15,
        18
      ]
    },
    {
      "id": 49,
      "x": -1.819,
      "y": 0.298,
      "angle": 45.0,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 50,
      "x": -2.078,
      "y": 0.165,
      "angle": 25.0,
      "layer": 3,
      "closed_by": [
        51
      ]
    },
    {
      "id": 51,
      "x": -2.223,
      "y": 0.129,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        10,
        11,
        12,
        16
      ]
    }
  ],
  "cards_in_layout_amount": 44,
  "cards_in_deck_amount": 28,
  "layers_in_level": 6,
  "open_cards": 8,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 7.1,
    "star2": 75.2,
    "star3": 16.9
  }
}