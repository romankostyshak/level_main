{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.412,
      "y": 0.303,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 50,
      "x": -1.514,
      "y": 0.953,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 49,
      "x": -1.598,
      "y": 0.856,
      "angle": 9.998,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": -2.367,
      "y": 0.855,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 47,
      "x": -2.457,
      "y": 0.953,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 46,
      "x": -1.99,
      "y": 0.786,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 45,
      "x": 2.466,
      "y": 0.972,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        27
      ]
    },
    {
      "id": 44,
      "x": 1.524,
      "y": -0.363,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        28
      ]
    },
    {
      "id": 43,
      "x": -2.459,
      "y": -0.368,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        23
      ]
    },
    {
      "id": 42,
      "x": -1.61,
      "y": -0.259,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 41,
      "x": 2.375,
      "y": -0.259,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 40,
      "x": 2.0,
      "y": -0.18,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 39,
      "x": -1.991,
      "y": 0.312,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -0.508,
      "y": 0.221,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 37,
      "x": -0.5,
      "y": 0.384,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -0.412,
      "y": 0.303,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 35,
      "x": 2.467,
      "y": -0.363,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        41
      ]
    },
    {
      "id": 34,
      "x": -1.514,
      "y": -0.363,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 33,
      "x": 0.5,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 32,
      "x": 0.497,
      "y": 0.381,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 0.583,
      "y": 0.319,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 30,
      "x": -0.583,
      "y": 0.319,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 29,
      "x": 2.003,
      "y": 0.298,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 1.615,
      "y": -0.263,
      "angle": 9.998,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 27,
      "x": 2.375,
      "y": 0.861,
      "angle": 9.998,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 26,
      "x": 1.531,
      "y": 0.966,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        25
      ]
    },
    {
      "id": 25,
      "x": 1.618,
      "y": 0.861,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 24,
      "x": 2.0,
      "y": 0.794,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 23,
      "x": -2.368,
      "y": -0.259,
      "angle": 9.998,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 22,
      "x": -1.99,
      "y": -0.177,
      "layer": 3,
      "closed_by": [
        39
      ]
    }
  ],
  "cards_in_layout_amount": 30,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 24.9,
    "star2": 68.2,
    "star3": 6.0
  }
}