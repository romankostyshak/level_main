{
  "layout_cards": [
    {
      "id": 47,
      "x": 0.611,
      "y": 0.31,
      "layer": 2,
      "closed_by": [
        44,
        43
      ]
    },
    {
      "id": 46,
      "x": 1.166,
      "y": 0.31,
      "layer": 2,
      "closed_by": [
        43,
        42
      ]
    },
    {
      "id": 45,
      "x": 1.725,
      "y": 0.314,
      "layer": 2,
      "closed_by": [
        42,
        41
      ]
    },
    {
      "id": 39,
      "x": -0.601,
      "y": 0.865,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 36,
      "x": -1.434,
      "y": -0.177,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 35,
      "x": -1.758,
      "y": 0.712,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 34,
      "x": -1.756,
      "y": -0.104,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 31,
      "x": -0.372,
      "y": 0.865,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -1.144,
      "y": 0.865,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 29,
      "x": -1.144,
      "y": -0.256,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 28,
      "x": -0.875,
      "y": 0.865,
      "layer": 1,
      "closed_by": [
        39,
        30
      ]
    },
    {
      "id": 27,
      "x": -0.875,
      "y": -0.254,
      "layer": 1,
      "closed_by": [
        29,
        38
      ]
    },
    {
      "id": 26,
      "x": -0.374,
      "y": -0.254,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 37,
      "x": -1.434,
      "y": 0.785,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 33,
      "x": -1.904,
      "y": 0.303,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 51,
      "x": 0.333,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 44,
      "x": 0.333,
      "y": 0.859,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 43,
      "x": 0.892,
      "y": 0.859,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 42,
      "x": 1.452,
      "y": 0.859,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 2.015,
      "y": 0.859,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 48,
      "x": 2.015,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 49,
      "x": 1.452,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        46,
        45
      ]
    },
    {
      "id": 50,
      "x": 0.893,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        47,
        46
      ]
    },
    {
      "id": 40,
      "x": -0.335,
      "y": -0.254,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 32,
      "x": -0.331,
      "y": 0.865,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 38,
      "x": -0.597,
      "y": -0.254,
      "layer": 2,
      "closed_by": [
        40
      ]
    }
  ],
  "cards_in_layout_amount": 26,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 57.9,
    "star2": 23.4,
    "star3": 18.3
  }
}