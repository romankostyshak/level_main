{
  "layout_cards": [
    {
      "id": 49,
      "y": 0.215,
      "layer": 1,
      "closed_by": [
        48,
        47
      ]
    },
    {
      "id": 48,
      "x": 0.001,
      "y": 0.697,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 47,
      "x": 0.001,
      "y": -0.256,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 46,
      "x": 0.002,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        44,
        50
      ]
    },
    {
      "id": 45,
      "y": -0.497,
      "layer": 3,
      "closed_by": [
        43,
        42
      ]
    },
    {
      "id": 44,
      "x": -0.342,
      "y": 0.984,
      "angle": 14.998,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 50,
      "x": 0.351,
      "y": 0.981,
      "angle": 345.0,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 43,
      "x": 0.344,
      "y": -0.536,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 42,
      "x": -0.331,
      "y": -0.537,
      "angle": 345.0,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 41,
      "x": 0.61,
      "y": 0.912,
      "angle": 345.0,
      "layer": 5,
      "closed_by": [
        35
      ]
    },
    {
      "id": 40,
      "x": -0.597,
      "y": 0.916,
      "angle": 15.0,
      "layer": 5,
      "closed_by": [
        34
      ]
    },
    {
      "id": 39,
      "x": 0.597,
      "y": -0.467,
      "angle": 15.0,
      "layer": 5,
      "closed_by": [
        35
      ]
    },
    {
      "id": 38,
      "x": -0.583,
      "y": -0.469,
      "angle": 345.0,
      "layer": 5,
      "closed_by": [
        34
      ]
    },
    {
      "id": 35,
      "x": 0.573,
      "y": 0.216,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -0.569,
      "y": 0.224,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 1.131,
      "y": 0.216,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -1.128,
      "y": 0.216,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 1.235,
      "y": 0.31,
      "angle": 20.0,
      "layer": 5,
      "closed_by": [
        32
      ]
    },
    {
      "id": 29,
      "x": -1.238,
      "y": 0.303,
      "angle": 340.0,
      "layer": 5,
      "closed_by": [
        33
      ]
    },
    {
      "id": 28,
      "x": -1.281,
      "y": 0.351,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 30,
      "x": 1.276,
      "y": 0.363,
      "angle": 30.0,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 36,
      "x": 1.299,
      "y": 0.421,
      "angle": 40.0,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 27,
      "x": -1.299,
      "y": 0.412,
      "angle": 320.0,
      "layer": 3,
      "closed_by": [
        28
      ]
    }
  ],
  "cards_in_layout_amount": 23,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 58.6,
    "star2": 4.3,
    "star3": 36.4
  }
}