{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.119,
      "y": 0.035,
      "angle": 344.997,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": 0.142,
      "y": 0.606,
      "angle": 9.998,
      "layer": 2,
      "closed_by": [
        49
      ]
    },
    {
      "id": 49,
      "x": -0.252,
      "y": 0.141,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 48,
      "x": -0.178,
      "y": 0.55,
      "angle": 10.0,
      "layer": 4,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "x": -0.625,
      "y": 0.238,
      "angle": 344.997,
      "layer": 5,
      "closed_by": [
        46
      ]
    },
    {
      "id": 46,
      "x": -0.479,
      "y": 0.5,
      "angle": 10.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 45,
      "x": 0.642,
      "y": -0.115,
      "angle": 344.997,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 44,
      "x": -1.12,
      "y": 0.375,
      "angle": 344.997,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 43,
      "x": -1.151,
      "y": 0.432,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 42,
      "x": -1.182,
      "y": 0.493,
      "angle": 344.997,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 0.669,
      "y": 0.702,
      "angle": 9.998,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 40,
      "x": 0.722,
      "y": 0.493,
      "layer": 2,
      "closed_by": [
        41,
        39
      ]
    },
    {
      "id": 39,
      "x": 0.79,
      "y": -0.158,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 38,
      "x": 0.93,
      "y": 0.748,
      "angle": 9.998,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 37,
      "x": 0.907,
      "y": -0.193,
      "angle": 344.997,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 36,
      "x": 1.031,
      "y": 0.279,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 1.669,
      "y": 0.303,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 1.61,
      "y": 0.745,
      "angle": 9.998,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 33,
      "x": 1.59,
      "y": -0.134,
      "angle": 344.997,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 32,
      "x": 1.738,
      "y": -0.411,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 31,
      "x": 1.764,
      "y": 0.911,
      "angle": 10.0,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 30,
      "x": 1.824,
      "y": 0.266,
      "layer": 2,
      "closed_by": [
        32,
        31
      ]
    },
    {
      "id": 29,
      "x": -1.855,
      "y": 0.141,
      "layer": 1,
      "closed_by": [
        28,
        27
      ]
    },
    {
      "id": 28,
      "x": -1.853,
      "y": -0.574,
      "layer": 2,
      "closed_by": [
        26
      ]
    },
    {
      "id": 27,
      "x": -1.901,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        26,
        22
      ]
    },
    {
      "id": 26,
      "x": -1.901,
      "y": -0.017,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 25,
      "x": -1.901,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 24,
      "x": -1.983,
      "y": 0.143,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 23,
      "x": -1.85,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        27
      ]
    },
    {
      "id": 22,
      "x": -1.906,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 21,
      "x": -1.985,
      "y": 1.019,
      "layer": 4,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 31,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 34.4,
    "star2": 56.8,
    "star3": 8.1
  }
}