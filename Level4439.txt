{
  "layout_cards": [
    {
      "id": 13,
      "x": 1.258,
      "y": 0.538,
      "angle": 45.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 14,
      "x": -1.251,
      "y": 0.082,
      "angle": 45.0,
      "layer": 5,
      "closed_by": [],
      "card_value": "",
      "effect_id": 0,
      "card_type": 0
    },
    {
      "id": 15,
      "x": 1.779,
      "y": -0.439,
      "layer": 5,
      "closed_by": [],
      "card_value": "",
      "effect_id": 0,
      "card_type": 0
    },
    {
      "id": 18,
      "x": -1.72,
      "y": 1.039,
      "layer": 5,
      "closed_by": [],
      "card_value": "",
      "effect_id": 0,
      "card_type": 0
    },
    {
      "id": 19,
      "x": -1.978,
      "y": 0.799,
      "layer": 4,
      "closed_by": [
        18
      ]
    },
    {
      "id": 20,
      "x": 2.019,
      "y": -0.199,
      "layer": 4,
      "closed_by": [
        15
      ]
    },
    {
      "id": 21,
      "x": 2.178,
      "y": 0.2,
      "layer": 3,
      "closed_by": [
        20
      ]
    },
    {
      "id": 22,
      "x": -2.24,
      "layer": 2,
      "closed_by": [
        51
      ]
    },
    {
      "id": 23,
      "x": 2.279,
      "y": 0.6,
      "layer": 2,
      "closed_by": [
        21
      ]
    },
    {
      "id": 24,
      "x": -2.14,
      "y": -0.398,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 25,
      "x": 2.158,
      "y": 1.0,
      "layer": 1,
      "closed_by": [
        23
      ]
    },
    {
      "id": 26,
      "x": -1.58,
      "y": -0.398,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 27,
      "x": 1.598,
      "y": 1.0,
      "layer": 1,
      "closed_by": [
        28
      ]
    },
    {
      "id": 28,
      "x": 1.506,
      "y": 0.931,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 29,
      "x": -1.434,
      "y": -0.233,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 30,
      "x": 1.442,
      "y": 0.847,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 31,
      "x": 1.406,
      "y": 0.749,
      "angle": 45.0,
      "layer": 4,
      "closed_by": [
        13
      ]
    },
    {
      "id": 32,
      "x": -1.404,
      "y": -0.129,
      "angle": 45.0,
      "layer": 4,
      "closed_by": [
        14
      ]
    },
    {
      "id": 33,
      "x": -1.042,
      "y": 0.238,
      "angle": 45.0,
      "layer": 4,
      "closed_by": [
        14
      ]
    },
    {
      "id": 34,
      "x": -0.458,
      "y": -0.338,
      "angle": 45.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -0.833,
      "y": 0.469,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 36,
      "x": -1.493,
      "y": -0.319,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 38,
      "x": -0.418,
      "y": -0.239,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        33,
        34
      ]
    },
    {
      "id": 39,
      "x": -0.575,
      "y": 0.633,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 40,
      "x": -0.358,
      "y": -0.158,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        35,
        38
      ]
    },
    {
      "id": 41,
      "x": -0.279,
      "y": -0.1,
      "layer": 1,
      "closed_by": [
        39,
        40
      ]
    },
    {
      "id": 42,
      "x": -0.279,
      "y": 0.72,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 43,
      "x": 1.039,
      "y": 0.379,
      "angle": 45.0,
      "layer": 4,
      "closed_by": [
        13
      ]
    },
    {
      "id": 44,
      "x": 0.46,
      "y": 0.959,
      "angle": 45.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 45,
      "x": 0.832,
      "y": 0.151,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 46,
      "x": 0.36,
      "y": 0.778,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        45,
        50
      ]
    },
    {
      "id": 47,
      "x": 0.572,
      "y": -0.014,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 48,
      "x": 0.279,
      "y": -0.1,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 49,
      "x": 0.279,
      "y": 0.72,
      "layer": 1,
      "closed_by": [
        46,
        47
      ]
    },
    {
      "id": 50,
      "x": 0.419,
      "y": 0.859,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        43,
        44
      ]
    },
    {
      "id": 51,
      "x": -2.14,
      "y": 0.398,
      "layer": 3,
      "closed_by": [
        19
      ],
      "card_value": "",
      "effect_id": 0,
      "card_type": 0
    }
  ],
  "free_cards_from_stickers": [],
  "cards_in_layout_amount": 36,
  "cards_in_deck_amount": 26,
  "layers_in_level": 5,
  "open_cards": 6,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 8.7,
    "star2": 78.6,
    "star3": 11.5
  }
}