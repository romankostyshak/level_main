{
  "layout_cards": [
    {
      "id": 51,
      "x": 1.786,
      "y": -0.136,
      "layer": 2,
      "closed_by": [
        36,
        35
      ]
    },
    {
      "id": 50,
      "x": -0.418,
      "y": 0.319,
      "layer": 1,
      "closed_by": [
        47,
        45,
        33
      ]
    },
    {
      "id": 49,
      "x": 0.34,
      "y": -0.279,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 48,
      "x": -0.335,
      "y": -0.279,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 47,
      "x": -0.518,
      "y": 0.319,
      "layer": 2,
      "closed_by": [
        48,
        24
      ]
    },
    {
      "id": 46,
      "x": 0.518,
      "y": 0.319,
      "layer": 2,
      "closed_by": [
        49,
        23
      ]
    },
    {
      "id": 45,
      "y": 0.782,
      "layer": 2,
      "closed_by": [
        24,
        23
      ]
    },
    {
      "id": 44,
      "x": -1.804,
      "y": -0.136,
      "layer": 2,
      "closed_by": [
        31,
        30
      ]
    },
    {
      "id": 43,
      "x": 1.794,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        18,
        17
      ]
    },
    {
      "id": 42,
      "x": -1.804,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        28,
        27
      ]
    },
    {
      "id": 41,
      "x": 0.418,
      "y": 0.319,
      "layer": 1,
      "closed_by": [
        46,
        45,
        33
      ]
    },
    {
      "id": 40,
      "x": 2.213,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        51,
        43,
        19
      ],
      "card_type": 5
    },
    {
      "id": 39,
      "x": -1.386,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        44,
        42,
        26
      ],
      "card_type": 5
    },
    {
      "id": 38,
      "x": -2.186,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        44,
        42,
        25
      ],
      "card_type": 3
    },
    {
      "id": 37,
      "x": 2.121,
      "y": 0.298,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 1.468,
      "y": -0.319,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 35,
      "x": 2.121,
      "y": -0.319,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 33,
      "y": -0.112,
      "layer": 2,
      "closed_by": [
        49,
        48
      ]
    },
    {
      "id": 32,
      "x": -2.121,
      "y": 0.298,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -2.121,
      "y": -0.319,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 30,
      "x": -1.478,
      "y": -0.319,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 29,
      "x": -1.478,
      "y": 0.298,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 28,
      "x": -1.48,
      "y": 0.879,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 27,
      "x": -2.121,
      "y": 0.879,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 26,
      "x": -1.213,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        30,
        28
      ]
    },
    {
      "id": 25,
      "x": -2.384,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        31,
        27
      ]
    },
    {
      "id": 24,
      "x": -0.335,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 23,
      "x": 0.337,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 22,
      "x": 1.394,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        51,
        43,
        20
      ],
      "card_type": 5
    },
    {
      "id": 21,
      "x": 1.47,
      "y": 0.298,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 20,
      "x": 1.215,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        36,
        17
      ]
    },
    {
      "id": 19,
      "x": 2.388,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        35,
        18
      ]
    },
    {
      "id": 18,
      "x": 2.121,
      "y": 0.879,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 17,
      "x": 1.47,
      "y": 0.879,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 34,
      "y": 0.34,
      "layer": 4,
      "closed_by": [],
      "card_type": 2
    }
  ],
  "cards_in_layout_amount": 35,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 56.4,
    "star2": 22.7,
    "star3": 20.6
  }
}