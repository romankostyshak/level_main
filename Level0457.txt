{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.335,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 50,
      "x": 0.962,
      "y": -0.574,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 49,
      "x": 0.642,
      "y": -0.574,
      "layer": 2,
      "closed_by": [
        50,
        37
      ]
    },
    {
      "id": 48,
      "x": 0.643,
      "y": -0.259,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 47,
      "x": -0.328,
      "y": -0.158,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 46,
      "x": -0.305,
      "y": 0.958,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 45,
      "x": 0.268,
      "y": 0.819,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": 0.314,
      "y": 0.958,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 43,
      "x": -0.259,
      "y": 0.819,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 42,
      "x": 1.741,
      "y": 0.379,
      "layer": 1,
      "closed_by": [
        40,
        41
      ]
    },
    {
      "id": 40,
      "x": 1.493,
      "y": 0.379,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 39,
      "x": -1.74,
      "y": 0.379,
      "layer": 3,
      "closed_by": [],
      "effect_id": 4
    },
    {
      "id": 38,
      "x": -1.74,
      "y": 0.379,
      "layer": 1,
      "closed_by": [
        32,
        28
      ]
    },
    {
      "id": 37,
      "x": 0.333,
      "y": -0.158,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 36,
      "x": -0.962,
      "y": -0.574,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 35,
      "x": -0.638,
      "y": -0.574,
      "layer": 2,
      "closed_by": [
        47,
        36
      ]
    },
    {
      "id": 34,
      "x": -0.331,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 33,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        45,
        43
      ]
    },
    {
      "id": 32,
      "x": -2.003,
      "y": 0.379,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 31,
      "x": -1.215,
      "y": 0.379,
      "layer": 1,
      "closed_by": [
        28
      ]
    },
    {
      "id": 30,
      "x": 1.223,
      "y": 0.379,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 25,
      "x": 1.758,
      "y": 0.379,
      "layer": 3,
      "closed_by": [],
      "effect_id": 4
    },
    {
      "id": 27,
      "x": -0.564,
      "y": 0.958,
      "layer": 4,
      "closed_by": [],
      "effect_id": 4
    },
    {
      "id": 26,
      "x": 0.574,
      "y": 0.958,
      "layer": 4,
      "closed_by": [],
      "effect_id": 4
    },
    {
      "id": 29,
      "x": -0.634,
      "y": -0.259,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 28,
      "x": -1.478,
      "y": 0.379,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 41,
      "x": 2.026,
      "y": 0.379,
      "layer": 2,
      "closed_by": [
        25
      ]
    }
  ],
  "cards_in_layout_amount": 27,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 60.0,
    "star2": 30.0,
    "star3": 9.0
  }
}