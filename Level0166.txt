{
  "layout_cards": [
    {
      "id": 51,
      "x": -1.557,
      "y": 1.039,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 50,
      "x": -1.559,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        41,
        36
      ]
    },
    {
      "id": 49,
      "x": 1.559,
      "y": 1.039,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 48,
      "x": 1.564,
      "y": 0.222,
      "layer": 2,
      "closed_by": [
        45,
        37
      ]
    },
    {
      "id": 47,
      "x": 1.565,
      "y": -0.597,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 46,
      "x": 1.646,
      "y": -0.337,
      "angle": 344.997,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 45,
      "x": 1.623,
      "y": 0.777,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": 1.478,
      "y": 0.679,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 43,
      "x": -1.559,
      "y": -0.597,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 42,
      "x": 0.554,
      "y": 0.578,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        34
      ]
    },
    {
      "id": 41,
      "x": -1.46,
      "y": -0.238,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 40,
      "x": -1.639,
      "y": -0.337,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 39,
      "x": -1.876,
      "y": 0.303,
      "angle": 24.999,
      "layer": 5,
      "closed_by": [
        28
      ]
    },
    {
      "id": 38,
      "x": 1.878,
      "y": 0.298,
      "angle": 335.0,
      "layer": 5,
      "closed_by": [
        29
      ]
    },
    {
      "id": 37,
      "x": 1.463,
      "y": -0.238,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 36,
      "x": -1.618,
      "y": 0.777,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 35,
      "x": -1.478,
      "y": 0.675,
      "angle": 344.997,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 34,
      "x": 0.648,
      "y": 0.537,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 33,
      "x": 0.809,
      "y": 0.518,
      "angle": 15.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -0.804,
      "y": 0.518,
      "angle": 344.997,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -0.643,
      "y": 0.535,
      "angle": 340.0,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 30,
      "x": -0.55,
      "y": 0.578,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 29,
      "x": 2.124,
      "y": 0.822,
      "angle": 335.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 28,
      "x": -2.151,
      "y": 0.827,
      "angle": 30.0,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 24,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 58.7,
    "star2": 8.5,
    "star3": 32.7
  }
}