{
  "layout_cards": [
    {
      "id": 51,
      "x": -1.184,
      "y": 0.805,
      "layer": 2,
      "closed_by": [
        29,
        28
      ]
    },
    {
      "id": 50,
      "x": 1.197,
      "y": -0.177,
      "layer": 2,
      "closed_by": [
        31,
        30
      ]
    },
    {
      "id": 49,
      "x": 1.174,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        42,
        41
      ]
    },
    {
      "id": 48,
      "x": -1.179,
      "y": -0.165,
      "layer": 2,
      "closed_by": [
        43,
        40
      ]
    },
    {
      "id": 47,
      "x": -1.182,
      "y": 0.8,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 46,
      "x": 1.177,
      "y": 0.782,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 45,
      "x": -1.179,
      "y": -0.164,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 44,
      "x": 1.2,
      "y": -0.177,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 43,
      "x": -0.648,
      "y": -0.331,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 42,
      "x": 1.707,
      "y": 0.981,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 41,
      "x": 0.637,
      "y": 0.986,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 40,
      "x": -1.74,
      "y": -0.333,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 39,
      "x": 0.717,
      "y": -0.314,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 38,
      "x": 1.679,
      "y": -0.305,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 37,
      "x": 1.636,
      "y": 0.958,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 36,
      "x": -0.707,
      "y": 0.953,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        51
      ]
    },
    {
      "id": 35,
      "x": -1.654,
      "y": 0.958,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        51
      ]
    },
    {
      "id": 34,
      "x": -1.659,
      "y": -0.312,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 33,
      "x": -0.722,
      "y": -0.303,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 32,
      "x": 0.712,
      "y": 0.967,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 31,
      "x": 0.646,
      "y": -0.333,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        44
      ],
      "effect_id": 2
    },
    {
      "id": 30,
      "x": 1.753,
      "y": -0.33,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        44
      ],
      "effect_id": 1
    },
    {
      "id": 29,
      "x": -0.637,
      "y": 0.981,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        47
      ],
      "effect_id": 2
    },
    {
      "id": 28,
      "x": -1.73,
      "y": 0.98,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        47
      ],
      "effect_id": 1
    }
  ],
  "cards_in_layout_amount": 24,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 51.0,
    "star2": 3.4,
    "star3": 45.3
  }
}