{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.442,
      "y": -0.305,
      "angle": 35.0,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 50,
      "x": -0.439,
      "y": -0.307,
      "angle": 325.0,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 49,
      "x": 0.638,
      "y": -0.298,
      "angle": 35.0,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 48,
      "x": -0.638,
      "y": -0.307,
      "angle": 325.0,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 47,
      "x": 0.845,
      "y": -0.303,
      "angle": 35.0,
      "layer": 3,
      "closed_by": [
        42,
        40
      ]
    },
    {
      "id": 46,
      "x": -0.837,
      "y": -0.307,
      "angle": 325.0,
      "layer": 3,
      "closed_by": [
        45,
        41
      ]
    },
    {
      "id": 45,
      "x": -1.034,
      "y": -0.31,
      "angle": 325.0,
      "layer": 4,
      "closed_by": [
        43,
        38
      ]
    },
    {
      "id": 44,
      "x": 1.279,
      "y": -0.3,
      "angle": 35.0,
      "layer": 5,
      "closed_by": [
        37
      ]
    },
    {
      "id": 43,
      "x": -1.245,
      "y": -0.305,
      "angle": 325.0,
      "layer": 5,
      "closed_by": [
        36
      ]
    },
    {
      "id": 42,
      "x": 1.044,
      "y": -0.298,
      "angle": 35.0,
      "layer": 4,
      "closed_by": [
        44,
        39
      ]
    },
    {
      "id": 41,
      "x": -0.523,
      "y": -0.527,
      "angle": 325.0,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 40,
      "x": 0.523,
      "y": -0.527,
      "angle": 35.0,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 39,
      "x": 0.717,
      "y": -0.526,
      "angle": 35.0,
      "layer": 5,
      "closed_by": [
        37
      ]
    },
    {
      "id": 38,
      "x": -0.717,
      "y": -0.531,
      "angle": 325.0,
      "layer": 5,
      "closed_by": [
        36
      ]
    },
    {
      "id": 37,
      "x": 0.953,
      "y": -0.527,
      "angle": 35.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -0.925,
      "y": -0.527,
      "angle": 325.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -1.343,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 29,
      "x": -0.546,
      "y": 0.62,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 28,
      "x": -0.305,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        15
      ]
    },
    {
      "id": 27,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        28,
        26
      ]
    },
    {
      "id": 26,
      "x": 0.305,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        16
      ]
    },
    {
      "id": 25,
      "x": 0.544,
      "y": 0.62,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 22,
      "x": 1.347,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 18,
      "x": 1.988,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 16,
      "x": 0.305,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 15,
      "x": -0.305,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 23,
      "x": 1.347,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 21,
      "x": 1.659,
      "y": 1.019,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 20,
      "x": 1.988,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 19,
      "x": 2.226,
      "y": 0.62,
      "layer": 1,
      "closed_by": [
        18
      ]
    },
    {
      "id": 34,
      "x": -1.985,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 35,
      "x": -1.985,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 33,
      "x": -1.659,
      "y": 1.019,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 1.133,
      "y": 0.62,
      "layer": 1,
      "closed_by": [
        23
      ]
    },
    {
      "id": 32,
      "x": -1.343,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 30,
      "x": -1.133,
      "y": 0.62,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 17,
      "x": -2.226,
      "y": 0.62,
      "layer": 1,
      "closed_by": [
        34
      ]
    }
  ],
  "cards_in_layout_amount": 37,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 5.9,
    "star2": 57.7,
    "star3": 35.4
  }
}