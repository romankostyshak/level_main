{
  "layout_cards": [
    {
      "id": 49,
      "x": -0.004,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        48,
        47
      ]
    },
    {
      "id": 48,
      "x": 0.307,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        45,
        43
      ]
    },
    {
      "id": 47,
      "x": -0.303,
      "y": 0.3,
      "layer": 2,
      "closed_by": [
        46,
        42
      ]
    },
    {
      "id": 46,
      "x": -0.386,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 45,
      "x": 0.386,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": 0.703,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 44,
      "x": -0.703,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 43,
      "x": 0.307,
      "y": 0.061,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 42,
      "x": -0.303,
      "y": 0.061,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 41,
      "x": 0.31,
      "y": -0.172,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 40,
      "x": -0.303,
      "y": -0.172,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 39,
      "x": -0.305,
      "y": -0.414,
      "layer": 5,
      "closed_by": [
        32
      ]
    },
    {
      "id": 38,
      "x": 0.31,
      "y": -0.412,
      "layer": 5,
      "closed_by": [
        33
      ]
    },
    {
      "id": 35,
      "x": 1.024,
      "y": 1.019,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 34,
      "x": -1.024,
      "y": 1.019,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 32,
      "x": -0.305,
      "y": -0.574,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 0.307,
      "y": -0.578,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 1.345,
      "y": 1.019,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -1.345,
      "y": 1.019,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 29,
      "x": 0.865,
      "y": 0.143,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 28,
      "x": -0.865,
      "y": 0.143,
      "layer": 1,
      "closed_by": [
        27
      ]
    },
    {
      "id": 36,
      "x": 0.948,
      "y": 0.063,
      "layer": 2,
      "closed_by": [
        26
      ]
    },
    {
      "id": 27,
      "x": -0.947,
      "y": 0.063,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 26,
      "x": 1.026,
      "y": -0.016,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 25,
      "x": -1.026,
      "y": -0.016,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 24,
      "x": 1.108,
      "y": -0.097,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 23,
      "x": -1.103,
      "y": -0.093,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 22,
      "x": 1.184,
      "y": -0.178,
      "layer": 5,
      "closed_by": [
        37
      ]
    },
    {
      "id": 21,
      "x": -1.184,
      "y": -0.178,
      "layer": 5,
      "closed_by": [
        20
      ]
    },
    {
      "id": 37,
      "x": 1.266,
      "y": -0.256,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 20,
      "x": -1.266,
      "y": -0.256,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 31,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 36.3,
    "star2": 54.0,
    "star3": 9.6
  }
}