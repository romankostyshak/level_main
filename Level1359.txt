{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.442,
      "y": 0.032,
      "angle": 315.0,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": -0.441,
      "y": 0.103,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        49
      ]
    },
    {
      "id": 49,
      "x": -0.441,
      "y": 0.18,
      "angle": 315.0,
      "layer": 3,
      "closed_by": [
        48,
        42
      ]
    },
    {
      "id": 48,
      "x": -0.441,
      "y": 0.263,
      "angle": 315.0,
      "layer": 4,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "x": -0.439,
      "y": 0.342,
      "angle": 315.0,
      "layer": 5,
      "closed_by": [
        46
      ]
    },
    {
      "id": 46,
      "x": -0.439,
      "y": 0.423,
      "angle": 315.0,
      "layer": 6,
      "closed_by": [
        39
      ]
    },
    {
      "id": 45,
      "x": 0.448,
      "y": 0.425,
      "angle": 45.0,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": 0.448,
      "y": 0.349,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 43,
      "x": 0.449,
      "y": 0.27,
      "angle": 45.0,
      "layer": 3,
      "closed_by": [
        48,
        42
      ]
    },
    {
      "id": 42,
      "x": 0.448,
      "y": 0.187,
      "angle": 45.0,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 41,
      "x": 0.449,
      "y": 0.112,
      "angle": 45.0,
      "layer": 5,
      "closed_by": [
        40
      ]
    },
    {
      "id": 40,
      "x": 0.449,
      "y": 0.028,
      "angle": 45.0,
      "layer": 6,
      "closed_by": [
        38
      ]
    },
    {
      "id": 39,
      "x": -0.625,
      "y": 0.679,
      "layer": 7,
      "closed_by": [
        37
      ]
    },
    {
      "id": 38,
      "x": 0.638,
      "y": -0.418,
      "layer": 7,
      "closed_by": [
        36
      ]
    },
    {
      "id": 37,
      "x": -0.259,
      "y": 0.873,
      "layer": 8,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 0.268,
      "y": -0.206,
      "layer": 8,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 1.21,
      "y": -0.421,
      "layer": 8,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -1.213,
      "y": 0.662,
      "layer": 8,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 1.246,
      "y": -0.377,
      "layer": 7,
      "closed_by": [
        35
      ]
    },
    {
      "id": 32,
      "x": -1.25,
      "y": 0.62,
      "layer": 7,
      "closed_by": [
        34
      ]
    },
    {
      "id": 31,
      "x": 1.284,
      "y": -0.337,
      "layer": 6,
      "closed_by": [
        33
      ]
    },
    {
      "id": 30,
      "x": -1.286,
      "y": 0.574,
      "layer": 6,
      "closed_by": [
        32
      ]
    },
    {
      "id": 29,
      "x": 1.322,
      "y": -0.298,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 28,
      "x": -1.325,
      "y": 0.532,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 27,
      "x": 1.603,
      "y": -0.238,
      "angle": 320.0,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 26,
      "x": 1.603,
      "y": -0.158,
      "angle": 320.0,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 25,
      "x": 1.6,
      "y": -0.079,
      "angle": 320.0,
      "layer": 2,
      "closed_by": [
        26
      ]
    },
    {
      "id": 24,
      "x": 1.598,
      "y": 0.002,
      "angle": 320.0,
      "layer": 1,
      "closed_by": [
        25
      ]
    },
    {
      "id": 23,
      "x": -1.639,
      "y": 0.476,
      "angle": 315.0,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 22,
      "x": -1.639,
      "y": 0.393,
      "angle": 315.0,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 21,
      "x": -1.641,
      "y": 0.31,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 20,
      "x": -1.643,
      "y": 0.224,
      "angle": 315.0,
      "layer": 1,
      "closed_by": [
        21
      ]
    }
  ],
  "cards_in_layout_amount": 32,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 41.9,
    "star2": 45.2,
    "star3": 12.3
  }
}