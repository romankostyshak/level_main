{
  "layout_cards": [
    {
      "id": 51,
      "x": 1.054,
      "y": -0.421,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 50,
      "x": 1.059,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 49,
      "x": -0.4,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        35,
        26
      ]
    },
    {
      "id": 48,
      "x": -0.337,
      "y": 0.537,
      "layer": 5,
      "closed_by": [
        28
      ]
    },
    {
      "id": 47,
      "y": 0.819,
      "layer": 4,
      "closed_by": [
        48,
        34
      ]
    },
    {
      "id": 46,
      "x": -1.853,
      "y": 0.46,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 45,
      "x": -1.853,
      "y": 0.859,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 44,
      "x": -1.853,
      "y": -0.017,
      "layer": 2,
      "closed_by": [
        46,
        25
      ]
    },
    {
      "id": 43,
      "x": -1.853,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 42,
      "x": 1.853,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        41
      ]
    },
    {
      "id": 41,
      "x": 1.853,
      "y": -0.017,
      "layer": 2,
      "closed_by": [
        40,
        37
      ]
    },
    {
      "id": 40,
      "x": 1.853,
      "y": 0.46,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 39,
      "x": 1.853,
      "y": 0.859,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 0.402,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        35,
        27
      ]
    },
    {
      "id": 37,
      "x": 1.46,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 36,
      "x": 1.054,
      "y": -0.017,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 35,
      "y": -0.418,
      "layer": 5,
      "closed_by": [
        28
      ]
    },
    {
      "id": 34,
      "x": 0.337,
      "y": 0.537,
      "layer": 5,
      "closed_by": [
        28
      ]
    },
    {
      "id": 33,
      "x": 0.66,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 32,
      "x": -0.66,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        48
      ]
    },
    {
      "id": 31,
      "x": -0.337,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        47,
        32
      ]
    },
    {
      "id": 30,
      "x": 0.337,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        47,
        33
      ]
    },
    {
      "id": 29,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        31,
        30
      ]
    },
    {
      "id": 28,
      "y": 0.136,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 0.74,
      "y": -0.418,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -0.731,
      "y": -0.418,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 25,
      "x": -1.449,
      "y": -0.421,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 24,
      "x": -1.052,
      "y": -0.017,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 23,
      "x": -1.039,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        24
      ]
    },
    {
      "id": 22,
      "x": -1.039,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        26
      ]
    }
  ],
  "cards_in_layout_amount": 30,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 41.2,
    "star2": 48.6,
    "star3": 9.7
  }
}