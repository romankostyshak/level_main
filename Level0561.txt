{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.209,
      "y": 0.3,
      "angle": 315.0,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 50,
      "x": -0.143,
      "y": 0.3,
      "angle": 315.0,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 49,
      "x": 1.195,
      "y": 1.024,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 48,
      "x": 1.121,
      "y": 1.024,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 47,
      "x": 1.207,
      "y": -0.178,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 46,
      "x": 1.044,
      "y": 1.024,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 45,
      "x": 1.279,
      "y": -0.178,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": 1.36,
      "y": -0.179,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 43,
      "x": 0.972,
      "y": 1.023,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -0.972,
      "y": -0.421,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 41,
      "x": -1.049,
      "y": -0.421,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 40,
      "x": -1.121,
      "y": -0.421,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 39,
      "x": -1.189,
      "y": -0.421,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 38,
      "x": -1.207,
      "y": 0.782,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 37,
      "x": -1.355,
      "y": 0.782,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 0.134,
      "y": 0.3,
      "angle": 315.0,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 35,
      "x": 0.054,
      "y": 0.3,
      "angle": 315.0,
      "layer": 6,
      "closed_by": [
        32
      ]
    },
    {
      "id": 34,
      "x": 1.133,
      "y": -0.178,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 33,
      "x": -0.078,
      "y": 0.298,
      "angle": 315.0,
      "layer": 5,
      "closed_by": [
        35
      ]
    },
    {
      "id": 32,
      "x": -0.014,
      "y": 0.298,
      "angle": 315.0,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 0.202,
      "y": 0.3,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 30,
      "x": -1.281,
      "y": 0.782,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 29,
      "x": -1.131,
      "y": 0.782,
      "layer": 1,
      "closed_by": [
        38
      ]
    }
  ],
  "cards_in_layout_amount": 23,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 58.1,
    "star2": 6.5,
    "star3": 34.7
  }
}