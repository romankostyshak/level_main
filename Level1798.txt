{
  "layout_cards": [
    {
      "id": 49,
      "x": 0.333,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 48,
      "x": -0.331,
      "y": -0.577,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 47,
      "x": 0.574,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        50,
        39
      ]
    },
    {
      "id": 46,
      "x": -0.573,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        45,
        38
      ]
    },
    {
      "id": 45,
      "x": -0.437,
      "y": 0.023,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        43,
        40
      ]
    },
    {
      "id": 50,
      "x": 0.437,
      "y": 0.014,
      "angle": 340.0,
      "layer": 3,
      "closed_by": [
        44,
        40
      ]
    },
    {
      "id": 44,
      "x": 0.546,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 43,
      "x": -0.546,
      "y": 0.624,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 42,
      "x": -0.331,
      "y": 1.019,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 0.333,
      "y": 1.016,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 40,
      "x": -0.002,
      "y": -0.577,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 39,
      "x": 0.947,
      "y": -0.414,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 38,
      "x": -0.944,
      "y": -0.412,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 35,
      "x": 1.345,
      "y": -0.414,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 34,
      "x": -1.345,
      "y": -0.414,
      "layer": 4,
      "closed_by": [
        32
      ]
    },
    {
      "id": 33,
      "x": 1.345,
      "y": 0.298,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 32,
      "x": -1.345,
      "y": 0.298,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 31,
      "x": 1.347,
      "y": 1.019,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -1.343,
      "y": 1.018,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 19,
  "cards_in_deck_amount": 22,
  "stars_statistic": {
    "star1": 15.0,
    "star2": 0.2,
    "star3": 84.3
  }
}