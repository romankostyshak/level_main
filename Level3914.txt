{
  "layout_cards": [
    {
      "id": 14,
      "x": -0.337,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 15,
      "x": 0.337,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        25
      ]
    },
    {
      "id": 16,
      "x": 0.898,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        24,
        25
      ]
    },
    {
      "id": 17,
      "x": 0.337,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        24
      ]
    },
    {
      "id": 18,
      "x": 1.46,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        26
      ]
    },
    {
      "id": 19,
      "x": -0.337,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        23
      ]
    },
    {
      "id": 20,
      "x": -0.898,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        22,
        23
      ]
    },
    {
      "id": 21,
      "x": -1.457,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 22,
      "x": -0.759,
      "y": -0.238,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        29,
        36
      ]
    },
    {
      "id": 23,
      "x": -0.759,
      "y": 0.837,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 24,
      "x": 0.759,
      "y": 0.837,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 25,
      "x": 0.759,
      "y": -0.238,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        26,
        28
      ]
    },
    {
      "id": 26,
      "x": 1.098,
      "y": -0.039,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        32,
        37
      ]
    },
    {
      "id": 27,
      "x": -2.118,
      "y": -0.078,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 28,
      "x": 0.537,
      "y": -0.238,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 29,
      "x": -1.1,
      "y": -0.039,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        33,
        38
      ]
    },
    {
      "id": 30,
      "x": 0.759,
      "y": 0.837,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 31,
      "x": -0.759,
      "y": 0.837,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 32,
      "x": 1.46,
      "y": 0.578,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 33,
      "x": -1.457,
      "y": 0.578,
      "angle": 30.0,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 34,
      "x": 1.319,
      "y": 0.039,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -1.319,
      "y": 0.039,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -0.537,
      "y": -0.238,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 37,
      "x": 0.74,
      "y": -0.179,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -0.74,
      "y": -0.179,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -0.56,
      "y": 0.898,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 0.559,
      "y": 0.898,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 2.259,
      "y": 0.46,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 42,
      "x": -2.259,
      "y": 0.46,
      "layer": 1,
      "closed_by": [
        27
      ]
    },
    {
      "id": 43,
      "x": 2.118,
      "y": -0.079,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": 2.42,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 45,
      "x": -2.417,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 46,
      "x": -2.559,
      "y": 0.081,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [
        50
      ]
    },
    {
      "id": 47,
      "x": -2.759,
      "y": -0.279,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 48,
      "x": 2.559,
      "y": 0.079,
      "angle": 30.0,
      "layer": 4,
      "closed_by": [
        49
      ]
    },
    {
      "id": 49,
      "x": 2.559,
      "y": -0.337,
      "angle": 330.0,
      "layer": 5,
      "closed_by": [
        51
      ]
    },
    {
      "id": 50,
      "x": -2.559,
      "y": -0.337,
      "angle": 30.0,
      "layer": 5,
      "closed_by": [
        47
      ]
    },
    {
      "id": 51,
      "x": 2.759,
      "y": -0.279,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 38,
  "cards_in_deck_amount": 26,
  "layers_in_level": 6,
  "open_cards": 8,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 28.8,
    "star2": 67.7,
    "star3": 3.0
  }
}