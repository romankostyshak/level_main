{
  "layout_cards": [
    {
      "id": 49,
      "x": -0.33,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        47,
        44
      ]
    },
    {
      "id": 48,
      "x": 0.333,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        47,
        50
      ]
    },
    {
      "id": 47,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        38,
        35
      ]
    },
    {
      "id": 46,
      "x": 0.335,
      "y": 0.3,
      "layer": 1,
      "closed_by": [
        47,
        50,
        43,
        42
      ]
    },
    {
      "id": 45,
      "x": -0.333,
      "y": 0.3,
      "layer": 1,
      "closed_by": [
        47,
        44,
        43,
        41
      ]
    },
    {
      "id": 50,
      "x": 0.545,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 44,
      "x": -0.546,
      "y": -0.177,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 43,
      "x": -0.014,
      "y": 0.697,
      "layer": 2,
      "closed_by": [
        40,
        39
      ]
    },
    {
      "id": 42,
      "x": 0.546,
      "y": 0.698,
      "layer": 2,
      "closed_by": [
        39,
        38
      ]
    },
    {
      "id": 41,
      "x": -0.546,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        40,
        35
      ]
    },
    {
      "id": 40,
      "x": -0.305,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 39,
      "x": 0.307,
      "y": 1.023,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 38,
      "x": 0.453,
      "y": -0.143,
      "angle": 315.0,
      "layer": 3,
      "closed_by": [
        34,
        32
      ]
    },
    {
      "id": 35,
      "x": -0.453,
      "y": -0.143,
      "angle": 45.0,
      "layer": 3,
      "closed_by": [
        33,
        30
      ]
    },
    {
      "id": 34,
      "x": 0.488,
      "y": 0.578,
      "angle": 45.0,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 33,
      "x": -0.488,
      "y": 0.574,
      "angle": 315.0,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 32,
      "x": 1.077,
      "angle": 45.0,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 30,
      "x": -1.077,
      "y": -0.013,
      "angle": 315.0,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 29,
      "x": 0.683,
      "y": 0.136,
      "layer": 5,
      "closed_by": [
        36,
        27
      ]
    },
    {
      "id": 31,
      "x": -0.671,
      "y": 0.128,
      "layer": 5,
      "closed_by": [
        28,
        26
      ]
    },
    {
      "id": 28,
      "x": -0.944,
      "y": 0.537,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 0.944,
      "y": 0.537,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 0.944,
      "y": -0.277,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -0.944,
      "y": -0.275,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 24,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 32.3,
    "star2": 0.7,
    "star3": 66.3
  }
}