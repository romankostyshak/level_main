{
  "layout_cards": [
    {
      "id": 9,
      "x": -1.294,
      "y": 0.3,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 10,
      "x": 1.373,
      "y": 0.3,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 11,
      "x": -1.531,
      "y": 0.303,
      "layer": 5,
      "closed_by": [
        9
      ]
    },
    {
      "id": 12,
      "x": -1.72,
      "y": 0.782,
      "angle": 45.0,
      "layer": 4,
      "closed_by": [
        11
      ]
    },
    {
      "id": 13,
      "x": 1.718,
      "y": 0.785,
      "angle": 315.0,
      "layer": 4,
      "closed_by": [
        16
      ]
    },
    {
      "id": 14,
      "x": -1.682,
      "y": 0.879,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        12
      ]
    },
    {
      "id": 15,
      "x": 1.679,
      "y": 0.875,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        13
      ]
    },
    {
      "id": 16,
      "x": 1.534,
      "y": 0.3,
      "layer": 5,
      "closed_by": [
        10
      ]
    },
    {
      "id": 18,
      "x": -1.616,
      "y": 0.962,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        14
      ]
    },
    {
      "id": 19,
      "x": 1.616,
      "y": 0.958,
      "angle": 345.0,
      "layer": 2,
      "closed_by": [
        15
      ]
    },
    {
      "id": 20,
      "x": -1.531,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        18
      ]
    },
    {
      "id": 21,
      "x": -0.001,
      "y": -0.574,
      "layer": 8,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -1.713,
      "y": -0.18,
      "angle": 315.0,
      "layer": 4,
      "closed_by": [
        11
      ]
    },
    {
      "id": 23,
      "x": 1.715,
      "y": -0.17,
      "angle": 45.0,
      "layer": 4,
      "closed_by": [
        16
      ]
    },
    {
      "id": 24,
      "x": -1.674,
      "y": -0.28,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 25,
      "x": 1.679,
      "y": -0.27,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 26,
      "x": -1.613,
      "y": -0.354,
      "angle": 345.0,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 27,
      "x": 1.616,
      "y": -0.351,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 28,
      "x": 1.531,
      "y": -0.416,
      "layer": 1,
      "closed_by": [
        27
      ]
    },
    {
      "id": 29,
      "x": -0.492,
      "y": -0.513,
      "angle": 75.0,
      "layer": 7,
      "closed_by": [
        21
      ]
    },
    {
      "id": 30,
      "x": 0.492,
      "y": -0.518,
      "angle": 285.0,
      "layer": 7,
      "closed_by": [
        21
      ]
    },
    {
      "id": 31,
      "x": -0.504,
      "y": -0.409,
      "angle": 60.0,
      "layer": 6,
      "closed_by": [
        29
      ]
    },
    {
      "id": 32,
      "x": 0.504,
      "y": -0.416,
      "angle": 300.0,
      "layer": 6,
      "closed_by": [
        30
      ]
    },
    {
      "id": 33,
      "x": -0.492,
      "y": -0.305,
      "angle": 45.0,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 34,
      "x": 0.493,
      "y": -0.312,
      "angle": 315.0,
      "layer": 5,
      "closed_by": [
        32
      ]
    },
    {
      "id": 35,
      "x": -0.763,
      "y": 0.985,
      "angle": 315.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -1.531,
      "y": -0.414,
      "layer": 1,
      "closed_by": [
        26
      ]
    },
    {
      "id": 37,
      "x": 1.531,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        19
      ]
    },
    {
      "id": 38,
      "x": 0.796,
      "y": 0.989,
      "angle": 45.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -0.451,
      "y": -0.209,
      "angle": 30.0,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 40,
      "x": 0.449,
      "y": -0.216,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 41,
      "x": -0.646,
      "y": 0.929,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [
        9,
        35
      ]
    },
    {
      "id": 42,
      "x": 0.651,
      "y": 0.93,
      "angle": 30.0,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 43,
      "x": -0.384,
      "y": -0.136,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 44,
      "x": 0.388,
      "y": -0.14,
      "angle": 345.0,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 45,
      "x": 0.384,
      "y": 0.819,
      "angle": 14.998,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 46,
      "x": -0.003,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        45,
        50
      ]
    },
    {
      "id": 47,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        43,
        44
      ]
    },
    {
      "id": 48,
      "x": -0.004,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 49,
      "x": -0.001,
      "y": -0.097,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 50,
      "x": -0.384,
      "y": 0.819,
      "angle": 345.0,
      "layer": 3,
      "closed_by": [
        41
      ]
    }
  ],
  "cards_in_layout_amount": 41,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 1.3,
    "star2": 39.9,
    "star3": 58.1
  }
}