{
  "layout_cards": [
    {
      "id": 12,
      "x": -1.136,
      "y": -0.337,
      "layer": 7,
      "closed_by": [
        14
      ]
    },
    {
      "id": 13,
      "x": 0.893,
      "y": -0.337,
      "layer": 8,
      "closed_by": []
    },
    {
      "id": 14,
      "x": -0.893,
      "y": -0.337,
      "layer": 8,
      "closed_by": []
    },
    {
      "id": 15,
      "x": 1.128,
      "y": -0.337,
      "layer": 7,
      "closed_by": [
        13
      ]
    },
    {
      "id": 16,
      "x": 1.131,
      "y": -0.574,
      "layer": 6,
      "closed_by": [
        15
      ]
    },
    {
      "id": 17,
      "x": -1.133,
      "y": -0.577,
      "layer": 6,
      "closed_by": [
        12
      ]
    },
    {
      "id": 18,
      "x": 1.827,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        19
      ]
    },
    {
      "id": 19,
      "x": 1.608,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 20,
      "x": 1.345,
      "y": -0.574,
      "layer": 5,
      "closed_by": [
        16
      ]
    },
    {
      "id": 21,
      "x": -1.347,
      "y": -0.577,
      "layer": 5,
      "closed_by": [
        17
      ]
    },
    {
      "id": 22,
      "x": -1.582,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 23,
      "x": -1.83,
      "y": -0.335,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 24,
      "x": 1.825,
      "y": -0.574,
      "layer": 2,
      "closed_by": [
        18
      ]
    },
    {
      "id": 25,
      "x": -1.827,
      "y": -0.574,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 26,
      "x": 2.072,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        24
      ]
    },
    {
      "id": 27,
      "x": -2.065,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        25
      ]
    },
    {
      "id": 28,
      "x": 0.893,
      "y": 1.019,
      "layer": 8,
      "closed_by": []
    },
    {
      "id": 29,
      "x": 1.133,
      "y": 1.018,
      "layer": 7,
      "closed_by": [
        28
      ]
    },
    {
      "id": 30,
      "x": 1.371,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        43
      ]
    },
    {
      "id": 31,
      "x": 1.613,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 32,
      "x": 1.853,
      "y": 1.018,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 33,
      "x": 1.853,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 34,
      "x": 2.065,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 35,
      "x": -0.892,
      "y": 1.016,
      "layer": 8,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -1.136,
      "y": 1.018,
      "layer": 7,
      "closed_by": [
        35
      ]
    },
    {
      "id": 37,
      "x": -1.35,
      "y": 0.781,
      "layer": 5,
      "closed_by": [
        42
      ]
    },
    {
      "id": 38,
      "x": -1.616,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 39,
      "x": -1.83,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 40,
      "x": -1.832,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 41,
      "x": -2.065,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 42,
      "x": -1.133,
      "y": 0.777,
      "layer": 6,
      "closed_by": [
        36
      ]
    },
    {
      "id": 43,
      "x": 1.133,
      "y": 0.699,
      "layer": 6,
      "closed_by": [
        29
      ]
    },
    {
      "id": 44,
      "x": -0.312,
      "y": 1.018,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 45,
      "x": 0.3,
      "y": 1.019,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 46,
      "x": -0.305,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 47,
      "x": -0.314,
      "y": 0.786,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 48,
      "x": -0.303,
      "y": -0.337,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 49,
      "x": 0.307,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        51
      ]
    },
    {
      "id": 50,
      "x": 0.3,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 51,
      "x": 0.307,
      "y": -0.34,
      "layer": 2,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 40,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 2.5,
    "star2": 49.2,
    "star3": 47.6
  }
}