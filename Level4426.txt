{
  "layout_cards": [
    {
      "id": 24,
      "y": 1.019,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 25,
      "x": -0.859,
      "y": -0.338,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 26,
      "x": 0.86,
      "y": -0.338,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 27,
      "x": -0.978,
      "y": -0.458,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 28,
      "x": 1.22,
      "y": -0.458,
      "layer": 5,
      "closed_by": [
        26,
        30
      ]
    },
    {
      "id": 29,
      "x": 0.98,
      "y": -0.458,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 30,
      "x": 1.5,
      "y": 0.279,
      "angle": 315.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -1.498,
      "y": 0.279,
      "angle": 45.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 1.22,
      "y": 0.56,
      "angle": 315.0,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 33,
      "x": -1.22,
      "y": 0.56,
      "angle": 45.0,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 34,
      "x": -0.939,
      "y": 0.838,
      "angle": 45.0,
      "layer": 4,
      "closed_by": [
        33
      ],
      "card_value": "",
      "effect_id": 0,
      "card_type": 0
    },
    {
      "id": 35,
      "x": 0.939,
      "y": 0.838,
      "angle": 315.0,
      "layer": 4,
      "closed_by": [
        32
      ],
      "card_value": "",
      "effect_id": 0,
      "card_type": 0
    },
    {
      "id": 36,
      "x": -1.22,
      "y": -0.458,
      "layer": 5,
      "closed_by": [
        25,
        31
      ]
    },
    {
      "id": 38,
      "y": -0.458,
      "layer": 3,
      "closed_by": [],
      "card_value": "",
      "effect_id": 0,
      "card_type": 0
    },
    {
      "id": 39,
      "x": -0.759,
      "y": -0.458,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 40,
      "x": 0.759,
      "y": -0.458,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 41,
      "x": 0.46,
      "y": -0.5,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        26,
        38,
        40
      ]
    },
    {
      "id": 42,
      "x": -0.458,
      "y": -0.5,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        25,
        38,
        39
      ]
    },
    {
      "id": 43,
      "x": -0.759,
      "y": 0.379,
      "layer": 3,
      "closed_by": [
        25,
        34
      ]
    },
    {
      "id": 44,
      "x": 0.759,
      "y": 0.379,
      "layer": 3,
      "closed_by": [
        26,
        35
      ]
    },
    {
      "id": 45,
      "x": 0.46,
      "y": 0.418,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        24,
        44
      ]
    },
    {
      "id": 46,
      "x": -0.579,
      "y": -0.1,
      "layer": 1,
      "closed_by": [
        42,
        50
      ]
    },
    {
      "id": 47,
      "x": 0.578,
      "y": -0.1,
      "layer": 1,
      "closed_by": [
        41,
        45
      ]
    },
    {
      "id": 48,
      "y": 0.859,
      "layer": 1,
      "closed_by": [
        45,
        50
      ]
    },
    {
      "id": 49,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        41,
        42
      ]
    },
    {
      "id": 50,
      "x": -0.458,
      "y": 0.418,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        24,
        43
      ]
    }
  ],
  "free_cards_from_stickers": [],
  "cards_in_layout_amount": 26,
  "cards_in_deck_amount": 26,
  "layers_in_level": 6,
  "open_cards": 6,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 49.4,
    "star2": 48.9,
    "star3": 1.3
  }
}