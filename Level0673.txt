{
  "layout_cards": [
    {
      "id": 51,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        50,
        49
      ]
    },
    {
      "id": 50,
      "x": 0.462,
      "y": -0.497,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 49,
      "x": -0.465,
      "y": -0.495,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 48,
      "x": 0.865,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 47,
      "x": -0.865,
      "y": -0.416,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 46,
      "x": 1.269,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        44
      ]
    },
    {
      "id": 45,
      "x": -1.266,
      "y": -0.335,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 44,
      "x": 1.671,
      "y": -0.254,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 43,
      "x": -1.667,
      "y": -0.256,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 42,
      "y": 1.021,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 0.455,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 40,
      "x": -0.46,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 39,
      "x": 0.864,
      "y": 0.861,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 38,
      "x": -0.86,
      "y": 0.861,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 37,
      "x": 1.258,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 36,
      "x": -1.264,
      "y": 0.781,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 35,
      "x": 1.664,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        37
      ]
    },
    {
      "id": 34,
      "x": -1.667,
      "y": 0.708,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 33,
      "x": 2.24,
      "y": -0.179,
      "layer": 5,
      "closed_by": [
        29
      ]
    },
    {
      "id": 32,
      "x": 2.24,
      "y": 0.699,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 27,
      "x": -2.24,
      "y": 0.657,
      "layer": 5,
      "closed_by": [
        28
      ]
    },
    {
      "id": 25,
      "x": -2.24,
      "y": -0.179,
      "layer": 5,
      "closed_by": [
        24
      ]
    },
    {
      "id": 24,
      "x": -2.24,
      "y": -0.216,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 28,
      "x": -2.24,
      "y": 0.699,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -2.24,
      "y": 0.238,
      "layer": 4,
      "closed_by": [
        27,
        25
      ]
    },
    {
      "id": 31,
      "x": 2.24,
      "y": 0.657,
      "layer": 5,
      "closed_by": [
        32
      ]
    },
    {
      "id": 30,
      "x": 2.24,
      "y": 0.238,
      "layer": 4,
      "closed_by": [
        33,
        31
      ]
    },
    {
      "id": 29,
      "x": 2.24,
      "y": -0.216,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 28,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 42.3,
    "star2": 50.5,
    "star3": 6.2
  }
}