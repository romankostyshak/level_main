{
  "layout_cards": [
    {
      "id": 47,
      "x": 0.282,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        46,
        43
      ]
    },
    {
      "id": 46,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        41,
        40
      ]
    },
    {
      "id": 43,
      "x": 0.66,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 41,
      "x": 0.337,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 40,
      "x": -0.337,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 31,
      "x": 0.699,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 22,
      "x": 1.86,
      "y": 0.059,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 21,
      "x": -1.858,
      "y": 0.039,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 33,
      "x": -0.898,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 35,
      "x": -0.699,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 38,
      "x": -0.898,
      "y": 0.46,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 37,
      "x": -0.479,
      "y": 0.537,
      "layer": 4,
      "closed_by": [
        38,
        28,
        39
      ]
    },
    {
      "id": 32,
      "x": 0.479,
      "y": 0.537,
      "layer": 4,
      "closed_by": [
        29,
        26,
        39
      ]
    },
    {
      "id": 29,
      "x": 0.279,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        27
      ]
    },
    {
      "id": 28,
      "x": -0.27,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        27
      ]
    },
    {
      "id": 27,
      "y": 1.019,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 0.337,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 17,
      "x": 1.452,
      "y": 0.46,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 1.659,
      "y": 0.398,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        17
      ]
    },
    {
      "id": 20,
      "x": 2.059,
      "y": -0.298,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 25,
      "x": -1.452,
      "y": 0.46,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 23,
      "x": -1.659,
      "y": 0.398,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 19,
      "x": -2.059,
      "y": -0.319,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        21
      ]
    },
    {
      "id": 26,
      "x": 0.898,
      "y": 0.46,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 0.898,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 30,
      "x": -0.328,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 49,
      "x": -0.892,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": -0.892,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 42,
      "x": -0.66,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 51,
      "x": -0.337,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 39,
      "y": -0.259,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 48,
      "x": -0.279,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        46,
        42
      ]
    },
    {
      "id": 18,
      "x": 0.337,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 45,
      "x": 0.893,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 50,
      "x": 0.893,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        45
      ]
    }
  ],
  "cards_in_layout_amount": 35,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 14.0,
    "star2": 71.0,
    "star3": 14.6
  }
}