{
  "layout_cards": [
    {
      "id": 21,
      "x": -2.059,
      "y": -0.298,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 22,
      "x": 2.2,
      "y": 0.238,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 23,
      "x": -2.2,
      "y": 0.238,
      "angle": 30.0,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 24,
      "x": -1.978,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 25,
      "x": 1.98,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 26,
      "x": -1.899,
      "y": -0.298,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 27,
      "x": 1.899,
      "y": -0.298,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 28,
      "x": -1.758,
      "y": 0.238,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        26
      ]
    },
    {
      "id": 29,
      "x": -0.859,
      "y": 0.777,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 0.86,
      "y": 0.777,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -1.039,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 32,
      "x": 1.039,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 33,
      "x": -0.859,
      "y": -0.179,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 0.86,
      "y": -0.179,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 1.039,
      "y": -0.1,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 36,
      "x": -1.039,
      "y": -0.1,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 37,
      "x": 1.758,
      "y": 0.238,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        27
      ]
    },
    {
      "id": 38,
      "x": 2.059,
      "y": -0.298,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 39,
      "x": 0.337,
      "y": 0.217,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 40,
      "x": -0.337,
      "y": 0.217,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 0.5,
      "y": 0.859,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 42,
      "x": -0.499,
      "y": 0.859,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 43,
      "x": -0.499,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        29,
        33,
        40
      ]
    },
    {
      "id": 44,
      "x": 0.5,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        30,
        34,
        39
      ]
    },
    {
      "id": 45,
      "x": 0.898,
      "y": 0.439,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        32,
        35,
        44
      ]
    },
    {
      "id": 46,
      "x": -1.2,
      "y": -0.079,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 47,
      "x": -0.499,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 48,
      "x": 1.2,
      "y": -0.079,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 49,
      "x": 0.499,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 50,
      "x": -0.898,
      "y": 0.439,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        31,
        36,
        43
      ]
    }
  ],
  "cards_in_layout_amount": 30,
  "cards_in_deck_amount": 26,
  "layers_in_level": 5,
  "open_cards": 8,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 25.7,
    "star2": 70.7,
    "star3": 3.4
  }
}