{
  "layout_cards": [
    {
      "id": 9,
      "x": -1.825,
      "y": 0.221,
      "layer": 1,
      "closed_by": [
        11,
        16
      ]
    },
    {
      "id": 10,
      "x": 1.827,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        12,
        13
      ]
    },
    {
      "id": 11,
      "x": -1.745,
      "y": -0.256,
      "layer": 2,
      "closed_by": [
        14
      ]
    },
    {
      "id": 12,
      "x": 1.745,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        18
      ]
    },
    {
      "id": 13,
      "x": 1.746,
      "y": 0.702,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 14,
      "x": -1.664,
      "y": -0.416,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 15,
      "x": -1.664,
      "y": 0.86,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 16,
      "x": -1.743,
      "y": 0.702,
      "layer": 2,
      "closed_by": [
        15
      ]
    },
    {
      "id": 18,
      "x": 1.661,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 19,
      "x": 1.664,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 20,
      "x": -1.264,
      "y": 0.224,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 21,
      "y": -0.572,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 22,
      "x": 0.003,
      "y": -0.097,
      "layer": 6,
      "closed_by": [
        21
      ]
    },
    {
      "id": 23,
      "x": 0.004,
      "y": 0.3,
      "layer": 5,
      "closed_by": [
        22
      ]
    },
    {
      "id": 24,
      "x": 0.004,
      "y": 0.694,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 25,
      "x": 0.004,
      "y": 1.041,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 26,
      "x": -1.264,
      "y": -0.495,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 27,
      "x": -1.264,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 28,
      "x": 1.264,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 29,
      "x": -1.105,
      "y": -0.416,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 30,
      "x": -1.105,
      "y": 0.865,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 31,
      "x": 1.11,
      "y": -0.416,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 32,
      "x": 1.105,
      "y": 0.861,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 33,
      "x": -1.024,
      "y": 0.703,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 34,
      "x": -1.026,
      "y": -0.256,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 35,
      "x": 1.266,
      "y": -0.493,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 36,
      "x": 1.264,
      "y": 0.224,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 0.412,
      "y": -0.177,
      "layer": 4,
      "closed_by": [
        23,
        48
      ]
    },
    {
      "id": 39,
      "x": 1.026,
      "y": 0.703,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 40,
      "x": -0.944,
      "y": 0.222,
      "layer": 1,
      "closed_by": [
        33,
        34
      ]
    },
    {
      "id": 41,
      "x": 0.944,
      "y": 0.221,
      "layer": 1,
      "closed_by": [
        39,
        42
      ]
    },
    {
      "id": 42,
      "x": 1.026,
      "y": -0.254,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 43,
      "x": -0.416,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        23,
        47
      ]
    },
    {
      "id": 44,
      "x": -0.416,
      "y": 1.041,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 45,
      "x": -0.416,
      "y": 0.625,
      "layer": 2,
      "closed_by": [
        25,
        46
      ]
    },
    {
      "id": 46,
      "x": -0.416,
      "y": 0.224,
      "layer": 3,
      "closed_by": [
        24,
        43
      ]
    },
    {
      "id": 47,
      "x": -0.416,
      "y": -0.574,
      "layer": 5,
      "closed_by": [
        22
      ]
    },
    {
      "id": 48,
      "x": 0.412,
      "y": -0.574,
      "layer": 5,
      "closed_by": [
        22
      ]
    },
    {
      "id": 49,
      "x": 0.414,
      "y": 0.224,
      "layer": 3,
      "closed_by": [
        24,
        38
      ]
    },
    {
      "id": 50,
      "x": 0.414,
      "y": 1.041,
      "layer": 1,
      "closed_by": [
        51
      ]
    },
    {
      "id": 51,
      "x": 0.414,
      "y": 0.623,
      "layer": 2,
      "closed_by": [
        25,
        49
      ]
    }
  ],
  "cards_in_layout_amount": 41,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 1.8,
    "star2": 43.2,
    "star3": 54.6
  }
}