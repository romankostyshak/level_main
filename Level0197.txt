{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.573,
      "y": -0.414,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": 0.666,
      "y": -0.416,
      "layer": 2,
      "closed_by": [
        49
      ]
    },
    {
      "id": 49,
      "x": 0.787,
      "y": -0.416,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 48,
      "x": 1.452,
      "y": -0.416,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 47,
      "x": 0.303,
      "y": 0.574,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 46,
      "x": 0.698,
      "y": 0.574,
      "layer": 4,
      "closed_by": [
        47
      ]
    },
    {
      "id": 45,
      "x": 1.057,
      "y": 0.574,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 44,
      "x": 1.457,
      "y": 0.574,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 43,
      "x": -0.293,
      "y": 0.035,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -0.685,
      "y": 0.035,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 41,
      "x": -1.047,
      "y": 0.035,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 40,
      "x": -1.447,
      "y": 0.035,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 39,
      "x": -1.554,
      "y": 1.029,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 38,
      "x": -1.457,
      "y": 1.031,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 37,
      "x": -1.654,
      "y": 1.029,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 36,
      "x": -0.79,
      "y": 1.021,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -0.67,
      "y": 1.021,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 34,
      "x": -0.569,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 33,
      "x": 1.656,
      "y": -0.416,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 32,
      "x": 1.559,
      "y": -0.416,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 31,
      "x": 1.715,
      "y": 0.574,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 30,
      "x": -1.713,
      "y": 0.035,
      "layer": 1,
      "closed_by": [
        40
      ]
    }
  ],
  "cards_in_layout_amount": 22,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 53.3,
    "star2": 6.1,
    "star3": 40.1
  }
}