{
  "layout_cards": [
    {
      "id": 13,
      "x": -1.029,
      "y": -0.354,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 14,
      "x": 1.026,
      "y": -0.351,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 15,
      "x": -1.029,
      "y": 0.462,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 18,
      "x": 1.031,
      "y": 0.46,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 19,
      "x": -0.62,
      "y": -0.273,
      "layer": 5,
      "closed_by": [
        13,
        15
      ]
    },
    {
      "id": 20,
      "x": 0.629,
      "y": -0.272,
      "layer": 5,
      "closed_by": [
        14,
        18
      ]
    },
    {
      "id": 21,
      "x": -0.624,
      "y": 0.541,
      "layer": 5,
      "closed_by": [
        15
      ]
    },
    {
      "id": 22,
      "x": 0.335,
      "y": -0.192,
      "layer": 4,
      "closed_by": [
        20,
        37
      ]
    },
    {
      "id": 23,
      "x": -0.324,
      "y": -0.187,
      "layer": 4,
      "closed_by": [
        19,
        21
      ]
    },
    {
      "id": 24,
      "x": -0.326,
      "y": 0.619,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 25,
      "x": 0.337,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 26,
      "x": -1.825,
      "y": -0.351,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 27,
      "x": -1.825,
      "y": 0.462,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 1.827,
      "y": 0.46,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -1.424,
      "y": -0.273,
      "layer": 5,
      "closed_by": [
        13,
        15,
        26,
        27
      ]
    },
    {
      "id": 30,
      "x": 1.424,
      "y": -0.272,
      "layer": 5,
      "closed_by": [
        14,
        18,
        28,
        36
      ]
    },
    {
      "id": 31,
      "x": -1.422,
      "y": 0.54,
      "layer": 5,
      "closed_by": [
        15,
        27
      ]
    },
    {
      "id": 32,
      "x": 1.424,
      "y": 0.54,
      "layer": 5,
      "closed_by": [
        18,
        28
      ]
    },
    {
      "id": 33,
      "x": -1.026,
      "y": -0.187,
      "layer": 4,
      "closed_by": [
        19,
        21,
        29,
        31
      ]
    },
    {
      "id": 34,
      "x": 1.026,
      "y": -0.187,
      "layer": 4,
      "closed_by": [
        20,
        30,
        32,
        37
      ]
    },
    {
      "id": 35,
      "x": -1.026,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        21,
        31
      ]
    },
    {
      "id": 36,
      "x": 1.824,
      "y": -0.351,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 37,
      "x": 0.629,
      "y": 0.541,
      "layer": 5,
      "closed_by": [
        18
      ]
    },
    {
      "id": 38,
      "x": 1.026,
      "y": 0.623,
      "layer": 4,
      "closed_by": [
        32,
        37
      ]
    },
    {
      "id": 39,
      "x": -0.625,
      "y": -0.108,
      "layer": 3,
      "closed_by": [
        23,
        24,
        33,
        35
      ]
    },
    {
      "id": 40,
      "x": -0.625,
      "y": 0.702,
      "layer": 3,
      "closed_by": [
        24,
        35
      ]
    },
    {
      "id": 41,
      "x": 0.633,
      "y": -0.112,
      "layer": 3,
      "closed_by": [
        22,
        25,
        34,
        38
      ]
    },
    {
      "id": 42,
      "x": 0.632,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        25,
        38
      ]
    },
    {
      "id": 43,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        24,
        25
      ]
    },
    {
      "id": 44,
      "x": 0.003,
      "y": -0.108,
      "layer": 3,
      "closed_by": [
        22,
        23,
        24,
        25
      ]
    },
    {
      "id": 45,
      "x": -0.326,
      "y": -0.032,
      "layer": 2,
      "closed_by": [
        39,
        40,
        43,
        44
      ]
    },
    {
      "id": 46,
      "x": 0.337,
      "y": -0.032,
      "layer": 2,
      "closed_by": [
        41,
        42,
        43,
        44
      ]
    },
    {
      "id": 47,
      "x": 0.337,
      "y": 0.787,
      "layer": 2,
      "closed_by": [
        42,
        43
      ]
    },
    {
      "id": 48,
      "x": -0.326,
      "y": 0.787,
      "layer": 2,
      "closed_by": [
        40,
        43
      ]
    },
    {
      "id": 49,
      "x": 0.004,
      "y": 0.223,
      "layer": 1,
      "closed_by": [
        45,
        46,
        47,
        48
      ]
    }
  ],
  "cards_in_layout_amount": 35,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 30.5,
    "star2": 64.3,
    "star3": 4.7
  }
}