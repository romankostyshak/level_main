{
  "layout_cards": [
    {
      "id": 28,
      "x": -2.463,
      "y": 0.221,
      "layer": 2,
      "closed_by": [
        40,
        48
      ]
    },
    {
      "id": 29,
      "x": 2.388,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        41,
        49
      ]
    },
    {
      "id": 30,
      "x": 0.735,
      "y": 0.221,
      "layer": 2,
      "closed_by": [
        32,
        51
      ]
    },
    {
      "id": 31,
      "x": -0.813,
      "y": 0.223,
      "layer": 2,
      "closed_by": [
        33,
        50
      ]
    },
    {
      "id": 32,
      "x": 0.975,
      "y": -0.34,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -1.049,
      "y": -0.34,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 1.376,
      "y": -0.34,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 35,
      "x": -1.45,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 36,
      "x": -1.848,
      "y": -0.34,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 37,
      "x": 1.771,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 38,
      "x": -2.226,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        28,
        36
      ]
    },
    {
      "id": 39,
      "x": 2.148,
      "y": -0.34,
      "layer": 1,
      "closed_by": [
        29,
        37
      ]
    },
    {
      "id": 40,
      "x": -2.226,
      "y": 0.782,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 2.148,
      "y": 0.787,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 42,
      "x": 1.746,
      "y": 0.782,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 43,
      "x": -1.424,
      "y": 0.782,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 44,
      "x": 1.345,
      "y": 0.782,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 45,
      "x": -1.049,
      "y": 0.782,
      "layer": 1,
      "closed_by": [
        31,
        43
      ],
      "card_type": 8
    },
    {
      "id": 46,
      "x": 0.975,
      "y": 0.785,
      "layer": 1,
      "closed_by": [
        30,
        44
      ]
    },
    {
      "id": 47,
      "x": -1.825,
      "y": 0.782,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 48,
      "x": -2.782,
      "y": 0.222,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 49,
      "x": 2.709,
      "y": 0.223,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 50,
      "x": -0.493,
      "y": 0.216,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 51,
      "x": 0.412,
      "y": 0.216,
      "layer": 3,
      "closed_by": []
    }
  ],
  "level_mechanic": 2,
  "free_cards_from_stickers": [
    28,
    31,
    33,
    35,
    36,
    38,
    40,
    43,
    45,
    47,
    48,
    50
  ],
  "cards_in_layout_amount": 24,
  "cards_in_deck_amount": 22,
  "stars_statistic": {
    "star1": 68.6,
    "star2": 7.8,
    "star3": 22.8
  }
}