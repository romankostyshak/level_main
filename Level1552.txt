{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.425,
      "y": 0.432,
      "angle": 335.0,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 50,
      "x": -0.418,
      "y": 0.432,
      "angle": 25.0,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 49,
      "x": -0.418,
      "y": 0.893,
      "angle": 335.0,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": 0.425,
      "y": 0.897,
      "angle": 25.0,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "x": 0.56,
      "y": 0.666,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 46,
      "x": -0.541,
      "y": 0.671,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 45,
      "x": 0.597,
      "y": 0.888,
      "angle": 25.0,
      "layer": 4,
      "closed_by": [
        44,
        24
      ]
    },
    {
      "id": 44,
      "x": 0.605,
      "y": 0.432,
      "angle": 335.0,
      "layer": 5,
      "closed_by": [
        25
      ]
    },
    {
      "id": 43,
      "x": -0.592,
      "y": 0.888,
      "angle": 335.0,
      "layer": 4,
      "closed_by": [
        42,
        23
      ]
    },
    {
      "id": 42,
      "x": -0.592,
      "y": 0.432,
      "angle": 25.0,
      "layer": 5,
      "closed_by": [
        26
      ]
    },
    {
      "id": 41,
      "x": 0.075,
      "y": -0.307,
      "angle": 335.0,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 40,
      "x": -0.074,
      "y": -0.3,
      "angle": 25.0,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 39,
      "x": 0.202,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 38,
      "x": -0.202,
      "y": -0.416,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 36,
      "x": 0.893,
      "y": -0.5,
      "layer": 1,
      "closed_by": [
        34
      ]
    },
    {
      "id": 35,
      "x": -0.893,
      "y": -0.497,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 34,
      "x": 1.019,
      "y": 0.064,
      "angle": 335.0,
      "layer": 2,
      "closed_by": [
        32,
        22
      ]
    },
    {
      "id": 33,
      "x": -1.016,
      "y": 0.071,
      "angle": 25.0,
      "layer": 2,
      "closed_by": [
        31,
        21
      ]
    },
    {
      "id": 32,
      "x": 1.194,
      "y": 0.43,
      "angle": 335.0,
      "layer": 3,
      "closed_by": [
        30,
        27
      ]
    },
    {
      "id": 31,
      "x": -1.182,
      "y": 0.425,
      "angle": 25.0,
      "layer": 3,
      "closed_by": [
        29,
        20
      ]
    },
    {
      "id": 30,
      "x": 1.192,
      "y": 0.884,
      "angle": 25.0,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 29,
      "x": -1.182,
      "y": 0.875,
      "angle": 335.0,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 37,
      "y": -0.328,
      "layer": 5,
      "closed_by": [
        26,
        25
      ]
    },
    {
      "id": 26,
      "x": -0.272,
      "y": 0.142,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 0.277,
      "y": 0.143,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 0.921,
      "y": 1.047,
      "angle": 65.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 23,
      "x": -0.911,
      "y": 1.047,
      "angle": 295.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 22,
      "x": 1.026,
      "y": -0.504,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 21,
      "x": -1.024,
      "y": -0.5,
      "layer": 3,
      "closed_by": [
        20
      ]
    },
    {
      "id": 27,
      "x": 1.065,
      "y": -0.143,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 20,
      "x": -1.059,
      "y": -0.128,
      "layer": 4,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 31,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 37.4,
    "star2": 50.7,
    "star3": 11.6
  }
}