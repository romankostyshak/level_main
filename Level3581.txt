{
  "layout_cards": [
    {
      "id": 3,
      "x": 1.103,
      "y": 1.019,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 4,
      "x": 1.355,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        3
      ]
    },
    {
      "id": 5,
      "x": 1.667,
      "y": 0.856,
      "layer": 3,
      "closed_by": [
        4
      ]
    },
    {
      "id": 6,
      "x": 1.35,
      "y": -0.497,
      "layer": 4,
      "closed_by": [
        7
      ]
    },
    {
      "id": 7,
      "x": 1.088,
      "y": -0.574,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 8,
      "x": 1.666,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        6
      ]
    },
    {
      "id": 9,
      "x": -2.013,
      "y": -0.574,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 10,
      "x": -2.144,
      "y": -0.416,
      "layer": 3,
      "closed_by": [
        9
      ]
    },
    {
      "id": 11,
      "x": -2.144,
      "y": 0.86,
      "layer": 3,
      "closed_by": [
        17
      ]
    },
    {
      "id": 12,
      "x": -1.825,
      "y": 0.221,
      "layer": 2,
      "closed_by": [
        10,
        11,
        13
      ]
    },
    {
      "id": 13,
      "x": -1.496,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        26,
        27
      ]
    },
    {
      "id": 14,
      "x": -1.108,
      "y": -0.18,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 15,
      "x": -1.105,
      "y": 0.629,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 16,
      "x": -2.144,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        12
      ]
    },
    {
      "id": 17,
      "x": -2.013,
      "y": 1.021,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 18,
      "x": -0.703,
      "y": -0.185,
      "layer": 5,
      "closed_by": [
        14
      ]
    },
    {
      "id": 19,
      "x": -0.323,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        18,
        20
      ]
    },
    {
      "id": 20,
      "x": -0.703,
      "y": 0.629,
      "layer": 5,
      "closed_by": [
        15
      ]
    },
    {
      "id": 21,
      "x": -0.324,
      "y": 0.625,
      "layer": 3,
      "closed_by": [
        19
      ]
    },
    {
      "id": 22,
      "x": -0.837,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 23,
      "x": -0.833,
      "y": -0.577,
      "layer": 4,
      "closed_by": [
        18
      ]
    },
    {
      "id": 24,
      "x": -1.345,
      "y": -0.574,
      "layer": 5,
      "closed_by": [
        14
      ]
    },
    {
      "id": 25,
      "x": -1.345,
      "y": 1.019,
      "layer": 5,
      "closed_by": [
        15
      ]
    },
    {
      "id": 26,
      "x": -1.345,
      "y": -0.335,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 27,
      "x": -1.345,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        25
      ]
    },
    {
      "id": 28,
      "x": -1.024,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        22,
        27
      ]
    },
    {
      "id": 29,
      "x": -0.703,
      "y": -0.493,
      "layer": 2,
      "closed_by": [
        36,
        37
      ]
    },
    {
      "id": 30,
      "x": -0.703,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        21,
        28
      ]
    },
    {
      "id": 31,
      "x": -0.326,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 32,
      "x": -0.324,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        20,
        30
      ]
    },
    {
      "id": 33,
      "x": 0.833,
      "y": -0.495,
      "layer": 4,
      "closed_by": [
        7
      ]
    },
    {
      "id": 34,
      "x": 0.837,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        3
      ]
    },
    {
      "id": 35,
      "x": 1.103,
      "y": -0.414,
      "layer": 3,
      "closed_by": [
        6,
        33
      ]
    },
    {
      "id": 36,
      "x": -1.026,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        23,
        26
      ]
    },
    {
      "id": 37,
      "x": -0.321,
      "y": -0.335,
      "layer": 3,
      "closed_by": [
        19
      ]
    },
    {
      "id": 38,
      "x": 0.574,
      "y": -0.414,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 39,
      "x": 1.108,
      "y": 0.861,
      "layer": 3,
      "closed_by": [
        4,
        34
      ]
    },
    {
      "id": 40,
      "x": 0.572,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 41,
      "x": 0.331,
      "y": -0.333,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 42,
      "x": 1.345,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        8,
        35
      ]
    },
    {
      "id": 43,
      "x": 1.345,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        5,
        39
      ]
    },
    {
      "id": 44,
      "x": 0.333,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 45,
      "x": 0.842,
      "y": 0.776,
      "layer": 2,
      "closed_by": [
        39,
        40
      ]
    },
    {
      "id": 46,
      "x": 1.105,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        43,
        45
      ]
    },
    {
      "id": 47,
      "x": 0.574,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        44,
        45
      ]
    },
    {
      "id": 48,
      "x": 1.105,
      "y": -0.261,
      "layer": 1,
      "closed_by": [
        42,
        50
      ]
    },
    {
      "id": 49,
      "x": 0.573,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        41,
        50
      ]
    },
    {
      "id": 50,
      "x": 0.837,
      "y": -0.333,
      "layer": 2,
      "closed_by": [
        35,
        38
      ]
    }
  ],
  "cards_in_layout_amount": 48,
  "cards_in_deck_amount": 32,
  "stars_statistic": {
    "star1": 1.1,
    "star2": 37.2,
    "star3": 61.4
  }
}