{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.689,
      "y": -0.368,
      "angle": 335.0,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 50,
      "x": -0.689,
      "y": -0.128,
      "angle": 335.0,
      "layer": 3,
      "closed_by": [
        49
      ]
    },
    {
      "id": 49,
      "x": -0.689,
      "y": 0.014,
      "angle": 335.0,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 48,
      "x": 0.72,
      "y": -0.256,
      "angle": 25.0,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "x": 0.72,
      "y": -0.134,
      "angle": 25.0,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 46,
      "x": 0.717,
      "y": 0.008,
      "angle": 25.0,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 45,
      "x": -0.689,
      "y": -0.254,
      "angle": 335.0,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 44,
      "x": 0.717,
      "y": -0.372,
      "angle": 25.0,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 43,
      "x": -0.564,
      "y": 0.541,
      "angle": 9.998,
      "layer": 5,
      "closed_by": [
        41
      ]
    },
    {
      "id": 42,
      "x": 0.583,
      "y": 0.545,
      "angle": 349.997,
      "layer": 5,
      "closed_by": [
        40
      ]
    },
    {
      "id": 41,
      "x": -0.307,
      "y": 0.587,
      "angle": 9.998,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 0.331,
      "y": 0.591,
      "angle": 349.997,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 0.004,
      "y": 0.944,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 37,
      "x": 0.008,
      "y": -0.008,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 36,
      "x": 0.008,
      "y": -0.414,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 35,
      "x": 1.23,
      "y": 0.172,
      "angle": 25.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -1.21,
      "y": 0.17,
      "angle": 335.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 1.289,
      "y": 0.652,
      "angle": 25.0,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 32,
      "x": -1.276,
      "y": 0.652,
      "angle": 335.0,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 31,
      "x": 1.526,
      "y": 0.768,
      "angle": 25.0,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 30,
      "x": -1.521,
      "y": 0.763,
      "angle": 335.0,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 29,
      "x": 1.639,
      "y": -0.074,
      "angle": 25.0,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 28,
      "x": -1.621,
      "y": -0.081,
      "angle": 335.0,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 27,
      "x": 1.875,
      "y": 0.034,
      "angle": 25.0,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 26,
      "x": -1.865,
      "y": 0.028,
      "angle": 335.0,
      "layer": 1,
      "closed_by": [
        28
      ]
    },
    {
      "id": 39,
      "x": 0.008,
      "y": 0.537,
      "layer": 5,
      "closed_by": [
        41,
        40
      ]
    }
  ],
  "cards_in_layout_amount": 26,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 55.4,
    "star2": 28.3,
    "star3": 15.6
  }
}