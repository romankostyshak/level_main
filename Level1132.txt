{
  "layout_cards": [
    {
      "id": 49,
      "x": -0.333,
      "y": -0.569,
      "layer": 1,
      "closed_by": [
        46,
        40
      ]
    },
    {
      "id": 48,
      "x": 0.333,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        47,
        40
      ]
    },
    {
      "id": 47,
      "x": 0.574,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 46,
      "x": -0.569,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 45,
      "x": 0.813,
      "y": 0.061,
      "layer": 3,
      "closed_by": [
        44,
        35
      ]
    },
    {
      "id": 50,
      "x": -0.813,
      "y": 0.059,
      "layer": 3,
      "closed_by": [
        43,
        34
      ]
    },
    {
      "id": 44,
      "x": 1.054,
      "y": 0.46,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 43,
      "x": -1.052,
      "y": 0.46,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 42,
      "x": 1.294,
      "y": 0.859,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 41,
      "x": -1.292,
      "y": 0.86,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 40,
      "y": -0.495,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 39,
      "y": -0.093,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 35,
      "x": 1.054,
      "y": -0.351,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 34,
      "x": -1.054,
      "y": -0.351,
      "layer": 4,
      "closed_by": [
        32
      ]
    },
    {
      "id": 33,
      "x": 1.057,
      "y": -0.573,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 32,
      "x": -1.054,
      "y": -0.574,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 31,
      "x": 1.264,
      "y": -0.573,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -1.266,
      "y": -0.574,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 0.001,
      "y": 0.298,
      "layer": 4,
      "closed_by": [
        28,
        29
      ]
    },
    {
      "id": 28,
      "x": -0.261,
      "y": 0.702,
      "layer": 5,
      "closed_by": [
        36
      ]
    },
    {
      "id": 29,
      "x": 0.263,
      "y": 0.703,
      "layer": 5,
      "closed_by": [
        36
      ]
    },
    {
      "id": 36,
      "y": 0.939,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 22,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 33.0,
    "star2": 1.7,
    "star3": 65.1
  }
}