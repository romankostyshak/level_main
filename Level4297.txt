{
  "layout_cards": [
    {
      "id": 21,
      "x": 1.659,
      "y": -0.119,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        23,
        25
      ]
    },
    {
      "id": 22,
      "x": -1.659,
      "y": -0.119,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        26
      ]
    },
    {
      "id": 23,
      "x": 1.659,
      "y": 0.72,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        27
      ]
    },
    {
      "id": 24,
      "x": -1.457,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 25,
      "x": 1.46,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 26,
      "x": -1.457,
      "y": -0.179,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 27,
      "x": 1.46,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 28,
      "x": -1.059,
      "y": -0.179,
      "angle": 300.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 29,
      "x": 1.059,
      "y": -0.179,
      "angle": 60.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 1.059,
      "y": 0.777,
      "angle": 300.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 31,
      "y": 0.939,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 32,
      "y": -0.337,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -0.18,
      "y": 0.56,
      "angle": 330.0,
      "layer": 6,
      "closed_by": [
        31
      ]
    },
    {
      "id": 34,
      "x": 0.18,
      "y": 0.039,
      "angle": 330.0,
      "layer": 6,
      "closed_by": [
        32
      ]
    },
    {
      "id": 35,
      "x": 0.18,
      "y": 0.56,
      "angle": 30.0,
      "layer": 5,
      "closed_by": [
        33,
        34
      ]
    },
    {
      "id": 36,
      "x": -1.059,
      "y": 0.777,
      "angle": 60.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 37,
      "x": -1.659,
      "y": 0.72,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        22,
        24
      ]
    },
    {
      "id": 38,
      "x": -0.18,
      "y": 0.039,
      "angle": 30.0,
      "layer": 5,
      "closed_by": [
        33,
        34
      ]
    },
    {
      "id": 39,
      "x": -0.458,
      "y": 0.72,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [
        35,
        36,
        38
      ]
    },
    {
      "id": 40,
      "x": 0.46,
      "y": -0.119,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [
        29,
        35,
        38
      ]
    },
    {
      "id": 41,
      "x": -0.458,
      "y": -0.119,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        28,
        39
      ]
    },
    {
      "id": 42,
      "x": 0.46,
      "y": 0.72,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        30,
        40
      ]
    },
    {
      "id": 43,
      "x": -0.66,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        38,
        41
      ]
    },
    {
      "id": 44,
      "x": -0.66,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        33,
        39
      ]
    },
    {
      "id": 45,
      "x": -0.499,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 46,
      "x": 0.66,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        35,
        42
      ]
    },
    {
      "id": 47,
      "x": 0.66,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        34,
        40
      ]
    },
    {
      "id": 48,
      "x": 0.5,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 49,
      "x": 0.5,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 50,
      "x": -0.499,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        44
      ]
    }
  ],
  "cards_in_layout_amount": 30,
  "cards_in_deck_amount": 28,
  "layers_in_level": 7,
  "open_cards": 6,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 49.5,
    "star2": 48.9,
    "star3": 1.4
  }
}