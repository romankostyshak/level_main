{
  "layout_cards": [
    {
      "id": 51,
      "x": -1.911,
      "y": 0.379,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 50,
      "x": -1.365,
      "y": 0.379,
      "layer": 2,
      "closed_by": [
        39,
        38
      ]
    },
    {
      "id": 49,
      "x": -0.809,
      "y": 0.379,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 48,
      "x": 0.814,
      "y": 0.379,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 47,
      "x": 1.366,
      "y": 0.379,
      "layer": 2,
      "closed_by": [
        41,
        40
      ]
    },
    {
      "id": 46,
      "x": 1.929,
      "y": 0.379,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 45,
      "x": -1.085,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        50,
        49
      ]
    },
    {
      "id": 44,
      "x": -1.649,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        51,
        50
      ]
    },
    {
      "id": 43,
      "x": 1.072,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        48,
        47
      ]
    },
    {
      "id": 42,
      "x": 1.659,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        47,
        46
      ]
    },
    {
      "id": 41,
      "x": 1.082,
      "y": 0.54,
      "layer": 3,
      "closed_by": [
        24,
        22
      ]
    },
    {
      "id": 40,
      "x": 1.661,
      "y": 0.541,
      "layer": 3,
      "closed_by": [
        23,
        22
      ]
    },
    {
      "id": 39,
      "x": -1.077,
      "y": 0.54,
      "layer": 3,
      "closed_by": [
        25,
        21
      ]
    },
    {
      "id": 38,
      "x": -1.644,
      "y": 0.537,
      "layer": 3,
      "closed_by": [
        26,
        21
      ]
    },
    {
      "id": 36,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 32,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        34
      ]
    },
    {
      "id": 34,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 37,
      "y": 0.786,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 28,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 35,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 33,
      "y": -0.333,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 31,
      "y": -0.398,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 27,
      "y": 0.298,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -1.911,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 25,
      "x": -0.805,
      "y": 0.781,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 24,
      "x": 0.814,
      "y": 0.781,
      "layer": 4,
      "closed_by": [
        19
      ]
    },
    {
      "id": 23,
      "x": 1.932,
      "y": 0.782,
      "layer": 4,
      "closed_by": [
        18
      ]
    },
    {
      "id": 22,
      "x": 1.366,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        19,
        18
      ]
    },
    {
      "id": 21,
      "x": -1.363,
      "y": 0.782,
      "layer": 4,
      "closed_by": [
        20,
        29
      ]
    },
    {
      "id": 20,
      "x": -1.644,
      "y": 0.939,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -1.07,
      "y": 0.939,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 19,
      "x": 1.082,
      "y": 0.939,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 18,
      "x": 1.664,
      "y": 0.939,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 33,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 45.5,
    "star2": 51.4,
    "star3": 2.8
  }
}