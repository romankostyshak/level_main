{
  "layout_cards": [
    {
      "id": 3,
      "x": 1.827,
      "y": 0.859,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 4,
      "x": 1.827,
      "y": 0.05,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 5,
      "x": -1.664,
      "y": -0.462,
      "layer": 5,
      "closed_by": [
        6
      ]
    },
    {
      "id": 6,
      "x": -1.825,
      "y": 0.052,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 7,
      "x": -1.824,
      "y": 0.86,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 8,
      "x": 1.672,
      "y": -0.453,
      "layer": 5,
      "closed_by": [
        4
      ]
    },
    {
      "id": 9,
      "x": 1.672,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        8
      ]
    },
    {
      "id": 10,
      "x": -0.81,
      "y": 0.939,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 11,
      "x": 0.814,
      "y": 0.939,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 12,
      "x": 1.019,
      "y": 0.46,
      "layer": 5,
      "closed_by": [
        11
      ]
    },
    {
      "id": 13,
      "x": -1.664,
      "y": 0.349,
      "layer": 5,
      "closed_by": [
        6,
        7
      ]
    },
    {
      "id": 14,
      "x": 1.667,
      "y": 0.358,
      "layer": 5,
      "closed_by": [
        3,
        4
      ]
    },
    {
      "id": 15,
      "x": -0.813,
      "y": -0.574,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 16,
      "x": -1.016,
      "y": 0.46,
      "layer": 5,
      "closed_by": [
        10
      ]
    },
    {
      "id": 17,
      "x": -1.664,
      "y": -0.574,
      "layer": 4,
      "closed_by": [
        5
      ]
    },
    {
      "id": 18,
      "x": 0.814,
      "y": -0.574,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 19,
      "x": -1.052,
      "y": -0.574,
      "layer": 5,
      "closed_by": [
        15
      ]
    },
    {
      "id": 20,
      "x": 1.054,
      "y": -0.574,
      "layer": 5,
      "closed_by": [
        18
      ]
    },
    {
      "id": 21,
      "x": 1.133,
      "y": -0.574,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 22,
      "x": -0.266,
      "y": -0.017,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 0.27,
      "y": -0.017,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 0.27,
      "y": 0.939,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 25,
      "x": -0.261,
      "y": 0.939,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -0.492,
      "y": -0.017,
      "layer": 5,
      "closed_by": [
        15,
        22
      ]
    },
    {
      "id": 27,
      "x": 0.493,
      "y": -0.017,
      "layer": 5,
      "closed_by": [
        18,
        23
      ]
    },
    {
      "id": 28,
      "x": -0.49,
      "y": 0.939,
      "layer": 5,
      "closed_by": [
        10,
        25
      ]
    },
    {
      "id": 29,
      "x": 1.667,
      "y": 0.458,
      "layer": 4,
      "closed_by": [
        14
      ]
    },
    {
      "id": 30,
      "x": 0.652,
      "y": 0.46,
      "layer": 4,
      "closed_by": [
        12,
        27,
        36
      ]
    },
    {
      "id": 31,
      "x": 0.814,
      "y": -0.012,
      "layer": 3,
      "closed_by": [
        21,
        30
      ]
    },
    {
      "id": 32,
      "x": 1.427,
      "y": -0.013,
      "layer": 3,
      "closed_by": [
        9,
        12,
        21,
        29
      ]
    },
    {
      "id": 33,
      "x": 1.427,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        12,
        29
      ]
    },
    {
      "id": 34,
      "x": 0.814,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 35,
      "x": 0.81,
      "y": 0.458,
      "layer": 2,
      "closed_by": [
        31,
        34
      ]
    },
    {
      "id": 36,
      "x": 0.493,
      "y": 0.939,
      "layer": 5,
      "closed_by": [
        11,
        24
      ]
    },
    {
      "id": 37,
      "x": -1.123,
      "y": -0.574,
      "layer": 4,
      "closed_by": [
        19
      ]
    },
    {
      "id": 38,
      "x": 1.427,
      "y": 0.458,
      "layer": 2,
      "closed_by": [
        32,
        33
      ]
    },
    {
      "id": 39,
      "x": 1.128,
      "y": -0.093,
      "layer": 1,
      "closed_by": [
        35,
        38
      ]
    },
    {
      "id": 40,
      "x": 1.12,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        35,
        38
      ]
    },
    {
      "id": 41,
      "x": -1.661,
      "y": 0.458,
      "layer": 4,
      "closed_by": [
        13
      ]
    },
    {
      "id": 42,
      "x": -0.652,
      "y": 0.46,
      "layer": 4,
      "closed_by": [
        16,
        26,
        28
      ]
    },
    {
      "id": 43,
      "x": -1.424,
      "y": -0.017,
      "layer": 3,
      "closed_by": [
        16,
        17,
        37,
        41
      ]
    },
    {
      "id": 44,
      "x": -0.81,
      "y": -0.014,
      "layer": 3,
      "closed_by": [
        37,
        42
      ]
    },
    {
      "id": 45,
      "x": -0.81,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 46,
      "x": -0.81,
      "y": 0.46,
      "layer": 2,
      "closed_by": [
        44,
        45
      ]
    },
    {
      "id": 47,
      "x": -1.424,
      "y": 0.462,
      "layer": 2,
      "closed_by": [
        43,
        49
      ]
    },
    {
      "id": 48,
      "x": -1.123,
      "y": -0.093,
      "layer": 1,
      "closed_by": [
        46,
        47
      ]
    },
    {
      "id": 49,
      "x": -1.424,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        16,
        41
      ]
    },
    {
      "id": 51,
      "x": -1.12,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        46,
        47
      ]
    }
  ],
  "cards_in_layout_amount": 48,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 1.0,
    "star2": 41.9,
    "star3": 56.3
  }
}