{
  "layout_cards": [
    {
      "id": 19,
      "x": -2.341,
      "y": -0.337,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 20,
      "x": -2.255,
      "y": -0.335,
      "layer": 3,
      "closed_by": [
        19
      ]
    },
    {
      "id": 21,
      "x": -2.164,
      "y": -0.34,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 22,
      "x": -1.082,
      "y": 0.781,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 23,
      "x": -1.161,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 24,
      "x": -1.261,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 25,
      "x": -1.345,
      "y": 0.781,
      "layer": 1,
      "closed_by": [
        24
      ]
    },
    {
      "id": 26,
      "x": -2.085,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        21
      ]
    },
    {
      "id": 27,
      "x": -2.733,
      "y": 0.54,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 28,
      "x": -2.894,
      "y": 0.708,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 29,
      "x": -2.144,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        30
      ]
    },
    {
      "id": 30,
      "x": -2.068,
      "y": 0.628,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 31,
      "x": -2.818,
      "y": 0.629,
      "layer": 2,
      "closed_by": [
        27
      ]
    },
    {
      "id": 32,
      "x": -1.993,
      "y": 0.545,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -1.268,
      "y": -0.114,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 34,
      "x": -0.545,
      "y": -0.104,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 35,
      "x": -1.343,
      "y": -0.18,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 36,
      "x": -0.632,
      "y": -0.18,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 37,
      "x": -1.424,
      "y": -0.256,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -0.703,
      "y": -0.254,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 39,
      "x": 1.445,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        41,
        42,
        43,
        44
      ],
      "card_type": 8
    },
    {
      "id": 40,
      "x": 0.893,
      "y": 1.024,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 1.842,
      "y": 0.643,
      "angle": 335.0,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 42,
      "x": 1.062,
      "y": 0.643,
      "angle": 25.0,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 43,
      "x": 1.095,
      "y": -0.266,
      "angle": 340.0,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 44,
      "x": 1.817,
      "y": -0.256,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 45,
      "x": 0.972,
      "y": 0.86,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 46,
      "x": 0.975,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        49
      ]
    },
    {
      "id": 47,
      "x": 1.983,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 48,
      "x": 1.996,
      "y": 0.92,
      "layer": 3,
      "closed_by": [
        51
      ]
    },
    {
      "id": 49,
      "x": 0.893,
      "y": -0.497,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 50,
      "x": 2.065,
      "y": -0.499,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 51,
      "x": 2.065,
      "y": 1.019,
      "layer": 4,
      "closed_by": []
    }
  ],
  "level_mechanic": 1,
  "free_cards_from_stickers": [
    39,
    40,
    41,
    42,
    43,
    44,
    45,
    46,
    47,
    48,
    49,
    50,
    51
  ],
  "cards_in_layout_amount": 33,
  "cards_in_deck_amount": 22,
  "stars_statistic": {
    "star1": 69.8,
    "star2": 22.0,
    "star3": 7.5
  }
}