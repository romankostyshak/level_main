{
  "layout_cards": [
    {
      "id": 49,
      "x": -0.948,
      "y": -0.008,
      "layer": 2,
      "closed_by": [
        47,
        35
      ]
    },
    {
      "id": 48,
      "x": -0.981,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 47,
      "x": -0.679,
      "y": 0.141,
      "layer": 3,
      "closed_by": [
        46,
        28
      ]
    },
    {
      "id": 46,
      "x": -0.412,
      "y": -0.014,
      "layer": 4,
      "closed_by": [
        45
      ]
    },
    {
      "id": 45,
      "x": -0.37,
      "y": -0.412,
      "layer": 5,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": -0.68,
      "y": -0.577,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 44,
      "x": 0.986,
      "y": -0.414,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 43,
      "x": 0.947,
      "y": -0.017,
      "layer": 2,
      "closed_by": [
        42,
        38
      ]
    },
    {
      "id": 42,
      "x": 0.679,
      "y": 0.142,
      "layer": 3,
      "closed_by": [
        41,
        36
      ]
    },
    {
      "id": 41,
      "x": 0.412,
      "y": -0.017,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 40,
      "x": 0.37,
      "y": -0.416,
      "layer": 5,
      "closed_by": [
        39
      ]
    },
    {
      "id": 39,
      "x": 0.68,
      "y": -0.574,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -1.21,
      "y": 0.224,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 38,
      "x": 1.212,
      "y": 0.221,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 34,
      "x": 1.376,
      "y": 0.379,
      "layer": 4,
      "closed_by": [
        32
      ]
    },
    {
      "id": 33,
      "x": -1.37,
      "y": 0.379,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 32,
      "x": 1.588,
      "y": 0.061,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 31,
      "x": -1.588,
      "y": 0.059,
      "layer": 5,
      "closed_by": [
        29
      ]
    },
    {
      "id": 30,
      "x": 1.347,
      "y": -0.493,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -1.343,
      "y": -0.5,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 28,
      "x": -0.624,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 36,
      "x": 0.625,
      "y": 0.856,
      "layer": 4,
      "closed_by": [
        26
      ]
    },
    {
      "id": 27,
      "x": -0.492,
      "y": 0.939,
      "layer": 5,
      "closed_by": [
        25
      ]
    },
    {
      "id": 26,
      "x": 0.497,
      "y": 0.939,
      "layer": 5,
      "closed_by": [
        24
      ]
    },
    {
      "id": 25,
      "x": -0.333,
      "y": 0.86,
      "layer": 6,
      "closed_by": [
        23
      ]
    },
    {
      "id": 24,
      "x": 0.333,
      "y": 0.859,
      "layer": 6,
      "closed_by": [
        23
      ]
    },
    {
      "id": 23,
      "x": 0.004,
      "y": 0.785,
      "layer": 7,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 27,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 68.4,
    "star2": 16.3,
    "star3": 14.3
  }
}