{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.261,
      "y": -0.108,
      "layer": 2,
      "closed_by": [
        49,
        28
      ]
    },
    {
      "id": 50,
      "x": -0.435,
      "y": -0.059,
      "angle": 25.0,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 49,
      "x": 0.43,
      "y": -0.059,
      "angle": 335.0,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 48,
      "x": 0.605,
      "y": 0.312,
      "angle": 335.0,
      "layer": 4,
      "closed_by": [
        46
      ]
    },
    {
      "id": 47,
      "x": -0.609,
      "y": 0.312,
      "angle": 25.0,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 46,
      "x": 0.781,
      "y": 0.684,
      "angle": 335.0,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 45,
      "x": -1.024,
      "y": -0.442,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 44,
      "x": -1.276,
      "y": 0.46,
      "angle": 25.0,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 43,
      "x": 1.095,
      "y": 0.089,
      "angle": 335.0,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 42,
      "x": 1.026,
      "y": -0.428,
      "angle": 9.998,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 41,
      "x": 1.269,
      "y": 0.46,
      "angle": 335.0,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 40,
      "x": -1.095,
      "y": 0.082,
      "angle": 24.999,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 39,
      "x": -1.281,
      "y": -0.395,
      "angle": 349.997,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 38,
      "x": 1.294,
      "y": -0.379,
      "angle": 9.998,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 37,
      "x": -1.539,
      "y": -0.349,
      "angle": 349.997,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -0.004,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        51,
        35
      ]
    },
    {
      "id": 35,
      "x": -0.259,
      "y": -0.107,
      "layer": 2,
      "closed_by": [
        50,
        28
      ]
    },
    {
      "id": 34,
      "x": 1.557,
      "y": -0.333,
      "angle": 9.998,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -0.785,
      "y": 0.689,
      "angle": 25.0,
      "layer": 5,
      "closed_by": [
        32
      ]
    },
    {
      "id": 32,
      "x": -0.414,
      "y": 0.865,
      "angle": 25.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 0.412,
      "y": 0.856,
      "angle": 335.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 30,
      "y": 0.939,
      "layer": 5,
      "closed_by": [
        32,
        31
      ]
    },
    {
      "id": 29,
      "y": 0.759,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 28,
      "y": 0.56,
      "layer": 3,
      "closed_by": [
        29
      ]
    }
  ],
  "cards_in_layout_amount": 24,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 54.3,
    "star2": 6.8,
    "star3": 38.5
  }
}