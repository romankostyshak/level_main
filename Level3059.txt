{
  "layout_cards": [
    {
      "id": 13,
      "x": 2.707,
      "y": 1.019,
      "layer": 5,
      "closed_by": [
        42
      ]
    },
    {
      "id": 14,
      "x": -0.892,
      "y": 0.54,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 15,
      "x": -0.263,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        17,
        30
      ]
    },
    {
      "id": 16,
      "x": 0.268,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        18,
        30
      ]
    },
    {
      "id": 17,
      "x": -0.574,
      "y": 0.781,
      "layer": 2,
      "closed_by": [
        14,
        31
      ]
    },
    {
      "id": 18,
      "x": 0.574,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        19,
        32
      ]
    },
    {
      "id": 19,
      "x": 0.893,
      "y": 0.54,
      "layer": 3,
      "closed_by": [
        20
      ]
    },
    {
      "id": 20,
      "x": 1.105,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 21,
      "x": -1.105,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 22,
      "x": 1.427,
      "y": 1.019,
      "layer": 5,
      "closed_by": [
        24
      ]
    },
    {
      "id": 23,
      "x": -1.424,
      "y": 1.019,
      "layer": 5,
      "closed_by": [
        25
      ]
    },
    {
      "id": 24,
      "x": 1.746,
      "y": 0.777,
      "layer": 6,
      "closed_by": [
        26
      ]
    },
    {
      "id": 25,
      "x": -1.743,
      "y": 0.782,
      "layer": 6,
      "closed_by": [
        29
      ]
    },
    {
      "id": 26,
      "x": 2.068,
      "y": 0.54,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 2.069,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        49
      ]
    },
    {
      "id": 28,
      "x": -0.892,
      "y": -0.093,
      "layer": 2,
      "closed_by": [
        14
      ]
    },
    {
      "id": 29,
      "x": -2.065,
      "y": 0.541,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 30,
      "y": 0.781,
      "layer": 2,
      "closed_by": [
        31,
        32
      ]
    },
    {
      "id": 31,
      "x": -0.333,
      "y": 0.544,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 32,
      "x": 0.333,
      "y": 0.544,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 33,
      "x": 0.001,
      "y": 0.298,
      "layer": 4,
      "closed_by": [
        34,
        35
      ]
    },
    {
      "id": 34,
      "x": 0.335,
      "y": -0.012,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -0.333,
      "y": -0.014,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 0.893,
      "y": -0.093,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 37,
      "x": 0.893,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 38,
      "x": -0.888,
      "y": -0.577,
      "layer": 1,
      "closed_by": [
        28
      ]
    },
    {
      "id": 39,
      "x": -0.333,
      "y": -0.574,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 40,
      "x": 0.333,
      "y": -0.574,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 41,
      "x": -0.002,
      "y": -0.574,
      "layer": 3,
      "closed_by": [
        39,
        40
      ]
    },
    {
      "id": 42,
      "x": 2.384,
      "y": 0.777,
      "layer": 6,
      "closed_by": [
        26
      ]
    },
    {
      "id": 43,
      "x": -2.384,
      "y": 0.777,
      "layer": 6,
      "closed_by": [
        29
      ]
    },
    {
      "id": 44,
      "x": -2.625,
      "y": 1.019,
      "layer": 5,
      "closed_by": [
        43
      ]
    },
    {
      "id": 45,
      "x": 2.071,
      "y": -0.104,
      "layer": 6,
      "closed_by": [
        26
      ]
    },
    {
      "id": 46,
      "x": -2.062,
      "y": -0.096,
      "layer": 6,
      "closed_by": [
        29
      ]
    },
    {
      "id": 47,
      "x": 2.069,
      "y": -0.186,
      "layer": 5,
      "closed_by": [
        45
      ]
    },
    {
      "id": 48,
      "x": -2.065,
      "y": -0.172,
      "layer": 5,
      "closed_by": [
        46
      ]
    },
    {
      "id": 49,
      "x": 2.069,
      "y": -0.259,
      "layer": 4,
      "closed_by": [
        47
      ]
    },
    {
      "id": 50,
      "x": -2.065,
      "y": -0.256,
      "layer": 4,
      "closed_by": [
        48
      ]
    },
    {
      "id": 51,
      "x": -2.065,
      "y": -0.335,
      "layer": 3,
      "closed_by": [
        50
      ]
    }
  ],
  "cards_in_layout_amount": 39,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 2.7,
    "star2": 51.0,
    "star3": 45.6
  }
}