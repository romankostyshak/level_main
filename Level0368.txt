{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.99,
      "y": -0.165,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 50,
      "x": 1.616,
      "y": 0.652,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 49,
      "x": 1.616,
      "y": -0.193,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 48,
      "x": -1.621,
      "y": -0.187,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 47,
      "x": -0.398,
      "y": 1.047,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 46,
      "x": 0.981,
      "y": -0.326,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 45,
      "x": 0.98,
      "y": -0.172,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 44,
      "x": 0.98,
      "y": 0.791,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 43,
      "x": -0.989,
      "y": -0.326,
      "layer": 2,
      "closed_by": [
        51
      ]
    },
    {
      "id": 42,
      "y": 0.634,
      "layer": 2,
      "closed_by": [
        32,
        30
      ]
    },
    {
      "id": 41,
      "x": -0.398,
      "y": -0.587,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 40,
      "x": -1.621,
      "y": 0.231,
      "layer": 1,
      "closed_by": [
        48,
        39
      ]
    },
    {
      "id": 39,
      "x": -1.62,
      "y": 0.652,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 38,
      "x": -1.621,
      "y": 0.907,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 37,
      "x": 1.618,
      "y": 0.906,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 0.381,
      "y": 1.047,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 35,
      "x": 0.98,
      "y": -0.486,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 34,
      "x": 1.618,
      "y": -0.441,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -1.621,
      "y": -0.442,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 0.381,
      "y": 0.228,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 31,
      "x": 0.381,
      "y": -0.587,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -0.4,
      "y": 0.228,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 29,
      "x": 0.001,
      "y": -0.194,
      "layer": 4,
      "closed_by": [
        41,
        31
      ]
    },
    {
      "id": 28,
      "x": -0.99,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        25
      ]
    },
    {
      "id": 27,
      "x": -0.99,
      "y": -0.486,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 25,
      "x": -0.989,
      "y": 0.791,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 24,
      "x": -0.99,
      "y": 0.643,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 23,
      "x": 0.98,
      "y": 0.648,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 22,
      "x": 0.98,
      "y": 0.231,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 21,
      "x": 0.98,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 20,
      "x": 1.616,
      "y": 0.231,
      "layer": 1,
      "closed_by": [
        50,
        49
      ]
    },
    {
      "id": 26,
      "x": -0.99,
      "y": 0.231,
      "layer": 4,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 32,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 22.0,
    "star2": 65.5,
    "star3": 12.0
  }
}