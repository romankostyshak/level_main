{
  "layout_cards": [
    {
      "id": 23,
      "x": 1.024,
      "y": 0.46,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 24,
      "x": 0.386,
      "y": 0.458,
      "layer": 2,
      "closed_by": [
        26,
        37,
        38
      ]
    },
    {
      "id": 25,
      "x": -0.381,
      "y": 0.143,
      "layer": 2,
      "closed_by": [
        28,
        37,
        38
      ]
    },
    {
      "id": 26,
      "x": 0.703,
      "y": 0.46,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 27,
      "x": 0.004,
      "y": 0.303,
      "layer": 1,
      "closed_by": [
        24,
        25
      ]
    },
    {
      "id": 28,
      "x": -0.703,
      "y": 0.143,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 29,
      "x": -1.024,
      "y": 0.143,
      "layer": 4,
      "closed_by": [
        33
      ],
      "card_type": 3
    },
    {
      "id": 30,
      "x": 1.264,
      "y": 0.859,
      "layer": 5,
      "closed_by": [
        34
      ]
    },
    {
      "id": 33,
      "x": -1.264,
      "y": -0.256,
      "layer": 5,
      "closed_by": [
        35
      ]
    },
    {
      "id": 34,
      "x": 1.506,
      "y": 0.46,
      "layer": 6,
      "closed_by": [],
      "card_type": 2
    },
    {
      "id": 35,
      "x": -1.503,
      "y": 0.136,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -2.013,
      "y": -0.252,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 37,
      "x": 0.006,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 38,
      "x": 0.004,
      "y": -0.18,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 39,
      "x": 0.008,
      "y": 0.939,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 0.003,
      "y": -0.414,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 41,
      "x": -1.827,
      "y": 0.136,
      "layer": 5,
      "closed_by": [
        35
      ]
    },
    {
      "id": 42,
      "x": 1.824,
      "y": 0.46,
      "layer": 5,
      "closed_by": [
        34
      ]
    },
    {
      "id": 43,
      "x": 2.009,
      "y": 0.028,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 44,
      "x": 2.009,
      "y": 0.864,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 45,
      "x": -2.013,
      "y": 0.574,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 46,
      "x": 2.302,
      "y": 0.462,
      "layer": 3,
      "closed_by": [
        43,
        44
      ]
    },
    {
      "id": 47,
      "x": -2.305,
      "y": 0.143,
      "layer": 3,
      "closed_by": [
        36,
        45
      ]
    },
    {
      "id": 48,
      "x": 2.463,
      "y": 0.856,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 49,
      "x": 2.463,
      "y": 0.025,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 50,
      "x": -2.466,
      "y": 0.574,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 51,
      "x": -2.466,
      "y": -0.256,
      "layer": 2,
      "closed_by": [
        47
      ]
    }
  ],
  "cards_in_layout_amount": 27,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 49.0,
    "star2": 6.8,
    "star3": 43.4
  }
}