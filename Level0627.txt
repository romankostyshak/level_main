{
  "layout_cards": [
    {
      "id": 51,
      "x": -1.674,
      "y": 0.827,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": -1.672,
      "y": 0.832,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        49
      ]
    },
    {
      "id": 49,
      "x": -1.677,
      "y": 0.828,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 48,
      "x": 1.684,
      "y": 0.804,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "x": 1.672,
      "y": 0.81,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 46,
      "x": 1.667,
      "y": 0.814,
      "angle": 15.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 45,
      "x": -0.004,
      "y": -0.28,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": -0.008,
      "y": -0.273,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 43,
      "x": 0.004,
      "y": -0.275,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 42,
      "x": -1.672,
      "y": 0.828,
      "angle": 15.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 41,
      "y": -0.273,
      "angle": 15.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 1.669,
      "y": 0.81,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 39,
      "x": 0.002,
      "y": 0.75,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 38,
      "x": -0.092,
      "y": 0.953,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 37,
      "x": 0.096,
      "y": 0.953,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 36,
      "x": 1.69,
      "y": -0.303,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 35,
      "x": 1.608,
      "y": -0.508,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 34,
      "x": 1.799,
      "y": -0.5,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 33,
      "x": 0.006,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        25
      ]
    },
    {
      "id": 32,
      "x": 1.695,
      "y": -0.574,
      "layer": 4,
      "closed_by": [
        26
      ]
    },
    {
      "id": 31,
      "x": -1.69,
      "y": -0.31,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 30,
      "x": -1.784,
      "y": -0.509,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 29,
      "x": -1.585,
      "y": -0.509,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 28,
      "x": -1.694,
      "y": -0.574,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 27,
      "x": -1.69,
      "y": -0.312,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 26,
      "x": 1.694,
      "y": -0.3,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 0.004,
      "y": 0.753,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 27,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 60.8,
    "star2": 31.3,
    "star3": 7.2
  }
}