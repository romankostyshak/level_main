{
  "layout_cards": [
    {
      "id": 4,
      "x": 1.294,
      "y": 0.298,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 5,
      "x": 0.771,
      "y": 0.298,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 6,
      "x": -1.281,
      "y": 0.298,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 8,
      "x": -0.759,
      "y": 0.303,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 9,
      "x": -2.703,
      "y": 0.699,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 10,
      "x": 2.707,
      "y": -0.107,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 11,
      "x": 2.707,
      "y": 0.699,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 12,
      "x": 2.546,
      "y": 0.3,
      "layer": 4,
      "closed_by": [
        10,
        11
      ]
    },
    {
      "id": 13,
      "x": -2.305,
      "y": -0.108,
      "layer": 3,
      "closed_by": [
        17
      ]
    },
    {
      "id": 14,
      "x": -2.305,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        17
      ]
    },
    {
      "id": 15,
      "x": 2.308,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        12
      ]
    },
    {
      "id": 16,
      "x": -2.706,
      "y": -0.108,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 17,
      "x": -2.545,
      "y": 0.298,
      "layer": 4,
      "closed_by": [
        9,
        16
      ]
    },
    {
      "id": 18,
      "x": 2.309,
      "y": -0.107,
      "layer": 3,
      "closed_by": [
        12
      ]
    },
    {
      "id": 19,
      "x": -2.065,
      "y": -0.108,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 20,
      "x": -2.065,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 21,
      "x": 2.065,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        23
      ]
    },
    {
      "id": 22,
      "x": -1.904,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        13,
        14,
        36
      ]
    },
    {
      "id": 23,
      "x": 1.906,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        15,
        18,
        28
      ]
    },
    {
      "id": 24,
      "x": -1.664,
      "y": -0.104,
      "layer": 5,
      "closed_by": [
        6
      ]
    },
    {
      "id": 25,
      "x": -1.664,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        6
      ]
    },
    {
      "id": 26,
      "x": 1.664,
      "y": -0.104,
      "layer": 5,
      "closed_by": [
        4
      ]
    },
    {
      "id": 27,
      "x": 1.666,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        4
      ]
    },
    {
      "id": 28,
      "x": 1.585,
      "y": 0.3,
      "layer": 3,
      "closed_by": [
        31,
        32
      ]
    },
    {
      "id": 29,
      "x": -1.343,
      "y": -0.104,
      "layer": 4,
      "closed_by": [
        24,
        41
      ]
    },
    {
      "id": 30,
      "x": -1.345,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        25,
        42
      ]
    },
    {
      "id": 31,
      "x": 1.347,
      "y": -0.104,
      "layer": 4,
      "closed_by": [
        26,
        39
      ]
    },
    {
      "id": 32,
      "x": 1.348,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        27,
        40
      ]
    },
    {
      "id": 33,
      "x": -0.513,
      "y": -0.108,
      "layer": 5,
      "closed_by": [
        8
      ]
    },
    {
      "id": 34,
      "x": 0.509,
      "y": -0.108,
      "layer": 5,
      "closed_by": [
        5
      ]
    },
    {
      "id": 35,
      "x": 0.513,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        5
      ]
    },
    {
      "id": 36,
      "x": -1.582,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        29,
        30
      ]
    },
    {
      "id": 38,
      "x": -0.513,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        8
      ]
    },
    {
      "id": 39,
      "x": 1.026,
      "y": -0.103,
      "layer": 5,
      "closed_by": [
        4,
        5
      ]
    },
    {
      "id": 40,
      "x": 1.026,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        4,
        5
      ]
    },
    {
      "id": 41,
      "x": -1.023,
      "y": -0.104,
      "layer": 5,
      "closed_by": [
        6,
        8
      ]
    },
    {
      "id": 42,
      "x": -1.024,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        6,
        8
      ]
    },
    {
      "id": 43,
      "x": 0.786,
      "y": 0.298,
      "layer": 4,
      "closed_by": [
        34,
        35,
        39,
        40
      ]
    },
    {
      "id": 44,
      "x": 0.541,
      "y": 0.3,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 45,
      "x": -0.796,
      "y": 0.298,
      "layer": 4,
      "closed_by": [
        33,
        38,
        41,
        42
      ]
    },
    {
      "id": 46,
      "x": -0.532,
      "y": 0.3,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 47,
      "x": -0.263,
      "y": 0.3,
      "layer": 2,
      "closed_by": [
        33,
        38,
        46
      ]
    },
    {
      "id": 48,
      "x": -0.001,
      "y": -0.111,
      "layer": 1,
      "closed_by": [
        47,
        50
      ]
    },
    {
      "id": 49,
      "x": 0.002,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        47,
        50
      ]
    },
    {
      "id": 50,
      "x": 0.263,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        34,
        35,
        44
      ]
    },
    {
      "id": 51,
      "x": 2.065,
      "y": -0.104,
      "layer": 1,
      "closed_by": [
        23,
        26
      ]
    }
  ],
  "cards_in_layout_amount": 46,
  "cards_in_deck_amount": 32,
  "stars_statistic": {
    "star1": 3.0,
    "star2": 62.2,
    "star3": 34.6
  }
}