{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.972,
      "y": -0.411,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 50,
      "x": 2.075,
      "y": -0.411,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 49,
      "x": 0.972,
      "y": 0.004,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 48,
      "x": 2.069,
      "y": 0.004,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 47,
      "x": 1.202,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        49,
        45
      ]
    },
    {
      "id": 46,
      "x": 1.85,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        48,
        45
      ]
    },
    {
      "id": 45,
      "x": 1.518,
      "y": 0.004,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 44,
      "x": 1.526,
      "y": -0.412,
      "layer": 1,
      "closed_by": [
        47,
        46
      ]
    },
    {
      "id": 43,
      "x": -1.139,
      "y": -0.136,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -0.62,
      "y": -0.136,
      "layer": 1,
      "closed_by": [
        41
      ]
    },
    {
      "id": 41,
      "x": -0.879,
      "y": -0.136,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 40,
      "x": -0.638,
      "y": 0.74,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 39,
      "x": -1.439,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 38,
      "x": -0.898,
      "y": 0.74,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 37,
      "x": -1.139,
      "y": 0.74,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -1.439,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 35,
      "x": -1.439,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        43,
        37,
        34
      ]
    },
    {
      "id": 34,
      "x": -1.72,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        32,
        28
      ]
    },
    {
      "id": 33,
      "x": -1.957,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        34,
        31,
        30
      ]
    },
    {
      "id": 32,
      "x": -1.96,
      "y": 0.72,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -2.24,
      "y": -0.1,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 30,
      "x": -2.24,
      "y": 0.72,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 29,
      "x": -2.24,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 28,
      "x": -1.957,
      "y": -0.1,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 0.972,
      "y": 0.259,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 24,
      "x": 1.519,
      "y": 0.259,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 23,
      "x": 2.069,
      "y": 0.256,
      "layer": 4,
      "closed_by": [
        26
      ]
    },
    {
      "id": 22,
      "x": 0.972,
      "y": 0.699,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 21,
      "x": 1.519,
      "y": 0.699,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 26,
      "x": 2.069,
      "y": 0.702,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 30,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 38.9,
    "star2": 51.9,
    "star3": 8.4
  }
}