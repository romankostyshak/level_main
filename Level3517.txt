{
  "layout_cards": [
    {
      "id": 8,
      "x": -0.972,
      "y": -0.254,
      "layer": 3,
      "closed_by": [
        12
      ]
    },
    {
      "id": 9,
      "x": 0.972,
      "y": -0.254,
      "layer": 3,
      "closed_by": [
        11
      ]
    },
    {
      "id": 10,
      "x": 0.972,
      "y": 0.86,
      "layer": 3,
      "closed_by": [
        16
      ]
    },
    {
      "id": 11,
      "x": 1.054,
      "y": -0.172,
      "layer": 4,
      "closed_by": [
        15
      ]
    },
    {
      "id": 12,
      "x": -1.049,
      "y": -0.172,
      "layer": 4,
      "closed_by": [
        14
      ]
    },
    {
      "id": 13,
      "x": -1.049,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        14
      ]
    },
    {
      "id": 14,
      "x": -1.291,
      "y": 0.3,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 15,
      "x": 1.292,
      "y": 0.3,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 16,
      "x": 1.054,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        15
      ]
    },
    {
      "id": 17,
      "x": -0.972,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        13
      ]
    },
    {
      "id": 18,
      "x": -1.824,
      "y": 0.3,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 19,
      "x": 1.825,
      "y": 0.298,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 20,
      "x": -0.305,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        23
      ]
    },
    {
      "id": 21,
      "x": -0.305,
      "y": 0.54,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 22,
      "x": 0.305,
      "y": 0.541,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 23,
      "y": 1.021,
      "layer": 2,
      "closed_by": [
        21,
        22
      ]
    },
    {
      "id": 24,
      "x": 0.307,
      "y": 0.059,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 25,
      "x": -0.303,
      "y": 0.059,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 26,
      "x": 0.307,
      "y": -0.416,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 27,
      "x": -0.303,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 28,
      "x": 0.307,
      "y": -0.097,
      "layer": 5,
      "closed_by": [
        24
      ]
    },
    {
      "id": 29,
      "x": -1.587,
      "y": -0.172,
      "layer": 4,
      "closed_by": [
        14,
        18
      ]
    },
    {
      "id": 30,
      "x": 1.585,
      "y": -0.165,
      "layer": 4,
      "closed_by": [
        15,
        19
      ]
    },
    {
      "id": 31,
      "x": -1.585,
      "y": 0.782,
      "layer": 4,
      "closed_by": [
        14,
        18
      ]
    },
    {
      "id": 32,
      "x": 1.582,
      "y": 0.782,
      "layer": 4,
      "closed_by": [
        15,
        19
      ]
    },
    {
      "id": 33,
      "x": -1.506,
      "y": -0.256,
      "layer": 3,
      "closed_by": [
        12,
        29
      ]
    },
    {
      "id": 34,
      "x": 1.506,
      "y": -0.256,
      "layer": 3,
      "closed_by": [
        11,
        30
      ]
    },
    {
      "id": 35,
      "x": 1.503,
      "y": 0.865,
      "layer": 3,
      "closed_by": [
        16,
        32
      ]
    },
    {
      "id": 36,
      "x": -0.303,
      "y": -0.1,
      "layer": 5,
      "closed_by": [
        25
      ]
    },
    {
      "id": 37,
      "x": -0.303,
      "y": -0.259,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 38,
      "x": -1.506,
      "y": 0.861,
      "layer": 3,
      "closed_by": [
        13,
        31
      ]
    },
    {
      "id": 39,
      "x": 0.002,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        26,
        27
      ]
    },
    {
      "id": 40,
      "x": 0.307,
      "y": -0.261,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 41,
      "x": 1.424,
      "y": -0.335,
      "layer": 2,
      "closed_by": [
        9,
        34
      ]
    },
    {
      "id": 42,
      "x": 1.424,
      "y": 0.944,
      "layer": 2,
      "closed_by": [
        10,
        35
      ]
    },
    {
      "id": 43,
      "x": -1.424,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        8,
        33
      ]
    },
    {
      "id": 44,
      "x": -1.424,
      "y": 0.944,
      "layer": 2,
      "closed_by": [
        17,
        38
      ]
    },
    {
      "id": 45,
      "x": 1.373,
      "y": -0.416,
      "layer": 1,
      "closed_by": [
        41
      ]
    },
    {
      "id": 46,
      "x": -1.373,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 47,
      "x": 1.373,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 48,
      "x": 0.305,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        23
      ]
    },
    {
      "id": 49,
      "x": 0.002,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 50,
      "x": -1.373,
      "y": -0.416,
      "layer": 1,
      "closed_by": [
        43
      ]
    }
  ],
  "cards_in_layout_amount": 43,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 4.8,
    "star2": 67.9,
    "star3": 26.5
  }
}