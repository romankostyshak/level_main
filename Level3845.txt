{
  "layout_cards": [
    {
      "id": 10,
      "x": -2.279,
      "y": 0.217,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 11,
      "x": 2.279,
      "y": 0.217,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 12,
      "x": 2.539,
      "y": 0.217,
      "layer": 1,
      "closed_by": [
        11
      ]
    },
    {
      "id": 13,
      "x": -2.019,
      "y": 0.217,
      "layer": 3,
      "closed_by": [
        10
      ]
    },
    {
      "id": 14,
      "x": 2.019,
      "y": 0.217,
      "layer": 3,
      "closed_by": [
        11
      ]
    },
    {
      "id": 15,
      "x": -1.819,
      "y": 0.699,
      "angle": 295.0,
      "layer": 2,
      "closed_by": [
        13,
        51
      ]
    },
    {
      "id": 16,
      "x": -1.825,
      "y": -0.256,
      "angle": 65.0,
      "layer": 2,
      "closed_by": [
        13,
        51
      ]
    },
    {
      "id": 17,
      "x": 1.82,
      "y": -0.259,
      "angle": 295.0,
      "layer": 2,
      "closed_by": [
        14,
        50
      ]
    },
    {
      "id": 18,
      "x": 1.815,
      "y": 0.699,
      "angle": 65.0,
      "layer": 2,
      "closed_by": [
        14,
        50
      ]
    },
    {
      "id": 19,
      "x": -2.539,
      "y": 0.217,
      "layer": 1,
      "closed_by": [
        10
      ]
    },
    {
      "id": 20,
      "x": -1.085,
      "y": -0.597,
      "angle": 65.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 21,
      "x": 1.08,
      "y": -0.605,
      "angle": 295.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -1.077,
      "y": 1.041,
      "angle": 295.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 1.074,
      "y": 1.046,
      "angle": 65.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 24,
      "x": -0.638,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        22,
        46
      ]
    },
    {
      "id": 25,
      "x": 0.638,
      "y": 0.72,
      "layer": 3,
      "closed_by": [
        23,
        47
      ]
    },
    {
      "id": 26,
      "x": 0.638,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        21,
        48
      ]
    },
    {
      "id": 27,
      "x": -0.898,
      "y": 0.638,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 28,
      "x": -0.898,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        32,
        46
      ]
    },
    {
      "id": 29,
      "x": 0.898,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        26,
        47
      ]
    },
    {
      "id": 30,
      "x": 0.898,
      "y": 0.638,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 31,
      "x": -1.279,
      "y": 0.72,
      "layer": 1,
      "closed_by": [
        15,
        22,
        27,
        46
      ]
    },
    {
      "id": 32,
      "x": -0.638,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        20,
        49
      ]
    },
    {
      "id": 33,
      "x": -1.279,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        16,
        20,
        28,
        49
      ]
    },
    {
      "id": 34,
      "x": 1.279,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        17,
        21,
        29,
        48
      ]
    },
    {
      "id": 35,
      "x": 1.279,
      "y": 0.72,
      "layer": 1,
      "closed_by": [
        18,
        23,
        30,
        47
      ]
    },
    {
      "id": 36,
      "y": 1.019,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 37,
      "x": -0.379,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        24,
        36
      ]
    },
    {
      "id": 38,
      "x": 0.379,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        25,
        36
      ]
    },
    {
      "id": 39,
      "x": -0.638,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        27,
        37
      ]
    },
    {
      "id": 40,
      "x": -0.379,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        32,
        43
      ]
    },
    {
      "id": 41,
      "x": 0.379,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        26,
        43
      ]
    },
    {
      "id": 42,
      "x": 0.638,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        29,
        41
      ]
    },
    {
      "id": 43,
      "y": -0.578,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 44,
      "x": -0.638,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        28,
        40
      ]
    },
    {
      "id": 45,
      "x": 0.638,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        30,
        38
      ]
    },
    {
      "id": 46,
      "x": -0.859,
      "y": 0.578,
      "angle": 295.0,
      "layer": 4,
      "closed_by": [
        49
      ]
    },
    {
      "id": 47,
      "x": 0.86,
      "y": 0.578,
      "angle": 65.0,
      "layer": 4,
      "closed_by": [
        48
      ]
    },
    {
      "id": 48,
      "x": 0.86,
      "y": -0.136,
      "angle": 295.0,
      "layer": 5,
      "closed_by": [
        50
      ]
    },
    {
      "id": 49,
      "x": -0.859,
      "y": -0.136,
      "angle": 65.0,
      "layer": 5,
      "closed_by": [
        51
      ]
    },
    {
      "id": 50,
      "x": 1.279,
      "y": 0.217,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -1.279,
      "y": 0.217,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 42,
  "cards_in_deck_amount": 32,
  "layers_in_level": 6,
  "open_cards": 10,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 29.3,
    "star2": 65.3,
    "star3": 4.7
  }
}