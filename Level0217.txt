{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.319,
      "y": -0.238,
      "layer": 4,
      "closed_by": [
        50,
        40
      ]
    },
    {
      "id": 50,
      "y": -0.574,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 49,
      "x": 0.544,
      "y": -0.574,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 48,
      "x": -1.62,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 47,
      "x": -1.08,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        43,
        41
      ]
    },
    {
      "id": 46,
      "x": 0.319,
      "y": -0.238,
      "layer": 4,
      "closed_by": [
        50,
        49
      ]
    },
    {
      "id": 45,
      "x": -1.62,
      "y": 0.259,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": -1.401,
      "y": -0.238,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 43,
      "x": -1.401,
      "y": 0.697,
      "layer": 2,
      "closed_by": [
        45,
        22
      ]
    },
    {
      "id": 42,
      "x": -1.08,
      "y": -0.574,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 41,
      "x": -0.861,
      "y": 0.697,
      "layer": 2,
      "closed_by": [
        39,
        22
      ]
    },
    {
      "id": 40,
      "x": -0.537,
      "y": -0.574,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -0.537,
      "y": 0.259,
      "layer": 3,
      "closed_by": [
        51,
        25
      ]
    },
    {
      "id": 38,
      "x": 0.319,
      "y": 0.697,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 37,
      "x": -0.319,
      "y": 0.697,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 36,
      "x": 0.86,
      "y": -0.238,
      "layer": 4,
      "closed_by": [
        49,
        34
      ]
    },
    {
      "id": 35,
      "x": 0.86,
      "y": 0.697,
      "layer": 2,
      "closed_by": [
        24,
        23
      ]
    },
    {
      "id": 34,
      "x": 1.08,
      "y": -0.574,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 1.406,
      "y": -0.238,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 32,
      "x": 1.406,
      "y": 0.697,
      "layer": 2,
      "closed_by": [
        31,
        24
      ]
    },
    {
      "id": 31,
      "x": 1.618,
      "y": 0.259,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 30,
      "x": -0.537,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        41,
        37
      ]
    },
    {
      "id": 29,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        38,
        37
      ]
    },
    {
      "id": 28,
      "x": 1.077,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        35,
        32
      ]
    },
    {
      "id": 27,
      "x": 1.618,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 26,
      "x": 0.544,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        38,
        35
      ]
    },
    {
      "id": 25,
      "x": -0.861,
      "y": -0.238,
      "layer": 4,
      "closed_by": [
        42,
        40
      ]
    },
    {
      "id": 24,
      "x": 1.077,
      "y": 0.259,
      "layer": 3,
      "closed_by": [
        36,
        33
      ],
      "card_type": 8
    },
    {
      "id": 23,
      "x": 0.537,
      "y": 0.259,
      "layer": 3,
      "closed_by": [
        46,
        36
      ]
    },
    {
      "id": 22,
      "x": -1.08,
      "y": 0.259,
      "layer": 3,
      "closed_by": [
        44,
        25
      ],
      "card_type": 8
    }
  ],
  "cards_in_layout_amount": 30,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 64.4,
    "star2": 13.8,
    "star3": 21.4
  }
}