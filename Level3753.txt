{
  "layout_cards": [
    {
      "id": 13,
      "x": 0.898,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 14,
      "x": -0.898,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        23
      ]
    },
    {
      "id": 15,
      "x": -0.379,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        21
      ]
    },
    {
      "id": 16,
      "x": 0.379,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        21
      ]
    },
    {
      "id": 17,
      "x": 0.898,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 18,
      "x": 0.379,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        24
      ]
    },
    {
      "id": 19,
      "x": -0.379,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        24
      ]
    },
    {
      "id": 20,
      "x": -0.898,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        23
      ]
    },
    {
      "id": 21,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        45,
        47
      ]
    },
    {
      "id": 22,
      "x": 0.898,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        33,
        34
      ]
    },
    {
      "id": 23,
      "x": -0.898,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        36,
        37
      ]
    },
    {
      "id": 24,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        45,
        47
      ]
    },
    {
      "id": 25,
      "x": 1.58,
      "y": 0.799,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 26,
      "x": -1.457,
      "y": 0.298,
      "layer": 4,
      "closed_by": [
        43,
        44
      ]
    },
    {
      "id": 27,
      "x": 1.5,
      "y": -0.136,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 28,
      "x": 1.577,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 29,
      "x": -1.58,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 30,
      "x": -1.58,
      "y": 0.799,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 31,
      "x": 1.539,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        27,
        35
      ]
    },
    {
      "id": 32,
      "x": -1.539,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        38,
        39
      ]
    },
    {
      "id": 33,
      "x": 0.898,
      "y": -0.136,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 34,
      "x": 0.898,
      "y": 0.74,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 35,
      "x": 1.5,
      "y": 0.759,
      "layer": 3,
      "closed_by": [
        40
      ],
      "card_type": 3
    },
    {
      "id": 36,
      "x": -0.898,
      "y": -0.136,
      "layer": 3,
      "closed_by": [
        51
      ]
    },
    {
      "id": 37,
      "x": -0.898,
      "y": 0.74,
      "layer": 3,
      "closed_by": [
        51
      ]
    },
    {
      "id": 38,
      "x": -1.498,
      "y": 0.759,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 39,
      "x": -1.498,
      "y": -0.136,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 40,
      "x": 1.46,
      "y": 0.298,
      "layer": 4,
      "closed_by": [
        41,
        42
      ]
    },
    {
      "id": 41,
      "x": 1.419,
      "y": -0.1,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 42,
      "x": 1.419,
      "y": 0.72,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 43,
      "x": -1.419,
      "y": -0.1,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 44,
      "x": -1.419,
      "y": 0.72,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 45,
      "x": 0.337,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        48,
        49
      ]
    },
    {
      "id": 46,
      "x": 0.898,
      "y": 0.298,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 47,
      "x": -0.337,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        48,
        49
      ]
    },
    {
      "id": 48,
      "y": -0.119,
      "layer": 4,
      "closed_by": [
        50
      ]
    },
    {
      "id": 49,
      "y": 0.72,
      "layer": 4,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "y": 0.319,
      "layer": 5,
      "closed_by": [],
      "card_type": 2
    },
    {
      "id": 51,
      "x": -0.898,
      "y": 0.298,
      "layer": 4,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 39,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 34.8,
    "star2": 61.6,
    "star3": 3.0
  }
}