{
  "layout_cards": [
    {
      "id": 45,
      "x": -1.343,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 44,
      "x": 0.497,
      "y": 0.057,
      "layer": 3,
      "closed_by": [
        41,
        36
      ]
    },
    {
      "id": 43,
      "x": -0.782,
      "y": -0.101,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 42,
      "x": -1.059,
      "y": -0.259,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 0.785,
      "y": -0.101,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 39,
      "x": 1.065,
      "y": -0.261,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -0.493,
      "y": 0.057,
      "layer": 3,
      "closed_by": [
        43,
        37
      ]
    },
    {
      "id": 37,
      "x": -0.256,
      "y": 0.136,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 36,
      "x": 0.263,
      "y": 0.136,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 35,
      "x": -0.256,
      "y": -0.418,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 0.263,
      "y": -0.421,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 0.263,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 32,
      "x": -0.25,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        30
      ]
    },
    {
      "id": 31,
      "x": 0.462,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 30,
      "x": -0.462,
      "y": 1.021,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 29,
      "x": 0.865,
      "y": 0.944,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 28,
      "x": -0.861,
      "y": 0.943,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 27,
      "x": 1.108,
      "y": 0.782,
      "layer": 4,
      "closed_by": [
        25
      ]
    },
    {
      "id": 26,
      "x": -1.103,
      "y": 0.785,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 25,
      "x": 1.348,
      "y": 0.625,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 24,
      "x": -1.342,
      "y": 0.615,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 49,
      "x": 1.345,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 50,
      "x": 1.059,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        41,
        49
      ]
    },
    {
      "id": 51,
      "x": 0.777,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        44,
        50
      ]
    },
    {
      "id": 40,
      "x": 0.495,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        51
      ]
    },
    {
      "id": 48,
      "x": -0.49,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "x": -0.777,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        38,
        46
      ]
    },
    {
      "id": 46,
      "x": -1.059,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        45,
        43
      ]
    }
  ],
  "cards_in_layout_amount": 28,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 52.3,
    "star2": 38.2,
    "star3": 8.6
  }
}