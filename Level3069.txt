{
  "layout_cards": [
    {
      "id": 3,
      "x": 0.006,
      "y": -0.5,
      "layer": 3,
      "closed_by": [
        17,
        20
      ]
    },
    {
      "id": 5,
      "x": 1.35,
      "y": -0.321,
      "angle": 45.0,
      "layer": 4,
      "closed_by": [
        48
      ]
    },
    {
      "id": 6,
      "x": 0.492,
      "y": -0.577,
      "layer": 1,
      "closed_by": [
        8,
        24
      ]
    },
    {
      "id": 7,
      "x": 1.08,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        8
      ]
    },
    {
      "id": 8,
      "x": 0.819,
      "y": -0.545,
      "angle": 9.998,
      "layer": 2,
      "closed_by": [
        9
      ]
    },
    {
      "id": 9,
      "x": 1.136,
      "y": -0.458,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        5,
        15
      ]
    },
    {
      "id": 10,
      "x": -0.49,
      "y": -0.577,
      "layer": 1,
      "closed_by": [
        12,
        23
      ]
    },
    {
      "id": 11,
      "x": -1.08,
      "y": -0.577,
      "layer": 1,
      "closed_by": [
        12
      ]
    },
    {
      "id": 12,
      "x": -0.799,
      "y": -0.546,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        13
      ]
    },
    {
      "id": 13,
      "x": -1.108,
      "y": -0.462,
      "angle": 340.0,
      "layer": 3,
      "closed_by": [
        14,
        16
      ]
    },
    {
      "id": 14,
      "x": -1.325,
      "y": -0.333,
      "angle": 320.0,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 15,
      "x": 0.597,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 16,
      "x": -0.597,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        17
      ]
    },
    {
      "id": 17,
      "x": -0.537,
      "y": -0.388,
      "angle": 349.997,
      "layer": 5,
      "closed_by": [
        21
      ]
    },
    {
      "id": 18,
      "x": -0.518,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        37,
        42
      ]
    },
    {
      "id": 19,
      "y": 0.46,
      "layer": 2,
      "closed_by": [
        25,
        30
      ]
    },
    {
      "id": 20,
      "x": 0.537,
      "y": -0.388,
      "angle": 9.998,
      "layer": 5,
      "closed_by": [
        22
      ]
    },
    {
      "id": 21,
      "x": -0.465,
      "y": -0.372,
      "angle": 340.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 22,
      "x": 0.467,
      "y": -0.37,
      "angle": 20.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 23,
      "x": -0.256,
      "y": -0.574,
      "layer": 2,
      "closed_by": [
        3,
        16
      ]
    },
    {
      "id": 24,
      "x": 0.263,
      "y": -0.577,
      "layer": 2,
      "closed_by": [
        3,
        15
      ]
    },
    {
      "id": 25,
      "x": -0.259,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        18,
        33
      ]
    },
    {
      "id": 26,
      "x": -0.375,
      "y": 0.493,
      "angle": 335.0,
      "layer": 1,
      "closed_by": [
        19,
        29
      ]
    },
    {
      "id": 27,
      "x": 0.393,
      "y": 0.495,
      "angle": 25.0,
      "layer": 1,
      "closed_by": [
        19,
        28
      ]
    },
    {
      "id": 28,
      "x": 0.545,
      "y": 0.544,
      "layer": 2,
      "closed_by": [
        30,
        31
      ]
    },
    {
      "id": 29,
      "x": -0.544,
      "y": 0.541,
      "layer": 2,
      "closed_by": [
        25,
        32
      ]
    },
    {
      "id": 30,
      "x": 0.266,
      "y": 0.615,
      "layer": 3,
      "closed_by": [
        33,
        34
      ]
    },
    {
      "id": 31,
      "x": 0.865,
      "y": 0.619,
      "layer": 3,
      "closed_by": [
        34,
        35
      ]
    },
    {
      "id": 32,
      "x": -0.865,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        18,
        36
      ]
    },
    {
      "id": 33,
      "x": 0.002,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        37,
        40
      ]
    },
    {
      "id": 34,
      "x": 0.527,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        40,
        41
      ]
    },
    {
      "id": 35,
      "x": 1.064,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        41,
        43
      ]
    },
    {
      "id": 36,
      "x": -1.052,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        39,
        42
      ]
    },
    {
      "id": 37,
      "x": -0.266,
      "y": 0.777,
      "layer": 5,
      "closed_by": [
        44,
        46
      ]
    },
    {
      "id": 38,
      "x": 1.059,
      "y": 0.859,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -1.297,
      "y": 0.777,
      "layer": 5,
      "closed_by": [
        47,
        50
      ]
    },
    {
      "id": 40,
      "x": 0.268,
      "y": 0.777,
      "layer": 5,
      "closed_by": [
        44,
        45
      ]
    },
    {
      "id": 41,
      "x": 0.787,
      "y": 0.782,
      "layer": 5,
      "closed_by": [
        38,
        45
      ]
    },
    {
      "id": 42,
      "x": -0.781,
      "y": 0.781,
      "layer": 5,
      "closed_by": [
        46,
        47
      ]
    },
    {
      "id": 43,
      "x": 1.315,
      "y": 0.777,
      "layer": 5,
      "closed_by": [
        38,
        49
      ]
    },
    {
      "id": 44,
      "y": 0.859,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 45,
      "x": 0.527,
      "y": 0.859,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 46,
      "x": -0.531,
      "y": 0.86,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 47,
      "x": -1.062,
      "y": 0.861,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 48,
      "x": 1.526,
      "y": -0.108,
      "angle": 55.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 49,
      "x": 1.585,
      "y": 0.859,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 50,
      "x": -1.585,
      "y": 0.859,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -1.529,
      "y": -0.133,
      "angle": 310.0,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 48,
  "cards_in_deck_amount": 34,
  "stars_statistic": {
    "star1": 0.5,
    "star2": 18.9,
    "star3": 79.9
  }
}