{
  "layout_cards": [
    {
      "id": 51,
      "x": -2.328,
      "y": 0.246,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": -2.749,
      "y": 0.246,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 49,
      "x": 2.305,
      "y": 0.238,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 48,
      "x": 1.924,
      "y": 0.243,
      "layer": 2,
      "closed_by": [
        49,
        41,
        31
      ]
    },
    {
      "id": 47,
      "x": 1.524,
      "y": 0.243,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 46,
      "x": -1.526,
      "y": 0.246,
      "layer": 1,
      "closed_by": [
        26
      ]
    },
    {
      "id": 45,
      "x": 0.001,
      "y": 0.648,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 44,
      "x": -0.861,
      "y": 0.698,
      "angle": 9.998,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 43,
      "x": 0.851,
      "y": -0.216,
      "angle": 9.998,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 42,
      "x": 0.851,
      "y": 0.698,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 41,
      "x": 1.498,
      "y": -0.298,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 40,
      "x": 0.731,
      "y": 0.238,
      "layer": 2,
      "closed_by": [
        43,
        42
      ]
    },
    {
      "id": 39,
      "x": -0.273,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 38,
      "x": -0.87,
      "y": -0.216,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 37,
      "x": 0.277,
      "y": 1.067,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 36,
      "x": -1.167,
      "y": -0.379,
      "angle": 349.997,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 1.151,
      "y": -0.379,
      "angle": 9.998,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 0.273,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 33,
      "x": -0.275,
      "y": 1.069,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 32,
      "x": -1.506,
      "y": -0.296,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 31,
      "x": 1.496,
      "y": 0.832,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 30,
      "x": -1.159,
      "y": 0.878,
      "angle": 9.998,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 29,
      "x": 1.161,
      "y": 0.875,
      "angle": 349.997,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 2.657,
      "y": 0.238,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 27,
      "x": -0.731,
      "y": 0.238,
      "layer": 2,
      "closed_by": [
        44,
        38
      ]
    },
    {
      "id": 26,
      "x": -1.921,
      "y": 0.246,
      "layer": 2,
      "closed_by": [
        51,
        32,
        23
      ]
    },
    {
      "id": 25,
      "x": 0.001,
      "y": 0.238,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 24,
      "y": -0.171,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 23,
      "x": -1.508,
      "y": 0.827,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        30
      ]
    }
  ],
  "cards_in_layout_amount": 29,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 35.1,
    "star2": 57.1,
    "star3": 7.0
  }
}