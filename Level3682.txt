{
  "layout_cards": [
    {
      "id": 15,
      "x": -2.065,
      "y": 0.782,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 18,
      "x": 1.988,
      "y": -0.178,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 20,
      "x": 0.386,
      "y": 0.064,
      "layer": 4,
      "closed_by": [
        26,
        28
      ]
    },
    {
      "id": 21,
      "x": -0.004,
      "y": 0.46,
      "layer": 1,
      "closed_by": [
        24
      ]
    },
    {
      "id": 22,
      "x": 0.386,
      "y": 0.458,
      "layer": 3,
      "closed_by": [
        20
      ]
    },
    {
      "id": 23,
      "x": -0.384,
      "y": 0.46,
      "layer": 4,
      "closed_by": [
        27,
        29
      ]
    },
    {
      "id": 24,
      "x": -0.006,
      "y": 0.063,
      "layer": 2,
      "closed_by": [
        22,
        25
      ]
    },
    {
      "id": 25,
      "x": -0.386,
      "y": 0.064,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 26,
      "x": 0.629,
      "y": -0.172,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 27,
      "x": -0.625,
      "y": 0.703,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 28,
      "x": 0.628,
      "y": 0.702,
      "layer": 5,
      "closed_by": [
        30,
        32
      ]
    },
    {
      "id": 29,
      "x": -0.628,
      "y": -0.172,
      "layer": 5,
      "closed_by": [
        31,
        35
      ]
    },
    {
      "id": 30,
      "x": 0.865,
      "y": -0.017,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -0.865,
      "y": 0.541,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 0.865,
      "y": 0.861,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -1.825,
      "y": 0.377,
      "layer": 5,
      "closed_by": [
        15,
        51
      ]
    },
    {
      "id": 34,
      "x": -1.424,
      "y": 0.54,
      "layer": 4,
      "closed_by": [
        33,
        38
      ]
    },
    {
      "id": 35,
      "x": -0.865,
      "y": -0.337,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 1.187,
      "y": 0.143,
      "layer": 5,
      "closed_by": [
        30,
        32
      ]
    },
    {
      "id": 37,
      "x": -1.184,
      "y": -0.499,
      "layer": 5,
      "closed_by": [
        35
      ]
    },
    {
      "id": 38,
      "x": -1.187,
      "y": 0.379,
      "layer": 5,
      "closed_by": [
        31,
        35
      ]
    },
    {
      "id": 39,
      "x": 1.187,
      "y": 1.021,
      "layer": 5,
      "closed_by": [
        32
      ]
    },
    {
      "id": 40,
      "x": 1.429,
      "y": 0.861,
      "layer": 4,
      "closed_by": [
        36,
        39,
        47,
        48
      ]
    },
    {
      "id": 41,
      "x": 1.424,
      "y": -0.017,
      "layer": 4,
      "closed_by": [
        36,
        47
      ]
    },
    {
      "id": 42,
      "x": -1.424,
      "y": -0.335,
      "layer": 4,
      "closed_by": [
        33,
        37,
        38,
        49
      ]
    },
    {
      "id": 43,
      "x": 1.667,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        40,
        41
      ]
    },
    {
      "id": 44,
      "x": -1.743,
      "y": -0.096,
      "layer": 3,
      "closed_by": [
        34,
        42
      ]
    },
    {
      "id": 45,
      "x": 1.667,
      "y": -0.177,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 46,
      "x": -1.746,
      "y": 0.782,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 47,
      "x": 1.746,
      "y": 0.142,
      "layer": 5,
      "closed_by": [
        18,
        50
      ]
    },
    {
      "id": 48,
      "x": 1.746,
      "y": 1.019,
      "layer": 5,
      "closed_by": [
        50
      ]
    },
    {
      "id": 49,
      "x": -1.825,
      "y": -0.495,
      "layer": 5,
      "closed_by": [
        51
      ]
    },
    {
      "id": 50,
      "x": 1.983,
      "y": 0.781,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -2.065,
      "y": -0.254,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 34,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 30.4,
    "star2": 63.2,
    "star3": 5.7
  }
}