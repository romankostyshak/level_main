{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.49,
      "y": 0.86,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 50,
      "x": 0.333,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        49,
        24
      ]
    },
    {
      "id": 49,
      "x": 0.707,
      "y": 0.216,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 48,
      "x": -0.703,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        26
      ]
    },
    {
      "id": 47,
      "x": -0.703,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        27
      ]
    },
    {
      "id": 46,
      "x": -0.703,
      "y": 0.216,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 45,
      "x": -0.703,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        21
      ]
    },
    {
      "id": 44,
      "x": -0.49,
      "y": 0.861,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 43,
      "x": 0.707,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        50
      ]
    },
    {
      "id": 42,
      "x": 0.707,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 41,
      "x": 0.707,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        17
      ]
    },
    {
      "id": 40,
      "x": -0.493,
      "y": -0.416,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 39,
      "x": 1.225,
      "y": 0.939,
      "layer": 5,
      "closed_by": [
        38
      ]
    },
    {
      "id": 38,
      "x": 1.225,
      "y": 0.216,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 37,
      "x": 1.225,
      "y": -0.5,
      "layer": 5,
      "closed_by": [
        38
      ]
    },
    {
      "id": 36,
      "x": -1.225,
      "y": -0.5,
      "layer": 5,
      "closed_by": [
        35
      ]
    },
    {
      "id": 35,
      "x": -1.225,
      "y": 0.216,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -1.225,
      "y": 0.939,
      "layer": 5,
      "closed_by": [
        35
      ]
    },
    {
      "id": 33,
      "x": 1.667,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 32,
      "x": 1.667,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        33,
        31
      ]
    },
    {
      "id": 31,
      "x": 1.667,
      "y": -0.5,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 30,
      "x": -1.667,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 29,
      "x": -1.667,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        30,
        28
      ]
    },
    {
      "id": 28,
      "x": -1.667,
      "y": -0.5,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 27,
      "x": -0.335,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 26,
      "x": -0.335,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        46,
        24
      ]
    },
    {
      "id": 25,
      "x": 0.333,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 24,
      "x": -0.001,
      "y": 1.018,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 0.492,
      "y": -0.416,
      "layer": 1,
      "closed_by": [
        41
      ]
    },
    {
      "id": 22,
      "x": -0.703,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        19
      ]
    },
    {
      "id": 21,
      "x": -0.333,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 20,
      "x": 0.708,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        18
      ]
    },
    {
      "id": 19,
      "x": -0.333,
      "y": -0.259,
      "layer": 5,
      "closed_by": [
        46,
        16
      ]
    },
    {
      "id": 18,
      "x": 0.335,
      "y": -0.259,
      "layer": 5,
      "closed_by": [
        49,
        16
      ]
    },
    {
      "id": 17,
      "x": 0.335,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        20
      ]
    },
    {
      "id": 16,
      "y": -0.578,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 36,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 11.1,
    "star2": 69.3,
    "star3": 19.1
  }
}