{
  "layout_cards": [
    {
      "id": 49,
      "x": 0.333,
      "y": 0.305,
      "layer": 1,
      "closed_by": [
        51
      ]
    },
    {
      "id": 44,
      "x": 0.418,
      "y": -0.179,
      "layer": 5,
      "closed_by": [
        32,
        31
      ]
    },
    {
      "id": 43,
      "x": -0.949,
      "y": 0.3,
      "layer": 1,
      "closed_by": [
        41,
        22
      ]
    },
    {
      "id": 39,
      "x": -0.418,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        36,
        30
      ]
    },
    {
      "id": 38,
      "x": -0.859,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 37,
      "x": -0.418,
      "y": -0.179,
      "layer": 5,
      "closed_by": [
        33,
        31
      ]
    },
    {
      "id": 36,
      "x": -0.859,
      "y": 0.777,
      "layer": 5,
      "closed_by": [
        35
      ]
    },
    {
      "id": 35,
      "x": -0.569,
      "y": 0.86,
      "layer": 6,
      "closed_by": [
        26
      ]
    },
    {
      "id": 34,
      "x": 0.573,
      "y": 0.859,
      "layer": 6,
      "closed_by": [
        25
      ]
    },
    {
      "id": 33,
      "x": -0.708,
      "y": -0.254,
      "layer": 6,
      "closed_by": [
        28
      ]
    },
    {
      "id": 32,
      "x": 0.708,
      "y": -0.254,
      "layer": 6,
      "closed_by": [
        29
      ]
    },
    {
      "id": 31,
      "x": -0.002,
      "y": -0.256,
      "layer": 6,
      "closed_by": [
        29,
        28
      ]
    },
    {
      "id": 30,
      "x": -0.001,
      "y": 0.861,
      "layer": 6,
      "closed_by": [
        26,
        25
      ]
    },
    {
      "id": 29,
      "x": 0.333,
      "y": -0.497,
      "layer": 7,
      "closed_by": [
        27
      ]
    },
    {
      "id": 28,
      "x": -0.335,
      "y": -0.497,
      "layer": 7,
      "closed_by": [
        27
      ]
    },
    {
      "id": 27,
      "x": -0.004,
      "y": -0.574,
      "layer": 8,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -0.333,
      "y": 0.939,
      "layer": 7,
      "closed_by": [
        24
      ]
    },
    {
      "id": 25,
      "x": 0.331,
      "y": 0.939,
      "layer": 7,
      "closed_by": [
        24
      ]
    },
    {
      "id": 24,
      "y": 1.021,
      "layer": 8,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 1.383,
      "y": 0.307,
      "layer": 2,
      "closed_by": [
        21,
        20
      ]
    },
    {
      "id": 18,
      "x": -1.378,
      "y": -0.1,
      "layer": 3,
      "closed_by": [
        14
      ]
    },
    {
      "id": 50,
      "x": 0.638,
      "y": 0.337,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        47,
        46
      ]
    },
    {
      "id": 47,
      "x": 0.418,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        30,
        45
      ]
    },
    {
      "id": 41,
      "x": -0.68,
      "y": 0.36,
      "angle": 29.999,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 40,
      "x": -0.697,
      "y": 0.354,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        39,
        38
      ]
    },
    {
      "id": 42,
      "x": -0.333,
      "y": 0.3,
      "layer": 1,
      "closed_by": [
        37,
        35,
        41
      ]
    },
    {
      "id": 19,
      "x": -1.378,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        15
      ]
    },
    {
      "id": 21,
      "x": 1.378,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        17
      ]
    },
    {
      "id": 45,
      "x": 0.86,
      "y": 0.777,
      "layer": 5,
      "closed_by": [
        34
      ]
    },
    {
      "id": 17,
      "x": 1.664,
      "y": 0.777,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 16,
      "x": 1.664,
      "y": -0.179,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 20,
      "x": 1.378,
      "y": -0.179,
      "layer": 3,
      "closed_by": [
        16
      ]
    },
    {
      "id": 46,
      "x": 0.86,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        44
      ]
    },
    {
      "id": 48,
      "x": 0.948,
      "y": 0.307,
      "layer": 1,
      "closed_by": [
        23,
        51
      ]
    },
    {
      "id": 51,
      "x": 0.634,
      "y": 0.331,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 22,
      "x": -1.389,
      "y": 0.3,
      "layer": 2,
      "closed_by": [
        18,
        19
      ]
    },
    {
      "id": 14,
      "x": -1.664,
      "y": -0.1,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 15,
      "x": -1.664,
      "y": 0.777,
      "layer": 4,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 38,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 13.2,
    "star2": 67.9,
    "star3": 18.6
  }
}