{
  "layout_cards": [
    {
      "id": 12,
      "x": -2.305,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        14
      ]
    },
    {
      "id": 13,
      "x": 2.309,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        15
      ]
    },
    {
      "id": 14,
      "x": -2.078,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        26,
        27
      ]
    },
    {
      "id": 15,
      "x": 2.069,
      "y": -0.093,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 18,
      "x": 0.975,
      "y": -0.574,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 19,
      "x": -0.972,
      "y": -0.573,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 20,
      "x": 1.264,
      "y": 0.939,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 21,
      "x": 2.444,
      "y": 0.893,
      "angle": 25.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -2.421,
      "y": 0.896,
      "angle": 335.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 23,
      "x": -1.85,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        22,
        31
      ]
    },
    {
      "id": 24,
      "x": 1.85,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        21,
        32
      ]
    },
    {
      "id": 25,
      "x": 2.302,
      "y": 0.754,
      "angle": 335.0,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 26,
      "x": -2.299,
      "y": 0.675,
      "angle": 25.0,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 27,
      "x": -1.952,
      "y": -0.064,
      "angle": 25.0,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 28,
      "x": -0.002,
      "y": -0.259,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 29,
      "x": 0.078,
      "y": -0.298,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 30,
      "x": -0.063,
      "y": -0.298,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 31,
      "x": -1.521,
      "y": 0.412,
      "angle": 25.0,
      "layer": 5,
      "closed_by": [
        51
      ]
    },
    {
      "id": 32,
      "x": 1.524,
      "y": 0.414,
      "angle": 335.0,
      "layer": 5,
      "closed_by": [
        20
      ]
    },
    {
      "id": 33,
      "x": 0.119,
      "y": 0.976,
      "angle": 20.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -0.108,
      "y": 0.972,
      "angle": 340.0,
      "layer": 5,
      "closed_by": [
        33
      ]
    },
    {
      "id": 35,
      "x": 0.79,
      "y": 0.708,
      "angle": 335.0,
      "layer": 5,
      "closed_by": [
        20
      ]
    },
    {
      "id": 36,
      "x": 1.955,
      "y": 0.012,
      "angle": 335.0,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 38,
      "x": -0.773,
      "y": 0.708,
      "angle": 25.0,
      "layer": 5,
      "closed_by": [
        51
      ]
    },
    {
      "id": 39,
      "x": 0.261,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        34,
        41
      ]
    },
    {
      "id": 40,
      "x": -0.256,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 41,
      "x": 0.521,
      "y": 0.578,
      "angle": 20.0,
      "layer": 4,
      "closed_by": [
        33,
        35
      ]
    },
    {
      "id": 42,
      "x": -0.509,
      "y": 0.574,
      "angle": 340.0,
      "layer": 4,
      "closed_by": [
        34,
        38
      ]
    },
    {
      "id": 43,
      "x": 1.322,
      "y": 0.017,
      "angle": 20.0,
      "layer": 4,
      "closed_by": [
        32
      ]
    },
    {
      "id": 44,
      "x": -1.324,
      "y": -0.017,
      "angle": 340.0,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 45,
      "x": -0.744,
      "y": 0.014,
      "angle": 25.0,
      "layer": 3,
      "closed_by": [
        42,
        44
      ]
    },
    {
      "id": 46,
      "x": 0.333,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        30,
        50
      ]
    },
    {
      "id": 47,
      "x": -0.331,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        30,
        45
      ]
    },
    {
      "id": 48,
      "x": 0.266,
      "y": -0.5,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 49,
      "x": -0.256,
      "y": -0.497,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 50,
      "x": 0.745,
      "y": 0.013,
      "angle": 335.0,
      "layer": 3,
      "closed_by": [
        41,
        43
      ]
    },
    {
      "id": 51,
      "x": -1.268,
      "y": 0.939,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 37,
  "cards_in_deck_amount": 32,
  "stars_statistic": {
    "star1": 31.1,
    "star2": 64.0,
    "star3": 4.6
  }
}