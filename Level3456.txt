{
  "layout_cards": [
    {
      "id": 10,
      "y": -0.177,
      "layer": 4,
      "closed_by": [
        33,
        50
      ]
    },
    {
      "id": 11,
      "y": 1.019,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 12,
      "x": 1.61,
      "y": 0.493,
      "angle": 350.0,
      "layer": 5,
      "closed_by": [
        34
      ]
    },
    {
      "id": 13,
      "x": -1.526,
      "y": -0.223,
      "angle": 350.0,
      "layer": 2,
      "closed_by": [
        15
      ]
    },
    {
      "id": 14,
      "x": 1.513,
      "y": -0.243,
      "angle": 10.0,
      "layer": 2,
      "closed_by": [
        18
      ]
    },
    {
      "id": 15,
      "x": -1.429,
      "y": 0.317,
      "angle": 350.0,
      "layer": 3,
      "closed_by": [
        46,
        49
      ]
    },
    {
      "id": 16,
      "x": -1.61,
      "y": 0.5,
      "angle": 10.0,
      "layer": 5,
      "closed_by": [
        43
      ]
    },
    {
      "id": 18,
      "x": 1.422,
      "y": 0.312,
      "angle": 10.0,
      "layer": 3,
      "closed_by": [
        47,
        48
      ]
    },
    {
      "id": 20,
      "x": 0.002,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        25,
        30
      ]
    },
    {
      "id": 21,
      "x": -0.317,
      "y": 0.995,
      "angle": 9.998,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 22,
      "x": -0.312,
      "y": 0.412,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        20,
        23,
        31
      ]
    },
    {
      "id": 23,
      "x": -0.657,
      "y": 1.0,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 24,
      "x": -0.661,
      "y": 0.409,
      "angle": 9.998,
      "layer": 4,
      "closed_by": [
        25,
        40
      ]
    },
    {
      "id": 25,
      "x": -0.388,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        11
      ]
    },
    {
      "id": 26,
      "x": 0.319,
      "y": 0.99,
      "angle": 349.997,
      "layer": 1,
      "closed_by": [
        27
      ]
    },
    {
      "id": 27,
      "x": 0.316,
      "y": 0.405,
      "angle": 9.998,
      "layer": 2,
      "closed_by": [
        20,
        28,
        32
      ]
    },
    {
      "id": 28,
      "x": 0.657,
      "y": 0.994,
      "angle": 9.998,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 29,
      "x": 0.665,
      "y": 0.412,
      "angle": 349.997,
      "layer": 4,
      "closed_by": [
        30,
        39
      ]
    },
    {
      "id": 30,
      "x": 0.4,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        11
      ]
    },
    {
      "id": 31,
      "x": -0.412,
      "y": -0.158,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        10,
        24,
        37
      ]
    },
    {
      "id": 32,
      "x": 0.412,
      "y": -0.143,
      "angle": 9.998,
      "layer": 3,
      "closed_by": [
        10,
        29,
        38
      ]
    },
    {
      "id": 33,
      "x": -0.317,
      "y": -0.523,
      "angle": 9.998,
      "layer": 5,
      "closed_by": [
        51
      ]
    },
    {
      "id": 34,
      "x": 1.304,
      "y": 0.407,
      "angle": 349.997,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -0.449,
      "y": -0.546,
      "angle": 9.998,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 36,
      "x": 0.451,
      "y": -0.55,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 37,
      "x": -0.697,
      "y": -0.55,
      "angle": 349.997,
      "layer": 4,
      "closed_by": [
        33,
        42
      ]
    },
    {
      "id": 38,
      "x": 0.722,
      "y": -0.55,
      "angle": 9.998,
      "layer": 4,
      "closed_by": [
        41,
        50
      ]
    },
    {
      "id": 39,
      "x": 1.047,
      "y": 0.407,
      "angle": 10.0,
      "layer": 5,
      "closed_by": [
        34
      ]
    },
    {
      "id": 40,
      "x": -1.049,
      "y": 0.409,
      "angle": 349.997,
      "layer": 5,
      "closed_by": [
        43
      ]
    },
    {
      "id": 41,
      "x": 1.052,
      "y": -0.55,
      "angle": 349.997,
      "layer": 5,
      "closed_by": [
        44
      ]
    },
    {
      "id": 42,
      "x": -1.049,
      "y": -0.55,
      "angle": 9.998,
      "layer": 5,
      "closed_by": [
        45
      ]
    },
    {
      "id": 43,
      "x": -1.309,
      "y": 0.412,
      "angle": 9.998,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 44,
      "x": 1.317,
      "y": -0.55,
      "angle": 9.998,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 45,
      "x": -1.317,
      "y": -0.55,
      "angle": 349.997,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 46,
      "x": -1.307,
      "y": 1.008,
      "angle": 349.997,
      "layer": 4,
      "closed_by": [
        16,
        40
      ]
    },
    {
      "id": 47,
      "x": 1.297,
      "y": 1.001,
      "angle": 9.998,
      "layer": 4,
      "closed_by": [
        12,
        39
      ]
    },
    {
      "id": 48,
      "x": 1.172,
      "y": -0.289,
      "angle": 10.0,
      "layer": 4,
      "closed_by": [
        12,
        39,
        41
      ]
    },
    {
      "id": 49,
      "x": -1.202,
      "y": -0.287,
      "angle": 349.997,
      "layer": 4,
      "closed_by": [
        16,
        40,
        42
      ]
    },
    {
      "id": 50,
      "x": 0.314,
      "y": -0.527,
      "angle": 349.997,
      "layer": 5,
      "closed_by": [
        51
      ]
    },
    {
      "id": 51,
      "y": -0.497,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 40,
  "cards_in_deck_amount": 32,
  "stars_statistic": {
    "star1": 31.9,
    "star2": 64.9,
    "star3": 2.9
  }
}