{
  "layout_cards": [
    {
      "id": 16,
      "x": -1.985,
      "y": -0.344,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        20
      ]
    },
    {
      "id": 17,
      "x": -1.927,
      "y": 0.758,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        20
      ]
    },
    {
      "id": 18,
      "x": 1.898,
      "y": -0.402,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 19,
      "x": 1.96,
      "y": 0.722,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 20,
      "x": -1.822,
      "y": 0.158,
      "angle": 349.997,
      "layer": 4,
      "closed_by": [
        22,
        38
      ]
    },
    {
      "id": 21,
      "x": 1.758,
      "y": 0.192,
      "angle": 349.997,
      "layer": 4,
      "closed_by": [
        23,
        24
      ]
    },
    {
      "id": 22,
      "x": -1.827,
      "y": 0.828,
      "angle": 349.997,
      "layer": 5,
      "closed_by": [
        25
      ]
    },
    {
      "id": 23,
      "x": 1.804,
      "y": -0.467,
      "angle": 349.997,
      "layer": 5,
      "closed_by": [
        26
      ]
    },
    {
      "id": 24,
      "x": 1.804,
      "y": 0.833,
      "angle": 349.997,
      "layer": 5,
      "closed_by": [
        26
      ]
    },
    {
      "id": 25,
      "x": -1.572,
      "y": 0.171,
      "angle": 349.997,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 26,
      "x": 1.519,
      "y": 0.165,
      "angle": 349.997,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 27,
      "x": -0.799,
      "y": 0.172,
      "angle": 349.997,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 28,
      "x": -0.003,
      "y": 0.165,
      "angle": 349.997,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -1.184,
      "y": 0.143,
      "layer": 2,
      "closed_by": [
        43,
        45
      ]
    },
    {
      "id": 30,
      "x": 1.184,
      "y": 0.143,
      "layer": 2,
      "closed_by": [
        44,
        51
      ]
    },
    {
      "id": 31,
      "x": -1.187,
      "y": -0.497,
      "layer": 5,
      "closed_by": [
        25,
        27
      ]
    },
    {
      "id": 32,
      "x": -1.184,
      "y": 0.86,
      "layer": 5,
      "closed_by": [
        25,
        27
      ]
    },
    {
      "id": 33,
      "x": 1.184,
      "y": -0.497,
      "layer": 5,
      "closed_by": [
        26,
        37
      ]
    },
    {
      "id": 34,
      "x": 1.187,
      "y": 0.861,
      "layer": 5,
      "closed_by": [
        26,
        37
      ]
    },
    {
      "id": 35,
      "x": 0.411,
      "y": -0.495,
      "layer": 5,
      "closed_by": [
        28,
        37
      ]
    },
    {
      "id": 36,
      "x": -0.416,
      "y": -0.495,
      "layer": 5,
      "closed_by": [
        27,
        28
      ]
    },
    {
      "id": 37,
      "x": 0.809,
      "y": 0.165,
      "angle": 349.997,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -1.822,
      "y": -0.472,
      "angle": 349.997,
      "layer": 5,
      "closed_by": [
        25
      ]
    },
    {
      "id": 39,
      "x": 0.412,
      "y": 0.861,
      "layer": 5,
      "closed_by": [
        28,
        37
      ]
    },
    {
      "id": 40,
      "x": -0.412,
      "y": 0.861,
      "layer": 5,
      "closed_by": [
        27,
        28
      ]
    },
    {
      "id": 41,
      "x": -0.81,
      "y": 0.141,
      "layer": 4,
      "closed_by": [
        31,
        32,
        36,
        40
      ]
    },
    {
      "id": 42,
      "x": 0.814,
      "y": 0.143,
      "layer": 4,
      "closed_by": [
        33,
        34,
        35,
        39
      ]
    },
    {
      "id": 43,
      "x": -0.81,
      "y": -0.493,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 44,
      "x": 0.814,
      "y": -0.493,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 45,
      "x": -0.81,
      "y": 0.861,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 46,
      "x": 0.002,
      "y": 0.143,
      "layer": 4,
      "closed_by": [
        35,
        36,
        39,
        40
      ]
    },
    {
      "id": 47,
      "x": 0.001,
      "y": 0.861,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": -0.414,
      "y": 0.143,
      "layer": 2,
      "closed_by": [
        43,
        45,
        47
      ]
    },
    {
      "id": 49,
      "x": 0.412,
      "y": 0.143,
      "layer": 2,
      "closed_by": [
        44,
        47,
        51
      ]
    },
    {
      "id": 50,
      "x": -0.002,
      "y": -0.493,
      "layer": 1,
      "closed_by": [
        46,
        48,
        49
      ]
    },
    {
      "id": 51,
      "x": 0.813,
      "y": 0.861,
      "layer": 3,
      "closed_by": [
        42
      ]
    }
  ],
  "cards_in_layout_amount": 36,
  "cards_in_deck_amount": 32,
  "stars_statistic": {
    "star1": 53.5,
    "star2": 40.1,
    "star3": 6.3
  }
}