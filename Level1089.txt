{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.734,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 50,
      "x": -0.734,
      "y": -0.577,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 49,
      "x": -0.587,
      "y": -0.527,
      "angle": 340.0,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": 0.587,
      "y": -0.527,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "x": 0.442,
      "y": -0.532,
      "angle": 40.0,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 46,
      "x": -0.437,
      "y": -0.527,
      "angle": 320.0,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 45,
      "x": -0.347,
      "y": -0.112,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 44,
      "x": 0.347,
      "y": -0.115,
      "angle": 344.997,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 43,
      "x": 0.001,
      "y": -0.017,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 42,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        41,
        40
      ]
    },
    {
      "id": 41,
      "x": 0.412,
      "y": 1.006,
      "angle": 354.997,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 40,
      "x": -0.395,
      "y": 1.008,
      "angle": 5.0,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 39,
      "x": 0.699,
      "y": 0.97,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        37,
        32
      ]
    },
    {
      "id": 38,
      "x": -0.699,
      "y": 0.967,
      "angle": 9.998,
      "layer": 3,
      "closed_by": [
        36,
        33
      ]
    },
    {
      "id": 37,
      "x": 0.986,
      "y": 0.906,
      "angle": 344.997,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 36,
      "x": -0.981,
      "y": 0.907,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 35,
      "x": 1.264,
      "y": 0.819,
      "angle": 340.0,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 34,
      "x": -1.258,
      "y": 0.819,
      "angle": 20.0,
      "layer": 5,
      "closed_by": [
        29
      ]
    },
    {
      "id": 33,
      "x": -0.351,
      "y": 0.787,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 32,
      "x": 0.356,
      "y": 0.776,
      "angle": 344.997,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 31,
      "y": 0.976,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 1.546,
      "y": 0.699,
      "angle": 335.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -1.554,
      "y": 0.698,
      "angle": 25.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 28,
      "x": -1.809,
      "y": 0.546,
      "angle": 30.0,
      "layer": 5,
      "closed_by": [
        29
      ]
    },
    {
      "id": 27,
      "x": 1.794,
      "y": 0.555,
      "angle": 330.0,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 26,
      "x": 2.023,
      "y": 0.398,
      "angle": 325.0,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 25,
      "x": -2.039,
      "y": 0.388,
      "angle": 35.0,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 24,
      "x": 2.232,
      "y": 0.216,
      "angle": 320.0,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 23,
      "x": -2.256,
      "y": 0.206,
      "angle": 40.0,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 22,
      "x": 2.427,
      "y": 0.017,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 21,
      "x": -2.457,
      "y": 0.003,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 20,
      "x": 2.592,
      "y": -0.186,
      "angle": 310.0,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 19,
      "x": -2.628,
      "y": -0.202,
      "angle": 50.0,
      "layer": 1,
      "closed_by": [
        21
      ]
    }
  ],
  "cards_in_layout_amount": 33,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 30.8,
    "star2": 62.0,
    "star3": 6.7
  }
}