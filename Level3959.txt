{
  "layout_cards": [
    {
      "id": 3,
      "x": 2.5,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        36,
        40
      ]
    },
    {
      "id": 4,
      "x": -2.14,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        10
      ]
    },
    {
      "id": 5,
      "x": -0.337,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        12,
        13
      ]
    },
    {
      "id": 6,
      "x": 0.259,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        13,
        14
      ]
    },
    {
      "id": 7,
      "x": -0.939,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        11,
        12
      ]
    },
    {
      "id": 8,
      "x": -1.539,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        10,
        11
      ]
    },
    {
      "id": 9,
      "x": 0.86,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        14,
        28,
        29
      ]
    },
    {
      "id": 10,
      "x": -1.838,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        15,
        16,
        19,
        20
      ]
    },
    {
      "id": 11,
      "x": -1.24,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        19,
        20,
        21,
        22
      ]
    },
    {
      "id": 12,
      "x": -0.638,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        21,
        22,
        23,
        24
      ]
    },
    {
      "id": 13,
      "x": -0.039,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        23,
        24,
        25,
        26
      ]
    },
    {
      "id": 14,
      "x": 0.559,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        18,
        25,
        26,
        27
      ]
    },
    {
      "id": 15,
      "x": -2.098,
      "y": -0.34,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 16,
      "x": -2.098,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 17,
      "x": -1.498,
      "y": 0.217,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 18,
      "x": 0.898,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 19,
      "x": -1.498,
      "y": -0.34,
      "layer": 3,
      "closed_by": [
        41,
        42
      ]
    },
    {
      "id": 20,
      "x": -1.498,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        45,
        49
      ]
    },
    {
      "id": 21,
      "x": -0.898,
      "y": -0.34,
      "layer": 3,
      "closed_by": [
        42,
        43
      ]
    },
    {
      "id": 22,
      "x": -0.898,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        46,
        49
      ]
    },
    {
      "id": 23,
      "x": -0.298,
      "y": -0.34,
      "layer": 3,
      "closed_by": [
        37,
        43
      ]
    },
    {
      "id": 24,
      "x": -0.298,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        46,
        48
      ]
    },
    {
      "id": 25,
      "x": 0.298,
      "y": -0.34,
      "layer": 3,
      "closed_by": [
        37,
        44
      ]
    },
    {
      "id": 26,
      "x": 0.298,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        47,
        48
      ]
    },
    {
      "id": 27,
      "x": 0.898,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 28,
      "x": 1.22,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        27,
        35,
        50
      ]
    },
    {
      "id": 29,
      "x": 1.22,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        18,
        39,
        50
      ]
    },
    {
      "id": 30,
      "x": 1.618,
      "y": 0.217,
      "layer": 1,
      "closed_by": [
        28,
        29,
        31,
        32
      ]
    },
    {
      "id": 31,
      "x": 1.86,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        35,
        36
      ]
    },
    {
      "id": 32,
      "x": 1.86,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        38,
        39
      ]
    },
    {
      "id": 33,
      "x": 2.22,
      "y": 0.217,
      "layer": 1,
      "closed_by": [
        3,
        31,
        32,
        34
      ]
    },
    {
      "id": 34,
      "x": 2.5,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        38,
        40
      ]
    },
    {
      "id": 35,
      "x": 1.46,
      "y": -0.578,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 2.098,
      "y": -0.578,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 37,
      "x": -0.039,
      "y": -0.5,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 38,
      "x": 2.098,
      "y": 1.019,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 39,
      "x": 1.46,
      "y": 1.019,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 2.72,
      "y": 0.217,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 41,
      "x": -1.838,
      "y": -0.5,
      "layer": 4,
      "closed_by": [
        17
      ]
    },
    {
      "id": 42,
      "x": -1.24,
      "y": -0.5,
      "layer": 4,
      "closed_by": [
        17
      ]
    },
    {
      "id": 43,
      "x": -0.638,
      "y": -0.5,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 44,
      "x": 0.559,
      "y": -0.5,
      "layer": 4,
      "closed_by": [
        50
      ]
    },
    {
      "id": 45,
      "x": -1.838,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        17
      ]
    },
    {
      "id": 46,
      "x": -0.638,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 47,
      "x": 0.559,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        50
      ]
    },
    {
      "id": 48,
      "x": -0.039,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 49,
      "x": -1.24,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        17
      ]
    },
    {
      "id": 50,
      "x": 0.898,
      "y": 0.217,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -0.298,
      "y": 0.217,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 49,
  "cards_in_deck_amount": 34,
  "layers_in_level": 5,
  "open_cards": 8,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 8.2,
    "star2": 76.9,
    "star3": 14.5
  }
}