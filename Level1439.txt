{
  "layout_cards": [
    {
      "id": 49,
      "x": 0.001,
      "y": 1.026,
      "layer": 1,
      "closed_by": [
        48,
        47
      ]
    },
    {
      "id": 48,
      "x": -0.259,
      "y": 0.861,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 47,
      "x": 0.263,
      "y": 0.861,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 46,
      "x": 0.001,
      "y": 0.698,
      "layer": 3,
      "closed_by": [
        50,
        45
      ]
    },
    {
      "id": 50,
      "x": -0.303,
      "y": 0.698,
      "layer": 4,
      "closed_by": [
        44,
        42
      ]
    },
    {
      "id": 41,
      "x": -0.001,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        40,
        39
      ]
    },
    {
      "id": 40,
      "x": 0.307,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 38,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        34,
        35
      ]
    },
    {
      "id": 34,
      "x": -0.379,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        32
      ]
    },
    {
      "id": 33,
      "x": 0.777,
      "y": -0.337,
      "layer": 5,
      "closed_by": [
        29
      ]
    },
    {
      "id": 43,
      "x": 0.574,
      "y": 0.537,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 27,
      "x": -1.264,
      "y": 0.537,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -0.939,
      "y": 0.537,
      "layer": 6,
      "closed_by": [
        27
      ]
    },
    {
      "id": 45,
      "x": 0.305,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        43,
        44
      ]
    },
    {
      "id": 31,
      "x": 0.939,
      "y": 0.537,
      "layer": 6,
      "closed_by": [
        36
      ]
    },
    {
      "id": 36,
      "x": 1.264,
      "y": 0.537,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 26,
      "x": 1.424,
      "y": -0.337,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 29,
      "x": 1.098,
      "y": -0.337,
      "layer": 6,
      "closed_by": [
        26
      ]
    },
    {
      "id": 35,
      "x": 0.379,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 39,
      "x": -0.305,
      "y": -0.497,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 32,
      "x": -0.777,
      "y": -0.337,
      "layer": 5,
      "closed_by": [
        28
      ]
    },
    {
      "id": 28,
      "x": -1.1,
      "y": -0.337,
      "layer": 6,
      "closed_by": [
        25
      ]
    },
    {
      "id": 25,
      "x": -1.424,
      "y": -0.337,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 44,
      "y": 0.537,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -0.572,
      "y": 0.537,
      "layer": 5,
      "closed_by": [
        30
      ]
    }
  ],
  "cards_in_layout_amount": 25,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 47.2,
    "star2": 4.8,
    "star3": 47.3
  }
}