{
  "layout_cards": [
    {
      "id": 51,
      "y": -0.495,
      "layer": 1,
      "closed_by": [
        48,
        47
      ]
    },
    {
      "id": 50,
      "x": -0.569,
      "y": 0.861,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 49,
      "x": 0.572,
      "y": 0.861,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 46,
      "x": 0.712,
      "y": 0.782,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 45,
      "x": -0.707,
      "y": 0.782,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 43,
      "x": 1.296,
      "y": 0.782,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -1.452,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 41,
      "y": -0.25,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 37,
      "x": -0.81,
      "y": -0.337,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 36,
      "y": -0.09,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -1.292,
      "y": 0.777,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -0.962,
      "y": 0.703,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 33,
      "x": -1.371,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 32,
      "x": 1.375,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 31,
      "x": 1.452,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 38,
      "x": 0.842,
      "y": -0.337,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 0.574,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 48,
      "x": 0.307,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        41,
        40
      ]
    },
    {
      "id": 47,
      "x": -0.296,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        41,
        39
      ]
    },
    {
      "id": 39,
      "x": -0.555,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 44,
      "x": 0.976,
      "y": 0.702,
      "layer": 3,
      "closed_by": [
        43
      ]
    }
  ],
  "cards_in_layout_amount": 21,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 38.7,
    "star2": 1.7,
    "star3": 59.0
  }
}