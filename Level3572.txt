{
  "layout_cards": [
    {
      "id": 9,
      "x": 0.331,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 10,
      "x": -2.223,
      "y": 0.059,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 11,
      "x": 2.226,
      "y": 0.625,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 12,
      "x": 1.906,
      "y": -0.335,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 13,
      "x": -0.465,
      "y": -0.093,
      "layer": 3,
      "closed_by": [
        51
      ]
    },
    {
      "id": 14,
      "x": 0.465,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        9
      ]
    },
    {
      "id": 15,
      "x": -1.745,
      "y": 0.861,
      "layer": 4,
      "closed_by": [
        10,
        16
      ]
    },
    {
      "id": 16,
      "x": -1.906,
      "y": 1.019,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 18,
      "x": 1.743,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        12
      ]
    },
    {
      "id": 19,
      "x": -1.904,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        15
      ]
    },
    {
      "id": 20,
      "x": 1.906,
      "y": -0.097,
      "layer": 3,
      "closed_by": [
        11,
        18
      ]
    },
    {
      "id": 21,
      "x": 1.373,
      "y": -0.335,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 22,
      "x": -2.223,
      "y": -0.259,
      "layer": 4,
      "closed_by": [
        10
      ]
    },
    {
      "id": 23,
      "x": 2.226,
      "y": 0.944,
      "layer": 4,
      "closed_by": [
        11
      ]
    },
    {
      "id": 24,
      "x": -2.065,
      "y": -0.093,
      "layer": 3,
      "closed_by": [
        22,
        35
      ]
    },
    {
      "id": 25,
      "x": 2.065,
      "y": 0.781,
      "layer": 3,
      "closed_by": [
        23,
        38
      ]
    },
    {
      "id": 26,
      "x": 1.906,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        20,
        25,
        40
      ]
    },
    {
      "id": 27,
      "x": -1.904,
      "y": 0.221,
      "layer": 2,
      "closed_by": [
        19,
        24,
        39
      ]
    },
    {
      "id": 28,
      "x": 0.059,
      "y": 0.777,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 29,
      "x": 0.259,
      "y": -0.093,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 30,
      "x": -0.252,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 31,
      "x": 0.493,
      "y": -0.093,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 32,
      "x": -0.49,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 33,
      "x": 0.652,
      "y": 0.3,
      "layer": 2,
      "closed_by": [
        14,
        31
      ]
    },
    {
      "id": 34,
      "x": -0.652,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        13,
        32
      ]
    },
    {
      "id": 35,
      "x": -1.664,
      "y": -0.256,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -0.059,
      "y": -0.096,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 37,
      "x": -1.373,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        15
      ]
    },
    {
      "id": 38,
      "x": 1.666,
      "y": 0.939,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -1.506,
      "y": -0.093,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 40,
      "x": 1.506,
      "y": 0.782,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 41,
      "x": 1.348,
      "y": 0.541,
      "layer": 3,
      "closed_by": [
        18,
        40
      ]
    },
    {
      "id": 42,
      "x": -1.345,
      "y": 0.143,
      "layer": 3,
      "closed_by": [
        15,
        39
      ]
    },
    {
      "id": 43,
      "x": 1.174,
      "y": 0.3,
      "layer": 2,
      "closed_by": [
        21,
        41
      ]
    },
    {
      "id": 44,
      "x": 1.506,
      "y": -0.096,
      "layer": 1,
      "closed_by": [
        26,
        43
      ]
    },
    {
      "id": 45,
      "x": 0.888,
      "y": 0.781,
      "layer": 1,
      "closed_by": [
        33,
        43
      ]
    },
    {
      "id": 46,
      "x": -1.184,
      "y": 0.303,
      "layer": 2,
      "closed_by": [
        37,
        42
      ]
    },
    {
      "id": 47,
      "x": -1.529,
      "y": 0.781,
      "layer": 1,
      "closed_by": [
        27,
        46
      ]
    },
    {
      "id": 48,
      "x": -0.888,
      "y": -0.093,
      "layer": 1,
      "closed_by": [
        34,
        46
      ]
    },
    {
      "id": 49,
      "x": -0.893,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        34,
        46
      ]
    },
    {
      "id": 50,
      "x": 0.892,
      "y": -0.093,
      "layer": 1,
      "closed_by": [
        33,
        43
      ]
    },
    {
      "id": 51,
      "x": -0.331,
      "y": -0.093,
      "layer": 4,
      "closed_by": [
        36
      ]
    }
  ],
  "cards_in_layout_amount": 42,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 11.9,
    "star2": 72.1,
    "star3": 15.7
  }
}