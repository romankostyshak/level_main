{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.513,
      "y": 0.846,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 50,
      "x": 0.517,
      "y": -0.275,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 49,
      "x": 1.23,
      "y": 0.846,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 48,
      "x": 1.235,
      "y": -0.275,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 47,
      "x": -0.523,
      "y": 0.8,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 46,
      "x": -0.523,
      "y": -0.228,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 45,
      "x": -0.791,
      "y": 0.8,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 44,
      "x": -1.057,
      "y": 0.804,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 43,
      "x": -1.057,
      "y": -0.228,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 42,
      "x": 0.261,
      "y": 0.303,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 41,
      "x": -0.259,
      "y": 0.303,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 40,
      "y": 0.303,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 39,
      "x": -1.74,
      "y": 0.736,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 38,
      "x": -1.998,
      "y": 0.736,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 37,
      "x": -2.259,
      "y": 0.736,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 36,
      "x": -2.259,
      "y": 0.303,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -2.259,
      "y": -0.136,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 34,
      "x": -1.74,
      "y": -0.136,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 33,
      "x": -2.0,
      "y": -0.136,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 31,
      "x": 2.009,
      "y": 0.3,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 1.74,
      "y": 0.3,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 29,
      "x": 1.475,
      "y": 0.3,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 28,
      "x": -1.74,
      "y": 0.3,
      "layer": 1,
      "closed_by": [
        39,
        34
      ]
    },
    {
      "id": 32,
      "x": -0.791,
      "y": -0.228,
      "layer": 2,
      "closed_by": [
        46
      ]
    }
  ],
  "cards_in_layout_amount": 24,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 63.4,
    "star2": 10.1,
    "star3": 25.8
  }
}