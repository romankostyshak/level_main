{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.532,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 50,
      "x": 0.527,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 49,
      "x": 0.527,
      "y": 0.859,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 48,
      "x": 0.527,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 47,
      "y": 0.859,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 46,
      "y": 0.537,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 45,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "y": -0.1,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 43,
      "y": -0.418,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -1.067,
      "y": -0.017,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 0.794,
      "y": 0.059,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 40,
      "x": 1.338,
      "y": 0.059,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 39,
      "x": -0.787,
      "y": 0.059,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 38,
      "x": -0.532,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 37,
      "x": -0.532,
      "y": 0.859,
      "layer": 1,
      "closed_by": [
        51
      ]
    },
    {
      "id": 36,
      "x": -1.065,
      "y": 0.879,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -1.062,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 34,
      "x": 1.054,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 33,
      "x": 1.057,
      "y": 0.879,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 1.588,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 31,
      "x": 1.588,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 30,
      "x": 1.588,
      "y": 0.859,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 29,
      "x": -1.059,
      "y": 0.995,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 28,
      "x": -1.592,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 27,
      "x": -1.592,
      "y": 0.859,
      "layer": 1,
      "closed_by": [
        25
      ]
    },
    {
      "id": 26,
      "x": -1.327,
      "y": 0.059,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 25,
      "x": -1.592,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 24,
      "x": 1.059,
      "y": 0.995,
      "layer": 1,
      "closed_by": [
        34
      ]
    },
    {
      "id": 23,
      "x": 1.067,
      "y": -0.013,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 29,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 39.3,
    "star2": 54.5,
    "star3": 5.8
  }
}