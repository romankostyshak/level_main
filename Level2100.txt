{
  "layout_cards": [
    {
      "id": 49,
      "x": -0.33,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": 0.333,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "x": 0.465,
      "y": -0.409,
      "layer": 2,
      "closed_by": [
        50,
        41
      ]
    },
    {
      "id": 46,
      "x": -0.465,
      "y": -0.412,
      "layer": 2,
      "closed_by": [
        45,
        42
      ]
    },
    {
      "id": 45,
      "x": -0.625,
      "y": -0.574,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 50,
      "x": 0.625,
      "y": -0.574,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 44,
      "x": -0.414,
      "y": -0.573,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 43,
      "x": 0.411,
      "y": -0.574,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -0.465,
      "y": 0.3,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 41,
      "x": 0.462,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 40,
      "x": -0.331,
      "y": 0.298,
      "layer": 4,
      "closed_by": [
        35,
        34
      ]
    },
    {
      "id": 39,
      "x": 0.333,
      "y": 0.3,
      "layer": 4,
      "closed_by": [
        38,
        34
      ]
    },
    {
      "id": 38,
      "x": 0.625,
      "y": 0.296,
      "layer": 5,
      "closed_by": [
        32,
        31
      ]
    },
    {
      "id": 35,
      "x": -0.625,
      "y": 0.298,
      "layer": 5,
      "closed_by": [
        33,
        30
      ]
    },
    {
      "id": 34,
      "x": 0.001,
      "y": 0.462,
      "layer": 5,
      "closed_by": [
        33,
        32
      ]
    },
    {
      "id": 33,
      "x": -0.307,
      "y": 0.62,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 0.307,
      "y": 0.62,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 0.893,
      "y": 0.86,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -0.865,
      "y": 0.861,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 29,
      "x": 1.187,
      "y": 1.019,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 28,
      "x": 1.189,
      "y": 0.14,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 36,
      "x": -1.187,
      "y": 1.018,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 27,
      "x": -1.19,
      "y": 0.14,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 26,
      "x": 1.269,
      "y": 0.54,
      "layer": 4,
      "closed_by": [
        29,
        28
      ]
    },
    {
      "id": 25,
      "x": -1.266,
      "y": 0.541,
      "layer": 4,
      "closed_by": [
        36,
        27
      ]
    },
    {
      "id": 24,
      "x": 1.266,
      "y": -0.333,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 23,
      "x": -1.266,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 22,
      "x": 1.347,
      "y": 0.303,
      "layer": 3,
      "closed_by": [
        26,
        24
      ]
    },
    {
      "id": 21,
      "x": 1.345,
      "y": -0.577,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 37,
      "x": -1.345,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        25,
        23
      ]
    },
    {
      "id": 20,
      "x": -1.345,
      "y": -0.574,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 19,
      "x": 1.424,
      "y": -0.331,
      "layer": 2,
      "closed_by": [
        22,
        21
      ]
    },
    {
      "id": 18,
      "x": -1.429,
      "y": -0.335,
      "layer": 2,
      "closed_by": [
        37,
        20
      ]
    }
  ],
  "cards_in_layout_amount": 33,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 42.8,
    "star2": 53.4,
    "star3": 2.8
  }
}