{
  "layout_cards": [
    {
      "id": 51,
      "x": 1.291,
      "y": -0.246,
      "layer": 4,
      "closed_by": [
        44
      ]
    },
    {
      "id": 50,
      "x": 1.373,
      "y": -0.162,
      "layer": 3,
      "closed_by": [
        51
      ]
    },
    {
      "id": 49,
      "x": 1.45,
      "y": -0.079,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 48,
      "x": -1.452,
      "y": -0.086,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 47,
      "x": 1.531,
      "y": -0.004,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 46,
      "x": -1.531,
      "y": -0.003,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 45,
      "x": -1.292,
      "y": -0.254,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 44,
      "x": 1.213,
      "y": -0.331,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 43,
      "x": -1.213,
      "y": -0.333,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -1.371,
      "y": -0.165,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 41,
      "x": -0.625,
      "y": -0.335,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 40,
      "x": -0.319,
      "y": 0.063,
      "layer": 2,
      "closed_by": [
        36,
        35,
        34
      ]
    },
    {
      "id": 39,
      "x": 0.625,
      "y": 0.541,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 38,
      "x": 0.624,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 37,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        40,
        31
      ]
    },
    {
      "id": 36,
      "x": -0.625,
      "y": 0.541,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 35,
      "y": 0.541,
      "layer": 3,
      "closed_by": [
        33,
        28
      ]
    },
    {
      "id": 34,
      "x": -0.319,
      "y": -0.335,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -0.319,
      "y": 0.541,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -0.319,
      "y": 0.883,
      "layer": 2,
      "closed_by": [
        36,
        35
      ]
    },
    {
      "id": 31,
      "x": 0.319,
      "y": 0.063,
      "layer": 2,
      "closed_by": [
        39,
        35,
        30
      ]
    },
    {
      "id": 30,
      "x": 0.319,
      "y": -0.335,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 29,
      "x": 0.319,
      "y": 0.883,
      "layer": 2,
      "closed_by": [
        39,
        35
      ]
    },
    {
      "id": 28,
      "x": 0.319,
      "y": 0.541,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 0.625,
      "y": 0.883,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 26,
      "x": -0.625,
      "y": 0.883,
      "layer": 1,
      "closed_by": [
        32
      ]
    }
  ],
  "cards_in_layout_amount": 26,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 53.3,
    "star2": 14.4,
    "star3": 31.7
  }
}