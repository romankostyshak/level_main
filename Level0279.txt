{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.293,
      "y": 0.527,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 50,
      "x": 1.44,
      "y": -0.347,
      "layer": 1,
      "closed_by": [
        44,
        27
      ]
    },
    {
      "id": 49,
      "x": -0.912,
      "y": 0.256,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 48,
      "x": 0.912,
      "y": 0.944,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 47,
      "x": -0.912,
      "y": 0.943,
      "layer": 2,
      "closed_by": [
        49
      ]
    },
    {
      "id": 46,
      "x": -0.912,
      "y": -0.34,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 45,
      "x": 0.915,
      "y": -0.358,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": 1.174,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        33,
        30
      ]
    },
    {
      "id": 43,
      "x": 1.955,
      "y": -0.344,
      "layer": 1,
      "closed_by": [
        27
      ]
    },
    {
      "id": 42,
      "x": 0.293,
      "y": -0.017,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 41,
      "x": 0.293,
      "y": -0.559,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 40,
      "x": -0.289,
      "y": -0.017,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 39,
      "x": -1.434,
      "y": -0.34,
      "layer": 1,
      "closed_by": [
        31,
        28
      ]
    },
    {
      "id": 38,
      "x": -1.432,
      "y": 0.256,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 37,
      "x": -0.289,
      "y": -0.559,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -0.289,
      "y": 0.527,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 35,
      "x": 0.293,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        51
      ]
    },
    {
      "id": 34,
      "x": -1.166,
      "y": 0.469,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 0.912,
      "y": 0.261,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 32,
      "x": -1.947,
      "y": -0.342,
      "layer": 1,
      "closed_by": [
        28
      ]
    },
    {
      "id": 31,
      "x": -1.174,
      "y": -0.409,
      "layer": 2,
      "closed_by": [
        49,
        38
      ]
    },
    {
      "id": 30,
      "x": 1.429,
      "y": 0.25,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 29,
      "x": 1.174,
      "y": 0.462,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 28,
      "x": -1.7,
      "y": 0.067,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 27,
      "x": 1.694,
      "y": 0.064,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 26,
      "x": -0.287,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        36
      ]
    }
  ],
  "cards_in_layout_amount": 26,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 54.6,
    "star2": 21.5,
    "star3": 23.1
  }
}