{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.291,
      "y": 0.259,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 50,
      "x": -0.287,
      "y": 0.365,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 49,
      "x": 1.353,
      "y": 0.975,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 48,
      "x": -1.439,
      "y": -0.518,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 47,
      "x": -1.521,
      "y": 0.828,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 46,
      "x": -1.437,
      "y": 0.967,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 45,
      "x": -1.61,
      "y": 0.68,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 44,
      "x": 1.432,
      "y": 0.833,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 43,
      "x": 1.521,
      "y": 0.685,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 42,
      "x": 1.44,
      "y": -0.377,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 41,
      "x": 1.521,
      "y": -0.238,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 40,
      "x": -1.687,
      "y": 0.209,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 39,
      "x": -1.967,
      "y": 0.209,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 1.898,
      "y": 0.224,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 37,
      "x": 1.613,
      "y": 0.224,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 36,
      "x": 0.293,
      "y": 0.18,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 0.291,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 34,
      "x": 1.358,
      "y": -0.517,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 33,
      "x": -1.605,
      "y": -0.238,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 32,
      "x": -1.521,
      "y": -0.377,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 31,
      "x": -0.286,
      "y": 0.324,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 30,
      "x": -0.286,
      "y": 0.442,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -0.286,
      "y": 0.398,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 28,
      "x": 0.291,
      "y": 0.303,
      "layer": 1,
      "closed_by": [
        51
      ]
    }
  ],
  "cards_in_layout_amount": 24,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 60.4,
    "star2": 11.1,
    "star3": 28.0
  }
}