{
  "layout_cards": [
    {
      "id": 51,
      "x": 1.21,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 50,
      "x": 2.483,
      "y": 0.296,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 44,
      "x": -0.326,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 43,
      "x": 0.335,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 41,
      "x": -2.618,
      "y": 0.298,
      "layer": 4,
      "closed_by": [],
      "effect_id": 3
    },
    {
      "id": 36,
      "x": -0.324,
      "y": 1.036,
      "layer": 1,
      "closed_by": [
        34
      ]
    },
    {
      "id": 34,
      "y": 0.708,
      "layer": 2,
      "closed_by": [
        22,
        23
      ]
    },
    {
      "id": 33,
      "x": 1.848,
      "y": 0.296,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 32,
      "x": -1.945,
      "y": 0.298,
      "layer": 3,
      "closed_by": [],
      "effect_id": 3
    },
    {
      "id": 29,
      "x": -1.858,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 27,
      "x": 1.932,
      "y": 0.298,
      "layer": 3,
      "closed_by": [],
      "effect_id": 3
    },
    {
      "id": 31,
      "x": -2.49,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        28
      ]
    },
    {
      "id": 28,
      "x": -2.532,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 40,
      "x": -2.575,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 47,
      "x": 2.526,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 46,
      "x": 2.571,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 26,
      "x": 2.618,
      "y": 0.298,
      "layer": 4,
      "closed_by": [],
      "effect_id": 3
    },
    {
      "id": 39,
      "x": -1.901,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 37,
      "x": -1.251,
      "y": 0.298,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -1.205,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        37
      ]
    },
    {
      "id": 49,
      "x": 1.253,
      "y": 0.298,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 48,
      "x": 1.889,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        27
      ]
    },
    {
      "id": 38,
      "x": 0.331,
      "y": 1.036,
      "layer": 1,
      "closed_by": [
        34
      ]
    },
    {
      "id": 42,
      "x": 0.333,
      "y": 0.224,
      "layer": 1,
      "closed_by": [
        34,
        35
      ]
    },
    {
      "id": 45,
      "x": -0.328,
      "y": 0.224,
      "layer": 1,
      "closed_by": [
        34,
        35
      ]
    },
    {
      "id": 35,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        21,
        24
      ]
    },
    {
      "id": 22,
      "x": -0.28,
      "y": 0.986,
      "layer": 3,
      "closed_by": [
        20
      ]
    },
    {
      "id": 23,
      "x": 0.284,
      "y": 0.985,
      "layer": 3,
      "closed_by": [
        20
      ]
    },
    {
      "id": 21,
      "x": -0.28,
      "y": -0.531,
      "layer": 3,
      "closed_by": [
        19
      ]
    },
    {
      "id": 24,
      "x": 0.291,
      "y": -0.535,
      "layer": 3,
      "closed_by": [
        19
      ]
    },
    {
      "id": 20,
      "y": 0.754,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 19,
      "y": -0.305,
      "layer": 4,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 32,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 23.8,
    "star2": 67.2,
    "star3": 8.4
  }
}