{
  "layout_cards": [
    {
      "id": 14,
      "x": 1.694,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        16
      ]
    },
    {
      "id": 15,
      "x": -1.825,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 16,
      "x": 1.906,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        17
      ]
    },
    {
      "id": 17,
      "x": 2.147,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        20,
        49
      ]
    },
    {
      "id": 18,
      "x": -1.603,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        15
      ]
    },
    {
      "id": 19,
      "x": -2.062,
      "y": 0.222,
      "layer": 3,
      "closed_by": [
        21,
        48
      ]
    },
    {
      "id": 20,
      "x": 2.384,
      "y": -0.18,
      "layer": 4,
      "closed_by": [
        50
      ]
    },
    {
      "id": 21,
      "x": -1.868,
      "y": -0.224,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 22,
      "x": 0.587,
      "y": 0.319,
      "layer": 5,
      "closed_by": [
        44,
        45
      ]
    },
    {
      "id": 23,
      "x": -0.666,
      "y": 0.305,
      "layer": 5,
      "closed_by": [
        46,
        47
      ]
    },
    {
      "id": 24,
      "x": 0.887,
      "y": 0.666,
      "layer": 4,
      "closed_by": [
        22,
        43
      ]
    },
    {
      "id": 25,
      "x": 0.377,
      "y": -0.127,
      "layer": 4,
      "closed_by": [
        22,
        41
      ]
    },
    {
      "id": 26,
      "x": -0.374,
      "y": 0.671,
      "layer": 4,
      "closed_by": [
        23,
        40
      ]
    },
    {
      "id": 27,
      "x": -0.883,
      "y": -0.136,
      "layer": 4,
      "closed_by": [
        23,
        42
      ]
    },
    {
      "id": 28,
      "x": -0.628,
      "y": 0.263,
      "layer": 3,
      "closed_by": [
        26,
        27
      ]
    },
    {
      "id": 29,
      "x": 0.629,
      "y": 0.268,
      "layer": 3,
      "closed_by": [
        24,
        25
      ]
    },
    {
      "id": 30,
      "x": -1.08,
      "y": -0.574,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 31,
      "x": 0.172,
      "y": -0.574,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 32,
      "x": 1.169,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 33,
      "x": -0.09,
      "y": 1.018,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 34,
      "x": -0.331,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        28,
        33
      ]
    },
    {
      "id": 35,
      "x": 0.929,
      "y": 0.619,
      "layer": 2,
      "closed_by": [
        29,
        32
      ]
    },
    {
      "id": 36,
      "x": 0.421,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        29,
        31
      ]
    },
    {
      "id": 37,
      "x": -0.587,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        34,
        39
      ]
    },
    {
      "id": 38,
      "x": 0.671,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        35,
        36
      ]
    },
    {
      "id": 39,
      "x": -0.837,
      "y": -0.18,
      "layer": 2,
      "closed_by": [
        28,
        30
      ]
    },
    {
      "id": 40,
      "x": -0.128,
      "y": 0.967,
      "layer": 5,
      "closed_by": [
        46
      ]
    },
    {
      "id": 41,
      "x": 0.229,
      "y": -0.526,
      "layer": 5,
      "closed_by": [
        44
      ]
    },
    {
      "id": 42,
      "x": -1.036,
      "y": -0.523,
      "layer": 5,
      "closed_by": [
        47
      ]
    },
    {
      "id": 43,
      "x": 1.126,
      "y": 0.971,
      "layer": 5,
      "closed_by": [
        45
      ]
    },
    {
      "id": 44,
      "x": 0.333,
      "y": -0.078,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 45,
      "x": 0.845,
      "y": 0.712,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 46,
      "x": -0.416,
      "y": 0.717,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 47,
      "x": -0.93,
      "y": -0.086,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 48,
      "x": -2.305,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 49,
      "x": 1.949,
      "y": 0.666,
      "layer": 4,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": 2.184,
      "y": 0.263,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -2.105,
      "y": 0.172,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 38,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 15.5,
    "star2": 64.5,
    "star3": 19.5
  }
}