{
  "layout_cards": [
    {
      "id": 5,
      "x": -1.906,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        9,
        17
      ]
    },
    {
      "id": 8,
      "x": 1.906,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        10,
        11
      ]
    },
    {
      "id": 9,
      "x": -1.825,
      "y": 0.638,
      "layer": 2,
      "closed_by": [
        12
      ]
    },
    {
      "id": 10,
      "x": 1.83,
      "y": -0.187,
      "layer": 2,
      "closed_by": [
        13
      ]
    },
    {
      "id": 11,
      "x": 1.827,
      "y": 0.634,
      "layer": 2,
      "closed_by": [
        14
      ]
    },
    {
      "id": 12,
      "x": -1.745,
      "y": 0.781,
      "layer": 3,
      "closed_by": [
        15
      ]
    },
    {
      "id": 13,
      "x": 1.743,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 14,
      "x": 1.746,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 15,
      "x": -1.664,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        26,
        39
      ]
    },
    {
      "id": 16,
      "x": -1.745,
      "y": -0.335,
      "layer": 3,
      "closed_by": [
        15
      ]
    },
    {
      "id": 17,
      "x": -1.825,
      "y": -0.187,
      "layer": 2,
      "closed_by": [
        16
      ]
    },
    {
      "id": 18,
      "x": 1.664,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        27,
        42
      ]
    },
    {
      "id": 19,
      "x": -0.305,
      "y": -0.2,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 20,
      "x": -0.305,
      "y": 0.648,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 21,
      "x": 0.307,
      "y": 0.638,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 22,
      "x": 0.003,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        20,
        21
      ]
    },
    {
      "id": 23,
      "x": -0.002,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        19,
        37
      ]
    },
    {
      "id": 24,
      "x": -0.001,
      "y": -0.497,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 25,
      "x": 0.002,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 26,
      "x": -1.343,
      "y": 0.699,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 1.347,
      "y": 0.699,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 0.953,
      "y": 0.782,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 29,
      "x": 0.573,
      "y": 0.864,
      "layer": 3,
      "closed_by": [
        21,
        28
      ]
    },
    {
      "id": 30,
      "x": -0.572,
      "y": 0.865,
      "layer": 3,
      "closed_by": [
        20,
        36
      ]
    },
    {
      "id": 31,
      "x": -1.105,
      "y": 0.856,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 32,
      "x": 1.105,
      "y": 0.856,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 33,
      "x": -0.707,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        30,
        31
      ]
    },
    {
      "id": 34,
      "x": 0.703,
      "y": 0.935,
      "layer": 2,
      "closed_by": [
        29,
        32
      ]
    },
    {
      "id": 35,
      "x": 0.331,
      "y": 1.016,
      "layer": 1,
      "closed_by": [
        25,
        34
      ]
    },
    {
      "id": 36,
      "x": -0.944,
      "y": 0.785,
      "layer": 4,
      "closed_by": [
        26
      ]
    },
    {
      "id": 37,
      "x": 0.303,
      "y": -0.2,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -0.331,
      "y": 1.018,
      "layer": 1,
      "closed_by": [
        25,
        33
      ]
    },
    {
      "id": 39,
      "x": -1.347,
      "y": -0.259,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 40,
      "x": -0.944,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 41,
      "x": -0.569,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        19,
        40
      ]
    },
    {
      "id": 42,
      "x": 1.345,
      "y": -0.261,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 43,
      "x": 0.952,
      "y": -0.34,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 44,
      "x": 0.574,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        37,
        43
      ]
    },
    {
      "id": 45,
      "x": 1.105,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 46,
      "x": -0.703,
      "y": -0.497,
      "layer": 2,
      "closed_by": [
        41,
        50
      ]
    },
    {
      "id": 47,
      "x": 0.703,
      "y": -0.497,
      "layer": 2,
      "closed_by": [
        44,
        45
      ]
    },
    {
      "id": 48,
      "x": 0.333,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        24,
        47
      ]
    },
    {
      "id": 49,
      "x": -0.331,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        24,
        46
      ]
    },
    {
      "id": 50,
      "x": -1.105,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        40
      ]
    }
  ],
  "cards_in_layout_amount": 44,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 0.6,
    "star2": 20.5,
    "star3": 78.2
  }
}