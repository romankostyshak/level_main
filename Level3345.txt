{
  "layout_cards": [
    {
      "id": 2,
      "x": 1.613,
      "y": -0.335,
      "layer": 4,
      "closed_by": [
        16
      ]
    },
    {
      "id": 3,
      "x": -0.33,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        15
      ]
    },
    {
      "id": 4,
      "x": 0.331,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        18
      ]
    },
    {
      "id": 5,
      "x": 0.865,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        14
      ]
    },
    {
      "id": 6,
      "x": -0.333,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        19
      ]
    },
    {
      "id": 7,
      "x": -0.865,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        13
      ]
    },
    {
      "id": 8,
      "x": 0.333,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        17
      ]
    },
    {
      "id": 9,
      "x": 0.865,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        12
      ]
    },
    {
      "id": 10,
      "x": -0.865,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        11
      ]
    },
    {
      "id": 11,
      "x": -0.944,
      "y": 0.944,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 12,
      "x": 0.944,
      "y": 0.944,
      "layer": 2,
      "closed_by": [
        21
      ]
    },
    {
      "id": 13,
      "x": -0.944,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 14,
      "x": 0.944,
      "y": -0.34,
      "layer": 2,
      "closed_by": [
        21
      ]
    },
    {
      "id": 15,
      "x": -0.259,
      "y": 0.943,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 16,
      "x": 1.531,
      "y": -0.256,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 17,
      "x": 0.266,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 18,
      "x": 0.263,
      "y": -0.34,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 19,
      "x": -0.259,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 20,
      "x": -1.212,
      "y": 0.305,
      "layer": 3,
      "closed_by": [
        24,
        25
      ]
    },
    {
      "id": 21,
      "x": 1.213,
      "y": 0.3,
      "layer": 3,
      "closed_by": [
        2,
        22
      ]
    },
    {
      "id": 22,
      "x": 1.613,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 23,
      "x": 2.144,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 24,
      "x": -1.613,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 25,
      "x": -1.613,
      "y": -0.335,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 26,
      "x": 2.144,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        32
      ]
    },
    {
      "id": 27,
      "x": -2.144,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 28,
      "x": -2.144,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 29,
      "x": -1.531,
      "y": 0.861,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -1.534,
      "y": -0.259,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 1.534,
      "y": 0.861,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 2.226,
      "y": -0.259,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 2.223,
      "y": 0.865,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -2.223,
      "y": 0.861,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -1.985,
      "y": 0.303,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 36,
      "x": -1.883,
      "y": -0.179,
      "layer": 3,
      "closed_by": [
        25,
        28
      ]
    },
    {
      "id": 37,
      "x": -2.223,
      "y": -0.259,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 1.876,
      "y": -0.177,
      "layer": 3,
      "closed_by": [
        2,
        26
      ]
    },
    {
      "id": 39,
      "x": 1.873,
      "y": 0.785,
      "layer": 3,
      "closed_by": [
        22,
        23
      ]
    },
    {
      "id": 40,
      "x": -1.881,
      "y": 0.785,
      "layer": 3,
      "closed_by": [
        24,
        27
      ]
    },
    {
      "id": 41,
      "x": 1.878,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        38,
        39
      ]
    },
    {
      "id": 42,
      "x": -1.881,
      "y": 0.303,
      "layer": 2,
      "closed_by": [
        36,
        40
      ]
    },
    {
      "id": 43,
      "x": 1.985,
      "y": 0.3,
      "layer": 1,
      "closed_by": [
        41
      ]
    },
    {
      "id": 44,
      "x": 0.008,
      "y": 0.711,
      "layer": 3,
      "closed_by": [
        46,
        48
      ]
    },
    {
      "id": 45,
      "x": 0.333,
      "y": 0.712,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 46,
      "x": -0.259,
      "y": 0.298,
      "layer": 4,
      "closed_by": [
        49,
        50
      ]
    },
    {
      "id": 47,
      "y": -0.108,
      "layer": 3,
      "closed_by": [
        46,
        48
      ]
    },
    {
      "id": 48,
      "x": 0.261,
      "y": 0.305,
      "layer": 4,
      "closed_by": [
        45,
        51
      ]
    },
    {
      "id": 49,
      "x": -0.328,
      "y": 0.716,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 50,
      "x": -0.331,
      "y": -0.108,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 51,
      "x": 0.33,
      "y": -0.104,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 50,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 0.4,
    "star2": 28.7,
    "star3": 70.0
  }
}