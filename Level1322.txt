{
  "layout_cards": [
    {
      "id": 51,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        26,
        25,
        50,
        41
      ]
    },
    {
      "id": 49,
      "x": -0.006,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        45,
        44
      ]
    },
    {
      "id": 48,
      "x": 0.545,
      "y": 0.296,
      "layer": 3,
      "closed_by": [
        44,
        43
      ]
    },
    {
      "id": 47,
      "y": -0.414,
      "layer": 3,
      "closed_by": [
        43,
        42
      ]
    },
    {
      "id": 46,
      "x": -0.541,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        45,
        42
      ]
    },
    {
      "id": 45,
      "x": -0.259,
      "y": 0.703,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 44,
      "x": 0.259,
      "y": 0.703,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 43,
      "x": 0.259,
      "y": -0.104,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 42,
      "x": -0.259,
      "y": -0.104,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 40,
      "x": -1.452,
      "y": -0.453,
      "angle": 340.0,
      "layer": 1,
      "closed_by": [
        37
      ]
    },
    {
      "id": 39,
      "x": 1.455,
      "y": 0.976,
      "angle": 340.0,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 38,
      "x": 1.677,
      "y": 0.847,
      "angle": 320.0,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 37,
      "x": -1.669,
      "y": -0.326,
      "angle": 320.0,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 36,
      "x": 1.848,
      "y": 0.638,
      "angle": 300.0,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 35,
      "x": -1.84,
      "y": -0.125,
      "angle": 300.0,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 34,
      "x": 1.94,
      "y": 0.391,
      "angle": 280.0,
      "layer": 4,
      "closed_by": [
        32
      ]
    },
    {
      "id": 33,
      "x": -1.932,
      "y": 0.118,
      "angle": 280.0,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 32,
      "x": 1.87,
      "y": 0.601,
      "angle": 300.0,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 30,
      "x": 1.707,
      "y": 0.819,
      "angle": 320.0,
      "layer": 6,
      "closed_by": [
        31
      ]
    },
    {
      "id": 29,
      "x": -1.863,
      "y": -0.086,
      "angle": 300.0,
      "layer": 5,
      "closed_by": [
        28
      ]
    },
    {
      "id": 28,
      "x": -1.705,
      "y": -0.296,
      "angle": 320.0,
      "layer": 6,
      "closed_by": [
        27
      ]
    },
    {
      "id": 27,
      "x": -1.493,
      "y": -0.441,
      "angle": 340.0,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 26,
      "x": 0.412,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        49,
        48
      ]
    },
    {
      "id": 25,
      "x": 0.418,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        48,
        47
      ]
    },
    {
      "id": 50,
      "x": -0.416,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        49,
        46
      ]
    },
    {
      "id": 41,
      "x": -0.414,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        47,
        46
      ]
    },
    {
      "id": 24,
      "x": 0.002,
      "y": 0.298,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 1.493,
      "y": 0.962,
      "angle": 340.0,
      "layer": 7,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 28,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 62.0,
    "star2": 23.0,
    "star3": 14.6
  }
}