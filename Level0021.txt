{
  "layout_cards": [
    {
      "id": 51,
      "x": 1.72,
      "y": -0.597,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 50,
      "x": -2.319,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 49,
      "x": -2.319,
      "y": 0.6,
      "layer": 1,
      "closed_by": [
        39,
        38
      ]
    },
    {
      "id": 45,
      "x": 2.239,
      "y": -0.597,
      "layer": 1,
      "closed_by": [
        44,
        40
      ]
    },
    {
      "id": 44,
      "x": 2.519,
      "y": -0.224,
      "layer": 2,
      "closed_by": [
        41,
        47
      ]
    },
    {
      "id": 42,
      "x": 1.72,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 2.24,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 1.96,
      "y": -0.224,
      "layer": 2,
      "closed_by": [
        42,
        41
      ]
    },
    {
      "id": 39,
      "x": -2.039,
      "y": 0.209,
      "layer": 2,
      "closed_by": [
        50,
        30
      ]
    },
    {
      "id": 38,
      "x": -2.588,
      "y": 0.208,
      "layer": 2,
      "closed_by": [
        50,
        43
      ]
    },
    {
      "id": 37,
      "x": -0.518,
      "y": 0.537,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 36,
      "x": 0.518,
      "y": -0.1,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 35,
      "x": -0.254,
      "y": 0.418,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 0.266,
      "y": -0.017,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -0.254,
      "y": 0.418,
      "layer": 1,
      "closed_by": [
        37,
        31
      ]
    },
    {
      "id": 32,
      "x": 0.266,
      "y": -0.023,
      "layer": 1,
      "closed_by": [
        36,
        31
      ]
    },
    {
      "id": 31,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        35,
        34
      ],
      "card_type": 5
    },
    {
      "id": 30,
      "x": -1.799,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -1.799,
      "y": 0.6,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 48,
      "x": -2.838,
      "y": 0.6,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 43,
      "x": -2.838,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 47,
      "x": 2.759,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 46,
      "x": 2.759,
      "y": -0.597,
      "layer": 1,
      "closed_by": [
        44
      ]
    }
  ],
  "cards_in_layout_amount": 23,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 48.8,
    "star2": 4.5,
    "star3": 46.1
  }
}