{
  "layout_cards": [
    {
      "id": 5,
      "x": -2.042,
      "y": -0.432,
      "layer": 1,
      "closed_by": [
        9,
        51
      ]
    },
    {
      "id": 6,
      "x": 2.045,
      "y": -0.43,
      "layer": 1,
      "closed_by": [
        10,
        11
      ]
    },
    {
      "id": 7,
      "x": 1.531,
      "y": -0.432,
      "layer": 1,
      "closed_by": [
        11
      ]
    },
    {
      "id": 8,
      "x": -1.531,
      "y": -0.432,
      "layer": 1,
      "closed_by": [
        9
      ]
    },
    {
      "id": 9,
      "x": -1.871,
      "y": -0.187,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 10,
      "x": 2.388,
      "y": -0.187,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 11,
      "x": 1.868,
      "y": -0.187,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 12,
      "x": -1.427,
      "y": 1.044,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 13,
      "x": -1.94,
      "y": 1.044,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 14,
      "x": 1.939,
      "y": 1.044,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 15,
      "x": -1.636,
      "y": 0.62,
      "layer": 5,
      "closed_by": [
        12,
        13
      ]
    },
    {
      "id": 16,
      "x": -2.148,
      "y": 0.619,
      "layer": 5,
      "closed_by": [
        13
      ]
    },
    {
      "id": 17,
      "x": 1.424,
      "y": 1.044,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 18,
      "x": 1.633,
      "y": 0.62,
      "layer": 5,
      "closed_by": [
        14,
        17
      ]
    },
    {
      "id": 19,
      "x": 2.148,
      "y": 0.615,
      "layer": 5,
      "closed_by": [
        14
      ]
    },
    {
      "id": 20,
      "x": -2.387,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        16
      ]
    },
    {
      "id": 21,
      "x": 2.384,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        19
      ]
    },
    {
      "id": 22,
      "x": 1.871,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        18,
        19
      ]
    },
    {
      "id": 23,
      "x": -2.625,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        20
      ]
    },
    {
      "id": 24,
      "x": 2.625,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 25,
      "x": -2.387,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 26,
      "x": 2.384,
      "y": 0.615,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 27,
      "x": -2.144,
      "y": 1.046,
      "layer": 1,
      "closed_by": [
        16,
        25
      ]
    },
    {
      "id": 28,
      "x": -1.666,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 29,
      "x": 1.664,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 30,
      "x": -1.424,
      "y": 0.619,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 31,
      "x": 1.424,
      "y": 0.615,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 32,
      "x": -1.184,
      "y": 1.044,
      "layer": 1,
      "closed_by": [
        15,
        30
      ]
    },
    {
      "id": 33,
      "x": 1.187,
      "y": 1.044,
      "layer": 1,
      "closed_by": [
        18,
        31
      ]
    },
    {
      "id": 34,
      "x": -0.544,
      "y": -0.335,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 0.55,
      "y": -0.337,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 2.144,
      "y": 1.044,
      "layer": 1,
      "closed_by": [
        19,
        26
      ]
    },
    {
      "id": 37,
      "x": -1.873,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        15,
        16
      ]
    },
    {
      "id": 38,
      "x": -0.544,
      "y": 0.619,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 39,
      "x": 0.546,
      "y": 0.62,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 40,
      "x": -0.465,
      "y": -0.266,
      "layer": 5,
      "closed_by": [
        34
      ]
    },
    {
      "id": 41,
      "x": -0.465,
      "y": 0.541,
      "layer": 5,
      "closed_by": [
        38
      ]
    },
    {
      "id": 42,
      "x": 0.465,
      "y": 0.541,
      "layer": 5,
      "closed_by": [
        39
      ]
    },
    {
      "id": 43,
      "x": 0.462,
      "y": -0.273,
      "layer": 5,
      "closed_by": [
        35
      ]
    },
    {
      "id": 44,
      "x": -0.545,
      "y": 0.142,
      "layer": 4,
      "closed_by": [
        40,
        41
      ]
    },
    {
      "id": 45,
      "y": 0.142,
      "layer": 4,
      "closed_by": [
        40,
        41,
        42,
        43
      ]
    },
    {
      "id": 46,
      "x": -0.3,
      "y": 0.142,
      "layer": 3,
      "closed_by": [
        44,
        45
      ]
    },
    {
      "id": 47,
      "x": 0.298,
      "y": 0.142,
      "layer": 3,
      "closed_by": [
        45,
        50
      ]
    },
    {
      "id": 48,
      "x": -0.001,
      "y": -0.097,
      "layer": 2,
      "closed_by": [
        46,
        47
      ]
    },
    {
      "id": 49,
      "y": 0.3,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 50,
      "x": 0.546,
      "y": 0.142,
      "layer": 4,
      "closed_by": [
        42,
        43
      ]
    },
    {
      "id": 51,
      "x": -2.388,
      "y": -0.187,
      "layer": 2,
      "closed_by": [
        23
      ]
    }
  ],
  "cards_in_layout_amount": 47,
  "cards_in_deck_amount": 32,
  "stars_statistic": {
    "star1": 0.5,
    "star2": 30.9,
    "star3": 67.8
  }
}