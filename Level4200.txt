{
  "layout_cards": [
    {
      "id": 8,
      "x": -1.37,
      "y": -0.039,
      "angle": 335.0,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 9,
      "x": 1.758,
      "y": 0.216,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 10,
      "x": 2.059,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        9
      ]
    },
    {
      "id": 11,
      "x": 2.098,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        10
      ]
    },
    {
      "id": 12,
      "x": 0.008,
      "y": 0.238,
      "angle": 25.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 13,
      "x": 1.46,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        9,
        17
      ]
    },
    {
      "id": 14,
      "x": 0.898,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        17
      ]
    },
    {
      "id": 15,
      "x": -2.2,
      "y": -0.398,
      "angle": 24.999,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 16,
      "x": -0.638,
      "y": 0.119,
      "angle": 25.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 17,
      "x": 1.179,
      "y": 0.216,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 18,
      "x": -1.72,
      "y": -0.179,
      "angle": 25.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 19,
      "x": -2.059,
      "y": 0.56,
      "angle": 25.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 20,
      "x": -2.539,
      "y": 0.34,
      "angle": 25.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 21,
      "x": -2.098,
      "y": 0.5,
      "layer": 3,
      "closed_by": [
        18,
        19,
        20
      ]
    },
    {
      "id": 22,
      "x": -2.437,
      "y": -0.319,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 23,
      "x": -2.499,
      "y": 0.5,
      "layer": 2,
      "closed_by": [
        21
      ]
    },
    {
      "id": 24,
      "x": -2.64,
      "y": 0.379,
      "angle": 25.0,
      "layer": 1,
      "closed_by": [
        22,
        23
      ]
    },
    {
      "id": 25,
      "x": -1.799,
      "y": -0.136,
      "angle": 25.0,
      "layer": 2,
      "closed_by": [
        8,
        21,
        37
      ]
    },
    {
      "id": 26,
      "x": -2.16,
      "y": 0.6,
      "angle": 25.0,
      "layer": 1,
      "closed_by": [
        23
      ]
    },
    {
      "id": 27,
      "x": -2.279,
      "y": -0.358,
      "angle": 25.0,
      "layer": 1,
      "closed_by": [
        21,
        22
      ]
    },
    {
      "id": 28,
      "x": -1.159,
      "y": 0.418,
      "angle": 335.0,
      "layer": 1,
      "closed_by": [
        8,
        29
      ]
    },
    {
      "id": 29,
      "x": -0.865,
      "y": 0.6,
      "angle": 25.0,
      "layer": 2,
      "closed_by": [
        16
      ]
    },
    {
      "id": 30,
      "x": -0.518,
      "y": -0.136,
      "angle": 25.0,
      "layer": 2,
      "closed_by": [
        16
      ]
    },
    {
      "id": 31,
      "x": -0.358,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        30,
        33,
        34
      ]
    },
    {
      "id": 32,
      "x": -0.358,
      "y": 0.638,
      "layer": 1,
      "closed_by": [
        29,
        30,
        33
      ]
    },
    {
      "id": 33,
      "x": -0.158,
      "y": 0.6,
      "angle": 25.0,
      "layer": 2,
      "closed_by": [
        12
      ]
    },
    {
      "id": 34,
      "x": 0.187,
      "y": -0.136,
      "angle": 25.0,
      "layer": 2,
      "closed_by": [
        12
      ]
    },
    {
      "id": 35,
      "x": 1.46,
      "y": 0.638,
      "layer": 3,
      "closed_by": [
        13
      ]
    },
    {
      "id": 36,
      "x": -1.498,
      "y": -0.319,
      "angle": 335.0,
      "layer": 1,
      "closed_by": [
        25
      ]
    },
    {
      "id": 37,
      "x": -2.098,
      "y": -0.319,
      "layer": 3,
      "closed_by": [
        15,
        18,
        20
      ]
    },
    {
      "id": 38,
      "x": 0.898,
      "y": 0.638,
      "layer": 3,
      "closed_by": [
        14
      ]
    },
    {
      "id": 39,
      "x": 0.86,
      "y": 0.638,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 40,
      "x": 1.5,
      "y": 0.638,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 41,
      "x": 1.5,
      "y": 0.859,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 42,
      "x": 0.86,
      "y": 0.859,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 43,
      "x": 0.337,
      "y": 0.638,
      "layer": 1,
      "closed_by": [
        33,
        34
      ]
    },
    {
      "id": 44,
      "x": 2.059,
      "y": 0.638,
      "layer": 1,
      "closed_by": [
        11
      ]
    },
    {
      "id": 45,
      "x": 0.86,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        14
      ]
    },
    {
      "id": 46,
      "x": 2.059,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        11
      ]
    },
    {
      "id": 47,
      "x": 1.46,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 48,
      "x": 0.898,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 49,
      "x": 0.337,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        34
      ]
    },
    {
      "id": 50,
      "x": 1.5,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        13
      ]
    }
  ],
  "cards_in_layout_amount": 43,
  "cards_in_deck_amount": 26,
  "layers_in_level": 5,
  "open_cards": 8,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 2.0,
    "star2": 71.4,
    "star3": 26.3
  }
}