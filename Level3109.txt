{
  "layout_cards": [
    {
      "id": 12,
      "x": 2.269,
      "y": -0.202,
      "angle": 20.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 13,
      "x": -2.121,
      "y": 0.699,
      "angle": 20.0,
      "layer": 5,
      "closed_by": [
        16
      ]
    },
    {
      "id": 14,
      "x": 2.121,
      "y": -0.254,
      "angle": 20.0,
      "layer": 5,
      "closed_by": [
        12
      ]
    },
    {
      "id": 15,
      "x": -1.797,
      "y": 0.818,
      "angle": 20.0,
      "layer": 4,
      "closed_by": [
        13
      ]
    },
    {
      "id": 16,
      "x": -2.279,
      "y": 0.643,
      "angle": 20.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 18,
      "x": 1.792,
      "y": -0.375,
      "angle": 20.0,
      "layer": 4,
      "closed_by": [
        14
      ]
    },
    {
      "id": 19,
      "x": -2.046,
      "angle": 20.0,
      "layer": 1,
      "closed_by": [
        37
      ]
    },
    {
      "id": 20,
      "x": 2.035,
      "y": 0.437,
      "angle": 20.0,
      "layer": 1,
      "closed_by": [
        21
      ]
    },
    {
      "id": 21,
      "x": 1.804,
      "y": 0.354,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 22,
      "x": -1.562,
      "y": 0.179,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        15,
        46
      ]
    },
    {
      "id": 23,
      "x": 1.559,
      "y": 0.263,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        18,
        43
      ]
    },
    {
      "id": 24,
      "x": -1.825,
      "y": -0.179,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 1.827,
      "y": 0.62,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -1.503,
      "y": -0.177,
      "layer": 5,
      "closed_by": [
        24
      ]
    },
    {
      "id": 27,
      "x": 1.508,
      "y": 0.619,
      "layer": 5,
      "closed_by": [
        25
      ]
    },
    {
      "id": 28,
      "x": 0.948,
      "y": -0.259,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -0.62,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        36
      ]
    },
    {
      "id": 30,
      "x": 0.625,
      "y": -0.259,
      "layer": 5,
      "closed_by": [
        28
      ]
    },
    {
      "id": 31,
      "x": -0.865,
      "y": -0.259,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 0.87,
      "y": 0.699,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -0.462,
      "y": -0.261,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 34,
      "x": 0.465,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        32
      ]
    },
    {
      "id": 35,
      "x": 0.307,
      "y": -0.259,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 36,
      "x": -1.024,
      "y": 0.699,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 37,
      "x": -1.814,
      "y": 0.086,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        22,
        26
      ]
    },
    {
      "id": 38,
      "x": -0.303,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 39,
      "x": 0.143,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        34,
        38
      ]
    },
    {
      "id": 40,
      "x": -0.143,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        33,
        35
      ]
    },
    {
      "id": 41,
      "x": 0.093,
      "y": -0.256,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 42,
      "x": -0.09,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 43,
      "x": 1.184,
      "y": 0.619,
      "layer": 4,
      "closed_by": [
        27,
        32
      ]
    },
    {
      "id": 44,
      "x": 0.845,
      "y": 0.469,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        30,
        34,
        43
      ]
    },
    {
      "id": 45,
      "x": 0.333,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        41,
        42,
        50
      ]
    },
    {
      "id": 46,
      "x": -1.184,
      "y": -0.172,
      "layer": 4,
      "closed_by": [
        26,
        31
      ]
    },
    {
      "id": 47,
      "x": -0.845,
      "y": -0.052,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        29,
        33,
        46
      ]
    },
    {
      "id": 48,
      "x": -0.606,
      "y": 0.032,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        38,
        40,
        47
      ]
    },
    {
      "id": 49,
      "x": -0.331,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        41,
        42,
        48
      ]
    },
    {
      "id": 50,
      "x": 0.597,
      "y": 0.379,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        35,
        39,
        44
      ]
    }
  ],
  "cards_in_layout_amount": 38,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 21.3,
    "star2": 71.4,
    "star3": 6.8
  }
}