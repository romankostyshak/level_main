{
  "layout_cards": [
    {
      "id": 3,
      "x": -1.904,
      "y": -0.416,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 4,
      "x": 0.001,
      "y": 1.019,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 5,
      "x": 0.307,
      "y": 0.939,
      "layer": 5,
      "closed_by": [
        4
      ]
    },
    {
      "id": 6,
      "x": 1.904,
      "y": -0.414,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 8,
      "x": -0.303,
      "y": 0.939,
      "layer": 5,
      "closed_by": [
        4
      ]
    },
    {
      "id": 9,
      "x": -0.412,
      "y": 0.782,
      "layer": 4,
      "closed_by": [
        8
      ]
    },
    {
      "id": 10,
      "x": -1.746,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 11,
      "x": 1.748,
      "y": 0.861,
      "layer": 4,
      "closed_by": [
        32
      ]
    },
    {
      "id": 12,
      "x": 1.906,
      "y": 0.479,
      "layer": 3,
      "closed_by": [
        11
      ]
    },
    {
      "id": 13,
      "x": -1.661,
      "y": -0.414,
      "layer": 4,
      "closed_by": [
        3
      ]
    },
    {
      "id": 14,
      "x": 1.661,
      "y": -0.416,
      "layer": 4,
      "closed_by": [
        6
      ]
    },
    {
      "id": 15,
      "x": -0.001,
      "y": -0.337,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 16,
      "x": -1.904,
      "y": 0.86,
      "layer": 2,
      "closed_by": [
        17
      ]
    },
    {
      "id": 17,
      "x": -1.904,
      "y": 0.474,
      "layer": 3,
      "closed_by": [
        10
      ]
    },
    {
      "id": 18,
      "x": 0.333,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        15
      ]
    },
    {
      "id": 19,
      "x": -0.333,
      "y": -0.34,
      "layer": 4,
      "closed_by": [
        15
      ]
    },
    {
      "id": 20,
      "x": 0.573,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 21,
      "x": 0.813,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 22,
      "x": -0.81,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 23,
      "x": -0.972,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        22,
        42
      ]
    },
    {
      "id": 24,
      "x": 0.972,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        21,
        30
      ]
    },
    {
      "id": 25,
      "x": 1.906,
      "y": 0.861,
      "layer": 2,
      "closed_by": [
        12
      ]
    },
    {
      "id": 26,
      "x": 1.904,
      "y": 0.544,
      "layer": 1,
      "closed_by": [
        25,
        28
      ]
    },
    {
      "id": 27,
      "x": 1.375,
      "y": -0.335,
      "layer": 3,
      "closed_by": [
        14
      ]
    },
    {
      "id": 28,
      "x": 1.906,
      "y": 0.05,
      "layer": 2,
      "closed_by": [
        12,
        36
      ]
    },
    {
      "id": 29,
      "x": 1.587,
      "y": -0.335,
      "layer": 1,
      "closed_by": [
        28,
        30
      ]
    },
    {
      "id": 30,
      "x": 1.373,
      "y": 0.064,
      "layer": 2,
      "closed_by": [
        27,
        31
      ]
    },
    {
      "id": 31,
      "x": 1.373,
      "y": 0.479,
      "layer": 3,
      "closed_by": [
        11,
        33
      ]
    },
    {
      "id": 32,
      "x": 1.373,
      "y": 0.861,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 0.972,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        32
      ]
    },
    {
      "id": 34,
      "x": 0.412,
      "y": 0.781,
      "layer": 4,
      "closed_by": [
        5
      ]
    },
    {
      "id": 35,
      "x": -1.904,
      "y": 0.544,
      "layer": 1,
      "closed_by": [
        16,
        49
      ]
    },
    {
      "id": 36,
      "x": 1.904,
      "y": -0.333,
      "layer": 3,
      "closed_by": [
        14
      ]
    },
    {
      "id": 37,
      "x": -0.573,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        19
      ]
    },
    {
      "id": 38,
      "x": -1.371,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        13
      ]
    },
    {
      "id": 40,
      "x": -0.66,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        9,
        44
      ]
    },
    {
      "id": 41,
      "x": -1.371,
      "y": 0.859,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -1.371,
      "y": 0.064,
      "layer": 2,
      "closed_by": [
        38,
        43
      ]
    },
    {
      "id": 43,
      "x": -1.371,
      "y": 0.476,
      "layer": 3,
      "closed_by": [
        10,
        44
      ]
    },
    {
      "id": 44,
      "x": -0.972,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 45,
      "x": 0.331,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 46,
      "x": -0.001,
      "y": 0.62,
      "layer": 1,
      "closed_by": [
        34,
        45,
        47
      ]
    },
    {
      "id": 47,
      "x": -0.333,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 48,
      "x": 0.652,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        33,
        34
      ]
    },
    {
      "id": 49,
      "x": -1.904,
      "y": 0.054,
      "layer": 2,
      "closed_by": [
        17,
        51
      ]
    },
    {
      "id": 50,
      "x": -1.582,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        42,
        49
      ]
    },
    {
      "id": 51,
      "x": -1.904,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        13
      ]
    }
  ],
  "cards_in_layout_amount": 47,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 1.4,
    "star2": 35.4,
    "star3": 62.3
  }
}