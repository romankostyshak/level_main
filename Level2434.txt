{
  "layout_cards": [
    {
      "id": 13,
      "x": -0.879,
      "y": 0.381,
      "layer": 1,
      "closed_by": [
        19,
        21
      ]
    },
    {
      "id": 14,
      "x": 0.878,
      "y": -0.493,
      "layer": 1,
      "closed_by": [
        16
      ]
    },
    {
      "id": 15,
      "x": 0.884,
      "y": 0.381,
      "layer": 1,
      "closed_by": [
        16,
        22
      ]
    },
    {
      "id": 16,
      "x": 0.814,
      "y": -0.097,
      "layer": 2,
      "closed_by": [
        17
      ]
    },
    {
      "id": 17,
      "x": 0.949,
      "y": -0.097,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 18,
      "x": -0.875,
      "y": -0.493,
      "layer": 1,
      "closed_by": [
        19
      ]
    },
    {
      "id": 19,
      "x": -0.813,
      "y": -0.1,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 20,
      "x": -0.943,
      "y": -0.097,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 21,
      "x": -0.813,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 22,
      "x": 0.814,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 23,
      "x": 0.949,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 24,
      "x": -0.939,
      "y": 0.781,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 25,
      "x": 1.026,
      "y": -0.1,
      "layer": 4,
      "closed_by": [
        32
      ]
    },
    {
      "id": 26,
      "x": -1.559,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 27,
      "x": -1.427,
      "y": 0.3,
      "layer": 2,
      "closed_by": [
        20,
        24,
        38,
        39
      ]
    },
    {
      "id": 28,
      "x": -1.021,
      "y": -0.1,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 29,
      "x": 1.026,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 30,
      "x": -1.023,
      "y": 0.782,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 31,
      "x": 1.108,
      "y": 0.777,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 1.108,
      "y": -0.101,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -1.103,
      "y": 0.777,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -1.1,
      "y": -0.1,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 1.424,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        17,
        23,
        36,
        37
      ]
    },
    {
      "id": 36,
      "x": 1.746,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 37,
      "x": 1.746,
      "y": -0.1,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 38,
      "x": -1.746,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 39,
      "x": -1.746,
      "y": -0.093,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 40,
      "x": 1.564,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 41,
      "x": 1.557,
      "y": -0.097,
      "layer": 4,
      "closed_by": [
        32
      ]
    },
    {
      "id": 42,
      "x": -1.552,
      "y": -0.093,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 43,
      "x": -0.259,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 44,
      "x": 0.259,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 45,
      "x": -0.259,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 46,
      "y": 0.298,
      "layer": 5,
      "closed_by": [
        51
      ]
    },
    {
      "id": 47,
      "x": 0.259,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        49
      ]
    },
    {
      "id": 48,
      "x": -0.259,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 49,
      "x": 0.259,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        46
      ]
    },
    {
      "id": 51,
      "y": 1.019,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 39,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 5.0,
    "star2": 50.8,
    "star3": 43.3
  }
}