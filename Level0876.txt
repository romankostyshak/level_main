{
  "layout_cards": [
    {
      "id": 51,
      "x": -1.825,
      "y": -0.108,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": -1.835,
      "y": 0.032,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        49
      ]
    },
    {
      "id": 49,
      "x": -1.835,
      "y": 0.439,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 48,
      "x": 1.83,
      "y": -0.256,
      "angle": 349.997,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "x": 1.824,
      "y": -0.123,
      "angle": 340.0,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 46,
      "x": 1.832,
      "y": 0.017,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 45,
      "x": 1.832,
      "y": 0.432,
      "angle": 30.0,
      "layer": 4,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": 1.815,
      "y": 0.578,
      "angle": 20.0,
      "layer": 5,
      "closed_by": [
        43
      ]
    },
    {
      "id": 43,
      "x": 1.827,
      "y": 0.717,
      "angle": 9.998,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 42,
      "x": 0.625,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        28
      ]
    },
    {
      "id": 41,
      "x": -0.625,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 40,
      "x": 0.33,
      "y": -0.418,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 39,
      "x": 0.328,
      "y": 0.465,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -0.328,
      "y": 0.458,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 37,
      "x": -0.33,
      "y": -0.418,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 36,
      "y": 0.063,
      "layer": 3,
      "closed_by": [
        40,
        39,
        38,
        37
      ]
    },
    {
      "id": 35,
      "x": -0.259,
      "y": -0.349,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 34,
      "y": -0.497,
      "layer": 1,
      "closed_by": [
        35,
        25
      ]
    },
    {
      "id": 33,
      "x": -1.824,
      "y": 0.578,
      "angle": 340.0,
      "layer": 5,
      "closed_by": [
        32
      ]
    },
    {
      "id": 32,
      "x": -1.838,
      "y": 0.717,
      "angle": 349.997,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -0.625,
      "y": -0.499,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 30,
      "x": -1.83,
      "y": -0.254,
      "angle": 9.998,
      "layer": 1,
      "closed_by": [
        51
      ]
    },
    {
      "id": 29,
      "x": -0.384,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        27
      ]
    },
    {
      "id": 28,
      "x": 0.388,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        27
      ]
    },
    {
      "id": 27,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        39,
        38
      ]
    },
    {
      "id": 26,
      "x": 0.625,
      "y": -0.497,
      "layer": 1,
      "closed_by": [
        25
      ]
    },
    {
      "id": 25,
      "x": 0.268,
      "y": -0.344,
      "layer": 2,
      "closed_by": [
        36
      ]
    }
  ],
  "cards_in_layout_amount": 27,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 66.1,
    "star2": 18.5,
    "star3": 15.0
  }
}