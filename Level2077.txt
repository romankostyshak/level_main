{
  "layout_cards": [
    {
      "id": 18,
      "x": 1.455,
      "y": 1.016,
      "layer": 1,
      "closed_by": [
        28
      ]
    },
    {
      "id": 19,
      "x": 1.447,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 21,
      "x": 1.452,
      "y": -0.172,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 22,
      "x": 1.291,
      "y": -0.256,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 23,
      "x": 1.603,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 24,
      "x": 1.45,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 26,
      "x": 1.593,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        19
      ]
    },
    {
      "id": 27,
      "x": 1.45,
      "y": 1.019,
      "layer": 5,
      "closed_by": [
        36
      ]
    },
    {
      "id": 28,
      "x": 1.296,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        26
      ]
    },
    {
      "id": 29,
      "x": 1.452,
      "y": -0.178,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 30,
      "x": 1.294,
      "y": -0.259,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 0.093,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        34
      ]
    },
    {
      "id": 34,
      "x": 0.093,
      "y": 0.865,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 35,
      "x": 0.093,
      "y": 0.703,
      "layer": 3,
      "closed_by": [
        41,
        48
      ]
    },
    {
      "id": 36,
      "x": 1.294,
      "y": 0.939,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 37,
      "x": -1.261,
      "y": -0.256,
      "layer": 3,
      "closed_by": [
        40,
        49
      ]
    },
    {
      "id": 38,
      "x": -1.261,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 39,
      "x": -1.261,
      "y": -0.573,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 40,
      "x": -0.873,
      "y": -0.256,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 41,
      "x": -0.289,
      "y": 0.702,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 42,
      "x": -0.472,
      "y": -0.256,
      "layer": 5,
      "closed_by": [
        44
      ]
    },
    {
      "id": 43,
      "x": -0.688,
      "y": 0.702,
      "layer": 5,
      "closed_by": [
        45
      ]
    },
    {
      "id": 44,
      "x": -0.068,
      "y": -0.256,
      "layer": 6,
      "closed_by": [
        46
      ]
    },
    {
      "id": 45,
      "x": -1.09,
      "y": 0.699,
      "layer": 6,
      "closed_by": [
        47
      ]
    },
    {
      "id": 46,
      "x": 0.323,
      "y": -0.256,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 47,
      "x": -1.49,
      "y": 0.699,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 48,
      "x": 0.324,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        50
      ]
    },
    {
      "id": 49,
      "x": -1.491,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 50,
      "x": 0.324,
      "y": 0.703,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -1.49,
      "y": -0.256,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 30,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 42.5,
    "star2": 48.1,
    "star3": 8.9
  }
}