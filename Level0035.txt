{
  "layout_cards": [
    {
      "id": 51,
      "x": -2.539,
      "y": -0.425,
      "angle": 354.997,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": -2.378,
      "y": -0.277,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        49
      ]
    },
    {
      "id": 49,
      "x": -2.14,
      "y": -0.18,
      "angle": 335.0,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 48,
      "x": -0.643,
      "y": 0.574,
      "angle": 320.0,
      "layer": 2,
      "closed_by": [
        36,
        35
      ]
    },
    {
      "id": 47,
      "x": 1.225,
      "y": 0.122,
      "angle": 55.0,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 46,
      "x": 2.14,
      "y": -0.186,
      "angle": 30.0,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 45,
      "x": 2.388,
      "y": -0.277,
      "angle": 9.998,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 44,
      "x": 2.673,
      "y": -0.577,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 43,
      "x": 2.562,
      "y": -0.425,
      "angle": 5.0,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 42,
      "x": -1.098,
      "y": -0.569,
      "angle": 60.0,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 41,
      "x": -2.641,
      "y": -0.577,
      "layer": 1,
      "closed_by": [
        51
      ]
    },
    {
      "id": 40,
      "x": 0.971,
      "y": 0.307,
      "angle": 50.0,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 39,
      "x": 0.66,
      "y": 0.578,
      "angle": 40.0,
      "layer": 2,
      "closed_by": [
        40,
        34
      ]
    },
    {
      "id": 38,
      "x": -0.518,
      "y": -0.527,
      "angle": 60.0,
      "layer": 2,
      "closed_by": [
        42,
        33
      ]
    },
    {
      "id": 37,
      "x": -1.207,
      "y": 0.115,
      "angle": 305.0,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 36,
      "x": -0.952,
      "y": 0.303,
      "angle": 310.0,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 35,
      "x": -0.411,
      "y": 0.962,
      "angle": 325.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 0.418,
      "y": 0.967,
      "angle": 35.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 33,
      "y": -0.495,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 0.507,
      "y": -0.532,
      "angle": 299.997,
      "layer": 2,
      "closed_by": [
        33,
        31
      ]
    },
    {
      "id": 31,
      "x": 1.088,
      "y": -0.582,
      "angle": 300.0,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 30,
      "x": 1.552,
      "y": -0.231,
      "angle": 325.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -1.554,
      "y": -0.216,
      "angle": 35.0,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 23,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 56.7,
    "star2": 5.8,
    "star3": 36.9
  }
}