{
  "layout_cards": [
    {
      "id": 19,
      "x": -1.371,
      "y": -0.573,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 20,
      "x": -2.319,
      "y": 0.698,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -2.463,
      "y": 1.021,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 23,
      "x": -2.463,
      "y": -0.497,
      "layer": 1,
      "closed_by": [
        24
      ]
    },
    {
      "id": 24,
      "x": -2.463,
      "y": -0.256,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 25,
      "x": -2.171,
      "y": -0.256,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 26,
      "x": -2.171,
      "y": -0.497,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 27,
      "x": -2.463,
      "y": 0.781,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 28,
      "x": -2.171,
      "y": 1.021,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 29,
      "x": -2.174,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        20
      ],
      "card_type": 10
    },
    {
      "id": 30,
      "x": -1.503,
      "y": 0.781,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 31,
      "x": -1.503,
      "y": 1.021,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 32,
      "x": -1.503,
      "y": -0.497,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 33,
      "x": -1.503,
      "y": -0.256,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 34,
      "x": -1.213,
      "y": 1.021,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 35,
      "x": 0.648,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 36,
      "x": -1.212,
      "y": -0.256,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 37,
      "x": -1.215,
      "y": 0.781,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -1.213,
      "y": -0.5,
      "layer": 4,
      "closed_by": [
        19
      ],
      "card_type": 10,
      "card_color": 1
    },
    {
      "id": 39,
      "x": -0.625,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 40,
      "x": -0.625,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 41,
      "x": -0.624,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        42
      ],
      "card_type": 8
    },
    {
      "id": 42,
      "x": -0.625,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 43,
      "x": -0.625,
      "y": -0.578,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 44,
      "x": 0.786,
      "y": 0.059,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 45,
      "x": 0.675,
      "y": 0.492,
      "angle": 315.0,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 46,
      "x": 1.452,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 47,
      "x": 1.59,
      "y": -0.177,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 48,
      "x": 1.557,
      "y": 0.284,
      "angle": 315.0,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 49,
      "x": 2.331,
      "y": 0.462,
      "layer": 1,
      "closed_by": [
        51
      ]
    },
    {
      "id": 50,
      "x": 2.467,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        51
      ]
    },
    {
      "id": 51,
      "x": 2.434,
      "y": 0.046,
      "angle": 315.0,
      "layer": 2,
      "closed_by": []
    }
  ],
  "level_mechanic": 2,
  "free_cards_from_stickers": [
    19,
    20,
    22,
    23,
    24,
    25,
    26,
    27,
    28,
    29,
    30,
    31,
    32,
    33,
    34,
    36,
    37,
    38,
    39,
    40,
    41,
    42,
    43
  ],
  "cards_in_layout_amount": 32,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 29.7,
    "star2": 65.8,
    "star3": 3.7
  }
}