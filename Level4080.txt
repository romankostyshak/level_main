{
  "layout_cards": [
    {
      "id": 4,
      "x": -1.6,
      "y": 0.699,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 5,
      "x": -1.6,
      "y": -0.119,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 8,
      "y": 0.699,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 9,
      "x": 1.598,
      "y": 0.699,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 10,
      "x": 1.598,
      "y": -0.119,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 11,
      "y": -0.119,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 13,
      "x": -0.418,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 14,
      "x": -0.418,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        13,
        18
      ]
    },
    {
      "id": 15,
      "x": 0.418,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        16,
        17
      ]
    },
    {
      "id": 16,
      "x": 0.418,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 17,
      "x": 0.418,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 18,
      "x": -0.418,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 19,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        20,
        21
      ]
    },
    {
      "id": 20,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 21,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 22,
      "x": 0.279,
      "y": -0.179,
      "angle": 30.0,
      "layer": 5,
      "closed_by": [
        23
      ]
    },
    {
      "id": 23,
      "x": -0.279,
      "y": -0.179,
      "angle": 330.0,
      "layer": 6,
      "closed_by": [
        11
      ]
    },
    {
      "id": 24,
      "x": -0.279,
      "y": 0.777,
      "angle": 30.0,
      "layer": 5,
      "closed_by": [
        25
      ]
    },
    {
      "id": 25,
      "x": 0.279,
      "y": 0.777,
      "angle": 330.0,
      "layer": 6,
      "closed_by": [
        8
      ]
    },
    {
      "id": 26,
      "x": -1.6,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        41,
        42,
        43,
        44
      ]
    },
    {
      "id": 27,
      "x": 1.179,
      "y": 0.298,
      "layer": 4,
      "closed_by": [
        37,
        38
      ]
    },
    {
      "id": 28,
      "x": 1.598,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        30
      ]
    },
    {
      "id": 29,
      "x": 1.598,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        30
      ]
    },
    {
      "id": 30,
      "x": 1.598,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        31,
        32,
        33,
        34
      ]
    },
    {
      "id": 31,
      "x": 2.019,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 32,
      "x": 1.179,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 33,
      "x": 2.019,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 34,
      "x": 1.179,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 35,
      "x": 2.019,
      "y": 0.298,
      "layer": 4,
      "closed_by": [
        36,
        39
      ]
    },
    {
      "id": 36,
      "x": 1.878,
      "y": -0.179,
      "angle": 30.0,
      "layer": 5,
      "closed_by": [
        38
      ]
    },
    {
      "id": 37,
      "x": 1.319,
      "y": 0.777,
      "angle": 30.0,
      "layer": 5,
      "closed_by": [
        39
      ]
    },
    {
      "id": 38,
      "x": 1.319,
      "y": -0.179,
      "angle": 330.0,
      "layer": 6,
      "closed_by": [
        10
      ]
    },
    {
      "id": 39,
      "x": 1.878,
      "y": 0.777,
      "angle": 330.0,
      "layer": 6,
      "closed_by": [
        9
      ]
    },
    {
      "id": 40,
      "x": -1.6,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        26
      ]
    },
    {
      "id": 41,
      "x": -1.179,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 42,
      "x": -2.019,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 43,
      "x": -1.179,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 44,
      "x": -2.019,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 45,
      "x": -1.179,
      "y": 0.298,
      "layer": 4,
      "closed_by": [
        49,
        51
      ]
    },
    {
      "id": 46,
      "x": -1.878,
      "y": -0.179,
      "angle": 330.0,
      "layer": 6,
      "closed_by": [
        5
      ]
    },
    {
      "id": 47,
      "x": -2.019,
      "y": 0.298,
      "layer": 4,
      "closed_by": [
        46,
        50
      ]
    },
    {
      "id": 48,
      "x": -1.6,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        26
      ]
    },
    {
      "id": 49,
      "x": -1.319,
      "y": -0.179,
      "angle": 30.0,
      "layer": 5,
      "closed_by": [
        46
      ]
    },
    {
      "id": 50,
      "x": -1.878,
      "y": 0.777,
      "angle": 30.0,
      "layer": 5,
      "closed_by": [
        51
      ]
    },
    {
      "id": 51,
      "x": -1.319,
      "y": 0.777,
      "angle": 330.0,
      "layer": 6,
      "closed_by": [
        4
      ]
    }
  ],
  "cards_in_layout_amount": 45,
  "cards_in_deck_amount": 32,
  "layers_in_level": 7,
  "open_cards": 6,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 28.7,
    "star2": 67.3,
    "star3": 3.5
  }
}