{
  "layout_cards": [
    {
      "id": 49,
      "x": 0.335,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 48,
      "x": 0.624,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "x": 0.944,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 46,
      "x": 1.264,
      "y": -0.577,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 45,
      "x": -0.335,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": -0.625,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": -0.944,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 43,
      "x": -1.266,
      "y": -0.578,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -0.335,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        40,
        38
      ]
    },
    {
      "id": 41,
      "x": 0.335,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        40,
        39
      ]
    },
    {
      "id": 40,
      "x": -0.004,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        35,
        34
      ]
    },
    {
      "id": 39,
      "x": 0.62,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 38,
      "x": -0.624,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 35,
      "x": -0.333,
      "y": 0.861,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 34,
      "x": 0.34,
      "y": 0.861,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 33,
      "x": -0.003,
      "y": 0.698,
      "layer": 4,
      "closed_by": [
        32,
        31
      ]
    },
    {
      "id": 32,
      "x": 0.337,
      "y": 0.545,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 31,
      "x": -0.331,
      "y": 0.544,
      "layer": 5,
      "closed_by": [
        29
      ]
    },
    {
      "id": 30,
      "x": 0.791,
      "y": 0.381,
      "layer": 6,
      "closed_by": [
        28
      ]
    },
    {
      "id": 29,
      "x": -0.782,
      "y": 0.377,
      "layer": 6,
      "closed_by": [
        36
      ]
    },
    {
      "id": 28,
      "x": 1.108,
      "y": 0.305,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -1.105,
      "y": 0.307,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 1.771,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 26,
      "x": 1.771,
      "y": 0.136,
      "layer": 1,
      "closed_by": [
        23
      ]
    },
    {
      "id": 25,
      "x": -1.773,
      "y": 0.143,
      "layer": 1,
      "closed_by": [
        37
      ]
    },
    {
      "id": 24,
      "x": -1.774,
      "y": 1.029,
      "layer": 1,
      "closed_by": [
        21
      ]
    },
    {
      "id": 23,
      "x": 1.911,
      "y": 0.136,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 22,
      "x": 1.906,
      "y": 1.021,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 21,
      "x": -1.906,
      "y": 1.026,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 37,
      "x": -1.899,
      "y": 0.143,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 20,
      "x": 2.009,
      "y": 0.587,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 19,
      "x": -2.009,
      "y": 0.587,
      "layer": 3,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 32,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 27.8,
    "star2": 62.5,
    "star3": 8.7
  }
}