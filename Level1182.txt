{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.414,
      "y": 0.944,
      "layer": 1,
      "closed_by": [
        47,
        38
      ]
    },
    {
      "id": 50,
      "x": -0.412,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        46,
        38
      ]
    },
    {
      "id": 49,
      "x": 0.412,
      "y": -0.497,
      "layer": 1,
      "closed_by": [
        45,
        38
      ]
    },
    {
      "id": 48,
      "x": -0.412,
      "y": -0.497,
      "layer": 1,
      "closed_by": [
        44,
        38
      ]
    },
    {
      "id": 47,
      "x": 0.465,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        42,
        35
      ]
    },
    {
      "id": 46,
      "x": -0.465,
      "y": 1.021,
      "layer": 2,
      "closed_by": [
        41,
        37
      ]
    },
    {
      "id": 45,
      "x": 0.467,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        43,
        34
      ]
    },
    {
      "id": 44,
      "x": -0.465,
      "y": -0.574,
      "layer": 2,
      "closed_by": [
        40,
        36
      ]
    },
    {
      "id": 43,
      "x": 0.893,
      "y": -0.261,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 42,
      "x": 0.888,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 41,
      "x": -0.865,
      "y": 0.702,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 40,
      "x": -0.865,
      "y": -0.261,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 38,
      "y": 0.224,
      "layer": 2,
      "closed_by": [
        37,
        36,
        35,
        34
      ]
    },
    {
      "id": 37,
      "x": -0.259,
      "y": 0.632,
      "layer": 3,
      "closed_by": [
        32,
        39
      ]
    },
    {
      "id": 36,
      "x": -0.256,
      "y": -0.185,
      "layer": 3,
      "closed_by": [
        33,
        39
      ]
    },
    {
      "id": 35,
      "x": 0.263,
      "y": 0.633,
      "layer": 3,
      "closed_by": [
        32,
        31
      ]
    },
    {
      "id": 34,
      "x": 0.263,
      "y": -0.179,
      "layer": 3,
      "closed_by": [
        33,
        31
      ]
    },
    {
      "id": 33,
      "x": 0.001,
      "y": -0.574,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -0.001,
      "y": 1.019,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 0.546,
      "y": 0.224,
      "layer": 4,
      "closed_by": [
        30,
        29
      ]
    },
    {
      "id": 30,
      "x": 0.787,
      "y": 0.938,
      "layer": 5,
      "closed_by": [
        23
      ]
    },
    {
      "id": 29,
      "x": 0.782,
      "y": -0.493,
      "layer": 5,
      "closed_by": [
        24
      ]
    },
    {
      "id": 28,
      "x": -0.782,
      "y": 0.938,
      "layer": 5,
      "closed_by": [
        26
      ]
    },
    {
      "id": 27,
      "x": -0.782,
      "y": -0.493,
      "layer": 5,
      "closed_by": [
        25
      ]
    },
    {
      "id": 26,
      "x": -0.865,
      "y": 1.019,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 25,
      "x": -0.865,
      "y": -0.574,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 0.896,
      "y": -0.574,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -0.544,
      "y": 0.23,
      "layer": 4,
      "closed_by": [
        28,
        27
      ]
    },
    {
      "id": 23,
      "x": 0.892,
      "y": 1.019,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 29,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 57.6,
    "star2": 10.2,
    "star3": 31.5
  }
}