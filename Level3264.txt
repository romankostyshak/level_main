{
  "layout_cards": [
    {
      "x": 2.252,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        1
      ]
    },
    {
      "id": 1,
      "x": 2.496,
      "y": -0.497,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 2,
      "x": 2.253,
      "y": -0.261,
      "layer": 1,
      "closed_by": [
        3
      ]
    },
    {
      "id": 3,
      "x": 2.493,
      "y": -0.34,
      "layer": 2,
      "closed_by": [
        0
      ]
    },
    {
      "id": 14,
      "x": -2.253,
      "y": -0.497,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 15,
      "x": -2.493,
      "y": -0.421,
      "layer": 3,
      "closed_by": [
        14
      ]
    },
    {
      "id": 18,
      "x": -2.252,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        15
      ]
    },
    {
      "id": 19,
      "x": -2.493,
      "y": -0.256,
      "layer": 1,
      "closed_by": [
        18
      ]
    },
    {
      "id": 21,
      "x": 0.453,
      "y": 0.976,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        23,
        26
      ]
    },
    {
      "id": 22,
      "y": 0.238,
      "layer": 3,
      "closed_by": [
        30,
        31,
        32,
        33
      ]
    },
    {
      "id": 23,
      "x": 0.536,
      "y": 0.28,
      "layer": 3,
      "closed_by": [
        30,
        32
      ]
    },
    {
      "id": 24,
      "x": -0.536,
      "y": 0.279,
      "layer": 3,
      "closed_by": [
        31,
        33
      ]
    },
    {
      "id": 25,
      "x": -0.002,
      "y": -0.577,
      "layer": 3,
      "closed_by": [
        31,
        32
      ]
    },
    {
      "id": 26,
      "y": 1.052,
      "layer": 3,
      "closed_by": [
        30,
        33
      ]
    },
    {
      "id": 27,
      "x": -0.46,
      "y": 0.972,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        24,
        26
      ]
    },
    {
      "id": 28,
      "x": -0.486,
      "y": -0.472,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        24,
        25
      ]
    },
    {
      "id": 29,
      "x": 0.5,
      "y": -0.472,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        23,
        25
      ]
    },
    {
      "id": 30,
      "x": 0.259,
      "y": 0.688,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -0.254,
      "y": -0.123,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 0.268,
      "y": -0.123,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -0.256,
      "y": 0.688,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 0.592,
      "y": -0.532,
      "angle": 60.0,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 35,
      "x": -0.587,
      "y": -0.531,
      "angle": 300.0,
      "layer": 1,
      "closed_by": [
        28
      ]
    },
    {
      "id": 36,
      "x": -0.559,
      "y": 1.031,
      "angle": 60.0,
      "layer": 1,
      "closed_by": [
        27
      ]
    },
    {
      "id": 37,
      "x": 0.555,
      "y": 1.024,
      "angle": 300.0,
      "layer": 1,
      "closed_by": [
        21
      ]
    },
    {
      "id": 38,
      "x": 0.785,
      "y": 0.275,
      "layer": 1,
      "closed_by": [
        21,
        29,
        42
      ]
    },
    {
      "id": 39,
      "x": -0.782,
      "y": 0.275,
      "layer": 1,
      "closed_by": [
        27,
        28,
        43
      ]
    },
    {
      "id": 40,
      "x": 1.963,
      "y": 0.943,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 1.373,
      "y": 0.462,
      "layer": 1,
      "closed_by": [
        42,
        45
      ]
    },
    {
      "id": 42,
      "x": 0.989,
      "y": 0.462,
      "angle": 300.0,
      "layer": 2,
      "closed_by": [
        23,
        49
      ]
    },
    {
      "id": 43,
      "x": -0.995,
      "y": 0.474,
      "angle": 60.0,
      "layer": 2,
      "closed_by": [
        24,
        50
      ]
    },
    {
      "id": 44,
      "x": -1.375,
      "y": 0.462,
      "layer": 1,
      "closed_by": [
        43,
        46
      ]
    },
    {
      "id": 45,
      "x": 1.697,
      "y": 0.874,
      "angle": 300.0,
      "layer": 2,
      "closed_by": [
        40,
        49
      ]
    },
    {
      "id": 46,
      "x": -1.71,
      "y": 0.884,
      "angle": 60.0,
      "layer": 2,
      "closed_by": [
        50,
        51
      ]
    },
    {
      "id": 47,
      "x": 1.96,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 48,
      "x": -1.963,
      "y": 0.781,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 49,
      "x": 1.375,
      "y": 0.619,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 50,
      "x": -1.373,
      "y": 0.62,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -1.962,
      "y": 0.943,
      "layer": 3,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 39,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 3.5,
    "star2": 55.1,
    "star3": 41.1
  }
}