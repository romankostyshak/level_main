{
  "layout_cards": [
    {
      "id": 9,
      "x": 2.309,
      "y": 0.298,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 10,
      "x": 2.232,
      "y": 0.298,
      "layer": 5,
      "closed_by": [
        9
      ]
    },
    {
      "id": 11,
      "x": 2.154,
      "y": 0.3,
      "layer": 4,
      "closed_by": [
        10
      ]
    },
    {
      "id": 12,
      "x": 1.998,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        16
      ]
    },
    {
      "id": 13,
      "x": 1.916,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        12
      ]
    },
    {
      "id": 14,
      "x": 0.425,
      "y": 0.303,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 15,
      "x": 0.163,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        14
      ]
    },
    {
      "id": 16,
      "x": 2.071,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        11
      ]
    },
    {
      "id": 18,
      "x": 1.353,
      "y": 0.3,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 19,
      "x": -0.962,
      "y": -0.016,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 20,
      "x": -0.397,
      "y": 0.293,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 21,
      "x": -0.14,
      "y": -0.111,
      "layer": 5,
      "closed_by": [
        20
      ]
    },
    {
      "id": 22,
      "x": 0.23,
      "y": -0.112,
      "layer": 4,
      "closed_by": [
        14,
        21
      ]
    },
    {
      "id": 23,
      "x": -0.093,
      "y": -0.108,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 24,
      "x": 0.753,
      "y": -0.112,
      "layer": 5,
      "closed_by": [
        14
      ]
    },
    {
      "id": 25,
      "x": 0.856,
      "y": -0.112,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 26,
      "x": 1.274,
      "y": -0.112,
      "layer": 5,
      "closed_by": [
        18
      ]
    },
    {
      "id": 27,
      "x": 0.423,
      "y": -0.111,
      "layer": 1,
      "closed_by": [
        25,
        47
      ]
    },
    {
      "id": 28,
      "x": -1.523,
      "y": -0.017,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 29,
      "x": -2.453,
      "y": -0.017,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 30,
      "x": -1.682,
      "y": -0.017,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 31,
      "x": -2.059,
      "y": -0.097,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -0.097,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        15,
        20
      ]
    },
    {
      "id": 33,
      "x": -1.524,
      "y": 0.702,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -2.618,
      "y": 0.703,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -2.319,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        31,
        34
      ]
    },
    {
      "id": 36,
      "x": -2.618,
      "y": -0.016,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 37,
      "x": -0.651,
      "y": -0.108,
      "layer": 5,
      "closed_by": [
        19,
        20
      ]
    },
    {
      "id": 38,
      "x": -1.804,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        31,
        33
      ]
    },
    {
      "id": 39,
      "x": -0.962,
      "y": 0.698,
      "layer": 5,
      "closed_by": [
        19
      ]
    },
    {
      "id": 40,
      "x": 1.274,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        18
      ]
    },
    {
      "id": 41,
      "x": -0.722,
      "y": 0.458,
      "layer": 4,
      "closed_by": [
        37,
        39
      ]
    },
    {
      "id": 42,
      "x": 1.008,
      "y": 0.462,
      "layer": 4,
      "closed_by": [
        24,
        26,
        40
      ]
    },
    {
      "id": 43,
      "x": 0.85,
      "y": 0.698,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 44,
      "x": -0.56,
      "y": 0.698,
      "layer": 3,
      "closed_by": [
        32,
        41
      ]
    },
    {
      "id": 45,
      "x": 0.423,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        15
      ]
    },
    {
      "id": 46,
      "x": 0.609,
      "y": 0.458,
      "layer": 3,
      "closed_by": [
        22,
        42,
        45
      ]
    },
    {
      "id": 47,
      "x": 0.05,
      "y": 0.462,
      "layer": 3,
      "closed_by": [
        22,
        32,
        45
      ]
    },
    {
      "id": 48,
      "x": 0.202,
      "y": 0.697,
      "layer": 2,
      "closed_by": [
        46,
        47
      ]
    },
    {
      "id": 49,
      "x": -0.17,
      "y": 0.694,
      "layer": 1,
      "closed_by": [
        48,
        50
      ]
    },
    {
      "id": 50,
      "x": -0.319,
      "y": 0.458,
      "layer": 2,
      "closed_by": [
        21,
        44,
        47
      ]
    }
  ],
  "cards_in_layout_amount": 41,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 4.7,
    "star2": 70.6,
    "star3": 23.9
  }
}