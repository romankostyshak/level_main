{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.368,
      "y": -0.532,
      "angle": 340.0,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 50,
      "x": -0.36,
      "y": -0.531,
      "angle": 20.0,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 49,
      "x": 0.441,
      "y": -0.458,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 48,
      "x": -0.435,
      "y": -0.446,
      "angle": 9.998,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 47,
      "x": 0.508,
      "y": -0.428,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 46,
      "x": -0.5,
      "y": -0.421,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 45,
      "x": 0.564,
      "y": -0.384,
      "angle": 9.998,
      "layer": 4,
      "closed_by": [
        16
      ]
    },
    {
      "id": 44,
      "x": -0.558,
      "y": -0.377,
      "angle": 349.997,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 43,
      "x": -0.601,
      "y": -0.321,
      "angle": 340.0,
      "layer": 5,
      "closed_by": [
        14
      ]
    },
    {
      "id": 42,
      "x": -0.354,
      "y": 1.006,
      "angle": 5.0,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 41,
      "x": 0.358,
      "y": 1.003,
      "angle": 354.997,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 40,
      "x": -0.479,
      "y": 0.944,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 39,
      "x": 0.479,
      "y": 0.943,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 38,
      "x": 0.652,
      "y": 0.878,
      "angle": 340.0,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 37,
      "x": -0.643,
      "y": 0.884,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 36,
      "x": 0.832,
      "y": 0.845,
      "angle": 349.997,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 35,
      "x": -0.824,
      "y": 0.851,
      "angle": 9.998,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 34,
      "x": 1.029,
      "y": 0.842,
      "layer": 5,
      "closed_by": [
        32
      ]
    },
    {
      "id": 33,
      "x": -1.026,
      "y": 0.846,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 32,
      "x": 1.45,
      "y": 0.143,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -1.449,
      "y": 0.142,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 1.376,
      "y": -0.418,
      "layer": 5,
      "closed_by": [
        32
      ]
    },
    {
      "id": 29,
      "x": -1.373,
      "y": -0.416,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 27,
      "x": -1.85,
      "y": 0.861,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 24,
      "x": 2.069,
      "y": 0.8,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 23,
      "x": -2.081,
      "y": 0.805,
      "angle": 30.0,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 22,
      "x": 2.085,
      "y": 0.722,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 21,
      "x": -2.092,
      "y": 0.731,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 20,
      "x": 2.101,
      "y": 0.647,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 19,
      "x": -2.108,
      "y": 0.651,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        21
      ]
    },
    {
      "id": 18,
      "x": 2.118,
      "y": 0.564,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        20
      ]
    },
    {
      "id": 17,
      "x": -2.121,
      "y": 0.564,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        19
      ]
    },
    {
      "id": 16,
      "x": 0.601,
      "y": -0.324,
      "angle": 20.0,
      "layer": 5,
      "closed_by": [
        15
      ]
    },
    {
      "id": 15,
      "x": 0.642,
      "y": -0.25,
      "angle": 30.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 14,
      "x": -0.638,
      "y": -0.261,
      "angle": 330.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 1.855,
      "y": 0.859,
      "layer": 5,
      "closed_by": [
        32
      ]
    }
  ],
  "cards_in_layout_amount": 36,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 7.5,
    "star2": 69.3,
    "star3": 22.6
  }
}