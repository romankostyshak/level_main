{
  "layout_cards": [
    {
      "id": 51,
      "x": -1.985,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 50,
      "x": -1.983,
      "y": 0.537,
      "layer": 3,
      "closed_by": [
        49
      ]
    },
    {
      "id": 49,
      "x": -1.618,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        48
      ]
    },
    {
      "id": 48,
      "x": -1.294,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        46
      ]
    },
    {
      "id": 47,
      "x": -1.618,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 46,
      "x": -0.893,
      "y": 0.787,
      "layer": 6,
      "closed_by": [
        45
      ]
    },
    {
      "id": 44,
      "x": -1.299,
      "y": -0.179,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 43,
      "x": -0.777,
      "y": -0.172,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -0.493,
      "y": -0.172,
      "layer": 6,
      "closed_by": [
        43
      ]
    },
    {
      "id": 40,
      "x": -0.064,
      "y": -0.416,
      "layer": 5,
      "closed_by": [
        42
      ]
    },
    {
      "id": 39,
      "x": 0.224,
      "y": -0.1,
      "layer": 4,
      "closed_by": [
        40,
        41
      ]
    },
    {
      "id": 38,
      "x": 0.703,
      "y": -0.416,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 37,
      "x": 1.133,
      "y": -0.256,
      "layer": 1,
      "closed_by": [
        38,
        36
      ]
    },
    {
      "id": 34,
      "x": 1.827,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 33,
      "x": 0.46,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        39,
        28,
        21
      ]
    },
    {
      "id": 32,
      "x": 0.799,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 31,
      "x": 1.026,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 30,
      "x": 0.216,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 29,
      "x": 0.537,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        30,
        28
      ]
    },
    {
      "id": 28,
      "x": 0.749,
      "y": 0.305,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 2.226,
      "y": 0.777,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 26,
      "x": 2.414,
      "y": 0.224,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 25,
      "x": 2.414,
      "y": -0.093,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 24,
      "x": 2.176,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 21,
      "x": 0.749,
      "y": -0.574,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 1.588,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 23,
      "x": 1.934,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        24
      ]
    },
    {
      "id": 36,
      "x": 1.271,
      "y": -0.1,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 41,
      "x": -0.067,
      "y": 0.46,
      "layer": 5,
      "closed_by": [
        42
      ]
    },
    {
      "id": 45,
      "x": -0.62,
      "y": 0.787,
      "layer": 7,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 30,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 44.0,
    "star2": 44.0,
    "star3": 11.1
  }
}