{
  "layout_cards": [
    {
      "id": 51,
      "x": -1.401,
      "y": 0.967,
      "angle": 344.997,
      "layer": 1,
      "closed_by": [
        26
      ]
    },
    {
      "id": 50,
      "x": -1.406,
      "y": -0.5,
      "angle": 344.997,
      "layer": 1,
      "closed_by": [
        26
      ]
    },
    {
      "id": 49,
      "x": -1.014,
      "y": 0.958,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": -1.128,
      "y": -0.513,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 47,
      "x": -1.802,
      "y": -0.512,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 46,
      "x": -0.994,
      "y": 0.231,
      "angle": 344.997,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 45,
      "x": 0.002,
      "y": 0.953,
      "angle": 344.997,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 44,
      "y": -0.509,
      "angle": 344.997,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 43,
      "x": 0.001,
      "y": 0.224,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        41,
        40
      ]
    },
    {
      "id": 42,
      "x": 0.404,
      "y": 0.958,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 41,
      "x": -0.256,
      "y": 0.953,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 40,
      "x": 0.27,
      "y": -0.509,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 39,
      "x": -0.393,
      "y": -0.508,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 38,
      "x": 0.407,
      "y": 0.224,
      "angle": 344.997,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 37,
      "x": -0.395,
      "y": 0.231,
      "angle": 344.997,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 1.396,
      "y": 0.948,
      "angle": 344.997,
      "layer": 1,
      "closed_by": [
        28
      ]
    },
    {
      "id": 35,
      "x": 1.401,
      "y": -0.517,
      "angle": 344.997,
      "layer": 1,
      "closed_by": [
        28
      ]
    },
    {
      "id": 34,
      "x": 1.807,
      "y": 0.957,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 33,
      "x": 1.128,
      "y": 0.958,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 32,
      "x": 1.001,
      "y": -0.508,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 31,
      "x": 1.674,
      "y": -0.508,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 30,
      "x": 1.812,
      "y": 0.216,
      "angle": 344.997,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 29,
      "x": 1.001,
      "y": 0.224,
      "angle": 344.997,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 1.401,
      "y": 0.224,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        33,
        31
      ]
    },
    {
      "id": 27,
      "x": -1.809,
      "y": 0.243,
      "angle": 344.997,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -1.401,
      "y": 0.216,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        48,
        25
      ]
    },
    {
      "id": 25,
      "x": -1.672,
      "y": 0.958,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        27
      ]
    }
  ],
  "cards_in_layout_amount": 27,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 59.9,
    "star2": 31.9,
    "star3": 8.0
  }
}