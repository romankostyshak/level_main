{
  "layout_cards": [
    {
      "id": 6,
      "x": -2.144,
      "y": 0.634,
      "layer": 2,
      "closed_by": [
        10
      ]
    },
    {
      "id": 7,
      "x": -2.144,
      "y": -0.186,
      "layer": 2,
      "closed_by": [
        12
      ]
    },
    {
      "id": 8,
      "y": 1.019,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 9,
      "x": 2.144,
      "y": 0.632,
      "layer": 2,
      "closed_by": [
        11
      ]
    },
    {
      "id": 10,
      "x": -2.144,
      "y": 1.021,
      "layer": 3,
      "closed_by": [
        14
      ]
    },
    {
      "id": 11,
      "x": 2.147,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        15
      ]
    },
    {
      "id": 12,
      "x": -2.147,
      "y": -0.574,
      "layer": 3,
      "closed_by": [
        13
      ]
    },
    {
      "id": 13,
      "x": -1.909,
      "y": -0.187,
      "layer": 4,
      "closed_by": [
        19
      ]
    },
    {
      "id": 14,
      "x": -1.906,
      "y": 0.634,
      "layer": 4,
      "closed_by": [
        18
      ]
    },
    {
      "id": 15,
      "x": 1.906,
      "y": 0.633,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 16,
      "x": 1.906,
      "y": -0.187,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 17,
      "x": 2.144,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        16
      ]
    },
    {
      "id": 18,
      "x": -1.853,
      "y": 1.019,
      "layer": 5,
      "closed_by": [
        22
      ]
    },
    {
      "id": 19,
      "x": -1.85,
      "y": -0.574,
      "layer": 5,
      "closed_by": [
        21
      ]
    },
    {
      "id": 20,
      "x": 1.855,
      "y": -0.578,
      "layer": 5,
      "closed_by": [
        23
      ]
    },
    {
      "id": 21,
      "x": -1.58,
      "y": -0.259,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -1.58,
      "y": 0.699,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 1.577,
      "y": -0.259,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 1.598,
      "y": 0.699,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 25,
      "x": -1.179,
      "y": -0.259,
      "layer": 5,
      "closed_by": [
        21
      ]
    },
    {
      "id": 26,
      "x": 1.179,
      "y": -0.259,
      "layer": 5,
      "closed_by": [
        23
      ]
    },
    {
      "id": 27,
      "x": -1.179,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        22
      ]
    },
    {
      "id": 28,
      "x": -0.939,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        25
      ]
    },
    {
      "id": 29,
      "x": 0.939,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        26
      ]
    },
    {
      "id": 30,
      "x": -0.939,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 31,
      "x": 0.939,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 32,
      "x": -0.939,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        28,
        30
      ]
    },
    {
      "id": 33,
      "x": 0.939,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        29,
        31
      ]
    },
    {
      "id": 34,
      "y": -0.578,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 35,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        8
      ]
    },
    {
      "id": 36,
      "x": 1.179,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        24
      ]
    },
    {
      "id": 37,
      "x": 1.85,
      "y": 1.021,
      "layer": 5,
      "closed_by": [
        24
      ]
    },
    {
      "id": 38,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        39,
        40
      ]
    },
    {
      "id": 39,
      "x": 0.384,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 40,
      "x": -0.384,
      "y": 0.781,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 41,
      "x": -0.546,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        32,
        40
      ]
    },
    {
      "id": 42,
      "x": 0.546,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        33,
        39
      ]
    },
    {
      "id": 43,
      "x": -0.384,
      "y": -0.578,
      "layer": 5,
      "closed_by": [
        34
      ]
    },
    {
      "id": 44,
      "x": 0.384,
      "y": -0.578,
      "layer": 5,
      "closed_by": [
        34
      ]
    },
    {
      "id": 45,
      "x": -0.001,
      "y": -0.1,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 46,
      "x": -0.305,
      "y": -0.577,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 47,
      "x": 0.307,
      "y": -0.577,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 48,
      "x": 0.305,
      "y": -0.1,
      "layer": 1,
      "closed_by": [
        42,
        47
      ]
    },
    {
      "id": 49,
      "x": -0.305,
      "y": -0.097,
      "layer": 1,
      "closed_by": [
        41,
        46
      ]
    },
    {
      "id": 50,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        43,
        44
      ]
    },
    {
      "id": 51,
      "x": 2.144,
      "y": -0.187,
      "layer": 2,
      "closed_by": [
        17
      ]
    }
  ],
  "cards_in_layout_amount": 46,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 0.2,
    "star2": 9.0,
    "star3": 90.6
  }
}