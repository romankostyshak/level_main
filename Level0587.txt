{
  "layout_cards": [
    {
      "id": 51,
      "x": 1.695,
      "y": 1.019,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 50,
      "x": 1.695,
      "y": 0.629,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 49,
      "x": -1.692,
      "y": 1.021,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 48,
      "x": -1.692,
      "y": 0.629,
      "layer": 4,
      "closed_by": [
        49
      ]
    },
    {
      "id": 47,
      "x": -1.695,
      "y": 0.79,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 46,
      "x": -1.695,
      "y": 0.828,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 45,
      "x": -1.695,
      "y": -0.172,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": -1.695,
      "y": -0.048,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 43,
      "x": -1.695,
      "y": 0.209,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 42,
      "x": 1.694,
      "y": 0.85,
      "layer": 1,
      "closed_by": [
        41
      ]
    },
    {
      "id": 41,
      "x": 1.694,
      "y": 0.79,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 40,
      "x": 1.694,
      "y": -0.028,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 39,
      "x": 1.694,
      "y": -0.172,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 38,
      "x": 1.694,
      "y": 0.209,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 37,
      "y": 0.063,
      "layer": 1,
      "closed_by": [
        35,
        24,
        25
      ]
    },
    {
      "id": 36,
      "x": -0.67,
      "y": 0.657,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 35,
      "x": -0.001,
      "y": 0.629,
      "layer": 2,
      "closed_by": [
        34,
        33
      ]
    },
    {
      "id": 34,
      "x": 0.597,
      "y": 0.722,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 33,
      "x": -0.587,
      "y": 0.717,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 32,
      "x": 0.409,
      "y": 0.975,
      "angle": 30.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 0.68,
      "y": 0.661,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 29,
      "x": -0.758,
      "y": 0.435,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 28,
      "x": 0.763,
      "y": 0.435,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        30
      ]
    },
    {
      "id": 24,
      "x": -0.303,
      "y": -0.256,
      "layer": 2,
      "closed_by": [
        23,
        21
      ]
    },
    {
      "id": 25,
      "x": 0.307,
      "y": -0.261,
      "layer": 2,
      "closed_by": [
        23,
        22
      ]
    },
    {
      "id": 23,
      "x": 0.001,
      "y": -0.335,
      "layer": 3,
      "closed_by": [
        26,
        20
      ]
    },
    {
      "id": 26,
      "x": -0.384,
      "y": -0.418,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 20,
      "x": 0.381,
      "y": -0.416,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 21,
      "x": -0.544,
      "y": -0.499,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 22,
      "x": 0.546,
      "y": -0.497,
      "layer": 3,
      "closed_by": [
        20
      ]
    },
    {
      "id": 31,
      "x": -0.4,
      "y": 0.97,
      "angle": 330.0,
      "layer": 4,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 31,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 31.3,
    "star2": 59.9,
    "star3": 8.0
  }
}