{
  "layout_cards": [
    {
      "id": 15,
      "x": 0.333,
      "y": 0.939,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 16,
      "x": -0.33,
      "y": 0.939,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 19,
      "x": -1.664,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 20,
      "x": -1.664,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 21,
      "x": 1.666,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 22,
      "x": -1.424,
      "y": -0.177,
      "layer": 3,
      "closed_by": [
        26,
        31
      ]
    },
    {
      "id": 23,
      "x": 1.427,
      "y": -0.172,
      "layer": 3,
      "closed_by": [
        27,
        43
      ]
    },
    {
      "id": 24,
      "x": -1.291,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        26,
        32
      ]
    },
    {
      "id": 25,
      "x": 1.294,
      "y": 0.861,
      "layer": 3,
      "closed_by": [
        27,
        44
      ]
    },
    {
      "id": 26,
      "x": -1.455,
      "y": 0.18,
      "angle": 45.0,
      "layer": 4,
      "closed_by": [
        29,
        30
      ]
    },
    {
      "id": 27,
      "x": 1.45,
      "y": 0.184,
      "angle": 315.0,
      "layer": 4,
      "closed_by": [
        41,
        42
      ]
    },
    {
      "id": 28,
      "x": 0.001,
      "y": -0.344,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -1.105,
      "y": 0.699,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -1.105,
      "y": -0.108,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -0.912,
      "y": -0.223,
      "angle": 315.0,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 32,
      "x": -0.916,
      "y": 0.578,
      "angle": 315.0,
      "layer": 4,
      "closed_by": [
        16,
        29,
        30
      ]
    },
    {
      "id": 33,
      "x": -0.707,
      "y": 0.133,
      "layer": 3,
      "closed_by": [
        31,
        32
      ]
    },
    {
      "id": 34,
      "x": -0.703,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 35,
      "x": -0.518,
      "y": 0.578,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        33,
        34,
        36
      ]
    },
    {
      "id": 36,
      "x": -0.007,
      "y": 0.469,
      "layer": 3,
      "closed_by": [
        15,
        16
      ]
    },
    {
      "id": 37,
      "x": 1.667,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 38,
      "x": -0.521,
      "y": -0.238,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        28,
        33
      ]
    },
    {
      "id": 39,
      "x": -0.331,
      "y": -0.344,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 40,
      "x": -0.328,
      "y": 0.472,
      "layer": 1,
      "closed_by": [
        35,
        38
      ]
    },
    {
      "id": 41,
      "x": 1.105,
      "y": -0.108,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 42,
      "x": 1.103,
      "y": 0.699,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 43,
      "x": 0.911,
      "y": -0.216,
      "angle": 45.0,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 44,
      "x": 0.907,
      "y": 0.587,
      "angle": 45.0,
      "layer": 4,
      "closed_by": [
        15,
        41,
        42
      ]
    },
    {
      "id": 45,
      "x": 0.708,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 46,
      "x": 0.513,
      "y": -0.238,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        28,
        50
      ]
    },
    {
      "id": 47,
      "x": 0.518,
      "y": 0.577,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        36,
        45,
        50
      ]
    },
    {
      "id": 48,
      "x": 0.333,
      "y": 0.469,
      "layer": 1,
      "closed_by": [
        46,
        47
      ]
    },
    {
      "id": 49,
      "x": 0.331,
      "y": -0.344,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 50,
      "x": 0.703,
      "y": 0.136,
      "layer": 3,
      "closed_by": [
        43,
        44
      ]
    }
  ],
  "cards_in_layout_amount": 34,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 46.5,
    "star2": 42.4,
    "star3": 10.2
  }
}