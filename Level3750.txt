{
  "layout_cards": [
    {
      "id": 24,
      "x": 1.519,
      "y": -0.577,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 1.0,
      "y": 0.061,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        26
      ]
    },
    {
      "id": 26,
      "x": 1.519,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        28,
        29,
        35
      ]
    },
    {
      "id": 27,
      "x": 2.039,
      "y": 0.061,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        26
      ]
    },
    {
      "id": 28,
      "x": 1.96,
      "y": -0.356,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        24,
        30
      ]
    },
    {
      "id": 29,
      "x": 1.077,
      "y": -0.356,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        24,
        31
      ]
    },
    {
      "id": 30,
      "x": 2.0,
      "y": 0.261,
      "angle": 300.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 1.039,
      "y": 0.282,
      "angle": 60.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 1.519,
      "y": 1.039,
      "layer": 1,
      "closed_by": [
        33,
        34
      ]
    },
    {
      "id": 33,
      "x": 1.22,
      "y": 1.039,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 34,
      "x": 1.82,
      "y": 1.039,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 35,
      "x": 1.519,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        30,
        31
      ]
    },
    {
      "id": 36,
      "x": -1.439,
      "y": -0.5,
      "layer": 1,
      "closed_by": [
        39,
        50
      ]
    },
    {
      "id": 37,
      "x": -1.74,
      "y": 1.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -2.059,
      "y": -0.5,
      "layer": 1,
      "closed_by": [
        39,
        51
      ]
    },
    {
      "id": 39,
      "x": -1.74,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 40,
      "x": -1.74,
      "y": -0.119,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 41,
      "x": -1.74,
      "y": 0.379,
      "layer": 2,
      "closed_by": [
        40,
        44,
        45
      ]
    },
    {
      "id": 42,
      "x": -2.559,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        44,
        47
      ]
    },
    {
      "id": 43,
      "x": -0.898,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        45,
        46
      ]
    },
    {
      "id": 44,
      "x": -2.18,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 45,
      "x": -1.299,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 46,
      "x": -0.759,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 47,
      "x": -2.72,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        49
      ]
    },
    {
      "id": 48,
      "x": -0.66,
      "y": 0.859,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 49,
      "x": -2.819,
      "y": 0.859,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 50,
      "x": -1.22,
      "y": -0.578,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -2.259,
      "y": -0.578,
      "layer": 2,
      "closed_by": []
    }
  ],
  "level_mechanic": 2,
  "free_cards_from_stickers": [
    36,
    37,
    38,
    39,
    40,
    41,
    42,
    43,
    44,
    45,
    46,
    47,
    48,
    49,
    50,
    51
  ],
  "cards_in_layout_amount": 28,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 74.8,
    "star2": 23.8,
    "star3": 0.4
  }
}