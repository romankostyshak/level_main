{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.952,
      "y": -0.136,
      "angle": 45.0,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 50,
      "x": -0.75,
      "y": 0.787,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        49
      ]
    },
    {
      "id": 49,
      "x": -0.944,
      "y": 0.597,
      "angle": 45.0,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 48,
      "x": 0.757,
      "y": -0.331,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        51
      ]
    },
    {
      "id": 47,
      "y": 0.194,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 46,
      "x": -1.082,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 45,
      "x": 0.573,
      "y": -0.569,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 44,
      "x": -0.572,
      "y": -0.568,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 43,
      "x": -0.832,
      "y": -0.568,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 42,
      "x": 0.836,
      "y": 1.023,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 41,
      "x": -0.573,
      "y": -0.569,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 40,
      "x": -0.833,
      "y": -0.569,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -0.564,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 38,
      "x": 0.574,
      "y": 1.023,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 37,
      "x": 0.573,
      "y": 1.023,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 36,
      "x": 1.082,
      "y": -0.569,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 35,
      "x": 0.833,
      "y": 1.021,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -1.136,
      "y": 0.405,
      "angle": 45.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 1.144,
      "y": 0.054,
      "angle": 45.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 32,
      "y": 0.128,
      "angle": 315.0,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 31,
      "x": 0.004,
      "y": 0.261,
      "angle": 315.0,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 30,
      "x": 0.008,
      "y": 0.326,
      "angle": 315.0,
      "layer": 4,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 22,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 48.7,
    "star2": 5.6,
    "star3": 45.4
  }
}