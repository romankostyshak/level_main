{
  "layout_cards": [
    {
      "id": 11,
      "x": -1.769,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        12
      ]
    },
    {
      "id": 12,
      "x": -1.769,
      "y": 0.703,
      "layer": 2,
      "closed_by": [
        14
      ]
    },
    {
      "id": 13,
      "x": 1.85,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        15
      ]
    },
    {
      "id": 14,
      "x": -1.769,
      "y": 0.224,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 15,
      "x": 1.853,
      "y": 0.216,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 16,
      "x": 1.85,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        13
      ]
    },
    {
      "id": 18,
      "x": -1.769,
      "y": -0.25,
      "layer": 2,
      "closed_by": [
        14
      ]
    },
    {
      "id": 19,
      "x": 1.853,
      "y": -0.261,
      "layer": 2,
      "closed_by": [
        15
      ]
    },
    {
      "id": 20,
      "x": 1.853,
      "y": -0.583,
      "layer": 1,
      "closed_by": [
        19
      ]
    },
    {
      "id": 21,
      "x": -1.026,
      "y": -0.495,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -1.026,
      "y": 0.939,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 1.026,
      "y": 0.939,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 1.029,
      "y": -0.497,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 25,
      "x": -0.944,
      "y": -0.416,
      "layer": 5,
      "closed_by": [
        21
      ]
    },
    {
      "id": 26,
      "x": 0.949,
      "y": -0.412,
      "layer": 5,
      "closed_by": [
        24
      ]
    },
    {
      "id": 27,
      "x": -0.947,
      "y": 0.861,
      "layer": 5,
      "closed_by": [
        22
      ]
    },
    {
      "id": 28,
      "x": -0.56,
      "y": 0.224,
      "layer": 4,
      "closed_by": [
        25,
        27
      ]
    },
    {
      "id": 29,
      "x": 0.572,
      "y": 0.224,
      "layer": 4,
      "closed_by": [
        26,
        36
      ]
    },
    {
      "id": 30,
      "x": -0.002,
      "y": 0.214,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -0.865,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 32,
      "x": 0.869,
      "y": -0.34,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 33,
      "x": -0.305,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        28,
        30
      ]
    },
    {
      "id": 34,
      "x": 0.307,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        29,
        30
      ]
    },
    {
      "id": 35,
      "x": -0.545,
      "y": -0.499,
      "layer": 2,
      "closed_by": [
        31,
        33
      ]
    },
    {
      "id": 36,
      "x": 0.948,
      "y": 0.861,
      "layer": 5,
      "closed_by": [
        23
      ]
    },
    {
      "id": 37,
      "x": -1.771,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        18
      ]
    },
    {
      "id": 38,
      "x": 0.559,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        32,
        34
      ]
    },
    {
      "id": 39,
      "y": -0.34,
      "layer": 2,
      "closed_by": [
        33,
        34
      ]
    },
    {
      "id": 40,
      "x": 0.333,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        38,
        39
      ]
    },
    {
      "id": 41,
      "x": -0.331,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        35,
        39
      ]
    },
    {
      "id": 42,
      "x": -0.865,
      "y": 0.782,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 43,
      "x": 0.865,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 44,
      "x": 0.307,
      "y": 0.697,
      "layer": 3,
      "closed_by": [
        29,
        30
      ]
    },
    {
      "id": 45,
      "x": -0.546,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        42,
        50
      ]
    },
    {
      "id": 46,
      "x": 0.546,
      "y": 0.944,
      "layer": 2,
      "closed_by": [
        43,
        44
      ]
    },
    {
      "id": 47,
      "x": -0.001,
      "y": 0.782,
      "layer": 2,
      "closed_by": [
        44,
        50
      ]
    },
    {
      "id": 48,
      "x": 0.331,
      "y": 1.023,
      "layer": 1,
      "closed_by": [
        46,
        47
      ]
    },
    {
      "id": 49,
      "x": -0.33,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        45,
        47
      ]
    },
    {
      "id": 50,
      "x": -0.303,
      "y": 0.703,
      "layer": 3,
      "closed_by": [
        28,
        30
      ]
    }
  ],
  "cards_in_layout_amount": 39,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 11.1,
    "star2": 61.1,
    "star3": 27.1
  }
}