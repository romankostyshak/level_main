{
  "layout_cards": [
    {
      "id": 20,
      "x": -1.052,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        21
      ]
    },
    {
      "id": 21,
      "x": -1.213,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 22,
      "x": 1.212,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 23,
      "x": -1.371,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 24,
      "x": 1.371,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        49
      ]
    },
    {
      "id": 25,
      "x": 0.386,
      "y": -0.574,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 26,
      "x": -0.004,
      "y": 0.3,
      "layer": 1,
      "closed_by": [
        28,
        29
      ]
    },
    {
      "id": 27,
      "x": -0.003,
      "y": -0.504,
      "layer": 1,
      "closed_by": [
        28,
        29
      ]
    },
    {
      "id": 28,
      "x": 0.259,
      "y": -0.101,
      "layer": 2,
      "closed_by": [
        25,
        30
      ]
    },
    {
      "id": 29,
      "x": -0.259,
      "y": -0.1,
      "layer": 2,
      "closed_by": [
        31,
        32
      ]
    },
    {
      "id": 30,
      "x": 0.386,
      "y": 0.379,
      "layer": 3,
      "closed_by": [
        33,
        34
      ]
    },
    {
      "id": 31,
      "x": -0.384,
      "y": 0.379,
      "layer": 3,
      "closed_by": [
        33,
        35
      ]
    },
    {
      "id": 32,
      "x": -0.384,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 33,
      "x": -0.004,
      "y": -0.1,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 0.546,
      "y": 0.702,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 35,
      "x": -0.546,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 36,
      "x": 0.869,
      "y": 1.019,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 37,
      "x": -0.865,
      "y": 1.021,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 1.506,
      "y": 0.231,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 39,
      "x": 1.187,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        36,
        38
      ]
    },
    {
      "id": 40,
      "x": -1.184,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        37,
        48
      ]
    },
    {
      "id": 41,
      "x": 1.373,
      "y": 0.381,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 42,
      "x": -1.37,
      "y": 0.381,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 43,
      "x": -1.585,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 44,
      "x": 1.585,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 45,
      "x": 1.266,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 46,
      "x": -1.266,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 47,
      "x": -1.508,
      "y": -0.578,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 48,
      "x": -1.506,
      "y": 0.236,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 49,
      "x": 1.508,
      "y": -0.578,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 50,
      "x": 1.054,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        22
      ]
    }
  ],
  "cards_in_layout_amount": 31,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 33.7,
    "star2": 57.1,
    "star3": 8.4
  }
}