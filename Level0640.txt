{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.712,
      "y": 0.957,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 50,
      "x": 0.712,
      "y": -0.509,
      "layer": 1,
      "closed_by": [
        17
      ]
    },
    {
      "id": 49,
      "x": 0.716,
      "y": 0.953,
      "layer": 2,
      "closed_by": [
        44,
        43
      ]
    },
    {
      "id": 48,
      "x": -0.721,
      "y": 0.953,
      "layer": 2,
      "closed_by": [
        46,
        45
      ]
    },
    {
      "id": 47,
      "x": -0.717,
      "y": -0.513,
      "layer": 2,
      "closed_by": [
        41,
        39
      ]
    },
    {
      "id": 46,
      "x": -1.014,
      "y": 0.873,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 45,
      "x": -0.411,
      "y": 0.875,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 44,
      "x": 0.412,
      "y": 0.87,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 43,
      "x": 1.013,
      "y": 0.874,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 42,
      "x": 0.412,
      "y": -0.428,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 41,
      "x": -0.407,
      "y": -0.428,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 40,
      "x": 1.016,
      "y": -0.43,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 39,
      "x": -1.018,
      "y": -0.432,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 38,
      "x": 0.465,
      "y": 0.638,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 37,
      "x": -0.462,
      "y": 0.638,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 36,
      "x": 0.462,
      "y": -0.172,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 35,
      "x": -0.467,
      "y": -0.187,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 34,
      "x": 0.273,
      "y": 0.231,
      "layer": 5,
      "closed_by": [
        19
      ]
    },
    {
      "id": 33,
      "x": 1.284,
      "y": 0.87,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 32,
      "x": 1.286,
      "y": -0.425,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 31,
      "x": -1.279,
      "y": 0.873,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 30,
      "x": -1.284,
      "y": -0.432,
      "layer": 4,
      "closed_by": [
        26
      ]
    },
    {
      "id": 29,
      "x": 1.554,
      "y": 0.87,
      "layer": 5,
      "closed_by": [
        25
      ]
    },
    {
      "id": 28,
      "x": -1.544,
      "y": 0.873,
      "layer": 5,
      "closed_by": [
        23
      ]
    },
    {
      "id": 27,
      "x": 1.554,
      "y": -0.425,
      "layer": 5,
      "closed_by": [
        24
      ]
    },
    {
      "id": 26,
      "x": -1.539,
      "y": -0.428,
      "layer": 5,
      "closed_by": [
        22
      ]
    },
    {
      "id": 25,
      "x": 1.82,
      "y": 0.873,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 1.82,
      "y": -0.425,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 23,
      "x": -1.814,
      "y": 0.875,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -1.817,
      "y": -0.428,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 21,
      "x": -0.722,
      "y": 0.957,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 20,
      "x": -0.263,
      "y": 0.231,
      "layer": 5,
      "closed_by": [
        19
      ]
    },
    {
      "id": 19,
      "x": 0.004,
      "y": 0.231,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 18,
      "x": -0.72,
      "y": -0.513,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 17,
      "x": 0.716,
      "y": -0.509,
      "layer": 2,
      "closed_by": [
        42,
        40
      ]
    }
  ],
  "cards_in_layout_amount": 35,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 19.6,
    "star2": 69.0,
    "star3": 10.9
  }
}