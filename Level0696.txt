{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.001,
      "y": 0.944,
      "layer": 1,
      "closed_by": [
        50,
        49
      ]
    },
    {
      "id": 50,
      "x": -0.256,
      "y": 0.703,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 49,
      "x": 0.261,
      "y": 0.703,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 48,
      "x": -0.518,
      "y": 0.944,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 47,
      "x": 0.523,
      "y": 0.944,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 46,
      "x": 0.787,
      "y": 0.699,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 45,
      "x": -0.785,
      "y": 0.703,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 44,
      "x": -0.785,
      "y": -0.178,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 43,
      "x": 0.787,
      "y": -0.179,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -0.518,
      "y": -0.414,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 41,
      "x": 0.518,
      "y": -0.412,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 40,
      "x": -0.261,
      "y": -0.178,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 39,
      "x": 0.27,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 38,
      "y": -0.414,
      "layer": 1,
      "closed_by": [
        40,
        39
      ]
    },
    {
      "id": 37,
      "x": 1.452,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 36,
      "x": 1.452,
      "y": -0.178,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 35,
      "x": -1.452,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 34,
      "x": -1.452,
      "y": -0.177,
      "layer": 1,
      "closed_by": [
        30
      ]
    },
    {
      "id": 33,
      "x": 1.746,
      "y": 0.785,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 32,
      "x": 1.746,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 31,
      "x": -1.746,
      "y": 0.786,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 30,
      "x": -1.746,
      "y": -0.261,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 29,
      "x": 1.985,
      "y": 0.259,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 28,
      "x": -1.985,
      "y": 0.256,
      "layer": 3,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 24,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 63.5,
    "star2": 15.9,
    "star3": 19.6
  }
}