{
  "layout_cards": [
    {
      "id": 8,
      "x": -2.223,
      "y": 0.46,
      "layer": 1,
      "closed_by": [
        9
      ]
    },
    {
      "id": 9,
      "x": -1.983,
      "y": 0.46,
      "layer": 2,
      "closed_by": [
        14
      ]
    },
    {
      "id": 10,
      "x": 1.985,
      "y": 0.462,
      "layer": 2,
      "closed_by": [
        15
      ]
    },
    {
      "id": 11,
      "x": -2.223,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        12
      ]
    },
    {
      "id": 12,
      "x": -1.985,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        14
      ]
    },
    {
      "id": 13,
      "x": 1.985,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        15
      ]
    },
    {
      "id": 14,
      "x": -1.985,
      "y": -0.096,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 15,
      "x": 1.985,
      "y": -0.093,
      "layer": 3,
      "closed_by": [
        19
      ]
    },
    {
      "id": 16,
      "x": 2.223,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        13
      ]
    },
    {
      "id": 17,
      "x": 2.223,
      "y": 0.462,
      "layer": 1,
      "closed_by": [
        10
      ]
    },
    {
      "id": 18,
      "x": -1.664,
      "y": -0.097,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 19,
      "x": 1.667,
      "y": -0.093,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 21,
      "x": -0.625,
      "y": -0.179,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -1.052,
      "y": 1.021,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 1.054,
      "y": 1.019,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 1.215,
      "y": -0.421,
      "layer": 1,
      "closed_by": [
        30
      ]
    },
    {
      "id": 25,
      "x": 1.373,
      "y": 0.379,
      "layer": 3,
      "closed_by": [
        19,
        26
      ]
    },
    {
      "id": 26,
      "x": 1.373,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 27,
      "x": -1.375,
      "y": -0.017,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 28,
      "x": -1.375,
      "y": 0.379,
      "layer": 3,
      "closed_by": [
        18,
        29
      ]
    },
    {
      "id": 29,
      "x": -1.375,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 30,
      "x": 1.373,
      "y": -0.017,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 31,
      "x": -1.213,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        27
      ]
    },
    {
      "id": 32,
      "x": 0.625,
      "y": -0.172,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -0.782,
      "y": 0.46,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 34,
      "x": 0.787,
      "y": 0.462,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 35,
      "x": -0.703,
      "y": 0.54,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 36,
      "x": 0.711,
      "y": 0.54,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 37,
      "x": 0.002,
      "y": 0.939,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -0.412,
      "y": 1.021,
      "layer": 6,
      "closed_by": [
        37
      ]
    },
    {
      "id": 39,
      "x": 0.414,
      "y": 1.021,
      "layer": 6,
      "closed_by": [
        37
      ]
    },
    {
      "id": 40,
      "x": 0.001,
      "y": 0.541,
      "layer": 4,
      "closed_by": [
        38,
        39
      ]
    },
    {
      "id": 41,
      "x": -0.786,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        22,
        38
      ]
    },
    {
      "id": 42,
      "x": 0.79,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        23,
        39
      ]
    },
    {
      "id": 43,
      "x": -0.409,
      "y": 0.458,
      "layer": 3,
      "closed_by": [
        21,
        40,
        41
      ]
    },
    {
      "id": 44,
      "x": 0.414,
      "y": 0.458,
      "layer": 3,
      "closed_by": [
        32,
        40,
        42
      ]
    },
    {
      "id": 45,
      "x": 0.409,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 46,
      "x": -0.411,
      "y": -0.416,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 47,
      "x": 0.523,
      "y": -0.488,
      "angle": 340.0,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 48,
      "x": -0.522,
      "y": -0.493,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 49,
      "x": -0.625,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 50,
      "x": 0.625,
      "y": -0.577,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 51,
      "x": 0.001,
      "y": -0.577,
      "layer": 1,
      "closed_by": [
        47,
        48
      ]
    }
  ],
  "cards_in_layout_amount": 43,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 0.4,
    "star2": 18.1,
    "star3": 80.9
  }
}