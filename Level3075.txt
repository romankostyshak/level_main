{
  "layout_cards": [
    {
      "id": 9,
      "x": -1.825,
      "y": 0.143,
      "layer": 2,
      "closed_by": [
        17
      ]
    },
    {
      "id": 10,
      "x": -1.825,
      "y": 0.777,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 11,
      "x": -1.187,
      "y": 0.541,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 12,
      "x": -1.195,
      "y": -0.344,
      "layer": 4,
      "closed_by": [
        16
      ]
    },
    {
      "id": 13,
      "x": -0.104,
      "y": -0.574,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 14,
      "x": 0.143,
      "y": -0.577,
      "layer": 5,
      "closed_by": [
        13
      ]
    },
    {
      "id": 15,
      "x": -0.165,
      "y": 0.414,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 16,
      "x": -1.245,
      "y": -0.388,
      "layer": 5,
      "closed_by": [
        19
      ]
    },
    {
      "id": 17,
      "x": -1.666,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        10,
        36
      ]
    },
    {
      "id": 18,
      "x": -1.824,
      "y": -0.416,
      "layer": 1,
      "closed_by": [
        9
      ]
    },
    {
      "id": 19,
      "x": -0.865,
      "y": -0.574,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 20,
      "x": -1.105,
      "y": 0.462,
      "layer": 5,
      "closed_by": [
        11
      ]
    },
    {
      "id": 21,
      "x": 2.065,
      "y": 0.781,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 22,
      "x": 1.906,
      "y": 0.46,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 23,
      "x": 1.587,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 24,
      "x": 1.746,
      "y": 0.143,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 25,
      "x": -0.412,
      "y": -0.574,
      "layer": 5,
      "closed_by": [
        13,
        19
      ]
    },
    {
      "id": 26,
      "x": -0.68,
      "y": -0.023,
      "layer": 4,
      "closed_by": [
        20,
        25
      ]
    },
    {
      "id": 27,
      "x": 1.424,
      "y": -0.5,
      "layer": 1,
      "closed_by": [
        23,
        51
      ]
    },
    {
      "id": 28,
      "x": 1.985,
      "y": 0.781,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 29,
      "x": 1.588,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        24,
        33
      ]
    },
    {
      "id": 30,
      "x": -0.944,
      "y": 0.222,
      "layer": 3,
      "closed_by": [
        12,
        26,
        36
      ]
    },
    {
      "id": 31,
      "x": 0.734,
      "y": 0.777,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 0.092,
      "y": 0.307,
      "layer": 5,
      "closed_by": [
        15
      ]
    },
    {
      "id": 33,
      "x": 1.187,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 34,
      "x": -0.944,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 35,
      "x": -0.545,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        34,
        42
      ]
    },
    {
      "id": 36,
      "x": -1.266,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 38,
      "x": 0.307,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        31,
        32
      ]
    },
    {
      "id": 39,
      "x": -0.104,
      "y": 0.734,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 40,
      "x": 0.787,
      "y": -0.194,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 41,
      "x": 0.869,
      "y": 0.712,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 42,
      "x": -0.143,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 43,
      "x": 0.745,
      "y": -0.143,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 44,
      "x": 0.703,
      "y": -0.097,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 45,
      "x": 0.418,
      "y": -0.096,
      "layer": 2,
      "closed_by": [
        14,
        40,
        50
      ]
    },
    {
      "id": 46,
      "x": -0.165,
      "y": -0.184,
      "layer": 4,
      "closed_by": [
        14,
        25,
        32
      ]
    },
    {
      "id": 47,
      "x": -0.126,
      "y": -0.136,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": -0.093,
      "y": -0.097,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 49,
      "x": 0.25,
      "y": 0.62,
      "layer": 1,
      "closed_by": [
        42,
        45,
        48
      ]
    },
    {
      "id": 50,
      "x": 0.546,
      "y": 0.619,
      "layer": 3,
      "closed_by": [
        38,
        41,
        44
      ]
    },
    {
      "id": 51,
      "x": 1.029,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        40
      ]
    }
  ],
  "cards_in_layout_amount": 42,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 2.5,
    "star2": 44.2,
    "star3": 52.6
  }
}