{
  "layout_cards": [
    {
      "id": 51,
      "x": 1.692,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 50,
      "x": -1.694,
      "y": -0.577,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 49,
      "x": -0.259,
      "y": -0.256,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 48,
      "x": -1.59,
      "y": -0.493,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 47,
      "x": 1.587,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 46,
      "y": -0.1,
      "layer": 1,
      "closed_by": [
        49,
        31
      ]
    },
    {
      "id": 45,
      "x": 0.379,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 44,
      "x": -1.511,
      "y": -0.416,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 43,
      "x": 1.506,
      "y": -0.416,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 42,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        41,
        40
      ]
    },
    {
      "id": 41,
      "x": -0.384,
      "y": 0.943,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 40,
      "x": 0.386,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 39,
      "x": 0.625,
      "y": 0.861,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 38,
      "x": -0.625,
      "y": 0.861,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 37,
      "x": 0.865,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 36,
      "x": 1.194,
      "y": 0.699,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -1.187,
      "y": 0.699,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -1.042,
      "y": -0.337,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 1.039,
      "y": -0.337,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 1.424,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 30,
      "x": -0.37,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 29,
      "x": -1.424,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 28,
      "x": -0.865,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 26,
      "x": -0.629,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 31,
      "x": 0.263,
      "y": -0.256,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 27,
      "x": 0.643,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        33
      ]
    }
  ],
  "cards_in_layout_amount": 26,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 54.4,
    "star2": 23.2,
    "star3": 21.5
  }
}