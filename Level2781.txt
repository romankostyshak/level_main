{
  "layout_cards": [
    {
      "id": 20,
      "x": -0.814,
      "y": 0.837,
      "angle": 14.998,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 21,
      "x": -0.5,
      "y": 0.754,
      "angle": 14.998,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 22,
      "x": -1.115,
      "y": 0.757,
      "angle": 14.998,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 23,
      "x": -1.434,
      "y": 0.67,
      "angle": 14.998,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 24,
      "x": -0.18,
      "y": 0.685,
      "angle": 14.998,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 25,
      "x": -1.115,
      "y": 0.601,
      "angle": 14.998,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 26,
      "x": -0.5,
      "y": 0.601,
      "angle": 14.998,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 27,
      "x": -0.814,
      "y": 0.522,
      "angle": 14.998,
      "layer": 1,
      "closed_by": [
        21,
        22,
        25,
        26
      ]
    },
    {
      "id": 28,
      "x": -1.447,
      "y": -0.214,
      "angle": 15.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -1.128,
      "y": -0.289,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 30,
      "x": -2.069,
      "y": -0.377,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 31,
      "x": -0.814,
      "y": -0.37,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 32,
      "x": -1.753,
      "y": -0.451,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 33,
      "x": -1.131,
      "y": -0.451,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 34,
      "x": -1.444,
      "y": -0.537,
      "angle": 15.0,
      "layer": 1,
      "closed_by": [
        29,
        32,
        33,
        35
      ]
    },
    {
      "id": 35,
      "x": -1.753,
      "y": -0.293,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 36,
      "x": 0.052,
      "y": -0.158,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 37,
      "x": 0.675,
      "y": 0.897,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        44
      ]
    },
    {
      "id": 38,
      "x": 0.986,
      "y": 0.652,
      "angle": 15.0,
      "layer": 1,
      "closed_by": [
        39,
        40
      ]
    },
    {
      "id": 39,
      "x": 1.294,
      "y": 0.735,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 40,
      "x": 0.675,
      "y": 0.74,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 41,
      "x": 1.613,
      "y": 0.814,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 42,
      "x": 0.356,
      "y": 0.805,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 43,
      "x": 1.294,
      "y": 0.893,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": 1.001,
      "y": 0.984,
      "angle": 15.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 45,
      "x": 0.358,
      "y": -0.393,
      "angle": 15.0,
      "layer": 1,
      "closed_by": [
        46,
        47
      ]
    },
    {
      "id": 46,
      "x": 0.662,
      "y": -0.312,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 47,
      "x": 0.054,
      "y": -0.314,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        49
      ]
    },
    {
      "id": 48,
      "x": 0.972,
      "y": -0.231,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 49,
      "x": -0.263,
      "y": -0.246,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 50,
      "x": 0.662,
      "y": -0.15,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 51,
      "x": 0.358,
      "y": -0.075,
      "angle": 15.0,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 32,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 23.6,
    "star2": 66.8,
    "star3": 8.7
  }
}