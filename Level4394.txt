{
  "layout_cards": [
    {
      "id": 18,
      "x": -2.059,
      "y": 0.217,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 19,
      "x": 2.059,
      "y": 0.217,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 20,
      "x": -1.878,
      "y": 0.418,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 21,
      "x": -1.878,
      "y": 0.017,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 22,
      "x": 1.878,
      "y": 0.017,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 23,
      "x": -1.74,
      "y": 0.217,
      "layer": 1,
      "closed_by": [
        21
      ]
    },
    {
      "id": 24,
      "x": 1.74,
      "y": 0.217,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 25,
      "x": -1.139,
      "y": 0.819,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -1.139,
      "y": -0.379,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 27,
      "x": -1.059,
      "y": 0.238,
      "angle": 345.0,
      "layer": 3,
      "closed_by": [
        25,
        26
      ]
    },
    {
      "id": 28,
      "x": 1.139,
      "y": 0.819,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 29,
      "x": 1.139,
      "y": -0.379,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 1.059,
      "y": 0.2,
      "angle": 345.0,
      "layer": 3,
      "closed_by": [
        28,
        29
      ]
    },
    {
      "id": 31,
      "x": 1.059,
      "y": 0.238,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 32,
      "x": -0.976,
      "y": 0.859,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 33,
      "x": -0.976,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 34,
      "x": 0.98,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 35,
      "x": 0.98,
      "y": 0.859,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 36,
      "x": -1.059,
      "y": 0.2,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        27
      ]
    },
    {
      "id": 37,
      "x": 1.878,
      "y": 0.418,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        19
      ]
    },
    {
      "id": 38,
      "x": -0.337,
      "y": 0.859,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 39,
      "y": 0.217,
      "layer": 6,
      "closed_by": [
        38,
        44
      ]
    },
    {
      "id": 40,
      "y": -0.418,
      "layer": 5,
      "closed_by": [
        39
      ]
    },
    {
      "id": 41,
      "x": -0.337,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 42,
      "x": 0.2,
      "y": -0.319,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 43,
      "x": -0.2,
      "y": -0.319,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 44,
      "x": 0.337,
      "y": -0.418,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 45,
      "x": 0.337,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        50
      ]
    },
    {
      "id": 46,
      "x": -0.2,
      "y": 0.759,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 47,
      "x": 0.2,
      "y": 0.759,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": 0.337,
      "y": 0.217,
      "layer": 1,
      "closed_by": [
        40,
        42,
        45,
        47
      ]
    },
    {
      "id": 49,
      "x": -0.337,
      "y": 0.217,
      "layer": 1,
      "closed_by": [
        41,
        43,
        46,
        50
      ]
    },
    {
      "id": 50,
      "y": 0.859,
      "layer": 5,
      "closed_by": [
        39
      ]
    }
  ],
  "cards_in_layout_amount": 33,
  "cards_in_deck_amount": 28,
  "layers_in_level": 7,
  "open_cards": 8,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 38.5,
    "star2": 57.5,
    "star3": 2.5
  }
}