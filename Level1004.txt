{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.444,
      "y": -0.136,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        45,
        33
      ]
    },
    {
      "id": 50,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        51,
        49,
        48,
        47
      ]
    },
    {
      "id": 49,
      "x": -0.432,
      "y": -0.143,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        33,
        43
      ]
    },
    {
      "id": 48,
      "x": -0.437,
      "y": 0.745,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        44,
        25
      ]
    },
    {
      "id": 47,
      "x": 0.439,
      "y": 0.74,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        46,
        25
      ]
    },
    {
      "id": 46,
      "x": 0.74,
      "y": 0.782,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 45,
      "x": 0.749,
      "y": -0.18,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 44,
      "x": -0.74,
      "y": 0.782,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 42,
      "x": -0.947,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 41,
      "x": 1.184,
      "y": 0.861,
      "layer": 5,
      "closed_by": [
        38
      ]
    },
    {
      "id": 40,
      "x": 1.184,
      "y": -0.259,
      "layer": 5,
      "closed_by": [
        36
      ]
    },
    {
      "id": 39,
      "x": -1.19,
      "y": 0.861,
      "layer": 5,
      "closed_by": [
        35
      ]
    },
    {
      "id": 38,
      "x": 1.264,
      "y": 0.861,
      "layer": 6,
      "closed_by": [
        32
      ]
    },
    {
      "id": 37,
      "x": -1.19,
      "y": -0.261,
      "layer": 5,
      "closed_by": [
        34
      ]
    },
    {
      "id": 36,
      "x": 1.263,
      "y": -0.256,
      "layer": 6,
      "closed_by": [
        31
      ]
    },
    {
      "id": 35,
      "x": -1.269,
      "y": 0.861,
      "layer": 6,
      "closed_by": [
        30
      ]
    },
    {
      "id": 34,
      "x": -1.266,
      "y": -0.261,
      "layer": 6,
      "closed_by": [
        29
      ]
    },
    {
      "id": 33,
      "x": -0.002,
      "y": -0.172,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 32,
      "x": 1.345,
      "y": 0.861,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 1.345,
      "y": -0.259,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -1.342,
      "y": 0.861,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -1.34,
      "y": -0.263,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 0.944,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 26,
      "x": 0.944,
      "y": 0.785,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 27,
      "x": -0.949,
      "y": 0.785,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 43,
      "x": -0.735,
      "y": -0.18,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 25,
      "x": 0.004,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 22,
      "x": 0.003,
      "y": 0.859,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 21,
      "y": -0.256,
      "layer": 4,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 29,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 54.5,
    "star2": 36.6,
    "star3": 8.0
  }
}