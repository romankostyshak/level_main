{
  "layout_cards": [
    {
      "id": 51,
      "x": -1.845,
      "y": 0.243,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 50,
      "x": 0.312,
      "y": 0.68,
      "layer": 2,
      "closed_by": [
        45,
        33
      ]
    },
    {
      "id": 49,
      "x": -2.482,
      "y": 0.244,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 48,
      "x": 2.187,
      "y": 0.244,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 47,
      "x": 2.486,
      "y": 0.244,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 46,
      "x": 0.312,
      "y": -0.18,
      "layer": 2,
      "closed_by": [
        45,
        33
      ]
    },
    {
      "id": 45,
      "x": 0.001,
      "y": 0.243,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 44,
      "x": 0.657,
      "y": 1.062,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 43,
      "x": 1.276,
      "y": 0.243,
      "layer": 1,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -2.18,
      "y": 0.244,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 41,
      "x": -0.66,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 40,
      "x": -0.66,
      "y": 0.244,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -1.258,
      "y": 0.244,
      "layer": 1,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 1.871,
      "y": 0.244,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 37,
      "x": -0.303,
      "y": 0.68,
      "layer": 2,
      "closed_by": [
        45,
        40
      ]
    },
    {
      "id": 36,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        46,
        31
      ]
    },
    {
      "id": 35,
      "x": -0.66,
      "y": 1.062,
      "layer": 1,
      "closed_by": [
        37
      ]
    },
    {
      "id": 34,
      "y": 1.062,
      "layer": 1,
      "closed_by": [
        50,
        37
      ]
    },
    {
      "id": 33,
      "x": 0.657,
      "y": 0.244,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 0.66,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 31,
      "x": -0.305,
      "y": -0.18,
      "layer": 2,
      "closed_by": [
        45,
        40
      ]
    },
    {
      "id": 30,
      "x": 0.004,
      "y": 0.244,
      "layer": 1,
      "closed_by": [
        50,
        46,
        37,
        31
      ]
    }
  ],
  "cards_in_layout_amount": 22,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 50.8,
    "star2": 6.1,
    "star3": 42.7
  }
}