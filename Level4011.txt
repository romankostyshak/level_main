{
  "layout_cards": [
    {
      "x": 2.5,
      "y": -0.578,
      "angle": 300.0,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 1,
      "x": -2.519,
      "y": -0.559,
      "angle": 60.0,
      "layer": 1,
      "closed_by": [
        3
      ]
    },
    {
      "id": 2,
      "x": -2.499,
      "y": 1.019,
      "angle": 300.0,
      "layer": 1,
      "closed_by": [
        7
      ]
    },
    {
      "id": 3,
      "x": -2.779,
      "y": -0.199,
      "layer": 2,
      "closed_by": [
        14
      ]
    },
    {
      "id": 4,
      "x": 0.418,
      "y": 0.74,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 5,
      "x": -0.418,
      "y": -0.298,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 6,
      "x": -0.418,
      "y": 0.74,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 7,
      "x": -2.779,
      "y": 0.638,
      "layer": 2,
      "closed_by": [
        14
      ]
    },
    {
      "id": 8,
      "x": 2.779,
      "y": 0.217,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 9,
      "x": 0.418,
      "y": -0.298,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 10,
      "x": 1.741,
      "y": 0.217,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 11,
      "x": -1.58,
      "y": 0.217,
      "layer": 4,
      "closed_by": [
        13
      ]
    },
    {
      "id": 12,
      "x": 2.262,
      "y": 0.217,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 13,
      "x": -1.741,
      "y": 0.217,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 14,
      "x": -2.398,
      "y": 0.217,
      "layer": 3,
      "closed_by": [
        18,
        31
      ]
    },
    {
      "id": 15,
      "x": 2.381,
      "y": 0.217,
      "layer": 3,
      "closed_by": [
        8,
        12
      ]
    },
    {
      "id": 16,
      "x": -2.22,
      "y": 0.217,
      "layer": 1,
      "closed_by": [
        50,
        51
      ]
    },
    {
      "id": 17,
      "x": 1.582,
      "y": 0.217,
      "layer": 4,
      "closed_by": [
        10
      ]
    },
    {
      "id": 18,
      "x": -2.259,
      "y": 0.217,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 19,
      "x": 2.22,
      "y": 0.217,
      "layer": 1,
      "closed_by": [
        33,
        49
      ]
    },
    {
      "id": 20,
      "x": -0.379,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        27,
        36
      ]
    },
    {
      "id": 21,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        23,
        24
      ]
    },
    {
      "id": 22,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        6,
        20,
        25
      ]
    },
    {
      "id": 23,
      "x": 0.379,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        26,
        32
      ]
    },
    {
      "id": 24,
      "x": -0.379,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        27,
        35
      ]
    },
    {
      "id": 25,
      "x": 0.379,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        26,
        37
      ]
    },
    {
      "id": 26,
      "x": 0.5,
      "y": 0.217,
      "layer": 3,
      "closed_by": [
        4,
        9
      ]
    },
    {
      "id": 27,
      "x": -0.499,
      "y": 0.217,
      "layer": 3,
      "closed_by": [
        5,
        6
      ]
    },
    {
      "id": 28,
      "x": 2.779,
      "y": 0.638,
      "layer": 2,
      "closed_by": [
        15
      ]
    },
    {
      "id": 29,
      "x": 2.779,
      "y": -0.199,
      "layer": 2,
      "closed_by": [
        15
      ]
    },
    {
      "id": 30,
      "x": 2.5,
      "y": 1.019,
      "angle": 60.0,
      "layer": 1,
      "closed_by": [
        8,
        28
      ]
    },
    {
      "id": 31,
      "x": -2.779,
      "y": 0.217,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 0.782,
      "y": -0.597,
      "angle": 60.0,
      "layer": 3,
      "closed_by": [
        9
      ]
    },
    {
      "id": 33,
      "x": 1.942,
      "y": -0.199,
      "angle": 300.0,
      "layer": 2,
      "closed_by": [
        15,
        46
      ]
    },
    {
      "id": 34,
      "x": -1.381,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        41,
        51
      ]
    },
    {
      "id": 35,
      "x": -0.781,
      "y": -0.597,
      "angle": 300.0,
      "layer": 3,
      "closed_by": [
        5
      ]
    },
    {
      "id": 36,
      "x": -0.781,
      "y": 1.059,
      "angle": 60.0,
      "layer": 3,
      "closed_by": [
        6
      ]
    },
    {
      "id": 37,
      "x": 0.782,
      "y": 1.059,
      "angle": 300.0,
      "layer": 3,
      "closed_by": [
        4
      ]
    },
    {
      "id": 38,
      "x": 1.182,
      "y": 1.059,
      "angle": 60.0,
      "layer": 2,
      "closed_by": [
        37,
        45
      ]
    },
    {
      "id": 39,
      "x": 1.182,
      "y": -0.597,
      "angle": 300.0,
      "layer": 2,
      "closed_by": [
        32,
        46
      ]
    },
    {
      "id": 40,
      "x": -1.179,
      "y": -0.597,
      "angle": 60.0,
      "layer": 2,
      "closed_by": [
        35,
        47
      ]
    },
    {
      "id": 41,
      "x": -1.179,
      "y": 1.047,
      "angle": 300.0,
      "layer": 2,
      "closed_by": [
        36,
        48
      ]
    },
    {
      "id": 42,
      "x": 1.381,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        38,
        49
      ]
    },
    {
      "id": 43,
      "x": 1.381,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        33,
        39
      ]
    },
    {
      "id": 44,
      "x": -1.381,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        40,
        50
      ]
    },
    {
      "id": 45,
      "x": 1.542,
      "y": 0.638,
      "layer": 3,
      "closed_by": [
        17
      ]
    },
    {
      "id": 46,
      "x": 1.542,
      "y": -0.199,
      "layer": 3,
      "closed_by": [
        17
      ]
    },
    {
      "id": 47,
      "x": -1.541,
      "y": -0.199,
      "layer": 3,
      "closed_by": [
        11
      ]
    },
    {
      "id": 48,
      "x": -1.541,
      "y": 0.638,
      "layer": 3,
      "closed_by": [
        11
      ]
    },
    {
      "id": 49,
      "x": 1.942,
      "y": 0.638,
      "angle": 60.0,
      "layer": 2,
      "closed_by": [
        15,
        45
      ]
    },
    {
      "id": 50,
      "x": -1.94,
      "y": -0.199,
      "angle": 60.0,
      "layer": 2,
      "closed_by": [
        14,
        47
      ]
    },
    {
      "id": 51,
      "x": -1.94,
      "y": 0.638,
      "angle": 300.0,
      "layer": 2,
      "closed_by": [
        14,
        48
      ]
    }
  ],
  "cards_in_layout_amount": 52,
  "cards_in_deck_amount": 28,
  "layers_in_level": 5,
  "open_cards": 10,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 0.8,
    "star2": 56.3,
    "star3": 42.2
  }
}