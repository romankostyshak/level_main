{
  "layout_cards": [
    {
      "id": 11,
      "y": 0.861,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 12,
      "x": -0.407,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        11
      ]
    },
    {
      "id": 13,
      "x": -1.904,
      "y": 0.541,
      "layer": 3,
      "closed_by": [
        15,
        19
      ]
    },
    {
      "id": 14,
      "x": 1.906,
      "y": 0.537,
      "layer": 3,
      "closed_by": [
        18,
        20
      ]
    },
    {
      "id": 15,
      "x": -1.904,
      "y": -0.093,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 16,
      "x": 0.412,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        11
      ]
    },
    {
      "id": 18,
      "x": 1.906,
      "y": -0.093,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 19,
      "x": -1.723,
      "y": 0.984,
      "angle": 315.0,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 20,
      "x": 1.722,
      "y": 0.981,
      "angle": 45.0,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 21,
      "x": 1.725,
      "y": -0.537,
      "angle": 315.0,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 22,
      "x": 1.514,
      "y": 0.976,
      "angle": 315.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 23,
      "x": -1.513,
      "y": 0.989,
      "angle": 45.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 24,
      "x": -0.263,
      "y": -0.577,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 0.268,
      "y": -0.578,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 26,
      "x": 0.004,
      "y": -0.495,
      "layer": 5,
      "closed_by": [
        24,
        25
      ]
    },
    {
      "id": 27,
      "x": 0.002,
      "y": -0.104,
      "layer": 4,
      "closed_by": [
        26
      ]
    },
    {
      "id": 28,
      "x": 0.002,
      "y": 0.54,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 29,
      "x": 0.55,
      "y": -0.493,
      "layer": 5,
      "closed_by": [
        25
      ]
    },
    {
      "id": 30,
      "x": -0.545,
      "y": -0.495,
      "layer": 5,
      "closed_by": [
        24
      ]
    },
    {
      "id": 31,
      "x": 1.514,
      "y": -0.532,
      "angle": 45.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 1.105,
      "y": 0.298,
      "layer": 4,
      "closed_by": [
        22,
        33
      ]
    },
    {
      "id": 33,
      "x": 1.105,
      "y": -0.107,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 34,
      "x": 0.814,
      "y": -0.104,
      "layer": 3,
      "closed_by": [
        29,
        32
      ]
    },
    {
      "id": 35,
      "x": 0.782,
      "y": 0.703,
      "layer": 3,
      "closed_by": [
        16,
        32
      ]
    },
    {
      "id": 36,
      "x": 0.002,
      "y": 0.222,
      "layer": 3,
      "closed_by": [
        12,
        16,
        27
      ]
    },
    {
      "id": 37,
      "x": -1.723,
      "y": -0.537,
      "angle": 45.0,
      "layer": 5,
      "closed_by": [
        50
      ]
    },
    {
      "id": 38,
      "x": 0.55,
      "y": -0.104,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 39,
      "x": 0.55,
      "y": 0.707,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 40,
      "x": 0.333,
      "y": -0.104,
      "layer": 1,
      "closed_by": [
        28,
        38
      ]
    },
    {
      "id": 41,
      "x": 0.331,
      "y": 0.703,
      "layer": 1,
      "closed_by": [
        28,
        39
      ]
    },
    {
      "id": 42,
      "x": -0.331,
      "y": -0.104,
      "layer": 1,
      "closed_by": [
        28,
        43
      ]
    },
    {
      "id": 43,
      "x": -0.537,
      "y": -0.104,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": -0.813,
      "y": -0.108,
      "layer": 3,
      "closed_by": [
        30,
        46
      ]
    },
    {
      "id": 45,
      "x": -1.105,
      "y": -0.112,
      "layer": 5,
      "closed_by": [
        50
      ]
    },
    {
      "id": 46,
      "x": -1.105,
      "y": 0.3,
      "layer": 4,
      "closed_by": [
        23,
        45
      ]
    },
    {
      "id": 47,
      "x": -0.785,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        12,
        46
      ]
    },
    {
      "id": 48,
      "x": -0.545,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 49,
      "x": -0.328,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        28,
        48
      ]
    },
    {
      "id": 50,
      "x": -1.508,
      "y": -0.541,
      "angle": 315.0,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 39,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 20.5,
    "star2": 68.2,
    "star3": 10.7
  }
}