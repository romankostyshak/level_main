{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.55,
      "y": 0.059,
      "layer": 2,
      "closed_by": [
        49,
        32
      ]
    },
    {
      "id": 50,
      "x": -0.544,
      "y": 0.057,
      "layer": 2,
      "closed_by": [
        48,
        35
      ]
    },
    {
      "id": 49,
      "x": 0.333,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 48,
      "x": -0.333,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 47,
      "x": 0.333,
      "y": 0.059,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 46,
      "x": 0.893,
      "y": 0.944,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 45,
      "x": -0.892,
      "y": 0.944,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 44,
      "x": 1.131,
      "y": 0.46,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 43,
      "x": -1.133,
      "y": 0.46,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 42,
      "x": 1.133,
      "y": 0.068,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 41,
      "x": -1.136,
      "y": 0.142,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 40,
      "x": 1.133,
      "y": -0.254,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 39,
      "x": -1.133,
      "y": -0.256,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 38,
      "x": 1.128,
      "y": -0.572,
      "layer": 5,
      "closed_by": [
        27
      ]
    },
    {
      "id": 37,
      "x": -1.133,
      "y": -0.572,
      "layer": 5,
      "closed_by": [
        26
      ]
    },
    {
      "id": 36,
      "x": -0.333,
      "y": 0.059,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -0.333,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 34,
      "x": -0.333,
      "y": 0.46,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 33,
      "x": -0.333,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 32,
      "x": 0.333,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 31,
      "x": 0.333,
      "y": 0.46,
      "layer": 4,
      "closed_by": [
        47
      ]
    },
    {
      "id": 30,
      "x": 0.333,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        51
      ]
    },
    {
      "id": 27,
      "x": 1.345,
      "y": -0.572,
      "layer": 6,
      "closed_by": [
        25
      ]
    },
    {
      "id": 26,
      "x": -1.347,
      "y": -0.572,
      "layer": 6,
      "closed_by": [
        24
      ]
    },
    {
      "id": 25,
      "x": 1.343,
      "y": -0.256,
      "layer": 7,
      "closed_by": [
        23
      ]
    },
    {
      "id": 24,
      "x": -1.345,
      "y": -0.256,
      "layer": 7,
      "closed_by": [
        22
      ]
    },
    {
      "id": 23,
      "x": 1.347,
      "y": 0.07,
      "layer": 8,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -1.345,
      "y": 0.136,
      "layer": 8,
      "closed_by": []
    },
    {
      "id": 21,
      "x": 1.909,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        20
      ]
    },
    {
      "id": 28,
      "x": -1.901,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        19
      ]
    },
    {
      "id": 20,
      "x": 2.144,
      "y": -0.572,
      "layer": 2,
      "closed_by": [
        18
      ]
    },
    {
      "id": 19,
      "x": -2.144,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        15
      ]
    },
    {
      "id": 18,
      "x": 1.911,
      "y": -0.337,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 15,
      "x": -1.906,
      "y": -0.259,
      "layer": 3,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 34,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 20.0,
    "star2": 69.6,
    "star3": 9.7
  }
}