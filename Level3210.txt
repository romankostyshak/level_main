{
  "layout_cards": [
    {
      "id": 15,
      "x": 0.467,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 16,
      "x": -0.465,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 17,
      "x": -0.3,
      "y": 0.781,
      "layer": 2,
      "closed_by": [
        16,
        27
      ]
    },
    {
      "id": 18,
      "x": 0.307,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        15,
        22
      ]
    },
    {
      "id": 19,
      "x": 1.024,
      "y": -0.115,
      "angle": 315.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 20,
      "x": 0.75,
      "y": -0.573,
      "layer": 1,
      "closed_by": [
        21
      ]
    },
    {
      "id": 21,
      "x": 0.474,
      "y": -0.126,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 22,
      "x": 0.476,
      "y": 0.079,
      "angle": 315.0,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 23,
      "x": 1.019,
      "y": 0.1,
      "angle": 45.0,
      "layer": 5,
      "closed_by": [
        19
      ]
    },
    {
      "id": 24,
      "x": 0.75,
      "y": 0.541,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 25,
      "x": -0.759,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        26
      ]
    },
    {
      "id": 26,
      "x": -0.469,
      "y": -0.136,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        27
      ]
    },
    {
      "id": 27,
      "x": -0.472,
      "y": 0.075,
      "angle": 45.0,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 28,
      "x": -0.754,
      "y": 0.541,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 29,
      "x": -1.021,
      "y": 0.071,
      "angle": 315.0,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 30,
      "x": -1.024,
      "y": -0.133,
      "angle": 45.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 1.483,
      "y": -0.462,
      "angle": 315.0,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 32,
      "x": 2.305,
      "y": -0.495,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -1.822,
      "y": -0.008,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 34,
      "x": 1.48,
      "y": 0.428,
      "angle": 45.0,
      "layer": 1,
      "closed_by": [
        37
      ]
    },
    {
      "id": 35,
      "x": -1.48,
      "y": 0.421,
      "angle": 315.0,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 36,
      "x": -1.483,
      "y": -0.455,
      "angle": 45.0,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 37,
      "x": 1.827,
      "y": 0.46,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 38,
      "x": 1.827,
      "y": -0.497,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 39,
      "x": -1.822,
      "y": 0.465,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 40,
      "x": -1.824,
      "y": -0.497,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 41,
      "x": 1.827,
      "y": 0.003,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 42,
      "x": 2.065,
      "y": 0.003,
      "layer": 4,
      "closed_by": [
        44,
        45
      ]
    },
    {
      "id": 43,
      "x": -2.065,
      "y": -0.008,
      "layer": 4,
      "closed_by": [
        46,
        47
      ]
    },
    {
      "id": 44,
      "x": 2.065,
      "y": 0.46,
      "layer": 5,
      "closed_by": [
        48
      ]
    },
    {
      "id": 45,
      "x": 2.065,
      "y": -0.497,
      "layer": 5,
      "closed_by": [
        32
      ]
    },
    {
      "id": 46,
      "x": -2.065,
      "y": 0.465,
      "layer": 5,
      "closed_by": [
        49
      ]
    },
    {
      "id": 47,
      "x": -2.065,
      "y": -0.497,
      "layer": 5,
      "closed_by": [
        50
      ]
    },
    {
      "id": 48,
      "x": 2.309,
      "y": 0.462,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 49,
      "x": -2.305,
      "y": 0.462,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 50,
      "x": -2.305,
      "y": -0.499,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 51,
      "x": 0.002,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        17,
        18
      ]
    }
  ],
  "cards_in_layout_amount": 37,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 34.6,
    "star2": 61.7,
    "star3": 3.0
  }
}