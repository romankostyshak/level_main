{
  "layout_cards": [
    {
      "id": 51,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        50,
        34
      ]
    },
    {
      "id": 50,
      "x": -0.303,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        49,
        35
      ]
    },
    {
      "id": 49,
      "x": -0.49,
      "y": -0.256,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 48,
      "x": 0.495,
      "y": -0.256,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 47,
      "x": 1.026,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 46,
      "x": -1.023,
      "y": 0.703,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 45,
      "x": -1.024,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 44,
      "x": 1.424,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        30,
        25
      ]
    },
    {
      "id": 43,
      "x": -1.419,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        45,
        31
      ]
    },
    {
      "id": 42,
      "x": 1.616,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 41,
      "x": 1.026,
      "y": -0.497,
      "layer": 1,
      "closed_by": [
        28
      ]
    },
    {
      "id": 40,
      "x": 1.616,
      "y": -0.497,
      "layer": 1,
      "closed_by": [
        28
      ]
    },
    {
      "id": 39,
      "x": -1.024,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 38,
      "x": -1.61,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 37,
      "x": 0.833,
      "y": 0.216,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -0.833,
      "y": 0.216,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -0.492,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 34,
      "x": 0.307,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        48,
        33
      ]
    },
    {
      "id": 33,
      "x": 0.493,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 32,
      "x": -1.424,
      "y": 0.703,
      "layer": 2,
      "closed_by": [
        46,
        31
      ]
    },
    {
      "id": 31,
      "x": -1.613,
      "y": 0.2,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 1.616,
      "y": 0.216,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -1.613,
      "y": -0.495,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 28,
      "x": 1.419,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        47,
        30
      ]
    },
    {
      "id": 27,
      "x": -1.026,
      "y": -0.499,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 26,
      "x": 1.026,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 25,
      "x": 1.029,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        37
      ]
    }
  ],
  "cards_in_layout_amount": 27,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 62.4,
    "star2": 26.0,
    "star3": 11.0
  }
}