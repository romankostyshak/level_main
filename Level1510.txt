{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.414,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        50,
        49
      ]
    },
    {
      "id": 50,
      "x": 0.652,
      "y": 0.781,
      "layer": 2,
      "closed_by": [
        45,
        39
      ]
    },
    {
      "id": 49,
      "x": 0.652,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        47,
        39
      ]
    },
    {
      "id": 48,
      "x": -0.412,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        41,
        40
      ]
    },
    {
      "id": 47,
      "x": 0.333,
      "y": -0.261,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 46,
      "x": -0.333,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 45,
      "x": 0.333,
      "y": 0.856,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 44,
      "x": -0.331,
      "y": 0.861,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 43,
      "x": 0.001,
      "y": -0.333,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 42,
      "x": -0.944,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 41,
      "x": -0.652,
      "y": 0.781,
      "layer": 2,
      "closed_by": [
        44,
        42
      ]
    },
    {
      "id": 40,
      "x": -0.656,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        46,
        42
      ]
    },
    {
      "id": 39,
      "x": 0.948,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 38,
      "x": 1.212,
      "y": 0.944,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 35,
      "x": -1.212,
      "y": -0.333,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 34,
      "x": 1.268,
      "y": 0.864,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 31,
      "x": -1.268,
      "y": -0.256,
      "layer": 5,
      "closed_by": [
        27
      ]
    },
    {
      "id": 30,
      "x": 1.35,
      "y": 0.782,
      "layer": 6,
      "closed_by": [
        26
      ]
    },
    {
      "id": 27,
      "x": -1.345,
      "y": -0.177,
      "layer": 6,
      "closed_by": [
        25
      ]
    },
    {
      "id": 26,
      "x": 1.455,
      "y": 0.298,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 25,
      "x": -1.455,
      "y": 0.303,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 24,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 23,
      "y": 0.782,
      "layer": 5,
      "closed_by": [
        21,
        20
      ]
    },
    {
      "id": 22,
      "x": 0.001,
      "y": -0.18,
      "layer": 5,
      "closed_by": [
        21,
        20
      ]
    },
    {
      "id": 21,
      "x": 0.268,
      "y": 0.293,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 20,
      "x": -0.256,
      "y": 0.296,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 26,
  "cards_in_deck_amount": 26,
  "layers_in_level": 7,
  "open_cards": 4,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 68.7,
    "star2": 29.5,
    "star3": 1.2
  }
}