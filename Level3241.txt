{
  "layout_cards": [
    {
      "id": 16,
      "y": 0.224,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 19,
      "x": -1.825,
      "y": 0.54,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 20,
      "x": 1.825,
      "y": 0.537,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 21,
      "x": 1.378,
      "y": -0.017,
      "layer": 6,
      "closed_by": [
        20
      ]
    },
    {
      "id": 22,
      "x": -1.621,
      "y": -0.418,
      "layer": 5,
      "closed_by": [
        37
      ]
    },
    {
      "id": 23,
      "x": -1.621,
      "y": 0.391,
      "layer": 5,
      "closed_by": [
        37
      ]
    },
    {
      "id": 24,
      "x": 1.623,
      "y": -0.425,
      "layer": 5,
      "closed_by": [
        21
      ]
    },
    {
      "id": 25,
      "x": 1.626,
      "y": 0.388,
      "layer": 5,
      "closed_by": [
        21
      ]
    },
    {
      "id": 26,
      "x": -1.105,
      "y": 0.388,
      "layer": 5,
      "closed_by": [
        37
      ]
    },
    {
      "id": 27,
      "x": 1.108,
      "y": 0.388,
      "layer": 5,
      "closed_by": [
        21
      ]
    },
    {
      "id": 28,
      "x": 1.266,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        25,
        27
      ]
    },
    {
      "id": 29,
      "y": -0.578,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -0.001,
      "y": 1.021,
      "layer": 2,
      "closed_by": [
        35,
        38
      ]
    },
    {
      "id": 31,
      "x": 0.572,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        34,
        35
      ]
    },
    {
      "id": 32,
      "x": -0.569,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        33,
        38
      ]
    },
    {
      "id": 33,
      "x": -0.865,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        36,
        41
      ]
    },
    {
      "id": 34,
      "x": 0.865,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        28,
        42
      ]
    },
    {
      "id": 35,
      "x": 0.333,
      "y": 0.943,
      "layer": 3,
      "closed_by": [
        16,
        42,
        51
      ]
    },
    {
      "id": 36,
      "x": -1.264,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        23,
        26
      ]
    },
    {
      "id": 37,
      "x": -1.358,
      "y": -0.017,
      "layer": 6,
      "closed_by": [
        19
      ]
    },
    {
      "id": 38,
      "x": -0.333,
      "y": 0.944,
      "layer": 3,
      "closed_by": [
        16,
        41,
        51
      ]
    },
    {
      "id": 39,
      "x": -1.105,
      "y": -0.418,
      "layer": 5,
      "closed_by": [
        37
      ]
    },
    {
      "id": 40,
      "x": 1.108,
      "y": -0.418,
      "layer": 5,
      "closed_by": [
        21
      ]
    },
    {
      "id": 41,
      "x": -0.703,
      "y": 0.3,
      "layer": 4,
      "closed_by": [
        26,
        39
      ]
    },
    {
      "id": 42,
      "x": 0.708,
      "y": 0.3,
      "layer": 4,
      "closed_by": [
        27,
        40
      ]
    },
    {
      "id": 43,
      "x": 0.331,
      "y": -0.574,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 44,
      "x": -0.331,
      "y": -0.577,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 45,
      "x": -0.331,
      "y": 0.119,
      "layer": 3,
      "closed_by": [
        16,
        41,
        44
      ]
    },
    {
      "id": 46,
      "x": -0.703,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 47,
      "x": 0.708,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 48,
      "x": -0.33,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 49,
      "x": 0.331,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 50,
      "x": 0.333,
      "y": 0.119,
      "layer": 3,
      "closed_by": [
        16,
        42,
        43
      ]
    },
    {
      "id": 51,
      "x": -0.001,
      "y": 1.039,
      "layer": 4,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 34,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 47.6,
    "star2": 41.2,
    "star3": 10.5
  }
}