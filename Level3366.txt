{
  "layout_cards": [
    {
      "x": 1.575,
      "y": 1.052,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 1,
      "x": 1.577,
      "y": -0.587,
      "layer": 1,
      "closed_by": [
        17
      ]
    },
    {
      "id": 2,
      "x": 1.577,
      "y": 0.231,
      "layer": 1,
      "closed_by": [
        17,
        31
      ]
    },
    {
      "id": 3,
      "x": 1.049,
      "y": 1.052,
      "layer": 1,
      "closed_by": [
        30,
        31
      ]
    },
    {
      "id": 4,
      "x": 1.049,
      "y": -0.587,
      "layer": 1,
      "closed_by": [
        17,
        18
      ]
    },
    {
      "id": 5,
      "x": 1.049,
      "y": 0.231,
      "layer": 1,
      "closed_by": [
        17,
        18,
        30,
        31
      ]
    },
    {
      "id": 6,
      "x": 0.521,
      "y": 1.057,
      "layer": 1,
      "closed_by": [
        27,
        30
      ]
    },
    {
      "id": 7,
      "x": 0.522,
      "y": -0.586,
      "layer": 1,
      "closed_by": [
        18,
        19
      ]
    },
    {
      "id": 8,
      "x": 0.522,
      "y": 0.237,
      "layer": 1,
      "closed_by": [
        18,
        19,
        27,
        30
      ]
    },
    {
      "id": 9,
      "x": -0.001,
      "y": 1.054,
      "layer": 1,
      "closed_by": [
        26,
        27
      ]
    },
    {
      "id": 10,
      "y": -0.586,
      "layer": 1,
      "closed_by": [
        19,
        20
      ]
    },
    {
      "id": 11,
      "x": 0.002,
      "y": 0.231,
      "layer": 1,
      "closed_by": [
        19,
        20,
        26,
        27
      ]
    },
    {
      "id": 12,
      "x": -0.523,
      "y": 1.052,
      "layer": 1,
      "closed_by": [
        25,
        26
      ]
    },
    {
      "id": 13,
      "x": -0.521,
      "y": 0.231,
      "layer": 1,
      "closed_by": [
        20,
        21,
        25,
        26
      ]
    },
    {
      "id": 14,
      "x": -0.521,
      "y": -0.587,
      "layer": 1,
      "closed_by": [
        20,
        21
      ]
    },
    {
      "id": 15,
      "x": -0.689,
      "y": 0.141,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 16,
      "x": -1.159,
      "y": 0.828,
      "layer": 2,
      "closed_by": [
        38,
        41
      ]
    },
    {
      "id": 17,
      "x": 1.457,
      "y": 0.012,
      "layer": 2,
      "closed_by": [
        29,
        33
      ]
    },
    {
      "id": 18,
      "x": 0.935,
      "y": 0.008,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 19,
      "x": 0.414,
      "y": -0.178,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 20,
      "x": -0.108,
      "y": -0.178,
      "layer": 2,
      "closed_by": [
        35,
        36
      ]
    },
    {
      "id": 21,
      "x": -0.633,
      "y": -0.178,
      "layer": 2,
      "closed_by": [
        28,
        36
      ]
    },
    {
      "id": 22,
      "x": -1.159,
      "y": 0.008,
      "layer": 2,
      "closed_by": [
        28,
        39,
        41
      ]
    },
    {
      "id": 23,
      "x": -1.682,
      "y": 0.008,
      "layer": 2,
      "closed_by": [
        39,
        42
      ]
    },
    {
      "id": 24,
      "x": -1.684,
      "y": 0.828,
      "layer": 2,
      "closed_by": [
        38,
        45
      ]
    },
    {
      "id": 25,
      "x": -0.634,
      "y": 0.638,
      "layer": 2,
      "closed_by": [
        28,
        36,
        40,
        41
      ]
    },
    {
      "id": 26,
      "x": -0.112,
      "y": 0.638,
      "layer": 2,
      "closed_by": [
        35,
        36,
        37,
        40
      ]
    },
    {
      "id": 27,
      "x": 0.409,
      "y": 0.638,
      "layer": 2,
      "closed_by": [
        35,
        37
      ]
    },
    {
      "id": 28,
      "x": -0.689,
      "y": -0.108,
      "layer": 3,
      "closed_by": [
        15
      ]
    },
    {
      "id": 29,
      "x": 0.986,
      "y": -0.063,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 0.93,
      "y": 0.828,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 31,
      "x": 1.45,
      "y": 0.827,
      "layer": 2,
      "closed_by": [
        32,
        34
      ]
    },
    {
      "id": 32,
      "x": 1.496,
      "y": 0.898,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 1.506,
      "y": -0.059,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 0.972,
      "y": 0.898,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 0.361,
      "y": -0.104,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 36,
      "x": -0.165,
      "y": -0.108,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 37,
      "x": 0.356,
      "y": 0.708,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 38,
      "x": -1.205,
      "y": 0.901,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -1.205,
      "y": -0.059,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 40,
      "x": -0.165,
      "y": 0.708,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 41,
      "x": -0.689,
      "y": 0.708,
      "layer": 3,
      "closed_by": [
        15
      ]
    },
    {
      "id": 42,
      "x": -1.73,
      "y": -0.059,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 43,
      "x": 0.36,
      "y": 0.455,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 44,
      "x": -0.164,
      "y": 0.298,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 45,
      "x": -1.73,
      "y": 0.898,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 46,
      "x": -1.567,
      "y": -0.587,
      "layer": 1,
      "closed_by": [
        22,
        23
      ]
    },
    {
      "id": 47,
      "x": -1.567,
      "y": 0.231,
      "layer": 1,
      "closed_by": [
        16,
        22,
        23,
        24
      ]
    },
    {
      "id": 48,
      "x": -1.57,
      "y": 1.054,
      "layer": 1,
      "closed_by": [
        16,
        24
      ]
    },
    {
      "id": 49,
      "x": -1.042,
      "y": -0.586,
      "layer": 1,
      "closed_by": [
        21,
        22
      ]
    },
    {
      "id": 50,
      "x": -1.042,
      "y": 0.236,
      "layer": 1,
      "closed_by": [
        16,
        21,
        22,
        25
      ]
    },
    {
      "id": 51,
      "x": -1.046,
      "y": 1.054,
      "layer": 1,
      "closed_by": [
        16,
        25
      ]
    }
  ],
  "cards_in_layout_amount": 52,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 0.2,
    "star2": 7.2,
    "star3": 92.2
  }
}