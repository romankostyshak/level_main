{
  "layout_cards": [
    {
      "id": 34,
      "x": 0.127,
      "y": -0.086,
      "angle": 60.0,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 35,
      "y": 1.023,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 0.381,
      "y": 0.962,
      "angle": 344.997,
      "layer": 6,
      "closed_by": [
        35
      ]
    },
    {
      "id": 37,
      "x": -0.374,
      "y": 0.962,
      "angle": 15.0,
      "layer": 5,
      "closed_by": [
        35
      ]
    },
    {
      "id": 38,
      "x": -0.712,
      "y": 0.824,
      "angle": 30.0,
      "layer": 4,
      "closed_by": [
        37,
        41
      ]
    },
    {
      "id": 39,
      "y": -0.238,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 0.699,
      "y": 0.824,
      "angle": 330.0,
      "layer": 5,
      "closed_by": [
        36,
        42
      ]
    },
    {
      "id": 41,
      "x": -0.994,
      "y": 0.625,
      "angle": 45.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 42,
      "x": 0.981,
      "y": 0.606,
      "angle": 315.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 43,
      "x": 1.179,
      "y": 0.324,
      "angle": 300.0,
      "layer": 5,
      "closed_by": [
        42
      ]
    },
    {
      "id": 44,
      "x": -1.213,
      "y": 0.344,
      "angle": 60.0,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 45,
      "x": 0.939,
      "y": -0.115,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 46,
      "x": -0.745,
      "y": -0.444,
      "angle": 300.0,
      "layer": 2,
      "closed_by": [
        48,
        51
      ]
    },
    {
      "id": 47,
      "x": 0.74,
      "y": -0.442,
      "angle": 60.0,
      "layer": 3,
      "closed_by": [
        34,
        45
      ]
    },
    {
      "id": 48,
      "x": -0.136,
      "y": -0.096,
      "angle": 300.0,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 49,
      "x": 0.337,
      "y": -0.439,
      "layer": 1,
      "closed_by": [
        47,
        48
      ]
    },
    {
      "id": 50,
      "x": -0.337,
      "y": -0.439,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 51,
      "x": -0.939,
      "y": -0.115,
      "layer": 3,
      "closed_by": [
        44
      ]
    }
  ],
  "cards_in_layout_amount": 18,
  "cards_in_deck_amount": 22,
  "layers_in_level": 7,
  "open_cards": 4,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 62.4,
    "star2": 35.9,
    "star3": 1.4
  }
}