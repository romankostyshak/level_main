{
  "layout_cards": [
    {
      "id": 48,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 46,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        45,
        42,
        43
      ]
    },
    {
      "id": 47,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        46,
        50,
        44
      ]
    },
    {
      "id": 49,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 45,
      "y": 1.019,
      "layer": 5,
      "closed_by": [
        41,
        40
      ]
    },
    {
      "id": 50,
      "x": 0.365,
      "y": -0.368,
      "angle": 340.0,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 44,
      "x": -0.361,
      "y": -0.365,
      "angle": 20.0,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 42,
      "x": -0.465,
      "y": 0.216,
      "layer": 5,
      "closed_by": [
        40,
        38
      ]
    },
    {
      "id": 41,
      "x": 0.467,
      "y": 0.939,
      "layer": 6,
      "closed_by": [
        35
      ]
    },
    {
      "id": 40,
      "x": -0.465,
      "y": 0.939,
      "layer": 6,
      "closed_by": [
        34
      ]
    },
    {
      "id": 38,
      "x": -0.625,
      "y": -0.416,
      "layer": 6,
      "closed_by": [
        34
      ]
    },
    {
      "id": 35,
      "x": 0.625,
      "y": 0.238,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -0.625,
      "y": 0.221,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -1.131,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        30
      ]
    },
    {
      "id": 31,
      "x": 1.268,
      "y": 0.86,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 30,
      "x": -1.266,
      "y": 0.864,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 29,
      "x": 1.213,
      "y": 0.541,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 28,
      "x": -1.207,
      "y": 0.544,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 36,
      "x": 1.266,
      "y": 0.384,
      "layer": 4,
      "closed_by": [
        25
      ]
    },
    {
      "id": 27,
      "x": -1.264,
      "y": 0.381,
      "layer": 4,
      "closed_by": [
        26
      ]
    },
    {
      "id": 33,
      "x": 1.133,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 43,
      "x": 0.465,
      "y": 0.222,
      "layer": 5,
      "closed_by": [
        41,
        39
      ]
    },
    {
      "id": 39,
      "x": 0.625,
      "y": -0.416,
      "layer": 6,
      "closed_by": [
        35
      ]
    },
    {
      "id": 25,
      "x": 1.268,
      "y": -0.096,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -1.264,
      "y": -0.096,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 1.268,
      "y": -0.495,
      "layer": 4,
      "closed_by": [
        25
      ]
    },
    {
      "id": 23,
      "x": -1.266,
      "y": -0.497,
      "layer": 4,
      "closed_by": [
        26
      ]
    },
    {
      "id": 22,
      "x": 1.213,
      "y": -0.574,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 21,
      "x": -1.212,
      "y": -0.574,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 37,
      "x": 1.213,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        36,
        22
      ]
    },
    {
      "id": 20,
      "x": -1.207,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        27,
        21
      ]
    },
    {
      "id": 19,
      "x": 1.345,
      "y": -0.017,
      "layer": 1,
      "closed_by": [
        29,
        37
      ]
    },
    {
      "id": 18,
      "x": -1.345,
      "y": -0.017,
      "layer": 1,
      "closed_by": [
        28,
        20
      ]
    }
  ],
  "cards_in_layout_amount": 33,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 34.3,
    "star2": 60.1,
    "star3": 4.7
  }
}