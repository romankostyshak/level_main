{
  "layout_cards": [
    {
      "id": 5,
      "x": 1.84,
      "y": 0.2,
      "layer": 5,
      "closed_by": [],
      "card_type": 2
    },
    {
      "id": 6,
      "x": 1.478,
      "y": -0.238,
      "layer": 4,
      "closed_by": [
        5
      ]
    },
    {
      "id": 7,
      "x": 2.2,
      "y": -0.238,
      "layer": 4,
      "closed_by": [
        5
      ],
      "card_type": 5
    },
    {
      "id": 8,
      "x": -2.539,
      "y": 0.279,
      "layer": 1,
      "closed_by": [
        23,
        26
      ],
      "card_type": 3
    },
    {
      "id": 9,
      "x": 2.2,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        5
      ]
    },
    {
      "id": 10,
      "x": 0.386,
      "y": 0.34,
      "angle": 30.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 11,
      "x": -0.291,
      "y": 0.059,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 12,
      "x": -2.052,
      "y": -0.5,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 13,
      "x": -1.353,
      "y": 1.019,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 14,
      "x": -1.353,
      "y": -0.5,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 15,
      "x": -1.712,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        13,
        17
      ]
    },
    {
      "id": 16,
      "x": -1.712,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        12,
        14
      ]
    },
    {
      "id": 17,
      "x": -2.052,
      "y": 1.019,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 18,
      "x": 1.48,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        5
      ],
      "card_type": 5
    },
    {
      "id": 19,
      "x": -1.034,
      "y": -0.238,
      "layer": 3,
      "closed_by": [
        14
      ]
    },
    {
      "id": 20,
      "x": -1.034,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        13
      ]
    },
    {
      "id": 21,
      "x": -2.371,
      "y": -0.238,
      "layer": 3,
      "closed_by": [
        12
      ]
    },
    {
      "id": 22,
      "x": -1.233,
      "y": 0.837,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        15,
        20
      ]
    },
    {
      "id": 23,
      "x": -2.19,
      "y": 0.837,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        15,
        37
      ]
    },
    {
      "id": 24,
      "x": -1.432,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 25,
      "x": -1.993,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        23
      ]
    },
    {
      "id": 26,
      "x": -2.19,
      "y": -0.179,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        16,
        21
      ]
    },
    {
      "id": 27,
      "x": -1.993,
      "y": -0.238,
      "layer": 1,
      "closed_by": [
        26
      ]
    },
    {
      "id": 28,
      "x": -1.432,
      "y": -0.238,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 29,
      "x": -0.753,
      "y": 0.1,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        11,
        19,
        20
      ]
    },
    {
      "id": 30,
      "x": -0.554,
      "y": 0.059,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 31,
      "x": 0.004,
      "y": 0.059,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 32,
      "x": 0.143,
      "y": 0.2,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        10,
        11
      ]
    },
    {
      "id": 33,
      "x": -1.233,
      "y": -0.179,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        16,
        19
      ]
    },
    {
      "id": 34,
      "x": 2.505,
      "y": 0.958,
      "layer": 3,
      "closed_by": [
        9
      ]
    },
    {
      "id": 35,
      "x": 2.5,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        7
      ]
    },
    {
      "id": 36,
      "x": 2.506,
      "y": -0.591,
      "angle": 300.0,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 37,
      "x": -2.371,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        17
      ]
    },
    {
      "id": 38,
      "x": 2.505,
      "y": 0.958,
      "angle": 60.0,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 39,
      "x": 2.697,
      "y": -0.238,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 40,
      "x": 2.697,
      "y": 0.62,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 41,
      "x": 2.125,
      "y": 0.958,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 42,
      "x": 2.125,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 43,
      "x": 1.179,
      "y": 0.958,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 44,
      "x": 1.182,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        6
      ]
    },
    {
      "id": 45,
      "x": 1.562,
      "y": 0.958,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 46,
      "x": 0.976,
      "y": 0.62,
      "layer": 1,
      "closed_by": [
        50,
        51
      ]
    },
    {
      "id": 47,
      "x": 1.182,
      "y": -0.578,
      "angle": 60.0,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 48,
      "x": 1.562,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 49,
      "x": 0.98,
      "y": -0.238,
      "layer": 1,
      "closed_by": [
        47,
        51
      ]
    },
    {
      "id": 50,
      "x": 1.179,
      "y": 0.958,
      "angle": 300.0,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 51,
      "x": 0.625,
      "y": 0.479,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        10
      ]
    }
  ],
  "cards_in_layout_amount": 47,
  "cards_in_deck_amount": 30,
  "layers_in_level": 5,
  "open_cards": 7,
  "bonus_cards_on_the_layers": {
    "layer:_4": [
      "+2",
      "+2"
    ]
  },
  "stars_statistic": {
    "star1": 24.3,
    "star2": 70.8,
    "star3": 4.7
  }
}