{
  "layout_cards": [
    {
      "id": 51,
      "y": 0.601,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 49,
      "y": -0.209,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 48,
      "x": 2.549,
      "y": 0.064,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 47,
      "x": -1.613,
      "y": 0.442,
      "layer": 2,
      "closed_by": [
        26
      ]
    },
    {
      "id": 46,
      "x": 2.549,
      "y": 0.446,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 45,
      "x": 0.001,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        49
      ]
    },
    {
      "id": 44,
      "x": -2.493,
      "y": -0.194,
      "layer": 1,
      "closed_by": [
        41
      ]
    },
    {
      "id": 43,
      "x": 1.666,
      "y": 0.064,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -2.496,
      "y": 0.702,
      "layer": 1,
      "closed_by": [
        41
      ]
    },
    {
      "id": 41,
      "x": -2.496,
      "y": 0.442,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 40,
      "x": -2.496,
      "y": 0.061,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -1.613,
      "y": 0.702,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 38,
      "x": -0.777,
      "y": 1.018,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 37,
      "x": -1.042,
      "y": 1.018,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 36,
      "x": -0.379,
      "y": 1.018,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 35,
      "y": 1.018,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 34,
      "x": 0.398,
      "y": 1.018,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 33,
      "x": 0.819,
      "y": 1.018,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 32,
      "x": 1.075,
      "y": 1.018,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 31,
      "x": 1.666,
      "y": 0.444,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 30,
      "x": 2.548,
      "y": -0.194,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 29,
      "x": 2.548,
      "y": 0.703,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 28,
      "x": 1.664,
      "y": 0.703,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 27,
      "x": 1.666,
      "y": -0.194,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 26,
      "x": -1.613,
      "y": 0.061,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 25,
      "x": -1.613,
      "y": -0.194,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 50,
      "y": 0.193,
      "layer": 4,
      "closed_by": [
        51
      ]
    }
  ],
  "cards_in_layout_amount": 27,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 51.7,
    "star2": 41.9,
    "star3": 5.8
  }
}