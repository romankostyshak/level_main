{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.331,
      "y": -0.414,
      "layer": 1,
      "closed_by": [
        49,
        47
      ]
    },
    {
      "id": 50,
      "x": 0.333,
      "y": -0.414,
      "layer": 1,
      "closed_by": [
        48,
        23
      ]
    },
    {
      "id": 49,
      "x": -0.782,
      "y": 0.222,
      "layer": 2,
      "closed_by": [
        44,
        41
      ]
    },
    {
      "id": 48,
      "x": 0.785,
      "y": 0.224,
      "layer": 2,
      "closed_by": [
        43,
        42
      ]
    },
    {
      "id": 47,
      "x": -0.256,
      "y": 0.222,
      "layer": 2,
      "closed_by": [
        44,
        41
      ]
    },
    {
      "id": 46,
      "x": -0.331,
      "y": 0.86,
      "layer": 1,
      "closed_by": [
        49,
        47
      ]
    },
    {
      "id": 45,
      "x": 0.333,
      "y": 0.859,
      "layer": 1,
      "closed_by": [
        48,
        23
      ]
    },
    {
      "id": 44,
      "x": -0.546,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        38,
        33
      ]
    },
    {
      "id": 43,
      "x": 0.546,
      "y": -0.254,
      "layer": 3,
      "closed_by": [
        37,
        34
      ]
    },
    {
      "id": 42,
      "x": 0.546,
      "y": 0.702,
      "layer": 3,
      "closed_by": [
        39,
        36
      ]
    },
    {
      "id": 41,
      "x": -0.546,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        40,
        35
      ]
    },
    {
      "id": 40,
      "x": -0.261,
      "y": 1.023,
      "layer": 4,
      "closed_by": [
        32,
        22
      ]
    },
    {
      "id": 39,
      "x": 0.263,
      "y": 1.021,
      "layer": 4,
      "closed_by": [
        31,
        22
      ]
    },
    {
      "id": 38,
      "x": -0.261,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        29,
        24
      ]
    },
    {
      "id": 37,
      "x": 0.27,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        30,
        24
      ]
    },
    {
      "id": 36,
      "x": 0.785,
      "y": 1.023,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 35,
      "x": -0.785,
      "y": 1.021,
      "layer": 4,
      "closed_by": [
        32
      ]
    },
    {
      "id": 34,
      "x": 0.787,
      "y": -0.574,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 33,
      "x": -0.787,
      "y": -0.574,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 32,
      "x": -0.796,
      "y": 0.74,
      "angle": 340.0,
      "layer": 5,
      "closed_by": [
        26
      ]
    },
    {
      "id": 31,
      "x": 0.796,
      "y": 0.745,
      "angle": 20.0,
      "layer": 5,
      "closed_by": [
        28
      ]
    },
    {
      "id": 30,
      "x": 0.804,
      "y": -0.3,
      "angle": 340.0,
      "layer": 5,
      "closed_by": [
        27
      ]
    },
    {
      "id": 29,
      "x": -0.8,
      "y": -0.3,
      "angle": 20.0,
      "layer": 5,
      "closed_by": [
        25
      ]
    },
    {
      "id": 28,
      "x": 1.001,
      "y": 0.736,
      "angle": 20.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 1.008,
      "y": -0.293,
      "angle": 340.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -1.003,
      "y": 0.736,
      "angle": 340.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 25,
      "x": -1.001,
      "y": -0.303,
      "angle": 20.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 24,
      "y": -0.179,
      "layer": 5,
      "closed_by": [
        20
      ]
    },
    {
      "id": 23,
      "x": 0.259,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        43,
        42,
        24
      ]
    },
    {
      "id": 22,
      "x": -0.006,
      "y": 0.634,
      "layer": 5,
      "closed_by": [
        21
      ]
    },
    {
      "id": 21,
      "x": -0.007,
      "y": 0.799,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 20,
      "y": -0.335,
      "layer": 7,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 32,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 49.3,
    "star2": 34.0,
    "star3": 16.4
  }
}