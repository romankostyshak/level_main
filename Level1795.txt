{
  "layout_cards": [
    {
      "id": 46,
      "y": 0.3,
      "layer": 3,
      "closed_by": [
        43,
        42,
        50,
        44
      ]
    },
    {
      "id": 49,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 47,
      "y": -0.177,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 45,
      "y": -0.495,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 43,
      "x": 0.384,
      "y": -0.172,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 42,
      "x": -0.384,
      "y": -0.177,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 50,
      "x": 0.384,
      "y": 0.702,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 48,
      "y": 0.702,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 44,
      "x": -0.384,
      "y": 0.703,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 41,
      "x": 0.549,
      "y": 0.861,
      "layer": 5,
      "closed_by": [
        35
      ]
    },
    {
      "id": 40,
      "x": -0.545,
      "y": 0.859,
      "layer": 5,
      "closed_by": [
        34
      ]
    },
    {
      "id": 39,
      "x": 0.541,
      "y": -0.335,
      "layer": 5,
      "closed_by": [
        33
      ]
    },
    {
      "id": 38,
      "x": -0.546,
      "y": -0.333,
      "layer": 5,
      "closed_by": [
        32
      ]
    },
    {
      "id": 35,
      "x": 0.735,
      "y": 1.019,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -0.731,
      "y": 1.019,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 0.734,
      "y": -0.497,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -0.731,
      "y": -0.493,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 1.11,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        35
      ]
    },
    {
      "id": 30,
      "x": 1.105,
      "y": -0.18,
      "layer": 5,
      "closed_by": [
        33
      ]
    },
    {
      "id": 29,
      "x": -1.103,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        34
      ]
    },
    {
      "id": 28,
      "x": -1.108,
      "y": -0.172,
      "layer": 5,
      "closed_by": [
        32
      ]
    },
    {
      "id": 36,
      "x": 1.264,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 27,
      "x": 1.268,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 26,
      "x": -1.266,
      "y": 0.703,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 25,
      "x": -1.266,
      "y": -0.177,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 24,
      "x": 1.427,
      "y": 0.86,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 23,
      "x": 1.427,
      "y": -0.335,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 22,
      "x": -1.424,
      "y": 0.861,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 21,
      "x": -1.424,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 37,
      "x": 1.588,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 20,
      "x": 1.585,
      "y": -0.497,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 19,
      "x": -1.585,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 18,
      "x": -1.585,
      "y": -0.497,
      "layer": 2,
      "closed_by": [
        21
      ]
    }
  ],
  "cards_in_layout_amount": 33,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 33.4,
    "star2": 58.9,
    "star3": 7.0
  }
}