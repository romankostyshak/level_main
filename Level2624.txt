{
  "layout_cards": [
    {
      "id": 9,
      "x": -2.144,
      "y": -0.497,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 10,
      "x": -2.144,
      "y": 0.14,
      "layer": 5,
      "closed_by": [
        9
      ]
    },
    {
      "id": 11,
      "x": 2.144,
      "y": 0.14,
      "layer": 5,
      "closed_by": [
        17
      ]
    },
    {
      "id": 12,
      "x": 1.983,
      "y": -0.495,
      "layer": 4,
      "closed_by": [
        11
      ]
    },
    {
      "id": 13,
      "x": -2.144,
      "y": -0.172,
      "layer": 3,
      "closed_by": [
        16
      ]
    },
    {
      "id": 14,
      "x": 2.144,
      "y": -0.1,
      "layer": 3,
      "closed_by": [
        12
      ]
    },
    {
      "id": 15,
      "x": -2.144,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        10
      ]
    },
    {
      "id": 16,
      "x": -1.985,
      "y": -0.493,
      "layer": 4,
      "closed_by": [
        10
      ]
    },
    {
      "id": 17,
      "x": 2.144,
      "y": -0.495,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 18,
      "x": 2.144,
      "y": 0.781,
      "layer": 3,
      "closed_by": [
        11
      ]
    },
    {
      "id": 19,
      "x": -1.988,
      "y": 0.136,
      "layer": 2,
      "closed_by": [
        13,
        15
      ]
    },
    {
      "id": 20,
      "x": 1.985,
      "y": 0.14,
      "layer": 2,
      "closed_by": [
        14,
        18
      ]
    },
    {
      "id": 21,
      "x": 0.925,
      "y": -0.495,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -0.409,
      "y": -0.493,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 0.414,
      "y": -0.497,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 24,
      "x": -0.625,
      "y": 1.019,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 0.625,
      "y": 1.019,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -0.384,
      "y": 1.019,
      "layer": 5,
      "closed_by": [
        24
      ]
    },
    {
      "id": 27,
      "x": 0.386,
      "y": 1.019,
      "layer": 5,
      "closed_by": [
        25
      ]
    },
    {
      "id": 28,
      "x": 0.266,
      "y": 0.86,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 29,
      "x": 0.414,
      "y": -0.096,
      "layer": 5,
      "closed_by": [
        23
      ]
    },
    {
      "id": 30,
      "x": -0.409,
      "y": -0.097,
      "layer": 5,
      "closed_by": [
        22
      ]
    },
    {
      "id": 31,
      "x": 0.518,
      "y": -0.008,
      "angle": 20.0,
      "layer": 4,
      "closed_by": [
        21,
        29
      ]
    },
    {
      "id": 32,
      "x": -0.517,
      "y": -0.017,
      "angle": 340.0,
      "layer": 4,
      "closed_by": [
        30,
        37
      ]
    },
    {
      "id": 33,
      "x": -0.648,
      "y": 0.136,
      "layer": 3,
      "closed_by": [
        32,
        36
      ]
    },
    {
      "id": 34,
      "x": 0.652,
      "y": 0.14,
      "layer": 3,
      "closed_by": [
        28,
        31
      ]
    },
    {
      "id": 35,
      "x": -0.81,
      "y": 0.09,
      "angle": 340.0,
      "layer": 2,
      "closed_by": [
        33,
        42
      ]
    },
    {
      "id": 36,
      "x": -0.259,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        26
      ]
    },
    {
      "id": 37,
      "x": -0.925,
      "y": -0.495,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 0.808,
      "y": 0.097,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 39,
      "x": -1.309,
      "y": 0.731,
      "angle": 340.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 40,
      "x": -1.419,
      "y": 0.73,
      "angle": 25.0,
      "layer": 5,
      "closed_by": [
        39
      ]
    },
    {
      "id": 41,
      "x": -1.312,
      "y": 0.509,
      "angle": 340.0,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 42,
      "x": -1.417,
      "y": 0.5,
      "angle": 24.999,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 43,
      "x": -1.309,
      "y": 0.27,
      "angle": 340.0,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 44,
      "x": -1.411,
      "y": 0.266,
      "angle": 25.0,
      "layer": 1,
      "closed_by": [
        19,
        35,
        43
      ]
    },
    {
      "id": 45,
      "x": 1.421,
      "y": 0.725,
      "angle": 335.0,
      "layer": 5,
      "closed_by": [
        49
      ]
    },
    {
      "id": 46,
      "x": 1.315,
      "y": 0.499,
      "angle": 20.0,
      "layer": 4,
      "closed_by": [
        45
      ]
    },
    {
      "id": 47,
      "x": 1.429,
      "y": 0.481,
      "angle": 335.0,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": 1.312,
      "y": 0.268,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 49,
      "x": 1.315,
      "y": 0.731,
      "angle": 20.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 51,
      "x": 1.429,
      "y": 0.252,
      "angle": 335.0,
      "layer": 1,
      "closed_by": [
        20,
        38,
        48
      ]
    }
  ],
  "cards_in_layout_amount": 42,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 0.9,
    "star2": 31.3,
    "star3": 67.1
  }
}