{
  "layout_cards": [
    {
      "id": 25,
      "x": -1.1,
      "y": 0.479,
      "angle": 60.0,
      "layer": 3,
      "closed_by": [
        32,
        37
      ]
    },
    {
      "id": 26,
      "x": -0.337,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 27,
      "x": 0.337,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        28
      ]
    },
    {
      "id": 28,
      "x": 0.537,
      "y": 0.158,
      "angle": 300.0,
      "layer": 2,
      "closed_by": [
        30,
        48
      ]
    },
    {
      "id": 29,
      "x": -0.537,
      "y": 0.158,
      "angle": 60.0,
      "layer": 2,
      "closed_by": [
        25,
        47
      ]
    },
    {
      "id": 30,
      "x": 1.098,
      "y": 0.479,
      "angle": 300.0,
      "layer": 3,
      "closed_by": [
        31,
        39
      ]
    },
    {
      "id": 31,
      "x": 1.618,
      "y": 0.777,
      "angle": 300.0,
      "layer": 4,
      "closed_by": [
        36,
        42
      ]
    },
    {
      "id": 32,
      "x": -1.639,
      "y": 0.791,
      "angle": 60.0,
      "layer": 4,
      "closed_by": [
        35,
        46
      ],
      "card_type": 3
    },
    {
      "id": 33,
      "x": 2.019,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 34,
      "x": -2.019,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 35,
      "x": -1.419,
      "y": 0.439,
      "layer": 5,
      "closed_by": [
        45
      ]
    },
    {
      "id": 36,
      "x": 1.419,
      "y": 0.439,
      "layer": 5,
      "closed_by": [
        41
      ]
    },
    {
      "id": 37,
      "x": -0.898,
      "y": 0.136,
      "layer": 4,
      "closed_by": [
        44
      ]
    },
    {
      "id": 38,
      "x": -2.019,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        51
      ]
    },
    {
      "id": 39,
      "x": 0.898,
      "y": 0.136,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 40,
      "x": 0.898,
      "y": -0.179,
      "layer": 5,
      "closed_by": [
        41
      ]
    },
    {
      "id": 41,
      "x": 1.258,
      "y": -0.039,
      "angle": 320.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 42,
      "x": 2.019,
      "y": 0.439,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 43,
      "x": 2.019,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 44,
      "x": -0.898,
      "y": -0.179,
      "layer": 5,
      "closed_by": [
        45
      ]
    },
    {
      "id": 45,
      "x": -1.258,
      "y": -0.017,
      "angle": 40.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 46,
      "x": -2.019,
      "y": 0.439,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 47,
      "x": -0.358,
      "y": 0.46,
      "angle": 5.0,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 48,
      "x": 0.367,
      "y": 0.46,
      "angle": 354.997,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 49,
      "x": 2.177,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 50,
      "y": 0.379,
      "layer": 4,
      "closed_by": [],
      "card_type": 2
    },
    {
      "id": 51,
      "x": -2.18,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        46
      ]
    }
  ],
  "cards_in_layout_amount": 27,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 52.0,
    "star2": 45.5,
    "star3": 2.0
  }
}