{
  "layout_cards": [
    {
      "id": 34,
      "x": 1.182,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        29,
        35
      ]
    },
    {
      "id": 37,
      "x": -0.624,
      "y": -0.578,
      "layer": 6,
      "closed_by": [
        26
      ]
    },
    {
      "id": 51,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 38,
      "x": 0.623,
      "y": -0.578,
      "layer": 6,
      "closed_by": [
        27
      ]
    },
    {
      "id": 28,
      "x": -1.182,
      "y": -0.578,
      "layer": 5,
      "closed_by": [
        36
      ]
    },
    {
      "id": 36,
      "x": -1.182,
      "y": -0.017,
      "layer": 6,
      "closed_by": [
        26
      ]
    },
    {
      "id": 33,
      "x": -1.182,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        36,
        39
      ]
    },
    {
      "id": 31,
      "x": -1.182,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 32,
      "x": 1.182,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 29,
      "x": 1.182,
      "y": -0.017,
      "layer": 6,
      "closed_by": [
        27
      ]
    },
    {
      "id": 30,
      "x": 1.182,
      "y": -0.578,
      "layer": 5,
      "closed_by": [
        29
      ]
    },
    {
      "id": 35,
      "x": 0.86,
      "y": 0.859,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -0.859,
      "y": 0.859,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -0.777,
      "y": -0.017,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 0.777,
      "y": -0.017,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 44,
      "x": 0.379,
      "y": -0.017,
      "layer": 4,
      "closed_by": [
        49
      ]
    },
    {
      "id": 50,
      "x": -0.379,
      "y": -0.017,
      "layer": 4,
      "closed_by": [
        48
      ]
    },
    {
      "id": 48,
      "x": -0.379,
      "y": -0.578,
      "layer": 5,
      "closed_by": [
        37
      ]
    },
    {
      "id": 49,
      "x": 0.379,
      "y": -0.578,
      "layer": 5,
      "closed_by": [
        38
      ]
    },
    {
      "id": 47,
      "y": -0.017,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 46,
      "y": 0.537,
      "layer": 3,
      "closed_by": [
        44,
        50,
        43,
        45
      ]
    },
    {
      "id": 42,
      "y": 1.019,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 43,
      "x": -0.379,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        42,
        40
      ]
    },
    {
      "id": 45,
      "x": 0.379,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        42,
        41
      ]
    },
    {
      "id": 41,
      "x": 0.574,
      "y": 1.019,
      "layer": 5,
      "closed_by": [
        35
      ]
    },
    {
      "id": 40,
      "x": -0.574,
      "y": 1.019,
      "layer": 5,
      "closed_by": [
        39
      ]
    }
  ],
  "cards_in_layout_amount": 26,
  "cards_in_deck_amount": 24,
  "layers_in_level": 7,
  "open_cards": 5,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 43.1,
    "star2": 51.9,
    "star3": 4.3
  }
}