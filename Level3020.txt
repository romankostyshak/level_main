{
  "layout_cards": [
    {
      "id": 21,
      "x": 1.748,
      "y": -0.574,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -0.879,
      "y": 0.372,
      "layer": 3,
      "closed_by": [
        27,
        28
      ]
    },
    {
      "id": 23,
      "x": -1.184,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        22,
        26
      ]
    },
    {
      "id": 24,
      "x": -0.569,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        22,
        25
      ]
    },
    {
      "id": 25,
      "x": -0.331,
      "y": 0.381,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 26,
      "x": -1.427,
      "y": 0.381,
      "layer": 3,
      "closed_by": [
        28,
        33
      ]
    },
    {
      "id": 27,
      "x": -0.564,
      "y": -0.017,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 28,
      "x": -1.192,
      "y": -0.013,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 29,
      "x": -1.424,
      "y": -0.416,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -0.331,
      "y": -0.414,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 1.187,
      "y": -0.256,
      "layer": 2,
      "closed_by": [
        35,
        37
      ]
    },
    {
      "id": 32,
      "x": 0.573,
      "y": -0.256,
      "layer": 2,
      "closed_by": [
        36,
        37
      ]
    },
    {
      "id": 33,
      "x": -1.746,
      "y": 0.703,
      "layer": 4,
      "closed_by": [
        49
      ]
    },
    {
      "id": 34,
      "x": 1.746,
      "y": -0.254,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 35,
      "x": 1.424,
      "y": 0.064,
      "layer": 3,
      "closed_by": [
        34,
        39
      ]
    },
    {
      "id": 36,
      "x": 0.333,
      "y": 0.061,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 37,
      "x": 0.875,
      "y": 0.061,
      "layer": 3,
      "closed_by": [
        38,
        39
      ]
    },
    {
      "id": 38,
      "x": 0.574,
      "y": 0.46,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 39,
      "x": 1.184,
      "y": 0.46,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 40,
      "x": 0.333,
      "y": 0.861,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 1.427,
      "y": 0.86,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -2.015,
      "y": 0.381,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 43,
      "x": 0.878,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        31,
        32
      ]
    },
    {
      "id": 44,
      "x": 2.013,
      "y": 0.061,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 45,
      "x": -2.013,
      "y": -0.017,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 46,
      "x": 2.013,
      "y": 0.462,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 47,
      "x": 2.012,
      "y": 0.859,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": -2.009,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 49,
      "x": -1.743,
      "y": 1.018,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -0.879,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        23,
        24
      ]
    }
  ],
  "cards_in_layout_amount": 30,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 46.4,
    "star2": 43.1,
    "star3": 9.8
  }
}