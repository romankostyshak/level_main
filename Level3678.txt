{
  "layout_cards": [
    {
      "id": 18,
      "x": -1.985,
      "y": 0.939,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 19,
      "x": 1.985,
      "y": 0.939,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 20,
      "x": -0.333,
      "y": -0.337,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 21,
      "x": -0.865,
      "y": 0.462,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 22,
      "x": 0.865,
      "y": 0.46,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 23,
      "x": -0.569,
      "y": 0.209,
      "layer": 4,
      "closed_by": [
        20,
        21
      ]
    },
    {
      "id": 24,
      "x": 0.572,
      "y": 0.209,
      "layer": 4,
      "closed_by": [
        22,
        37
      ]
    },
    {
      "id": 25,
      "x": -0.569,
      "y": 1.018,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 26,
      "x": 0.573,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 27,
      "x": -0.003,
      "y": 1.018,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 1.825,
      "y": 0.3,
      "layer": 4,
      "closed_by": [
        19,
        34
      ]
    },
    {
      "id": 29,
      "x": -1.585,
      "y": -0.172,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 30,
      "x": -1.184,
      "y": -0.495,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 31,
      "x": 1.585,
      "y": -0.172,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 32,
      "x": 1.184,
      "y": -0.493,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 33,
      "x": -1.427,
      "y": 0.781,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 1.424,
      "y": 0.781,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -1.131,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        21,
        33
      ]
    },
    {
      "id": 36,
      "x": -1.824,
      "y": 0.224,
      "layer": 4,
      "closed_by": [
        18,
        33
      ]
    },
    {
      "id": 37,
      "x": 0.331,
      "y": -0.337,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 1.133,
      "y": 1.018,
      "layer": 4,
      "closed_by": [
        22,
        34
      ]
    },
    {
      "id": 39,
      "x": 0.865,
      "y": -0.18,
      "layer": 1,
      "closed_by": [
        32,
        45
      ]
    },
    {
      "id": 40,
      "x": 0.001,
      "y": -0.5,
      "layer": 4,
      "closed_by": [
        20,
        37
      ]
    },
    {
      "id": 41,
      "x": -0.865,
      "y": -0.172,
      "layer": 1,
      "closed_by": [
        30,
        48
      ]
    },
    {
      "id": 42,
      "x": -0.865,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        23,
        25,
        35
      ]
    },
    {
      "id": 43,
      "x": 0.865,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        24,
        26,
        38
      ]
    },
    {
      "id": 44,
      "x": -0.333,
      "y": -0.18,
      "layer": 3,
      "closed_by": [
        23,
        40
      ]
    },
    {
      "id": 45,
      "x": 0.703,
      "y": 0.3,
      "layer": 2,
      "closed_by": [
        43,
        50
      ]
    },
    {
      "id": 46,
      "x": 0.331,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        24,
        26,
        45,
        47
      ]
    },
    {
      "id": 47,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        27,
        44,
        50
      ]
    },
    {
      "id": 48,
      "x": -0.703,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        42,
        44
      ]
    },
    {
      "id": 49,
      "x": -0.331,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        23,
        25,
        47,
        48
      ]
    },
    {
      "id": 50,
      "x": 0.333,
      "y": -0.178,
      "layer": 3,
      "closed_by": [
        24,
        40
      ]
    }
  ],
  "cards_in_layout_amount": 33,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 45.7,
    "star2": 51.9,
    "star3": 1.9
  }
}