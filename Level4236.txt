{
  "layout_cards": [
    {
      "id": 20,
      "x": -2.562,
      "y": -0.377,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        21
      ]
    },
    {
      "id": 21,
      "x": -2.398,
      "y": -0.279,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 22,
      "x": 2.4,
      "y": -0.279,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 23,
      "x": -2.18,
      "y": -0.216,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 24,
      "x": 2.177,
      "y": -0.216,
      "layer": 3,
      "closed_by": [
        26
      ],
      "card_type": 3
    },
    {
      "id": 25,
      "x": -1.978,
      "y": -0.216,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 26,
      "x": 1.98,
      "y": -0.216,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 27,
      "x": -1.758,
      "y": -0.279,
      "angle": 330.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 1.319,
      "y": -0.537,
      "angle": 30.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -1.319,
      "y": -0.537,
      "angle": 330.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 30,
      "y": 1.019,
      "layer": 5,
      "closed_by": [],
      "card_type": 2
    },
    {
      "id": 31,
      "x": -1.139,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 32,
      "x": 1.139,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 33,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 34,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 35,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 36,
      "x": 1.758,
      "y": -0.279,
      "angle": 30.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 37,
      "x": 2.569,
      "y": -0.379,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 38,
      "x": 0.638,
      "y": 0.578,
      "angle": 340.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -0.638,
      "y": 0.578,
      "angle": 20.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 0.939,
      "y": 0.439,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 41,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 42,
      "x": -0.939,
      "y": 0.439,
      "angle": 30.0,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 43,
      "x": 1.139,
      "y": 0.379,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 44,
      "x": -1.139,
      "y": 0.379,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 45,
      "x": -0.939,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 46,
      "x": 0.74,
      "y": -0.238,
      "angle": 300.0,
      "layer": 2,
      "closed_by": [
        43,
        50
      ]
    },
    {
      "id": 47,
      "x": -0.74,
      "y": -0.238,
      "angle": 60.0,
      "layer": 2,
      "closed_by": [
        44,
        45
      ]
    },
    {
      "id": 48,
      "x": -0.537,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 49,
      "x": 0.537,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 50,
      "x": 0.939,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        32
      ]
    }
  ],
  "cards_in_layout_amount": 31,
  "cards_in_deck_amount": 24,
  "layers_in_level": 5,
  "open_cards": 7,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 24.3,
    "star2": 71.7,
    "star3": 3.7
  }
}