{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.001,
      "y": 1.024,
      "layer": 1,
      "closed_by": [
        50,
        49
      ]
    },
    {
      "id": 50,
      "x": 0.266,
      "y": 0.305,
      "layer": 2,
      "closed_by": [
        48,
        45
      ]
    },
    {
      "id": 49,
      "x": -0.256,
      "y": 0.303,
      "layer": 2,
      "closed_by": [
        47,
        44
      ]
    },
    {
      "id": 48,
      "x": 0.467,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 47,
      "x": -0.465,
      "y": 1.021,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 46,
      "x": -0.001,
      "y": -0.412,
      "layer": 1,
      "closed_by": [
        50,
        49
      ]
    },
    {
      "id": 45,
      "x": 0.465,
      "y": -0.416,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 44,
      "x": -0.465,
      "y": -0.416,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 43,
      "x": 0.785,
      "y": 0.305,
      "layer": 4,
      "closed_by": [
        40,
        41
      ]
    },
    {
      "id": 42,
      "x": -0.782,
      "y": 0.303,
      "layer": 4,
      "closed_by": [
        38,
        39
      ]
    },
    {
      "id": 40,
      "x": 0.947,
      "y": -0.416,
      "layer": 5,
      "closed_by": [
        10
      ]
    },
    {
      "id": 38,
      "x": -0.944,
      "y": -0.418,
      "layer": 5,
      "closed_by": [
        16
      ]
    },
    {
      "id": 37,
      "x": 1.304,
      "y": 0.305,
      "layer": 4,
      "closed_by": [
        40,
        41
      ]
    },
    {
      "id": 36,
      "x": -1.307,
      "y": 0.305,
      "layer": 4,
      "closed_by": [
        38,
        39
      ]
    },
    {
      "id": 35,
      "x": 1.47,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 34,
      "x": 1.473,
      "y": -0.416,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 33,
      "x": -1.473,
      "y": -0.416,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 32,
      "x": -1.47,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 31,
      "x": 1.827,
      "y": 0.303,
      "layer": 2,
      "closed_by": [
        35,
        34
      ]
    },
    {
      "id": 30,
      "x": -1.827,
      "y": 0.303,
      "layer": 2,
      "closed_by": [
        33,
        32
      ]
    },
    {
      "id": 29,
      "x": 1.993,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        31,
        23
      ]
    },
    {
      "id": 28,
      "x": 1.996,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        31,
        23
      ]
    },
    {
      "id": 27,
      "x": -1.996,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        30,
        22
      ]
    },
    {
      "id": 26,
      "x": -2.002,
      "y": -0.414,
      "layer": 1,
      "closed_by": [
        30,
        22
      ]
    },
    {
      "id": 23,
      "x": 2.351,
      "y": 0.307,
      "layer": 2,
      "closed_by": [
        21,
        24
      ]
    },
    {
      "id": 22,
      "x": -2.358,
      "y": 0.307,
      "layer": 2,
      "closed_by": [
        20,
        19
      ]
    },
    {
      "id": 21,
      "x": 2.519,
      "y": 1.018,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 24,
      "x": 2.523,
      "y": -0.414,
      "layer": 3,
      "closed_by": [
        15
      ]
    },
    {
      "id": 20,
      "x": -2.522,
      "y": 1.021,
      "layer": 3,
      "closed_by": [
        14
      ]
    },
    {
      "id": 19,
      "x": -2.519,
      "y": -0.414,
      "layer": 3,
      "closed_by": [
        13
      ]
    },
    {
      "id": 18,
      "x": 2.138,
      "y": 0.777,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 15,
      "x": 2.134,
      "y": -0.172,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 14,
      "x": -2.151,
      "y": 0.777,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 13,
      "x": -2.148,
      "y": -0.179,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -0.949,
      "y": 1.019,
      "layer": 5,
      "closed_by": [
        12
      ]
    },
    {
      "id": 12,
      "x": -0.722,
      "y": 0.736,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 16,
      "x": -0.722,
      "y": -0.141,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 11,
      "x": 0.731,
      "y": 0.744,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 10,
      "x": 0.725,
      "y": -0.136,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 0.949,
      "y": 1.019,
      "layer": 5,
      "closed_by": [
        11
      ]
    }
  ],
  "cards_in_layout_amount": 40,
  "cards_in_deck_amount": 32,
  "stars_statistic": {
    "star1": 9.5,
    "star2": 67.9,
    "star3": 22.3
  }
}