{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.268,
      "y": 0.657,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 50,
      "x": -0.268,
      "y": 0.657,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 49,
      "x": 1.516,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        37
      ]
    },
    {
      "id": 45,
      "x": 0.518,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        51
      ]
    },
    {
      "id": 44,
      "x": -1.521,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 42,
      "x": 0.518,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        26
      ]
    },
    {
      "id": 41,
      "x": 0.735,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 40,
      "x": -0.731,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 39,
      "x": 1.518,
      "y": -0.582,
      "layer": 1,
      "closed_by": [
        34
      ]
    },
    {
      "id": 38,
      "x": -0.503,
      "y": 0.856,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 37,
      "x": 1.758,
      "y": 0.856,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 36,
      "x": -1.725,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 35,
      "x": -1.725,
      "y": 0.86,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 34,
      "x": 1.758,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 33,
      "x": 0.736,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 32,
      "x": -0.731,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 31,
      "x": -0.5,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 30,
      "x": -0.268,
      "y": -0.156,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 29,
      "x": -1.519,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 26,
      "x": 0.268,
      "y": -0.156,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 25,
      "x": 0.003,
      "y": 0.246,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 48,
      "x": 2.019,
      "y": 0.657,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "x": 2.019,
      "y": 0.238,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 46,
      "x": 2.019,
      "y": -0.158,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 27,
      "x": -1.998,
      "y": 0.238,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 43,
      "x": -1.998,
      "y": 0.657,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 28,
      "x": -1.998,
      "y": -0.158,
      "layer": 3,
      "closed_by": [
        27
      ]
    }
  ],
  "cards_in_layout_amount": 27,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 53.9,
    "star2": 40.2,
    "star3": 5.5
  }
}