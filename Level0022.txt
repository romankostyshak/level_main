{
  "layout_cards": [
    {
      "id": 51,
      "x": -1.23,
      "y": 0.782,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 50,
      "x": 1.23,
      "y": -0.178,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 49,
      "x": 0.97,
      "y": 0.782,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 48,
      "x": 1.23,
      "y": 0.782,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 47,
      "x": -0.972,
      "y": 0.782,
      "layer": 1,
      "closed_by": [
        51
      ]
    },
    {
      "id": 46,
      "x": 0.97,
      "y": -0.178,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 45,
      "x": -0.972,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": -1.233,
      "y": -0.178,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 43,
      "y": -0.261,
      "layer": 1,
      "closed_by": [
        42,
        41
      ]
    },
    {
      "id": 42,
      "x": 0.333,
      "y": -0.179,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 41,
      "x": -0.328,
      "y": -0.179,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 0.333,
      "y": 0.782,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 39,
      "y": 0.861,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -0.333,
      "y": 0.782,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 37,
      "x": 1.85,
      "y": 0.782,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 1.853,
      "y": -0.178,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -1.85,
      "y": 0.782,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -1.85,
      "y": -0.178,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 2.108,
      "y": 0.782,
      "layer": 1,
      "closed_by": [
        37
      ]
    },
    {
      "id": 32,
      "x": 2.115,
      "y": -0.178,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 31,
      "x": -2.111,
      "y": 0.781,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 30,
      "x": -2.115,
      "y": -0.178,
      "layer": 1,
      "closed_by": [
        34
      ]
    }
  ],
  "cards_in_layout_amount": 22,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 63.7,
    "star2": 19.4,
    "star3": 16.5
  }
}