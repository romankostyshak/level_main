{
  "layout_cards": [
    {
      "id": 18,
      "x": -1.751,
      "y": -0.462,
      "angle": 35.0,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 20,
      "x": 1.753,
      "y": -0.465,
      "angle": 325.0,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 21,
      "x": 2.213,
      "y": 0.416,
      "angle": 295.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -2.115,
      "y": 0.462,
      "angle": 50.0,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 23,
      "x": 2.13,
      "y": 0.469,
      "angle": 310.0,
      "layer": 2,
      "closed_by": [
        21
      ]
    },
    {
      "id": 24,
      "x": -2.015,
      "y": 0.483,
      "angle": 35.0,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 25,
      "x": 2.029,
      "y": 0.492,
      "angle": 325.0,
      "layer": 1,
      "closed_by": [
        23
      ]
    },
    {
      "id": 26,
      "x": -0.629,
      "y": 0.824,
      "angle": 15.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 0.629,
      "y": 0.822,
      "angle": 345.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 0.986,
      "y": 0.572,
      "angle": 335.0,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 29,
      "x": -1.192,
      "y": 0.256,
      "angle": 35.0,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 30,
      "x": 1.187,
      "y": 0.252,
      "angle": 325.0,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 31,
      "x": -1.355,
      "y": -0.068,
      "angle": 35.0,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 32,
      "x": 1.353,
      "y": -0.064,
      "angle": 325.0,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 33,
      "x": -1.396,
      "y": -0.384,
      "angle": 45.0,
      "layer": 1,
      "closed_by": [
        18,
        31
      ]
    },
    {
      "id": 34,
      "x": 1.391,
      "y": -0.381,
      "angle": 315.0,
      "layer": 1,
      "closed_by": [
        20,
        32
      ]
    },
    {
      "id": 35,
      "x": -0.002,
      "y": 0.944,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -0.981,
      "y": 0.574,
      "angle": 25.0,
      "layer": 4,
      "closed_by": [
        26
      ]
    },
    {
      "id": 37,
      "x": -2.207,
      "y": 0.414,
      "angle": 65.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 0.333,
      "y": 0.861,
      "layer": 4,
      "closed_by": [
        27,
        35
      ]
    },
    {
      "id": 39,
      "x": -0.331,
      "y": 0.86,
      "layer": 4,
      "closed_by": [
        26,
        35
      ]
    },
    {
      "id": 40,
      "x": -0.541,
      "y": 0.652,
      "angle": 25.0,
      "layer": 3,
      "closed_by": [
        36,
        39
      ]
    },
    {
      "id": 41,
      "x": 0.54,
      "y": 0.652,
      "angle": 335.0,
      "layer": 3,
      "closed_by": [
        28,
        38
      ]
    },
    {
      "id": 42,
      "x": -0.813,
      "y": 0.379,
      "angle": 35.0,
      "layer": 2,
      "closed_by": [
        29,
        40
      ]
    },
    {
      "id": 43,
      "x": -1.003,
      "y": -0.013,
      "angle": 45.0,
      "layer": 1,
      "closed_by": [
        31,
        42
      ]
    },
    {
      "id": 44,
      "x": 0.814,
      "y": 0.375,
      "angle": 325.0,
      "layer": 2,
      "closed_by": [
        30,
        41
      ]
    },
    {
      "id": 45,
      "x": 0.158,
      "y": -0.194,
      "angle": 330.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 46,
      "x": -0.172,
      "y": -0.194,
      "angle": 30.0,
      "layer": 4,
      "closed_by": [
        45
      ]
    },
    {
      "id": 47,
      "x": -0.18,
      "y": -0.377,
      "angle": 45.0,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": 0.187,
      "y": -0.384,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 49,
      "x": 0.004,
      "y": -0.063,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 50,
      "x": 1.006,
      "y": -0.008,
      "angle": 315.0,
      "layer": 1,
      "closed_by": [
        32,
        44
      ]
    }
  ],
  "cards_in_layout_amount": 32,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 42.4,
    "star2": 52.5,
    "star3": 4.9
  }
}