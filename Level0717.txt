{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.333,
      "y": 0.224,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 50,
      "x": 0.323,
      "y": 0.912,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 49,
      "x": -0.573,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 48,
      "x": 0.79,
      "y": -0.414,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 47,
      "x": -0.786,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        49
      ]
    },
    {
      "id": 46,
      "x": 1.029,
      "y": -0.414,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 45,
      "x": -1.029,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 44,
      "x": 1.271,
      "y": -0.414,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 43,
      "x": -1.271,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 42,
      "x": -0.331,
      "y": 0.216,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 0.574,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 40,
      "x": 0.939,
      "y": 0.685,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        30,
        29
      ]
    },
    {
      "id": 39,
      "x": -0.943,
      "y": 0.68,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        37,
        28
      ]
    },
    {
      "id": 38,
      "x": -0.319,
      "y": 0.912,
      "angle": 9.998,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 37,
      "x": -1.184,
      "y": 0.592,
      "angle": 9.998,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 36,
      "x": 1.447,
      "y": 0.586,
      "angle": 9.998,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 35,
      "x": -1.427,
      "y": 0.592,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 34,
      "x": 1.761,
      "y": 0.674,
      "angle": 20.0,
      "layer": 4,
      "closed_by": [
        32
      ]
    },
    {
      "id": 33,
      "x": -1.723,
      "y": 0.675,
      "angle": 340.0,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 32,
      "x": 2.069,
      "y": 0.814,
      "angle": 30.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -2.015,
      "y": 0.81,
      "angle": 330.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 1.184,
      "y": 0.582,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 29,
      "x": 0.66,
      "y": 0.819,
      "angle": 340.0,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 28,
      "x": -0.646,
      "y": 0.822,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 27,
      "y": -0.177,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 26,
      "x": 0.001,
      "y": -0.082,
      "layer": 4,
      "closed_by": [
        51,
        42
      ]
    },
    {
      "id": 25,
      "y": 0.86,
      "layer": 4,
      "closed_by": [
        51,
        42
      ]
    }
  ],
  "cards_in_layout_amount": 27,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 66.3,
    "star2": 25.2,
    "star3": 8.1
  }
}