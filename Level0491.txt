{
  "layout_cards": [
    {
      "id": 51,
      "x": -1.332,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 50,
      "x": -1.072,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        51
      ]
    },
    {
      "id": 49,
      "x": 1.072,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 48,
      "x": 1.332,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        47
      ]
    },
    {
      "id": 46,
      "x": -0.814,
      "y": -0.119,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 45,
      "y": 0.163,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "y": -0.254,
      "layer": 4,
      "closed_by": [
        29,
        27
      ]
    },
    {
      "id": 43,
      "y": 0.583,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 42,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 41,
      "x": -1.592,
      "y": 1.019,
      "layer": 5,
      "closed_by": [
        23
      ]
    },
    {
      "id": 40,
      "x": 0.814,
      "y": -0.079,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 39,
      "x": 0.814,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 38,
      "x": 0.814,
      "y": 0.657,
      "layer": 2,
      "closed_by": [
        49,
        37
      ]
    },
    {
      "id": 37,
      "x": 0.814,
      "y": 0.136,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 36,
      "x": 0.814,
      "y": -0.416,
      "layer": 5,
      "closed_by": [
        25
      ]
    },
    {
      "id": 35,
      "x": -0.814,
      "y": -0.416,
      "layer": 5,
      "closed_by": [
        28
      ]
    },
    {
      "id": 34,
      "x": -0.814,
      "y": 0.136,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 33,
      "x": -0.814,
      "y": 0.657,
      "layer": 2,
      "closed_by": [
        50,
        34
      ]
    },
    {
      "id": 32,
      "x": -0.814,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 28,
      "x": -0.652,
      "y": -0.495,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -0.259,
      "y": -0.416,
      "layer": 5,
      "closed_by": [
        28,
        26
      ]
    },
    {
      "id": 27,
      "x": 0.268,
      "y": -0.412,
      "layer": 5,
      "closed_by": [
        26,
        25
      ]
    },
    {
      "id": 26,
      "x": 0.001,
      "y": -0.365,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 0.652,
      "y": -0.495,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 47,
      "x": 1.595,
      "y": 1.019,
      "layer": 5,
      "closed_by": [
        24
      ]
    },
    {
      "id": 24,
      "x": 1.595,
      "y": 0.623,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 23,
      "x": -1.59,
      "y": 0.624,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 27,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 67.8,
    "star2": 15.9,
    "star3": 15.7
  }
}