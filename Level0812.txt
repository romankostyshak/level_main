{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.331,
      "y": -0.416,
      "layer": 1,
      "closed_by": [
        48,
        45
      ]
    },
    {
      "id": 50,
      "x": 0.333,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        49,
        45
      ]
    },
    {
      "id": 49,
      "x": 0.6,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 48,
      "x": -0.592,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 47,
      "x": 0.87,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 46,
      "x": -0.851,
      "y": -0.34,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 45,
      "y": -0.256,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 44,
      "x": -1.187,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        26
      ]
    },
    {
      "id": 43,
      "x": 0.004,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 42,
      "y": -0.256,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 41,
      "y": -0.578,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 1.771,
      "y": 0.54,
      "layer": 5,
      "closed_by": [
        19
      ]
    },
    {
      "id": 39,
      "x": -1.6,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 38,
      "x": 1.608,
      "y": 0.702,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 37,
      "x": -1.029,
      "y": 0.699,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 1.031,
      "y": 0.699,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -0.49,
      "y": 0.699,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 0.492,
      "y": 0.698,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -1.317,
      "y": 0.781,
      "layer": 3,
      "closed_by": [
        39,
        37
      ]
    },
    {
      "id": 32,
      "x": -0.734,
      "y": 0.781,
      "layer": 3,
      "closed_by": [
        37,
        35
      ]
    },
    {
      "id": 31,
      "x": 1.312,
      "y": 0.781,
      "layer": 3,
      "closed_by": [
        38,
        36
      ]
    },
    {
      "id": 30,
      "x": 0.731,
      "y": 0.782,
      "layer": 3,
      "closed_by": [
        36,
        34
      ]
    },
    {
      "id": 29,
      "x": -1.042,
      "y": 0.861,
      "layer": 2,
      "closed_by": [
        33,
        32
      ]
    },
    {
      "id": 28,
      "x": 1.199,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 27,
      "x": 1.524,
      "y": -0.495,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -1.506,
      "y": -0.495,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 1.031,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        31,
        30
      ]
    },
    {
      "id": 24,
      "x": -1.312,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 23,
      "x": 1.312,
      "y": 0.944,
      "layer": 1,
      "closed_by": [
        25
      ]
    },
    {
      "id": 22,
      "x": -0.731,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 21,
      "x": 0.731,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        25
      ]
    },
    {
      "id": 20,
      "x": -1.771,
      "y": 0.545,
      "layer": 5,
      "closed_by": [
        18
      ]
    },
    {
      "id": 19,
      "x": 1.932,
      "y": 0.381,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 18,
      "x": -1.934,
      "y": 0.384,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 34,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 21.4,
    "star2": 70.0,
    "star3": 7.8
  }
}