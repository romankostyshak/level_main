{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.273,
      "y": -0.254,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 50,
      "x": 0.004,
      "y": 0.833,
      "layer": 3,
      "closed_by": [
        44,
        45
      ]
    },
    {
      "id": 49,
      "x": -0.523,
      "y": 0.068,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 48,
      "x": 0.799,
      "y": -0.252,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 47,
      "x": -0.518,
      "y": 1.021,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 46,
      "x": 0.537,
      "y": 0.07,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": 0.275,
      "y": 0.442,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 43,
      "x": 0.27,
      "y": 0.441,
      "layer": 2,
      "closed_by": [
        50,
        46,
        37
      ]
    },
    {
      "id": 42,
      "x": 0.799,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 41,
      "x": -0.791,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 40,
      "x": -0.791,
      "y": -0.252,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 39,
      "x": 0.8,
      "y": 0.439,
      "layer": 2,
      "closed_by": [
        46,
        37
      ]
    },
    {
      "id": 38,
      "x": -0.27,
      "y": 0.439,
      "layer": 2,
      "closed_by": [
        50,
        49,
        47
      ]
    },
    {
      "id": 37,
      "x": 0.527,
      "y": 1.021,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 36,
      "x": 0.004,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        43,
        38
      ]
    },
    {
      "id": 35,
      "x": 1.773,
      "y": 1.047,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 34,
      "x": 1.771,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        24
      ]
    },
    {
      "id": 33,
      "x": -1.773,
      "y": 0.697,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 32,
      "x": -1.776,
      "y": 0.238,
      "layer": 1,
      "closed_by": [
        33,
        27
      ]
    },
    {
      "id": 31,
      "x": 1.771,
      "y": 0.685,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 30,
      "x": 1.774,
      "y": 0.231,
      "layer": 1,
      "closed_by": [
        31,
        24
      ]
    },
    {
      "id": 29,
      "x": -1.773,
      "y": 0.256,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 28,
      "x": -1.771,
      "y": 1.047,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 27,
      "x": -1.771,
      "y": -0.162,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 26,
      "x": -1.773,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        27
      ]
    },
    {
      "id": 25,
      "x": 1.771,
      "y": 0.254,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 1.771,
      "y": -0.164,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 23,
      "x": -0.266,
      "y": -0.254,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 22,
      "x": -0.791,
      "y": 0.439,
      "layer": 2,
      "closed_by": [
        49,
        47
      ]
    },
    {
      "id": 45,
      "x": -0.266,
      "y": 0.441,
      "layer": 4,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 30,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 35.2,
    "star2": 55.3,
    "star3": 8.9
  }
}