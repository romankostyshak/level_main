{
  "layout_cards": [
    {
      "id": 47,
      "x": -0.331,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 45,
      "x": 0.001,
      "y": -0.261,
      "layer": 3,
      "closed_by": [
        41,
        40
      ]
    },
    {
      "id": 44,
      "x": -0.002,
      "y": 0.861,
      "layer": 3,
      "closed_by": [
        43,
        42
      ]
    },
    {
      "id": 41,
      "x": 0.381,
      "y": -0.335,
      "layer": 4,
      "closed_by": [
        26
      ]
    },
    {
      "id": 40,
      "x": -0.384,
      "y": -0.331,
      "layer": 4,
      "closed_by": [
        25
      ]
    },
    {
      "id": 39,
      "x": 0.975,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 36,
      "x": 0.972,
      "y": -0.331,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 35,
      "x": -0.975,
      "y": -0.331,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 34,
      "x": -0.975,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 43,
      "x": 0.381,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 46,
      "x": 0.333,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 50,
      "x": -0.331,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 33,
      "x": 1.026,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 32,
      "x": -1.024,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 31,
      "x": 1.105,
      "y": 0.3,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 30,
      "x": -1.103,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 8,
      "x": -0.331,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        47,
        50
      ]
    },
    {
      "id": 49,
      "x": 0.333,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        46,
        48
      ]
    },
    {
      "id": 48,
      "x": 0.333,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 42,
      "x": -0.381,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 29,
      "x": -0.972,
      "y": 0.298,
      "layer": 4,
      "closed_by": [
        28,
        25
      ]
    },
    {
      "id": 37,
      "x": 0.972,
      "y": 0.3,
      "layer": 4,
      "closed_by": [
        27,
        26
      ]
    },
    {
      "id": 28,
      "x": -0.587,
      "y": 0.777,
      "layer": 5,
      "closed_by": [
        38
      ]
    },
    {
      "id": 27,
      "x": 0.596,
      "y": 0.777,
      "layer": 5,
      "closed_by": [
        22
      ]
    },
    {
      "id": 26,
      "x": 0.6,
      "y": -0.179,
      "layer": 5,
      "closed_by": [
        23
      ]
    },
    {
      "id": 25,
      "x": -0.62,
      "y": -0.178,
      "layer": 5,
      "closed_by": [
        24
      ]
    },
    {
      "id": 24,
      "x": -0.698,
      "y": -0.208,
      "angle": 45.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 0.685,
      "y": -0.216,
      "angle": 315.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 22,
      "x": 0.694,
      "y": 0.819,
      "angle": 45.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -0.671,
      "y": 0.809,
      "angle": 315.0,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 30,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 53.4,
    "star2": 26.8,
    "star3": 19.1
  }
}