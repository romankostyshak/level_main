{
  "layout_cards": [
    {
      "id": 13,
      "x": -2.065,
      "y": 0.46,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 14,
      "x": 2.062,
      "y": 0.458,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 15,
      "x": -2.065,
      "y": -0.414,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 18,
      "x": 2.069,
      "y": -0.414,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 19,
      "y": -0.172,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 20,
      "x": -0.776,
      "y": 0.064,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 21,
      "x": -0.261,
      "y": -0.013,
      "layer": 4,
      "closed_by": [
        19
      ]
    },
    {
      "id": 22,
      "x": 0.263,
      "y": -0.017,
      "layer": 4,
      "closed_by": [
        19
      ]
    },
    {
      "id": 23,
      "x": -2.144,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        13
      ]
    },
    {
      "id": 24,
      "x": 2.144,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        14
      ]
    },
    {
      "id": 25,
      "x": -2.144,
      "y": 0.059,
      "layer": 3,
      "closed_by": [
        13,
        15
      ]
    },
    {
      "id": 26,
      "x": 2.148,
      "y": 0.061,
      "layer": 3,
      "closed_by": [
        14,
        18
      ]
    },
    {
      "id": 27,
      "x": -1.605,
      "y": -0.497,
      "layer": 3,
      "closed_by": [
        15
      ]
    },
    {
      "id": 28,
      "x": -1.904,
      "y": -0.351,
      "layer": 2,
      "closed_by": [
        25,
        27
      ]
    },
    {
      "id": 29,
      "x": 1.906,
      "y": -0.36,
      "layer": 2,
      "closed_by": [
        26,
        36
      ]
    },
    {
      "id": 30,
      "x": -1.292,
      "y": -0.347,
      "layer": 2,
      "closed_by": [
        27,
        32
      ]
    },
    {
      "id": 31,
      "x": 1.292,
      "y": -0.354,
      "layer": 2,
      "closed_by": [
        33,
        36
      ]
    },
    {
      "id": 32,
      "x": -1.067,
      "y": 0.303,
      "layer": 3,
      "closed_by": [
        20
      ]
    },
    {
      "id": 33,
      "x": 1.07,
      "y": 0.305,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 34,
      "x": -0.544,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        20,
        21
      ]
    },
    {
      "id": 35,
      "x": 0.546,
      "y": 0.223,
      "layer": 3,
      "closed_by": [
        22,
        37
      ]
    },
    {
      "id": 36,
      "x": 1.613,
      "y": -0.495,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 37,
      "x": 0.777,
      "y": 0.063,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 38,
      "y": 0.141,
      "layer": 3,
      "closed_by": [
        21,
        22
      ]
    },
    {
      "id": 39,
      "x": 0.263,
      "y": 0.3,
      "layer": 2,
      "closed_by": [
        35,
        38
      ]
    },
    {
      "id": 40,
      "x": -0.261,
      "y": 0.3,
      "layer": 2,
      "closed_by": [
        34,
        38
      ]
    },
    {
      "id": 41,
      "x": 0.777,
      "y": 0.381,
      "layer": 2,
      "closed_by": [
        33,
        35
      ]
    },
    {
      "id": 42,
      "x": -0.773,
      "y": 0.381,
      "layer": 2,
      "closed_by": [
        32,
        34
      ]
    },
    {
      "id": 43,
      "x": 1.294,
      "y": 0.458,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 44,
      "x": 1.904,
      "y": 0.46,
      "layer": 2,
      "closed_by": [
        24,
        26
      ]
    },
    {
      "id": 45,
      "x": -1.294,
      "y": 0.462,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 46,
      "x": 1.613,
      "y": -0.093,
      "layer": 1,
      "closed_by": [
        14,
        29,
        31,
        43,
        44
      ]
    },
    {
      "id": 47,
      "x": 1.613,
      "y": 0.938,
      "layer": 1,
      "closed_by": [
        14,
        43,
        44
      ]
    },
    {
      "id": 48,
      "x": -1.61,
      "y": -0.093,
      "layer": 1,
      "closed_by": [
        28,
        30,
        45,
        50
      ]
    },
    {
      "id": 49,
      "x": -1.61,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        45,
        50
      ]
    },
    {
      "id": 50,
      "x": -1.904,
      "y": 0.46,
      "layer": 2,
      "closed_by": [
        23,
        25
      ]
    }
  ],
  "cards_in_layout_amount": 36,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 8.0,
    "star2": 65.9,
    "star3": 25.6
  }
}