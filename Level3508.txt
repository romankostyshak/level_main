{
  "layout_cards": [
    {
      "id": 17,
      "x": 1.108,
      "y": -0.103,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 18,
      "x": -1.105,
      "y": 0.699,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 19,
      "x": -1.105,
      "y": -0.103,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 20,
      "y": 0.216,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 21,
      "x": -1.824,
      "y": -0.337,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -1.825,
      "y": 0.939,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 1.825,
      "y": -0.34,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 1.825,
      "y": 0.939,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 25,
      "x": -1.666,
      "y": 0.703,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 26,
      "x": 1.664,
      "y": -0.097,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 27,
      "x": 1.664,
      "y": 0.703,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 28,
      "x": -1.628,
      "y": -0.333,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 29,
      "x": -1.628,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 30,
      "x": -1.424,
      "y": 0.303,
      "layer": 3,
      "closed_by": [
        18,
        19,
        25,
        33
      ]
    },
    {
      "id": 31,
      "x": -0.809,
      "y": -0.101,
      "layer": 4,
      "closed_by": [
        19,
        32
      ]
    },
    {
      "id": 32,
      "x": -0.326,
      "y": 0.221,
      "layer": 5,
      "closed_by": [
        20
      ]
    },
    {
      "id": 33,
      "x": -1.666,
      "y": -0.101,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 34,
      "x": 1.108,
      "y": 0.703,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -0.809,
      "y": 0.703,
      "layer": 4,
      "closed_by": [
        18,
        32
      ]
    },
    {
      "id": 36,
      "x": -0.652,
      "y": 0.541,
      "layer": 3,
      "closed_by": [
        31,
        35
      ]
    },
    {
      "id": 37,
      "x": -0.492,
      "y": 0.381,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 38,
      "x": -0.33,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        37
      ]
    },
    {
      "id": 39,
      "x": 1.628,
      "y": -0.335,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 40,
      "x": 1.628,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 41,
      "x": 1.422,
      "y": 0.3,
      "layer": 3,
      "closed_by": [
        17,
        26,
        27,
        34
      ]
    },
    {
      "id": 42,
      "x": 0.331,
      "y": 0.216,
      "layer": 5,
      "closed_by": [
        20
      ]
    },
    {
      "id": 43,
      "x": 0.81,
      "y": 0.703,
      "layer": 4,
      "closed_by": [
        34,
        42
      ]
    },
    {
      "id": 44,
      "x": 0.652,
      "y": 0.541,
      "layer": 3,
      "closed_by": [
        43,
        47
      ]
    },
    {
      "id": 45,
      "x": 0.493,
      "y": 0.379,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 46,
      "x": 0.333,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 47,
      "x": 0.813,
      "y": -0.104,
      "layer": 4,
      "closed_by": [
        17,
        42
      ]
    },
    {
      "id": 48,
      "x": -1.026,
      "y": 0.059,
      "layer": 2,
      "closed_by": [
        30,
        36
      ]
    },
    {
      "id": 49,
      "x": 1.026,
      "y": 0.063,
      "layer": 2,
      "closed_by": [
        41,
        44
      ]
    },
    {
      "id": 50,
      "x": 1.108,
      "y": 0.223,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 51,
      "x": -1.108,
      "y": 0.224,
      "layer": 1,
      "closed_by": [
        48
      ]
    }
  ],
  "cards_in_layout_amount": 35,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 52.5,
    "star2": 40.0,
    "star3": 6.7
  }
}