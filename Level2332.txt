{
  "layout_cards": [
    {
      "id": 28,
      "x": -1.105,
      "y": -0.414,
      "layer": 1,
      "closed_by": [
        32,
        35
      ]
    },
    {
      "id": 29,
      "x": -0.414,
      "y": -0.416,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 30,
      "x": 0.414,
      "y": -0.416,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 31,
      "x": 0.735,
      "y": -0.416,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -0.731,
      "y": -0.416,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 1.105,
      "y": -0.414,
      "layer": 1,
      "closed_by": [
        31,
        34
      ]
    },
    {
      "id": 34,
      "x": 1.534,
      "y": 0.143,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -1.531,
      "y": 0.143,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 1.901,
      "y": 0.143,
      "layer": 1,
      "closed_by": [
        34,
        38,
        48
      ]
    },
    {
      "id": 37,
      "x": -1.904,
      "y": 0.143,
      "layer": 1,
      "closed_by": [
        35,
        39,
        47
      ]
    },
    {
      "id": 38,
      "x": 2.305,
      "y": 0.546,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -2.305,
      "y": 0.546,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 2.703,
      "y": 0.544,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 41,
      "x": -2.703,
      "y": -0.5,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 42,
      "x": -2.703,
      "y": 0.544,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 43,
      "x": -1.105,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        35,
        45
      ]
    },
    {
      "id": 44,
      "x": 1.105,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        34,
        46
      ]
    },
    {
      "id": 45,
      "x": -0.73,
      "y": 0.698,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 46,
      "x": 0.734,
      "y": 0.699,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 47,
      "x": -2.302,
      "y": -0.495,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 48,
      "x": 2.328,
      "y": -0.495,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 49,
      "x": 2.703,
      "y": -0.5,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 50,
      "x": -0.337,
      "y": 0.698,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 51,
      "x": 0.335,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        46
      ]
    }
  ],
  "level_mechanic": 2,
  "free_cards_from_stickers": [
    28,
    29,
    32,
    35,
    37,
    39,
    41,
    42,
    43,
    45,
    47,
    50
  ],
  "cards_in_layout_amount": 24,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 56.4,
    "star2": 37.4,
    "star3": 5.7
  }
}