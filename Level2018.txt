{
  "layout_cards": [
    {
      "id": 47,
      "x": 0.55,
      "y": 0.059,
      "layer": 2,
      "closed_by": [
        43,
        50
      ]
    },
    {
      "id": 46,
      "x": -0.537,
      "y": 0.057,
      "layer": 2,
      "closed_by": [
        44,
        45
      ]
    },
    {
      "id": 43,
      "x": 0.333,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 44,
      "x": -0.333,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 39,
      "x": 0.333,
      "y": 0.059,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 0.893,
      "y": 0.944,
      "layer": 1,
      "closed_by": [
        34
      ]
    },
    {
      "id": 35,
      "x": -0.892,
      "y": 0.944,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 34,
      "x": 1.131,
      "y": 0.46,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 33,
      "x": -1.133,
      "y": 0.46,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 32,
      "x": 1.133,
      "y": 0.068,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 31,
      "x": -1.136,
      "y": 0.142,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 30,
      "x": 1.133,
      "y": -0.254,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 29,
      "x": -1.133,
      "y": -0.256,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 28,
      "x": 1.128,
      "y": -0.572,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -1.133,
      "y": -0.569,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 40,
      "x": -0.333,
      "y": 0.059,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 45,
      "x": -0.333,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 42,
      "x": -0.333,
      "y": 0.46,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 49,
      "x": -0.333,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 50,
      "x": 0.333,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 41,
      "x": 0.333,
      "y": 0.46,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 48,
      "x": 0.333,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        47
      ]
    }
  ],
  "cards_in_layout_amount": 22,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 41.2,
    "star2": 3.7,
    "star3": 54.7
  }
}