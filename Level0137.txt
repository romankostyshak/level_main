{
  "layout_cards": [
    {
      "id": 51,
      "x": -2.625,
      "y": 0.609,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 50,
      "x": -1.965,
      "y": -0.527,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 49,
      "x": -0.279,
      "y": 0.781,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 48,
      "x": 0.289,
      "y": 0.781,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 47,
      "x": -2.595,
      "y": 0.6,
      "angle": 300.0,
      "layer": 1,
      "closed_by": [
        51,
        29
      ]
    },
    {
      "id": 46,
      "x": 1.324,
      "y": -0.179,
      "angle": 60.0,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 45,
      "x": -1.952,
      "y": -0.535,
      "angle": 300.0,
      "layer": 1,
      "closed_by": [
        50,
        24
      ]
    },
    {
      "id": 44,
      "x": 2.582,
      "y": 0.595,
      "angle": 60.0,
      "layer": 1,
      "closed_by": [
        41,
        30
      ]
    },
    {
      "id": 43,
      "x": -1.304,
      "y": -0.162,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 42,
      "x": -0.518,
      "y": 0.481,
      "layer": 2,
      "closed_by": [
        49
      ]
    },
    {
      "id": 41,
      "x": 2.611,
      "y": 0.597,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 40,
      "x": -2.296,
      "y": 0.039,
      "angle": 29.999,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -0.411,
      "y": 0.119,
      "layer": 1,
      "closed_by": [
        42,
        26,
        22
      ]
    },
    {
      "id": 38,
      "x": 0.409,
      "y": 0.119,
      "layer": 1,
      "closed_by": [
        35,
        25,
        22
      ]
    },
    {
      "id": 37,
      "x": 0.409,
      "y": 1.024,
      "layer": 1,
      "closed_by": [
        35,
        22
      ]
    },
    {
      "id": 36,
      "x": -0.411,
      "y": 1.024,
      "layer": 1,
      "closed_by": [
        42,
        22
      ]
    },
    {
      "id": 35,
      "x": 0.522,
      "y": 0.481,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 34,
      "x": 1.96,
      "y": 0.975,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        20
      ]
    },
    {
      "id": 33,
      "x": 1.294,
      "y": -0.172,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        20
      ]
    },
    {
      "id": 32,
      "x": 2.286,
      "y": 0.032,
      "angle": 330.0,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 1.957,
      "y": -0.532,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 30,
      "x": 1.98,
      "y": 0.944,
      "angle": 60.0,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 29,
      "x": -1.988,
      "y": 0.953,
      "angle": 300.0,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 28,
      "x": -1.967,
      "y": 0.985,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 27,
      "x": 1.929,
      "y": -0.527,
      "angle": 60.0,
      "layer": 1,
      "closed_by": [
        46,
        31
      ]
    },
    {
      "id": 26,
      "x": -0.412,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 25,
      "x": 0.409,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 24,
      "x": -1.327,
      "y": -0.172,
      "angle": 300.0,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 23,
      "y": -0.377,
      "layer": 3,
      "closed_by": [],
      "card_type": 2
    },
    {
      "id": 22,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        49,
        48
      ],
      "card_type": 3
    },
    {
      "id": 21,
      "x": -1.631,
      "y": 0.407,
      "angle": 30.0,
      "layer": 4,
      "closed_by": [],
      "effect_id": 5
    },
    {
      "id": 20,
      "x": 1.623,
      "y": 0.397,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [],
      "effect_id": 5
    }
  ],
  "cards_in_layout_amount": 32,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 31.6,
    "star2": 60.0,
    "star3": 7.4
  }
}