{
  "layout_cards": [
    {
      "id": 49,
      "x": 0.335,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 48,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        50,
        43,
        42,
        44
      ]
    },
    {
      "id": 47,
      "x": -0.333,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 46,
      "x": 0.333,
      "y": -0.414,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 45,
      "x": -0.328,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 50,
      "x": 0.333,
      "y": -0.414,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 43,
      "x": 0.333,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 42,
      "x": -0.331,
      "y": 1.021,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 41,
      "x": 0.55,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 39,
      "x": 0.476,
      "y": 0.875,
      "angle": 330.0,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 38,
      "x": -0.479,
      "y": 0.883,
      "angle": 30.0,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 35,
      "x": 0.642,
      "y": 0.722,
      "angle": 305.0,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 34,
      "x": -0.657,
      "y": 0.722,
      "angle": 55.0,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 33,
      "x": 0.865,
      "y": -0.177,
      "layer": 5,
      "closed_by": [
        29,
        31
      ]
    },
    {
      "id": 30,
      "x": -0.864,
      "y": 0.412,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 44,
      "x": -0.323,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 40,
      "x": -0.628,
      "y": -0.335,
      "layer": 4,
      "closed_by": [
        32
      ]
    },
    {
      "id": 29,
      "x": 0.87,
      "y": -0.414,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 28,
      "x": -0.865,
      "y": -0.414,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -0.865,
      "y": -0.179,
      "layer": 5,
      "closed_by": [
        30,
        28
      ]
    },
    {
      "id": 31,
      "x": 0.869,
      "y": 0.412,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 1.452,
      "y": -0.418,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 1.455,
      "y": 0.412,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -1.452,
      "y": -0.416,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 25,
      "x": -1.452,
      "y": 0.412,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 1.5,
      "y": -0.017,
      "layer": 5,
      "closed_by": [
        36,
        27
      ]
    },
    {
      "id": 37,
      "x": 1.666,
      "y": -0.017,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 22,
      "x": 1.577,
      "y": -0.017,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 23,
      "x": -1.498,
      "y": 0.017,
      "layer": 5,
      "closed_by": [
        26,
        25
      ]
    },
    {
      "id": 21,
      "x": -1.585,
      "y": 0.017,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 20,
      "x": -1.664,
      "y": 0.017,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 19,
      "x": 1.748,
      "y": -0.017,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 18,
      "x": -1.743,
      "y": 0.017,
      "layer": 2,
      "closed_by": [
        20
      ]
    }
  ],
  "cards_in_layout_amount": 33,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 40.7,
    "star2": 54.9,
    "star3": 3.7
  }
}