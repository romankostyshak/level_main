{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.384,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        49
      ]
    },
    {
      "id": 50,
      "x": -0.384,
      "y": 1.023,
      "layer": 6,
      "closed_by": [
        41
      ]
    },
    {
      "id": 49,
      "x": -0.384,
      "y": 0.62,
      "layer": 5,
      "closed_by": [
        50,
        40,
        38
      ]
    },
    {
      "id": 48,
      "x": -0.384,
      "y": -0.179,
      "layer": 3,
      "closed_by": [
        51,
        36,
        34
      ]
    },
    {
      "id": 47,
      "x": -0.384,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        48,
        40,
        38
      ]
    },
    {
      "id": 46,
      "x": 0.384,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 45,
      "x": 0.384,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": 0.384,
      "y": -0.179,
      "layer": 3,
      "closed_by": [
        46,
        36,
        35
      ]
    },
    {
      "id": 43,
      "x": 0.384,
      "y": 0.62,
      "layer": 5,
      "closed_by": [
        42,
        40,
        39
      ]
    },
    {
      "id": 42,
      "x": 0.384,
      "y": 1.019,
      "layer": 6,
      "closed_by": [
        41
      ]
    },
    {
      "id": 41,
      "y": 1.039,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 40,
      "y": 0.216,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 39,
      "x": 0.546,
      "y": 0.209,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -0.546,
      "y": 0.209,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 37,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        47,
        45
      ]
    },
    {
      "id": 36,
      "y": -0.597,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 0.546,
      "y": -0.601,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -0.546,
      "y": -0.6,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -1.105,
      "y": -0.601,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 1.105,
      "y": 0.209,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -1.105,
      "y": 0.209,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 1.108,
      "y": 1.029,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 29,
      "x": -1.105,
      "y": 1.026,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 28,
      "x": 0.916,
      "y": 1.029,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 27,
      "x": -0.919,
      "y": 1.026,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 26,
      "x": 1.098,
      "y": -0.597,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 1.266,
      "y": -0.6,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 24,
      "x": 1.264,
      "y": 0.209,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 23,
      "x": -1.266,
      "y": -0.597,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 22,
      "x": -1.266,
      "y": 0.209,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 21,
      "x": 1.264,
      "y": 1.026,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 20,
      "x": -1.266,
      "y": 1.026,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 19,
      "x": 1.429,
      "y": 1.021,
      "layer": 2,
      "closed_by": [
        21
      ]
    },
    {
      "id": 18,
      "x": 1.427,
      "y": 0.209,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 17,
      "x": 1.424,
      "y": -0.597,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 16,
      "x": -1.427,
      "y": 1.023,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 15,
      "x": -1.427,
      "y": 0.209,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 14,
      "x": -1.424,
      "y": -0.596,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 13,
      "x": 1.582,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        19
      ]
    },
    {
      "id": 12,
      "x": 1.585,
      "y": 0.207,
      "layer": 1,
      "closed_by": [
        18
      ]
    },
    {
      "id": 11,
      "x": 1.582,
      "y": -0.597,
      "layer": 1,
      "closed_by": [
        17
      ]
    },
    {
      "id": 10,
      "x": -1.582,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        16
      ]
    },
    {
      "id": 9,
      "x": -1.585,
      "y": 0.209,
      "layer": 1,
      "closed_by": [
        15
      ]
    },
    {
      "id": 8,
      "x": -1.585,
      "y": -0.592,
      "layer": 1,
      "closed_by": [
        14
      ]
    }
  ],
  "cards_in_layout_amount": 44,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 0.0,
    "star2": 7.4,
    "star3": 91.9
  }
}