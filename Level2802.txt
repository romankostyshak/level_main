{
  "layout_cards": [
    {
      "id": 8,
      "x": -2.546,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 9,
      "x": 0.574,
      "y": -0.097,
      "layer": 3,
      "closed_by": [
        16
      ]
    },
    {
      "id": 10,
      "x": -0.259,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        12,
        49
      ]
    },
    {
      "id": 11,
      "x": 0.259,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        14,
        49
      ]
    },
    {
      "id": 12,
      "x": -0.573,
      "y": -0.093,
      "layer": 2,
      "closed_by": [
        13,
        20
      ]
    },
    {
      "id": 13,
      "x": -0.573,
      "y": 0.541,
      "layer": 3,
      "closed_by": [
        17
      ]
    },
    {
      "id": 14,
      "x": 0.574,
      "y": 0.541,
      "layer": 2,
      "closed_by": [
        9,
        19
      ]
    },
    {
      "id": 15,
      "x": 1.026,
      "y": -0.574,
      "layer": 5,
      "closed_by": [
        28
      ]
    },
    {
      "id": 16,
      "x": 0.787,
      "y": -0.335,
      "layer": 4,
      "closed_by": [
        15
      ]
    },
    {
      "id": 17,
      "x": -0.786,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        18
      ]
    },
    {
      "id": 18,
      "x": -1.024,
      "y": 1.019,
      "layer": 5,
      "closed_by": [
        29
      ]
    },
    {
      "id": 19,
      "x": 0.573,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 20,
      "x": -0.573,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 21,
      "x": 0.814,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        25
      ]
    },
    {
      "id": 22,
      "x": 2.177,
      "y": 0.224,
      "layer": 5,
      "closed_by": [
        23,
        31,
        34,
        35
      ]
    },
    {
      "id": 23,
      "x": 1.855,
      "y": 0.699,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 24,
      "x": -0.81,
      "y": -0.495,
      "layer": 4,
      "closed_by": [
        26
      ]
    },
    {
      "id": 25,
      "x": 1.031,
      "y": 1.021,
      "layer": 5,
      "closed_by": [
        27
      ]
    },
    {
      "id": 26,
      "x": -1.024,
      "y": -0.574,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 27,
      "x": 1.268,
      "y": 0.699,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 1.269,
      "y": -0.256,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -1.266,
      "y": 0.699,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -1.266,
      "y": -0.256,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 1.855,
      "y": -0.259,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -1.853,
      "y": 0.698,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -1.855,
      "y": -0.256,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 2.417,
      "y": -0.256,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 2.414,
      "y": 0.699,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -2.413,
      "y": 0.699,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 37,
      "x": -2.411,
      "y": -0.259,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 1.613,
      "y": 0.224,
      "layer": 5,
      "closed_by": [
        23,
        27,
        28,
        31
      ]
    },
    {
      "id": 39,
      "x": -1.534,
      "y": 0.216,
      "layer": 5,
      "closed_by": [
        29,
        30,
        32,
        33
      ]
    },
    {
      "id": 40,
      "x": -2.092,
      "y": 0.223,
      "layer": 5,
      "closed_by": [
        32,
        33,
        36,
        37
      ]
    },
    {
      "id": 41,
      "x": 2.467,
      "y": 0.224,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 42,
      "x": -0.001,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 43,
      "x": -2.463,
      "y": 0.223,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 46,
      "x": 2.549,
      "y": 0.223,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 47,
      "x": 2.628,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": -2.625,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        8,
        36,
        37
      ]
    },
    {
      "id": 49,
      "x": -0.001,
      "y": 0.143,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 50,
      "x": -0.003,
      "y": 0.067,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 51,
      "y": 0.379,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 42,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 0.7,
    "star2": 26.0,
    "star3": 72.3
  }
}