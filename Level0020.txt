{
  "layout_cards": [
    {
      "id": 51,
      "x": -1.45,
      "y": 0.865,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 49,
      "x": -0.321,
      "y": 0.643,
      "layer": 2,
      "closed_by": [
        27
      ]
    },
    {
      "id": 48,
      "x": -0.324,
      "y": 0.032,
      "layer": 4,
      "closed_by": [
        32
      ]
    },
    {
      "id": 47,
      "x": -1.452,
      "y": -0.319,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 45,
      "x": 1.455,
      "y": -0.312,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 44,
      "x": 1.455,
      "y": -0.546,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 43,
      "x": 1.455,
      "y": -0.428,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 42,
      "x": 1.371,
      "y": 0.731,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 41,
      "x": 1.455,
      "y": -0.209,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 1.455,
      "y": 0.615,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 1.378,
      "y": 1.0,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 37,
      "x": -1.371,
      "y": 0.995,
      "layer": 1,
      "closed_by": [
        51
      ]
    },
    {
      "id": 36,
      "x": 1.455,
      "y": 0.861,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 35,
      "x": -1.371,
      "y": 0.736,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 34,
      "x": -1.452,
      "y": 0.624,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 0.326,
      "y": 0.032,
      "layer": 4,
      "closed_by": [
        32
      ]
    },
    {
      "id": 32,
      "x": 0.006,
      "y": -0.279,
      "layer": 5,
      "closed_by": [
        29,
        28
      ]
    },
    {
      "id": 31,
      "x": 0.326,
      "y": 0.643,
      "layer": 2,
      "closed_by": [
        27
      ]
    },
    {
      "id": 30,
      "y": 0.962,
      "layer": 1,
      "closed_by": [
        49,
        31
      ]
    },
    {
      "id": 29,
      "x": -0.326,
      "y": -0.574,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 0.33,
      "y": -0.574,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 0.004,
      "y": 0.344,
      "layer": 3,
      "closed_by": [
        48,
        33
      ],
      "card_type": 7
    },
    {
      "id": 39,
      "x": -1.452,
      "y": -0.199,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 46,
      "x": -1.452,
      "y": -0.537,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": -1.452,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        47
      ]
    }
  ],
  "cards_in_layout_amount": 25,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 58.0,
    "star2": 7.5,
    "star3": 34.4
  }
}