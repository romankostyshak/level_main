{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.837,
      "y": 0.6,
      "layer": 3,
      "closed_by": [
        49
      ]
    },
    {
      "id": 50,
      "x": 0.86,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 49,
      "x": -0.93,
      "y": 0.56,
      "angle": 344.997,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 48,
      "y": 0.238,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 47,
      "y": 1.039,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 46,
      "y": -0.578,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 45,
      "x": -0.358,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        46
      ]
    },
    {
      "id": 44,
      "x": -0.699,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 43,
      "x": 0.36,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        46
      ]
    },
    {
      "id": 42,
      "x": 0.68,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 41,
      "x": 1.0,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 40,
      "x": 1.358,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        41
      ]
    },
    {
      "id": 39,
      "x": -1.019,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 38,
      "x": -1.338,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 37,
      "y": 1.036,
      "layer": 2,
      "closed_by": [
        48,
        47
      ]
    },
    {
      "id": 36,
      "y": 0.638,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -0.319,
      "y": 0.759,
      "layer": 1,
      "closed_by": [
        37,
        33,
        31
      ]
    },
    {
      "id": 34,
      "x": 0.319,
      "y": 0.759,
      "layer": 1,
      "closed_by": [
        37,
        32,
        31
      ]
    },
    {
      "id": 33,
      "x": -0.578,
      "y": 0.679,
      "layer": 2,
      "closed_by": [
        51
      ]
    },
    {
      "id": 32,
      "x": 0.587,
      "y": 0.679,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 31,
      "y": 0.238,
      "layer": 2,
      "closed_by": [
        48,
        47
      ]
    },
    {
      "id": 30,
      "x": 0.949,
      "y": 0.583,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 29,
      "x": 1.031,
      "y": 0.62,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 28,
      "x": -1.011,
      "y": 0.6,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 24,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 55.4,
    "star2": 10.4,
    "star3": 33.4
  }
}