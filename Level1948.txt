{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.333,
      "y": -0.577,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 50,
      "x": -0.333,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 49,
      "x": -0.333,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        47,
        40
      ]
    },
    {
      "id": 48,
      "x": 0.333,
      "y": -0.18,
      "layer": 2,
      "closed_by": [
        46,
        40
      ]
    },
    {
      "id": 47,
      "x": -0.625,
      "y": -0.014,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 46,
      "x": 0.625,
      "y": -0.017,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 45,
      "x": -0.865,
      "y": 0.136,
      "layer": 4,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": -0.865,
      "y": 0.702,
      "layer": 5,
      "closed_by": [
        42,
        35
      ]
    },
    {
      "id": 43,
      "x": 0.869,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        41,
        36
      ]
    },
    {
      "id": 42,
      "x": -0.574,
      "y": 1.019,
      "layer": 6,
      "closed_by": [
        33
      ]
    },
    {
      "id": 41,
      "x": 0.574,
      "y": 1.018,
      "layer": 6,
      "closed_by": [
        34
      ]
    },
    {
      "id": 40,
      "y": -0.017,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 39,
      "x": 0.865,
      "y": 0.136,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 38,
      "x": 1.19,
      "y": 0.46,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 37,
      "x": -1.187,
      "y": 0.46,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 36,
      "x": 1.026,
      "y": 0.224,
      "layer": 6,
      "closed_by": [
        34
      ]
    },
    {
      "id": 35,
      "x": -1.026,
      "y": 0.222,
      "layer": 6,
      "closed_by": [
        33
      ]
    },
    {
      "id": 34,
      "x": 0.734,
      "y": 0.609,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -0.731,
      "y": 0.586,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 0.001,
      "y": 0.379,
      "layer": 4,
      "closed_by": [
        31
      ]
    },
    {
      "id": 31,
      "y": 0.86,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 21,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 21.1,
    "star2": 0.3,
    "star3": 78.2
  }
}