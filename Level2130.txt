{
  "layout_cards": [
    {
      "id": 49,
      "y": -0.495,
      "layer": 1,
      "closed_by": [
        48,
        47
      ]
    },
    {
      "id": 48,
      "x": 0.462,
      "y": -0.536,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        45,
        51
      ]
    },
    {
      "id": 47,
      "x": -0.451,
      "y": -0.523,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        46,
        51
      ]
    },
    {
      "id": 46,
      "x": -0.574,
      "y": 0.059,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 45,
      "x": 0.573,
      "y": 0.059,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 50,
      "x": -0.446,
      "y": 0.578,
      "angle": 315.0,
      "layer": 4,
      "closed_by": [
        43,
        40
      ]
    },
    {
      "id": 43,
      "x": 0.002,
      "y": 0.62,
      "layer": 5,
      "closed_by": [
        21
      ]
    },
    {
      "id": 41,
      "x": 0.647,
      "y": 0.136,
      "layer": 5,
      "closed_by": [
        39
      ]
    },
    {
      "id": 40,
      "x": -0.634,
      "y": 0.136,
      "layer": 5,
      "closed_by": [
        38
      ]
    },
    {
      "id": 39,
      "x": 0.648,
      "y": -0.085,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 44,
      "x": 0.453,
      "y": 0.587,
      "angle": 45.0,
      "layer": 4,
      "closed_by": [
        43,
        41
      ]
    },
    {
      "id": 38,
      "x": -0.634,
      "y": -0.075,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 1.534,
      "y": 1.018,
      "layer": 1,
      "closed_by": [
        34,
        26
      ]
    },
    {
      "id": 36,
      "x": -1.531,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        33,
        27
      ]
    },
    {
      "id": 34,
      "x": 1.771,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 33,
      "x": -1.768,
      "y": 0.623,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 32,
      "x": 2.013,
      "y": 0.221,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 31,
      "x": -2.013,
      "y": 0.222,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 30,
      "x": 1.771,
      "y": -0.172,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 29,
      "x": -1.774,
      "y": -0.172,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 28,
      "x": 1.534,
      "y": -0.578,
      "layer": 5,
      "closed_by": [
        25
      ]
    },
    {
      "id": 37,
      "x": -1.534,
      "y": -0.578,
      "layer": 5,
      "closed_by": [
        24
      ]
    },
    {
      "id": 27,
      "x": -1.213,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 26,
      "x": 1.213,
      "y": 1.016,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 25,
      "x": 1.215,
      "y": -0.578,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 24,
      "x": -1.213,
      "y": -0.578,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 21,
      "x": 0.003,
      "y": 0.541,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 51,
      "y": -0.414,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 23,
      "x": -1.212,
      "y": 0.541,
      "layer": 3,
      "closed_by": [
        19
      ]
    },
    {
      "id": 22,
      "x": 1.213,
      "y": 0.545,
      "layer": 3,
      "closed_by": [
        20
      ]
    },
    {
      "id": 20,
      "x": 1.215,
      "y": 0.063,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 19,
      "x": -1.215,
      "y": 0.037,
      "layer": 4,
      "closed_by": [
        37
      ]
    }
  ],
  "cards_in_layout_amount": 32,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 37.7,
    "star2": 52.0,
    "star3": 9.7
  }
}