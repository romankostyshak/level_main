{
  "layout_cards": [
    {
      "id": 22,
      "x": -0.546,
      "y": 0.957,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 23,
      "x": 0.333,
      "y": -0.592,
      "layer": 1,
      "closed_by": [
        24
      ],
      "card_type": 3
    },
    {
      "id": 24,
      "x": 0.55,
      "y": -0.509,
      "layer": 2,
      "closed_by": [
        26
      ]
    },
    {
      "id": 25,
      "x": 0.333,
      "y": -0.187,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 26,
      "x": 0.55,
      "y": -0.27,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 27,
      "x": -0.333,
      "y": 1.036,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 28,
      "x": -0.546,
      "y": 0.721,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 29,
      "x": -0.335,
      "y": 0.638,
      "layer": 4,
      "closed_by": [],
      "card_type": 2
    },
    {
      "id": 30,
      "x": -0.333,
      "y": -0.194,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 31,
      "x": 0.331,
      "y": 1.024,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 32,
      "x": 0.333,
      "y": 0.638,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 33,
      "x": -0.333,
      "y": -0.587,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 34,
      "x": 0.573,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 35,
      "x": -1.588,
      "y": -0.49,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -1.375,
      "y": 0.725,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 37,
      "x": -0.573,
      "y": -0.5,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 38,
      "x": 0.497,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 39,
      "x": -0.495,
      "y": -0.259,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 40,
      "x": 0.381,
      "y": 0.888,
      "layer": 5,
      "closed_by": [
        42
      ]
    },
    {
      "id": 41,
      "x": -0.384,
      "y": -0.449,
      "layer": 5,
      "closed_by": [
        43
      ]
    },
    {
      "id": 42,
      "x": 0.446,
      "y": 0.754,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 43,
      "x": -0.449,
      "y": -0.307,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 44,
      "x": 1.373,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 45,
      "x": 1.588,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 46,
      "x": -1.588,
      "y": 0.722,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 47,
      "x": 1.534,
      "y": 0.379,
      "layer": 3,
      "closed_by": [
        49
      ]
    },
    {
      "id": 48,
      "x": -1.531,
      "y": 0.064,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 49,
      "x": 1.373,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 50,
      "x": -1.371,
      "y": -0.49,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 51,
      "x": 1.59,
      "y": 0.939,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 30,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 54.8,
    "star2": 34.8,
    "star3": 9.5
  }
}