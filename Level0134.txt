{
  "layout_cards": [
    {
      "id": 51,
      "x": 2.269,
      "y": 0.726,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 50,
      "x": 2.832,
      "y": 0.722,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 49,
      "x": 2.269,
      "y": -0.128,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 48,
      "x": -2.94,
      "y": 0.722,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 47,
      "x": -2.361,
      "y": 0.722,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 46,
      "x": -2.937,
      "y": -0.135,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 44,
      "x": 2.832,
      "y": -0.133,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 43,
      "x": -2.364,
      "y": -0.134,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 42,
      "x": -0.351,
      "y": 0.879,
      "angle": 5.0,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 41,
      "x": 0.356,
      "y": 0.879,
      "angle": 354.997,
      "layer": 2,
      "closed_by": [
        27
      ]
    },
    {
      "id": 40,
      "x": -0.499,
      "y": 0.939,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 38,
      "y": 0.837,
      "layer": 1,
      "closed_by": [
        42,
        41
      ]
    },
    {
      "id": 37,
      "x": 0.001,
      "y": -0.469,
      "layer": 1,
      "closed_by": [
        36,
        35
      ]
    },
    {
      "id": 36,
      "x": 0.328,
      "y": -0.428,
      "angle": 354.997,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 35,
      "x": -0.307,
      "y": -0.425,
      "angle": 5.0,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 34,
      "x": 0.462,
      "y": -0.358,
      "angle": 340.0,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 32,
      "x": -1.746,
      "y": 0.303,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -0.439,
      "y": -0.358,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 29,
      "x": -0.66,
      "y": -0.337,
      "angle": 25.0,
      "layer": 4,
      "closed_by": [],
      "effect_id": 1
    },
    {
      "id": 28,
      "x": 0.68,
      "y": -0.337,
      "angle": 335.0,
      "layer": 4,
      "closed_by": [],
      "effect_id": 1
    },
    {
      "id": 27,
      "x": 0.509,
      "y": 0.935,
      "angle": 339.997,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 26,
      "x": -0.72,
      "y": 0.98,
      "angle": 25.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 0.74,
      "y": 0.98,
      "angle": 335.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 1.667,
      "y": 0.3,
      "layer": 1,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -2.72,
      "y": 0.303,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 2.618,
      "y": 0.305,
      "layer": 2,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 26,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 53.0,
    "star2": 35.2,
    "star3": 11.3
  }
}