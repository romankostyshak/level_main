{
  "layout_cards": [
    {
      "x": 2.618,
      "y": 0.799,
      "layer": 1,
      "closed_by": [
        3
      ]
    },
    {
      "id": 1,
      "x": 2.618,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        2
      ]
    },
    {
      "id": 2,
      "x": 2.578,
      "y": -0.298,
      "layer": 2,
      "closed_by": [
        10
      ]
    },
    {
      "id": 3,
      "x": 2.578,
      "y": 0.759,
      "layer": 2,
      "closed_by": [
        51
      ]
    },
    {
      "id": 4,
      "x": 1.82,
      "y": 0.759,
      "layer": 2,
      "closed_by": [
        51
      ]
    },
    {
      "id": 5,
      "x": 1.779,
      "y": 0.799,
      "layer": 1,
      "closed_by": [
        4
      ]
    },
    {
      "id": 6,
      "x": -2.24,
      "y": -0.216,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 7,
      "x": 1.779,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        8
      ]
    },
    {
      "id": 8,
      "x": 1.82,
      "y": -0.298,
      "layer": 2,
      "closed_by": [
        10
      ]
    },
    {
      "id": 9,
      "x": -2.2,
      "y": -0.179,
      "layer": 3,
      "closed_by": [
        6
      ]
    },
    {
      "id": 10,
      "x": 2.2,
      "y": -0.179,
      "layer": 3,
      "closed_by": [
        11
      ]
    },
    {
      "id": 11,
      "x": 2.24,
      "y": -0.216,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 12,
      "x": 2.24,
      "y": 0.679,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 13,
      "x": -2.578,
      "y": 0.759,
      "layer": 2,
      "closed_by": [
        17
      ]
    },
    {
      "id": 14,
      "x": -1.819,
      "y": 0.759,
      "layer": 2,
      "closed_by": [
        17
      ]
    },
    {
      "id": 15,
      "x": -1.819,
      "y": -0.298,
      "layer": 2,
      "closed_by": [
        9
      ]
    },
    {
      "id": 16,
      "x": -2.24,
      "y": 0.679,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 17,
      "x": -2.2,
      "y": 0.638,
      "layer": 3,
      "closed_by": [
        16
      ]
    },
    {
      "id": 18,
      "x": -2.578,
      "y": -0.298,
      "layer": 2,
      "closed_by": [
        9
      ]
    },
    {
      "id": 19,
      "x": -1.779,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        15
      ]
    },
    {
      "id": 20,
      "x": -2.618,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        18
      ]
    },
    {
      "id": 21,
      "x": -2.618,
      "y": 0.799,
      "layer": 1,
      "closed_by": [
        13
      ]
    },
    {
      "id": 22,
      "x": -1.159,
      "y": 0.216,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 1.159,
      "y": 0.216,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 24,
      "x": -1.12,
      "y": 0.216,
      "layer": 5,
      "closed_by": [
        22
      ]
    },
    {
      "id": 25,
      "x": 1.12,
      "y": 0.216,
      "layer": 5,
      "closed_by": [
        23
      ]
    },
    {
      "id": 26,
      "x": -0.819,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 27,
      "x": -0.819,
      "y": -0.5,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 28,
      "y": -0.578,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 29,
      "y": 1.019,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 0.819,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        25
      ]
    },
    {
      "id": 31,
      "y": 0.98,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 32,
      "y": -0.537,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 33,
      "x": -0.777,
      "y": 0.898,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 34,
      "x": -0.777,
      "y": -0.458,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 35,
      "x": 0.777,
      "y": 0.898,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 36,
      "x": 0.819,
      "y": -0.5,
      "layer": 4,
      "closed_by": [
        25
      ]
    },
    {
      "id": 37,
      "x": -1.779,
      "y": 0.799,
      "layer": 1,
      "closed_by": [
        14
      ]
    },
    {
      "id": 38,
      "x": 0.777,
      "y": -0.458,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 39,
      "x": -0.62,
      "y": 0.74,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 40,
      "y": 0.819,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 41,
      "x": 0.62,
      "y": 0.74,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 42,
      "x": -0.62,
      "y": -0.298,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 43,
      "y": -0.379,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 44,
      "x": 0.62,
      "y": -0.298,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 45,
      "x": 0.578,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 46,
      "x": -0.578,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        39
      ]
    },
    {
      "id": 47,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 48,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 49,
      "x": -0.578,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 50,
      "x": 0.578,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        41
      ]
    },
    {
      "id": 51,
      "x": 2.2,
      "y": 0.638,
      "layer": 3,
      "closed_by": [
        12
      ]
    }
  ],
  "cards_in_layout_amount": 52,
  "cards_in_deck_amount": 28,
  "layers_in_level": 6,
  "open_cards": 8,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 0.3,
    "star2": 41.5,
    "star3": 57.9
  }
}