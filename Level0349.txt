{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.319,
      "y": 0.46,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 49,
      "x": 0.324,
      "y": 0.46,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": -0.418,
      "y": 0.638,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 47,
      "x": -0.453,
      "y": 0.606,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 46,
      "x": 0.425,
      "y": 0.638,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 45,
      "x": 0.36,
      "y": 0.43,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": 0.456,
      "y": 0.605,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 43,
      "x": 0.56,
      "y": 0.787,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -1.985,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 41,
      "x": -1.985,
      "y": -0.059,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 40,
      "x": -1.985,
      "y": -0.319,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 39,
      "x": 1.996,
      "y": 0.177,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 35,
      "x": 0.333,
      "y": -0.458,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -0.33,
      "y": -0.46,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 0.333,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 32,
      "x": -0.33,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        34
      ]
    },
    {
      "id": 28,
      "x": 1.998,
      "y": 0.421,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 1.998,
      "y": 0.99,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 50,
      "x": -0.356,
      "y": 0.432,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 30,
      "x": -0.56,
      "y": 0.79,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -1.985,
      "y": 0.18,
      "layer": 4,
      "closed_by": [
        26
      ]
    },
    {
      "id": 26,
      "x": -1.985,
      "y": 0.423,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -1.985,
      "y": 0.99,
      "layer": 4,
      "closed_by": [
        26
      ]
    },
    {
      "id": 38,
      "x": 1.995,
      "y": -0.059,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 37,
      "x": 1.995,
      "y": -0.319,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 36,
      "x": 1.993,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        37
      ]
    }
  ],
  "cards_in_layout_amount": 26,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 53.6,
    "star2": 30.3,
    "star3": 15.5
  }
}