{
  "layout_cards": [
    {
      "id": 9,
      "x": 1.983,
      "y": 0.384,
      "layer": 1,
      "closed_by": [
        10
      ]
    },
    {
      "id": 10,
      "x": 1.585,
      "y": 0.379,
      "layer": 2,
      "closed_by": [
        46,
        51
      ]
    },
    {
      "id": 13,
      "x": -0.864,
      "y": 0.781,
      "layer": 3,
      "closed_by": [
        19,
        33
      ]
    },
    {
      "id": 14,
      "x": -1.904,
      "y": 0.781,
      "layer": 1,
      "closed_by": [
        17,
        25
      ]
    },
    {
      "id": 15,
      "x": -0.49,
      "y": 0.781,
      "layer": 1,
      "closed_by": [
        16,
        24
      ]
    },
    {
      "id": 16,
      "x": -0.861,
      "y": 0.381,
      "layer": 2,
      "closed_by": [
        13,
        23
      ]
    },
    {
      "id": 17,
      "x": -1.529,
      "y": 0.381,
      "layer": 2,
      "closed_by": [
        18,
        22
      ]
    },
    {
      "id": 18,
      "x": -1.534,
      "y": 0.781,
      "layer": 3,
      "closed_by": [
        19,
        35
      ]
    },
    {
      "id": 19,
      "x": -1.2,
      "y": 0.781,
      "layer": 4,
      "closed_by": [
        36,
        37
      ]
    },
    {
      "id": 20,
      "x": -0.49,
      "y": -0.093,
      "layer": 1,
      "closed_by": [
        16,
        24
      ]
    },
    {
      "id": 21,
      "x": -1.904,
      "y": -0.093,
      "layer": 1,
      "closed_by": [
        17,
        25
      ]
    },
    {
      "id": 22,
      "x": -1.526,
      "y": -0.097,
      "layer": 3,
      "closed_by": [
        27,
        32
      ]
    },
    {
      "id": 23,
      "x": -0.861,
      "y": -0.096,
      "layer": 3,
      "closed_by": [
        32,
        34
      ]
    },
    {
      "id": 24,
      "x": -0.33,
      "y": 0.381,
      "layer": 2,
      "closed_by": [
        28,
        30
      ]
    },
    {
      "id": 25,
      "x": -2.065,
      "y": 0.381,
      "layer": 2,
      "closed_by": [
        29,
        31
      ]
    },
    {
      "id": 26,
      "x": 0.787,
      "y": 0.782,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 27,
      "x": -1.794,
      "y": -0.1,
      "layer": 4,
      "closed_by": [
        36,
        39
      ]
    },
    {
      "id": 28,
      "x": -0.331,
      "y": -0.097,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 29,
      "x": -2.065,
      "y": -0.1,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 30,
      "x": -0.331,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 31,
      "x": -2.065,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 32,
      "x": -1.194,
      "y": -0.096,
      "layer": 4,
      "closed_by": [
        36,
        37
      ]
    },
    {
      "id": 33,
      "x": -0.592,
      "y": 0.781,
      "layer": 4,
      "closed_by": [
        37,
        38
      ]
    },
    {
      "id": 34,
      "x": -0.592,
      "y": -0.096,
      "layer": 4,
      "closed_by": [
        37,
        38
      ]
    },
    {
      "id": 35,
      "x": -1.802,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        36,
        39
      ]
    },
    {
      "id": 36,
      "x": -1.457,
      "y": 0.375,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 37,
      "x": -0.93,
      "y": 0.365,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -0.412,
      "y": 0.365,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -1.975,
      "y": 0.375,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 0.333,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 41,
      "x": 0.333,
      "y": -0.093,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 42,
      "x": 0.333,
      "y": 0.384,
      "layer": 2,
      "closed_by": [
        26,
        43
      ]
    },
    {
      "id": 43,
      "x": 0.786,
      "y": -0.097,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": 0.787,
      "y": 0.386,
      "layer": 4,
      "closed_by": [
        45,
        47
      ]
    },
    {
      "id": 45,
      "x": 1.187,
      "y": 0.777,
      "layer": 5,
      "closed_by": [
        48
      ]
    },
    {
      "id": 46,
      "x": 1.985,
      "y": -0.093,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 47,
      "x": 1.184,
      "y": -0.093,
      "layer": 5,
      "closed_by": [
        48
      ]
    },
    {
      "id": 48,
      "x": 1.184,
      "y": 0.388,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 49,
      "x": 1.585,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        45
      ]
    },
    {
      "id": 50,
      "x": 1.585,
      "y": -0.093,
      "layer": 4,
      "closed_by": [
        47
      ]
    },
    {
      "id": 51,
      "x": 1.985,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        49
      ]
    }
  ],
  "cards_in_layout_amount": 41,
  "cards_in_deck_amount": 32,
  "stars_statistic": {
    "star1": 5.5,
    "star2": 58.2,
    "star3": 36.1
  }
}