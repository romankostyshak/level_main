{
  "layout_cards": [
    {
      "x": 1.825,
      "y": -0.337,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 1,
      "x": 1.827,
      "y": 0.781,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 2,
      "x": -1.825,
      "y": 0.777,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 3,
      "x": -1.746,
      "y": 0.861,
      "layer": 2,
      "closed_by": [
        2
      ]
    },
    {
      "id": 4,
      "x": 1.745,
      "y": 0.703,
      "layer": 2,
      "closed_by": [
        1
      ]
    },
    {
      "id": 5,
      "x": -1.692,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        7
      ]
    },
    {
      "id": 6,
      "x": 1.746,
      "y": -0.414,
      "layer": 2,
      "closed_by": [
        0
      ]
    },
    {
      "id": 7,
      "x": -1.746,
      "y": -0.256,
      "layer": 2,
      "closed_by": [
        11
      ]
    },
    {
      "id": 8,
      "x": -1.69,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        3
      ]
    },
    {
      "id": 9,
      "x": 1.694,
      "y": 0.62,
      "layer": 1,
      "closed_by": [
        4
      ]
    },
    {
      "id": 10,
      "x": 1.692,
      "y": -0.497,
      "layer": 1,
      "closed_by": [
        6
      ]
    },
    {
      "id": 11,
      "x": -1.825,
      "y": -0.335,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 12,
      "x": 0.261,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        19
      ]
    },
    {
      "id": 13,
      "x": -0.782,
      "y": 1.023,
      "layer": 1,
      "closed_by": [
        15,
        35
      ]
    },
    {
      "id": 14,
      "x": -0.256,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        15
      ]
    },
    {
      "id": 15,
      "x": -0.523,
      "y": 0.943,
      "layer": 2,
      "closed_by": [
        20,
        38
      ]
    },
    {
      "id": 16,
      "x": -0.98,
      "y": 0.837,
      "layer": 4,
      "closed_by": [
        50
      ]
    },
    {
      "id": 17,
      "x": 1.105,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 18,
      "x": 0.79,
      "y": 0.541,
      "layer": 1,
      "closed_by": [
        19,
        34
      ]
    },
    {
      "id": 19,
      "x": 0.544,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        25,
        37
      ]
    },
    {
      "id": 20,
      "x": -0.298,
      "y": 0.837,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 21,
      "x": -0.347,
      "y": 0.888,
      "layer": 4,
      "closed_by": [
        46
      ]
    },
    {
      "id": 22,
      "x": -0.786,
      "y": -0.092,
      "layer": 1,
      "closed_by": [
        29,
        36
      ]
    },
    {
      "id": 23,
      "x": -0.246,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 24,
      "x": 0.263,
      "y": -0.335,
      "layer": 1,
      "closed_by": [
        28
      ]
    },
    {
      "id": 25,
      "x": 0.307,
      "y": 0.731,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 26,
      "x": -0.293,
      "y": -0.284,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 27,
      "x": 0.791,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        17,
        28
      ]
    },
    {
      "id": 28,
      "x": 0.546,
      "y": -0.499,
      "layer": 2,
      "closed_by": [
        30,
        39
      ]
    },
    {
      "id": 29,
      "x": -0.513,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        26,
        40
      ]
    },
    {
      "id": 30,
      "x": 0.31,
      "y": -0.388,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 31,
      "x": -0.337,
      "y": -0.23,
      "layer": 4,
      "closed_by": [
        44
      ]
    },
    {
      "id": 32,
      "x": 0.354,
      "y": -0.439,
      "layer": 4,
      "closed_by": [
        48
      ]
    },
    {
      "id": 33,
      "x": 0.351,
      "y": 0.679,
      "layer": 4,
      "closed_by": [
        47
      ]
    },
    {
      "id": 34,
      "x": 1.108,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 35,
      "x": -1.105,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 36,
      "x": -1.105,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 37,
      "x": 0.939,
      "y": 0.666,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 38,
      "x": -0.939,
      "y": 0.892,
      "layer": 3,
      "closed_by": [
        16
      ]
    },
    {
      "id": 39,
      "x": 0.939,
      "y": -0.449,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 40,
      "x": -0.925,
      "y": -0.236,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 41,
      "x": 0.985,
      "y": 0.717,
      "layer": 4,
      "closed_by": [
        45
      ]
    },
    {
      "id": 42,
      "x": 0.985,
      "y": -0.4,
      "layer": 4,
      "closed_by": [
        49
      ]
    },
    {
      "id": 43,
      "x": -0.972,
      "y": -0.287,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 44,
      "x": -0.381,
      "y": -0.178,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 45,
      "x": 1.034,
      "y": 0.782,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 46,
      "x": -0.393,
      "y": 0.939,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 47,
      "x": 0.4,
      "y": 0.62,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 48,
      "x": 0.4,
      "y": -0.497,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 49,
      "x": 1.036,
      "y": -0.337,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 50,
      "x": -1.036,
      "y": 0.777,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -1.026,
      "y": -0.337,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 52,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 0.0,
    "star2": 0.7,
    "star3": 98.8
  }
}