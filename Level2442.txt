{
  "layout_cards": [
    {
      "id": 19,
      "x": -1.667,
      "y": -0.178,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 20,
      "x": -1.667,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        23
      ]
    },
    {
      "id": 21,
      "x": 1.669,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        25
      ]
    },
    {
      "id": 22,
      "x": -1.588,
      "y": -0.177,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 23,
      "x": -1.588,
      "y": 0.703,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 24,
      "x": 1.588,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 25,
      "x": 1.587,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 26,
      "x": -1.424,
      "y": 0.221,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 1.429,
      "y": 0.224,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 1.269,
      "y": 0.222,
      "layer": 3,
      "closed_by": [
        42,
        50
      ]
    },
    {
      "id": 29,
      "x": -0.569,
      "y": -0.178,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 0.569,
      "y": -0.179,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 0.569,
      "y": 0.702,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -0.572,
      "y": 0.702,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 0.87,
      "y": 0.209,
      "layer": 5,
      "closed_by": [
        30,
        31
      ]
    },
    {
      "id": 34,
      "x": -0.875,
      "y": 0.209,
      "layer": 5,
      "closed_by": [
        29,
        32
      ]
    },
    {
      "id": 35,
      "x": -0.004,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        38,
        39
      ]
    },
    {
      "id": 36,
      "x": -1.266,
      "y": 0.221,
      "layer": 3,
      "closed_by": [
        43,
        44
      ]
    },
    {
      "id": 37,
      "x": 1.669,
      "y": -0.172,
      "layer": 1,
      "closed_by": [
        24,
        27
      ]
    },
    {
      "id": 38,
      "x": 0.386,
      "y": 0.86,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 39,
      "x": -0.384,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 40,
      "x": 0.708,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        42
      ]
    },
    {
      "id": 41,
      "x": -0.703,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 42,
      "x": 1.026,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        27,
        33
      ]
    },
    {
      "id": 43,
      "x": -1.026,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        26,
        34
      ]
    },
    {
      "id": 44,
      "x": -1.026,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        26,
        34
      ]
    },
    {
      "id": 45,
      "x": -0.707,
      "y": -0.256,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 46,
      "x": 0.707,
      "y": -0.256,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 47,
      "x": -0.386,
      "y": -0.335,
      "layer": 2,
      "closed_by": [
        34,
        45
      ]
    },
    {
      "id": 48,
      "x": 0.388,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        33,
        46
      ]
    },
    {
      "id": 49,
      "y": -0.416,
      "layer": 1,
      "closed_by": [
        47,
        48
      ]
    },
    {
      "id": 50,
      "x": 1.029,
      "y": -0.177,
      "layer": 4,
      "closed_by": [
        27,
        33
      ]
    }
  ],
  "cards_in_layout_amount": 32,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 42.7,
    "star2": 44.1,
    "star3": 12.2
  }
}