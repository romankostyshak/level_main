{
  "layout_cards": [
    {
      "id": 49,
      "x": 1.743,
      "y": -0.177,
      "layer": 1,
      "closed_by": [
        48,
        46
      ]
    },
    {
      "id": 47,
      "x": 1.748,
      "y": 0.629,
      "layer": 1,
      "closed_by": [
        48,
        46
      ]
    },
    {
      "id": 48,
      "x": 2.062,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        13
      ]
    },
    {
      "id": 46,
      "x": 1.531,
      "y": 0.224,
      "layer": 2,
      "closed_by": [
        50,
        45
      ]
    },
    {
      "id": 50,
      "x": 1.531,
      "y": -0.178,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 44,
      "x": 1.294,
      "y": 0.703,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 43,
      "x": 1.294,
      "y": -0.252,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 42,
      "x": 1.052,
      "y": 0.786,
      "layer": 5,
      "closed_by": [
        18
      ]
    },
    {
      "id": 41,
      "x": 1.049,
      "y": -0.333,
      "layer": 5,
      "closed_by": [
        15
      ]
    },
    {
      "id": 39,
      "x": 0.648,
      "y": -0.18,
      "layer": 4,
      "closed_by": [
        41,
        34
      ]
    },
    {
      "id": 40,
      "x": 0.648,
      "y": 0.633,
      "layer": 4,
      "closed_by": [
        42,
        35
      ]
    },
    {
      "id": 45,
      "x": 1.536,
      "y": 0.629,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 32,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        38,
        33
      ]
    },
    {
      "id": 31,
      "x": -0.629,
      "y": 0.633,
      "layer": 4,
      "closed_by": [
        36,
        29
      ]
    },
    {
      "id": 30,
      "x": -0.629,
      "y": -0.187,
      "layer": 4,
      "closed_by": [
        27,
        28
      ]
    },
    {
      "id": 36,
      "x": -1.052,
      "y": 0.782,
      "layer": 5,
      "closed_by": [
        19
      ]
    },
    {
      "id": 27,
      "x": -1.052,
      "y": -0.337,
      "layer": 5,
      "closed_by": [
        14
      ]
    },
    {
      "id": 26,
      "x": -1.294,
      "y": 0.703,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 25,
      "x": -1.289,
      "y": -0.256,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 24,
      "x": -1.526,
      "y": 0.624,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 23,
      "x": -1.529,
      "y": -0.194,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 22,
      "x": -1.529,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        24,
        23
      ]
    },
    {
      "id": 21,
      "x": -1.746,
      "y": 0.625,
      "layer": 1,
      "closed_by": [
        22,
        20
      ]
    },
    {
      "id": 37,
      "x": -1.745,
      "y": -0.185,
      "layer": 1,
      "closed_by": [
        22,
        20
      ]
    },
    {
      "id": 35,
      "x": 0.412,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        18
      ]
    },
    {
      "id": 34,
      "x": 0.412,
      "y": -0.259,
      "layer": 5,
      "closed_by": [
        15
      ]
    },
    {
      "id": 38,
      "x": 0.412,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        39,
        40
      ]
    },
    {
      "id": 29,
      "x": -0.411,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        19
      ]
    },
    {
      "id": 33,
      "x": -0.414,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        31,
        30
      ]
    },
    {
      "id": 28,
      "x": -0.411,
      "y": -0.259,
      "layer": 5,
      "closed_by": [
        14
      ]
    },
    {
      "id": 20,
      "x": -2.065,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        12
      ]
    },
    {
      "id": 19,
      "x": -0.652,
      "y": 0.859,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 18,
      "x": 0.657,
      "y": 0.861,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 15,
      "x": 0.652,
      "y": -0.412,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 14,
      "x": -0.651,
      "y": -0.416,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 13,
      "x": 2.305,
      "y": 0.216,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 12,
      "x": -2.305,
      "y": 0.216,
      "layer": 3,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 37,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 11.8,
    "star2": 65.9,
    "star3": 21.3
  }
}