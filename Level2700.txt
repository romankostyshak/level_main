{
  "layout_cards": [
    {
      "id": 20,
      "x": -2.171,
      "y": 0.703,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 21,
      "x": 1.059,
      "y": 0.216,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -1.065,
      "y": 0.216,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 2.174,
      "y": -0.256,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 1.108,
      "y": 0.221,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 26,
      "x": 0.869,
      "y": 0.223,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 27,
      "x": -0.864,
      "y": 0.222,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 28,
      "x": -2.361,
      "y": 0.652,
      "angle": 285.0,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 29,
      "x": 2.361,
      "y": -0.216,
      "angle": 285.0,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 30,
      "x": -2.368,
      "y": 0.483,
      "angle": 310.0,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 31,
      "x": 2.368,
      "y": -0.048,
      "angle": 310.0,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 32,
      "x": -2.299,
      "y": 0.331,
      "angle": 335.0,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 33,
      "x": 2.299,
      "y": 0.111,
      "angle": 335.0,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 34,
      "x": -1.108,
      "y": 0.221,
      "layer": 3,
      "closed_by": [
        49
      ]
    },
    {
      "id": 35,
      "x": -0.135,
      "y": -0.108,
      "angle": 285.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 0.136,
      "y": 0.541,
      "angle": 285.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 37,
      "x": 0.007,
      "y": -0.2,
      "angle": 310.0,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 38,
      "x": 0.002,
      "y": 0.642,
      "angle": 310.0,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 39,
      "x": 0.172,
      "y": -0.222,
      "angle": 335.0,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 40,
      "x": -0.165,
      "y": 0.665,
      "angle": 335.0,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 41,
      "x": 0.333,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 42,
      "x": -2.171,
      "y": 0.224,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 43,
      "x": 2.174,
      "y": 0.224,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 44,
      "x": -0.572,
      "y": 0.223,
      "layer": 1,
      "closed_by": [
        22,
        27,
        37,
        47
      ]
    },
    {
      "id": 45,
      "x": 0.573,
      "y": 0.223,
      "layer": 1,
      "closed_by": [
        21,
        26,
        38,
        41
      ]
    },
    {
      "id": 46,
      "y": 0.224,
      "layer": 1,
      "closed_by": [
        41,
        47
      ]
    },
    {
      "id": 47,
      "x": -0.331,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 48,
      "x": 1.347,
      "y": 0.221,
      "layer": 4,
      "closed_by": [
        21,
        50
      ]
    },
    {
      "id": 49,
      "x": -1.345,
      "y": 0.216,
      "layer": 4,
      "closed_by": [
        22,
        51
      ]
    },
    {
      "id": 50,
      "x": 1.585,
      "y": 0.216,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -1.587,
      "y": 0.221,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 31,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 31.1,
    "star2": 60.9,
    "star3": 7.1
  }
}