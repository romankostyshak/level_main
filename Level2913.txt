{
  "layout_cards": [
    {
      "id": 10,
      "x": -1.896,
      "y": 0.474,
      "angle": 20.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 11,
      "x": -1.995,
      "y": 0.003,
      "angle": 20.0,
      "layer": 4,
      "closed_by": [
        10,
        18
      ]
    },
    {
      "id": 12,
      "x": -1.501,
      "y": 0.18,
      "angle": 20.0,
      "layer": 4,
      "closed_by": [
        10
      ]
    },
    {
      "id": 13,
      "x": -1.48,
      "y": -0.209,
      "angle": 20.0,
      "layer": 1,
      "closed_by": [
        51
      ]
    },
    {
      "id": 14,
      "x": 1.345,
      "y": 0.856,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 15,
      "x": 1.825,
      "y": -0.178,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 16,
      "x": 0.813,
      "y": 0.861,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 17,
      "x": -1.48,
      "y": -0.209,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        11,
        12
      ]
    },
    {
      "id": 18,
      "x": -2.381,
      "y": 0.293,
      "angle": 20.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 19,
      "x": 1.294,
      "y": -0.18,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 20,
      "x": 0.814,
      "y": 0.939,
      "layer": 5,
      "closed_by": [
        16
      ]
    },
    {
      "id": 21,
      "x": 1.291,
      "y": -0.259,
      "layer": 5,
      "closed_by": [
        19
      ]
    },
    {
      "id": 22,
      "x": 1.261,
      "y": 1.023,
      "layer": 1,
      "closed_by": [
        23
      ]
    },
    {
      "id": 23,
      "x": 1.195,
      "y": 1.023,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 24,
      "x": 1.664,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        26
      ]
    },
    {
      "id": 25,
      "x": 1.115,
      "y": 1.023,
      "layer": 3,
      "closed_by": [
        14,
        33
      ]
    },
    {
      "id": 26,
      "x": 1.588,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        15,
        32
      ]
    },
    {
      "id": 27,
      "x": 0.546,
      "y": 0.861,
      "layer": 1,
      "closed_by": [
        37
      ]
    },
    {
      "id": 28,
      "x": 1.024,
      "y": -0.178,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 29,
      "x": 1.024,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 30,
      "x": 1.026,
      "y": -0.34,
      "layer": 3,
      "closed_by": [
        32,
        34
      ]
    },
    {
      "id": 31,
      "x": 0.545,
      "y": 1.021,
      "layer": 3,
      "closed_by": [
        33,
        35
      ]
    },
    {
      "id": 32,
      "x": 1.292,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 33,
      "x": 0.814,
      "y": 1.021,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 34,
      "x": 0.62,
      "y": -0.335,
      "layer": 4,
      "closed_by": [
        48
      ]
    },
    {
      "id": 35,
      "x": 0.142,
      "y": 1.021,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 36,
      "x": -0.128,
      "y": 0.995,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 37,
      "x": 0.546,
      "y": 0.943,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 38,
      "x": 1.748,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        24
      ]
    },
    {
      "id": 39,
      "x": -0.619,
      "y": 0.814,
      "angle": 20.0,
      "layer": 5,
      "closed_by": [
        42
      ]
    },
    {
      "id": 40,
      "x": 0.326,
      "y": -0.354,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 41,
      "x": -0.187,
      "y": -0.535,
      "angle": 20.0,
      "layer": 5,
      "closed_by": [
        48
      ]
    },
    {
      "id": 42,
      "x": -0.409,
      "y": 0.98,
      "angle": 20.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 43,
      "x": -0.833,
      "y": 0.648,
      "angle": 20.0,
      "layer": 4,
      "closed_by": [
        39
      ]
    },
    {
      "id": 44,
      "x": -0.523,
      "y": 0.558,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 45,
      "x": -1.011,
      "y": 0.381,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 46,
      "x": -0.628,
      "y": 0.092,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        44,
        45,
        50
      ]
    },
    {
      "id": 47,
      "x": -0.397,
      "y": -0.537,
      "angle": 20.0,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 48,
      "x": 0.104,
      "y": -0.351,
      "angle": 20.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 49,
      "x": -0.699,
      "y": -0.428,
      "angle": 20.0,
      "layer": 1,
      "closed_by": [
        46,
        47,
        51
      ]
    },
    {
      "id": 50,
      "x": -0.231,
      "y": -0.216,
      "angle": 20.0,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 51,
      "x": -1.116,
      "y": -0.079,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        17,
        45
      ]
    }
  ],
  "cards_in_layout_amount": 42,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 2.7,
    "star2": 41.5,
    "star3": 54.9
  }
}