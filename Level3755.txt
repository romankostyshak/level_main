{
  "layout_cards": [
    {
      "id": 17,
      "x": -0.828,
      "y": 0.5,
      "angle": 50.0,
      "layer": 2,
      "closed_by": [
        29,
        32
      ]
    },
    {
      "id": 18,
      "y": -0.194,
      "layer": 1,
      "closed_by": [
        19
      ]
    },
    {
      "id": 19,
      "y": 0.363,
      "layer": 2,
      "closed_by": [
        20,
        34,
        35
      ]
    },
    {
      "id": 20,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        51
      ]
    },
    {
      "id": 21,
      "x": -1.08,
      "y": 0.216,
      "angle": 60.0,
      "layer": 1,
      "closed_by": [
        17
      ]
    },
    {
      "id": 22,
      "x": 1.065,
      "y": 0.216,
      "angle": 300.0,
      "layer": 1,
      "closed_by": [
        23
      ]
    },
    {
      "id": 23,
      "x": 0.828,
      "y": 0.5,
      "angle": 310.0,
      "layer": 2,
      "closed_by": [
        26,
        28
      ]
    },
    {
      "id": 24,
      "x": -0.62,
      "y": 0.777,
      "angle": 40.0,
      "layer": 1,
      "closed_by": [
        17,
        19
      ]
    },
    {
      "id": 25,
      "x": 0.62,
      "y": 0.777,
      "angle": 320.0,
      "layer": 1,
      "closed_by": [
        19,
        23
      ]
    },
    {
      "id": 26,
      "x": 1.205,
      "y": 0.3,
      "angle": 300.0,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 27,
      "x": 1.011,
      "y": 0.662,
      "angle": 310.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 0.72,
      "y": 0.898,
      "angle": 320.0,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 29,
      "x": -1.241,
      "y": 0.296,
      "angle": 60.0,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 30,
      "x": -1.975,
      "y": 0.916,
      "angle": 15.0,
      "layer": 1,
      "closed_by": [
        48
      ],
      "card_type": 3
    },
    {
      "id": 31,
      "x": -2.417,
      "y": -0.128,
      "angle": 15.0,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 32,
      "x": -0.74,
      "y": 0.916,
      "angle": 40.0,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 33,
      "x": -1.031,
      "y": 0.684,
      "angle": 50.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -0.259,
      "y": -0.156,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 35,
      "x": 0.259,
      "y": -0.156,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 36,
      "y": -0.119,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 37,
      "x": 2.052,
      "y": -0.097,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 38,
      "x": 2.351,
      "y": -0.115,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 39,
      "x": 2.253,
      "y": -0.158,
      "layer": 3,
      "closed_by": [
        49
      ]
    },
    {
      "id": 40,
      "x": -2.118,
      "y": -0.074,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 41,
      "x": -2.315,
      "y": -0.172,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 42,
      "x": 2.572,
      "y": 0.72,
      "angle": 344.997,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 43,
      "x": -2.637,
      "y": 0.72,
      "angle": 15.0,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": -2.736,
      "y": 0.72,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 45,
      "x": 2.673,
      "y": 0.74,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 46,
      "x": 1.939,
      "y": 0.898,
      "angle": 344.997,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "x": 2.22,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        49
      ]
    },
    {
      "id": 48,
      "x": -2.276,
      "y": 0.879,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 49,
      "x": 2.42,
      "y": 0.397,
      "angle": 325.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 50,
      "x": -2.476,
      "y": 0.418,
      "angle": 34.999,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 51,
      "y": 0.513,
      "layer": 5,
      "closed_by": [],
      "card_type": 2
    }
  ],
  "cards_in_layout_amount": 35,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 35.4,
    "star2": 60.5,
    "star3": 4.0
  }
}