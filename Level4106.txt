{
  "layout_cards": [
    {
      "id": 9,
      "y": 0.939,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 10,
      "y": -0.337,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 11,
      "x": -0.518,
      "y": 1.019,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 12,
      "x": 0.518,
      "y": 1.019,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 13,
      "x": 0.518,
      "y": -0.418,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 14,
      "x": -2.598,
      "y": 0.98,
      "angle": 345.0,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 15,
      "x": -2.598,
      "y": -0.379,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        21
      ]
    },
    {
      "id": 16,
      "x": -0.518,
      "y": -0.418,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 18,
      "x": -2.68,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        14,
        15
      ]
    },
    {
      "id": 19,
      "x": 2.598,
      "y": -0.379,
      "angle": 345.0,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 20,
      "x": 2.598,
      "y": 0.98,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 21,
      "x": -2.299,
      "y": -0.337,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 22,
      "x": 2.299,
      "y": -0.337,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 23,
      "x": -2.299,
      "y": 0.939,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 2.299,
      "y": 0.939,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 25,
      "x": -1.978,
      "y": 0.898,
      "angle": 345.0,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 26,
      "x": -2.059,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        25,
        27
      ]
    },
    {
      "id": 27,
      "x": -1.978,
      "y": -0.298,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        21
      ]
    },
    {
      "id": 28,
      "x": 1.98,
      "y": 0.898,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 29,
      "x": 2.059,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        28,
        36
      ]
    },
    {
      "id": 30,
      "x": -1.358,
      "y": 0.958,
      "angle": 345.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -1.358,
      "y": -0.358,
      "angle": 15.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 1.358,
      "y": -0.358,
      "angle": 345.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 1.358,
      "y": 0.958,
      "angle": 15.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -1.139,
      "y": 0.898,
      "angle": 345.0,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 35,
      "x": -1.139,
      "y": -0.298,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 36,
      "x": 1.98,
      "y": -0.298,
      "angle": 345.0,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 37,
      "x": 2.677,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        19,
        20
      ]
    },
    {
      "id": 38,
      "x": 1.139,
      "y": -0.298,
      "angle": 345.0,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 39,
      "x": 1.139,
      "y": 0.898,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 40,
      "x": -0.919,
      "y": 0.837,
      "angle": 345.0,
      "layer": 2,
      "closed_by": [
        11,
        34
      ]
    },
    {
      "id": 41,
      "x": -0.919,
      "y": -0.238,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        16,
        35
      ]
    },
    {
      "id": 42,
      "x": 0.92,
      "y": -0.238,
      "angle": 345.0,
      "layer": 2,
      "closed_by": [
        13,
        38
      ]
    },
    {
      "id": 43,
      "x": 0.92,
      "y": 0.837,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        12,
        39
      ]
    },
    {
      "id": 44,
      "x": -0.358,
      "y": 0.777,
      "angle": 345.0,
      "layer": 2,
      "closed_by": [
        11,
        49
      ]
    },
    {
      "id": 45,
      "x": 0.36,
      "y": -0.158,
      "angle": 345.0,
      "layer": 2,
      "closed_by": [
        13,
        49
      ]
    },
    {
      "id": 46,
      "x": 0.36,
      "y": 0.777,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        12,
        49
      ]
    },
    {
      "id": 47,
      "x": -0.62,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        40,
        41,
        44,
        50
      ]
    },
    {
      "id": 48,
      "x": 0.62,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        42,
        43,
        45,
        46
      ]
    },
    {
      "id": 49,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        9,
        10
      ]
    },
    {
      "id": 50,
      "x": -0.358,
      "y": -0.158,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        16,
        49
      ]
    }
  ],
  "cards_in_layout_amount": 41,
  "cards_in_deck_amount": 26,
  "layers_in_level": 4,
  "open_cards": 14,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 1.1,
    "star2": 61.2,
    "star3": 36.9
  }
}