{
  "layout_cards": [
    {
      "id": 19,
      "x": -1.503,
      "y": 0.377,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 20,
      "x": 1.429,
      "y": 0.368,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 21,
      "x": -1.508,
      "y": 0.86,
      "layer": 6,
      "closed_by": [
        19
      ]
    },
    {
      "id": 22,
      "x": 1.429,
      "y": -0.093,
      "layer": 6,
      "closed_by": [
        20
      ]
    },
    {
      "id": 23,
      "x": 1.429,
      "y": 0.856,
      "layer": 6,
      "closed_by": [
        20
      ]
    },
    {
      "id": 24,
      "x": -1.347,
      "y": -0.165,
      "layer": 5,
      "closed_by": [
        37
      ]
    },
    {
      "id": 25,
      "x": -1.345,
      "y": 0.944,
      "layer": 5,
      "closed_by": [
        21
      ]
    },
    {
      "id": 26,
      "x": 1.347,
      "y": -0.178,
      "layer": 5,
      "closed_by": [
        22
      ]
    },
    {
      "id": 27,
      "x": 1.345,
      "y": 0.939,
      "layer": 5,
      "closed_by": [
        23
      ]
    },
    {
      "id": 28,
      "x": -1.266,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        25
      ]
    },
    {
      "id": 29,
      "x": 1.266,
      "y": -0.254,
      "layer": 4,
      "closed_by": [
        26
      ]
    },
    {
      "id": 30,
      "x": 1.266,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 31,
      "x": -1.187,
      "y": -0.172,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 32,
      "x": -1.184,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 33,
      "x": 1.187,
      "y": -0.172,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 34,
      "x": 1.189,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 35,
      "x": -1.133,
      "y": -0.096,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 36,
      "x": -1.268,
      "y": -0.256,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 37,
      "x": -1.506,
      "y": -0.093,
      "layer": 6,
      "closed_by": [
        19
      ]
    },
    {
      "id": 38,
      "x": 1.133,
      "y": -0.093,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 39,
      "x": -1.133,
      "y": 0.861,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 40,
      "x": 1.133,
      "y": 0.856,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 41,
      "x": -1.133,
      "y": 0.379,
      "layer": 1,
      "closed_by": [
        35,
        39
      ]
    },
    {
      "id": 42,
      "x": 1.133,
      "y": 0.379,
      "layer": 1,
      "closed_by": [
        38,
        40
      ]
    },
    {
      "id": 43,
      "x": 0.001,
      "y": -0.414,
      "layer": 3,
      "closed_by": [
        46,
        50
      ]
    },
    {
      "id": 44,
      "x": 0.282,
      "y": -0.143,
      "angle": 340.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 45,
      "x": -0.465,
      "y": 0.379,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 46,
      "x": 0.469,
      "y": 0.379,
      "layer": 4,
      "closed_by": [
        44
      ]
    },
    {
      "id": 47,
      "x": 0.158,
      "y": 0.735,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": -0.163,
      "y": 0.736,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        45,
        47
      ]
    },
    {
      "id": 49,
      "x": 0.002,
      "y": 0.861,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 50,
      "x": -0.279,
      "y": -0.135,
      "angle": 20.0,
      "layer": 4,
      "closed_by": [
        44
      ]
    }
  ],
  "cards_in_layout_amount": 32,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 30.1,
    "star2": 59.5,
    "star3": 9.5
  }
}