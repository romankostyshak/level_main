{
  "layout_cards": [
    {
      "id": 10,
      "x": -1.985,
      "y": 0.303,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 11,
      "x": -1.452,
      "y": 0.314,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 12,
      "x": 1.452,
      "y": 0.307,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 13,
      "x": -1.723,
      "y": 0.782,
      "layer": 4,
      "closed_by": [
        10,
        11
      ]
    },
    {
      "id": 14,
      "x": -1.728,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        10,
        11
      ]
    },
    {
      "id": 15,
      "x": 1.717,
      "y": 0.781,
      "layer": 4,
      "closed_by": [
        12,
        16
      ]
    },
    {
      "id": 16,
      "x": 1.983,
      "y": 0.303,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 18,
      "x": 1.718,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        12,
        16
      ]
    },
    {
      "id": 19,
      "x": -1.725,
      "y": -0.416,
      "layer": 3,
      "closed_by": [
        14
      ]
    },
    {
      "id": 20,
      "x": 1.718,
      "y": -0.416,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 21,
      "x": -1.983,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        13
      ]
    },
    {
      "id": 22,
      "x": -1.45,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        13
      ]
    },
    {
      "id": 23,
      "x": -1.985,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 24,
      "x": -1.452,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 25,
      "x": 1.452,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 26,
      "x": 1.985,
      "y": -0.34,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 27,
      "x": 1.452,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 28,
      "x": -1.725,
      "y": -0.256,
      "layer": 1,
      "closed_by": [
        23,
        24
      ]
    },
    {
      "id": 29,
      "x": 1.718,
      "y": -0.256,
      "layer": 1,
      "closed_by": [
        25,
        26
      ]
    },
    {
      "id": 30,
      "x": 1.713,
      "y": 0.86,
      "layer": 1,
      "closed_by": [
        27,
        36
      ]
    },
    {
      "id": 31,
      "x": -1.722,
      "y": 0.861,
      "layer": 1,
      "closed_by": [
        21,
        22
      ]
    },
    {
      "id": 32,
      "x": -0.703,
      "y": -0.252,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -0.703,
      "y": 0.861,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 0.708,
      "y": -0.261,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 0.703,
      "y": 0.859,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 1.985,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 37,
      "x": 1.717,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        15
      ]
    },
    {
      "id": 38,
      "x": -0.465,
      "y": 0.943,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 39,
      "x": 0.467,
      "y": 0.943,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 40,
      "x": -0.462,
      "y": -0.333,
      "layer": 4,
      "closed_by": [
        32
      ]
    },
    {
      "id": 41,
      "x": 0.467,
      "y": -0.331,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 42,
      "x": -0.544,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        38,
        40
      ]
    },
    {
      "id": 43,
      "x": 0.546,
      "y": 0.303,
      "layer": 3,
      "closed_by": [
        39,
        41
      ]
    },
    {
      "id": 44,
      "y": -0.256,
      "layer": 3,
      "closed_by": [
        40,
        41
      ]
    },
    {
      "id": 45,
      "x": -0.386,
      "y": -0.177,
      "layer": 2,
      "closed_by": [
        42,
        44
      ]
    },
    {
      "id": 46,
      "x": -0.384,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        42,
        50
      ]
    },
    {
      "id": 47,
      "x": 0.384,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        43,
        44
      ]
    },
    {
      "id": 48,
      "x": 0.388,
      "y": 0.785,
      "layer": 2,
      "closed_by": [
        43,
        50
      ]
    },
    {
      "id": 49,
      "x": 0.004,
      "y": 0.3,
      "layer": 1,
      "closed_by": [
        45,
        46,
        47,
        48
      ]
    },
    {
      "id": 50,
      "x": -0.002,
      "y": 0.86,
      "layer": 3,
      "closed_by": [
        38,
        39
      ]
    }
  ],
  "cards_in_layout_amount": 40,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 2.6,
    "star2": 52.3,
    "star3": 44.5
  }
}