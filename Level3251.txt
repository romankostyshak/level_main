{
  "layout_cards": [
    {
      "id": 24,
      "x": 1.713,
      "y": 0.209,
      "angle": 35.0,
      "layer": 3,
      "closed_by": [
        34,
        49
      ]
    },
    {
      "id": 25,
      "x": -1.713,
      "y": 0.199,
      "angle": 325.0,
      "layer": 3,
      "closed_by": [
        35,
        51
      ]
    },
    {
      "id": 28,
      "x": 0.003,
      "y": 0.469,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 29,
      "x": 0.004,
      "y": 0.165,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 30,
      "x": 0.007,
      "y": 0.791,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 0.002,
      "y": -0.046,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 32,
      "y": -0.272,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 33,
      "y": -0.499,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 34,
      "x": 1.292,
      "y": 0.712,
      "angle": 45.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -1.294,
      "y": 0.703,
      "angle": 315.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 36,
      "x": 1.054,
      "y": 0.949,
      "angle": 60.0,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 37,
      "x": -1.054,
      "y": 0.939,
      "angle": 300.0,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 38,
      "x": -0.722,
      "y": -0.128,
      "angle": 300.0,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 39,
      "x": 0.726,
      "y": -0.136,
      "angle": 60.0,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 40,
      "x": -0.66,
      "y": 0.949,
      "angle": 60.0,
      "layer": 2,
      "closed_by": [
        28,
        37
      ]
    },
    {
      "id": 41,
      "x": 0.441,
      "y": -0.016,
      "angle": 45.0,
      "layer": 1,
      "closed_by": [
        32,
        39
      ]
    },
    {
      "id": 42,
      "x": -0.451,
      "y": -0.017,
      "angle": 315.0,
      "layer": 1,
      "closed_by": [
        32,
        38
      ]
    },
    {
      "id": 43,
      "x": 0.444,
      "y": 0.87,
      "angle": 315.0,
      "layer": 1,
      "closed_by": [
        29,
        45
      ]
    },
    {
      "id": 44,
      "x": -0.456,
      "y": 0.884,
      "angle": 45.0,
      "layer": 1,
      "closed_by": [
        29,
        40
      ]
    },
    {
      "id": 45,
      "x": 0.652,
      "y": 0.944,
      "angle": 300.0,
      "layer": 2,
      "closed_by": [
        28,
        36
      ]
    },
    {
      "id": 46,
      "x": -2.217,
      "y": -0.275,
      "angle": 55.0,
      "layer": 3,
      "closed_by": [
        51
      ]
    },
    {
      "id": 47,
      "x": 2.2,
      "y": -0.28,
      "angle": 305.0,
      "layer": 3,
      "closed_by": [
        49
      ]
    },
    {
      "id": 48,
      "x": 1.797,
      "y": -0.039,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        24,
        47
      ]
    },
    {
      "id": 49,
      "x": 2.19,
      "y": 0.135,
      "angle": 55.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 50,
      "x": -1.809,
      "y": -0.028,
      "angle": 9.998,
      "layer": 2,
      "closed_by": [
        25,
        46
      ]
    },
    {
      "id": 51,
      "x": -2.209,
      "y": 0.128,
      "angle": 305.0,
      "layer": 4,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 26,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 54.1,
    "star2": 8.1,
    "star3": 37.1
  }
}