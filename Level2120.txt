{
  "layout_cards": [
    {
      "id": 44,
      "x": 0.386,
      "y": 0.697,
      "layer": 4,
      "closed_by": [
        25,
        23,
        21,
        43
      ]
    },
    {
      "id": 33,
      "x": -0.384,
      "y": -0.18,
      "layer": 4,
      "closed_by": [
        24,
        21
      ]
    },
    {
      "id": 48,
      "x": -0.384,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 45,
      "x": -0.384,
      "y": 0.856,
      "layer": 3,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": -0.384,
      "y": 0.697,
      "layer": 4,
      "closed_by": [
        24,
        22,
        21,
        43
      ]
    },
    {
      "id": 46,
      "x": 0.386,
      "y": 0.856,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 47,
      "x": 0.386,
      "y": 1.018,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 29,
      "x": -0.386,
      "y": -0.256,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 28,
      "x": 0.386,
      "y": -0.342,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 36,
      "x": -0.384,
      "y": -0.335,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 26,
      "x": -0.384,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 30,
      "x": 0.386,
      "y": -0.261,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 34,
      "x": 0.386,
      "y": -0.18,
      "layer": 4,
      "closed_by": [
        25,
        21
      ]
    },
    {
      "id": 27,
      "x": 0.386,
      "y": -0.421,
      "layer": 1,
      "closed_by": [
        28
      ]
    },
    {
      "id": 32,
      "x": 0.972,
      "y": 0.698,
      "layer": 4,
      "closed_by": [
        25,
        23,
        20,
        19
      ]
    },
    {
      "id": 38,
      "x": 1.667,
      "y": 0.698,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 40,
      "x": 1.432,
      "y": 0.698,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 31,
      "x": -0.971,
      "y": 0.698,
      "layer": 4,
      "closed_by": [
        24,
        22,
        37,
        18
      ]
    },
    {
      "id": 41,
      "x": -1.213,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 39,
      "x": -1.434,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 35,
      "x": -1.666,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        39,
        37,
        18
      ]
    },
    {
      "id": 25,
      "x": 0.625,
      "y": 0.143,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 24,
      "x": -0.624,
      "y": 0.143,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 0.629,
      "y": 1.019,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -0.625,
      "y": 1.019,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 21,
      "x": 0.004,
      "y": 0.143,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 37,
      "x": -1.212,
      "y": 0.136,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 20,
      "x": 1.213,
      "y": 0.136,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 19,
      "x": 1.213,
      "y": 1.019,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 42,
      "x": 1.215,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 18,
      "x": -1.215,
      "y": 1.018,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 15,
      "x": 1.213,
      "y": -0.18,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 14,
      "x": -1.212,
      "y": -0.18,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 12,
      "x": -1.212,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        14
      ]
    },
    {
      "id": 16,
      "x": 1.213,
      "y": -0.34,
      "layer": 2,
      "closed_by": [
        13
      ]
    },
    {
      "id": 10,
      "x": 1.213,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        16
      ]
    },
    {
      "id": 13,
      "x": 1.213,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        15
      ]
    },
    {
      "id": 9,
      "x": -1.212,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        11
      ]
    },
    {
      "id": 11,
      "x": -1.212,
      "y": -0.34,
      "layer": 2,
      "closed_by": [
        12
      ]
    },
    {
      "id": 49,
      "y": 1.018,
      "layer": 1,
      "closed_by": [
        48,
        47
      ]
    },
    {
      "id": 43,
      "y": 1.018,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 41,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 1.0,
    "star2": 35.5,
    "star3": 63.1
  }
}