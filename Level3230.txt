{
  "layout_cards": [
    {
      "id": 14,
      "x": -2.197,
      "y": 0.773,
      "angle": 285.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 15,
      "x": 1.792,
      "y": 0.768,
      "angle": 75.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 18,
      "x": -2.187,
      "y": 0.46,
      "angle": 300.0,
      "layer": 4,
      "closed_by": [
        14
      ]
    },
    {
      "id": 19,
      "x": -2.046,
      "y": 0.101,
      "angle": 315.0,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 20,
      "x": -1.922,
      "y": -0.31,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 21,
      "x": -1.746,
      "y": -0.537,
      "angle": 345.0,
      "layer": 1,
      "closed_by": [
        20
      ]
    },
    {
      "id": 22,
      "x": 1.519,
      "y": -0.305,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 23,
      "x": 1.786,
      "y": 0.467,
      "angle": 60.0,
      "layer": 4,
      "closed_by": [
        15
      ]
    },
    {
      "id": 24,
      "x": 1.348,
      "y": -0.537,
      "angle": 15.0,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 25,
      "x": 0.708,
      "y": -0.497,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 26,
      "x": 0.708,
      "y": 0.939,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 0.172,
      "y": -0.493,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 0.43,
      "y": -0.412,
      "layer": 4,
      "closed_by": [
        25,
        27
      ]
    },
    {
      "id": 29,
      "x": 0.432,
      "y": 0.861,
      "layer": 4,
      "closed_by": [
        26,
        36
      ]
    },
    {
      "id": 30,
      "x": -0.097,
      "y": -0.412,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 31,
      "x": -0.093,
      "y": 0.861,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 32,
      "x": 0.18,
      "y": 0.23,
      "layer": 1,
      "closed_by": [
        39,
        40
      ]
    },
    {
      "id": 33,
      "x": 0.187,
      "y": -0.333,
      "layer": 3,
      "closed_by": [
        28,
        30
      ]
    },
    {
      "id": 34,
      "x": 0.18,
      "y": 0.781,
      "layer": 3,
      "closed_by": [
        29,
        31
      ]
    },
    {
      "id": 35,
      "x": -0.333,
      "y": -0.331,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 36,
      "x": 0.165,
      "y": 0.939,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 37,
      "x": 1.639,
      "y": 0.108,
      "angle": 45.0,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 38,
      "x": -0.333,
      "y": 0.781,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 39,
      "x": -0.092,
      "y": -0.254,
      "layer": 2,
      "closed_by": [
        33,
        35
      ]
    },
    {
      "id": 40,
      "x": -0.092,
      "y": 0.702,
      "layer": 2,
      "closed_by": [
        34,
        38
      ]
    },
    {
      "id": 41,
      "x": -1.105,
      "y": 0.23,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -1.108,
      "y": -0.416,
      "layer": 5,
      "closed_by": [
        41
      ]
    },
    {
      "id": 43,
      "x": -1.105,
      "y": 0.861,
      "layer": 5,
      "closed_by": [
        41
      ]
    },
    {
      "id": 44,
      "x": -1.024,
      "y": 0.222,
      "layer": 4,
      "closed_by": [
        42,
        43
      ]
    },
    {
      "id": 45,
      "x": -0.865,
      "y": 0.782,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 46,
      "x": -0.624,
      "y": -0.256,
      "layer": 2,
      "closed_by": [
        35,
        50
      ]
    },
    {
      "id": 47,
      "x": -0.625,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        38,
        45
      ]
    },
    {
      "id": 48,
      "x": -0.331,
      "y": 0.221,
      "layer": 1,
      "closed_by": [
        39,
        40,
        46,
        47
      ]
    },
    {
      "id": 49,
      "x": -0.865,
      "y": 0.224,
      "layer": 1,
      "closed_by": [
        46,
        47
      ]
    },
    {
      "id": 50,
      "x": -0.865,
      "y": -0.331,
      "layer": 3,
      "closed_by": [
        44
      ]
    }
  ],
  "cards_in_layout_amount": 35,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 36.3,
    "star2": 56.9,
    "star3": 6.6
  }
}