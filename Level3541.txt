{
  "layout_cards": [
    {
      "id": 9,
      "x": -1.343,
      "y": -0.208,
      "angle": 350.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 10,
      "x": -1.35,
      "y": 0.652,
      "angle": 10.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 11,
      "x": 1.348,
      "y": -0.206,
      "angle": 10.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 12,
      "x": -1.906,
      "y": 0.221,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 13,
      "x": 1.906,
      "y": 0.229,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 14,
      "x": 0.007,
      "y": 0.216,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 15,
      "x": -0.703,
      "y": 0.224,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 16,
      "x": 1.348,
      "y": 0.657,
      "angle": 350.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 18,
      "x": 0.703,
      "y": 0.229,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 19,
      "x": 0.004,
      "y": -0.184,
      "layer": 4,
      "closed_by": [
        14
      ]
    },
    {
      "id": 20,
      "x": 0.004,
      "y": 0.634,
      "layer": 4,
      "closed_by": [
        14
      ]
    },
    {
      "id": 21,
      "x": 0.703,
      "y": -0.259,
      "layer": 4,
      "closed_by": [
        18
      ]
    },
    {
      "id": 22,
      "x": 0.703,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        18
      ]
    },
    {
      "id": 23,
      "x": -0.707,
      "y": 0.702,
      "layer": 4,
      "closed_by": [
        15
      ]
    },
    {
      "id": 24,
      "x": -0.333,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        19,
        37
      ]
    },
    {
      "id": 25,
      "x": 0.335,
      "y": -0.34,
      "layer": 3,
      "closed_by": [
        19,
        21
      ]
    },
    {
      "id": 26,
      "x": 0.333,
      "y": 0.782,
      "layer": 3,
      "closed_by": [
        20,
        22
      ]
    },
    {
      "id": 27,
      "x": -0.333,
      "y": 0.782,
      "layer": 3,
      "closed_by": [
        20,
        23
      ]
    },
    {
      "id": 28,
      "x": 0.001,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        26,
        27
      ]
    },
    {
      "id": 29,
      "x": 1.929,
      "y": -0.27,
      "angle": 10.0,
      "layer": 5,
      "closed_by": [
        13
      ]
    },
    {
      "id": 30,
      "x": 1.526,
      "y": -0.34,
      "angle": 10.0,
      "layer": 4,
      "closed_by": [
        11,
        29
      ]
    },
    {
      "id": 31,
      "x": 1.131,
      "y": -0.411,
      "angle": 10.0,
      "layer": 3,
      "closed_by": [
        21,
        30
      ]
    },
    {
      "id": 32,
      "x": 0.726,
      "y": -0.479,
      "angle": 10.0,
      "layer": 2,
      "closed_by": [
        25,
        31
      ]
    },
    {
      "id": 33,
      "x": 0.358,
      "y": -0.544,
      "angle": 10.0,
      "layer": 1,
      "closed_by": [
        32,
        36
      ]
    },
    {
      "id": 34,
      "x": 1.929,
      "y": 0.702,
      "angle": 350.0,
      "layer": 5,
      "closed_by": [
        13
      ]
    },
    {
      "id": 35,
      "x": 1.529,
      "y": 0.777,
      "angle": 350.0,
      "layer": 4,
      "closed_by": [
        16,
        34
      ]
    },
    {
      "id": 36,
      "y": -0.578,
      "layer": 2,
      "closed_by": [
        24,
        25
      ]
    },
    {
      "id": 37,
      "x": -0.703,
      "y": -0.261,
      "layer": 4,
      "closed_by": [
        15
      ]
    },
    {
      "id": 38,
      "x": 1.131,
      "y": 0.847,
      "angle": 350.0,
      "layer": 3,
      "closed_by": [
        22,
        35
      ]
    },
    {
      "id": 39,
      "x": 0.731,
      "y": 0.921,
      "angle": 350.0,
      "layer": 2,
      "closed_by": [
        26,
        38
      ]
    },
    {
      "id": 40,
      "x": 0.356,
      "y": 0.99,
      "angle": 350.0,
      "layer": 1,
      "closed_by": [
        28,
        39
      ]
    },
    {
      "id": 41,
      "x": -1.929,
      "y": 0.702,
      "angle": 10.0,
      "layer": 5,
      "closed_by": [
        12
      ]
    },
    {
      "id": 42,
      "x": -1.531,
      "y": 0.773,
      "angle": 10.0,
      "layer": 4,
      "closed_by": [
        10,
        41
      ]
    },
    {
      "id": 43,
      "x": -1.133,
      "y": 0.847,
      "angle": 10.0,
      "layer": 3,
      "closed_by": [
        23,
        42
      ]
    },
    {
      "id": 44,
      "x": -0.731,
      "y": 0.921,
      "angle": 10.0,
      "layer": 2,
      "closed_by": [
        27,
        43
      ]
    },
    {
      "id": 45,
      "x": -1.934,
      "y": -0.252,
      "angle": 350.0,
      "layer": 5,
      "closed_by": [
        12
      ]
    },
    {
      "id": 46,
      "x": -1.531,
      "y": -0.331,
      "angle": 350.0,
      "layer": 4,
      "closed_by": [
        9,
        45
      ]
    },
    {
      "id": 47,
      "x": -0.73,
      "y": -0.476,
      "angle": 350.0,
      "layer": 2,
      "closed_by": [
        24,
        48
      ]
    },
    {
      "id": 48,
      "x": -1.133,
      "y": -0.404,
      "angle": 350.0,
      "layer": 3,
      "closed_by": [
        37,
        46
      ]
    },
    {
      "id": 49,
      "x": -0.361,
      "y": -0.544,
      "angle": 350.0,
      "layer": 1,
      "closed_by": [
        36,
        47
      ]
    },
    {
      "id": 50,
      "x": -0.356,
      "y": 0.99,
      "angle": 10.0,
      "layer": 1,
      "closed_by": [
        28,
        44
      ]
    }
  ],
  "cards_in_layout_amount": 41,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 12.2,
    "star2": 73.9,
    "star3": 12.9
  }
}