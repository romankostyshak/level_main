{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.331,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        50,
        48
      ]
    },
    {
      "id": 50,
      "x": 0.731,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 49,
      "x": -0.328,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        48,
        37
      ]
    },
    {
      "id": 48,
      "x": 0.001,
      "y": 0.216,
      "layer": 2,
      "closed_by": [
        43,
        42,
        41,
        40
      ]
    },
    {
      "id": 47,
      "x": 0.333,
      "y": -0.256,
      "layer": 1,
      "closed_by": [
        48,
        45
      ]
    },
    {
      "id": 46,
      "x": -0.331,
      "y": -0.254,
      "layer": 1,
      "closed_by": [
        48,
        44
      ]
    },
    {
      "id": 45,
      "x": 0.731,
      "y": -0.414,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 44,
      "x": -0.73,
      "y": -0.416,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 43,
      "x": -0.386,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 42,
      "x": 0.388,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 41,
      "x": 0.388,
      "y": -0.256,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 40,
      "x": -0.384,
      "y": -0.256,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 39,
      "x": 0.55,
      "y": 0.819,
      "angle": 315.0,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 38,
      "x": -0.55,
      "y": 0.828,
      "angle": 45.0,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 37,
      "x": -0.73,
      "y": 0.861,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 36,
      "x": 0.546,
      "y": -0.377,
      "angle": 45.0,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 35,
      "x": -0.545,
      "y": -0.384,
      "angle": 315.0,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 34,
      "x": -0.731,
      "y": 0.216,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 0.731,
      "y": 0.222,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 1.289,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        26
      ]
    },
    {
      "id": 31,
      "x": -1.292,
      "y": 0.861,
      "layer": 1,
      "closed_by": [
        27
      ]
    },
    {
      "id": 30,
      "x": 1.289,
      "y": 0.859,
      "layer": 1,
      "closed_by": [
        28
      ]
    },
    {
      "id": 29,
      "x": -1.292,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        25
      ]
    },
    {
      "id": 26,
      "x": 1.475,
      "y": -0.386,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 21,
      "x": -1.664,
      "y": 0.379,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 27,
      "x": -1.475,
      "y": 0.819,
      "angle": 315.0,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 28,
      "x": 1.473,
      "y": 0.824,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 22,
      "x": 1.661,
      "y": 0.059,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 1.661,
      "y": 0.379,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 23,
      "x": -1.661,
      "y": 0.059,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 25,
      "x": -1.48,
      "y": -0.381,
      "angle": 45.0,
      "layer": 2,
      "closed_by": [
        23
      ]
    }
  ],
  "cards_in_layout_amount": 31,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 36.4,
    "star2": 54.1,
    "star3": 8.5
  }
}