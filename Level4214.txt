{
  "layout_cards": [
    {
      "id": 24,
      "x": -0.259,
      "y": 0.216,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 0.259,
      "y": 0.216,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -1.457,
      "y": 0.216,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 1.46,
      "y": 0.216,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 0.86,
      "y": 0.216,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -0.298,
      "y": -0.199,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 30,
      "x": 0.298,
      "y": -0.199,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 31,
      "x": 0.298,
      "y": 0.638,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 32,
      "x": -0.298,
      "y": 0.638,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 33,
      "x": 1.22,
      "y": 0.18,
      "angle": 300.0,
      "layer": 3,
      "closed_by": [
        27,
        28
      ]
    },
    {
      "id": 34,
      "x": -1.22,
      "y": 0.18,
      "angle": 60.0,
      "layer": 3,
      "closed_by": [
        26,
        36
      ]
    },
    {
      "id": 35,
      "x": 0.5,
      "y": -0.238,
      "angle": 300.0,
      "layer": 2,
      "closed_by": [
        28,
        30
      ]
    },
    {
      "id": 36,
      "x": -0.859,
      "y": 0.216,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -0.499,
      "y": -0.238,
      "angle": 60.0,
      "layer": 2,
      "closed_by": [
        29,
        36
      ]
    },
    {
      "id": 39,
      "x": -1.22,
      "y": 0.259,
      "angle": 300.0,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 40,
      "x": -0.499,
      "y": 0.679,
      "angle": 300.0,
      "layer": 2,
      "closed_by": [
        32,
        36
      ]
    },
    {
      "id": 41,
      "x": 1.22,
      "y": 0.259,
      "angle": 60.0,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 42,
      "x": 0.5,
      "y": 0.679,
      "angle": 60.0,
      "layer": 2,
      "closed_by": [
        28,
        31
      ]
    },
    {
      "id": 43,
      "x": 0.298,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 44,
      "x": -0.298,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 45,
      "x": 1.419,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        41
      ]
    },
    {
      "id": 46,
      "x": -0.819,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        38,
        39,
        40
      ]
    },
    {
      "id": 47,
      "x": 0.819,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        35,
        41,
        42
      ]
    },
    {
      "id": 48,
      "x": 0.298,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 49,
      "x": -0.298,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 50,
      "x": -1.419,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        39
      ]
    }
  ],
  "cards_in_layout_amount": 26,
  "cards_in_deck_amount": 24,
  "layers_in_level": 4,
  "open_cards": 6,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 41.9,
    "star2": 55.3,
    "star3": 2.3
  }
}