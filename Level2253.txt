{
  "layout_cards": [
    {
      "id": 16,
      "x": -2.707,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        18
      ]
    },
    {
      "id": 17,
      "x": 2.706,
      "y": -0.333,
      "layer": 1,
      "closed_by": [
        19
      ]
    },
    {
      "id": 18,
      "x": -2.467,
      "y": -0.1,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 19,
      "x": 2.467,
      "y": -0.093,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 20,
      "x": 2.226,
      "y": 0.143,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 21,
      "x": -1.985,
      "y": 0.379,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 22,
      "x": -1.745,
      "y": 0.615,
      "layer": 5,
      "closed_by": [
        32
      ]
    },
    {
      "id": 23,
      "x": 1.983,
      "y": 0.381,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 24,
      "x": 1.746,
      "y": 0.62,
      "layer": 5,
      "closed_by": [
        33
      ]
    },
    {
      "id": 25,
      "x": -0.001,
      "y": 0.861,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 26,
      "x": 0.305,
      "y": 0.541,
      "layer": 4,
      "closed_by": [
        25,
        50
      ]
    },
    {
      "id": 27,
      "x": -0.002,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        28,
        29
      ]
    },
    {
      "id": 28,
      "x": 0.331,
      "y": 0.86,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 29,
      "x": -0.331,
      "y": 0.86,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 30,
      "x": 0.572,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        26,
        38
      ]
    },
    {
      "id": 31,
      "x": -0.569,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        35,
        39
      ]
    },
    {
      "id": 32,
      "x": -1.503,
      "y": 0.859,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 1.508,
      "y": 0.86,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -1.184,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        32
      ]
    },
    {
      "id": 35,
      "x": -0.305,
      "y": 0.541,
      "layer": 4,
      "closed_by": [
        25,
        51
      ]
    },
    {
      "id": 36,
      "x": -2.226,
      "y": 0.143,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 37,
      "x": 1.187,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        33
      ]
    },
    {
      "id": 38,
      "x": 0.865,
      "y": 0.541,
      "layer": 4,
      "closed_by": [
        37,
        50
      ]
    },
    {
      "id": 39,
      "x": -0.865,
      "y": 0.54,
      "layer": 4,
      "closed_by": [
        34,
        51
      ]
    },
    {
      "id": 40,
      "x": -0.865,
      "y": -0.337,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 0.87,
      "y": -0.337,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 42,
      "x": 0.31,
      "y": -0.337,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 43,
      "x": -0.303,
      "y": -0.337,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 44,
      "x": 0.549,
      "y": -0.416,
      "layer": 3,
      "closed_by": [
        41,
        42
      ]
    },
    {
      "id": 45,
      "x": -0.384,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        46,
        49
      ]
    },
    {
      "id": 46,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        42,
        43
      ]
    },
    {
      "id": 47,
      "x": 0.386,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        44,
        46
      ]
    },
    {
      "id": 48,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        45,
        47
      ]
    },
    {
      "id": 49,
      "x": -0.546,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        40,
        43
      ]
    },
    {
      "id": 50,
      "x": 0.625,
      "y": 0.861,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -0.628,
      "y": 0.86,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 36,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 10.7,
    "star2": 69.7,
    "star3": 19.1
  }
}