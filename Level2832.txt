{
  "layout_cards": [
    {
      "id": 28,
      "x": -2.545,
      "y": 0.379,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -1.69,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 30,
      "x": -2.226,
      "y": 0.943,
      "layer": 1,
      "closed_by": [
        32
      ]
    },
    {
      "id": 31,
      "x": -1.69,
      "y": 0.625,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 32,
      "x": -2.226,
      "y": 0.623,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 33,
      "x": -1.371,
      "y": 0.384,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -1.024,
      "y": 0.623,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 35,
      "x": -2.891,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 36,
      "x": -1.967,
      "y": -0.254,
      "layer": 1,
      "closed_by": [
        37,
        38
      ]
    },
    {
      "id": 37,
      "x": -1.531,
      "y": -0.495,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -2.384,
      "y": -0.495,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 39,
      "x": 1.521,
      "y": 0.305,
      "layer": 1,
      "closed_by": [
        43,
        44
      ]
    },
    {
      "id": 40,
      "x": 2.255,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 41,
      "x": 0.493,
      "y": -0.178,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 42,
      "x": 0.786,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 43,
      "x": 1.521,
      "y": -0.171,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 44,
      "x": 1.521,
      "y": 0.777,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 45,
      "x": 2.549,
      "y": 0.777,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 46,
      "x": 0.493,
      "y": 0.777,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 47,
      "x": 2.253,
      "y": -0.172,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 48,
      "x": 0.787,
      "y": -0.172,
      "layer": 1,
      "closed_by": [
        41
      ]
    },
    {
      "id": 49,
      "x": 2.546,
      "y": -0.172,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 50,
      "x": -1.131,
      "y": -0.499,
      "layer": 1,
      "closed_by": [
        37
      ]
    },
    {
      "id": 51,
      "x": -2.786,
      "y": -0.499,
      "layer": 1,
      "closed_by": [
        38
      ]
    }
  ],
  "level_mechanic": 2,
  "free_cards_from_stickers": [
    28,
    29,
    30,
    31,
    32,
    33,
    34,
    35,
    36,
    37,
    38,
    50,
    51
  ],
  "cards_in_layout_amount": 24,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 52.2,
    "star2": 41.1,
    "star3": 6.0
  }
}