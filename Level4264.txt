{
  "layout_cards": [
    {
      "id": 22,
      "x": -0.939,
      "y": 0.819,
      "layer": 1,
      "closed_by": [
        26,
        41
      ]
    },
    {
      "id": 23,
      "x": -0.939,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        27,
        41
      ]
    },
    {
      "id": 24,
      "x": 0.939,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        29,
        42
      ]
    },
    {
      "id": 25,
      "x": 0.939,
      "y": -0.216,
      "layer": 1,
      "closed_by": [
        30,
        42
      ]
    },
    {
      "id": 26,
      "x": -1.258,
      "y": 0.819,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 27,
      "x": -1.258,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 28,
      "x": -1.58,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 29,
      "x": 1.258,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 30,
      "x": 1.258,
      "y": -0.216,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 31,
      "x": 1.577,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 32,
      "x": 1.577,
      "y": -0.216,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 33,
      "x": -1.58,
      "y": 0.2,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 34,
      "x": 1.577,
      "y": 0.398,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 35,
      "x": -1.258,
      "y": 0.2,
      "layer": 5,
      "closed_by": [
        39
      ]
    },
    {
      "id": 36,
      "x": -1.58,
      "y": 0.819,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 38,
      "x": 1.258,
      "y": 0.398,
      "layer": 5,
      "closed_by": [
        40
      ]
    },
    {
      "id": 39,
      "x": -0.939,
      "y": 0.2,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 0.939,
      "y": 0.398,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 41,
      "x": -0.62,
      "y": 0.2,
      "layer": 5,
      "closed_by": [
        39
      ]
    },
    {
      "id": 42,
      "x": 0.62,
      "y": 0.398,
      "layer": 5,
      "closed_by": [
        40
      ]
    },
    {
      "id": 43,
      "x": -0.298,
      "y": 0.2,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 44,
      "x": 0.298,
      "y": 0.398,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 45,
      "x": 0.119,
      "y": -0.259,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        44,
        50
      ]
    },
    {
      "id": 46,
      "x": 0.158,
      "y": 0.98,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 47,
      "x": -0.418,
      "y": 0.819,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 48,
      "x": -0.119,
      "y": 0.859,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        43,
        46
      ]
    },
    {
      "id": 49,
      "x": 0.418,
      "y": -0.216,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 50,
      "x": -0.158,
      "y": -0.379,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        43
      ]
    }
  ],
  "cards_in_layout_amount": 28,
  "cards_in_deck_amount": 26,
  "layers_in_level": 6,
  "open_cards": 2,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 43.6,
    "star2": 51.8,
    "star3": 4.4
  }
}