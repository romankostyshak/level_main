{
  "layout_cards": [
    {
      "id": 29,
      "x": -2.789,
      "y": 0.781,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -1.751,
      "y": 0.781,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 2.773,
      "y": -0.254,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 1.738,
      "y": -0.256,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 1.365,
      "y": 0.699,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 0.331,
      "y": 0.699,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -0.331,
      "y": -0.172,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 38,
      "x": -1.366,
      "y": -0.172,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -0.851,
      "y": -0.179,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 0.847,
      "y": 0.699,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 2.253,
      "y": -0.256,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -2.269,
      "y": 0.777,
      "layer": 2,
      "closed_by": []
    },
    {
      "id": 43,
      "x": 0.546,
      "y": 0.303,
      "layer": 1,
      "closed_by": [
        34,
        40
      ]
    },
    {
      "id": 44,
      "x": -2.572,
      "y": 0.381,
      "layer": 1,
      "closed_by": [
        29,
        42
      ]
    },
    {
      "id": 45,
      "x": 1.949,
      "y": 0.141,
      "layer": 1,
      "closed_by": [
        32,
        41
      ]
    },
    {
      "id": 46,
      "x": 1.154,
      "y": 0.3,
      "layer": 1,
      "closed_by": [
        33,
        40
      ]
    },
    {
      "id": 47,
      "x": -1.957,
      "y": 0.381,
      "layer": 1,
      "closed_by": [
        30,
        42
      ]
    },
    {
      "id": 48,
      "x": -1.161,
      "y": 0.222,
      "layer": 1,
      "closed_by": [
        38,
        39
      ]
    },
    {
      "id": 49,
      "x": -0.546,
      "y": 0.222,
      "layer": 1,
      "closed_by": [
        35,
        39
      ]
    },
    {
      "id": 50,
      "x": 2.561,
      "y": 0.142,
      "layer": 1,
      "closed_by": [
        31,
        41
      ]
    }
  ],
  "level_mechanic": 1,
  "free_cards_from_stickers": [
    31,
    32,
    33,
    34,
    40,
    41,
    43,
    45,
    46,
    50
  ],
  "cards_in_layout_amount": 20,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 43.3,
    "star2": 3.9,
    "star3": 52.4
  }
}