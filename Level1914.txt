{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.254,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": -0.001,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        49
      ]
    },
    {
      "id": 49,
      "x": 0.224,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 48,
      "x": 0.465,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "x": 0.703,
      "y": -0.256,
      "layer": 5,
      "closed_by": [
        41
      ]
    },
    {
      "id": 46,
      "x": -0.786,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 45,
      "x": -0.865,
      "y": -0.335,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": -0.944,
      "y": -0.093,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 43,
      "x": -1.026,
      "y": 0.15,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 42,
      "x": -1.11,
      "y": 0.397,
      "layer": 5,
      "closed_by": [
        35
      ]
    },
    {
      "id": 41,
      "x": 0.948,
      "y": -0.18,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -1.187,
      "y": 0.62,
      "layer": 6,
      "closed_by": [
        19
      ]
    },
    {
      "id": 34,
      "x": -1.773,
      "y": 0.62,
      "layer": 6,
      "closed_by": [
        19
      ]
    },
    {
      "id": 33,
      "x": -1.85,
      "y": 0.384,
      "layer": 5,
      "closed_by": [
        34
      ]
    },
    {
      "id": 32,
      "x": -1.932,
      "y": 0.142,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 31,
      "x": -2.013,
      "y": -0.101,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 30,
      "x": -2.092,
      "y": -0.335,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 29,
      "x": -2.174,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        30
      ]
    },
    {
      "id": 28,
      "x": 0.943,
      "y": 0.702,
      "layer": 1,
      "closed_by": [
        27,
        26
      ]
    },
    {
      "id": 27,
      "x": 0.643,
      "y": 0.781,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 26,
      "x": 1.266,
      "y": 0.782,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 25,
      "x": 0.411,
      "y": 0.864,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 24,
      "x": 1.503,
      "y": 0.861,
      "layer": 3,
      "closed_by": [
        22
      ]
    },
    {
      "id": 23,
      "x": 0.165,
      "y": 0.943,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 22,
      "x": 1.746,
      "y": 0.943,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 21,
      "x": -0.067,
      "y": 1.021,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 20,
      "x": 1.98,
      "y": 1.019,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 19,
      "x": -1.48,
      "y": 0.699,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 1.269,
      "y": -0.259,
      "layer": 5,
      "closed_by": [
        41
      ]
    },
    {
      "id": 39,
      "x": 1.508,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        40
      ]
    },
    {
      "id": 38,
      "x": 1.748,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 37,
      "x": 1.988,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 36,
      "x": 2.226,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        37
      ]
    }
  ],
  "cards_in_layout_amount": 33,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 29.8,
    "star2": 64.0,
    "star3": 5.6
  }
}