{
  "layout_cards": [
    {
      "x": 2.259,
      "y": 1.059,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 1,
      "x": -2.259,
      "y": -0.578,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 2,
      "x": -2.259,
      "y": 1.059,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 3,
      "x": -2.078,
      "y": 0.638,
      "layer": 2,
      "closed_by": [
        2
      ]
    },
    {
      "id": 4,
      "x": 2.078,
      "y": 0.638,
      "layer": 2,
      "closed_by": [
        0
      ]
    },
    {
      "id": 5,
      "x": 2.078,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        7
      ]
    },
    {
      "id": 6,
      "x": -2.078,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        1
      ]
    },
    {
      "id": 7,
      "x": 2.259,
      "y": -0.578,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 8,
      "x": -1.878,
      "y": 1.059,
      "layer": 1,
      "closed_by": [
        3
      ]
    },
    {
      "id": 9,
      "x": -1.878,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        6
      ]
    },
    {
      "id": 10,
      "x": 1.878,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        5
      ]
    },
    {
      "id": 11,
      "x": 1.878,
      "y": 0.238,
      "layer": 1,
      "closed_by": [
        4,
        5
      ]
    },
    {
      "id": 12,
      "x": -1.12,
      "y": -0.578,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 13,
      "x": 1.098,
      "y": -0.578,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 14,
      "x": -1.358,
      "y": -0.5,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 15,
      "x": 1.358,
      "y": -0.5,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 16,
      "x": 1.878,
      "y": 1.059,
      "layer": 1,
      "closed_by": [
        4
      ]
    },
    {
      "id": 17,
      "x": -1.878,
      "y": 0.238,
      "layer": 1,
      "closed_by": [
        3,
        6
      ]
    },
    {
      "id": 18,
      "x": -1.358,
      "y": 1.059,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 19,
      "x": 1.358,
      "y": 1.059,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 20,
      "x": -1.358,
      "y": 0.72,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 21,
      "x": -1.1,
      "y": 1.059,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 22,
      "x": 1.098,
      "y": 1.059,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 23,
      "x": -1.358,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        12
      ]
    },
    {
      "id": 24,
      "x": 1.358,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        13
      ]
    },
    {
      "id": 25,
      "x": -0.837,
      "y": 1.059,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 26,
      "x": 0.837,
      "y": 1.059,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 27,
      "x": -1.358,
      "y": 0.398,
      "layer": 2,
      "closed_by": [
        20
      ]
    },
    {
      "id": 28,
      "x": -1.358,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        14
      ]
    },
    {
      "id": 29,
      "x": 1.358,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        15
      ]
    },
    {
      "id": 30,
      "x": -1.358,
      "y": -0.017,
      "layer": 1,
      "closed_by": [
        27,
        28
      ]
    },
    {
      "id": 31,
      "x": 1.358,
      "y": -0.017,
      "layer": 1,
      "closed_by": [
        29,
        36
      ]
    },
    {
      "id": 32,
      "y": 1.019,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 0.837,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        13
      ]
    },
    {
      "id": 34,
      "x": -0.837,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        12
      ]
    },
    {
      "id": 35,
      "y": 0.638,
      "layer": 2,
      "closed_by": [
        32,
        38
      ]
    },
    {
      "id": 36,
      "x": 1.358,
      "y": 0.398,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 37,
      "x": 1.358,
      "y": 0.72,
      "layer": 3,
      "closed_by": [
        19
      ]
    },
    {
      "id": 38,
      "y": 0.2,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -0.837,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        34,
        43
      ]
    },
    {
      "id": 40,
      "x": 0.837,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        33,
        44
      ]
    },
    {
      "id": 41,
      "x": -0.537,
      "y": -0.537,
      "angle": 300.0,
      "layer": 1,
      "closed_by": [
        39,
        46
      ]
    },
    {
      "id": 42,
      "x": 0.518,
      "y": -0.518,
      "angle": 60.0,
      "layer": 1,
      "closed_by": [
        40,
        47
      ]
    },
    {
      "id": 43,
      "x": -0.837,
      "y": 0.238,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 44,
      "x": 0.837,
      "y": 0.238,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 45,
      "x": -0.518,
      "y": 0.638,
      "layer": 3,
      "closed_by": [
        25,
        43
      ]
    },
    {
      "id": 46,
      "x": -0.319,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 47,
      "x": 0.319,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 48,
      "x": -0.518,
      "y": 0.158,
      "angle": 300.0,
      "layer": 1,
      "closed_by": [
        35,
        39,
        45,
        46
      ]
    },
    {
      "id": 49,
      "x": 0.518,
      "y": 0.158,
      "angle": 60.0,
      "layer": 1,
      "closed_by": [
        35,
        40,
        47,
        50
      ]
    },
    {
      "id": 50,
      "x": 0.518,
      "y": 0.638,
      "layer": 3,
      "closed_by": [
        26,
        44
      ]
    }
  ],
  "cards_in_layout_amount": 51,
  "cards_in_deck_amount": 26,
  "layers_in_level": 5,
  "open_cards": 12,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 0.0,
    "star2": 30.2,
    "star3": 69.4
  }
}