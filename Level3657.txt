{
  "layout_cards": [
    {
      "id": 11,
      "x": -2.058,
      "y": 0.282,
      "angle": 330.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 12,
      "x": 2.065,
      "y": 0.293,
      "angle": 30.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 13,
      "x": -1.988,
      "y": 0.202,
      "angle": 344.997,
      "layer": 4,
      "closed_by": [
        11
      ]
    },
    {
      "id": 14,
      "x": -1.906,
      "y": 0.136,
      "layer": 3,
      "closed_by": [
        13
      ]
    },
    {
      "id": 15,
      "x": 1.906,
      "y": 0.14,
      "layer": 3,
      "closed_by": [
        19
      ]
    },
    {
      "id": 16,
      "x": -1.824,
      "y": 0.104,
      "angle": 10.0,
      "layer": 2,
      "closed_by": [
        14
      ]
    },
    {
      "id": 17,
      "x": -1.595,
      "y": 0.1,
      "angle": 45.0,
      "layer": 1,
      "closed_by": [
        16,
        40
      ]
    },
    {
      "id": 18,
      "x": 1.789,
      "y": 0.096,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        15
      ]
    },
    {
      "id": 19,
      "x": 1.998,
      "y": 0.209,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        12
      ]
    },
    {
      "id": 20,
      "x": 1.585,
      "y": 0.097,
      "angle": 315.0,
      "layer": 1,
      "closed_by": [
        18,
        41
      ]
    },
    {
      "id": 21,
      "x": -1.207,
      "y": -0.488,
      "angle": 315.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -0.947,
      "y": -0.495,
      "layer": 5,
      "closed_by": [
        21
      ]
    },
    {
      "id": 23,
      "x": 0.947,
      "y": -0.495,
      "layer": 5,
      "closed_by": [
        38
      ]
    },
    {
      "id": 24,
      "x": -0.998,
      "y": -0.483,
      "angle": 45.0,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 25,
      "x": 0.986,
      "y": -0.486,
      "angle": 315.0,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 26,
      "x": -1.121,
      "y": 0.971,
      "angle": 330.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 1.12,
      "y": 0.976,
      "angle": 30.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 1.213,
      "y": 0.828,
      "angle": 15.0,
      "layer": 5,
      "closed_by": [
        27
      ]
    },
    {
      "id": 29,
      "y": 0.544,
      "layer": 8,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -0.143,
      "y": 0.43,
      "angle": 15.0,
      "layer": 7,
      "closed_by": [
        29
      ]
    },
    {
      "id": 31,
      "x": 0.143,
      "y": 0.425,
      "angle": 344.997,
      "layer": 6,
      "closed_by": [
        30
      ]
    },
    {
      "id": 32,
      "x": 0.243,
      "y": 0.25,
      "angle": 315.0,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 33,
      "x": -0.263,
      "y": 0.268,
      "angle": 45.0,
      "layer": 4,
      "closed_by": [
        32
      ]
    },
    {
      "id": 34,
      "x": -1.266,
      "y": 0.702,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 35,
      "x": 1.268,
      "y": 0.702,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 36,
      "x": -1.297,
      "y": 0.587,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 37,
      "x": -1.213,
      "y": 0.823,
      "angle": 344.997,
      "layer": 5,
      "closed_by": [
        26
      ]
    },
    {
      "id": 38,
      "x": 1.197,
      "y": -0.481,
      "angle": 45.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 39,
      "x": 1.281,
      "y": 0.583,
      "angle": 344.997,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 40,
      "x": -1.274,
      "y": 0.497,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 41,
      "x": 1.261,
      "y": 0.493,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 42,
      "x": -1.21,
      "y": 0.465,
      "angle": 45.0,
      "layer": 1,
      "closed_by": [
        40
      ]
    },
    {
      "id": 43,
      "x": 1.207,
      "y": 0.456,
      "angle": 315.0,
      "layer": 1,
      "closed_by": [
        41
      ]
    },
    {
      "id": 44,
      "x": 0.62,
      "y": -0.128,
      "angle": 315.0,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 45,
      "x": 0.508,
      "y": -0.23,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        25,
        32,
        44
      ]
    },
    {
      "id": 46,
      "x": 0.412,
      "y": -0.497,
      "layer": 1,
      "closed_by": [
        51
      ]
    },
    {
      "id": 47,
      "x": -0.632,
      "y": -0.123,
      "angle": 45.0,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 48,
      "x": -0.513,
      "y": -0.217,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        24,
        33,
        47
      ]
    },
    {
      "id": 49,
      "x": -0.414,
      "y": -0.372,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        30,
        48
      ]
    },
    {
      "id": 50,
      "x": -0.412,
      "y": -0.497,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 51,
      "x": 0.414,
      "y": -0.379,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        45
      ]
    }
  ],
  "cards_in_layout_amount": 41,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 16.5,
    "star2": 75.4,
    "star3": 7.7
  }
}