{
  "layout_cards": [
    {
      "id": 12,
      "y": 1.019,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 13,
      "x": 0.307,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        12
      ]
    },
    {
      "id": 14,
      "x": -0.305,
      "y": 0.939,
      "layer": 4,
      "closed_by": [
        12
      ]
    },
    {
      "id": 15,
      "x": -2.703,
      "y": 1.019,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 18,
      "x": 2.703,
      "y": 0.939,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 19,
      "x": -2.467,
      "y": 0.62,
      "layer": 5,
      "closed_by": [
        15
      ]
    },
    {
      "id": 20,
      "x": 2.463,
      "y": 0.62,
      "layer": 5,
      "closed_by": [
        18
      ]
    },
    {
      "id": 21,
      "x": 2.229,
      "y": 0.381,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 22,
      "x": -1.985,
      "y": -0.256,
      "layer": 3,
      "closed_by": [
        51
      ]
    },
    {
      "id": 23,
      "x": -1.985,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        51
      ]
    },
    {
      "id": 24,
      "x": 1.985,
      "y": 0.615,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 25,
      "x": 1.985,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 26,
      "x": -1.424,
      "y": -0.259,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 27,
      "x": 1.427,
      "y": -0.259,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 1.424,
      "y": 0.62,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -1.105,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        26,
        41
      ]
    },
    {
      "id": 30,
      "x": -1.105,
      "y": 0.476,
      "layer": 2,
      "closed_by": [
        26,
        36,
        39
      ]
    },
    {
      "id": 31,
      "x": 1.108,
      "y": -0.34,
      "layer": 2,
      "closed_by": [
        27,
        50
      ]
    },
    {
      "id": 32,
      "x": 1.108,
      "y": 0.472,
      "layer": 2,
      "closed_by": [
        27,
        28,
        40
      ]
    },
    {
      "id": 33,
      "x": -0.865,
      "y": 0.619,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 0.865,
      "y": 0.62,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -0.865,
      "y": -0.254,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -1.424,
      "y": 0.619,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 0.865,
      "y": -0.256,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -0.703,
      "y": 0.059,
      "layer": 4,
      "closed_by": [
        33,
        35
      ]
    },
    {
      "id": 40,
      "x": 0.707,
      "y": 0.061,
      "layer": 4,
      "closed_by": [
        34,
        38
      ]
    },
    {
      "id": 41,
      "x": -0.625,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 42,
      "x": -0.545,
      "y": 0.541,
      "layer": 3,
      "closed_by": [
        14,
        39
      ]
    },
    {
      "id": 43,
      "x": -0.305,
      "y": -0.414,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 44,
      "x": -0.305,
      "y": 0.481,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 45,
      "x": 0.546,
      "y": 0.54,
      "layer": 3,
      "closed_by": [
        13,
        40
      ]
    },
    {
      "id": 46,
      "x": 0.307,
      "y": 0.479,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 47,
      "x": -0.001,
      "y": 0.405,
      "layer": 1,
      "closed_by": [
        13,
        14,
        44,
        46
      ]
    },
    {
      "id": 48,
      "x": 0.303,
      "y": -0.416,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 49,
      "x": -0.007,
      "y": -0.493,
      "layer": 1,
      "closed_by": [
        43,
        48
      ]
    },
    {
      "id": 50,
      "x": 0.625,
      "y": -0.335,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 51,
      "x": -2.223,
      "y": 0.377,
      "layer": 4,
      "closed_by": [
        19
      ]
    }
  ],
  "cards_in_layout_amount": 37,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 8.6,
    "star2": 63.5,
    "star3": 27.3
  }
}