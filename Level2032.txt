{
  "layout_cards": [
    {
      "id": 5,
      "x": -1.769,
      "y": 0.62,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 8,
      "x": -1.189,
      "y": 0.62,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 9,
      "x": 1.189,
      "y": 0.62,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 10,
      "x": -1.983,
      "y": -0.5,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 11,
      "x": 1.985,
      "y": -0.497,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 12,
      "x": 1.985,
      "y": 0.54,
      "layer": 3,
      "closed_by": [
        17
      ]
    },
    {
      "id": 13,
      "x": -1.661,
      "y": 0.46,
      "layer": 2,
      "closed_by": [
        16,
        27
      ]
    },
    {
      "id": 14,
      "x": 1.667,
      "y": 0.46,
      "layer": 2,
      "closed_by": [
        12,
        28
      ]
    },
    {
      "id": 15,
      "x": -1.664,
      "y": -0.578,
      "layer": 5,
      "closed_by": [
        10,
        24
      ]
    },
    {
      "id": 16,
      "x": -1.983,
      "y": 0.537,
      "layer": 3,
      "closed_by": [
        5
      ]
    },
    {
      "id": 17,
      "x": 1.773,
      "y": 0.62,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 18,
      "x": 1.667,
      "y": -0.578,
      "layer": 5,
      "closed_by": [
        11,
        25
      ]
    },
    {
      "id": 20,
      "x": 0.001,
      "y": -0.017,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 21,
      "y": 0.86,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -0.625,
      "y": 0.86,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 23,
      "x": 0.628,
      "y": 0.861,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 24,
      "x": -1.424,
      "y": -0.499,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 1.424,
      "y": -0.499,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 26,
      "x": 1.105,
      "y": -0.578,
      "layer": 5,
      "closed_by": [
        25
      ]
    },
    {
      "id": 27,
      "x": -1.424,
      "y": 0.54,
      "layer": 3,
      "closed_by": [
        5,
        8
      ]
    },
    {
      "id": 28,
      "x": 1.427,
      "y": 0.54,
      "layer": 3,
      "closed_by": [
        9,
        17
      ]
    },
    {
      "id": 29,
      "x": -0.333,
      "y": 0.136,
      "layer": 5,
      "closed_by": [
        20,
        21,
        22
      ]
    },
    {
      "id": 30,
      "x": 0.333,
      "y": 0.136,
      "layer": 5,
      "closed_by": [
        20,
        21,
        23
      ]
    },
    {
      "id": 31,
      "x": 0.333,
      "y": 1.026,
      "layer": 5,
      "closed_by": [
        21,
        23
      ]
    },
    {
      "id": 32,
      "x": -0.333,
      "y": 1.024,
      "layer": 5,
      "closed_by": [
        21,
        22
      ]
    },
    {
      "id": 33,
      "x": 0.493,
      "y": 0.537,
      "layer": 4,
      "closed_by": [
        30,
        31
      ]
    },
    {
      "id": 34,
      "x": -0.493,
      "y": 0.537,
      "layer": 4,
      "closed_by": [
        29,
        32
      ]
    },
    {
      "id": 35,
      "x": 0.865,
      "y": 0.541,
      "layer": 3,
      "closed_by": [
        9,
        23,
        33
      ]
    },
    {
      "id": 36,
      "x": -0.864,
      "y": 0.54,
      "layer": 3,
      "closed_by": [
        8,
        22,
        34
      ]
    },
    {
      "id": 37,
      "x": -1.105,
      "y": 0.456,
      "layer": 2,
      "closed_by": [
        27,
        36
      ]
    },
    {
      "id": 38,
      "x": 1.103,
      "y": 0.46,
      "layer": 2,
      "closed_by": [
        28,
        35
      ]
    },
    {
      "id": 39,
      "x": 0.865,
      "y": 0.379,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 40,
      "x": -0.864,
      "y": 0.379,
      "layer": 1,
      "closed_by": [
        37
      ]
    },
    {
      "id": 41,
      "x": 0.006,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        50,
        51
      ]
    },
    {
      "id": 42,
      "x": -0.252,
      "y": -0.453,
      "layer": 4,
      "closed_by": [
        29
      ]
    },
    {
      "id": 43,
      "x": 0.263,
      "y": -0.453,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 44,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        42,
        43
      ]
    },
    {
      "id": 45,
      "x": -1.108,
      "y": -0.574,
      "layer": 5,
      "closed_by": [
        24
      ]
    },
    {
      "id": 46,
      "x": -0.865,
      "y": -0.5,
      "layer": 4,
      "closed_by": [
        45
      ]
    },
    {
      "id": 47,
      "x": 0.869,
      "y": -0.497,
      "layer": 4,
      "closed_by": [
        26
      ]
    },
    {
      "id": 48,
      "x": -0.545,
      "y": -0.574,
      "layer": 3,
      "closed_by": [
        42,
        46
      ]
    },
    {
      "id": 49,
      "x": 0.546,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        43,
        47
      ]
    },
    {
      "id": 50,
      "x": -0.305,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        44,
        48
      ]
    },
    {
      "id": 51,
      "x": 0.303,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        44,
        49
      ]
    }
  ],
  "cards_in_layout_amount": 44,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 0.4,
    "star2": 21.9,
    "star3": 77.4
  }
}