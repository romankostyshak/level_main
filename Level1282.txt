{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.002,
      "y": 0.303,
      "layer": 1,
      "closed_by": [
        49,
        50
      ]
    },
    {
      "id": 49,
      "x": -0.303,
      "y": -0.093,
      "layer": 2,
      "closed_by": [
        47,
        45,
        26
      ]
    },
    {
      "id": 47,
      "x": -0.303,
      "y": 0.619,
      "layer": 3,
      "closed_by": [
        44,
        28
      ]
    },
    {
      "id": 46,
      "x": 0.703,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        38,
        37
      ]
    },
    {
      "id": 45,
      "x": -0.703,
      "y": -0.256,
      "layer": 3,
      "closed_by": [
        44,
        43
      ]
    },
    {
      "id": 44,
      "x": -0.703,
      "y": 0.462,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 43,
      "x": -1.103,
      "y": -0.412,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 42,
      "x": 1.503,
      "y": -0.5,
      "layer": 5,
      "closed_by": [
        40
      ]
    },
    {
      "id": 41,
      "x": -1.506,
      "y": -0.493,
      "layer": 5,
      "closed_by": [
        39
      ]
    },
    {
      "id": 40,
      "x": 1.906,
      "y": -0.578,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -1.901,
      "y": -0.574,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 0.703,
      "y": 0.462,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 37,
      "x": 1.105,
      "y": -0.416,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 36,
      "x": 1.105,
      "y": 0.537,
      "layer": 5,
      "closed_by": [
        34
      ]
    },
    {
      "id": 35,
      "x": -1.103,
      "y": 0.537,
      "layer": 5,
      "closed_by": [
        33
      ]
    },
    {
      "id": 34,
      "x": 1.508,
      "y": 0.624,
      "layer": 6,
      "closed_by": [
        32
      ]
    },
    {
      "id": 33,
      "x": -1.503,
      "y": 0.615,
      "layer": 6,
      "closed_by": [
        31
      ]
    },
    {
      "id": 32,
      "x": 1.906,
      "y": 0.703,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -1.901,
      "y": 0.703,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 48,
      "x": 0.307,
      "y": 0.62,
      "layer": 3,
      "closed_by": [
        38,
        28
      ]
    },
    {
      "id": 28,
      "x": 0.001,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 27,
      "x": 0.002,
      "y": 0.777,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 50,
      "x": 0.305,
      "y": -0.093,
      "layer": 2,
      "closed_by": [
        46,
        48,
        26
      ]
    },
    {
      "id": 26,
      "x": 0.001,
      "y": -0.254,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 25,
      "y": -0.414,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 24,
      "x": 0.001,
      "y": -0.569,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 26,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 53.6,
    "star2": 14.1,
    "star3": 31.3
  }
}