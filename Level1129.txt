{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.333,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        44,
        41
      ]
    },
    {
      "id": 50,
      "x": -0.331,
      "y": 0.142,
      "layer": 1,
      "closed_by": [
        44,
        41,
        40
      ]
    },
    {
      "id": 49,
      "x": 0.333,
      "y": 1.024,
      "layer": 1,
      "closed_by": [
        47,
        41
      ]
    },
    {
      "id": 48,
      "x": 0.331,
      "y": 0.142,
      "layer": 1,
      "closed_by": [
        47,
        41,
        40
      ]
    },
    {
      "id": 47,
      "x": 0.787,
      "y": 0.3,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 46,
      "x": 0.787,
      "y": -0.414,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 45,
      "x": -0.786,
      "y": -0.414,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 44,
      "x": -0.785,
      "y": 0.3,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 43,
      "x": 1.184,
      "y": -0.418,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -1.187,
      "y": -0.416,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 41,
      "x": -0.002,
      "y": 0.861,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 40,
      "y": -0.017,
      "layer": 2,
      "closed_by": [
        39,
        14
      ]
    },
    {
      "id": 39,
      "y": 0.449,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 1.182,
      "y": 0.298,
      "layer": 4,
      "closed_by": [
        43,
        25
      ]
    },
    {
      "id": 37,
      "x": -1.608,
      "y": 1.023,
      "layer": 3,
      "closed_by": [
        29,
        26
      ]
    },
    {
      "id": 32,
      "x": 1.705,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 31,
      "x": 1.707,
      "y": 0.298,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 30,
      "x": 1.613,
      "y": 1.019,
      "layer": 3,
      "closed_by": [
        38,
        31
      ]
    },
    {
      "id": 29,
      "x": -1.707,
      "y": 0.298,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 28,
      "x": 2.413,
      "y": 1.021,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 27,
      "x": -2.414,
      "y": 0.298,
      "layer": 5,
      "closed_by": [
        18
      ]
    },
    {
      "id": 26,
      "x": -1.187,
      "y": 0.298,
      "layer": 4,
      "closed_by": [
        42,
        17
      ]
    },
    {
      "id": 25,
      "x": 1.098,
      "y": 1.019,
      "layer": 5,
      "closed_by": [
        15
      ]
    },
    {
      "id": 24,
      "x": -2.407,
      "y": 1.019,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 23,
      "x": -1.894,
      "y": 1.024,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 22,
      "x": 1.899,
      "y": 1.019,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 20,
      "x": 2.414,
      "y": 0.298,
      "layer": 5,
      "closed_by": [
        19
      ]
    },
    {
      "id": 19,
      "x": 2.22,
      "y": -0.418,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 18,
      "x": -2.223,
      "y": -0.418,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 17,
      "x": -1.1,
      "y": 1.023,
      "layer": 5,
      "closed_by": [
        16
      ]
    },
    {
      "id": 16,
      "x": -0.847,
      "y": 1.021,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 15,
      "x": 0.851,
      "y": 1.021,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 14,
      "y": -0.416,
      "layer": 3,
      "closed_by": [
        13,
        12
      ]
    },
    {
      "id": 13,
      "x": -0.266,
      "y": -0.414,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 12,
      "x": 0.268,
      "y": -0.416,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 21,
      "x": 1.901,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        32,
        30
      ]
    },
    {
      "id": 36,
      "x": -1.891,
      "y": 0.298,
      "layer": 2,
      "closed_by": [
        37,
        33
      ]
    },
    {
      "id": 35,
      "x": 2.413,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 34,
      "x": -2.411,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 33,
      "x": -1.71,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        29
      ]
    }
  ],
  "cards_in_layout_amount": 40,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 1.5,
    "star2": 40.5,
    "star3": 57.8
  }
}