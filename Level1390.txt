{
  "layout_cards": [
    {
      "id": 51,
      "y": -0.5,
      "layer": 1,
      "closed_by": [
        50,
        49
      ]
    },
    {
      "id": 50,
      "x": -0.305,
      "y": -0.497,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 49,
      "x": 0.307,
      "y": -0.497,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 48,
      "x": 0.384,
      "y": -0.416,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 47,
      "x": -0.384,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 46,
      "x": 0.467,
      "y": -0.335,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 45,
      "x": -0.467,
      "y": -0.34,
      "layer": 4,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": -0.541,
      "y": -0.416,
      "layer": 5,
      "closed_by": [
        41
      ]
    },
    {
      "id": 43,
      "x": 0.549,
      "y": -0.418,
      "layer": 5,
      "closed_by": [
        42
      ]
    },
    {
      "id": 42,
      "x": 0.625,
      "y": -0.499,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 41,
      "x": -0.625,
      "y": -0.499,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 0.384,
      "y": 0.62,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 39,
      "x": -0.384,
      "y": 0.623,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 38,
      "y": 0.671,
      "layer": 3,
      "closed_by": [
        37,
        36
      ]
    },
    {
      "id": 37,
      "x": 0.337,
      "y": 0.671,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 36,
      "x": -0.342,
      "y": 0.671,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 35,
      "x": 0.298,
      "y": 0.72,
      "layer": 5,
      "closed_by": [
        33
      ]
    },
    {
      "id": 34,
      "x": -0.3,
      "y": 0.722,
      "layer": 5,
      "closed_by": [
        33
      ]
    },
    {
      "id": 33,
      "x": -0.001,
      "y": 0.777,
      "layer": 6,
      "closed_by": [
        31
      ]
    },
    {
      "id": 32,
      "x": 0.002,
      "y": 0.541,
      "layer": 1,
      "closed_by": [
        40,
        39
      ]
    },
    {
      "id": 31,
      "x": -0.002,
      "y": 0.861,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 1.376,
      "y": 0.943,
      "layer": 1,
      "closed_by": [
        28
      ]
    },
    {
      "id": 29,
      "x": -1.371,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        27
      ]
    },
    {
      "id": 28,
      "x": 1.463,
      "y": 0.796,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        26
      ]
    },
    {
      "id": 27,
      "x": -1.452,
      "y": 0.8,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 26,
      "x": 1.516,
      "y": 0.595,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 25,
      "x": -1.521,
      "y": 0.596,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 24,
      "x": 1.559,
      "y": 0.361,
      "angle": 315.0,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 23,
      "x": -1.562,
      "y": 0.361,
      "angle": 45.0,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 21,
      "x": -1.577,
      "y": 0.119,
      "angle": 60.0,
      "layer": 5,
      "closed_by": [
        19
      ]
    },
    {
      "id": 22,
      "x": 1.577,
      "y": 0.1,
      "angle": 300.0,
      "layer": 5,
      "closed_by": [
        20
      ]
    },
    {
      "id": 20,
      "x": 1.559,
      "y": -0.119,
      "angle": 285.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 19,
      "x": -1.562,
      "y": -0.1,
      "angle": 75.0,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 33,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 41.5,
    "star2": 54.5,
    "star3": 3.2
  }
}