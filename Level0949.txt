{
  "layout_cards": [
    {
      "id": 51,
      "x": 1.373,
      "y": -0.246,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 50,
      "x": -1.373,
      "y": -0.25,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 49,
      "x": -1.294,
      "y": -0.09,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 48,
      "x": 1.292,
      "y": -0.093,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 47,
      "x": -1.213,
      "y": 0.067,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 46,
      "x": 1.21,
      "y": 0.063,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 45,
      "x": 1.376,
      "y": 0.62,
      "layer": 1,
      "closed_by": [
        48,
        42
      ]
    },
    {
      "id": 44,
      "x": -1.371,
      "y": 0.624,
      "layer": 1,
      "closed_by": [
        49,
        43
      ]
    },
    {
      "id": 43,
      "x": -1.103,
      "y": 0.787,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 42,
      "x": 1.105,
      "y": 0.782,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 41,
      "x": -0.972,
      "y": 0.786,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 0.972,
      "y": 0.787,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 39,
      "x": 0.263,
      "y": 0.625,
      "layer": 3,
      "closed_by": [
        35,
        33
      ]
    },
    {
      "id": 38,
      "x": 1.026,
      "y": -0.414,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -0.259,
      "y": 0.625,
      "layer": 3,
      "closed_by": [
        34,
        33
      ]
    },
    {
      "id": 35,
      "x": 0.333,
      "y": 0.944,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 34,
      "x": -0.331,
      "y": 0.939,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 33,
      "y": 0.064,
      "layer": 4,
      "closed_by": [
        32,
        31
      ]
    },
    {
      "id": 32,
      "x": -0.398,
      "y": 0.064,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 0.418,
      "y": 0.064,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 0.819,
      "y": 0.064,
      "layer": 4,
      "closed_by": [
        40,
        38,
        31
      ]
    },
    {
      "id": 29,
      "x": -0.799,
      "y": 0.064,
      "layer": 4,
      "closed_by": [
        41,
        32,
        37
      ]
    },
    {
      "id": 37,
      "x": -1.026,
      "y": -0.412,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 23,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 41.2,
    "star2": 2.7,
    "star3": 55.6
  }
}