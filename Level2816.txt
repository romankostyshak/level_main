{
  "layout_cards": [
    {
      "id": 8,
      "x": -2.226,
      "y": 0.307,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 9,
      "x": -2.19,
      "y": -0.291,
      "layer": 3,
      "closed_by": [
        8
      ]
    },
    {
      "id": 10,
      "x": 2.19,
      "y": -0.287,
      "layer": 3,
      "closed_by": [
        15
      ]
    },
    {
      "id": 11,
      "x": -2.147,
      "y": 0.782,
      "layer": 3,
      "closed_by": [
        8
      ]
    },
    {
      "id": 12,
      "x": -2.147,
      "y": -0.34,
      "layer": 2,
      "closed_by": [
        9
      ]
    },
    {
      "id": 13,
      "x": 2.147,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        10
      ]
    },
    {
      "id": 14,
      "x": 2.144,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        15
      ]
    },
    {
      "id": 15,
      "x": 2.223,
      "y": 0.298,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 17,
      "x": -0.572,
      "y": 0.823,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 18,
      "x": -0.699,
      "y": 0.995,
      "angle": 25.0,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 19,
      "x": -0.446,
      "y": 0.99,
      "angle": 335.0,
      "layer": 3,
      "closed_by": [
        17
      ]
    },
    {
      "id": 20,
      "x": -0.572,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        18
      ]
    },
    {
      "id": 21,
      "x": -0.444,
      "y": -0.555,
      "angle": 25.0,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 22,
      "x": -0.569,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        21
      ]
    },
    {
      "id": 23,
      "x": -0.712,
      "y": -0.546,
      "angle": 335.0,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 24,
      "x": -0.569,
      "y": -0.386,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 25,
      "x": 0.559,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        28
      ]
    },
    {
      "id": 26,
      "x": 0.563,
      "y": -0.259,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 27,
      "x": 0.428,
      "y": 0.995,
      "angle": 25.0,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 28,
      "x": 0.685,
      "y": 0.994,
      "angle": 335.0,
      "layer": 2,
      "closed_by": [
        27
      ]
    },
    {
      "id": 29,
      "x": 0.441,
      "y": -0.546,
      "angle": 335.0,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 30,
      "x": 1.508,
      "y": 0.224,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 31,
      "x": 0.625,
      "y": 0.216,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 0.712,
      "y": -0.541,
      "angle": 25.0,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 33,
      "x": 0.563,
      "y": -0.388,
      "layer": 4,
      "closed_by": [
        38
      ]
    },
    {
      "id": 34,
      "x": 0.56,
      "y": 0.827,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 35,
      "x": -0.259,
      "y": -0.18,
      "layer": 5,
      "closed_by": [
        39,
        40
      ]
    },
    {
      "id": 36,
      "x": -0.259,
      "y": 0.629,
      "layer": 5,
      "closed_by": [
        39,
        40
      ]
    },
    {
      "id": 37,
      "x": 0.266,
      "y": 0.624,
      "layer": 5,
      "closed_by": [
        31,
        39
      ]
    },
    {
      "id": 38,
      "x": 0.263,
      "y": -0.187,
      "layer": 5,
      "closed_by": [
        31,
        39
      ]
    },
    {
      "id": 39,
      "x": 0.003,
      "y": 0.216,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 40,
      "x": -0.625,
      "y": 0.222,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 1.213,
      "y": 0.216,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -1.213,
      "y": 0.216,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 43,
      "x": 1.371,
      "y": 0.574,
      "angle": 325.0,
      "layer": 5,
      "closed_by": [
        41
      ]
    },
    {
      "id": 44,
      "x": 1.373,
      "y": -0.128,
      "angle": 35.0,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 45,
      "x": -1.383,
      "y": 0.577,
      "angle": 35.0,
      "layer": 5,
      "closed_by": [
        42
      ]
    },
    {
      "id": 46,
      "x": -1.378,
      "y": -0.133,
      "angle": 325.0,
      "layer": 4,
      "closed_by": [
        45
      ]
    },
    {
      "id": 47,
      "x": -1.506,
      "y": 0.216,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": 1.906,
      "y": 0.541,
      "layer": 2,
      "closed_by": [
        14,
        30,
        43
      ]
    },
    {
      "id": 49,
      "x": -1.906,
      "y": 0.537,
      "layer": 2,
      "closed_by": [
        11,
        45,
        47
      ]
    },
    {
      "id": 50,
      "x": 1.909,
      "y": -0.097,
      "layer": 1,
      "closed_by": [
        13,
        48
      ]
    },
    {
      "id": 51,
      "x": -1.904,
      "y": -0.096,
      "layer": 1,
      "closed_by": [
        12,
        49
      ]
    }
  ],
  "cards_in_layout_amount": 43,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 2.5,
    "star2": 30.2,
    "star3": 66.8
  }
}