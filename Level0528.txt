{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.731,
      "y": -0.314,
      "layer": 1,
      "closed_by": [
        32,
        41
      ]
    },
    {
      "id": 50,
      "x": -0.731,
      "y": 0.8,
      "layer": 1,
      "closed_by": [
        33,
        41
      ]
    },
    {
      "id": 48,
      "x": 0.731,
      "y": 0.8,
      "layer": 1,
      "closed_by": [
        28,
        44
      ]
    },
    {
      "id": 47,
      "x": 0.735,
      "y": -0.314,
      "layer": 1,
      "closed_by": [
        34,
        44
      ]
    },
    {
      "id": 46,
      "x": 0.847,
      "y": -0.165,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 45,
      "x": 0.846,
      "y": 0.657,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 43,
      "x": 0.694,
      "y": 0.244,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 39,
      "y": 1.019,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 38,
      "y": 0.92,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 37,
      "y": -0.458,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 34,
      "x": 1.108,
      "y": -0.574,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 33,
      "x": -1.1,
      "y": 1.021,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 32,
      "x": -1.1,
      "y": -0.574,
      "layer": 2,
      "closed_by": [
        49
      ]
    },
    {
      "id": 31,
      "y": -0.578,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 1.108,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 44,
      "x": 0.652,
      "y": 0.245,
      "layer": 2,
      "closed_by": [
        46,
        45
      ]
    },
    {
      "id": 41,
      "x": -0.643,
      "y": 0.238,
      "layer": 2,
      "closed_by": [
        49,
        42
      ]
    },
    {
      "id": 49,
      "x": -0.845,
      "y": -0.158,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 42,
      "x": -0.845,
      "y": 0.657,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 40,
      "x": -0.683,
      "y": 0.238,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 29,
      "y": 0.657,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 36,
      "y": 0.799,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 35,
      "y": -0.319,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 30,
      "y": -0.18,
      "layer": 1,
      "closed_by": [
        35
      ]
    }
  ],
  "cards_in_layout_amount": 24,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 61.0,
    "star2": 8.7,
    "star3": 29.6
  }
}