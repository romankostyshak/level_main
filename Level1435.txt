{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.414,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 50,
      "x": 0.414,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 49,
      "x": -0.465,
      "y": 0.898,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": 0.465,
      "y": 0.893,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 46,
      "x": -0.508,
      "y": 0.85,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 45,
      "x": -0.509,
      "y": 0.694,
      "layer": 4,
      "closed_by": [
        37
      ]
    },
    {
      "id": 42,
      "x": 0.412,
      "y": -0.435,
      "layer": 5,
      "closed_by": [
        35
      ]
    },
    {
      "id": 41,
      "x": -0.412,
      "y": -0.435,
      "layer": 5,
      "closed_by": [
        35
      ]
    },
    {
      "id": 40,
      "x": 0.465,
      "y": -0.481,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 39,
      "x": -0.465,
      "y": -0.479,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 38,
      "x": -0.509,
      "y": -0.526,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 37,
      "x": -0.414,
      "y": 0.379,
      "layer": 5,
      "closed_by": [
        36
      ]
    },
    {
      "id": 35,
      "x": -0.001,
      "y": -0.435,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 0.513,
      "y": -0.527,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 33,
      "x": 0.559,
      "y": -0.574,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 32,
      "x": -0.55,
      "y": -0.574,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 43,
      "x": 0.418,
      "y": 0.381,
      "layer": 5,
      "closed_by": [
        36
      ]
    },
    {
      "id": 44,
      "x": 0.522,
      "y": 0.698,
      "layer": 4,
      "closed_by": [
        43
      ]
    },
    {
      "id": 47,
      "x": 0.518,
      "y": 0.837,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 26,
      "x": -1.615,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 30,
      "x": -1.615,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        26,
        28
      ]
    },
    {
      "id": 28,
      "x": -1.615,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 27,
      "x": 1.616,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 31,
      "x": 1.616,
      "y": 0.298,
      "layer": 1,
      "closed_by": [
        27,
        29
      ]
    },
    {
      "id": 29,
      "x": 1.616,
      "y": -0.259,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 25,
      "x": 1.746,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        23
      ]
    },
    {
      "id": 24,
      "x": -1.745,
      "y": 0.298,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 21,
      "x": -1.664,
      "y": 0.136,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 23,
      "x": 1.669,
      "y": 0.136,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 22,
      "x": 1.669,
      "y": 0.46,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 20,
      "x": -1.664,
      "y": 0.465,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 36,
      "y": 0.379,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 32,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 29.0,
    "star2": 60.9,
    "star3": 9.5
  }
}