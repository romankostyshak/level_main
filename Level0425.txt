{
  "layout_cards": [
    {
      "id": 51,
      "x": -1.929,
      "y": 0.638,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 50,
      "x": 1.934,
      "y": 0.638,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 49,
      "x": -2.062,
      "y": 0.082,
      "angle": 20.0,
      "layer": 2,
      "closed_by": [
        51
      ]
    },
    {
      "id": 48,
      "x": -2.062,
      "y": 0.587,
      "angle": 340.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 47,
      "x": 2.062,
      "y": 0.596,
      "angle": 20.0,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 46,
      "x": -1.932,
      "y": 0.037,
      "layer": 1,
      "closed_by": [
        49
      ]
    },
    {
      "id": 45,
      "x": 2.068,
      "y": 0.082,
      "angle": 340.0,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 44,
      "x": 0.37,
      "y": -0.435,
      "angle": 354.997,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 43,
      "x": -0.384,
      "y": -0.432,
      "angle": 5.0,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 42,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        37,
        35
      ],
      "card_type": 3
    },
    {
      "id": 41,
      "x": -0.003,
      "y": -0.416,
      "layer": 3,
      "closed_by": [],
      "card_type": 2
    },
    {
      "id": 40,
      "x": 0.726,
      "y": -0.483,
      "angle": 349.997,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 39,
      "x": -0.648,
      "y": 0.546,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 38,
      "x": 0.662,
      "y": 0.545,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 37,
      "x": -0.388,
      "y": 0.953,
      "layer": 2,
      "closed_by": [
        39,
        34
      ]
    },
    {
      "id": 36,
      "x": 0.662,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 35,
      "x": 0.4,
      "y": 0.953,
      "layer": 2,
      "closed_by": [
        38,
        34
      ]
    },
    {
      "id": 34,
      "y": 0.546,
      "layer": 3,
      "closed_by": [
        30,
        29
      ]
    },
    {
      "id": 33,
      "x": 1.934,
      "y": 0.037,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 32,
      "x": -0.74,
      "y": -0.481,
      "angle": 9.998,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 31,
      "x": -0.648,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        37
      ]
    },
    {
      "id": 30,
      "x": 0.4,
      "y": 0.465,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -0.388,
      "y": 0.465,
      "layer": 4,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 23,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 21.9,
    "star2": 0.3,
    "star3": 77.3
  }
}