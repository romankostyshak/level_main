{
  "layout_cards": [
    {
      "id": 50,
      "x": -0.259,
      "y": -0.414,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 49,
      "x": 0.27,
      "y": -0.418,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": -0.782,
      "y": -0.416,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 47,
      "x": -0.259,
      "y": -0.178,
      "layer": 2,
      "closed_by": [
        42,
        41
      ]
    },
    {
      "id": 46,
      "x": 0.275,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        41,
        40
      ]
    },
    {
      "id": 45,
      "x": 0.796,
      "y": -0.172,
      "layer": 2,
      "closed_by": [
        40,
        30
      ]
    },
    {
      "id": 43,
      "x": -1.042,
      "y": 0.059,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 42,
      "x": -0.518,
      "y": 0.059,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 41,
      "y": 0.057,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 40,
      "x": 0.518,
      "y": 0.057,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 38,
      "x": -0.518,
      "y": 0.46,
      "layer": 4,
      "closed_by": [
        33,
        32
      ]
    },
    {
      "id": 37,
      "x": -0.001,
      "y": 0.46,
      "layer": 4,
      "closed_by": [
        32,
        31
      ]
    },
    {
      "id": 36,
      "x": 0.521,
      "y": 0.46,
      "layer": 4,
      "closed_by": [
        34,
        31
      ]
    },
    {
      "id": 35,
      "x": 1.044,
      "y": 0.46,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 34,
      "x": 0.8,
      "y": 0.86,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -0.795,
      "y": 0.856,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -0.263,
      "y": 0.856,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 31,
      "x": 0.273,
      "y": 0.859,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 30,
      "x": 1.039,
      "y": 0.059,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 29,
      "x": 1.36,
      "y": 0.458,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 28,
      "x": -1.358,
      "y": 0.46,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 27,
      "x": 1.36,
      "y": 0.061,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 26,
      "x": -1.358,
      "y": 0.061,
      "layer": 1,
      "closed_by": [
        28
      ]
    },
    {
      "id": 44,
      "x": -0.786,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        43,
        42
      ]
    },
    {
      "id": 51,
      "x": 0.795,
      "y": -0.416,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 39,
      "x": -1.042,
      "y": 0.46,
      "layer": 4,
      "closed_by": [
        33
      ]
    }
  ],
  "cards_in_layout_amount": 26,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 51.6,
    "star2": 14.4,
    "star3": 33.2
  }
}