{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.49,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        48
      ]
    },
    {
      "id": 50,
      "x": -0.492,
      "y": -0.256,
      "layer": 1,
      "closed_by": [
        49,
        29
      ]
    },
    {
      "id": 49,
      "x": -0.546,
      "y": -0.335,
      "layer": 2,
      "closed_by": [
        11
      ]
    },
    {
      "id": 48,
      "x": -0.559,
      "y": 0.861,
      "layer": 2,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "x": -0.625,
      "y": 0.781,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 46,
      "x": -0.703,
      "y": -0.499,
      "layer": 4,
      "closed_by": [
        35
      ]
    },
    {
      "id": 45,
      "x": -0.708,
      "y": 0.703,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 44,
      "x": 0.493,
      "y": 0.938,
      "layer": 1,
      "closed_by": [
        42
      ]
    },
    {
      "id": 43,
      "x": 0.492,
      "y": -0.256,
      "layer": 1,
      "closed_by": [
        41,
        28
      ]
    },
    {
      "id": 42,
      "x": 0.549,
      "y": 0.861,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 41,
      "x": 0.546,
      "y": -0.333,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 40,
      "x": 0.625,
      "y": 0.782,
      "layer": 3,
      "closed_by": [
        38
      ]
    },
    {
      "id": 39,
      "x": 0.625,
      "y": -0.414,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 38,
      "x": 0.707,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        34
      ]
    },
    {
      "id": 37,
      "x": 0.707,
      "y": -0.493,
      "layer": 4,
      "closed_by": [
        33
      ]
    },
    {
      "id": 36,
      "x": -0.782,
      "y": 0.62,
      "layer": 5,
      "closed_by": [
        31,
        29
      ]
    },
    {
      "id": 35,
      "x": -0.786,
      "y": -0.574,
      "layer": 5,
      "closed_by": [
        30,
        27
      ]
    },
    {
      "id": 34,
      "x": 0.786,
      "y": 0.62,
      "layer": 5,
      "closed_by": [
        32,
        28
      ]
    },
    {
      "id": 33,
      "x": 0.782,
      "y": -0.573,
      "layer": 5,
      "closed_by": [
        26,
        10
      ]
    },
    {
      "id": 32,
      "x": 1.026,
      "y": 0.476,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 31,
      "x": -1.026,
      "y": 0.479,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -1.024,
      "y": -0.418,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -0.493,
      "y": 0.474,
      "layer": 6,
      "closed_by": [
        25
      ]
    },
    {
      "id": 28,
      "x": 0.493,
      "y": 0.474,
      "layer": 6,
      "closed_by": [
        24
      ]
    },
    {
      "id": 27,
      "x": -0.49,
      "y": -0.414,
      "layer": 6,
      "closed_by": [
        23
      ]
    },
    {
      "id": 26,
      "x": 0.495,
      "y": -0.411,
      "layer": 6,
      "closed_by": [
        22
      ]
    },
    {
      "id": 25,
      "x": -0.333,
      "y": 0.939,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 24,
      "x": 0.333,
      "y": 0.939,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 23,
      "x": -0.333,
      "y": -0.573,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 22,
      "x": 0.335,
      "y": -0.573,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 21,
      "x": 1.343,
      "y": 0.62,
      "layer": 5,
      "closed_by": [
        32
      ]
    },
    {
      "id": 20,
      "x": -1.347,
      "y": 0.62,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 19,
      "x": 1.343,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 18,
      "x": -1.347,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 17,
      "x": 1.343,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        19
      ]
    },
    {
      "id": 16,
      "x": -1.347,
      "y": 0.781,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 15,
      "x": 1.343,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        17
      ]
    },
    {
      "id": 14,
      "x": -1.347,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        13
      ]
    },
    {
      "id": 13,
      "x": -1.347,
      "y": 0.861,
      "layer": 2,
      "closed_by": [
        16
      ]
    },
    {
      "id": 11,
      "x": -0.625,
      "y": -0.414,
      "layer": 3,
      "closed_by": [
        46
      ]
    },
    {
      "id": 10,
      "x": 1.026,
      "y": -0.414,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 9,
      "x": 1.347,
      "y": -0.5,
      "layer": 5,
      "closed_by": [
        10
      ]
    },
    {
      "id": 8,
      "x": 1.347,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        9
      ]
    },
    {
      "id": 6,
      "x": -1.347,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        7
      ]
    },
    {
      "id": 7,
      "x": -1.347,
      "y": -0.5,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 5,
      "x": -1.347,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        6
      ]
    },
    {
      "id": 4,
      "x": 1.347,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        8
      ]
    },
    {
      "id": 12,
      "x": 1.343,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        15
      ]
    }
  ],
  "cards_in_layout_amount": 48,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 0.1,
    "star2": 4.2,
    "star3": 95.0
  }
}