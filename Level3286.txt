{
  "layout_cards": [
    {
      "id": 26,
      "x": -2.075,
      "y": 0.5,
      "angle": 325.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 27,
      "x": -1.531,
      "y": 0.194,
      "angle": 335.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 2.078,
      "y": 0.563,
      "angle": 40.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 29,
      "x": -2.118,
      "y": 0.976,
      "angle": 320.0,
      "layer": 5,
      "closed_by": [
        26
      ]
    },
    {
      "id": 30,
      "x": 2.115,
      "y": 0.976,
      "angle": 40.0,
      "layer": 5,
      "closed_by": [
        28
      ]
    },
    {
      "id": 31,
      "x": -1.646,
      "y": 0.643,
      "angle": 330.0,
      "layer": 5,
      "closed_by": [
        26,
        27
      ]
    },
    {
      "id": 32,
      "x": 1.641,
      "y": 0.648,
      "angle": 30.0,
      "layer": 5,
      "closed_by": [
        28,
        33
      ]
    },
    {
      "id": 33,
      "x": 1.534,
      "y": 0.222,
      "angle": 25.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 0.564,
      "y": -0.224,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 35,
      "x": -0.411,
      "y": -0.485,
      "angle": 30.0,
      "layer": 1,
      "closed_by": [
        37
      ]
    },
    {
      "id": 36,
      "x": 0.414,
      "y": -0.492,
      "angle": 330.0,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 37,
      "x": -0.488,
      "y": -0.356,
      "angle": 30.0,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 38,
      "x": 0.486,
      "y": -0.351,
      "angle": 330.0,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 39,
      "x": -0.558,
      "y": -0.231,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 40,
      "x": 0.541,
      "y": -0.074,
      "angle": 344.997,
      "layer": 4,
      "closed_by": [
        44
      ]
    },
    {
      "id": 41,
      "x": -0.541,
      "y": -0.068,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        45
      ]
    },
    {
      "id": 42,
      "x": -1.123,
      "y": 0.398,
      "angle": 340.0,
      "layer": 5,
      "closed_by": [
        27,
        48
      ]
    },
    {
      "id": 43,
      "x": 1.126,
      "y": 0.397,
      "angle": 20.0,
      "layer": 5,
      "closed_by": [
        33,
        47
      ]
    },
    {
      "id": 44,
      "x": 0.577,
      "y": 0.231,
      "angle": 9.998,
      "layer": 5,
      "closed_by": [
        47,
        49
      ]
    },
    {
      "id": 45,
      "x": -0.574,
      "y": 0.231,
      "angle": 349.997,
      "layer": 5,
      "closed_by": [
        48,
        50
      ]
    },
    {
      "id": 46,
      "x": 0.002,
      "y": 0.224,
      "layer": 5,
      "closed_by": [
        49,
        50
      ]
    },
    {
      "id": 47,
      "x": 0.953,
      "y": -0.008,
      "angle": 15.0,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 48,
      "x": -0.939,
      "y": -0.008,
      "angle": 344.997,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 49,
      "x": 0.3,
      "y": 0.652,
      "angle": 5.0,
      "layer": 6,
      "closed_by": [
        51
      ]
    },
    {
      "id": 50,
      "x": -0.291,
      "y": 0.648,
      "angle": 354.997,
      "layer": 6,
      "closed_by": [
        51
      ]
    },
    {
      "id": 51,
      "y": 0.924,
      "layer": 7,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 26,
  "cards_in_deck_amount": 24,
  "stars_statistic": {
    "star1": 48.5,
    "star2": 2.4,
    "star3": 48.3
  }
}