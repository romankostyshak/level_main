{
  "layout_cards": [
    {
      "id": 19,
      "y": -0.578,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 20,
      "y": 0.799,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 21,
      "x": 2.44,
      "y": 0.216,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 22,
      "x": 2.279,
      "y": -0.358,
      "angle": 330.0,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 23,
      "x": -2.279,
      "y": -0.358,
      "angle": 30.0,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 24,
      "x": -2.078,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 25,
      "x": 2.078,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        22
      ]
    },
    {
      "id": 26,
      "x": -2.078,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        24
      ]
    },
    {
      "id": 27,
      "x": 2.078,
      "y": 0.216,
      "layer": 1,
      "closed_by": [
        25
      ]
    },
    {
      "id": 28,
      "x": -0.439,
      "y": 0.74,
      "angle": 15.0,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 29,
      "x": -1.258,
      "y": 0.6,
      "angle": 300.0,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 30,
      "x": 1.258,
      "y": 0.6,
      "angle": 60.0,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 31,
      "x": -0.939,
      "y": 0.777,
      "angle": 300.0,
      "layer": 1,
      "closed_by": [
        29,
        50
      ]
    },
    {
      "id": 32,
      "x": 0.939,
      "y": 0.777,
      "angle": 60.0,
      "layer": 1,
      "closed_by": [
        30,
        45
      ]
    },
    {
      "id": 33,
      "x": -1.539,
      "y": 0.216,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 1.539,
      "y": 0.216,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 35,
      "x": -1.539,
      "y": -0.238,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 36,
      "x": 0.439,
      "y": 0.74,
      "angle": 345.0,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 37,
      "x": -2.437,
      "y": 0.216,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 1.539,
      "y": -0.238,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 39,
      "x": 1.139,
      "y": -0.238,
      "angle": 60.0,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 40,
      "x": -1.139,
      "y": -0.238,
      "angle": 300.0,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 41,
      "x": -0.379,
      "y": -0.458,
      "layer": 4,
      "closed_by": [
        19
      ]
    },
    {
      "id": 42,
      "x": 0.379,
      "y": -0.458,
      "layer": 4,
      "closed_by": [
        19
      ]
    },
    {
      "id": 43,
      "x": 0.46,
      "y": 0.158,
      "angle": 345.0,
      "layer": 3,
      "closed_by": [
        36,
        42
      ]
    },
    {
      "id": 44,
      "x": -0.458,
      "y": 0.158,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        28,
        41
      ]
    },
    {
      "id": 45,
      "x": 0.537,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 46,
      "x": -0.319,
      "y": -0.216,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 47,
      "x": 0.319,
      "y": -0.216,
      "layer": 2,
      "closed_by": [
        43
      ]
    },
    {
      "id": 48,
      "x": -0.518,
      "y": 0.119,
      "angle": 300.0,
      "layer": 1,
      "closed_by": [
        40,
        46,
        50
      ]
    },
    {
      "id": 49,
      "x": 0.518,
      "y": 0.119,
      "angle": 60.0,
      "layer": 1,
      "closed_by": [
        39,
        45,
        47
      ]
    },
    {
      "id": 50,
      "x": -0.537,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        44
      ]
    }
  ],
  "cards_in_layout_amount": 32,
  "cards_in_deck_amount": 28,
  "layers_in_level": 5,
  "open_cards": 6,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 21.0,
    "star2": 72.8,
    "star3": 5.8
  }
}