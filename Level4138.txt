{
  "layout_cards": [
    {
      "id": 4,
      "x": 2.358,
      "y": 0.859,
      "angle": 60.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 5,
      "x": -2.118,
      "y": -0.259,
      "angle": 60.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 6,
      "x": -1.498,
      "y": -0.259,
      "angle": 300.0,
      "layer": 4,
      "closed_by": [
        5
      ]
    },
    {
      "id": 7,
      "x": -2.118,
      "y": 0.859,
      "angle": 300.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 8,
      "x": 2.358,
      "y": -0.279,
      "angle": 300.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 9,
      "x": -1.498,
      "y": 0.859,
      "angle": 60.0,
      "layer": 4,
      "closed_by": [
        7
      ]
    },
    {
      "id": 10,
      "x": 1.74,
      "y": 0.859,
      "angle": 300.0,
      "layer": 4,
      "closed_by": [
        4
      ]
    },
    {
      "id": 11,
      "x": -2.318,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        17
      ]
    },
    {
      "id": 12,
      "x": -2.318,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        13
      ]
    },
    {
      "id": 13,
      "x": -2.219,
      "y": -0.216,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        15
      ]
    },
    {
      "id": 14,
      "x": -2.118,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        9
      ]
    },
    {
      "id": 15,
      "x": -2.118,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        6
      ]
    },
    {
      "id": 16,
      "x": -1.498,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        9
      ]
    },
    {
      "id": 17,
      "x": -2.219,
      "y": 0.819,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        14
      ]
    },
    {
      "id": 18,
      "x": 1.74,
      "y": -0.279,
      "angle": 60.0,
      "layer": 4,
      "closed_by": [
        8
      ]
    },
    {
      "id": 19,
      "x": -1.498,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        6
      ]
    },
    {
      "id": 20,
      "x": -1.399,
      "y": 0.819,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        16
      ]
    },
    {
      "id": 21,
      "x": -1.399,
      "y": -0.216,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        19
      ]
    },
    {
      "id": 22,
      "x": -1.299,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        21
      ]
    },
    {
      "id": 23,
      "x": 2.358,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        10
      ]
    },
    {
      "id": 24,
      "x": 2.457,
      "y": 0.819,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 25,
      "x": 2.555,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        4,
        24
      ]
    },
    {
      "id": 26,
      "x": 2.555,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        27
      ]
    },
    {
      "id": 27,
      "x": 2.457,
      "y": -0.216,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 28,
      "x": 2.358,
      "y": -0.279,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 29,
      "x": 1.74,
      "y": -0.279,
      "layer": 3,
      "closed_by": [
        18
      ]
    },
    {
      "id": 30,
      "x": 1.639,
      "y": 0.819,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 31,
      "x": 1.639,
      "y": -0.216,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 32,
      "x": 1.539,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        30
      ]
    },
    {
      "id": 33,
      "x": 1.539,
      "y": -0.179,
      "layer": 1,
      "closed_by": [
        31
      ]
    },
    {
      "id": 34,
      "x": -0.259,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 35,
      "x": -0.418,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 36,
      "x": -0.74,
      "y": -0.017,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 37,
      "x": 1.74,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        10
      ]
    },
    {
      "id": 38,
      "x": -1.299,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        20
      ]
    },
    {
      "id": 39,
      "x": -0.74,
      "y": 0.379,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 40,
      "x": -0.62,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 41,
      "x": -0.298,
      "y": 0.859,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 42,
      "x": 0.017,
      "y": 0.939,
      "layer": 1,
      "closed_by": [
        41,
        43
      ]
    },
    {
      "id": 43,
      "x": 0.298,
      "y": 1.019,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 44,
      "x": 0.537,
      "y": 0.939,
      "layer": 3,
      "closed_by": [
        45
      ]
    },
    {
      "id": 45,
      "x": 0.777,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        51
      ]
    },
    {
      "id": 46,
      "x": 0.98,
      "y": -0.179,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 47,
      "x": 0.777,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": 0.537,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 49,
      "x": 0.298,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 50,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        34,
        49
      ]
    },
    {
      "id": 51,
      "x": 0.98,
      "y": 0.777,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 48,
  "cards_in_deck_amount": 26,
  "layers_in_level": 5,
  "open_cards": 7,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 0.5,
    "star2": 62.3,
    "star3": 36.6
  }
}