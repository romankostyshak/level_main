{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.004,
      "y": 1.023,
      "layer": 1,
      "closed_by": [
        50,
        49
      ]
    },
    {
      "id": 50,
      "x": 0.333,
      "y": 0.861,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 49,
      "x": -0.331,
      "y": 0.864,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 48,
      "x": 0.172,
      "y": 0.814,
      "angle": 35.0,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "x": -0.172,
      "y": 0.814,
      "angle": 325.0,
      "layer": 4,
      "closed_by": [
        46
      ]
    },
    {
      "id": 46,
      "y": 0.462,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 42,
      "x": -0.569,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 40,
      "x": 0.337,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 39,
      "x": 0.001,
      "y": -0.418,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 0.865,
      "y": 0.703,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 37,
      "x": -0.865,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 36,
      "x": 1.184,
      "y": 0.703,
      "layer": 3,
      "closed_by": [
        34
      ]
    },
    {
      "id": 35,
      "x": -1.184,
      "y": 0.699,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 34,
      "x": 1.508,
      "y": 0.702,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 33,
      "x": -1.503,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 32,
      "x": 1.508,
      "y": -0.18,
      "layer": 1,
      "closed_by": [
        30
      ]
    },
    {
      "id": 31,
      "x": -1.503,
      "y": -0.18,
      "layer": 1,
      "closed_by": [
        29
      ]
    },
    {
      "id": 30,
      "x": 1.506,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        28
      ]
    },
    {
      "id": 29,
      "x": -1.503,
      "y": -0.414,
      "layer": 2,
      "closed_by": [
        27
      ]
    },
    {
      "id": 28,
      "x": 1.292,
      "y": -0.416,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 27,
      "x": -1.296,
      "y": -0.416,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 26,
      "x": 1.133,
      "y": -0.418,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 25,
      "x": -1.133,
      "y": -0.416,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 24,
      "x": 1.133,
      "y": -0.18,
      "layer": 5,
      "closed_by": [
        21
      ]
    },
    {
      "id": 23,
      "x": -1.136,
      "y": -0.18,
      "layer": 5,
      "closed_by": [
        20
      ]
    },
    {
      "id": 21,
      "x": 1.508,
      "y": 0.298,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 20,
      "x": -1.503,
      "y": 0.307,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 43,
      "x": 0.574,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 41,
      "x": -0.337,
      "y": -0.418,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 45,
      "x": 0.574,
      "y": -0.017,
      "layer": 1,
      "closed_by": [
        38,
        43
      ]
    },
    {
      "id": 44,
      "x": -0.569,
      "y": -0.017,
      "layer": 1,
      "closed_by": [
        42,
        37
      ]
    }
  ],
  "cards_in_layout_amount": 31,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 31.3,
    "star2": 60.0,
    "star3": 7.7
  }
}