{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.388,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        44
      ]
    },
    {
      "id": 50,
      "x": 0.388,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 48,
      "x": 0.384,
      "y": -0.18,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 47,
      "x": -0.384,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 46,
      "y": 0.136,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 45,
      "x": 0.263,
      "y": 0.856,
      "layer": 2,
      "closed_by": [
        46,
        38
      ]
    },
    {
      "id": 44,
      "x": -0.252,
      "y": 0.856,
      "layer": 2,
      "closed_by": [
        46,
        39
      ]
    },
    {
      "id": 43,
      "x": 0.652,
      "y": 0.136,
      "layer": 1,
      "closed_by": [
        48,
        45,
        35
      ]
    },
    {
      "id": 42,
      "x": -0.652,
      "y": 0.136,
      "layer": 1,
      "closed_by": [
        47,
        44,
        41
      ]
    },
    {
      "id": 41,
      "x": -0.976,
      "y": 0.224,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 40,
      "x": -1.052,
      "y": 0.46,
      "layer": 3,
      "closed_by": [
        33
      ]
    },
    {
      "id": 39,
      "x": -0.254,
      "y": 1.021,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 38,
      "x": 0.259,
      "y": 1.021,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 37,
      "y": 1.021,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 0.98,
      "y": 0.224,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 34,
      "x": 1.052,
      "y": 0.46,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 33,
      "x": -1.128,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 32,
      "x": -1.666,
      "y": 0.702,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 31,
      "x": 1.666,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 30,
      "x": 1.746,
      "y": 0.462,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 29,
      "x": -1.746,
      "y": 0.46,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 28,
      "x": 1.827,
      "y": 0.224,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 27,
      "x": -1.825,
      "y": 0.224,
      "layer": 2,
      "closed_by": [
        29
      ]
    },
    {
      "id": 26,
      "x": 1.401,
      "y": 0.136,
      "layer": 1,
      "closed_by": [
        35,
        28
      ]
    },
    {
      "id": 25,
      "x": -1.401,
      "y": 0.143,
      "layer": 1,
      "closed_by": [
        41,
        27
      ]
    },
    {
      "id": 24,
      "x": 1.133,
      "y": 0.699,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 23,
      "x": 1.399,
      "y": 0.782,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 22,
      "x": -1.401,
      "y": 0.782,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 36,
      "y": -0.5,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 49,
      "x": 0.001,
      "y": -0.18,
      "layer": 1,
      "closed_by": [
        48,
        47
      ]
    }
  ],
  "cards_in_layout_amount": 30,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 41.6,
    "star2": 48.1,
    "star3": 9.8
  }
}