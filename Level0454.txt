{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.256,
      "y": -0.572,
      "layer": 1,
      "closed_by": [
        25
      ]
    },
    {
      "id": 50,
      "x": 0.337,
      "y": -0.15,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 49,
      "x": -0.354,
      "y": -0.15,
      "angle": 9.998,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 48,
      "x": -0.002,
      "y": 0.689,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 47,
      "x": -0.001,
      "y": -0.119,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 46,
      "x": 1.182,
      "y": -0.18,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 45,
      "x": -1.2,
      "y": -0.184,
      "angle": 9.998,
      "layer": 3,
      "closed_by": [
        40
      ]
    },
    {
      "id": 44,
      "x": 0.259,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        24
      ]
    },
    {
      "id": 43,
      "x": -1.48,
      "y": 0.74,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 42,
      "x": 1.715,
      "y": -0.128,
      "angle": 349.997,
      "layer": 1,
      "closed_by": [
        33
      ]
    },
    {
      "id": 41,
      "x": -0.939,
      "y": 0.74,
      "angle": 349.997,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 40,
      "x": -0.939,
      "y": -0.136,
      "angle": 9.998,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 39,
      "x": -1.2,
      "y": 0.786,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        41
      ]
    },
    {
      "id": 38,
      "x": 1.457,
      "y": 0.74,
      "angle": 9.998,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 37,
      "x": 1.192,
      "y": 0.785,
      "angle": 9.998,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 36,
      "x": 1.72,
      "y": 0.74,
      "angle": 9.998,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 34,
      "x": 0.254,
      "y": -0.573,
      "layer": 1,
      "closed_by": [
        26
      ]
    },
    {
      "id": 33,
      "x": 1.452,
      "y": -0.128,
      "angle": 349.997,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 32,
      "x": -1.761,
      "y": -0.128,
      "angle": 9.998,
      "layer": 1,
      "closed_by": [
        28
      ]
    },
    {
      "id": 31,
      "x": 0.337,
      "y": 0.72,
      "angle": 9.998,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 30,
      "x": -0.351,
      "y": 0.72,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 29,
      "x": -1.758,
      "y": 0.74,
      "angle": 349.997,
      "layer": 1,
      "closed_by": [
        43
      ]
    },
    {
      "id": 28,
      "x": -1.48,
      "y": -0.136,
      "angle": 9.998,
      "layer": 2,
      "closed_by": [
        45
      ]
    },
    {
      "id": 27,
      "x": 0.933,
      "y": -0.136,
      "angle": 349.997,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 26,
      "x": 0.287,
      "y": -0.379,
      "angle": 354.997,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 25,
      "x": -0.291,
      "y": -0.379,
      "angle": 5.0,
      "layer": 2,
      "closed_by": [
        49
      ]
    },
    {
      "id": 24,
      "x": 0.289,
      "y": 0.92,
      "angle": 5.0,
      "layer": 2,
      "closed_by": [
        31
      ]
    },
    {
      "id": 23,
      "x": -0.259,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 22,
      "x": -0.291,
      "y": 0.92,
      "angle": 354.997,
      "layer": 2,
      "closed_by": [
        30
      ]
    },
    {
      "id": 35,
      "x": 0.93,
      "y": 0.74,
      "angle": 9.998,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 20,
      "x": 0.999,
      "y": 0.298,
      "layer": 5,
      "closed_by": [],
      "effect_id": 3
    },
    {
      "id": 21,
      "x": -1.001,
      "y": 0.298,
      "layer": 5,
      "closed_by": [],
      "effect_id": 3
    }
  ],
  "cards_in_layout_amount": 32,
  "cards_in_deck_amount": 30,
  "stars_statistic": {
    "star1": 22.8,
    "star2": 67.2,
    "star3": 9.4
  }
}