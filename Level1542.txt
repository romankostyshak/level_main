{
  "layout_cards": [
    {
      "id": 51,
      "x": -0.943,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        44,
        43
      ]
    },
    {
      "id": 50,
      "x": -0.943,
      "y": -0.569,
      "layer": 1,
      "closed_by": [
        46,
        45
      ]
    },
    {
      "id": 49,
      "x": 1.026,
      "y": -0.573,
      "layer": 1,
      "closed_by": [
        38,
        37
      ]
    },
    {
      "id": 48,
      "x": 0.685,
      "y": 0.976,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        36
      ]
    },
    {
      "id": 47,
      "x": 1.371,
      "y": 0.981,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        35
      ]
    },
    {
      "id": 46,
      "x": -0.595,
      "y": -0.537,
      "angle": 344.997,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 45,
      "x": -1.291,
      "y": -0.537,
      "angle": 15.0,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 44,
      "x": -0.671,
      "y": 0.944,
      "layer": 2,
      "closed_by": [
        42
      ]
    },
    {
      "id": 43,
      "x": -1.205,
      "y": 0.944,
      "layer": 2,
      "closed_by": [
        39
      ]
    },
    {
      "id": 42,
      "x": -0.67,
      "y": 0.865,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 41,
      "x": 0.763,
      "y": -0.416,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 40,
      "x": 1.299,
      "y": -0.416,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 39,
      "x": -1.205,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 38,
      "x": 1.297,
      "y": -0.523,
      "layer": 2,
      "closed_by": [
        40
      ]
    },
    {
      "id": 37,
      "x": 0.763,
      "y": -0.527,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 36,
      "x": 0.689,
      "y": 0.884,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        30
      ]
    },
    {
      "id": 34,
      "x": -0.601,
      "y": -0.449,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 33,
      "x": -1.276,
      "y": -0.449,
      "angle": 9.998,
      "layer": 3,
      "closed_by": [
        32
      ]
    },
    {
      "id": 32,
      "x": -1.261,
      "y": -0.354,
      "angle": 5.0,
      "layer": 4,
      "closed_by": [
        12
      ]
    },
    {
      "id": 31,
      "x": -0.619,
      "y": -0.354,
      "angle": 354.997,
      "layer": 4,
      "closed_by": [
        23
      ]
    },
    {
      "id": 30,
      "x": 0.708,
      "y": 0.795,
      "angle": 354.997,
      "layer": 4,
      "closed_by": [
        13
      ]
    },
    {
      "id": 28,
      "x": 0.759,
      "y": -0.335,
      "layer": 4,
      "closed_by": [
        21
      ]
    },
    {
      "id": 27,
      "x": 1.299,
      "y": -0.333,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 26,
      "x": -0.669,
      "y": 0.782,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 25,
      "x": -1.205,
      "y": 0.787,
      "layer": 4,
      "closed_by": [
        19
      ]
    },
    {
      "id": 23,
      "x": -0.642,
      "y": -0.25,
      "layer": 5,
      "closed_by": [
        16
      ]
    },
    {
      "id": 22,
      "x": 1.299,
      "y": -0.256,
      "layer": 5,
      "closed_by": [
        18
      ]
    },
    {
      "id": 21,
      "x": 0.762,
      "y": -0.256,
      "layer": 5,
      "closed_by": [
        17
      ]
    },
    {
      "id": 20,
      "x": -0.669,
      "y": 0.694,
      "layer": 5,
      "closed_by": [
        16
      ]
    },
    {
      "id": 19,
      "x": -1.205,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        15
      ]
    },
    {
      "id": 17,
      "x": 0.734,
      "y": 0.216,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 16,
      "x": -0.638,
      "y": 0.224,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 15,
      "x": -1.235,
      "y": 0.223,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 14,
      "x": 1.024,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        48,
        47
      ]
    },
    {
      "id": 13,
      "x": 0.734,
      "y": 0.679,
      "layer": 5,
      "closed_by": [
        17
      ]
    },
    {
      "id": 12,
      "x": -1.235,
      "y": -0.256,
      "layer": 5,
      "closed_by": [
        15
      ]
    },
    {
      "id": 29,
      "x": 1.353,
      "y": 0.796,
      "angle": 5.0,
      "layer": 4,
      "closed_by": [
        24
      ]
    },
    {
      "id": 24,
      "x": 1.33,
      "y": 0.699,
      "layer": 5,
      "closed_by": [
        18
      ]
    },
    {
      "id": 35,
      "x": 1.371,
      "y": 0.888,
      "angle": 9.998,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 18,
      "x": 1.33,
      "y": 0.216,
      "layer": 6,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 40,
  "cards_in_deck_amount": 28,
  "layers_in_level": 6,
  "open_cards": 4,
  "bonus_cards_on_the_layers": {},
  "stars_statistic": {
    "star1": 0.0,
    "star2": 16.3,
    "star3": 82.9
  }
}