{
  "layout_cards": [
    {
      "id": 49,
      "x": 0.172,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        47
      ]
    },
    {
      "id": 47,
      "x": 0.254,
      "y": 0.981,
      "angle": 345.0,
      "layer": 2,
      "closed_by": [
        46
      ]
    },
    {
      "id": 46,
      "x": 0.254,
      "y": 0.578,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        44
      ]
    },
    {
      "id": 48,
      "x": -0.171,
      "y": -0.33,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 45,
      "x": -0.246,
      "y": -0.527,
      "angle": 345.0,
      "layer": 2,
      "closed_by": [
        50
      ]
    },
    {
      "id": 50,
      "x": -0.246,
      "y": -0.112,
      "angle": 15.0,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 44,
      "x": 0.523,
      "y": 0.981,
      "angle": 345.0,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 43,
      "x": -0.512,
      "y": -0.527,
      "angle": 345.0,
      "layer": 4,
      "closed_by": [
        41
      ]
    },
    {
      "id": 42,
      "x": 0.517,
      "y": 0.572,
      "angle": 15.0,
      "layer": 5,
      "closed_by": [
        40
      ]
    },
    {
      "id": 41,
      "x": -0.521,
      "y": -0.103,
      "angle": 14.998,
      "layer": 5,
      "closed_by": [
        39
      ]
    },
    {
      "id": 40,
      "x": 0.805,
      "y": 0.981,
      "angle": 345.0,
      "layer": 6,
      "closed_by": [
        38
      ]
    },
    {
      "id": 39,
      "x": -0.781,
      "y": -0.526,
      "angle": 345.0,
      "layer": 6,
      "closed_by": [
        35
      ]
    },
    {
      "id": 38,
      "x": 0.8,
      "y": 0.573,
      "angle": 15.0,
      "layer": 7,
      "closed_by": [
        34
      ]
    },
    {
      "id": 35,
      "x": -0.794,
      "y": -0.097,
      "angle": 15.0,
      "layer": 7,
      "closed_by": [
        33
      ]
    },
    {
      "id": 30,
      "x": 1.659,
      "y": 0.777,
      "layer": 6,
      "closed_by": [
        32
      ]
    },
    {
      "id": 36,
      "x": -1.899,
      "y": -0.337,
      "layer": 5,
      "closed_by": [
        29
      ]
    },
    {
      "id": 32,
      "x": 1.419,
      "y": 0.777,
      "layer": 7,
      "closed_by": [
        34
      ]
    },
    {
      "id": 28,
      "x": 1.899,
      "y": 0.777,
      "layer": 5,
      "closed_by": [
        30
      ]
    },
    {
      "id": 27,
      "x": 2.138,
      "y": 0.777,
      "layer": 4,
      "closed_by": [
        28
      ]
    },
    {
      "id": 25,
      "x": 2.38,
      "y": 0.777,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 23,
      "x": 2.618,
      "y": 0.777,
      "layer": 2,
      "closed_by": [
        25
      ]
    },
    {
      "id": 31,
      "x": -1.439,
      "y": -0.337,
      "layer": 7,
      "closed_by": [
        33
      ]
    },
    {
      "id": 29,
      "x": -1.659,
      "y": -0.337,
      "layer": 6,
      "closed_by": [
        31
      ]
    },
    {
      "id": 26,
      "x": -2.14,
      "y": -0.337,
      "layer": 4,
      "closed_by": [
        36
      ]
    },
    {
      "id": 24,
      "x": -2.378,
      "y": -0.337,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 22,
      "x": -2.618,
      "y": -0.337,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 33,
      "x": -1.019,
      "y": -0.337,
      "layer": 8,
      "closed_by": []
    },
    {
      "id": 34,
      "x": 1.019,
      "y": 0.777,
      "layer": 8,
      "closed_by": []
    },
    {
      "id": 21,
      "x": -0.412,
      "y": 0.782,
      "layer": 1,
      "closed_by": [
        19
      ]
    },
    {
      "id": 51,
      "x": 0.412,
      "y": -0.337,
      "layer": 1,
      "closed_by": [
        20
      ]
    },
    {
      "id": 20,
      "x": 0.625,
      "y": -0.34,
      "layer": 2,
      "closed_by": [
        18
      ]
    },
    {
      "id": 19,
      "x": -0.624,
      "y": 0.785,
      "layer": 2,
      "closed_by": [
        15
      ]
    },
    {
      "id": 18,
      "x": 0.865,
      "y": -0.34,
      "layer": 3,
      "closed_by": [
        14
      ]
    },
    {
      "id": 15,
      "x": -0.864,
      "y": 0.785,
      "layer": 3,
      "closed_by": [
        13
      ]
    },
    {
      "id": 14,
      "x": 1.105,
      "y": -0.34,
      "layer": 4,
      "closed_by": [
        12
      ]
    },
    {
      "id": 13,
      "x": -1.103,
      "y": 0.785,
      "layer": 4,
      "closed_by": [
        16
      ]
    },
    {
      "id": 12,
      "x": 1.348,
      "y": -0.337,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 16,
      "x": -1.345,
      "y": 0.782,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 38,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 6.4,
    "star2": 61.7,
    "star3": 31.7
  }
}