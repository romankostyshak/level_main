{
  "layout_cards": [
    {
      "id": 12,
      "x": -1.049,
      "y": 0.3,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 13,
      "x": -1.985,
      "y": 0.236,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 14,
      "x": -1.983,
      "y": -0.578,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 15,
      "x": 1.988,
      "y": 0.238,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 16,
      "x": 1.054,
      "y": 0.3,
      "layer": 1,
      "closed_by": [
        28
      ]
    },
    {
      "id": 18,
      "x": 1.99,
      "y": -0.578,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 19,
      "x": -1.508,
      "y": -0.578,
      "layer": 3,
      "closed_by": [
        37
      ]
    },
    {
      "id": 20,
      "x": 1.511,
      "y": -0.583,
      "layer": 3,
      "closed_by": [
        21
      ]
    },
    {
      "id": 21,
      "x": 1.827,
      "y": -0.184,
      "layer": 4,
      "closed_by": [
        15,
        18
      ]
    },
    {
      "id": 22,
      "x": -1.825,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        13
      ]
    },
    {
      "id": 23,
      "x": 1.827,
      "y": 0.62,
      "layer": 4,
      "closed_by": [
        15
      ]
    },
    {
      "id": 24,
      "x": -1.266,
      "y": 0.62,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 1.269,
      "y": 0.62,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 26,
      "x": -1.503,
      "y": 0.228,
      "layer": 3,
      "closed_by": [
        22,
        24,
        37
      ]
    },
    {
      "id": 27,
      "x": 1.506,
      "y": 0.223,
      "layer": 3,
      "closed_by": [
        21,
        23,
        25
      ]
    },
    {
      "id": 28,
      "x": 1.266,
      "y": -0.178,
      "layer": 2,
      "closed_by": [
        20,
        27
      ]
    },
    {
      "id": 29,
      "x": -1.052,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 30,
      "x": 1.054,
      "y": -0.583,
      "layer": 1,
      "closed_by": [
        28
      ]
    },
    {
      "id": 31,
      "x": 0.002,
      "y": 1.021,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 32,
      "x": 0.273,
      "y": 0.86,
      "layer": 4,
      "closed_by": [
        31,
        35
      ]
    },
    {
      "id": 33,
      "x": -0.25,
      "y": 0.86,
      "layer": 4,
      "closed_by": [
        31,
        34
      ]
    },
    {
      "id": 34,
      "x": -0.513,
      "y": 0.379,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 35,
      "x": 0.518,
      "y": 0.379,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 36,
      "x": -1.264,
      "y": -0.179,
      "layer": 2,
      "closed_by": [
        19,
        26
      ]
    },
    {
      "id": 37,
      "x": -1.824,
      "y": -0.18,
      "layer": 4,
      "closed_by": [
        13,
        14
      ]
    },
    {
      "id": 38,
      "x": 0.001,
      "y": -0.574,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 39,
      "x": -0.509,
      "y": -0.574,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 0.513,
      "y": -0.574,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 0.307,
      "y": -0.259,
      "layer": 4,
      "closed_by": [
        35,
        38,
        40
      ]
    },
    {
      "id": 42,
      "x": -0.305,
      "y": -0.259,
      "layer": 4,
      "closed_by": [
        34,
        38,
        39
      ]
    },
    {
      "id": 43,
      "x": 0.002,
      "y": 0.224,
      "layer": 3,
      "closed_by": [
        32,
        33,
        41,
        42
      ]
    },
    {
      "id": 44,
      "x": -0.517,
      "y": 0.224,
      "layer": 3,
      "closed_by": [
        33,
        42
      ]
    },
    {
      "id": 45,
      "x": 0.333,
      "y": 0.541,
      "layer": 2,
      "closed_by": [
        43,
        50
      ]
    },
    {
      "id": 46,
      "x": -0.305,
      "y": 0.541,
      "layer": 2,
      "closed_by": [
        43,
        44
      ]
    },
    {
      "id": 47,
      "x": -0.513,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": 0.517,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        45
      ]
    },
    {
      "id": 49,
      "x": 0.002,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        45,
        46
      ]
    },
    {
      "id": 50,
      "x": 0.517,
      "y": 0.223,
      "layer": 3,
      "closed_by": [
        32,
        41
      ]
    }
  ],
  "cards_in_layout_amount": 38,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 37.0,
    "star2": 57.3,
    "star3": 4.9
  }
}