{
  "layout_cards": [
    {
      "id": 3,
      "x": -1.654,
      "y": 0.86,
      "layer": 1,
      "closed_by": [
        4
      ]
    },
    {
      "id": 4,
      "x": -1.651,
      "y": 0.615,
      "layer": 2,
      "closed_by": [
        8
      ]
    },
    {
      "id": 5,
      "x": 1.639,
      "y": -0.016,
      "layer": 2,
      "closed_by": [
        15
      ]
    },
    {
      "id": 6,
      "x": 1.639,
      "y": -0.256,
      "layer": 1,
      "closed_by": [
        5
      ]
    },
    {
      "id": 8,
      "x": -1.651,
      "y": 0.381,
      "layer": 3,
      "closed_by": [
        9
      ]
    },
    {
      "id": 9,
      "x": -1.649,
      "y": 0.14,
      "layer": 4,
      "closed_by": [
        12
      ]
    },
    {
      "id": 10,
      "x": 1.643,
      "y": 0.462,
      "layer": 4,
      "closed_by": [
        14
      ]
    },
    {
      "id": 11,
      "x": -1.649,
      "y": -0.337,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 12,
      "x": -1.648,
      "y": -0.097,
      "layer": 5,
      "closed_by": [
        11
      ]
    },
    {
      "id": 13,
      "x": 1.644,
      "y": 0.939,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 14,
      "x": 1.644,
      "y": 0.703,
      "layer": 5,
      "closed_by": [
        13
      ]
    },
    {
      "id": 15,
      "x": 1.639,
      "y": 0.224,
      "layer": 3,
      "closed_by": [
        10
      ]
    },
    {
      "id": 17,
      "x": -0.828,
      "y": 0.781,
      "layer": 3,
      "closed_by": [
        24
      ]
    },
    {
      "id": 18,
      "x": -0.827,
      "y": 0.3,
      "layer": 1,
      "closed_by": [
        19,
        20,
        21,
        22
      ]
    },
    {
      "id": 19,
      "x": -0.518,
      "y": -0.119,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 20,
      "x": -1.131,
      "y": -0.119,
      "layer": 2,
      "closed_by": [
        23
      ]
    },
    {
      "id": 21,
      "x": -0.518,
      "y": 0.699,
      "layer": 2,
      "closed_by": [
        17
      ]
    },
    {
      "id": 22,
      "x": -1.128,
      "y": 0.702,
      "layer": 2,
      "closed_by": [
        17
      ]
    },
    {
      "id": 23,
      "x": -0.828,
      "y": -0.18,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 24,
      "x": -0.832,
      "y": 0.861,
      "layer": 4,
      "closed_by": [
        26,
        29
      ]
    },
    {
      "id": 25,
      "x": -0.828,
      "y": -0.254,
      "layer": 4,
      "closed_by": [
        27,
        32
      ]
    },
    {
      "id": 26,
      "x": -0.517,
      "y": 0.939,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 27,
      "x": -0.521,
      "y": -0.335,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 0.819,
      "y": 0.3,
      "layer": 1,
      "closed_by": [
        33,
        34,
        35,
        36
      ]
    },
    {
      "id": 29,
      "x": -1.131,
      "y": 0.939,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 30,
      "x": -0.004,
      "y": -0.259,
      "layer": 5,
      "closed_by": [
        45
      ]
    },
    {
      "id": 31,
      "x": 0.824,
      "y": -0.259,
      "layer": 4,
      "closed_by": [
        42,
        43
      ]
    },
    {
      "id": 32,
      "x": -1.133,
      "y": -0.342,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 33,
      "x": 1.126,
      "y": 0.703,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 34,
      "x": 0.513,
      "y": 0.703,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 35,
      "x": 1.126,
      "y": -0.108,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 36,
      "x": 0.513,
      "y": -0.108,
      "layer": 2,
      "closed_by": [
        38
      ]
    },
    {
      "id": 37,
      "x": 0.819,
      "y": 0.782,
      "layer": 3,
      "closed_by": [
        39
      ]
    },
    {
      "id": 38,
      "x": 0.823,
      "y": -0.171,
      "layer": 3,
      "closed_by": [
        31
      ]
    },
    {
      "id": 39,
      "x": 0.819,
      "y": 0.864,
      "layer": 4,
      "closed_by": [
        40,
        41
      ]
    },
    {
      "id": 40,
      "x": 1.128,
      "y": 0.944,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 41,
      "x": 0.513,
      "y": 0.939,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 42,
      "x": 1.128,
      "y": -0.337,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 43,
      "x": 0.513,
      "y": -0.335,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 44,
      "y": 0.939,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 45,
      "x": -0.004,
      "y": -0.337,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 46,
      "x": -0.002,
      "y": 0.861,
      "layer": 5,
      "closed_by": [
        44
      ]
    },
    {
      "id": 47,
      "x": -0.004,
      "y": 0.782,
      "layer": 4,
      "closed_by": [
        46
      ]
    },
    {
      "id": 48,
      "x": -0.004,
      "y": -0.179,
      "layer": 4,
      "closed_by": [
        30
      ]
    },
    {
      "id": 49,
      "x": -0.003,
      "y": 0.716,
      "layer": 3,
      "closed_by": [
        47
      ]
    },
    {
      "id": 50,
      "x": -0.004,
      "y": -0.1,
      "layer": 3,
      "closed_by": [
        48
      ]
    },
    {
      "id": 51,
      "x": -0.007,
      "y": 0.305,
      "layer": 2,
      "closed_by": [
        49,
        50
      ]
    }
  ],
  "cards_in_layout_amount": 47,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 0.2,
    "star2": 4.8,
    "star3": 94.2
  }
}