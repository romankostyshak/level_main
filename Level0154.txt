{
  "layout_cards": [
    {
      "id": 51,
      "y": 0.99,
      "layer": 1,
      "closed_by": [
        50,
        49
      ]
    },
    {
      "id": 50,
      "x": 0.287,
      "y": 0.601,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 49,
      "x": -0.279,
      "y": 0.601,
      "layer": 2,
      "closed_by": [
        48
      ]
    },
    {
      "id": 48,
      "x": -0.133,
      "y": 0.379,
      "layer": 3,
      "closed_by": [
        43
      ]
    },
    {
      "id": 47,
      "x": -0.27,
      "y": -0.462,
      "layer": 2,
      "closed_by": [
        41
      ]
    },
    {
      "id": 46,
      "x": 0.279,
      "y": -0.462,
      "layer": 2,
      "closed_by": [
        44
      ]
    },
    {
      "id": 45,
      "y": -0.572,
      "layer": 1,
      "closed_by": [
        47,
        46
      ]
    },
    {
      "id": 44,
      "x": 0.638,
      "y": -0.391,
      "layer": 3,
      "closed_by": [
        43,
        28
      ]
    },
    {
      "id": 43,
      "x": 0.149,
      "y": 0.379,
      "layer": 4,
      "closed_by": [
        42
      ]
    },
    {
      "id": 42,
      "y": 0.379,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 41,
      "x": -0.629,
      "y": -0.393,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 40,
      "x": -1.791,
      "y": -0.119,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 39,
      "x": 1.799,
      "y": -0.119,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 38,
      "x": 1.419,
      "y": 0.92,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 37,
      "x": 2.128,
      "y": 0.92,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 36,
      "x": -1.394,
      "y": 0.92,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 35,
      "x": -2.095,
      "y": 0.92,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 34,
      "x": 1.779,
      "y": 1.034,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 33,
      "x": -1.735,
      "y": 1.031,
      "layer": 3,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -2.45,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        35
      ]
    },
    {
      "id": 31,
      "x": -1.044,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        36
      ]
    },
    {
      "id": 30,
      "x": 1.059,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        38
      ]
    },
    {
      "id": 29,
      "x": 2.489,
      "y": 0.699,
      "layer": 1,
      "closed_by": [
        37
      ]
    },
    {
      "id": 28,
      "x": 1.019,
      "y": -0.512,
      "layer": 4,
      "closed_by": [
        27
      ]
    },
    {
      "id": 27,
      "x": 1.48,
      "y": -0.512,
      "layer": 5,
      "closed_by": [
        39
      ]
    },
    {
      "id": 26,
      "x": -1.011,
      "y": -0.512,
      "layer": 4,
      "closed_by": [
        25
      ]
    },
    {
      "id": 25,
      "x": -1.47,
      "y": -0.512,
      "layer": 5,
      "closed_by": [
        40
      ]
    }
  ],
  "cards_in_layout_amount": 27,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 61.5,
    "star2": 28.6,
    "star3": 9.4
  }
}