{
  "layout_cards": [
    {
      "id": 17,
      "x": 1.906,
      "y": -0.577,
      "layer": 5,
      "closed_by": [
        48
      ]
    },
    {
      "id": 18,
      "x": 0.625,
      "y": -0.259,
      "layer": 3,
      "closed_by": [
        25
      ]
    },
    {
      "id": 19,
      "y": -0.577,
      "layer": 1,
      "closed_by": [
        22,
        23
      ]
    },
    {
      "id": 20,
      "x": 0.545,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        22
      ]
    },
    {
      "id": 21,
      "x": -0.544,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        23
      ]
    },
    {
      "id": 22,
      "x": 0.333,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        18,
        28
      ]
    },
    {
      "id": 23,
      "x": -0.331,
      "y": -0.418,
      "layer": 2,
      "closed_by": [
        24
      ]
    },
    {
      "id": 24,
      "x": -0.624,
      "y": -0.256,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 25,
      "x": 0.869,
      "y": -0.097,
      "layer": 4,
      "closed_by": [
        28,
        37
      ]
    },
    {
      "id": 26,
      "x": -0.865,
      "y": -0.097,
      "layer": 4,
      "closed_by": [
        27,
        38
      ]
    },
    {
      "id": 27,
      "x": -0.412,
      "y": -0.1,
      "layer": 5,
      "closed_by": [
        46
      ]
    },
    {
      "id": 28,
      "x": 0.414,
      "y": -0.097,
      "layer": 5,
      "closed_by": [
        46
      ]
    },
    {
      "id": 29,
      "y": 0.777,
      "layer": 1,
      "closed_by": [
        30,
        32
      ]
    },
    {
      "id": 30,
      "x": 0.467,
      "y": 0.86,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 31,
      "x": -1.345,
      "y": -0.254,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 32,
      "x": -0.465,
      "y": 0.86,
      "layer": 2,
      "closed_by": [
        34
      ]
    },
    {
      "id": 33,
      "x": 0.865,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        35
      ]
    },
    {
      "id": 34,
      "x": -0.865,
      "y": 0.859,
      "layer": 3,
      "closed_by": [
        36
      ]
    },
    {
      "id": 35,
      "x": 1.347,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        37,
        40
      ]
    },
    {
      "id": 36,
      "x": -1.345,
      "y": 0.859,
      "layer": 4,
      "closed_by": [
        38,
        41
      ]
    },
    {
      "id": 37,
      "x": 1.345,
      "y": 0.143,
      "layer": 5,
      "closed_by": [
        39
      ]
    },
    {
      "id": 38,
      "x": -1.345,
      "y": 0.142,
      "layer": 5,
      "closed_by": [
        31
      ]
    },
    {
      "id": 39,
      "x": 1.347,
      "y": -0.254,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 40,
      "x": 1.667,
      "y": 0.953,
      "layer": 5,
      "closed_by": [
        42
      ]
    },
    {
      "id": 41,
      "x": -1.666,
      "y": 0.953,
      "layer": 5,
      "closed_by": [
        43
      ]
    },
    {
      "id": 42,
      "x": 2.068,
      "y": 1.021,
      "layer": 6,
      "closed_by": [
        50
      ]
    },
    {
      "id": 43,
      "x": -2.065,
      "y": 1.019,
      "layer": 6,
      "closed_by": [
        51
      ]
    },
    {
      "id": 44,
      "x": 1.503,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        17,
        37
      ]
    },
    {
      "id": 45,
      "x": -1.503,
      "y": -0.578,
      "layer": 4,
      "closed_by": [
        38,
        47
      ]
    },
    {
      "id": 46,
      "y": -0.097,
      "layer": 6,
      "closed_by": []
    },
    {
      "id": 47,
      "x": -1.906,
      "y": -0.578,
      "layer": 5,
      "closed_by": [
        49
      ]
    },
    {
      "id": 48,
      "x": 2.069,
      "y": -0.259,
      "layer": 6,
      "closed_by": [
        50
      ]
    },
    {
      "id": 49,
      "x": -2.065,
      "y": -0.256,
      "layer": 6,
      "closed_by": [
        51
      ]
    },
    {
      "id": 50,
      "x": 2.065,
      "y": 0.379,
      "layer": 7,
      "closed_by": []
    },
    {
      "id": 51,
      "x": -2.065,
      "y": 0.388,
      "layer": 7,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 35,
  "cards_in_deck_amount": 28,
  "stars_statistic": {
    "star1": 18.7,
    "star2": 71.2,
    "star3": 9.5
  }
}