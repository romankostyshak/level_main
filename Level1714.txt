{
  "layout_cards": [
    {
      "id": 51,
      "x": 0.003,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        43,
        42
      ]
    },
    {
      "id": 50,
      "x": -0.002,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        45,
        44
      ]
    },
    {
      "id": 49,
      "x": 0.574,
      "y": -0.578,
      "layer": 1,
      "closed_by": [
        44,
        41
      ]
    },
    {
      "id": 48,
      "x": -0.573,
      "y": -0.574,
      "layer": 1,
      "closed_by": [
        45,
        40
      ]
    },
    {
      "id": 47,
      "x": 0.573,
      "y": 1.019,
      "layer": 1,
      "closed_by": [
        43,
        39
      ]
    },
    {
      "id": 46,
      "x": -0.569,
      "y": 1.021,
      "layer": 1,
      "closed_by": [
        42,
        38
      ]
    },
    {
      "id": 45,
      "x": -0.263,
      "y": -0.497,
      "layer": 2,
      "closed_by": [
        35,
        34
      ]
    },
    {
      "id": 44,
      "x": 0.27,
      "y": -0.5,
      "layer": 2,
      "closed_by": [
        37
      ]
    },
    {
      "id": 43,
      "x": 0.272,
      "y": 0.943,
      "layer": 2,
      "closed_by": [
        33
      ]
    },
    {
      "id": 42,
      "x": -0.261,
      "y": 0.944,
      "layer": 2,
      "closed_by": [
        32
      ]
    },
    {
      "id": 41,
      "x": 0.786,
      "y": -0.497,
      "layer": 2,
      "closed_by": [
        37,
        36
      ]
    },
    {
      "id": 40,
      "x": -0.786,
      "y": -0.495,
      "layer": 2,
      "closed_by": [
        35,
        34
      ]
    },
    {
      "id": 39,
      "x": 0.79,
      "y": 0.939,
      "layer": 2,
      "closed_by": [
        33,
        31
      ]
    },
    {
      "id": 38,
      "x": -0.782,
      "y": 0.943,
      "layer": 2,
      "closed_by": [
        32,
        30
      ]
    },
    {
      "id": 37,
      "x": 0.331,
      "y": -0.216,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        28
      ]
    },
    {
      "id": 36,
      "x": 0.842,
      "y": -0.321,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        28,
        21
      ]
    },
    {
      "id": 35,
      "x": -0.828,
      "y": -0.321,
      "angle": 9.998,
      "layer": 3,
      "closed_by": [
        29,
        23
      ]
    },
    {
      "id": 34,
      "x": -0.324,
      "y": -0.216,
      "angle": 9.998,
      "layer": 3,
      "closed_by": [
        29
      ]
    },
    {
      "id": 33,
      "x": 0.331,
      "y": 0.671,
      "angle": 9.998,
      "layer": 3,
      "closed_by": [
        26
      ]
    },
    {
      "id": 32,
      "x": -0.324,
      "y": 0.662,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        27
      ]
    },
    {
      "id": 31,
      "x": 0.837,
      "y": 0.763,
      "angle": 9.998,
      "layer": 3,
      "closed_by": [
        26,
        25
      ]
    },
    {
      "id": 30,
      "x": -0.833,
      "y": 0.757,
      "angle": 349.997,
      "layer": 3,
      "closed_by": [
        27,
        24
      ]
    },
    {
      "id": 29,
      "x": -0.504,
      "y": -0.222,
      "angle": 9.998,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 28,
      "x": 0.509,
      "y": -0.222,
      "angle": 349.997,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 27,
      "x": -0.504,
      "y": 0.657,
      "angle": 349.997,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 26,
      "x": 0.518,
      "y": 0.662,
      "angle": 9.998,
      "layer": 4,
      "closed_by": []
    },
    {
      "id": 25,
      "x": 1.054,
      "y": 0.666,
      "angle": 9.998,
      "layer": 4,
      "closed_by": [
        19
      ]
    },
    {
      "id": 24,
      "x": -1.042,
      "y": 0.662,
      "angle": 349.997,
      "layer": 4,
      "closed_by": [
        18
      ]
    },
    {
      "id": 23,
      "x": -1.042,
      "y": -0.216,
      "angle": 9.998,
      "layer": 4,
      "closed_by": [
        20
      ]
    },
    {
      "id": 22,
      "x": 1.202,
      "y": -0.388,
      "angle": 349.997,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 21,
      "x": 1.059,
      "y": -0.238,
      "angle": 349.997,
      "layer": 4,
      "closed_by": [
        22
      ]
    },
    {
      "id": 20,
      "x": -1.21,
      "y": -0.388,
      "angle": 10.0,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 19,
      "x": 1.223,
      "y": 0.833,
      "angle": 9.998,
      "layer": 5,
      "closed_by": []
    },
    {
      "id": 18,
      "x": -1.21,
      "y": 0.824,
      "angle": 349.997,
      "layer": 5,
      "closed_by": []
    }
  ],
  "cards_in_layout_amount": 34,
  "cards_in_deck_amount": 26,
  "stars_statistic": {
    "star1": 14.9,
    "star2": 76.5,
    "star3": 7.7
  }
}